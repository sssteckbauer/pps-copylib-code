000000**************************************************************/   49181208
000001*  COPYMEMBER: CPWSXBIR                                      */   49181208
000002*  RELEASE: ___1208______ SERVICE REQUEST(S): ____14918____  */   49181208
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/26/98__  */   49181208
000004*  DESCRIPTION:                                              */   49181208
000005*  - ADDED TEXT AND LABELS FOR ANTICIPATED RETIREMENT DATE,  */   49181208
000006*    DEDUCTION AMOUNT, DEDUCTION EFFECTIVE DATE ELEMENT NBR, */   49181208
000007*    DEDUCTION EFFECTIVE DATE, AND DCP PRETAX PLAN CODE.     */   49181208
000008*  - NO MARKS IN COL 73 FROM ORIGINAL RELEASE 1166; MARKED   */   49181208
000009*    'CPWSXBIR' IN COL 73 FOR RELEASE 1166.                  */   49181208
000010**************************************************************/   49181208
005200******************************************************************32591166
005300*   COPYMEMBER: CPWSXBIR                                         *32591166
005400*   RELEASE: __1166_______ SERVICE REQUEST(S): ___13259_____     *32591166
005500*   NAME:_ROY_STAPLES_____ MODIFICATION DATE:  ___01/05/98__     *32591166
005600*   DESCRIPTION:                                                 *32591166
005700*   - WORKING-STORAGE LAYOUTS FOR THE REJECTED ACTIVITY          *32591166
005800*     DETAIL LINES ON THE DBEM EXCEPTION REPORT.                 *32591166
005900*                                                                *32591166
006000******************************************************************32591166
006100*    COPYID=CPWSXBIR                                              32591166
006110******************************************************************32591166
006200                                                                  CPWSXBIR
006210 01 REJECTED-ACTIVITY-TEXT.                                       CPWSXBIR
006300*---          SHOWS ALL HEALTH PLAN REJECTED ACTIVITY             CPWSXBIR
006400  02 REJ-MAJOR-PLAN-TEXT.                                         CPWSXBIR
006500     05  REJ-MAJ-LABEL     PIC X(14) VALUE SPACE.                 CPWSXBIR
006600         88 MED-PLAN-REJ   VALUE  'MEDICAL PLAN: '.               CPWSXBIR
006700         88 DEN-PLAN-REJ   VALUE  'DENTAL PLAN:  '.               CPWSXBIR
006800         88 VIS-PLAN-REJ   VALUE  'VISION PLAN:  '.               CPWSXBIR
006900         88 LEG-PLAN-REJ   VALUE  'LEGAL PLAN:   '.               CPWSXBIR
007000     05  REJ-MAJ-CD        PIC X(02).                             CPWSXBIR
007100     05  FILLER            PIC X(02) VALUE SPACE.                 CPWSXBIR
007200     05  REJ-MAJ-CED-LBL   PIC X(04) VALUE SPACE.                 CPWSXBIR
007300         88 MED-CED-REJ              VALUE         'MED '.        CPWSXBIR
007400         88 DEN-CED-REJ              VALUE         'DEN '.        CPWSXBIR
007500         88 VIS-CED-REJ              VALUE         'VIS '.        CPWSXBIR
007600         88 LEG-CED-REJ              VALUE         'LEG '.        CPWSXBIR
007700     05  FILLER                 PIC X(05) VALUE SPACE.            CPWSXBIR
007800         88 CED-LABEL-ON                  VALUE    'CED: '.       CPWSXBIR
007900     05  REJ-MAJ-COV-EFFDT      PIC X(06).                        CPWSXBIR
008000     05  FILLER                 PIC X(02) VALUE SPACE.            CPWSXBIR
008100     05  REJ-MED-PCP-LBL        PIC X(05) VALUE SPACE.            CPWSXBIR
008200         88 MED-PCP-REJ         VALUE     'PCP: '.                CPWSXBIR
008300     05  REJ-MED-PROV-ID        PIC X(10).                        CPWSXBIR
008400     05  FILLER                 PIC X(84) VALUE SPACE.            CPWSXBIR
008500*---          EMPLOYEE PAID DISABILITY                            CPWSXBIR
008600  02 REJ-EMP-PD-DIS-TEXT.                                         CPWSXBIR
008700     05  REJ-EPD-LABEL           PIC X(26)                        CPWSXBIR
008800         VALUE 'EMPLOYEE PAID DISABILITY: '.                      CPWSXBIR
008900     05  REJ-EPD-WAIT-PERIOD     PIC ZZ9.                         CPWSXBIR
009000     05  FILLER                  PIC X(05) VALUE SPACE.           CPWSXBIR
009010     05  REJ-EPD-CED-LBL         PIC X(05) VALUE 'CED: '.         CPWSXBIR
009100     05  REJ-EPD-COV-EFFDT       PIC X(06).                       CPWSXBIR
009200     05  FILLER                  PIC X(77) VALUE SPACE.           CPWSXBIR
009300                                                                  CPWSXBIR
009400*---          EMPLOYEE PAID LIFE INSURANCE                        CPWSXBIR
009500  02 REJ-EMP-PD-LIFEINS-TEXT.                                     CPWSXBIR
009600     05  REJ-EP-LIFE-LABEL           PIC X(26) VALUE              CPWSXBIR
009700         'EMPLOYEE PAID LIFE:       ' .                           CPWSXBIR
009800     05  REJ-EP-LIFE-CD              PIC X(01).                   CPWSXBIR
009900     05  FILLER                      PIC X(08) VALUE SPACE.       CPWSXBIR
010000     05  REJ-EP-LIFE-CED-LBL         PIC X(05) VALUE 'CED: '.     CPWSXBIR
010100     05  REJ-EP-LIFE-COV-EFFDT       PIC X(06).                   CPWSXBIR
010200     05  FILLER                      PIC X(86) VALUE SPACE.       CPWSXBIR
010300                                                                  CPWSXBIR
010400*---          TIP ENROLLMENT                                      CPWSXBIR
010500  02 REJ-TIP-TEXT.                                                CPWSXBIR
010600     05  REJ-TIP-LABEL               PIC X(14) VALUE              CPWSXBIR
010700         'TIP:         '.                                         CPWSXBIR
010800     05  REJ-TIP-CD                  PIC X(01).                   CPWSXBIR
010900     05  FILLER                      PIC X(117) VALUE SPACE.      CPWSXBIR
011000                                                                  CPWSXBIR
011100*---          DEPENDENT LIFE INSURANCE                            CPWSXBIR
011200  02 REJ-DEP-LIFEINS-TEXT.                                        CPWSXBIR
011300     05  REJ-DEP-LIFE-LABEL          PIC X(26) VALUE              CPWSXBIR
011400         'DEPENDENT LIFE:           '.                            CPWSXBIR
011500     05  REJ-DEP-LIFE-CD             PIC X(01).                   CPWSXBIR
011600     05  FILLER                      PIC X(08) VALUE SPACE.       CPWSXBIR
011700     05  REJ-DEP-LIFE-CED-LBL        PIC X(05) VALUE 'CED: '.     CPWSXBIR
011800     05  REJ-DEP-LIFE-COV-EFFDT      PIC X(06).                   CPWSXBIR
011900     05  FILLER                      PIC X(86) VALUE SPACE.       CPWSXBIR
012000*---          HEALTH PLAN OPT OUT                                 CPWSXBIR
012100  02 REJ-MAJ-OPTOUT-TEXT.                                         CPWSXBIR
012200     05  REJ-MAJ-OPT-LABEL    PIC X(14) VALUE SPACE.              CPWSXBIR
012300         88 MED-OPT-REJ       VALUE 'MEDICAL PLAN: '.             CPWSXBIR
012400         88 DEN-OPT-REJ       VALUE 'DENTAL PLAN:  '.             CPWSXBIR
012500         88 VIS-OPT-REJ       VALUE 'VISION PLAN:  '.             CPWSXBIR
012600     05  REJ-MAJ-OPT-IND      PIC X(07) VALUE 'OPT OUT'.          CPWSXBIR
012700     05  FILLER               PIC X(111) VALUE SPACE.             CPWSXBIR
012800*---          AD & D PLAN                                         CPWSXBIR
012900  02 REJ-AD-D-TEXT.                                               CPWSXBIR
013000     05  REJ-AD-D-LABEL      PIC X(10) VALUE 'AD&D PLAN '.        CPWSXBIR
013100     05  REJ-AD-D-AMT-LBL    PIC X(08) VALUE 'AMOUNT: '.          CPWSXBIR
013200     05  REJ-AD-D-AMT        PIC ZZZ9.                            CPWSXBIR
013300     05  FILLER              PIC X(02).                           CPWSXBIR
013400     05  REJ-AD-D-TYPE-LBL   PIC X(11) VALUE 'AD&D TYPE: '.       CPWSXBIR
013500     05  REJ-AD-D-TYPE-CD    PIC X(01).                           CPWSXBIR
013600     05  FILLER              PIC X(03).                           CPWSXBIR
013700     05  REJ-AD-D-CED-LBL    PIC X(05) VALUE 'CED: '.             CPWSXBIR
013800     05  REJ-AD-D-COV-EFFDT  PIC X(06).                           CPWSXBIR
013900     05  FILLER              PIC X(82) VALUE SPACE.               CPWSXBIR
013920                                                                  CPWSXBIR
014300*---          DEPCARE ENROLLMENTS                                 CPWSXBIR
014400  02 REJ-DEPCARE-TEXT.                                            CPWSXBIR
014500     05  REJ-DC-LABEL        PIC X(16) VALUE 'DEPCARE AMOUNT: '.  CPWSXBIR
014600*****05  FILLER              PIC X(01).                           49181208
014610     05  FILLER              PIC X(01) VALUE SPACE.               49181208
014700     05  REJ-DC-AMOUNT       PIC ZZZZZ9.99.                       CPWSXBIR
014800     05  FILLER              PIC X(05) VALUE SPACE.               CPWSXBIR
014900     05  REJ-DC-CED-LBL      PIC X(05) VALUE 'CED: '.             CPWSXBIR
015000     05  REJ-DC-COV-EFFDT    PIC X(06).                           CPWSXBIR
015100     05  FILLER              PIC X(90) VALUE SPACE.               CPWSXBIR
015200                                                                  CPWSXBIR
015300*---          DEPENDENT ENROLLMENTS                               CPWSXBIR
015400  02 REJ-DEPENDENT-TEXT.                                          CPWSXBIR
016100     05 REJ-DEP-TRANS-TYPE     PIC X(14).                         CPWSXBIR
016200        88 REJ-DEP-ADD         VALUE 'DEPENDENT ADD:'.            CPWSXBIR
016300        88 REJ-DEP-CHG         VALUE 'DEPENDENT CHG:'.            CPWSXBIR
016400     05 FILLER                 PIC X(01).                         CPWSXBIR
016500     05 REJ-DEP-NUM            PIC X(02).                         CPWSXBIR
016600     05 FILLER                 PIC X(01).                         CPWSXBIR
016700     05 REJ-DEP-NAME           PIC X(26).                         CPWSXBIR
016800     05 FILLER                 PIC X(01).                         CPWSXBIR
016900     05 REJ-DEP-REL            PIC X(01).                         CPWSXBIR
017000     05 FILLER                 PIC X(01).                         CPWSXBIR
017100     05 REJ-DEP-BIRTH-DATE     PIC X(06).                         CPWSXBIR
017200     05 FILLER                 PIC X(01).                         CPWSXBIR
017300     05 REJ-DEP-SEX-CODE       PIC X(01).                         CPWSXBIR
017400     05 FILLER                 PIC X(01).                         CPWSXBIR
017500     05 REJ-DEP-SSN            PIC X(09).                         CPWSXBIR
017600     05 FILLER                 PIC X(01).                         CPWSXBIR
017700     05 REJ-DEP-MED-SW         PIC X(03).                         CPWSXBIR
017800        88 REJ-DEP-MED         VALUE 'MED'.                       CPWSXBIR
017900     05 FILLER                 PIC X(01).                         CPWSXBIR
018000     05 REJ-DEP-DEN-SW         PIC X(03).                         CPWSXBIR
018100        88 REJ-DEP-DEN         VALUE 'DEN'.                       CPWSXBIR
018200     05 FILLER                 PIC X(01).                         CPWSXBIR
018300     05 REJ-DEP-VIS-SW         PIC X(03).                         CPWSXBIR
018400        88 REJ-DEP-VIS         VALUE 'VIS'.                       CPWSXBIR
018500     05 FILLER                 PIC X(01).                         CPWSXBIR
018600     05 REJ-DEP-LEG-SW         PIC X(03).                         CPWSXBIR
018700        88 REJ-DEP-LEG         VALUE 'LEG'.                       CPWSXBIR
018800     05 FILLER                 PIC X(25).                         CPWSXBIR
018900                                                                  CPWSXBIR
019100*---          ANTICIPATED RETIREMENT DATE                         49181208
019200  02 REJ-ANTCP-RET-DATE-TEXT.                                     49181208
019300     05  REJ-ANTCP-RET-LABEL PIC X(24)                            49181208
019310                        VALUE 'ANTICIPATED RETIRE DATE:'.         49181208
019400     05  FILLER              PIC X(01)  VALUE SPACE.              49181208
019500     05  REJ-ANTCP-RET-DATE  PIC X(06).                           49181208
019900     05  FILLER              PIC X(101) VALUE SPACES.             49181208
020000                                                                  49181208
020100*---          DCP PRETAX PLAN CODE                                49181208
020200  02 REJ-DCP-PRETAX-TEXT.                                         49181208
020300     05  REJ-DCP-PLNCD-LABEL PIC X(21)                            49181208
020400                        VALUE 'DCP PRETAX PLAN CODE:'.            49181208
020500     05  FILLER              PIC X(05)  VALUE SPACES.             49181208
020600     05  REJ-DCP-PRETX-PLAN  PIC X(01).                           49181208
020700     05  FILLER              PIC X(105) VALUE SPACES.             49181208
020800                                                                  49181208
020900*---          DEDUCTION AMOUNT                                    49181208
021000  02 REJ-DEDUCTION-TEXT.                                          49181208
021100     05  REJ-DED-GTN-LABEL   PIC X(04)                            49181208
021200                        VALUE 'GTN:'.                             49181208
021300     05  FILLER              PIC X(01)  VALUE SPACE.              49181208
021400     05  REJ-DED-GTN-LABEL.                                       49181208
021410         07  REJ-DED-GTN-NUM PIC X(03).                           49181208
021420         07  FILLER          PIC X(01)  VALUE '-'.                49181208
021430         07  REJ-DED-GTN-TYP PIC X(01).                           49181208
021440     05  FILLER              PIC X(02)  VALUE SPACES.             49181208
021500     05  REJ-GTN-DESCR       PIC X(15).                           49181208
021600     05  REJ-DED-AMOUNT      PIC ZZZZ9.99.                        49181208
021700     05  FILLER              PIC X(02)  VALUE SPACES.             49181208
021800     05  REJ-DED-EFFDT-LBL   PIC X(10)  VALUE 'EFF DATE: '.       49181208
021900     05  REJ-DED-EFF-DATE    PIC X(06).                           49181208
022000     05  FILLER              PIC X(85)  VALUE SPACES.             49181208
