000000**************************************************************/   12680775
000001*  COPYMEMBER: CPLNKFBA                                      */   12680775
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____1268____  */   12680775
000003*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___06/01/93__  */   12680775
000004*  DESCRIPTION:                                              */   12680775
000005*   LINKAGE FOR PPFBAUTL SUBROUTINE CREATED FOR DB2 EDB      */   12680775
000007*                                                            */   12680775
000008**************************************************************/   12680775
000700******************************************************************CPLNKFBA
001000*    COPYID=CPLNKFBA                                              CPLNKFBA
001100******************************************************************CPLNKFBA
001200*01  PPFBAUTL-INTERFACE.                                          CPLNKFBA
001300*                                                                *CPLNKFBA
001400******************************************************************CPLNKFBA
001500*    P P F B A U T L   I N T E R F A C E                         *CPLNKFBA
001600******************************************************************CPLNKFBA
001700*                                                                *CPLNKFBA
001800     05  PPFBAUTL-ERROR-SW       PICTURE  X(01).                  CPLNKFBA
001900         88  PPFBAUTL-ERROR                          VALUE 'Y'.   CPLNKFBA
002000     05  PPFBAUTL-EMPLOYEE-ID    PICTURE  X(09).                  CPLNKFBA
002100     05  PPFBAUTL-ROW-COUNT      PICTURE S9(04)      COMP.        CPLNKFBA
002200******************************************************************CPLNKFBA
