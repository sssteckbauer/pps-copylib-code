000000**************************************************************/   30871401
000001*  COPYMEMBER: CPCTDESI                                      */   30871401
000002*  RELEASE: ___1401______ SERVICE REQUEST(S): _____3087____  */   30871401
000003*  NAME:_____SRS_________ CREATION DATE:      ___03/20/02__  */   30871401
000004*  DESCRIPTION:                                              */   30871401
000005*  - NEW COPY MEMBER FOR CODE TRANSLATION TABLE (DES) INPUT  */   30871401
000007**************************************************************/   30871401
000008*    COPYID=CPCTDESI                                              CPCTDESI
010000*01  DATA-ELEM-SCRN-DES-TABLE-INPUT.                              CPCTDESI
010100     05 DESI-DATABASE-ID                 PIC XXX.                 CPCTDESI
010400     05 DESI-ELEM-NO.                                             CPCTDESI
010500        10 DESI-ELEM-NO-9                PIC 9999.                CPCTDESI
010600     05 DESI-SCREEN-ID                   PIC XXXX.                CPCTDESI
010600     05 DESI-PROTECT-IND                 PIC X.                   CPCTDESI
