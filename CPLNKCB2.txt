000010**************************************************************/   36290694
000020*  COPYMEMBER: CPLNKCB2                                      */   36290694
000030*  RELEASE: ____0694____  SERVICE REQUEST(S): ____3629____   */   36290694
000040*  NAME:    __J.LUCAS___  CREATION DATE:      __08/07/92__   */   36290694
000050*  DESCRIPTION:                                              */   36290694
000060*   ADD INTERFACE FOR  LAYOFF UNIT CODE (LUC)                */   36290694
000070**************************************************************/   36290694
000100**************************************************************/   36210607
000200*  COPYMEMBER: CPLNKCB2                                      */   36210607
000300*  RELEASE: ____0607____  SERVICE REQUEST(S): ____3621____   */   36210607
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __10/14/91__   */   36210607
000500*  DESCRIPTION:                                              */   36210607
000600*   - ADD INTERFACE FOR JGID                                 */   36210607
000900**************************************************************/   36210607
000100******************************************************************30930413
000200*  COPYMEMBER: CPLNKCB2                                          *30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______    *30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____    *30930413
000600*  DESCRIPTION                                                   *30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.    *30930413
000900******************************************************************30930413
000100******************************************************************30860356
000200*  PROGRAM:  PPCB02                                              *30860356
000300*  REL#:  0356   REF: NONE  SERVICE REQUESTS:   ____3086____     *30860356
000400*  NAME ___JLT_________   MODIFICATION DATE ____05/28/88_____    *30860356
000500*  DESCRIPTION                                                   *30860356
000600*     - ADD CONSISTENCY CHECKS BETWEEN THE LEAVE ACCRUAL TABLE   *30860356
000700*       AND THE LEAVE RATE TABLE.                                *30860356
000800******************************************************************30860356
000900******************************************************************14420298
001000*  PROGRAM:  PPCB02                                              *14420298
001100*  REL#:  0298   REF: NONE  SERVICE REQUESTS:   ____1442____     *14420298
001200*  NAME ___CXR_________   MODIFICATION DATE ____05/26/87_____    *14420298
001300*  DESCRIPTION                                                   *14420298
001400*      CREATED NEW LINKAGE COPY MEMBER.                          *14420298
001500*                                                                *14420298
001600******************************************************************14420298
001700*COPYID=CPLNKCB2                                                 *CPLNKCB2
001800******************************************************************CPLNKCB2
001900*                        C P L N K C B 2                         *CPLNKCB2
002000*                                                                *CPLNKCB2
002100*01  PPCB02-INTERFACE.                                            30930413
002200*                                                                *CPLNKCB2
002300******************************************************************30860356
002400*      ONE OR BOTH CALL FUNCTIONS ARE PASSED BY THE CALLER; THE   30860356
002500*      STANDARD CALL IS PERFORMED FOR TITLE CODE, BUT, AND BRT    30860356
002600*      TABLE CONSISTENCY; THE LEAVE CALL IS PERFORMED FOR LAT AND 30860356
002700*      LRT TABLE CONSISTENCY.                                     30860356
003200*      THE JGID-TITLE CALL IS PERFORMED FOR TITLE CODE AND JGT    36210607
003201*      TABLE CONSISTENCY.                                         36210607
003202*      THE JGID-HOME CALL IS PERFORMED FOR HOME-DEPT  AND JGT     36210607
003203*      TABLE CONSISTENCY.                                         36210607
003204*      THE JGID-DESC CALL IS PERFORMED FOR JGT AND JGD TABLE      36210607
003205*      CONSISTENCY.                                               36210607
002800******************************************************************30860356
002900*                                                                 30860356
003000     05  LNKCB2-STANDARD-CALL-SW         PIC X VALUE SPACE.       30860356
003100         88  LNKCB2-STANDARD-CALL            VALUE 'X'.           30860356
003200*                                                                 30860356
003300     05  LNKCB2-LEAVE-CALL-SW            PIC X VALUE SPACE.       30860356
003400         88  LNKCB2-LEAVE-CALL               VALUE 'X'.           30860356
003401*                                                                 36210607
003402     05  LNKCB2-JGID-TITLE-CALL-SW       PIC X VALUE SPACE.       36210607
003403         88  LNKCB2-JGID-TITLE-CALL          VALUE 'X'.           36210607
003404*                                                                 36210607
003405     05  LNKCB2-JGID-HOME-CALL-SW        PIC X VALUE SPACE.       36210607
003406         88  LNKCB2-JGID-HOME-CALL           VALUE 'X'.           36210607
003407*                                                                 36210607
003408     05  LNKCB2-JGID-DESC-CALL-SW        PIC X VALUE SPACE.       36210607
003409         88  LNKCB2-JGID-DESC-CALL           VALUE 'X'.           36210607
003500*                                                                *30860356
003510     05  LNKCB2-LUC-CALL-SW              PIC X VALUE SPACE.       36290694
003520         88  LNKCB2-LUC-CALL                 VALUE 'X'.           36290694
003530*                                                                 36290694
003600******************************************************************CPLNKCB2
003700*      THE FOLLOWING ACTION CODE IS PASSED BY THE CALLER.        *CPLNKCB2
003800******************************************************************CPLNKCB2
003900*                                                                 CPLNKCB2
004000     05  LNKCB2-ACTION-CODE              PIC X(01).               CPLNKCB2
004100         88  LNKCB2-PRODUCE-PPP0198          VALUE 'R'.           CPLNKCB2
004200         88  LNKCB2-SUPPRESS-PPP0198         VALUE SPACE.         CPLNKCB2
004300*                                                                 CPLNKCB2
004400******************************************************************CPLNKCB2
004500*      THE FOLLOWING RETURN CODE IS RETURNED TO THE CALLER       *CPLNKCB2
004600******************************************************************CPLNKCB2
004700*                                                                 CPLNKCB2
004800     05  LNKCB2-RETURN-CODE              PIC X(01).               CPLNKCB2
004900         88  LNKCB2-CONSISTENT-TABLES        VALUE '0'.           CPLNKCB2
005000         88  LNKCB2-INCONSISTENT-TABLES      VALUES '1' THRU '9'. CPLNKCB2
