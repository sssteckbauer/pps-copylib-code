000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPFAU11                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______JLT_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*   Initial release of Procedure Division copymember which   */   32021138
000700*   is used by program PPP530 in conjunction with copymembers*/   32021138
000800*   CPWSXFAU and CPWSFAUR.                                   */   32021138
000900**************************************************************/   32021138
001000*                                                             *   CPPFAU11
001100*  This copymember is one of the Full Accounting Unit modules *   CPPFAU11
001200*  which campuses may need to modify to accomodate their own  *   CPPFAU11
001300*  Chart of Accounts structure.                               *   CPPFAU11
001400*                                                             *   CPPFAU11
001500***************************************************************   CPPFAU11
001600*                                                                 CPPFAU11
001700***************************************************************** CPPFAU11
001800*  Apply the Location being processed to the FAU.                 CPPFAU11
001900***************************************************************** CPPFAU11
002000                                                                  CPPFAU11
002100        MOVE FAUR-LOCATION        TO XFAU-FAU-LOCATION            CPPFAU11
002200                                  OF XFAU-FAU-UNFORMATTED         CPPFAU11
002300                                                                  CPPFAU11
002400************** END OF COPY CPPFAU11 ***************************   CPPFAU11
