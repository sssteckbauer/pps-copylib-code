000010*==========================================================%      UCSD0017
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0017
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD0017     =%      UCSD0017
000040*=                                                        =%      UCSD0017
000050*==========================================================%      UCSD0017
000060*==========================================================%      UCSD0017
000070*=    COPY MEMBER: CPFDXTOF                               =%      UCSD0017
000080*=    CHANGE # 0017         PROJ. REQUEST: DT-017         =%      UCSD0017
000090*=    NAME: G CHIU          MODIFICATION DATE: 09/04/87   =%      UCSD0017
000091*=                                                        =%      UCSD0017
000092*=    DESCRIPTION:                                        =%      UCSD0017
000093*=       ADD BLOCK CONTAINS 0 RECORDS CLAUSE.             =%      UCSD0017
000094*=                                                        =%      UCSD0017
000095*==========================================================%      UCSD0017
000100*    COPYID=CPFDXTOF                                              CPFDXTOF
000200*     FILE DESCRIPTION ENTRY FOR FICA TAPE - PRODUCED QUARTERLY   CPFDXTOF
000300*      WITHIN PPP060 MODULE.                                      CPFDXTOF
000400     LABEL RECORDS STANDARD                                       CPFDXTOF
000500     RECORDING MODE IS F                                          CPFDXTOF
000600*    BLOCK CONTAINS 10 RECORDS                                    CPFDXTOF
000610     BLOCK CONTAINS 0 RECORDS                                     UCSD0017
000700     DATA RECORD IS TAPEOUT-FICA.                                 CPFDXTOF
000800                                                                  CPFDXTOF
000900 01  TAPEOUT-FICA                PIC X(66).                       CPFDXTOF
