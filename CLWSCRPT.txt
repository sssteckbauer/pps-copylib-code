000800*==========================================================%      DS413
000801*=    COPY MEMBER: CLWSCRPT                               =%      DS413
000802*=    CHANGE #DS413         PROJ. REQUEST: COBOL II       =%      DS413
000803*=                                         CONVERSION     =%      DS413
000804*=    NAME: G. CHIU         MODIFICATION DATE: 07/13/89   =%      DS413
000805*=                                                        =%      DS413
000806*=    DESCRIPTION                                         =%      DS413
000807*=    CHANGED FOR CONVERSION FROM COBOL TO VS COBOL II.   =%      DS413
000808*=                                                        =%      DS413
000900*=                                                        =%      DS413
008000*==========================================================%      DS413
008100*    WORKING STORAGE FOR THE CONTROLREPORT                        CLWSCRPT
008200*                                                                 CLWSCRPT
008300 01  COMPILE-TIME-AND-DATE.                                       CLWSCRPT
008500     03  COMPILE-DATE        PIC X(008).                          DS413
008400     03  COMPILE-TIME        PIC X(008).                          DS413
008400*    03  COMPILE-TIME        PIC X(008).                          DS413
008500*    03  COMPILE-DATE        PIC X(012).                          DS413
008600*01  TIME-WORK-AREA.                                              DS413
008700*    03  TWA-HH              PIC X(002).                          DS413
008800*    03  TWA-MM              PIC X(002).                          DS413
008900*    03  TWA-SS              PIC X(002).                          DS413
009000*01  TIME-WORK-AREA-SPLIT.                                        DS413
009100*    03  TWA-HH              PIC X(002).                          DS413
009200*    03  FILLER              PIC X(001) VALUE ':'.                DS413
009300*    03  TWA-MM              PIC X(002).                          DS413
009400*    03  FILLER              PIC X(001) VALUE ':'.                DS413
009500*    03  TWA-SS              PIC X(002).                          DS413
009600 01  TIME-WORK-AREA.                                              DS413
009700     05  TWA-HHMMSS.                                              DS413
009800         10 TWA-HH           PIC X(002).                          DS413
009900         10 TWA-MM           PIC X(002).                          DS413
010000         10 TWA-SS           PIC X(002).                          DS413
010100     05  TWA-HS              PIC X(002).                          DS413
010200 01  TIME-WORK-AREA-SPLIT.                                        DS413
010300     05  TWA-HHMMSS.                                              DS413
010400         10  TWA-HH          PIC X(002).                          DS413
010500         10  FILLER          PIC X(001) VALUE ':'.                DS413
010600         10  TWA-MM          PIC X(002).                          DS413
010700         10  FILLER          PIC X(001) VALUE ':'.                DS413
010800         10  TWA-SS          PIC X(002).                          DS413
010900 01  STATS-OPTIONS.                                               CLWSCRPT
011000     03  SO-READ             PIC X(009) VALUE 'READ     '.        CLWSCRPT
011100     03  SO-CREATED          PIC X(009) VALUE 'CREATED  '.        CLWSCRPT
011200 01  END-OF-REPORT-MSG.                                           CLWSCRPT
011300     03  FILLER              PIC X(132) VALUE ALL                 CLWSCRPT
011400     'END OF REPORT '.                                            CLWSCRPT
011500 01  NO-CONTROL-CARD.                                             CLWSCRPT
011600     03  FILLER              PIC X(132) VALUE                     CLWSCRPT
011700     'NOT APPLICABLE'.                                            CLWSCRPT
011800 01  CTL-REPORT.                                                  CLWSCRPT
011900     03  CR-HDG-LINE1.                                            CLWSCRPT
012000         05  FILLER          PIC X(008) VALUE 'REPORT: '.         CLWSCRPT
012100         05  CR-HL1-RPT      PIC X(008).                          CLWSCRPT
012200         05  FILLER          PIC X(036) VALUE SPACES.             CLWSCRPT
012300         05  FILLER          PIC X(080) VALUE                     CLWSCRPT
012400         'UCSD PAYROLL/PERSONNEL SYSTEM'.                         CLWSCRPT
012500     03  CR-HDG-LINE2.                                            CLWSCRPT
012600         05  FILLER          PIC X(008) VALUE 'RUN ON: '.         CLWSCRPT
012700         05  CR-HL2-RUN-DATE PIC X(012).                          CLWSCRPT
012800         05  FILLER          PIC X(004) VALUE ' AT '.             CLWSCRPT
012900         05  CR-HL2-RUN-TIME PIC X(008).                          CLWSCRPT
013000         05  FILLER          PIC X(028) VALUE SPACES.             CLWSCRPT
013100         05  FILLER          PIC X(072) VALUE                     CLWSCRPT
013200         'CONTROL REPORT'.                                        CLWSCRPT
013300     03  CR-HDG-LINE3.                                            CLWSCRPT
013400         05  FILLER          PIC X(019) VALUE                     CLWSCRPT
013500         'PROGRAM COMPILED: '.                                    CLWSCRPT
013600         05  CR-HL3-CMP-DATE PIC X(012).                          CLWSCRPT
013700         05  FILLER          PIC X(004) VALUE ' AT '.             CLWSCRPT
013800         05  CR-HL3-CMP-TIME PIC X(008).                          CLWSCRPT
013900         05  FILLER          PIC X(089) VALUE SPACES.             CLWSCRPT
014000     03  CR-HDG-LINE4.                                            CLWSCRPT
014100         05  FILLER          PIC X(132) VALUE                     CLWSCRPT
014200         'INPUT CONTROLS'.                                        CLWSCRPT
014300     03  CR-DET-LINE5.                                            CLWSCRPT
014400         05  CR-DL5-CTL-CARD PIC X(132).                          CLWSCRPT
014500     03  CR-HDG-LINE6.                                            CLWSCRPT
014600         05  FILLER          PIC X(132) VALUE                     CLWSCRPT
014700         'PROGRAM STATUS'.                                        CLWSCRPT
014800     03  CR-DET-LINE7.                                            CLWSCRPT
014900         05  CR-DL7-CTL-STAT PIC X(050) VALUE                     CLWSCRPT
015000         'PROGRAM HAS SUCCESSFULLY COMPLETED AT '.                CLWSCRPT
015100         05  CR-DL7-CTL-TIME PIC X(008).                          CLWSCRPT
015200         05  FILLER          PIC X(074) VALUE SPACES.             CLWSCRPT
015300     03  CR-HDG-LINE8.                                            CLWSCRPT
015400         05  FILLER          PIC X(132) VALUE                     CLWSCRPT
015500         'PROCESS STATISTICS'.                                    CLWSCRPT
015600     03  CR-DET-LINE9.                                            CLWSCRPT
015700         05  CR-DL9-FILE     PIC X(016).                          CLWSCRPT
015800         05  FILLER          PIC X(009) VALUE ' RECORDS '.        CLWSCRPT
015900         05  CR-DL9-ACTION   PIC X(009).                          CLWSCRPT
016000         05  FILLER          PIC X(002) VALUE '= '.               CLWSCRPT
016100         05  CR-DL9-VALUE    PIC ZZ,ZZZ,ZZ9.                      CLWSCRPT
016200         05  FILLER          PIC X(086) VALUE SPACES.             CLWSCRPT
