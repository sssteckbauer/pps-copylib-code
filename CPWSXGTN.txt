000000**************************************************************/   48881487
000001*  COPYMEMBER: CPWSXGTN                                      */   48881487
000002*  RELEASE: ___1487______ SERVICE REQUEST(S): ____14888____  */   48881487
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___04/28/03__  */   48881487
000004*  DESCRIPTION:                                              */   48881487
000005*  - ADD GTN DIRECT DEPOSIT INDICATOR                        */   48881487
000007**************************************************************/   48881487
000000**************************************************************/   48071211
000001*  COPYMEMBER: CPWSXGTN                                      */   48071211
000002*  RELEASE: ___1211______ SERVICE REQUEST(S): ____14807____  */   48071211
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___10/13/98__  */   48071211
000004*  DESCRIPTION:                                              */   48071211
000006*  - ADD LINK GTN NUMBER                                     */   48071211
000005*        INITIAL LINK GTN FLAG                               */   48071211
000007**************************************************************/   48071211
000000**************************************************************/   32031184
000001*  COPYMEMBER: CPWSXGTN                                      */   32031184
000002*  RELEASE: ___1184______ SERVICE REQUEST(S): ____13203____  */   32031184
000003*  NAME:______SRS________ MODIFICATION DATE:  ___03/09/98__  */   32031184
000004*  DESCRIPTION:                                              */   32031184
000005*  - MODIFIED TO ADD COLUMN GTN-DEPT-PAR-IND                 */   32031184
000007**************************************************************/   32031184
000100**************************************************************/   32021138
000200*  COPYMEMBER: CPWSXGTN                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_____J.WILCOX____ CREATION DATE:      ___05/12/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  - Complete replacement copymember for Generic FAU Project.*/   32021138
000700*    Last revisions to prior version as of Release 717.      */   32021138
000800*  - Liability, Receivable, and Prepayment 'account' fields  */   32021138
000900*    converted to Generic FAU fields.                        */   32021138
001000*  - Entire record restructured.                             */   32021138
001100**************************************************************/   32021138
002100*    COPYID=CPWSXGTN                                              CPWSXGTN
001300*01  XGTN-GROSS-TO-NET-RECORD.                                    CPWSXGTN
002300*--------------------------------------------------------------   CPWSXGTN
002400*               G R O S S - T O - N E T  R E C O R D          *   CPWSXGTN
002500*--------------------------------------------------------------   CPWSXGTN
001700     03  XGTN-DELETE                           PIC X.             CPWSXGTN
002700     03  XGTN-KEY.                                                CPWSXGTN
001900         05  XGTN-KEY-CONSTANT                 PIC X(6).          CPWSXGTN
002000         05  XGTN-PRIORITY                     PIC X(4).          CPWSXGTN
002100         05  XGTN-DEDUCTION                    PIC 9(3).          CPWSXGTN
002200     03  XGTN-PROCESS-DATA.                                       CPWSXGTN
002300         05  XGTN-MATCH-ELMT                   PIC 9(3).          CPWSXGTN
002400         05  XGTN-DESC                         PIC X(15).         CPWSXGTN
002500         05  XGTN-TYPE                         PIC X.             CPWSXGTN
002600         05  XGTN-GROUP                        PIC X.             CPWSXGTN
003800         05  XGTN-SCHEDULES.                                      CPWSXGTN
002800             07  XGTN-SM-SCHED-CODE            PIC X.             CPWSXGTN
002900             07  XGTN-BI-SCHED-CODE            PIC X.             CPWSXGTN
003000             07  XGTN-MN-SCHED-CODE            PIC X.             CPWSXGTN
004500         05  XGTN-BALANCE-DATA.                                   CPWSXGTN
003200             07  XGTN-YTD                      PIC X.             CPWSXGTN
003300             07  XGTN-SUSPENSE                 PIC X.             CPWSXGTN
003400             07  XGTN-QTD                      PIC X.             CPWSXGTN
003500             07  XGTN-DECLIN-BAL               PIC X.             CPWSXGTN
003600             07  XGTN-ETD                      PIC X.             CPWSXGTN
003700             07  XGTN-FYTD                     PIC X.             CPWSXGTN
003800             07  XGTN-USER                     PIC X.             CPWSXGTN
003900         05  FILLER                            PIC X(2).          CPWSXGTN
004000         05  XGTN-FULL-PART                    PIC X.             CPWSXGTN
004100         05  XGTN-PRT-YTD-ON-CK                PIC X.             CPWSXGTN
004200         05  XGTN-USAGE                        PIC X.             CPWSXGTN
004300         05  XGTN-BASE                         PIC X.             CPWSXGTN
004400         05  XGTN-REDUCE-BASE                  PIC X.             CPWSXGTN
004500         05  XGTN-SP-CALC-NO                   PIC X(2).          CPWSXGTN
004600         05  XGTN-SP-CALC-IND                  PIC X.             CPWSXGTN
004700         05  XGTN-SP-UPDATE-NO                 PIC X(2).          CPWSXGTN
004800         05  XGTN-STATUS                       PIC X.             CPWSXGTN
004900         05  XGTN-STOP-AT-TERM                 PIC X.             CPWSXGTN
005000         05  XGTN-VALU-RNG-EDIT                PIC X.             CPWSXGTN
005100         05  XGTN-ACCEPTBL-VALUES.                                CPWSXGTN
005200             07  XGTN-VALUE1                   PIC 9(5)V99.       CPWSXGTN
005300             07  XGTN-VALUE2                   PIC 9(5)V99.       CPWSXGTN
005400         05  XGTN-FILLER-1                     PIC X.             CPWSXGTN
005500         05  XGTN-DEDUCTION-CODE               PIC X(2).          CPWSXGTN
005600         05  XGTN-LIABILITY-FAU                PIC X(30).         CPWSXGTN
005700         05  XGTN-LIABILITY-OBJECT             PIC X(4).          CPWSXGTN
005800         05  XGTN-BEN-CODE                     PIC X.             CPWSXGTN
005900         05  XGTN-REDUCTION-INDS.                                 CPWSXGTN
006000             07  XGTN-REDUCTION-FWT            PIC X.             CPWSXGTN
006100             07  XGTN-REDUCTION-SWT            PIC X.             CPWSXGTN
006200             07  XGTN-REDUCTION-FICA           PIC X.             CPWSXGTN
006300             07  FILLER                        PIC X.             CPWSXGTN
006400         05  XGTN-COLL-BARGN-ELIG-LEVEL-IND    PIC X.             CPWSXGTN
006500         05  XGTN-MAX-AMOUNT                   PIC 9(5)V99.       CPWSXGTN
006600         05  XGTN-COLL-BARG-BEHAVIOR-CODE      PIC X.             CPWSXGTN
006700         05  XGTN-BENEFIT-TYPE                 PIC X.             CPWSXGTN
006800         05  XGTN-BENEFIT-PLAN                 PIC X(2).          CPWSXGTN
006900         05  XGTN-SET-INDICATOR                PIC X.             CPWSXGTN
007000         05  XGTN-IGTN-INDICATOR               PIC X.             CPWSXGTN
007100         05  XGTN-IDED-INDICATOR               PIC X.             CPWSXGTN
007200         05  XGTN-IRTR-INDICATOR               PIC X.             CPWSXGTN
007300         05  XGTN-IRET-INDICATOR               PIC X.             CPWSXGTN
007400         05  XGTN-OVRD-DED-X                   PIC X(3).          CPWSXGTN
007500         05  XGTN-OVRD-DED                     REDEFINES          CPWSXGTN
007600             XGTN-OVRD-DED-X                   PIC 9(3).          CPWSXGTN
007700         05  XGTN-ICED-INDICATOR               PIC X.             CPWSXGTN
007800         05  XGTN-CON-EDIT-RTN                 PIC X(3).          CPWSXGTN
007900         05  XGTN-RECEIVABLE-FAU               PIC X(30).         CPWSXGTN
008000         05  XGTN-PREPAYMENT-FAU               PIC X(30).         CPWSXGTN
008000         05  XGTN-DEPT-PAR-IND                 PIC X(01).         32031184
008100         05  XGTN-LINK-GTN-NUM                 PIC X(03).         48071211
008100         05  XGTN-INIT-LINK-FLAG               PIC X(01).         48071211
135204         05  XGTN-DIR-DEP-IND                  PIC X(01).         48881487
135205         05  FILLER                            PIC X(12).         48881487
135206*********05  FILLER                            PIC X(13).         48881487
008200         05  XGTN-LAST-UPDT-INFO.                                 CPWSXGTN
008300             07  XGTN-LAST-UPDT-ACTION         PIC X.             CPWSXGTN
008400             07  XGTN-LAST-UPDT-DATE           PIC X(6).          CPWSXGTN
