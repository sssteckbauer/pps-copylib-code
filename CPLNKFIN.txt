000001**************************************************************/   32021138
000002*  COPYMEMBER: CPLNKFIN                                      */   32021138
000003*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000004*  NAME:______WJG________ MODIFICATION DATE:  ___08/04/97__  */   32021138
000005*  DESCRIPTION:                                              */   32021138
000006*                                                            */   32021138
000013*  - Modification of interface to PPGRSFIN in support of     */   32021138
000014*    the flexible Full Accounting Unit (3202) project        */   32021138
000018*                                                            */   32021138
000019**************************************************************/   32021138
000000**************************************************************/   28521087
000001*  COPYMEMBER: CPLNKFIN                                      */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___07/01/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  - CHANGED KFIN-FISCAL-YEAR-END AND KFIN-FISCAL-YEAR-BEGIN */   28521087
000006*    TO ISO DATE FORMAT.                                     */   28521087
000007**************************************************************/   28521087
000100******************************************************************36060628
000200*  COPY MEMBER: CPLNKFIN                                         *36060628
000300*  RELEASE # ___0628____  SERVICE REQUEST NO(S)___3606__________ *36060628
000400*  NAME ____G. STEINITZ_  DATE CREATED:  _______01/10/92_____    *36060628
000500*  DESCRIPTION                                                   *36060628
000600*     FIELDS NEEDED FOR FINANCIAL AID/WORK STUDY PROCESSING      *36060628
000700*      IN THE COMPUTE PROCESS.                                   *36060628
000800******************************************************************CPLNKFIN
000900*                        C P L N K F I N                         *CPLNKFIN
001000******************************************************************CPLNKFIN
001100*                                                                *CPLNKFIN
001200*01  FINAID-INTERFACE.                                            CPLNKFIN
001300*                                                                 CPLNKFIN
001400******************************************************************CPLNKFIN
001500     SKIP1                                                        CPLNKFIN
001600***   NBR-OF-ROWS SET IN PPP390 OR PPRCGRSS, USED IN PPGRSFIN     CPLNKFIN
001700     05  KFIN-NBR-FNW-ROWS           PIC S9(04)   SYNC  COMP.     CPLNKFIN
001800     05  KFIN-NBR-FNA-ROWS           PIC S9(04)   SYNC  COMP.     CPLNKFIN
001900     SKIP1                                                        CPLNKFIN
002000***  MAX NUMBERS SET IN PPGRSPAR FROM CPWSXIC2; USED IN PPGRSFIN. CPLNKFIN
002100     05  KFIN-MAX-PAR-ACCTS          PIC S9(04)   SYNC  COMP.     CPLNKFIN
002200     05  KFIN-MAX-FNW-ROWS           PIC S9(04)   SYNC  COMP.     CPLNKFIN
002300     05  KFIN-MAX-FNA-ROWS           PIC S9(04)   SYNC  COMP.     CPLNKFIN
002400     SKIP1                                                        CPLNKFIN
002500***  FEDERAL FUND NUMBER RANGE FROM CPWSXIC2;   USED IN PPGRSFIN. CPLNKFIN
002600***    THESE ARE PIC 9 BECAUSE OTHER REFS TO FUND ARE PIC 9       CPLNKFIN
002700**** 05  KFIN-MIN-FEDERAL-FUND       PIC  9(05).                  32021138
002800**** 05  KFIN-MAX-FEDERAL-FUND       PIC  9(05).                  32021138
002900     SKIP1                                                        CPLNKFIN
003000***  CURRENT-WPAR-SLOT SET IN PPGRSFIN, ALSO USED IN PPGRSRP5     CPLNKFIN
003100     05  KFIN-CURRENT-WPAR-SLOT      PIC S9(04)   SYNC  COMP.     CPLNKFIN
003200     SKIP1                                                        CPLNKFIN
003300***  LAST-USED-WPAR-SLOT INITIALIZED IN PPGRSPAR, POSSIBLY INCRE- CPLNKFIN
003400***  MENTED IN PPGRSFIN, AND THEN CHECKED IN PPGRSPAR.            CPLNKFIN
003500     05  KFIN-LAST-USED-WPAR-SLOT    PIC S9(04)   SYNC  COMP.     CPLNKFIN
003600     SKIP1                                                        CPLNKFIN
003700***  WSP-LIMIT-SWITCH SET FROM SYSPARM 045. 0=NO FINAID,          CPLNKFIN
003800***  1= ACCUM ONLY, 2=ACCUM AND APPLY LIMITS. USED IN PPGRSFIN    CPLNKFIN
003900     05  KFIN-WSP-LIMIT-SWITCH       PIC S9(01)         COMP-3.   CPLNKFIN
004000     SKIP1                                                        CPLNKFIN
004100***  FOLLOWING AMOUNTS SET IN PPGRSFIN FOR PPGRSRP5.              CPLNKFIN
004200     05  KFIN-LIMIT                  PIC S9(05)         COMP-3.   CPLNKFIN
004300     05  KFIN-REMAINING              PIC S9(05)V9(02)   COMP-3.   CPLNKFIN
004400     05  KFIN-AMT-OVER-LIMIT         PIC S9(05)V9(02)   COMP-3.   CPLNKFIN
004500     SKIP1                                                        CPLNKFIN
004600***  DEPT-CODE SET IN PPGRSFIN USED IN PPGRSRP5                   CPLNKFIN
004700     05  KFIN-DEPT-CODE              PIC  X(06).                  CPLNKFIN
004800     SKIP1                                                        CPLNKFIN
004900***  CURR-STUD-FLAG SET IN PPP390 OR PPRCGRSS; RESET IN PPGRSFIN  CPLNKFIN
005000     05  KFIN-CURR-STUD-FLAG         PIC  X(01).                  CPLNKFIN
005100     SKIP1                                                        CPLNKFIN
005200***  FISCAL YEAR RANGE SET IN PPP390; USED IN PPGRSFIN            CPLNKFIN
005300*****05  KFIN-FISCAL-YEAR-BEGIN.                                  28521087
005400******   10  KFIN-FY-BEGIN-YY        PIC  9(02).                  28521087
005500******   10  KFIN-FY-BEGIN-MM        PIC  9(02).                  28521087
005600******   10  KFIN-FY-BEGIN-DD        PIC  9(02).                  28521087
005700*****05  KFIN-FISCAL-YEAR-END.                                    28521087
005800*****    10  KFIN-FY-END-YY          PIC  9(02).                  28521087
005900*****    10  KFIN-FY-END-MM          PIC  9(02).                  28521087
006000*****    10  KFIN-FY-END-DD          PIC  9(02).                  28521087
006010     05  KFIN-FISCAL-YEAR-BEGIN-ISO.                              28521087
006020         10  KFIN-FY-BEGIN-CCYY      PIC  9(04).                  28521087
006021         10  KFIN-FY-BEGIN-SL1       PIC  X(01).                  28521087
006030         10  KFIN-FY-BEGIN-MM        PIC  9(02).                  28521087
006031         10  KFIN-FY-BEGIN-SL2       PIC  X(01).                  28521087
006040         10  KFIN-FY-BEGIN-DD        PIC  9(02).                  28521087
006050     05  KFIN-FISCAL-YEAR-END-ISO.                                28521087
006051         10  KFIN-FY-END-CCYY        PIC  9(04).                  28521087
006052         10  KFIN-FY-END-SL1         PIC  X(01).                  28521087
006070         10  KFIN-FY-END-MM          PIC  9(02).                  28521087
006071         10  KFIN-FY-END-SL2         PIC  X(01).                  28521087
006080         10  KFIN-FY-END-DD          PIC  9(02).                  28521087
006100     SKIP1                                                        CPLNKFIN
