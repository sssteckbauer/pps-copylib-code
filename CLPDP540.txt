000100*                                                                 CLPDP540
000200*      CONTROL REPORT PPP540CR                                    CLPDP540
000300*                                                                 CLPDP540
000400*                                                                 CLPDP540
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP540
000600                                                                  CLPDP540
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP540
000800                                                                  CLPDP540
000900     MOVE 'PPP540CR'             TO CR-HL1-RPT.                   CLPDP540
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP540
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP540
001200                                                                  CLPDP540
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP540
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP540
001500                                                                  CLPDP540
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP540
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP540
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP540
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP540
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP540
002100                                                                  CLPDP540
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP540
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP540
002400                                                                  CLPDP540
002500     MOVE SPEC-CARD-RECORD-HOLD  TO CR-DL5-CTL-CARD.              CLPDP540
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP540
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP540
002800                                                                  CLPDP540
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP540
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP540
003100                                                                  CLPDP540
003110     ACCEPT TIME-WORK-AREA FROM TIME.                             CLPDP540
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP540
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP540
003410     IF ABORT-RUN-SW = ZERO                                       CLPDP540
003420          NEXT SENTENCE                                           CLPDP540
003430     ELSE                                                         CLPDP540
003500         MOVE '******  ATTENTION PROGRAM HAS ABORTED AT  '        CLPDP540
003501            TO CR-DL7-CTL-STAT.                                   CLPDP540
003510     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP540
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP540
003700                                                                  CLPDP540
003740*                                                                 CLPDP540
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP540
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP540
004000*                                                                 CLPDP540
004100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP540
004200*                                                                 CLPDP540
004300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP540
004400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP540
004500*                                                                 CLPDP540
004600     MOVE 'TOE UPDATE RECORDS  '        TO CR-DL9-FILE.           CLPDP540
004700     MOVE SO-READ                       TO CR-DL9-ACTION.         CLPDP540
004800     MOVE TOE-UPDT-RECS-READ            TO CR-DL9-VALUE.          CLPDP540
004900     MOVE CR-DET-LINE9                  TO CONTROL-DATA.          CLPDP540
005000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP540
005100*                                                                 CLPDP540
005200*                                                                 CLPDP540
005300*                                                                 CLPDP540
005400     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP540
005500     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP540
005600                                                                  CLPDP540
005700     CLOSE CONTROLREPORT.                                         CLPDP540
