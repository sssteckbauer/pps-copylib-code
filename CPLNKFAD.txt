000100******************************************************************36090553
000200*  COPYMEMBER: CPLNKFAD                                          *36090553
000300*  RELEASE: ____0553____  SERVICE REQUEST(S): ____3609____       *36090553
000400*  NAME:__PXP___________  CREATION DATE:      __04/08/91__       *36090553
000500*  DESCRIPTION:                                                  *36090553
000600*    LINKAGE FOR PPFADUTL SUBROUTINE CREATED FOR DB2 EDB         *36090553
000700*    CONVERSION.                                                 *36090553
000800*                                                                *36090553
000900******************************************************************CPLNKFAD
001000*    COPYID=CPLNKFAD                                              CPLNKFAD
001100*01  PPFADUTL-INTERFACE.                                          CPLNKFAD
001200*                                                                *CPLNKFAD
001300******************************************************************CPLNKFAD
001400*    P P F A D U T L   I N T E R F A C E                         *CPLNKFAD
001500******************************************************************CPLNKFAD
001600*                                                                *CPLNKFAD
001700     05  PPFADUTL-ERROR-SW       PICTURE X(1).                    CPLNKFAD
001800     88  PPFADUTL-ERROR VALUE 'Y'.                                CPLNKFAD
001900     05  PPFADUTL-FAD-FOUND-SW   PICTURE X(1).                    CPLNKFAD
002000     88  PPFADUTL-FAD-FOUND VALUE 'Y'.                            CPLNKFAD
002100     05  PPFADUTL-EMPLOYEE-ID   PICTURE X(9).                     CPLNKFAD
