000100**************************************************************/   45340564
000200*  COPYMEMBER: CPFDRCPW                                      */   45340564
000300*  RELEASE: ___0564______ SERVICE REQUEST(S): _____4534____  */   45340564
000400*  NAME:___M. BAPTISTA___ MODIFICATION DATE:  ___02/20/91__  */   45340564
000500*  DESCRIPTION:                                              */   45340564
000600*  - REMOVED 'RECORD CONTAINS' STATEMENT AS IT IS NOT NEEDED */   45340564
000700*    AND WOULD REQUIRE THAT THE MIN AND MAX VALUES BE CHANGED*/   45340564
000800*    WHENEVER FILE SIZE CHANGES.                             */   45340564
000900**************************************************************/   45340564
000100**************************************************************/   36030532
000200*  COPY MODULE:  CPFDRCPW                                    */   36030532
000300*  RELEASE # ___0532___   SERVICE REQUEST NO(S)____3603______*/   36030532
000400*  NAME ___SRS_________   MODIFICATION DATE ____08/22/90_____*/   36030532
000500*  DESCRIPTION                                               */   36030532
000600*  NEW COPYLIB FOR RUSH CHECK PAYROLL AUDIT RECORD           */   36030532
000800**************************************************************/   36030532
001900     SKIP2                                                        CPFDRCPW
002000*    COPYID=CPFDRCPW                                              CPFDRCPW
002100     BLOCK CONTAINS 0 RECORDS                                     CPFDRCPW
002200     RECORDING MODE IS V                                          CPFDRCPW
002100*****RECORD CONTAINS 1269 TO 11754 CHARACTERS                     45340564
002600     LABEL RECORDS ARE STANDARD                                   CPFDRCPW
002700     DATA RECORD IS RCPW-PAYROLL-AUDIT-RECORD.                    CPFDRCPW
002800     SKIP1                                                        CPFDRCPW
