000100**************************************************************/   01171422
000200*  COPYMEMBER: CPWSRBUF                                      */   01171422
000300*  RELEASE: ___1422______ SERVICE REQUEST(S): ____80117____  */   01171422
000400*                                             ____14870____  */   01171422
000500*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___07/23/02__  */   01171422
000600*  DESCRIPTION:                                              */   01171422
000700*  - Added bargaining unit Per-item Charge and SSN Mask      */   01171422
000800*    Length fields.                                          */   01171422
000900**************************************************************/   01171422
000000**************************************************************/   48611390
000001*  COPYMEMBER: CPWSRBUF                                      */   48611390
000002*  RELEASE: ___1390______ SERVICE REQUEST(S): ____14861____  */   48611390
000003*  NAME:_______QUAN______ CREATION DATE:      ___01/24/02__  */   48611390
000004*  DESCRIPTION:                                              */   48611390
000005*  - AN ARRAY CONTAINING AGENCY FEE DATA FROM PPPBUF TABLE.  */   48611390
000007**************************************************************/   48611390
000900*    COPYID=CPWSRBUF                                              CPWSRBUF
001300*01  RBUF-BUC-ORGANIZATION-ARRAY.                                 CPWSRBUF
001473     03  RBUF-MAX-ENTRIES-ALLOWED         PIC S9(4)  COMP SYNC.   CPWSRBUF
001473     03  RBUF-MAX-ENTRIES-LOADED          PIC S9(4)  COMP SYNC.   CPWSRBUF
001474**************************************************************/   CPWSRBUF
001475*            AN ARRAY OF AGENCY FEE DATA FROM PPPBUF TABLE.  */   CPWSRBUF
001478**************************************************************/   CPWSRBUF
001479     03  RBUF-ROW-ARRAY.                                          CPWSRBUF
001480         05  RBUF-ENTRIES    OCCURS 0 TO 200 TIMES                CPWSRBUF
001480                             DEPENDING ON RBUF-MAX-ENTRIES-LOADED CPWSRBUF
001482                             INDEXED BY RBUF-INDX.                CPWSRBUF
001483             07 RBUF-BUC              PIC X(2).                   CPWSRBUF
001484             07 RBUF-REP              PIC X(1).                   CPWSRBUF
001485             07 RBUF-SHC              PIC X(1).                   CPWSRBUF
001486             07 RBUF-DISTRIBUTION     PIC X(1).                   CPWSRBUF
001487             07 RBUF-UDUE-GTN         PIC X(3).                   CPWSRBUF
001488             07 RBUF-AGENCY-FEE-GTN   PIC X(3).                   CPWSRBUF
001489             07 RBUF-AGENCY-FEE-VAL   PIC S9(7)V9(2) USAGE COMP-3.CPWSRBUF
001490             07 RBUF-CHARITY-GTN1     PIC X(3).                   CPWSRBUF
001491             07 RBUF-CHARITY-GTN2     PIC X(3).                   CPWSRBUF
001492             07 RBUF-CHARITY-GTN3     PIC X(3).                   CPWSRBUF
001493             07 RBUF-UNIT-LINK        PIC X(1).                   CPWSRBUF
001494             07 RBUF-UDUE-BASE        PIC X(2).                   CPWSRBUF
001495             07 RBUF-AF-BASE          PIC X(2).                   CPWSRBUF
001496             07 RBUF-LAST-ACTION      PIC X(1).                   CPWSRBUF
001497             07 RBUF-LAST-ACTION-DT   PIC X(10).                  CPWSRBUF
001498             07 RBUF-BUC-ORGNZ-LINK   PIC X(1).                   CPWSRBUF
004400             07 RBUF-PER-ITEM-CHRG    PIC X(1).                   01171422
004500             07 RBUF-SSN-MASK-LNGTH   PIC 9(1).                   01171422
001600*******************  END OF COPYBOOK CPWSRBUF  *******************CPWSRBUF
