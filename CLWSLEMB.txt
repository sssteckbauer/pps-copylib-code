000100     EJECT                                                        CLWSLEMB
000200*    COPYID=CLWSLEMB                                              CLWSLEMB
000300     SKIP1                                                        CLWSLEMB
000400*01  LEMB-EMPL-EXTRACT-RECORD      PICTURE  X(127).            ***CLWSLEMB
000500*                                                                 CLWSLEMB
000600*                                                                 CLWSLEMB
000630     05  LEMB-KEY.                                                CLWSLEMB
000640         10  LEMB-EMB-ID                   PICTURE  X(10).        CLWSLEMB
000650         10  LEMB-EMB-ID-TAB               PICTURE  X(1).         CLWSLEMB
000660         10  LEMB-REFRESH-VERSION-KEY.                            CLWSLEMB
000670             15  LEMB-REFRESH-DATE-FULL.                          CLWSLEMB
000680                 20  LEMB-REFRESH-CN               PICTURE  X(2). CLWSLEMB
000690                 20  LEMB-REFRESH-DATE.                           CLWSLEMB
000691                     25  LEMB-REFRESH-YR           PICTURE  X(2). CLWSLEMB
000692                     25  LEMB-REFRESH-MO           PICTURE  X(2). CLWSLEMB
000693                     25  LEMB-REFRESH-DA           PICTURE  X(2). CLWSLEMB
000694             15  LEMB-REFRESH-DATE-FULL-TAB    PICTURE  X(1).     CLWSLEMB
000695             15  LEMB-REFRESH-VERSION-CODE     PICTURE  X(1).     CLWSLEMB
000696             15  LEMB-REFRESH-VERSION-CODE-TAB PICTURE  X(1).     CLWSLEMB
000699*                                                                 CLWSLEMB
000700     05  LEMB-ID-GROUP.                                           CLWSLEMB
000701*                                                                 CLWSLEMB
000702         10  LEMB-PERSON-ID                PICTURE  X(9).         CLWSLEMB
000703         10  LEMB-PERSON-ID-TAB            PICTURE  X(1).         CLWSLEMB
000704         10  LEMB-EMPLOYEE-NUM             PICTURE  X(9).         CLWSLEMB
000705         10  LEMB-EMPLOYEE-NUM-TAB         PICTURE  X(1).         CLWSLEMB
000706*                                                                 CLWSLEMB
000709         10  LEMB-EMPLOYEE-NAME            PICTURE  X(26).        CLWSLEMB
000710         10  LEMB-EMPLOYEE-NAME-TAB        PICTURE  X(1).         CLWSLEMB
000711*                                                                 CLWSLEMB
000713     05  LEMB-CHANGE-GROUP.                                       CLWSLEMB
000714*                                                                 CLWSLEMB
002220         10  LEMB-CHANGE-DATE.                                    CLWSLEMB
002221             15  LEMB-CHANGE-DATE-CN           PICTURE  X(2).     CLWSLEMB
002222             15  LEMB-CHANGE-DATE-YR           PICTURE  X(2).     CLWSLEMB
002224             15  LEMB-CHANGE-DATE-MO           PICTURE  X(2).     CLWSLEMB
002226             15  LEMB-CHANGE-DATE-DA           PICTURE  X(2).     CLWSLEMB
002227         10  LEMB-CHANGE-DATE-TAB          PICTURE  X(1).         CLWSLEMB
002237         10  LEMB-CHANGE-USER-ID           PICTURE  X(8).         CLWSLEMB
002238         10  LEMB-CHANGE-USER-ID-TAB       PICTURE  X(1).         CLWSLEMB
002239         10  LEMB-CHANGE-USER-NAME         PICTURE  X(30).        CLWSLEMB
002240         10  LEMB-CHANGE-USER-NAME-TAB     PICTURE  X(1).         CLWSLEMB
002241         10  LEMB-CHANGE-FLAG-ARRAY.                              CLWSLEMB
002242             15  LEMB-CHANGE-FLAG-EMP          PICTURE  X(1).     CLWSLEMB
002243             15  LEMB-CHANGE-FLAG-EMP-TAB      PICTURE  X(1).     CLWSLEMB
002244             15  LEMB-CHANGE-FLAG-SEC          PICTURE  X(1).     CLWSLEMB
002245             15  LEMB-CHANGE-FLAG-SEC-TAB      PICTURE  X(1).     CLWSLEMB
002250             15  LEMB-CHANGE-FLAG-ACT          PICTURE  X(1).     CLWSLEMB
002251             15  LEMB-CHANGE-FLAG-ACT-TAB      PICTURE  X(1).     CLWSLEMB
002260             15  LEMB-CHANGE-FLAG-APP          PICTURE  X(1).     CLWSLEMB
002261             15  LEMB-CHANGE-FLAG-APP-TAB      PICTURE  X(1).     CLWSLEMB
002270             15  LEMB-CHANGE-FLAG-DIS          PICTURE  X(1).     CLWSLEMB
033400*                                                                 CLWSLEMB
033500*                                                                 CLWSLEMB
