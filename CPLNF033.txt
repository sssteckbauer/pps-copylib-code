000180*==========================================================%      UCSD9999
000181*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD9999
000182*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD9999
000183*=                                                        =%      UCSD9999
000184*==========================================================%      UCSD9999
000185*==========================================================%      UCSD0102
000186*=    PROGRAM: PPFAU033                                   =%      UCSD0102
000187*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000090*=    NAME: R WOLBERG       MODIFICATION DATE:  7/30/98   =%      UCSD0102
000189*=                                                        =%      UCSD0102
000190*=    DESCRIPTION: CHANGES MADE TO ACCOMMODATE THE LOCAL  =%      UCSD0102
000093*=                 COA.                                   =%      UCSD0102
000189*=                                                        =%      UCSD0102
000194*==========================================================%      UCSD0102
000100**************************************************************/   EFIX1228
000200*  COPYMEMBER: CPLNF033                                      */   EFIX1228
000300*  RELEASE: ___1228______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1228
000400*  NAME:_______JLT_______ MODIFICATION DATE:  ___02/03/99__  */   EFIX1228
000500*  DESCRIPTION:                                              */   EFIX1228
000600*   - ADDED F033-KEY-CALL-REPT-ONLY-IND TO REFLECT THE VALUE */   EFIX1228
000700*     OF THE "REPORT ONLY" INDICATOR FROM THE COST ACTION/   */   EFIX1228
000800*     DESIGNATED FUND TABLE (TABLE #18).                     */   EFIX1228
000900**************************************************************/   EFIX1228
000020**************************************************************/   32021176
000021*  COPYMEMBER: CPLNF033                                      */   32021176
000022*  RELEASE: ___1176______ SERVICE REQUEST(S): ____13202____  */   32021176
000023*  NAME:_______WJG_______ CREATION DATE:      ___03/03/97__  */   32021176
000024*  DESCRIPTION:                                              */   32021176
000025*                                                            */   32021176
000026*  Initial release of linkage copymember between calling     */   32021176
000027*  program and module PPFAU033                               */   32021176
000028*                                                            */   32021176
000029**************************************************************/   32021176
000900*                                                            */   CPLNF033
001000*  This copymember is one of the Full Accounting Unit modules*/   CPLNF033
001100*  which each campus may need to modify to accommodate its   */   CPLNF033
001200*  Chart of Accounts structure.                              */   CPLNF033
001300*                                                            */   CPLNF033
002100**************************************************************/   CPLNF033
002300*01  PPFAU033-INTERFACE.                                          CPLNF033
002400    05  F033-INPUTS.                                              CPLNF033
002500        10  F033-CALL-TYPE                   PIC X(04).           CPLNF033
002501            88  F033-SORT-KEY-CALL           VALUE 'KEY '.        CPLNF033
002502            88  F033-MAIN-RPT-CALL           VALUE 'REPT'.        CPLNF033
002503            88  F033-FSR-RPT-CALL            VALUE 'FSR '.        CPLNF033
002508        10  F033-FAU                         PIC X(30).           CPLNF033
002509        10  F033-ACTION-CODE-IN              PIC X(02).           CPLNF033
002510        10  F033-SORT-KEY-IN                 PIC X(55).           CPLNF033
002511        10  F033-SAVE-KEY-IN                 PIC X(55).           CPLNF033
002512****    10  F033-FSR-KEY-IN                  PIC X(42).           UCSD0102
002512        10  F033-FSR-KEY-IN                  PIC X(24).           UCSD0102
002513        10  F033-BREAK-LEVEL-IN              PIC S9(04) COMP.     CPLNF033
002520            88  F033-FIRST-SORT-RECORD       VALUE -1.            CPLNF033
003900        10  F033-KEY-CALL-REPT-ONLY-IND      PIC X(01).           EFIX1228
004000            88  F033-KEY-CALL-REPT-ONLY      VALUE 'Y'.           EFIX1228
002620    05  F033-OUTPUTS.                                             CPLNF033
002621        10  F033-FAU-RELATED-ITEMS.                               CPLNF033
002630            15  F033-FUND-GROUP-CODE         PIC X(04).           CPLNF033
002631            15  F033-ACTION-CODE-OUT         PIC X(02).           CPLNF033
002632            15  F033-FUNCTION-OUT            PIC X(02).           CPLNF033
002632            15  F033-PROGRAM-OUT             PIC X(06).           UCSD0102
002633            15  F033-SORT-FAU-OUT            PIC X(40).           CPLNF033
002634*           15  F033-FSR-FAU-OUT             PIC X(40).           UCSD0102
002634            15  F033-FSR-FAU-OUT             PIC X(22).           UCSD0102
005000*           15  F033-STA-FAU                 PIC X(30).           UCSD0102
005000            15  F033-STA-FAU                 PIC X(35).           UCSD0102
002636*           15  F033-BJL-FAU                 PIC X(24).           UCSD0102
002636            15  F033-BJL-FAU                 PIC X(30).           UCSD0102
002637*           15  F033-BJL-APPR-FAU            PIC X(24).           UCSD0102
002637            15  F033-BJL-APPR-FAU            PIC X(30).           UCSD0102
005300*           15  F033-BJL-OFFSET-FAU          PIC X(24).           UCSD0102
005300            15  F033-BJL-OFFSET-FAU          PIC X(30).           UCSD0102
005400            15  F033-TFL-FAU                 PIC X(30).           CPLNF033
005500            15  F033-TFL-APPR-FAU            PIC X(30).           CPLNF033
005600            15  F033-TFL-OFFSET-FAU          PIC X(30).           CPLNF033
005800*           15  F033-ML1-FAU                 PIC X(36).           UCSD0102
005800            15  F033-ML1-FAU                 PIC X(43).           UCSD0102
002642*           15  F033-TFL-APPR-ML-FAU         PIC X(36).           UCSD0102
002642            15  F033-TFL-APPR-ML-FAU         PIC X(43).           UCSD0102
002643*           15  F033-PRT-APPR-FAU            PIC X(30).           UCSD0102
002643            15  F033-PRT-APPR-FAU            PIC X(35).           UCSD0102
002644            15  F033-ACT-FAU-HDR             PIC X(36).           CPLNF033
002645            15  F033-PARTIAL-FAU-HEAD-1      PIC X(12).           CPLNF033
002646*           15  F033-PARTIAL-FAU-HEAD-2      PIC X(20).           UCSD0102
002646            15  F033-PARTIAL-FAU-HEAD-2      PIC X(24).           UCSD0102
006500*           15  F033-PART-FAU-SUBHEAD        PIC X(59).           UCSD0102
006500            15  F033-PART-FAU-SUBHEAD        PIC X(55).           UCSD0102
002648            15  F033-FUND-DESCRIPTION        PIC X(30).           CPLNF033
002649        10  F033-DESIG-DESCRIPTION           PIC X(30).           CPLNF033
002650        10  F033-BREAK-LEVEL-OUT             PIC S9(04) COMP.     CPLNF033
002651            88  F033-FUNCTION-BREAK          VALUE +1.            CPLNF033
007000            88  F033-NO-BREAK                VALUE ZERO.          CPLNF033
002652        10  F033-SWITCHES.                                        CPLNF033
002653            15  F033-DESIG-FUND-SW           PIC X(01).           CPLNF033
002654                88  F033-DESIGNATED-FUNDS     VALUE 'D'.          CPLNF033
002655                88  F033-NON-DESIGNATED-FUNDS VALUE 'N'.          CPLNF033
002656                88  F033-NOT-DESIGNATED-FUNDS VALUE 'O'.          CPLNF033
002657            15  F033-ACADEMIC-STAFF-SW       PIC X(01).           CPLNF033
002658        10  F033-ACCUMULATORS.                                    CPLNF033
002659            15  F033-DESIGNATED-FUNDS-COUNT  PIC S9(04) COMP.     CPLNF033
002730        10  F033-FAILURE-CODE                PIC S9(04) COMP.     CPLNF033
002731            88  F033-SUCCESSFUL              VALUE ZERO.          CPLNF033
002740        10  F033-FAILURE-TEXT                PIC X(35).           CPLNF033
