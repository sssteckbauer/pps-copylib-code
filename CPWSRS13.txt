000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWSRS13                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    PERIODIC MAINTNENANCE PASS AREA                         *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100*01  PERIODIC-MAINTENANCE-PASS-AREA.                              CPWSRS13
001200     05  RS-IDENTIFIER        PIC X(10).                          CPWSRS13
001300     05  RS-CARD-TYPE         PIC X(01).                          CPWSRS13
001400     05  FILLER               PIC X(01).                          CPWSRS13
001500     05  RS-PERIOD-INDICATORS.                                    CPWSRS13
001600         10  RS-NEXTCYCLE-MO  PIC X(02).                          CPWSRS13
001700         10  RS-NEXTCYCLE-QTR PIC X(01).                          CPWSRS13
001800         10  RS-NEXTCYCLE-YR  PIC X(01).                          CPWSRS13
001900         10  RS-NEXTCYCLE-FYR PIC X(01).                          CPWSRS13
002000     05  FILLER               PIC X(01).                          CPWSRS13
002100     05  RS-PURGE-MONTHS      PIC X(02).                          CPWSRS13
002200     05  RS-PURGE-MONTHS-9 REDEFINES RS-PURGE-MONTHS              CPWSRS13
002300                              PIC 9(02).                          CPWSRS13
002400         88  RS-VALID-PURGE        VALUES ARE 13 THRU 99.         CPWSRS13
002500     05  FILLER               PIC X(60).                          CPWSRS13
