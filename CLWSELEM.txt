000010*==========================================================%      DS413
000020*=    COPYMEMBER: CLWSELEM                                =%      DS413
000030*=    CHANGE # DS413        PROJ. REQUEST: DS413          =%      DS413
000040*=    NAME: MARI MCGEE      MODIFICATION DATE: 07/19/89   =%      DS413
000050*=                                                        =%      DS413
000060*=    DESCRIPTION:                                        =%      DS413
000070*=     COBOL II CONVERSION                                =%      DS413
000080*=                                                        =%      DS413
000090*==========================================================%      DS413
000100*01  ELEM-RETRIEVAL.                                              DS413
000200     03  ELEM-RETR-MAX           PIC S9(4) COMP SYNC.             CLWSELEM
000300         88  ELEM-RETR-IN-BOUNDS         VALUE ZERO THRU +100.    CLWSELEM
000400     03  ELEM-RETR-ACCESS            PIC X(10).                   CLWSELEM
000500         88  VALID-ACCESS                VALUE 'INITIALIZE',      CLWSELEM
000600                                               'SPLEXTRACT',      CLWSELEM
000700                                               'EXTRACT'.         CLWSELEM
000800         88  ELEM-RETR-INIT              VALUE 'INITIALIZE'.      CLWSELEM
000900         88  ELEM-RETR-SPECIAL-EXTRACT   VALUE 'SPLEXTRACT'.      CLWSELEM
001000         88  ELEM-RETR-EXTRACT           VALUE 'EXTRACT'.         CLWSELEM
001100     03  ELEM-RETR-ERROR-CODE        PIC 9(2).                    CLWSELEM
001200     03  ELEM-RETR-IO-ERROR-CODE     PIC 9(2).                    CLWSELEM
001300     03  ELEM-RETR-ENTRY     OCCURS 100 TIMES                     CLWSELEM
001400                             INDEXED BY ELEM-RETR-ENTRY-INX.      CLWSELEM
001500       04  ELEM-RETR-INPUT.                                       CLWSELEM
001600         05  ELEM-RETR-NO            PIC X(4).                    CLWSELEM
001700             88 VALID-DET-NO             VALUE '0001' THRU '5999'.CLWSELEM
001800             88 VALID-APPT-DIST-NO       VALUE '2001' THRU '4699'.CLWSELEM
001900             88 VALID-FALSE-VAR-NO       VALUE '9001' THRU '9999'.CLWSELEM
002000         05  ELEM-RETR-FALSE-VAR-ENTRY REDEFINES                  CLWSELEM
002100             ELEM-RETR-NO.                                        CLWSELEM
002200             07  ELEM-RETR-FALSE-VAR-IND                          CLWSELEM
002300                                     PIC X(1).                    CLWSELEM
002400                 88  VALID-FALSE-VAR-IND VALUE '9'.               CLWSELEM
002500             07  ELEM-RETR-FALSE-VAR PIC X(3).                    CLWSELEM
002600                 88  VALID-FALSE-VAR     VALUE '001' THRU '999'.  CLWSELEM
002700         05  ELEM-RETR-GTN-ENTRY REDEFINES                        CLWSELEM
002800             ELEM-RETR-NO.                                        CLWSELEM
002900             07  ELEM-RETR-GTN-NO    PIC X(3).                    CLWSELEM
003000                 88  VALID-GTN-NO        VALUE '001' THRU '300'.  CLWSELEM
003100             07  ELEM-RETR-GTN-IND   PIC X(1).                    CLWSELEM
003200                 88  VALID-GTN-IND       VALUE 'G', 'S', 'Q',     CLWSELEM
003300                                               'D', 'Y', 'E',     CLWSELEM
003400                                               'F', 'U'.          CLWSELEM
003500         05  ELEM-RETR-SEG-NO    REDEFINES                        CLWSELEM
003600             ELEM-RETR-NO.                                        CLWSELEM
003700             07  ELEM-RETR-SEG       PIC 9(2).                    CLWSELEM
003800             07  ELEM-RETR-SER       PIC 9(2).                    CLWSELEM
003900         05  ELEM-RETR-FORMAT        PIC X(1).                    CLWSELEM
004000             88  VALID-FORMAT            VALUE '0', '1'.          CLWSELEM
004100             88  ELEM-RETR-RAW           VALUE '0'.               CLWSELEM
004200             88  ELEM-RETR-EDITED        VALUE '1'.               CLWSELEM
004300       04  ELEM-RETR-OUTPUT.                                      CLWSELEM
004400         05  ELEM-RETR-INIT-STATUS   PIC X(1).                    CLWSELEM
004500             88  ELEM-RETR-INITIALIZED   VALUE '1'.               CLWSELEM
004600         05  ELEM-RETR-EXTR-STATUS   PIC X(1).                    CLWSELEM
004700             88  ELEM-RETR-EXTRACTED     VALUE '1'.               CLWSELEM
004800         05  ELEM-RETR-SEG-LOC       PIC 9(2).                    CLWSELEM
004900         05  ELEM-RETR-CHECK-DIGIT   PIC 9(1).                    CLWSELEM
005000         05  ELEM-RETR-START-POS     PIC 9(3).                    CLWSELEM
005100         05  ELEM-RETR-INT-LENGTH    PIC 9(2).                    CLWSELEM
005200         05  ELEM-RETR-EXT-LENGTH    PIC 9(2).                    CLWSELEM
005300         05  ELEM-RETR-DEC-SIZE      PIC 9(2).                    CLWSELEM
005400         05  ELEM-RETR-DATA-TYPE     PIC X(1).                    CLWSELEM
005500             88  ELEM-RETR-NUMERIC       VALUE '1'.               CLWSELEM
005600             88  ELEM-RETR-ALPHANUM      VALUE '2'.               CLWSELEM
005700             88  ELEM-RETR-DATE          VALUE '3'.               CLWSELEM
005800         05  ELEM-RETR-MODE          PIC X(1).                    CLWSELEM
005900             88  ELEM-RETR-CHAR          VALUE 'C'.               CLWSELEM
006000             88  ELEM-RETR-PACK          VALUE 'P'.               CLWSELEM
006100         05  ELEM-RETR-VALUE.                                     CLWSELEM
006200             07  ELEM-RETR-VALUE-CHAR                             CLWSELEM
006300                                     PIC X(1)                     CLWSELEM
006400                             OCCURS 30 TIMES                      CLWSELEM
006500                             INDEXED BY ELEM-RETR-VALUE-INX.      CLWSELEM
