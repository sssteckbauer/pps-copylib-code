000100**************************************************************/   36360847
000200*  COPYMEMBER: CPWSDVAL                                      */   36360847
000300*  RELEASE: ___0847______ SERVICE REQUEST(S): _____3636____  */   36360847
000400*  NAME:_____J.WILCOX____ CREATION DATE:      ___12/16/93__  */   36360847
000500*  DESCRIPTION:                                              */   36360847
000600*  - NEW COPYMEMBER CONTAINING THE WORKING STORAGE FIELDS    */   36360847
000700*    USED BY PROCEDURE DIVISION COPYMEMBER CPPDDVAL IN       */   36360847
000800*    INVOKING LE/370 DATE VALIDATION SERVICES.               */   36360847
000900*  - MAY ALSO BE USED TO PROVIDE WORKING STORAGE FIELDS      */   36360847
001000*    FOR DIRECT CALLS TO LE/370 DATE VALIDATION SERVICES     */   36360847
001100**************************************************************/   36360847
001200*                                                                 CPWSDVAL
001300* THIS WORKING STORAGE COPYMEMBER PROVIDES THE NECESSARY WORK     CPWSDVAL
001400* FIELDS FOR THE DATE VALIDATION ROUTINE CONTAINED IN PROCEDURE   CPWSDVAL
001500* DIVISION COPYMEMBER CPPDDVAL, OR FOR DIRECT CALLS TO LE/370     CPWSDVAL
001600* DATE/TIME VALIDATION SERVICES.                                  CPWSDVAL
001700*                                                                 CPWSDVAL
001800* NOTE: WHEN YOU COPY THIS MEMBER, YOU *MUST* DO A                CPWSDVAL
001900*                                                                 CPWSDVAL
002000*   COPY REPLACING ==PIC X(LENGTH)== BY ==PIC X(N)==              CPWSDVAL
002100*                                                                 CPWSDVAL
002200* WHERE 'N' IS THE MAXIMUM LENGTH OF THE DATE AND DATE FORMAT     CPWSDVAL
002300* STRINGS YOU ARE GOING TO USE.                                   CPWSDVAL
002400*                                                                 CPWSDVAL
002500* FOR EXAMPLE, IF YOU WERE GOING TO VALIDATE DATES IN THE         CPWSDVAL
002600* FORMAT MM/DD/YYYY, THEN THE LENGTH OF BOTH THE DATE STRING      CPWSDVAL
002700* AND THE DATE FORMAT STRING WOULD BE 10, SO YOU WOULD USE        CPWSDVAL
002800*                                                                 CPWSDVAL
002900*    COPY REPLACING ==PIC X(LENGTH)== BY ==PIC X(10)==            CPWSDVAL
003000*                                                                 CPWSDVAL
003100*01  DVAL-WORK-AREAS.                                             CPWSDVAL
003200     05  DVAL-PROGNAMES.                                          CPWSDVAL
003300         10  DVAL-PGM-CEEDAYS          PIC X(08) VALUE 'CEEDAYS'. CPWSDVAL
003400         10  DVAL-PGM-CEEDCOD          PIC X(08) VALUE 'CEEDCOD'. CPWSDVAL
003500     05  DVAL-CHARDATE.                                           CPWSDVAL
003600         10  DVAL-CHARDATE-LENGTH      PIC S9(4) COMP.            CPWSDVAL
003700         10  DVAL-CHARDATE-STRING      PIC X(LENGTH).             CPWSDVAL
003800     05  DVAL-DATEPIC.                                            CPWSDVAL
003900         10  DVAL-DATEPIC-LENGTH       PIC S9(4) COMP.            CPWSDVAL
004000         10  DVAL-DATEPIC-STRING       PIC X(LENGTH).             CPWSDVAL
004100     05  DVAL-MESSAGE-NUMBER           PIC S9(4) COMP.            CPWSDVAL
004200     SKIP1                                                        CPWSDVAL
004300     05  DVAL-LILIAN-DATE              PIC S9(9) COMP.            CPWSDVAL
004400     05  DVAL-FEEDBACK-CODE            PIC X(12).                 CPWSDVAL
004500     05  DVAL-SEVERITY                 PIC S9(4) COMP.            CPWSDVAL
004600     05  DVAL-CASE                     PIC S9(4) COMP.            CPWSDVAL
004700     05  DVAL-SEVERITY-2               PIC S9(4) COMP.            CPWSDVAL
004800     05  DVAL-CONTROL                  PIC S9(4) COMP.            CPWSDVAL
004900     05  DVAL-FACILITY-ID              PIC X(3).                  CPWSDVAL
005000     05  DVAL-I-S-INFO                 PIC S9(9) COMP.            CPWSDVAL
005100     05  DVAL-FEEDBACK-CODE-2          PIC X(12).                 CPWSDVAL
