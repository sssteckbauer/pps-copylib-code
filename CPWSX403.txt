000100**************************************************************/   34791102
000200*  COPYMEMBER: CPWSX403                                      */   34791102
000300*  RELEASE: ____1102____  SERVICE REQUEST(S): ___13479____   */   34791102
000400*  NAME:    __S.ISAACS__  MODIFICATION DATE:  __12/10/96__   */   34791102
000500*  DESCRIPTION:                                              */   34791102
000700*    - NEW COPYMEMBER DEFINING BENEFITS 403B INTERFACE FILE. */   34791102
001800**************************************************************/   34791102
001900*    COPYID=CPWSXTCP                                              CPWSX403
002000*01  X403-INTERFACE-REC.                                          CPWSX403
002700     05  X403-SSN                    PIC X(09).                   CPWSX403
002800     05  X403-EMP-NAME               PIC X(26).                   CPWSX403
002900     05  X403-LOCATION-CODE          PIC X(02).                   CPWSX403
003000     05  X403-DESCRIPTION            PIC X(08) VALUE 'W2 TOTAL'.  CPWSX403
003100     05  X403-TRANS-TYPE             PIC X(03) VALUE '900'.       CPWSX403
003200     05  X403-LAST-DATE-OF-TAX-YR.                                CPWSX403
003200         10 X403-LAST-DATE-YR        PIC X(02).                   CPWSX403
003200         10 X403-LAST-DATE-MMDD      PIC X(04).                   CPWSX403
003200     05  X403-403B-CONTRIBUTION      PIC 9(07)V99.                CPWSX403
