000100*==========================================================%      DUNNN
000200*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      DUNNN
000300*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - DUNNN        =%      DUNNN
000400*==========================================================%      DUNNN
000500*==========================================================%      DU053
000600*=    PROGRAM: CLWSXLVW                                   =%      DU053
000700*=    CHANGE # DU053        PROJ. REQUEST: LEAVE PGM MAINT=%      DU053
000800*=    NAME:JIM PIERCE       MODIFICATION DATE:09/21/94    =%      DU053
000900*=                                                        =%      DU053
001000*=    DESCRIPTION: COPYMEMBER 01-LEVEL COMMENTED OUT      =%      DU053
001100*==========================================================%      DU053
000200*    COPYID=CLWSXLVW                                              CLWSXLVW
000300     SKIP1                                                        CLWSXLVW
001400*01  COST-WORK-RECORD.                                            DU053
000500*                                                                 CLWSXLVW
000600*        RECORD SIZE = 180 BYTES                                  CLWSXLVW
000700*                                                                 CLWSXLVW
000800     05  FINANCIAL-GROUP-INFO.                                    CLWSXLVW
000900         10  PROCESS-DATE-LONG.                                   CLWSXLVW
001000             15  PROCESS-DATE-CN  PICTURE  X(2).                  CLWSXLVW
001100             15  PROCESS-DATE.                                    CLWSXLVW
001200                 20  PROCESS-DATE-YR  PICTURE  X(2).              CLWSXLVW
001300                 20  PROCESS-DATE-MO  PICTURE  X(2).              CLWSXLVW
001400                 20  PROCESS-DATE-DA  PICTURE  X(2).              CLWSXLVW
001500         10  IFIS-ACCT-INFO.                                      CLWSXLVW
001600             15  IFIS-INDEX       PICTURE  X(7).                  CLWSXLVW
001700             15  IFIS-SUB-ACCT    PICTURE  X(1).                  CLWSXLVW
001800             15  IFIS-FUND        PICTURE  X(6).                  CLWSXLVW
001900             15  IFIS-ORG         PICTURE  X(6).                  CLWSXLVW
002000             15  IFIS-PROG        PICTURE  X(6).                  CLWSXLVW
002100         10  OLD-ACCT-INFO.                                       CLWSXLVW
002200             15  OLD-ACCOUNT      PICTURE  X(6).                  CLWSXLVW
002300             15  OLD-FUND         PICTURE  X(5).                  CLWSXLVW
002400         10  LEAVE-FUND-TYPE  PICTURE  X(1).                      CLWSXLVW
002500             88  STATE-FUND-TYPE  VALUE  'S'.                     CLWSXLVW
002600             88  HOSP-FUND-TYPE   VALUE  'H'.                     CLWSXLVW
002700             88  OTHER-FUND-TYPE  VALUE  'O'.                     CLWSXLVW
002800         10  GROUP-TIMEKEEPER.                                    CLWSXLVW
002900             15  GROUP-TMKP-UNIT  PICTURE  9(6).                  CLWSXLVW
003000             15  GROUP-TMKP-CODE  PICTURE  9(2).                  CLWSXLVW
003100         10  LCW-FILLER-1     PICTURE  X(2).                      CLWSXLVW
003200         10  RECORD-TYPE      PICTURE  X(1).                      CLWSXLVW
003300             88  GROUP-REC-TYPE   VALUE  '1' .                    CLWSXLVW
003400             88  DETAIL-REC-TYPE  VALUE  '2' .                    CLWSXLVW
003500*                                                                 CLWSXLVW
003600     05  DETAIL-INFO.                                             CLWSXLVW
003700         10  EMPLOYEENO       PICTURE  9(9).                      CLWSXLVW
003800         10  PERIOD-DATE-LONG.                                    CLWSXLVW
003900             15  PERIOD-DATE-CN   PICTURE  X(2).                  CLWSXLVW
004000             15  PERIOD-DATE.                                     CLWSXLVW
004100                 20  PERIOD-DATE-YR   PICTURE  X(2).              CLWSXLVW
004200                 20  PERIOD-DATE-MO   PICTURE  X(2).              CLWSXLVW
004300                 20  PERIOD-DATE-DA   PICTURE  X(2).              CLWSXLVW
004400         10  HOURS-IN-MONTH   PICTURE  9(3).                      CLWSXLVW
004500         10  EMPNAME          PICTURE  X(20).                     CLWSXLVW
004600         10  LCW-FILLER-2     PICTURE  X(2).                      CLWSXLVW
004700         10  BENEFIT-CLASS    PICTURE  X(2).                      CLWSXLVW
004800         10  BEN-CLASS-FACTOR PICTURE  9(1)V9(4).                 CLWSXLVW
004900         10  VAC-ACCR-FACTOR  PICTURE  9(1)V9(4).                 CLWSXLVW
005000         10  EMP-TIMEKEEPER.                                      CLWSXLVW
005100             15  EMP-TMKP-UNIT    PICTURE  9(6).                  CLWSXLVW
005200             15  EMP-TMKP-CODE    PICTURE  9(2).                  CLWSXLVW
005300         10  PAY-RATE-SIGN    PICTURE  X(1).                      CLWSXLVW
005400         10  PAY-RATE         PICTURE  9(5)V9(2).                 CLWSXLVW
005500         10  PAY-RATE-TYPE    PICTURE  X(1).                      CLWSXLVW
005600             88  PAY-RATE-HOURLY  VALUE  'H'.                     CLWSXLVW
005700             88  PAY-RATE-MONTHLY VALUE  'M'.                     CLWSXLVW
005800         10  TERMINAL-FLAG    PICTURE  X(1).                      CLWSXLVW
005900             88  TERMINAL-LEAVE   VALUE  'T'.                     CLWSXLVW
006000         10  VAC-ACCR-INFO.                                       CLWSXLVW
006100             15  VAC-ACCR-HOURS-S PICTURE  X(1).                  CLWSXLVW
006200             15  VAC-ACCR-HOURS   PICTURE  9(2)V9(6).             CLWSXLVW
006300             15  VAC-ACCR-PAY-S   PICTURE  X(1).                  CLWSXLVW
006400             15  VAC-ACCR-PAY     PICTURE  9(5)V9(2).             CLWSXLVW
006500             15  VAC-ACCR-BEN-S   PICTURE  X(1).                  CLWSXLVW
006600             15  VAC-ACCR-BEN     PICTURE  9(5)V9(2).             CLWSXLVW
006700         10  VAC-USED-INFO.                                       CLWSXLVW
006800             15  VAC-USED-HOURS-S PICTURE  X(1).                  CLWSXLVW
006900             15  VAC-USED-HOURS   PICTURE  9(3)V9(6).             CLWSXLVW
007000             15  VAC-USED-PAY-S   PICTURE  X(1).                  CLWSXLVW
007100             15  VAC-USED-PAY     PICTURE  9(5)V9(2).             CLWSXLVW
007200             15  VAC-USED-BEN-S   PICTURE  X(1).                  CLWSXLVW
007300             15  VAC-USED-BEN     PICTURE  9(5)V9(2).             CLWSXLVW
