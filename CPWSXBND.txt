000100**************************************************************/   15410872
000200*  COPYMEMBER: CPWSXBND                                      */   15410872
000300*  RELEASE: ___0872______ SERVICE REQUEST(S): ____11541____  */   15410872
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___02/09/94__  */   15410872
000500*  DESCRIPTION:                                              */   15410872
000600*  - MODIFICATIONS TO SUPPORT RECORD CHANGES REQUESTED BY    */   15410872
000700*    THE FEDERAL RESERVE BANK.                               */   15410872
000800**************************************************************/   15410872
000000**************************************************************/   02190662
000001*  COPYMEMBER: CPWSXBND                                      */   02190662
000002*  RELEASE: ___0662______ SERVICE REQUEST(S): ____10219____  */   02190662
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___03/23/92__  */   02190662
000004*  DESCRIPTION:                                              */   02190662
000008*  RECORD LAYOUT FOR FEDERAL RESERVE TAPE OUTPUT OF PPP580   */   02190662
000009*  FOR U.S. SAVINGS BOND DEDUCTIONS                          */   02190662
000010*                                                            */   02190662
000011*  - THIS COMPLETELY REPLACES THE PREVIOUS CPWSXBND.         */   02190662
000012*                                                            */   02190662
000013*    SIGNIFICANT CHANGES HAVE BEEN MADE TO THE OUTPUT FILE   */   02190662
000014*    DUE TO FEDERAL RESERVE BANK REQUIREMENTS. A HEADER AND  */   02190662
000015*    TRAILER RECORD HAVE BEEN ADDED, WHICH REQUIRE NEW DATA. */   02190662
000016*    CPWSXICD HAS BEEN CHANGED TO INCLUDE THAT DATA.         */   02190662
000018*                                                            */   02190662
000019*    SOME FIELDS ARE DEFINED AS OPTIONAL BY THE FRB. IF NOT  */   02190662
000020*    USED, THEY MUST BE BLANK, NOT ZERO FILLED. THE FOLLOWING*/   02190662
000021*    FIELDS HAVE PIC X REDEFINES TO ALLOW SPACES.            */   02190662
000022*        05 XBND-HDR-COMP-ZIP4      PIC 9(04)                */   02190662
000023*        05 XBND-HDR-RESRV-ACCT     PIC 9(09)                */   02190662
000024*        05 XBND-HDR-RESRV-BRANCH   PIC 9(04)                */   02190662
000025*        05 XBND-DET-OWNER-ZIP4     PIC 9(04)                */   02190662
000028*           07 XBND-DET-ISSUE-MM    PIC 9(02)                */   02190662
000029*           07 XBND-DET-ISSUE-YY    PIC 9(02)                */   02190662
000030**************************************************************/   02190662
002600*    COPYID=CPWSXBND                                              CPWSXBND
002700*01  XBND-BOND-FILE.                                              CPWSXBND
002800*---------------------------------------------------------------  CPWSXBND
002900*           FEDERAL RESERVE TAPE - U.S. SAVINGS BONDS             CPWSXBND
003000*---------------------------------------------------------------  CPWSXBND
003101     03  XBND-HEADER-RECORD.                                      CPWSXBND
003102         05 XBND-HDR-ID             PIC X(01).                    CPWSXBND
003103         05 XBND-HDR-ISSUE-DATE.                                  CPWSXBND
003104            07 XBND-HDR-ISSUE-MM    PIC 9(02).                    CPWSXBND
003105            07 XBND-HDR-ISSUE-YY    PIC 9(02).                    CPWSXBND
004400*********05 XBND-HDR-COMP-ID        PIC X(10).                    15410872
004500         05 XBND-HDR-COMPANY-ID     PIC X(06).                    15410872
004600         05 FILLER                  PIC X(04).                    15410872
004700*********05 XBND-HDR-PLANT-ID       PIC X(10).                    15410872
004800         05 XBND-HDR-LOCATION-ID    PIC X(04).                    15410872
004900         05 FILLER                  PIC X(06).                    15410872
003108         05 XBND-HDR-END-DATE.                                    CPWSXBND
003109            07 XBND-HDR-END-MM      PIC 9(02).                    CPWSXBND
003110            07 XBND-HDR-END-DD      PIC 9(02).                    CPWSXBND
003111            07 XBND-HDR-END-YY      PIC 9(02).                    CPWSXBND
003112         05 XBND-HDR-COMP-NAME      PIC X(33).                    CPWSXBND
003113         05 XBND-HDR-COMP-STREET    PIC X(33).                    CPWSXBND
003114         05 XBND-HDR-COMP-CITY      PIC X(20).                    CPWSXBND
003115         05 XBND-HDR-COMP-STATE     PIC X(02).                    CPWSXBND
003116         05 XBND-HDR-COMP-ZIP5      PIC 9(05).                    CPWSXBND
003117         05 XBND-HDR-COMP-ZIP4      PIC 9(04).                    CPWSXBND
003118         05 XBND-HDR-COMP-ZIP4-X REDEFINES                        CPWSXBND
003119            XBND-HDR-COMP-ZIP4      PIC X(04).                    CPWSXBND
003120         05 XBND-HDR-RESRV-ACCT     PIC 9(09).                    CPWSXBND
003121         05 XBND-HDR-RESRV-ACCT-X REDEFINES                       CPWSXBND
003122            XBND-HDR-RESRV-ACCT     PIC X(09).                    CPWSXBND
003123         05 XBND-HDR-RESRV-BRANCH   PIC 9(04).                    CPWSXBND
003124         05 XBND-HDR-RESRV-BRANCH-X REDEFINES                     CPWSXBND
003125            XBND-HDR-RESRV-BRANCH   PIC X(04).                    CPWSXBND
003126         05 XBND-HDR-PAY-PROMO      PIC 9(01).                    CPWSXBND
003127            88 XBND-HDR-PAY                       VALUE 0.        CPWSXBND
003128            88 XBND-HDR-PROMO                     VALUE 1.        CPWSXBND
003129         05 XBND-HDR-BOND50         PIC 9(06).                    CPWSXBND
003130         05 XBND-HDR-BOND75         PIC 9(06).                    CPWSXBND
003131         05 XBND-HDR-BOND100        PIC 9(06).                    CPWSXBND
003132         05 XBND-HDR-BOND200        PIC 9(06).                    CPWSXBND
003133         05 XBND-HDR-BOND500        PIC 9(06).                    CPWSXBND
003134         05 XBND-HDR-BOND1K         PIC 9(06).                    CPWSXBND
003135         05 XBND-HDR-BOND5K         PIC 9(06).                    CPWSXBND
003136         05 XBND-HDR-BOND10K        PIC 9(06).                    CPWSXBND
003137         05 XBND-HDR-ISSUE-TOT      PIC 9(08)V99.                 CPWSXBND
003138         05 XBND-HDR-DISTRICT       PIC 9(03).                    CPWSXBND
008100         05 FILLER                  PIC X(117).                   15410872
008200*********05 XBND-HDR-FRB-SPACE      PIC X(30).                    15410872
008300*********05 FILLER                  PIC X(87).                    15410872
003141*                                                                 CPWSXBND
003142     03  XBND-DETAIL-RECORD.                                      CPWSXBND
003143         05 XBND-DET-ID             PIC X(01).                    CPWSXBND
003144         05 XBND-DET-OWNER-SS-NO    PIC X(09).                    CPWSXBND
003145         05 XBND-DET-OWNER-NAME     PIC X(33).                    CPWSXBND
003146         05 XBND-DET-USA-FOREIGN    PIC 9(01).                    CPWSXBND
003147            88 XBND-DET-USA-ADDR                  VALUE 0.        CPWSXBND
003148            88 XBND-DET-FOREIGN-ADDR              VALUE 1.        CPWSXBND
003149         05 XBND-DET-MAILTO-CD      PIC 9(01).                    CPWSXBND
003150            88 XBND-DET-MAILTO-OWNER              VALUE 0.        CPWSXBND
003151            88 XBND-DET-MAILTO-ADDR1              VALUE 1.        CPWSXBND
003152            88 XBND-DET-MAILTO-COMPANY            VALUE 2.        CPWSXBND
003153         05 XBND-DET-OWNER-ADDR1    PIC X(33).                    CPWSXBND
003154         05 XBND-DET-MAILTO REDEFINES XBND-DET-OWNER-ADDR1.       CPWSXBND
003155            07 XBND-DET-MAILTO-NAME PIC X(23).                    CPWSXBND
003156            07 FILLER               PIC X(10).                    CPWSXBND
003157         05 XBND-DET-OWNER-ADDR2    PIC X(33).                    CPWSXBND
003158         05 XBND-DET-OWNER-ADDR3    PIC X(33).                    CPWSXBND
003159         05 XBND-DET-OWNER-CITY     PIC X(20).                    CPWSXBND
003160         05 XBND-DET-OWNER-STATE    PIC X(02).                    CPWSXBND
003161         05 XBND-DET-OWNER-ZIP5     PIC 9(05).                    CPWSXBND
003162         05 XBND-DET-OWNER-ZIP4     PIC 9(04).                    CPWSXBND
003163         05 XBND-DET-OWNER-ZIP4-X REDEFINES                       CPWSXBND
003164            XBND-DET-OWNER-ZIP4     PIC X(04).                    CPWSXBND
003165         05 XBND-DET-BEN-CO-DESC    PIC X(03).                    CPWSXBND
003166            88 XBND-DET-NO-CO-BENE                VALUE '   '.    CPWSXBND
003167            88 XBND-DET-CO-OWNER                  VALUE 'OR '.    CPWSXBND
003168            88 XBND-DET-BENEFICIARY               VALUE 'POD'.    CPWSXBND
003169         05 XBND-DET-BEN-CO-NAME    PIC X(28).                    CPWSXBND
003170         05 XBND-DET-DENOM-CODE     PIC 9(01).                    CPWSXBND
003171            88 XBND-DET-DENOM-50                  VALUE 2.        CPWSXBND
003172            88 XBND-DET-DENOM-75                  VALUE 3.        CPWSXBND
003173            88 XBND-DET-DENOM-100                 VALUE 4.        CPWSXBND
003174            88 XBND-DET-DENOM-200                 VALUE 5.        CPWSXBND
003175            88 XBND-DET-DENOM-500                 VALUE 6.        CPWSXBND
003176            88 XBND-DET-DENOM-1K                  VALUE 7.        CPWSXBND
003177            88 XBND-DET-DENOM-5K                  VALUE 8.        CPWSXBND
003178            88 XBND-DET-DENOM-10K                 VALUE 9.        CPWSXBND
003179         05 XBND-DET-NO-BONDS       PIC 9(04).                    CPWSXBND
012300*********05 XBND-DET-COMP-ID        PIC X(10).                    15410872
012400         05 XBND-DET-COMPANY-ID     PIC X(06).                    15410872
012500         05 FILLER                  PIC X(04).                    CPWSXBND
012600*********05 XBND-DET-PLANT-ID       PIC X(10).                    15410872
012700         05 XBND-DET-LOCATION-ID    PIC X(04).                    15410872
012800         05 FILLER                  PIC X(06).                    15410872
003400         05 XBND-DET-EMP-NO         PIC 9(09).                    CPWSXBND
003500         05 XBND-DET-EMP-NO-X REDEFINES                           CPWSXBND
003600            XBND-DET-EMP-NO         PIC X(09).                    CPWSXBND
004800         05 XBND-DET-AVG-ISSUE-DATE.                              CPWSXBND
004900            07 XBND-DET-ISSUE-MM    PIC 9(02).                    CPWSXBND
004910            07 XBND-DET-ISSUE-YY    PIC 9(02).                    CPWSXBND
004911         05 XBND-DET-AVG-ISSUE-DATE-X REDEFINES                   CPWSXBND
004912            XBND-DET-AVG-ISSUE-DATE PIC X(04).                    CPWSXBND
013700         05 FILLER                  PIC X(76).                    15410872
013800*********05 XBND-DET-FRB-SPACE      PIC X(30).                    15410872
013900*********05 FILLER                  PIC X(46).                    15410872
005200*                                                                 CPWSXBND
005210     03  XBND-TRAILER-RECORD.                                     CPWSXBND
005400         05 XBND-TRL-ID             PIC 9(10).                    CPWSXBND
005500         05 FILLER                  PIC X(310).                   CPWSXBND
