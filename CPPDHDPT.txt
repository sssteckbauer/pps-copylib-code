000008**************************************************************/   32021138
000009*  COPYMEMBER: CPPDHDPT                                      */   32021138
000010*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000011*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___08/04/97__  */   32021138
000012*  DESCRIPTION:                                              */   32021138
000013*  - MODIFIED FOR FULL ACCOUNTING UNIT.                      */   32021138
000014**************************************************************/   32021138
000100******************************************************************28551032
000200*  COPYMEMBER: CPPDHDPT                                          *28551032
000300*  RELEASE: ___1032______ SERVICE REQUEST(S): ____12855____      *28551032
000400*  NAME:_______JLT_______ MODIFICATION DATE:  ___11/14/95__      *28551032
000500*  DESCRIPTION:                                                  *28551032
000600*  - RESOLVE IDENTICAL COLUMN NAMES WHEN MULTIPLE VIEWS ARE      *28551032
000700*    USED IN PROGRAM.                                            *28551032
000800******************************************************************28551032
000000******************************************************************36410762
000001*  COPYMEMBER: CPPDHDPT                                          *36410762
000002*  RELEASE: ___0762______ SERVICE REQUEST(S): _____3641____      *36410762
000003*  REF. RELEASE: ___717___                                       *36410762
000004*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___05/05/93__      *36410762
000005*  DESCRIPTION:                                                  *36410762
000006*  - ERROR REPORT 974                                            *36410762
000007*    SET KRMI-RETURN-WITH-VSAM-ERR TO TRUE IF ENCOUNTERS         *36410762
000008*    VSAM ERROR.                                                 *36410762
000009******************************************************************36410762
000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPPDHDPT                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    HOME DEPARTMENT DERIVATION                              *|*36410717
000900*|*    INCLUDED IN PPEA001, PPEA002, PPEI503                   *|*36410717
001000*|**************************************************************|*36410717
001100*----------------------------------------------------------------*36410717
001200* COPY-ID=CPPDHDPT                                                CPPDHDPT
001300******************************************************************CPPDHDPT
001400** IF HOME DEPARTMENT NUMBER IS ZERO, USE THE LOCATION, ACCOUNT,**CPPDHDPT
001500** AND FUND OF THE FIRST NON-ZERO DISTRIBUTION FOR AN EMPLOYEE  **CPPDHDPT
001600** TO OBTAIN THE HOME DEPARTMENT NUMBER FROM THE ACCOUNT-FUND   **CPPDHDPT
001700** PROFILE.                                                     **CPPDHDPT
001800******************************************************************CPPDHDPT
001900*-----------------------------------------------------------------CPPDHDPT
002000*XXXX-HOME-DEPT  SECTION.                                         CPPDHDPT
002100*-----------------------------------------------------------------CPPDHDPT
002200                                                                  CPPDHDPT
002300     MOVE SPACES                  TO WRK-HOME-DEPT.               CPPDHDPT
002400                                                                  CPPDHDPT
002500     SET DISTRIBUTION-NOT-FOUND   TO TRUE.                        CPPDHDPT
002600                                                                  CPPDHDPT
002700     PERFORM                                                      CPPDHDPT
002800         VARYING APPT-PNTR FROM +1 BY +3                          CPPDHDPT
002900           UNTIL APPT-PNTR > 25                                   CPPDHDPT
003000              OR DISTRIBUTION-FOUND                               CPPDHDPT
003100                                                                  CPPDHDPT
003200         IF  APPT-NUM  OF APP-ROW (APPT-PNTR, 1) NOT = ZERO       CPPDHDPT
003300                                                                  CPPDHDPT
003400             COMPUTE NEXT-APPT-PNTR = APPT-PNTR + 3               CPPDHDPT
003500                                                                  CPPDHDPT
003600             MOVE 2               TO DIST-PNTR                    CPPDHDPT
003700                                                                  CPPDHDPT
003800             PERFORM                                              CPPDHDPT
003900                 VARYING CURR-APPT-PNTR FROM APPT-PNTR BY +1      CPPDHDPT
004000                   UNTIL CURR-APPT-PNTR NOT < NEXT-APPT-PNTR      CPPDHDPT
004100                      OR DISTRIBUTION-FOUND                       CPPDHDPT
004200                                                                  CPPDHDPT
006100*****            PERFORM XXXX-ACCESS-AFI                          32021138
006110                 PERFORM XXXX-ACCESS-FAU                          32021138
004400                     VARYING DIST-PNTR FROM DIST-PNTR BY +1       CPPDHDPT
004500                       UNTIL DIST-PNTR  > +3                      CPPDHDPT
004600                          OR DISTRIBUTION-FOUND                   CPPDHDPT
004700                                                                  CPPDHDPT
004800                 MOVE 1           TO DIST-PNTR                    CPPDHDPT
004900                                                                  CPPDHDPT
005000             END-PERFORM                                          CPPDHDPT
005100                                                                  CPPDHDPT
005200         END-IF                                                   CPPDHDPT
005300                                                                  CPPDHDPT
005400     END-PERFORM.                                                 CPPDHDPT
005500                                                                  CPPDHDPT
005600******************************************************************CPPDHDPT
005700**  IF DERIVED WORK DEPARTMENT IS SPACES SUPPLY DEFAULT VALUE   **CPPDHDPT
005800******************************************************************CPPDHDPT
005900                                                                  CPPDHDPT
006000     IF  WRK-HOME-DEPT NOT = SPACES                               CPPDHDPT
007900****     MOVE WRK-HOME-DEPT       TO HOME-DEPT                    28551032
008000         MOVE WRK-HOME-DEPT       TO HOME-DEPT  OF PER-ROW        28551032
006200     ELSE                                                         CPPDHDPT
008200****     MOVE '999999'            TO HOME-DEPT                    28551032
008300         MOVE '999999'            TO HOME-DEPT  OF PER-ROW        28551032
006400     END-IF.                                                      CPPDHDPT
006500                                                                  CPPDHDPT
006600     MOVE E0114                   TO DL-FIELD.                    CPPDHDPT
006700                                                                  CPPDHDPT
006800     PERFORM 9050-AUDITING-RESPONSIBILITIES.                      CPPDHDPT
006900                                                                  CPPDHDPT
007000                                                                  CPPDHDPT
009100*-----------------------------------------------------------------32021138
009200*XXXX-ACCESS-AFI  SECTION.                                        32021138
009300*-----------------------------------------------------------------32021138
009400*****                                                             32021138
009500*****IF  DIST-NUM     (CURR-APPT-PNTR, DIST-PNTR) NOT = ZERO      32021138
009600*****                                                             32021138
009700*****    MOVE DIS-ROW (CURR-APPT-PNTR, DIST-PNTR)                 32021138
009800*****                             TO WORK-DIST-DATA               32021138
009900*****                                                             32021138
010000*****    SET DISTRIBUTION-FOUND   TO TRUE                         32021138
010100*****                                                             32021138
010200*****    IF    WORK-LOC     = XAFI-LOC                            32021138
010300*****      AND WORK-ACCOUNT = XAFI-ACCT                           32021138
010400*****        MOVE XAFI-AR-DEPT-CD TO WRK-HOME-DEPT                32021138
010500*****    ELSE                                                     32021138
010600*****        MOVE WORK-LOC        TO XAFI-LOC                     32021138
010700*****        MOVE WORK-ACCOUNT    TO XAFI-ACCT                    32021138
010800*****        MOVE WORK-FUND       TO XAFI-FUND                    32021138
010900*****        MOVE 'YUU'           TO XAFI-FILE-SELECTION-CRITERIA 32021138
011000*****        MOVE 'READ '         TO XAFI-ACCESS-FUNCTION         32021138
011100*****        PERFORM 9120-CALL-PPINAFP                            32021138
011200*****                                                             32021138
011300*****        IF XAFI-ERROR-CODE = ZERO                            32021138
011400*****            MOVE XAFI-AR-DEPT-CD                             32021138
011500*****                             TO WRK-HOME-DEPT                32021138
011600*****        ELSE                                                 32021138
011700*****           IF XAFI-ERROR-CODE = 60                           32021138
011800*****              SET KRMI-RETURN-WITH-VSAM-ERR TO TRUE          32021138
011900*****           END-IF                                            32021138
012000*****        END-IF                                               32021138
012100*****                                                             32021138
012200*****    END-IF                                                   32021138
012300*****                                                             32021138
012400*****END-IF.                                                      32021138
012410*-----------------------------------------------------------------32021138
012420 XXXX-ACCESS-FAU  SECTION.                                        32021138
012430*-----------------------------------------------------------------32021138
012440                                                                  32021138
012450     IF DIST-NUM     (CURR-APPT-PNTR, DIST-PNTR) NOT = ZERO       32021138
012480        MOVE DIS-ROW (CURR-APPT-PNTR, DIST-PNTR) TO WORK-DIST-DATA32021138
012491        SET DISTRIBUTION-FOUND   TO TRUE                          32021138
012492                                                                  32021138
010200         IF WORK-FULL-ACCT-UNIT = F002-FAU                        32021138
010400            MOVE F002-DEPARTMENT-CODE TO WRK-HOME-DEPT            32021138
010500         ELSE                                                     32021138
010200            MOVE WORK-FULL-ACCT-UNIT TO F002-FAU                  32021138
012493            CALL WS-PGM-PPFAU002 USING PPFAU002-INTERFACE         32021138
010400            MOVE F002-DEPARTMENT-CODE TO WRK-HOME-DEPT            32021138
010200         END-IF                                                   32021138
012515     END-IF.                                                      32021138
012520                                                                  32021138
