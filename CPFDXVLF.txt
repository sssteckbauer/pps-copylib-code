000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPFDXVLF                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*  - CHANGE FROM A FIXED LENGTH TO A VARIABLE LENGHT FILE.   *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
000100*    COPYID=CPFDXVLF                                              CPFDXVLF
000200     BLOCK CONTAINS 0 RECORDS                                     CPFDXVLF
001300     RECORD CONTAINS 83 TO 323 CHARACTERS                         CPFDXVLF
001400     RECORDING MODE IS V                                          CPFDXVLF
000500     LABEL RECORDS ARE STANDARD.                                  CPFDXVLF
