000100**************************************************************/   30871304
000200*  COPYMEMBER: CPLNCTRI                                      */   30871304
000300*  RELEASE: ____1304____  SERVICE REQUEST(S): ____3087____   */   30871304
000400*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __05/24/99__   */   30871304
000500*  DESCRIPTION:                                              */   30871304
000600*                                                            */   30871304
000700*    - New Copymember. Linkage module for the Control        */   30871304
000800*      Table Report Module, PPCTLRPT.                        */   30871304
000900**************************************************************/   30871304
001000*    COPYID=CPLNCTRI                                              CPLNCTRI
001100*01  CTRI-CTL-REPORT-INTERFACE.                                   CPLNCTRI
001200     05  CTRI-ACTION-CODE                  PIC X(01).             CPLNCTRI
001300         88  CTRI-INIT-CALL                  VALUE 'I'.           CPLNCTRI
001400         88  CTRI-PRINT-CALL                 VALUE 'P'.           CPLNCTRI
001500         88  CTRI-BREAK-CALL                 VALUE 'B'.           CPLNCTRI
001600         88  CTRI-FINAL-CALL                 VALUE 'F'.           CPLNCTRI
001700     05  CTRI-SPACING-OVERRIDE-FLAG.                              CPLNCTRI
001800         10  CTRI-SPACING-OVERRIDE         PIC 9(01).             CPLNCTRI
001900     05  CTRI-TRANSACTION                  PIC X(80).             CPLNCTRI
002000     05  CTRI-REPORT-HEADER-DATA           REDEFINES              CPLNCTRI
002100         CTRI-TRANSACTION.                                        CPLNCTRI
002200*--------> These redefinitions only apply to the INIT call!       CPLNCTRI
002300         10  CTRI-PASS-INST                PIC X(64).             CPLNCTRI
002400         10  CTRI-PASS-DATE                PIC X(08).             CPLNCTRI
002500         10  CTRI-PASS-TIME                PIC X(08).             CPLNCTRI
002600     05  CTRI-TRANSACTION-DISPO            PIC X(40).             CPLNCTRI
002700     05  CTRI-TEXT-LINE.                                          CPLNCTRI
002800         10  FILLER                        PIC X(83).             CPLNCTRI
002900         10  CTRI-TEXT-DISPO               PIC X(49).             CPLNCTRI
003000     05  CTRI-MESSAGE-NUMBER               PIC X(05).             CPLNCTRI
003100     05  CTRI-MESSAGE-SEVERITY             PIC 9(02).             CPLNCTRI
003200         88  CTRI-TRANS-REJECTED             VALUES 5 THRU 7.     CPLNCTRI
003300         88  CTRI-RUN-ABORTED                VALUES 8 THRU 99.    CPLNCTRI
