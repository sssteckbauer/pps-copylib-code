000100**************************************************************/   36460773
000200*  COPYMEMBER: CPWSXMSG                                      */   36460773
000300*  RELEASE # ____0773____ SERVICE REQUEST NO(S)___3646_______*/   36460773
000400*  NAME _________JLT___   MODIFICATION DATE ____05/27/93_____*/   36460773
000600*  DESCRIPTION                                               */   36460773
000700*   - ADDED NEW SEVERITIES (BATCH & ONLINE).                 */   36460773
000900**************************************************************/   36460773
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXMSG                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   30620330
000200*  COPYMEMBER: CPWSXMSG                                      */   30620330
000300*  RELEASE # ____0330____ SERVICE REQUEST NO(S)____3062______*/   30620330
000400*  NAME ______JLT______   MODIFICATION DATE ____10/28/87_____*/   30620330
000500*                                                            */   30620330
000600*  DESCRIPTION                                               */   30620330
000700*   - ADD MESSAGE ROUTING CODES.                             */   30620330
000800**************************************************************/   30620330
000900*    COPYID=CPWSXMSG                                              CPWSXMSG
001000*01  MESSAGE-SEGMENT.                                             30930413
001100*-----------------------------------------------------------------CPWSXMSG
001200*             M E S S A G E  T A B L E  R E C O R D               CPWSXMSG
001300*-----------------------------------------------------------------CPWSXMSG
001400     03 MESSAGE-DELETE            PICTURE X.                      CPWSXMSG
001500     03 MESSAGE-KEY.                                              CPWSXMSG
001600        05 MESSAGE-KEY-FILL       PICTURE X(8).                   CPWSXMSG
001700        05  MESSAGE-NO-AREA.                                      CPWSXMSG
001800           07  MESSAGE-PROG-ID    PICTURE XX.                     CPWSXMSG
001900           07  MESSAGE-NUMBER     PICTURE X(3).                   CPWSXMSG
002000     03 MESSAGE-DATA.                                             CPWSXMSG
002100        05 MESSAGE-REFERENCE      PICTURE 9.                      CPWSXMSG
002200        05 MESSAGE-SEVERITY       PICTURE 9.                      CPWSXMSG
002300        05 MESSAGE-TURNAROUND     PICTURE 9.                      CPWSXMSG
002400        05 MESSAGE-TEXT           PICTURE X(65).                  CPWSXMSG
002400        05 MESSAGE-BATCH-SEVERITY PICTURE X(01).                  36460773
002400        05 MESSAGE-ONLINE-SEVERITY PICTURE X(01).                 36460773
002500        05 FILLER                 PICTURE X(129).                 36460773
002500****    05 FILLER                 PICTURE X(131).                 36460773
002600        05 MESSAGE-ROUTING-CDS    PICTURE X(4).                   30620330
002700     03  FILLER                   PICTURE X(7).                   30620330
002800*****03  FILLER                   PICTURE X(142).                 30620330
002900     SKIP1                                                        CPWSXMSG
003000     SKIP1                                                        CPWSXMSG
