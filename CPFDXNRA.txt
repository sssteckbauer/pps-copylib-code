000100**************************************************************/   36020424
000200*  COPYMEMBER: CPFDXNRA                                      */   36020424
000300*  RELEASE # ____0424____ SERVICE REQUEST NO(S)__3602________*/   36020424
000400*  NAME ______DXJ______   MODIFICATION DATE ____06/21/89_____*/   36020424
000500*                                                            */   36020424
000600*  DESCRIPTION                                               */   36020424
000700*  -NEW COPY MEMBER                                          */   36020424
000800*  -CONTAINS NON-RESIDENT-ALIEN (NRA) TAX TREATY REPORT DATA */   36020424
000900**************************************************************/   36020424
001000*    COPYID=CPFDXNRA                                              CPFDXNRA
001100     BLOCK  CONTAINS   0 RECORDS                                  CPFDXNRA
001200     RECORD CONTAINS 120 CHARACTERS                               CPFDXNRA
001300     RECORDING  MODE  IS  F                                       CPFDXNRA
001400     LABEL  RECORDS  ARE  STANDARD.                               CPFDXNRA
001500                                                                  CPFDXNRA
001600 01  NRA-TAX-TREATY-RECORD               PIC X(120).              CPFDXNRA
