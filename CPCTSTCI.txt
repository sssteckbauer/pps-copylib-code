000000**************************************************************/   30871460
000001*  COPYMEMBER: CPCTSTCI                                      */   30871460
000002*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000003*  NAME:_____SRS_________ CREATION DATE:      ___01/22/03__  */   30871460
000004*  DESCRIPTION:                                              */   30871460
000005*  - NEW COPY MEMBER FOR STATE TAX TABLE INPUT (PPPSTC)      */   30871460
000007**************************************************************/   30871460
000008*    COPYID=CPCTSTCI                                              CPCTSTCI
000009*01  STC-TABLE-INPUT.                                             CPCTSTCI
000100     05  STCI-KEY.                                                CPCTSTCI
000110         10 STCI-PERIOD-TYPE             PIC X.                   CPCTSTCI
000110         10 STCI-MARITAL-STATUS          PIC X.                   CPCTSTCI
000230     05  STCI-AMT.                                                CPCTSTCI
000240         10 STCI-AMT-N                   PIC S9(5)V99.            CPCTSTCI
