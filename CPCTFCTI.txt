000000**************************************************************/   30871424
000001*  COPYMEMBER: CPCTFCTI                                      */   30871424
000002*  RELEASE: ___1424______ SERVICE REQUEST(S): _____3087____  */   30871424
000003*  NAME:_____SRS_________ CREATION DATE:      ___07/30/02__  */   30871424
000004*  DESCRIPTION:                                              */   30871424
000005*  - NEW COPY MEMBER FOR FOREIGN COUNTRY TABLE INPUT         */   30871424
000007**************************************************************/   30871424
000008*    COPYID=CPCTFCTI                                              CPCTFCTI
010000*01  FOREIGN-CNTRY-FCT-TABLE-INPUT.                               CPCTFCTI
010100     05 FCTI-COUNTRY-CODE                PIC XX.                  CPCTFCTI
010200     05 FCTI-COUNTRY-NAME                PIC X(30).               CPCTFCTI
