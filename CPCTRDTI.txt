000000**************************************************************/   30871460
000001*  COPYMEMBER: CPCTRDTI                                      */   30871460
000002*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000003*  NAME:_____SRS_________ CREATION DATE:      ___01/22/03__  */   30871460
000004*  DESCRIPTION:                                              */   30871460
000005*  - NEW COPY MEMBER FOR REPORT DESCRIPTION TABLE INPUT      */   30871460
000007**************************************************************/   30871460
000008*    COPYID=CPCTRDTI                                              CPCTRDTI
010000*01  REPORT-DESCRIPTION-TABLE-INPUT.                              CPCTRDTI
010100     05 RDTI-REPORT-ID                   PIC XX.                  CPCTRDTI
010300        88  RDTI-VALID-REPORT-ID         VALUE '01' THRU '98'.    CPCTRDTI
010400     05 RDTI-LINE-NUMBER.                                         CPCTRDTI
010400        10 RDTI-LINE-NUMBER-N            PIC 999.                 CPCTRDTI
010800     05 RDTI-START-POSITION.                                      CPCTRDTI
010800        10 RDTI-START-POSITION-N         PIC 999.                 CPCTRDTI
011200     05 RDTI-FIELDS.                                              CPCTRDTI
011200        10 RDTI-ELEMENT.                                          CPCTRDTI
011200           15 RDTI-ELEMENT-N             PIC 9999.                CPCTRDTI
011300        10 RDTI-TRANS-EDIT.                                       CPCTRDTI
011300           15 RDTI-TRANS-EDIT-N          PIC 99.                  CPCTRDTI
011350        10 RDTI-TRANS-LENGTH.                                     CPCTRDTI
011350           15 RDTI-TRANS-LENGTH-N        PIC 999.                 CPCTRDTI
011600     05 RDTI-DEFAULT                     PIC X(30).               CPCTRDTI
