000100*==========================================================%      UCSD0167
000200*=    COPY MEMBER:CPWSHTMW                                =%      UCSD0167
000300*=    CHANGE # UCSD0167     PROJ. REQUEST: REL1415        =%      UCSD0167
000400*=    NAME: G CHIU          MODIFICATION DATE: 9/23/02    =%      UCSD0167
000500*=                                                        =%      UCSD0167
000600*=    DESCRIPTION                                         =%      UCSD0167
000700*=    FIXED DISPLAY BRACKET PROBLEM.                      =%      UCSD0167
000800*==========================================================%      UCSD0167
000001**************************************************************/   02381440
000002*  COPYMEMBER: CPWSHTMW                                      */   02381440
000003*  RELEASE: ___1440______ SERVICE REQUEST(S): ____80238____  */   02381440
000004*  NAME:_______SBI_______ MODIFICATION DATE:  __11/08/02____ */   02381440
000005*  DESCRIPTION:                                              */   02381440
000006*     WORK AREA FOR PAN HTML FORMATTING.                     */   02381440
000007*                                                            */   02381440
000008**************************************************************/   02381440
000009**************************************************************/   52131415
000010*  COPYMEMBER: CPWSHTMW                                      */   52131415
000011*  RELEASE: ___1415______ SERVICE REQUEST(S): ____15213____  */   52131415
000012*  NAME:_______MLG_______ MODIFICATION DATE:  __06/10/02____ */   52131415
000013*  DESCRIPTION:                                              */   52131415
000014*     WORK AREA FOR PAN HTML FORMATTING.                     */   52131415
000015*                                                            */   52131415
000016**************************************************************/   52131415
000020     05  WS-STRING-VARIABLES.                                     CPWSHTMW
000101         10  WS-STRING                 PIC X(100).                CPWSHTMW
000102         10  WS-STRING-LENGTH          PIC 9(04) COMP.            CPWSHTMW
000110         10  WS-STRING-2               PIC X(100).                CPWSHTMW
000120         10  WS-STRING-2-LENGTH        PIC 9(04) COMP.            CPWSHTMW
000300         10  WS-STRING-OUT             PIC X(300).                CPWSHTMW
000400         10  WS-STRING-OUT-LENGTH      PIC 9(04) COMP.            CPWSHTMW
000500         10  WS-STRING-SUB             PIC 9(04) COMP.            CPWSHTMW
003574     05  HTML-CONSTANTS.                                          CPWSHTMW
003575         10  HTML-TABLE-HEAD.                                     CPWSHTMW
003576             15  FILLER                 PIC X(15) VALUE           CPWSHTMW
003577            '<TABLE BORDER="'.                                    CPWSHTMW
003578             15  HTML-TABLE-BORDER      PIC X(01) VALUE '0'.      CPWSHTMW
003579             15  FILLER                 PIC X(47) VALUE           CPWSHTMW
003580            '" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">'.    CPWSHTMW
003575         10  HTML-TABLE-HEAD-NO-WIDTH.                            02381440
003576             15  FILLER                 PIC X(15) VALUE           02381440
003577            '<TABLE BORDER="'.                                    02381440
003578             15  HTML-TABLE-BORDER      PIC X(01) VALUE '0'.      02381440
003579             15  FILLER                 PIC X(47) VALUE           02381440
003580            '" CELLPADDING="0" CELLSPACING="0">'.                 02381440
003581         10  HTML-TABLE-HEAD-DIST.                                CPWSHTMW
003582             15  FILLER                 PIC X(15) VALUE           CPWSHTMW
003583            '<TABLE BORDER="'.                                    CPWSHTMW
003584             15  HTML-TABLE-BORDER      PIC X(01) VALUE '0'.      CPWSHTMW
003585             15  FILLER                 PIC X(46) VALUE           CPWSHTMW
003586            '" CELLPADDING="2" CELLSPACING="2" WIDTH="80%">'.     CPWSHTMW
003587         10  HTML-TABLE-END             PIC X(08) VALUE           CPWSHTMW
003588            '</TABLE>'.                                           CPWSHTMW
003589         10  HTML-FONT-LABEL            PIC X(50) VALUE           CPWSHTMW
003590            '<FONT SIZE="1" FACE="Arial" COLOR="BLACK"><STRONG>'. CPWSHTMW
003591         10  HTML-FONT-DATA-NORMAL      PIC X(44) VALUE           CPWSHTMW
003592            '<FONT SIZE="1" COLOR="#0000A0" FACE="Arial">'.       CPWSHTMW
003593         10  HTML-FONT-DATA-PRIOR       PIC X(44) VALUE           CPWSHTMW
003594            '<FONT SIZE="1" COLOR="#0000A0" FACE="Arial">'.       CPWSHTMW
003595         10  HTML-FONT-DATA-CHANGED     PIC X(52) VALUE           CPWSHTMW
003596          '<FONT SIZE="1" COLOR="#FF8000" FACE="Arial"><STRONG>'. CPWSHTMW
003597         10  HTML-FONT-SECTION-HEAD     PIC X(50) VALUE           CPWSHTMW
003598          '<FONT SIZE="2" FACE="Arial" COLOR="BLACK"><STRONG>'.   CPWSHTMW
003600         10  HTML-FONT-END              PIC X(16) VALUE           CPWSHTMW
003601            '</STRONG></FONT>'.                                   CPWSHTMW
003602         10  HTML-ROW                   PIC X(21) VALUE           CPWSHTMW
003603            '<TR VALIGN="BOTTOM"> '.                              CPWSHTMW
003604         10  HTML-CELL                  PIC X(27) VALUE           CPWSHTMW
003605            '<TD VALIGN="BOTTOM" NOWRAP>'.                        CPWSHTMW
003606         10  HTML-BLANK-CELL            PIC X(37) VALUE           CPWSHTMW
003607            '<TD><FONT SIZE="1">&nbsp;</FONT></TD>'.              CPWSHTMW
003608         10  HTML-CELL-END              PIC X(05) VALUE '</TD>'.  CPWSHTMW
003609         10  HTML-SPACE                 PIC X(06) VALUE '&nbsp;'. CPWSHTMW
003610         10  HTML-HYPHEN                PIC X(01) VALUE '-'.      CPWSHTMW
005300*****    10  HTML-LEFT-BRACKET          PIC X(06) VALUE '&lt&lt'. UCSD0167
005400*****    10  HTML-RIGHT-BRACKET         PIC X(06) VALUE '&gt&gt'. UCSD0167
003611         10  HTML-LEFT-BRACKET          PIC X(08) VALUE           UCSD0167
005600                                              '&lt;&lt;'.         UCSD0167
003612         10  HTML-RIGHT-BRACKET         PIC X(08) VALUE           UCSD0167
005800                                              '&gt;&gt;'.         UCSD0167
003613         10  HTML-LEFT-ALLIGN-LABEL     PIC X(35)                 02381440
003614               VALUE '<TD ALIGN="LEFT" CLASS="ROWDATA" >'.        02381440
003615         10  HTML-LEFT-ALLIGN-DATA      PIC X(32)                 02381440
003616               VALUE '<TD ALIGN="LEFT" CLASS="DATA" >'.           02381440
003617                                                                  CPWSHTMW
003618     05  HTML-VARIABLES.                                          CPWSHTMW
003620         10  FILLER.                                              CPWSHTMW
003621             15  HTML-CELL-BEGIN OCCURS 15.                       CPWSHTMW
003622                 20  FILLER                 PIC X(34) VALUE       CPWSHTMW
003623                     '<TD VALIGN="BOTTOM" NOWRAP WIDTH="'.        CPWSHTMW
003624                 20  HTML-COLUMN-WIDTH      PIC X(02).            CPWSHTMW
003625                 20  FILLER                 PIC X(02) VALUE '%"'. CPWSHTMW
003626                 20  FILLER                 PIC X(10) VALUE       CPWSHTMW
003627                     ' COLSPAN="'.                                CPWSHTMW
003628                 20  HTML-COLUMNS-SPANNED   PIC X(02).            CPWSHTMW
003629                 20  FILLER                 PIC X(01) VALUE '"'.  CPWSHTMW
003632                 20  HTML-STYLE             PIC X(30).            CPWSHTMW
003633                     88  HTML-STYLE-LABEL       VALUE             CPWSHTMW
003634                         ' CLASS="DETAILLABEL"          '.        CPWSHTMW
003635                     88  HTML-STYLE-DATA-MOD    VALUE             CPWSHTMW
003636                         ' CLASS="DETAILMODDATA"        '.        CPWSHTMW
003637                     88  HTML-STYLE-DATA-NORMAL  VALUE            CPWSHTMW
003638                         ' CLASS="DETAILNORMDATA"       '.        CPWSHTMW
003639                     88  HTML-STYLE-DATA-PRIOR    VALUE           CPWSHTMW
003640                         ' CLASS="DETAILPRIORMDATA"     '.        CPWSHTMW
003641                     88  HTML-STYLE-SECTION-HEAD  VALUE           CPWSHTMW
003642                         ' CLASS="DETAILSECTIONHEAD"    '.        CPWSHTMW
003643                     88  HTML-STYLE-DETAIL-FLAG   VALUE           CPWSHTMW
003644                         ' CLASS="DETAILFLAG"           '.        CPWSHTMW
003645                     88  HTML-STYLE-COLUMN-HEAD   VALUE           CPWSHTMW
003646                         ' CLASS="DETAILCOLUMNHEAD"     '.        CPWSHTMW
003647                     88  HTML-STYLE-APPTDIST-HEAD VALUE           CPWSHTMW
003648                         ' CLASS="DETAILAPPTDISTHEAD"   '.        CPWSHTMW
003649                 20  FILLER                 PIC X(01) VALUE '>'.  CPWSHTMW
003650         10  HTML-COLUMN-NUMBER          PIC 9(02).               CPWSHTMW
003651         10  HTML-COLSPAN                PIC 9(02).               CPWSHTMW
003652         10  HTML-SUBTYPE                PIC 9(01).               CPWSHTMW
003653             88  HTML-SUBTYPE-GINF           VALUE 1.             CPWSHTMW
003654             88  HTML-SUBTYPE-PIEC           VALUE 2.             CPWSHTMW
003655             88  HTML-SUBTYPE-INDL           VALUE 3.             CPWSHTMW
003656             88  HTML-SUBTYPE-WSPC           VALUE 4.             CPWSHTMW
003657             88  HTML-SUBTYPE-SABB           VALUE 5.             CPWSHTMW
003658             88  HTML-SUBTYPE-LAYO           VALUE 6.             CPWSHTMW
003659             88  HTML-SUBTYPE-BENE           VALUE 7.             CPWSHTMW
003660             88  HTML-SUBTYPE-APPT           VALUE 8.             CPWSHTMW
003661     05  HTML-WORK-AREA.                                          CPWSHTMW
003662          10  HTML-FONT-STRING            PIC X(52).              CPWSHTMW
003663          10  HTML-FONT-END-STRING        PIC X(16).              CPWSHTMW
003664          10  HTML-LABEL                  PIC X(75).              CPWSHTMW
003665          10  HTML-CURRENT-DATA           PIC X(100).             CPWSHTMW
003666          10  HTML-PRIOR-DATA             PIC X(100).             CPWSHTMW
003667          10  HTML-CURRENT-TRANSLATION    PIC X(100).             CPWSHTMW
003668          10  HTML-PRIOR-TRANSLATION      PIC X(100).             CPWSHTMW
003669          10  HTML-TD-TEXT                PIC X(100).             CPWSHTMW
003670          10  HTML-DATA-TYPE              PIC X(01).              CPWSHTMW
003671              88  HTML-DATA-CHARACTER         VALUE 'C'.          CPWSHTMW
003672              88  HTML-DATA-DATE              VALUE 'D'.          CPWSHTMW
003673              88  HTML-DATA-NUMERIC           VALUE 'N'.          CPWSHTMW
003674          10  HTML-DATA-TRANSLATED-SW     PIC X(01).              CPWSHTMW
003675              88  HTML-DATA-TRANSLATED        VALUE 'Y'.          CPWSHTMW
003676          10  HTML-NEW-ROW-SW             PIC X(01).              CPWSHTMW
003677              88  HTML-NEW-ROW                VALUE 'Y'.          CPWSHTMW
003678          10  HTML-NEW-CELL-FOR-PRIOR-SW  PIC X(01) VALUE 'N'.    CPWSHTMW
003679              88  HTML-NEW-CELL-FOR-PRIOR     VALUE 'Y'.          CPWSHTMW
003680          10  HTML-CURRENT-DATA-ONLY-SW   PIC X(01) VALUE 'N'.    CPWSHTMW
003681              88  HTML-CURRENT-DATA-ONLY      VALUE 'Y'.          CPWSHTMW
003682          10  HTML-PRIOR-DATA-ONLY-SW     PIC X(01) VALUE 'N'.    CPWSHTMW
003683              88  HTML-PRIOR-DATA-ONLY        VALUE 'Y'.          CPWSHTMW
003684          10  HTML-BYPASS-LABEL-SW        PIC X(01) VALUE 'N'.    CPWSHTMW
003685              88  HTML-BYPASS-LABEL           VALUE 'Y'.          CPWSHTMW
003686          10  HTML-NO-BRACKETS-SW         PIC X(01) VALUE 'N'.    CPWSHTMW
003687              88  HTML-NO-BRACKETS            VALUE 'Y'.          CPWSHTMW
003690                                                                  CPWSHTMW
