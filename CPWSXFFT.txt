000100**************************************************************/   45420485
000200*  COPYMEMBER: CPWSXFFT                                      */   45420485
000300*  RELEASE # ____0485____ SERVICE REQUEST NO(S)___4542_______*/   45420485
000400*  NAME __M. BAPTISTA__   MODIFICATION DATE ____06/06/90_____*/   45420485
000500*  DESCRIPTION                                               */   45420485
000600*   - ADDED NEW                                              */   45420485
000700**************************************************************/   45420485
000800*01  FUND-GROUPINGS.                                              CPWSXFFT
000900     05  FUND-NO-OF-ENTRIES      PIC 9(02)  VALUE 40.             CPWSXFFT
001000     05  FUND-GROUPS.                                             CPWSXFFT
001100         10  FILLER              PIC X(11)  VALUE 'K0410009599'.  CPWSXFFT
001200         10  FILLER              PIC X(11)  VALUE 'K1990019999'.  CPWSXFFT
001300         10  FILLER              PIC X(11)  VALUE 'K2000020399'.  CPWSXFFT
001400         10  FILLER              PIC X(11)  VALUE 'K2040020499'.  CPWSXFFT
001500         10  FILLER              PIC X(11)  VALUE 'K2050120599'.  CPWSXFFT
001600         10  FILLER              PIC X(11)  VALUE 'K2060020939'.  CPWSXFFT
001700         10  FILLER              PIC X(11)  VALUE 'K2094020999'.  CPWSXFFT
001800         10  FILLER              PIC X(11)  VALUE 'K2100021099'.  CPWSXFFT
001900         10  FILLER              PIC X(11)  VALUE 'K2110024999'.  CPWSXFFT
002000         10  FILLER              PIC X(11)  VALUE 'K2500028999'.  CPWSXFFT
002100         10  FILLER              PIC X(11)  VALUE 'K2900033999'.  CPWSXFFT
002200         10  FILLER              PIC X(11)  VALUE 'K3410039599'.  CPWSXFFT
002300         10  FILLER              PIC X(11)  VALUE 'K3960039999'.  CPWSXFFT
002400         10  FILLER              PIC X(11)  VALUE 'K4000059999'.  CPWSXFFT
002500         10  FILLER              PIC X(11)  VALUE 'K6000062999'.  CPWSXFFT
002600         10  FILLER              PIC X(11)  VALUE 'K6300063999'.  CPWSXFFT
002700         10  FILLER              PIC X(11)  VALUE 'K6400065999'.  CPWSXFFT
002800         10  FILLER              PIC X(11)  VALUE 'K6600069999'.  CPWSXFFT
002900         10  FILLER              PIC X(11)  VALUE 'K7000074999'.  CPWSXFFT
003000         10  FILLER              PIC X(11)  VALUE 'K7500075999'.  CPWSXFFT
003100         10  FILLER              PIC X(11)  VALUE '20410009599'.  CPWSXFFT
003200         10  FILLER              PIC X(11)  VALUE '21990019999'.  CPWSXFFT
003300         10  FILLER              PIC X(11)  VALUE '22000020399'.  CPWSXFFT
003400         10  FILLER              PIC X(11)  VALUE '22040020499'.  CPWSXFFT
003500         10  FILLER              PIC X(11)  VALUE '22050120599'.  CPWSXFFT
003600         10  FILLER              PIC X(11)  VALUE '22060020939'.  CPWSXFFT
003700         10  FILLER              PIC X(11)  VALUE '22094020999'.  CPWSXFFT
003800         10  FILLER              PIC X(11)  VALUE '22100021099'.  CPWSXFFT
003900         10  FILLER              PIC X(11)  VALUE '22110024999'.  CPWSXFFT
004000         10  FILLER              PIC X(11)  VALUE '22500028999'.  CPWSXFFT
004100         10  FILLER              PIC X(11)  VALUE '22900033999'.  CPWSXFFT
004200         10  FILLER              PIC X(11)  VALUE '23410039599'.  CPWSXFFT
004300         10  FILLER              PIC X(11)  VALUE '23960039999'.  CPWSXFFT
004400         10  FILLER              PIC X(11)  VALUE '24000059999'.  CPWSXFFT
004500         10  FILLER              PIC X(11)  VALUE '26000062999'.  CPWSXFFT
004600         10  FILLER              PIC X(11)  VALUE '26300063999'.  CPWSXFFT
004700         10  FILLER              PIC X(11)  VALUE '26400065999'.  CPWSXFFT
004800         10  FILLER              PIC X(11)  VALUE '26600069999'.  CPWSXFFT
004900         10  FILLER              PIC X(11)  VALUE '27000074999'.  CPWSXFFT
005000         10  FILLER              PIC X(11)  VALUE '27500075999'.  CPWSXFFT
005100     05  FILLER                  REDEFINES                        CPWSXFFT
005200         FUND-GROUPS.                                             CPWSXFFT
005300         10  FILLER              OCCURS  40 TIMES.                CPWSXFFT
005400             15  FUND-GRP-LOC    PIC X(01).                       CPWSXFFT
005500             15  FUND-GRP-BEG    PIC 9(05).                       CPWSXFFT
005600             15  FUND-GRP-END    PIC 9(05).                       CPWSXFFT
005700*                                                                 CPWSXFFT
