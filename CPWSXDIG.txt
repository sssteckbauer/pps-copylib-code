000100**************************************************************/   30930413
000200*  COPY MODULE:  CPWSXDIG                                    */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000300*    COPYID=CPWSXDIG                                              CPWSXDIG
000200*01  ELEM-NO-WITH-CHK-DIG.                                        30930413
000300     05  ELEM-NO                 PIC 9(4).                        CPWSXDIG
000400     05  FILLER      REDEFINES ELEM-NO.                           CPWSXDIG
000500         10  SEG-NO-2            PIC 99.                          CPWSXDIG
000600         10  ELEM-NO-2           PIC 99.                          CPWSXDIG
000700     05  FILLER      REDEFINES ELEM-NO.                           CPWSXDIG
000800         10  SEG-NO-1            PIC 9.                           CPWSXDIG
000900         10  ELEM-NO-3           PIC 999.                         CPWSXDIG
001000     05  ELEM-DIGITS REDEFINES ELEM-NO.                           CPWSXDIG
001100         10  DIG1                PIC 9.                           CPWSXDIG
001200         10  DIG2                PIC 9.                           CPWSXDIG
001300         10  DIG3                PIC 9.                           CPWSXDIG
001400         10  DIG4                PIC 9.                           CPWSXDIG
001500     05  CKDIG                   PIC 9.                           CPWSXDIG
001600     05  CALC-CHECK-DIGIT        PIC 9.                           CPWSXDIG
