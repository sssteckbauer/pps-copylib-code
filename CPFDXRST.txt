000100**************************************************************/   13190159
000200*  COPY MODULE:  CPFDXRST                                    */   13190159
000300*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
000400*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
000500*  DESCRIPTION                                               */   13190159
000600*  FOR PHASE 2 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190159
000700*  USER-CODED TRANSACTION INPUT RECORD LENGTHS WILL BE       */   13190159
000800*  EXPANDED BY 22 BYTES, AND RECORDS AND WORK AREAS          */   13190159
000900*  RELATING TO THE EDB FILE WILL BE EXPANDED AS WELL.        */   13190159
001000*  FOR THIS COPY MEMBER, EXPAND THE LENGTH OF THE EDB        */   13190159
001100*  RESTORE RECORD FROM 224 TO 290 BYTES.                     */   13190159
001200**************************************************************/   13190159
001300     SKIP2                                                        13190159
001400**************************************************************/   13190153
001500*  COPY MODULE:  CPFDXRST                                    */   13190153
001600*  RELEASE # ___0153___   SERVICE REQUEST NO(S)____1319______*/   13190153
001700*  NAME _______________   MODIFICATION DATE ____07/09/85_____*/   13190153
001800*  DESCRIPTION                                               */   13190153
001900*  IN THE PROCESS OF SEPARATING THE CONTROL TABLES FROM      */   13190153
002000*  THE EDB FILE, CREATE A SEPARATE FILE DESCRIPTION FOR      */   13190153
002100*  THE CTL RESTORE FILE AND CHANGE THIS FD TO REFLECT        */   13190153
002200*  THE EDB RESTORE FILE.                                     */   13190153
002300**************************************************************/   13190153
002400     SKIP2                                                        13190153
002500*    COPYID=CPFDXRST                                              CPFDXRST
002600*    RECORD CONTAINS 224 CHARACTERS                               13190159
002700     RECORD CONTAINS 290 CHARACTERS                               13190159
002800     BLOCK CONTAINS 0 RECORDS                                     CPFDXRST
002900     RECORDING MODE IS F                                          CPFDXRST
003000     LABEL RECORDS ARE STANDARD                                   CPFDXRST
003100*    DATA RECORD IS RESTORE-RECORD.                               13190153
003200     DATA RECORD IS EDB-RESTORE-RECORD.                           13190153
003300     SKIP1                                                        CPFDXRST
003400*01  RESTORE-RECORD.                                              13190153
003500*    05  RESTORE-DELETE      PIC X.                               13190153
003600*    05  RESTORE-KEY.                                             13190153
003700*        10  RESTORE-IDNO    PIC X(9).                            13190153
003800*        10  RESTORE-SEGMNT  PIC X(4).                            13190153
003900*    05  RESTORE-DATA        PIC X(210).                          13190153
004000 01  EDB-RESTORE-RECORD.                                          13190153
004100     05  EDB-RESTORE-DELETE      PIC X.                           13190153
004200     05  EDB-RESTORE-KEY.                                         13190153
004300         10  EDB-RESTORE-IDNO    PIC X(9).                        13190153
004400         10  EDB-RESTORE-SEGMNT  PIC X(4).                        13190153
004500*    05  EDB-RESTORE-DATA        PIC X(210).                      13190159
004600     05  EDB-RESTORE-DATA        PIC X(276).                      13190159
