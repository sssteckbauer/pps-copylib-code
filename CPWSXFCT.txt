000100**************************************************************/   37270493
000200*  COPYMEMBER: CPWSXFCT                                      */   37270493
000300*  RELEASE: ____0493____  SERVICE REQUEST(S): ____3727____   */   37270493
000400*  NAME:    __J.WILCOX__  CREATION DATE:      __07/09/90__   */   37270493
000500*  DESCRIPTION:                                              */   37270493
000600*                                                            */   37270493
000700*    - NEW COPYMEMBER PROVIDING RECORD LAYOUT FOR 'FOREIGN   */   37270493
000800*      COUNTRY' TABLE ADDED WITH THIS RELEASE.               */   37270493
000900**************************************************************/   37270493
001000*    COPYID=CPWSXFCT                                              CPWSXFCT
001100*01  XFCT-FOREIGN-COUNTRY-RECORD.                                 CPWSXFCT
001200*-----------------------------------------------------------------CPWSXFCT
001300*     F O R E I G N   C O U N T R Y   T A B L E   R E C O R D     CPWSXFCT
001400*-----------------------------------------------------------------CPWSXFCT
001500     05 XFCT-DELETE                  PIC X(01).                   CPWSXFCT
001600     05 XFCT-KEY.                                                 CPWSXFCT
001700         10  XFCT-KEY-CONSTANT       PIC X(04).                   CPWSXFCT
001800         10  XFCT-KEY-COUNTRY        PIC X(02).                   CPWSXFCT
001900         10  FILLER                  PIC X(07).                   CPWSXFCT
002000     05  XFCT-COUNTRY-NAME           PIC X(30).                   CPWSXFCT
002100     05  FILLER                      PIC X(173).                  CPWSXFCT
002200     05  XFCT-LAST-ACTION            PIC X(01).                   CPWSXFCT
002300     05  XFCT-LAST-UPDATE            PIC X(06).                   CPWSXFCT
