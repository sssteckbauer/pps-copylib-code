000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSLATI                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*                                                            */   32021138
000006*  IMPLEMENTATION OF THE FLEXIBLE FULL ACCOUNTING UNIT (3202)*/   32021138
000007*                                                            */   32021138
000008**************************************************************/   32021138
000000**************************************************************/   28521087
000001*  COPYMEMBER: CPWSLATI                                      */   28521087
000002*  RELEASE: ___1087______ SERVICE REQUEST(S): ____12852____  */   28521087
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___07/15/96__  */   28521087
000004*  DESCRIPTION:                                              */   28521087
000005*  ** DATE CONVERSION II **                                  */   28521087
000006*  -  INCLUDE CENTURY IN THE DATE STRUCTURE.                 */   28521087
000008**************************************************************/   28521087
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSLATI                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   30860356
000200*  COPYMEMBER: CPWSLATI                                      */   30860356
000300*  RELEASE # ____0356____ SERVICE REQUEST NO(S)____3086______*/   30860356
000400*  NAME ______JLT______   MODIFICATION DATE ____05/28/88_____*/   30860356
000500*                                                            */   30860356
000600*  DESCRIPTION                                               */   30860356
000700*   INITIAL INSTALLATION OF COPY MEMBER.                     */   30860356
000800**************************************************************/   30860356
000900*COPYID=CPWSLATI                                                  CPWSLATI
001000******************************************************************CPWSLATI
001100*                        C P W S L A T I                         *CPWSLATI
001200******************************************************************CPWSLATI
001300*                                                                *CPWSLATI
001400*  LEAVE ACCRUAL TABLE INTERFACE:                                *CPWSLATI
001500*          USED FOR LAT TABLE UTILITY MODULE:  PPLATUTL          *CPWSLATI
001600*                                                                *CPWSLATI
001700******************************************************************CPWSLATI
001800*01  PPLATUTL-INTERFACE.                                          30930413
001900*                                                                 CPWSLATI
002000     05  LATI-TABLE-HAS-BEEN-LOADED     PIC X VALUE 'N'.          CPWSLATI
002100*                                                                 CPWSLATI
002200     05  LATI-IN-SEARCH-KEY.                                      CPWSLATI
002300         07  LATI-SRCH-TITLE-TYPE   PIC X(01).                    CPWSLATI
002400         07  LATI-SRCH-TUC          PIC X(02).                    CPWSLATI
002500         07  LATI-SRCH-AREP         PIC X(01).                    CPWSLATI
002600         07  LATI-SRCH-SHC          PIC X(01).                    CPWSLATI
002700         07  LATI-SRCH-DUC          PIC X(01).                    CPWSLATI
002710*********07  LATI-SRCH-DATE-YYMMDD  PIC 9(6).                     28521087
002720*********07  LATI-SRCH-DATE-RDF      REDEFINES                    28521087
002730*********    LATI-SRCH-DATE-YYMMDD.                               28521087
002740*********    09  LATI-SRCH-DATE-YYMM    PIC 9(4).                 28521087
002750*********    09  FILLER                 PIC X(2).                 28521087
003300*********                                                         28521087
002800         07  LATI-SRCH-DATE-CCYYMMDD  PIC 9(8).                   28521087
002900         07  LATI-SRCH-DATE-RDF      REDEFINES                    28521087
003000             LATI-SRCH-DATE-CCYYMMDD.                             28521087
003100             09  LATI-SRCH-DATE-CCYYMM    PIC 9(6).               28521087
003110             09  FILLER   REDEFINES                               28521087
003111                 LATI-SRCH-DATE-CCYYMM.                           28521087
003120                 11  LATI-SRCH-DATE-CCYY  PIC 9(04).              28521087
003130                 11  LATI-SRCH-DATE-MM    PIC 9(02).              28521087
003200             09  LATI-SRCH-DATE-DD      PIC 9(02).                28521087
003312         07  LATI-SRCH-RECORD-TYPE    PIC X(01).                  32021138
003300*                                                                 28521087
003400*        THE DAYS ARE STRIPPED OFF THE ABOVE ENTERED DATE         CPWSLATI
003500*        BECAUSE ONLY THE YEAR AND MONTH ARE APPLICABLE TO        CPWSLATI
003600*        THE LAT KEY.                                             CPWSLATI
003610******** 07  LATI-SRCH-EFF-DATE     PIC S9(5)     COMP-3.         28521087
003620********      IN 0YYMM FORMAT                                     28521087
003700         07  LATI-SRCH-EFF-DATE     PIC S9(7)     COMP-3.         28521087
003800*             IN 0CCYYMM FORMAT                                   28521087
003900*                                                                 CPWSLATI
004000     05  LATI-ERROR-COND          PIC X  VALUE ZERO.              CPWSLATI
004100         88  LATI-OK                   VALUE ZERO.                CPWSLATI
004200*********88  LATI-ERR-NO-DELIM         VALUE '1'.                 28521087
004210*********88  LATI-ERR-I-O-ERROR        VALUE '2'.                 28521087
004300         88  LATI-ERR-DB2-ERROR        VALUE '2'.                 28521087
004400         88  LATI-ERR-LAT-OVRFLO       VALUE '3'.                 CPWSLATI
004500         88  LATI-ERR-EMPTY-FILE       VALUE '4'.                 CPWSLATI
004600         88  LATI-ERR-NOT-FOUND        VALUE '5'.                 CPWSLATI
004700*                                                                 CPWSLATI
004800*                                                                 CPWSLATI
004900     05  LATI-DEFALUT-LEVELS.                                     CPWSLATI
005000         07  LATI-1ST-DEFAULT     PIC S9(4) COMP SYNC VALUE ZERO. CPWSLATI
005100         07  LATI-2ND-DEFAULT     PIC S9(4) COMP SYNC VALUE ZERO. CPWSLATI
005200         07  LATI-3RD-DEFAULT     PIC S9(4) COMP SYNC VALUE ZERO. CPWSLATI
005300         07  LATI-4TH-DEFAULT     PIC S9(4) COMP SYNC VALUE ZERO. CPWSLATI
005400         07  LATI-5TH-DEFAULT     PIC S9(4) COMP SYNC VALUE ZERO. CPWSLATI
005500*                                                                 CPWSLATI
005600     05  LATI-FUND-INDX           PIC S9(4) COMP SYNC VALUE ZERO. CPWSLATI
005700     05  LATI-FUND-INDX-MAX       PIC S9(4) COMP SYNC             CPWSLATI
005800                                             VALUE +6.            CPWSLATI
005900*                                                                 CPWSLATI
006000     05  LATI-INDX                PIC S9(4) COMP SYNC VALUE ZERO. CPWSLATI
006100     05  LATI-INDX-MAX            PIC S9(4) COMP SYNC             CPWSLATI
006200                                           VALUE +200.            CPWSLATI
006300     05  LATI-LAT-TABLE.                                          CPWSLATI
006400         07  LATI-LAT-RECORD              OCCURS  200.            CPWSLATI
006500*************09  LATI-DELETE                PIC X.                28521087
006600             09  LATI-KEY.                                        CPWSLATI
006700*****************11  LATI-KEY-CONSTANT      PIC X(04).            28521087
006800                 11  LATI-TITLE-TYPE        PIC X(01).            CPWSLATI
006900                 11  LATI-TITLE-UNIT-CD     PIC X(02).            CPWSLATI
007000                 11  LATI-AREP-CD           PIC X(01).            CPWSLATI
007100                 11  LATI-SPEC-HAND-CD      PIC X(01).            CPWSLATI
007200                 11  LATI-DISTR-UNIT-CD     PIC X(01).            CPWSLATI
007300*****************11  LATI-EFF-DATE          PIC S9(5)     COMP-3. 28521087
007400*****************    DATE: FIVE NUMERIC POSITIONS: SYYMM,         28521087
007410                 11  LATI-EFF-DATE-CCYYMM   PIC S9(7)     COMP-3. 28521087
007420*                    DATE: FIVE NUMERIC POSITIONS: SCCYYMM,       28521087
007500*                          WHERE "S" IS UNUSED (SLACK).           CPWSLATI
007600             09  LATI-KEY-RDF  REDEFINES                          CPWSLATI
007700                 LATI-KEY.                                        CPWSLATI
007800*****************11  FILLER                 PIC X(04).            28521087
007900*****************11  LATI-SHORT-KEY         PIC X(09).            28521087
007910                 11  LATI-SHORT-KEY         PIC X(10).            28521087
008000     SKIP1                                                        CPWSLATI
008100             09  LATI-PROCESS-DATA.                               CPWSLATI
008200                 11  LATI-THRESH-PERCNT     PIC S9V9999   COMP-3. CPWSLATI
008300                 11  LATI-MAX-HRS-OVERIDE.                        CPWSLATI
008400                     13  LATI-MHO-VAC       PIC S9(5)V99  COMP-3. CPWSLATI
008500                     13  LATI-MHO-SKL       PIC S9(5)V99  COMP-3. CPWSLATI
008600                     13  LATI-MHO-PTO       PIC S9(5)V99  COMP-3. CPWSLATI
008700                 11  LATI-HRS-PER-DAY       PIC S9(3)V99  COMP-3. CPWSLATI
008800                 11  LATI-PRE-CALC-CD       PIC 9(02).            CPWSLATI
008900                 11  LATI-RT-SCHED-NO       PIC 9(3).             CPWSLATI
009000                 11  LATI-POST-CALC-CD      PIC 9(02).            CPWSLATI
009100                 11  FILLER                 PIC X(18).            32021138
009100                 11  LATI-FUND-RANGE-DATA  OCCURS 6 TIMES.        CPWSLATI
009200****                 13  LATI-FR-LO         PIC 9(5).             32021138
009300****                 13  LATI-FR-HI         PIC 9(5).             32021138
009310                     13  LATI-FND-GRP-CD    PIC X(04).            32021138
009400                     13  LATI-UTIL-FACTORS.                       CPWSLATI
009500                         15  LATI-UF-VAC    PIC S9V9(4)  COMP-3.  CPWSLATI
009600                         15  LATI-UF-SKL    PIC S9V9(4)  COMP-3.  CPWSLATI
009700                         15  LATI-UF-PTO    PIC S9V9(4)  COMP-3.  CPWSLATI
009800****                 13  LATI-LV-RES-ACCT   PIC 9(6).             32021138
009810                     13  LATI-LV-RES-FAU    PIC X(30).            32021138
009900*********  WARNING: THE REMAINDER OF THE LAT RECORD (I.E., 28     32021138
010000*********           BYTES FILLER AND 7 BYTES LAST UPDT INFO) IS   32021138
010100*********           TRUNCATED.                                    32021138
010110*          WARNING: BECAUSE OF THE IMPLEMENTATION OF THE GENERIC  32021138
010120*                   FULL ACCOUNTING UNIT THE LEAVE LIABILITY      32021138
010130*                   ACCOUNT HAS BEEN OBSOLETED IN FAVOR OF THE    32021138
010140*                   LEAVE LIABILITY FAU.  THIS REQUIRED THAT THE  32021138
010150*                   VSAM VERSION OF THE LAT BECOME S MULTIPLE     32021138
010160*                   RECORD TABLE.  HENCE, THE CPWSLATI STRUCTURE  32021138
010170*                   NO LONGER CORRESPONDS TO THE VSAM TABLE       32021138
010180*                   STRUCTURE.                                    32021138
010200                                                                  CPWSLATI
010300*  *  *  *  *                                                     CPWSLATI
