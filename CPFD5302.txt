000100**************************************************************/   32021138
000200*  COPYMEMBER: CPFD5302                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ___13202_____  */   32021138
000400*  NAME: ______JLT_______ MODIFICATION DATE:  __08/04/97____ */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*    - MODIFIED FOR GENERIC FAU PROCESSING.                  */   32021138
000700**************************************************************/   32021138
000100**************************************************************/   36500900
000200*  COPYMEMBER: CPFD5302                                      */   36500900
000300*  RELEASE: ___0900______ SERVICE REQUEST(S): ____3650_____  */   36500900
000400*  NAME: ___ WRW (BIT)___ MODIFICATION DATE:  __05/20/94____ */   36500900
000500*  DESCRIPTION:                                              */   36500900
000600*    - ADDED COST CENTER AND PROJECT CODE.                   */   36500900
000700**************************************************************/   36500900
000000**************************************************************/   12690838
000001*  COPYMEMBER: CPFD5302                                      */   12690838
000002*  RELEASE: ___0838______ SERVICE REQUEST(S): _____11269___  */   12690838
000003*  NAME:_____J. QUAN_____ CREATION DATE:      ___11/30/93__  */   12690838
000004*  DESCRIPTION:                                              */   12690838
000005*  -  INITIAL IMPLEMENTATION OF COPYMEMBER                   */   12690838
000006*     FILE DESCRIPTION OF PPP5302 PRINT IMAGE FILE.          */   12690838
000007**************************************************************/   12690838
000800*    COPYID=CPFD5302                                              CPFD5302
000900     BLOCK CONTAINS 0 RECORDS                                     CPFD5302
002500*                                                              CD 32021138
002600**** RECORD CONTAINS 177 CHARACTERS                               32021138
002700     RECORD CONTAINS 185 CHARACTERS                               32021138
001100     RECORDING MODE IS F                                          CPFD5302
001200     LABEL RECORDS ARE STANDARD.                                  CPFD5302
001300*                                                                 CPFD5302
003100*                                                              CD 32021138
003200*01**P5302-IMAGE-RECORD          PIC X(177).                      32021138
003300 01  P5302-IMAGE-RECORD          PIC X(185).                      32021138
001400*                                                                 CPFD5302
