000100**************************************************************/   34221114
000200*  COPYMEMBER: CPWSIHDB                                      */   34221114
000300*  RELEASE: ___1114______ SERVICE REQUEST(S): _____3422____  */   34221114
000400*  NAME:______M SANO_____ MODIFICATION DATE:  ___01/16/97__  */   34221114
000500*  DESCRIPTION:                                              */   34221114
000600*     INCREASED SIZE OF OCCURENCE KEY TO 30                  */   34221114
000700**************************************************************/   34221114
000100**************************************************************/   36330704
000200*  COPYBOOK: CPWSIHDB                                        */   36330704
000300*  RELEASE # ___0704___   SERVICE REQUEST NO(S)____3633______*/   36330704
000400*  NAME __B.I.T._______   CREATION DATE     ____10/02/92_____*/   36330704
000500*  DESCRIPTION                                               */   36330704
000600*                                                            */   36330704
000700*       CPWSIHDB IS A NEW COPY BOOK FOR RELEASE # 0704       */   36330704
000800*                                                            */   36330704
000900**************************************************************/   36330704
001000*-----------------------------------------------------------------CPWSIHDB
001100*    HDB INTERFACE AREA                                           CPWSIHDB
001200*    USED BY EMPLOYEE HISTORY MAINTENANCE PROGRAMS                CPWSIHDB
001300*    SIZE OF TABLE SHOULD BE AT LEAST = TO THE NUMBER OF FIELDS   CPWSIHDB
001400*    IN THE LARGEST TABLE.                                        CPWSIHDB
001500*-----------------------------------------------------------------CPWSIHDB
001600*01  IHDB-INTERFACE.                                              CPWSIHDB
001700     05  IHDB-PROCESS            PIC X.                           CPWSIHDB
001800         88 IHDB-ECF                        VALUE 'E'.            CPWSIHDB
001900         88 IHDB-PERIODIC                   VALUE 'P'.            CPWSIHDB
002000         88 IHDB-INITIALIZE                 VALUE 'I'.            CPWSIHDB
002100         88 IHDB-CONVERSION                 VALUE 'C'.            CPWSIHDB
002200     05  IHDB-ERROR              PIC XX.                          CPWSIHDB
002300         88 IHDB-OK                         VALUE '00'.           CPWSIHDB
002400         88 IHDB-WARNING                    VALUE '02'.           CPWSIHDB
002500         88 IHDB-BAD-RETURN-PCD             VALUE '03'.           CPWSIHDB
002600         88 IHDB-SYSTEMS                    VALUE '04'.           CPWSIHDB
002700         88 IHDB-EDB-NOT-FOUND              VALUE '05'.           CPWSIHDB
002800         88 IHDB-HDB-NOT-FOUND              VALUE '06'.           CPWSIHDB
002900         88 IHDB-BAD-RETURN-EDB             VALUE '07'.           CPWSIHDB
003000         88 IHDB-BAD-RETURN-HDB             VALUE '08'.           CPWSIHDB
003100     05  IHDB-EDB-AUDIT          PIC X.                           CPWSIHDB
003200         88 EDB-AUDIT                       VALUE 'Y'.            CPWSIHDB
003300     05  IHDB-HDB-AUDIT          PIC X.                           CPWSIHDB
003400         88 HDB-AUDIT                       VALUE 'Y'.            CPWSIHDB
003500     05  IHDB-INITIALIZED-ROW    PIC X.                           CPWSIHDB
003600         88 INITIALIZED-ROW                 VALUE 'Y'.            CPWSIHDB
003700     05  IHDB-ITERATION-UPDATE   PIC X.                           CPWSIHDB
003800         88 NEW-ITERATION                   VALUE 'Y'.            CPWSIHDB
003900     05  IHDB-LOADED             PIC 9(4).                        CPWSIHDB
004000     05  IHDB-ROWS               PIC 9(4).                        CPWSIHDB
004100     05  IHDB-IDI-COUNT          PIC 9(6).                        CPWSIHDB
004200     05  IHDB-ATN-COUNT          PIC 9(6).                        CPWSIHDB
004300     05  IHDB-TBL-NAME           PIC X(18).                       CPWSIHDB
004400     05  IHDB-EMPLOYEE-ID        PIC X(9).                        CPWSIHDB
004500     05  IHDB-ITERATION          PIC X(3).                        CPWSIHDB
004600     05  IHDB-PROCESS-DATE.                                       CPWSIHDB
004700         10  IHDB-DATE-CC        PIC XX.                          CPWSIHDB
004800         10  IHDB-DATE-YY        PIC XX.                          CPWSIHDB
004900         10  IHDB-DATE-FILL-1    PIC X.                           CPWSIHDB
005000         10  IHDB-DATE-MM        PIC XX.                          CPWSIHDB
005100         10  IHDB-DATE-FILL-2    PIC X.                           CPWSIHDB
005200         10  IHDB-DATE-DD        PIC XX.                          CPWSIHDB
005300     05  IHDB-PROCESS-TIME.                                       CPWSIHDB
005400         10  IHDB-TIME-HH        PIC XX.                          CPWSIHDB
005500         10  IHDB-TIME-FILL-1    PIC X.                           CPWSIHDB
005600         10  IHDB-TIME-MM        PIC XX.                          CPWSIHDB
005700         10  IHDB-TIME-FILL-2    PIC X.                           CPWSIHDB
005800         10  IHDB-TIME-SS        PIC XX.                          CPWSIHDB
005900     05  IHDB-HDE-TABLE.                                          CPWSIHDB
006000         10  IHDB-DATA-ELEMENTS      OCCURS 100 TIMES             CPWSIHDB
006100                                     INDEXED BY IHDB.             CPWSIHDB
006200             15  IHDB-COL-NAME       PIC X(18).                   CPWSIHDB
006300             15  IHDB-EDB-DET-NO     PIC X(4).                    CPWSIHDB
007100****         15  IHDB-OCC-KEY        PIC X(18).                   34221114
007200             15  IHDB-OCC-KEY        PIC X(30).                   34221114
006500             15  IHDB-DATA-TYPE      PIC X.                       CPWSIHDB
006600             15  IHDB-DATA-USE       PIC X.                       CPWSIHDB
006700             15  IHDB-DECIMAL        PIC 9.                       CPWSIHDB
006800             15  IHDB-LENGTH         PIC 99.                      CPWSIHDB
006900             15  IHDB-COUNT          PIC 9(6).                    CPWSIHDB
007000             15  IHDB-VALUES.                                     CPWSIHDB
007100                 20  IHDB-OLD-VAL    PIC X(30).                   CPWSIHDB
007200                 20  IHDB-OLD-NUM    REDEFINES IHDB-OLD-VAL       CPWSIHDB
007300                                     PIC S9(9)V9(9)   COMP-3.     CPWSIHDB
007400                 20  IHDB-NEW-VAL    PIC X(30).                   CPWSIHDB
007500                 20  IHDB-NEW-NUM    REDEFINES IHDB-NEW-VAL       CPWSIHDB
007600                                     PIC S9(9)V9(9)   COMP-3.     CPWSIHDB
007700             15  IHDB-EDB-VALUES.                                 CPWSIHDB
007800                 20  IHDB-EDB-VAL    PIC X(30).                   CPWSIHDB
007900                 20  IHDB-EDB-NUM    REDEFINES IHDB-EDB-VAL       CPWSIHDB
008000                                     PIC S9(9)V9(9)   COMP-3.     CPWSIHDB
