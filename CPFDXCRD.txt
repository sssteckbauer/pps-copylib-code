000100**************************************************************/   30930413
000200*  COPYMEMBER: CPFDXCRD                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/09/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000902*************************************************************/    73820213
000903*  COPYMEMBER:  CPFDXCRD                                    */    73820213
000904*  RELEASE:  0213   REF: 0210     SERVICE REQUEST:  7382    */    73820213
000905*  NAME:     G. STEINITZ    MODIFICATION DATE:  06/09/86    */    73820213
000906*  DESCRIPTION                                              */    73820213
000907*     THE 01 NAME CARD-RECORD HAS BEEN RE-ESTABLISHED FOR   */    73820213
000908*       THOSE PROGRAMS WHICH REFERENCE IT DIRECTLY.         */    73820213
000909*************************************************************/    73820210
000910*  COPYMEMBER:  CPFDXCRD                                    */    73820210
000920*  RELEASE:  0210                 SERVICE REQUEST:  7382    */    73820210
000930*  NAME:     G. STEINITZ    MODIFICATION DATE:  05/31/86    */    73820210
000940*  DESCRIPTION                                              */    73820210
000950*     THIS IS THE FILE DESCRIPTION FOR CARDFILE - SPEC      */    73820000
000960*************************************************************/    73820210
000970*****BLOCK CONTAINS 0 RECORDS                                     30930413
000980     RECORD CONTAINS 80 CHARACTERS                                73820210
000990     LABEL RECORD IS OMITTED                                      73820210
001000     DATA RECORD IS CARDFILE-REC.                                 73820210
001100                                                                  73820210
001200 01  CARDFILE-REC                    PIC  X(80).                  73820210
001300                                                                  73820213
001400 01  CARD-RECORD                     PIC  X(80).                  73820213
