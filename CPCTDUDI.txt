000000**************************************************************/   30871465
000001*  COPYMEMBER: CPCTDUDI                                      */   30871465
000002*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000003*  NAME:_____SRS_________ CREATION DATE:      ___02/11/03__  */   30871465
000004*  DESCRIPTION:                                              */   30871465
000005*  - NEW COPY MEMBER FOR RANGE ADJUSTMENT DUD TABLE INPUT    */   30871465
000007**************************************************************/   30871465
000008*    COPYID=CPCTDUDI                                              CPCTDUDI
010000*01  RANGE-ADJUST-DUD-TABLE-INPUT.                                CPCTDUDI
010100     05 DUDI-TUC                         PIC XX.                  CPCTDUDI
010200     05 DUDI-RDUC                        PIC X.                   CPCTDUDI
010300        88  DUDI-VALID-RDUC              VALUES SPACE             CPCTDUDI
010400                                               'A' THRU 'Z'       CPCTDUDI
010500                                               '0' THRU '9'.      CPCTDUDI
010600     05 DUDI-DESCRIPTION                 PIC X(30).               CPCTDUDI
