000560*================================================================*DU025
000570*=    PROGRAM: PAR100                                           =*DU025
000580*=    CHANGE DU025          PROJ. REQUEST:  DU025               =*DU025
000590*=    NAME: MARI MCGEE      MODIFICATION DATE: 10/20/89         =*DU025
000591*=                                                              =*DU025
000592*=    DESCRIPTION:                                              =*DU025
000593*=     CHANGES ARE PART OF THE OFFLOAD FORM PRIME TO IBM.       =*DU025
000594*=                                                              =*DU025
000595*=  1. ONLY QUARTERLY PROCESSING IS PERFORMED.                  =*DU025
000596*=     NINE MONTH FACULTY (NMF)                                 =*DU025
000597*=         RUN     EXTRACT NMF QTR  OTHER STAFF  MONTHS SELECTED=*DU025
000598*=  QTR    MONTH   MONTH   MONTHS   QTR MONTHS   FOR PROCESSING =*DU025
000599*=-------- -----  -------  ------   -----------  ---------------=*DU025
000600*=1 SUMMER  11      09     NONE      07,08,09    07,08,09,10    =*DU025
000601*=                                                              =*DU025
000603*=2 FALL    02      12     07,08,    10,11,12    07,08,09,10,   =*DU025
000604*=                         09,10                 11,12,01       =*DU025
000605*=                                                              =*DU025
000606*=3 WINTER  05      03     11,12     01,02,03    11,12,01,02,03,=*DU025
000607*=                         01,02                 04             =*DU025
000608*=                                                              =*DU025
000609*=4 SPRING  08      06     03,04,    04,05,06    03,04,05,06,07 =*DU025
000610*=                         05,06                                =*DU025
000611*=                                                              =*DU025
000612*=  2. MORE THAN ONE PAR FILE IS READ, THE PCR RECORDS ARE      =*DU025
000613*=     BYPASSED                                                 =*DU025
000614*=                                                              =*DU025
000615*=  3. CHANGES NECESSARY TO CONFORM TO COBOL 2.                 =*DU025
000616*=                                                              =*DU025
000617*================================================================*DU025
