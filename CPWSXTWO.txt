001700*----------------------------------------------------------------*36410717
001800*|**************************************************************|*36410717
001900*|* COPYMEMBER: CPWSXTWO                                       *|*36410717
002000*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
002100*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
002200*|*  DESCRIPTION:                                              *|*36410717
002300*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
002400*|*  - THIS COPYMEMBER DEFINES THE X2 TRANSACTION IMAGE        *|*36410717
002500*|*    FOR USE BY THE KEY CHANGE AND DELETION PROCESS          *|*36410717
002600*|**************************************************************|*36410717
002700*----------------------------------------------------------------*36410717
002800***  COPYID=CPWSXTWO                                              CPWSXTWO
002900*                                                                 CPWSXTWO
003000*01  X2-RECORD.                                                   CPWSXTWO
003100     05  X2-CARD-CODE            PIC X(02).                       CPWSXTWO
003200     05  X2-ACTN-TYPE            PIC X(01).                       CPWSXTWO
003300     05  X2-SS-NO                PIC X(09).                       CPWSXTWO
003400     05  X2-EFFECT-DATE          PIC X(06).                       CPWSXTWO
003500     05  X2-CARD-IMAGE.                                           CPWSXTWO
003600         07  X2-DAT-EL-NO        PIC 9(05).                       CPWSXTWO
003700         07  FILLER              PIC X(21).                       CPWSXTWO
003800         07  X2-DATA-AR          PIC X(09).                       CPWSXTWO
002300         07  FILLER              PIC X(49).                       CPWSXTWO
