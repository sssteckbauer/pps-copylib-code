000100*                                                                *CPYRIGHT
000200******************************************************************CPYRIGHT
000300*  ****                                                    ****  *CPYRIGHT
000400*  ****  CONFIDENTIAL INFORMATION                          ****  *CPYRIGHT
000500*  ****                                                    ****  *CPYRIGHT
000600*  ****  THIS PROGRAM IS CONFIDENTIAL AND IS THE PROPERTY  ****  *CPYRIGHT
000700*  ****  OF THE UNIVERSITY OF CALIFORNIA; THIS PROGRAM IS  ****  *CPYRIGHT
000800*  ****  NOT TO BE COPIED, REPRODUCED, OR TRANSMITTED IN   ****  *CPYRIGHT
000900*  ****  ANY FORM, BY ANY MEANS, IN WHOLE OR PART, WITHOUT ****  *CPYRIGHT
001000*  ****  THE WRITTEN PERMISSION FROM THE BOARD OF REGENTS, ****  *CPYRIGHT
001100*  ****  UNIVERSITY OF CALIFORNIA.                         ****  *CPYRIGHT
001200*  ****                                                    ****  *CPYRIGHT
001300******************************************************************CPYRIGHT
001400*                                                                *CPYRIGHT
