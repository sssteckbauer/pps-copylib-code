000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSXLAT                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*                                                            */   32021138
000006*  IMPLEMENTATION OF THE FLEXIBLE FULL ACCOUNTING UNIT (3202)*/   32021138
000007*                                                            */   32021138
000008**************************************************************/   32021138
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXLAT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   30860356
000200*  COPYMEMBER: CPWSXLAT                                      */   30860356
000300*  RELEASE # ___0356___   SERVICE REQUEST NO(S) 3086________ */   30860356
000400*  NAME ____WEJ________   MODIFICATION DATE ____05/05/88____ */   30860356
000500*                                                            */   30860356
000600*  DESCRIPTION                                               */   30860356
000700*    - INITIAL RELEASE: CONTROL FILE RECORD DESCRIPTION      */   30860356
000800*      FOR THE LEAVE ACCRUAL RECORD.                         */   30860356
000900**************************************************************/   30860356
001000*    COPYID=CPWSXLAT                                              CPWSXLAT
001100*01  XLAT-LEAVE-ACCRUAL-RECORD.                                   30930413
001200*--------------------------------------------------------------   CPWSXLAT
001300*    L E A V E   A C C R U A L   T A B L E    R E C O R D    *    CPWSXLAT
001400*--------------------------------------------------------------   CPWSXLAT
001500     SKIP1                                                        CPWSXLAT
001600     05  XLAT-DELETE                PIC X.                        CPWSXLAT
001700     05  XLAT-KEY.                                                CPWSXLAT
001800         10  XLAT-KEY-CONSTANT      PIC X(04).                    CPWSXLAT
001900         10  XLAT-TITLE-TYPE        PIC X(01).                    CPWSXLAT
002000         10  XLAT-TITLE-UNIT-CD     PIC X(02).                    CPWSXLAT
002100         10  XLAT-AREP-CD           PIC X(01).                    CPWSXLAT
002200         10  XLAT-SPEC-HAND-CD      PIC X(01).                    CPWSXLAT
002300         10  XLAT-DISTR-UNIT-CD     PIC X(01).                    CPWSXLAT
002400         10  XLAT-EFF-DATE          PIC S9(04) COMP.              32021138
002401         10  XLAT-RECORD-TYPE       PIC X(01).                    32021138
002410****     10  XLAT-EFF-DATE          PIC S9(5)     COMP-3.         32021138
002500****         DATE: FIVE NUMERIC POSITIONS: SYYMM,                 32021138
002600****               WHERE "S" IS UNUSED (SLACK).                   32021138
002700     SKIP1                                                        CPWSXLAT
002800     05  XLAT-PROCESS-DATA.                                       CPWSXLAT
002900         10  XLAT-THRESH-PERCNT     PIC S9V9999   COMP-3.         CPWSXLAT
003000         10  XLAT-MAX-HRS-OVERIDE.                                CPWSXLAT
003100             15  XLAT-MHO-VAC       PIC S9(5)V99  COMP-3.         CPWSXLAT
003200             15  XLAT-MHO-SKL       PIC S9(5)V99  COMP-3.         CPWSXLAT
003300             15  XLAT-MHO-PTO       PIC S9(5)V99  COMP-3.         CPWSXLAT
003400         10  XLAT-HRS-PER-DAY       PIC S9(3)V99  COMP-3.         CPWSXLAT
003500         10  XLAT-PRE-CALC-CD       PIC 9(02).                    CPWSXLAT
003600         10  XLAT-RT-SCHED-NO       PIC 9(3).                     CPWSXLAT
003700         10  XLAT-POST-CALC-CD      PIC 9(02).                    CPWSXLAT
003800         10  FILLER                 PIC X(178).                   32021138
003810****     10  XLAT-FUND-RANGE-DATA  OCCURS 6 TIMES.                32021138
003900****         15  XLAT-FR-LO         PIC 9(5).                     32021138
004000****         15  XLAT-FR-HI         PIC 9(5).                     32021138
004010****         15  XLAT-FUND-GROUP    PIC X(04).                    32021138
004100****         15  XLAT-UTIL-FACTORS.                               32021138
004200****             20  XLAT-UF-VAC    PIC S9V9(4)  COMP-3.          32021138
004300****             20  XLAT-UF-SKL    PIC S9V9(4)  COMP-3.          32021138
004400****             20  XLAT-UF-PTO    PIC S9V9(4)  COMP-3.          32021138
004500****         15  XLAT-LV-RES-ACCT   PIC 9(6).                     32021138
004600****     10  FILLER                 PIC X(28).                    32021138
004700         10  XLAT-LAST-UPDT-INFO.                                 CPWSXLAT
004800             15  XLAT-LAST-UPDT-ACTION  PIC X.                    CPWSXLAT
004900             15  XLAT-LAST-UPDT-DATE    PIC X(06).                CPWSXLAT
004910     05  XLAT-UTILIZATION-ACCTING-DATA REDEFINES                  32021138
004920         XLAT-PROCESS-DATA.                                       32021138
004940         10  XLAT-FND-GROUP-CODE       PIC X(04).                 32021138
004950         10  XLAT-UTIL-FACTORS.                                   32021138
004960             15  XLAT-UF-VAC           PIC S9V9(4)  COMP-3.       32021138
004970             15  XLAT-UF-SKL           PIC S9V9(4)  COMP-3.       32021138
004980             15  XLAT-UF-PTO           PIC S9V9(4)  COMP-3.       32021138
004990         10  XLAT-LV-RES-FAU           PIC X(30).                 32021138
004991         10  FILLER                    PIC X(167).                32021138
005000     EJECT                                                        32021138
