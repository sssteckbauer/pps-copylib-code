000000**************************************************************/   15400936
000001*  COPYMEMBER: CPLN142F                                      */   15400936
000002*  RELEASE: ___0936______ SERVICE REQUEST(S): ____11540____  */   15400936
000003*  NAME:_______QUAN______ CREATION DATE:      ___09/19/94__  */   15400936
000004*  DESCRIPTION:                                              */   15400936
000005*                                                            */   15400936
002000*    - INITIAL RELEASE OF COPYMEMBER FOR LINKAGE BETWEEN     */   15400936
002100*      CALLING PROGRAM AND PP1042SF                          */   15400936
002200**************************************************************/   15400936
002300*01  PP1042SF-INTERFACE.                                          CPLN142F
002400     05  S1042F-CALL-TYPE                PIC X(01).               CPLN142F
002500         88  S1042F-INITIAL-CALL              VALUE 'I'.          CPLN142F
002600         88  S1042F-EMPLOYEE-CALL             VALUE 'E'.          CPLN142F
002700         88  S1042F-FINAL-CALL                VALUE 'F'.          CPLN142F
002800     05  S1042F-RETURN-CODE              PIC 9(01).               CPLN142F
002900         88  S1042F-RETURN-NORMAL             VALUE 0.            CPLN142F
003000         88  S1042F-CALLING-SEQUENCE-ERR      VALUE 1.            CPLN142F
003400         88  S1042F-RET-NEVER-SET-BY-S1042F   VALUE 9.            CPLN142F
003500     05  S1042F-PROGRAM-STATE            PIC 9(01).               CPLN142F
003600         88  S1042F-EXPECTING-INITIAL-CALL    VALUE 0.            CPLN142F
003700         88  S1042F-EXPECTING-EMPLOYEE-CALL   VALUE 1.            CPLN142F
003800         88  S1042F-RECEIVED-FINAL-CALL       VALUE 2.            CPLN142F
003900     SKIP1                                                        CPLN142F
004000     05  S1042F-INITIAL-CALL-DATA.                                CPLN142F
004100         10  S1042F-REPORT-PERIOD.                                CPLN142F
004200             15   S1042F-CENTURY         PIC 9(02).               CPLN142F
004300             15   S1042F-YEAR            PIC 9(02).               CPLN142F
004400         10  S1042F-OUTPUT-SELECTORS.                             CPLN142F
004900             15  S1042F-FORM             PIC X(01).               CPLN142F
005100                 88 S1042F-NO-1042-FORM       VALUE  'N'.         CPLN142F
005101                 88 S1042F-BLANKS-ONLY        VALUE  'B'.         CPLN142F
005102             15  S1042F-EMPLR-COPY       PIC X(01).               CPLN142F
005103                 88  S1042F-DO-1042-COPY      VALUE  'Y'.         CPLN142F
005104                 88  S1042F-NO-1042-COPY      VALUE  'N'.         CPLN142F
005110             15  S1042F-FICHE-FILE       PIC X(01).               CPLN142F
005120                 88 S1042F-MAKE-1042-FICHE    VALUE  'Y'.         CPLN142F
005130                 88 S1042F-NO-1042-FICHE      VALUE  'N'.         CPLN142F
005200             15  S1042F-BLANK-1042-FORMS.                         CPLN142F
005310                 17  S1042F-NBR-BLANK-1042-FORMS                  CPLN142F
005320                                         PIC 9(03).               CPLN142F
005500     SKIP1                                                        CPLN142F
005600     05  S1042F-FORMS-GROUP-SEQUENCE.                             CPLN142F
005700         10  S1042F-GROUP-SEQ            PIC X(01).               CPLN142F
005800             88  S1042F-GROUP-RESERVED-0      VALUE '0'.          CPLN142F
005900             88  S1042F-GROUP-EMPL-ID         VALUE '1'.          CPLN142F
006000             88  S1042F-GROUP-SOCIAL-SEC      VALUE '2'.          CPLN142F
006100             88  S1042F-GROUP-HOME-DEPT       VALUE '3'.          CPLN142F
006200             88  S1042F-GROUP-COUNTRY-CODE    VALUE '4'.          CPLN142F
006300             88  S1042F-GROUP-RESERVED-5      VALUE '5'.          CPLN142F
006400             88  S1042F-GROUP-BLANK-1042      VALUE '6'.          CPLN142F
006410             88  S1042F-GROUP-RESERVED-7      VALUE '7'.          CPLN142F
006500             88  S1042F-GROUP-OTHERS          VALUE '8'.          CPLN142F
006800     SKIP1                                                        CPLN142F
007000     05  S1042F-FINAL-TOTALS.                                     CPLN142F
007100         10  S1042F-FINAL-EMPLOYEE-COUNT   PIC S9(07) COMP.       CPLN142F
007101         10  S1042F-FINAL-1042-COUNT       PIC S9(07) COMP.       CPLN142F
007110         10  S1042F-FINAL-TOTAL-GROSS-PD   PIC S9(09)V99 COMP-3.  CPLN142F
007700         10  S1042F-FINAL-TOTAL-FWT-TAX    PIC S9(09)V99 COMP-3.  CPLN142F
008910         10  S1042F-FINAL-TOTAL-ALLOWANCE  PIC S9(09)V99 COMP-3.  CPLN142F
008912         10  S1042F-FINAL-TOTAL-NET-INCOME PIC S9(09)V99 COMP-3.  CPLN142F
