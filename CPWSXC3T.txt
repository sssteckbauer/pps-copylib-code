000010**************************************************************/   32021138
000020*  COPYMEMBER: CPWSXC3T                                      */   32021138
000030*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000040*  NAME:_P. THOMPSON_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000050*  DESCRIPTION:                                              */   32021138
000060*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000080**************************************************************/   32021138
000100**************************************************************/   36370967
000200*  COPYMEMBER: CPWSXC3T                                      */   36370967
000300*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3637____  */   36370967
000400*  NAME:_____SRS_________ MODIFICATION DATE:  ___03/10/95__  */   36370967
000500*  DESCRIPTION:                                              */   36370967
000600*  - MODIFIED FORMATS FOR REFERENCE OF 1ST OCCURENCES        */   36370967
000700**************************************************************/   36370967
000000**************************************************************/   37540566
000001*  COPYMEMBER: CPWSXC3T                                      */   37540566
000002*  RELEASE: ___0566______ SERVICE REQUEST(S): _____3754____  */   37540566
000003*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___05/06/91__  */   37540566
000004*  DESCRIPTION:                                              */   37540566
000005*  - ADDED NEW DA TRNASACTION LAYOUT                         */   37540566
000006*                                                            */   37540566
000007**************************************************************/   37540566
000100**************************************************************/   30950468
000200*  COPY MODULE:  CPWSXC3T                                    */   30950468
000300*  RELEASE # ____0468____ SERVICE REQUEST NO(S)___3095_______*/   30950468
000400*  NAME __M. BAPTISTA__   MODIFICATION DATE ____03/20/89_____*/   30950468
000600*  DESCRIPTION                                               */   30950468
000700*   - ADDED A REDEFINE FOR XC3T-TRAN-IMAGE TO REFLECT THE    */   30950468
000800*     FORMAT OF THE NEW 'LA' TRANSACTION.                    */   30950468
000900**************************************************************/   30950468
000100**************************************************************/   30930413
000200*  COPY MODULE:  CPWSXC3T                                    */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   13190159
000200*  COPY MODULE:  CPWSXC3T                                    */   13190159
000300*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
000400*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
000500*  DESCRIPTION                                               */   13190159
000600*  THE INPUT CARD RECORD LENGTH WAS INCREASED                */   13190159
000700*  FROM 80 TO 102 BYTES.                                     */   13190159
000800**************************************************************/   13190159
000900     SKIP2                                                        13190159
001000*    COPYID=CPWSXC3T                                              CPWSXC3T
001100*01  XC3T-C-O-H-INPUT-CARD-3.                                     30930413
001200*    THIS FORMAT ALSO USED FOR HA, DA, DS, PS, RF, AD.            CPWSXC3T
001300     05  XC3T-ID.                                                 CPWSXC3T
001400         10  XC3T-ACTN-CD        PIC X.                           CPWSXC3T
001500         10  XC3T-SEQ-NO         PIC X(5).                        CPWSXC3T
004600         10  XC3T-SEQ-NO9                                         36370967
004700             REDEFINES                                            36370967
004800             XC3T-SEQ-NO         PIC 9(5).                        36370967
001600         10  XC3T-FILLER         PIC X(3).                        CPWSXC3T
001700     05  XC3T-TRAN-CD            PIC XX.                          CPWSXC3T
001800     05  XC3T-TRAN-IMAGE.                                         CPWSXC3T
001900         10  XC3T-CHK.                                            CPWSXC3T
002000             15  XC3T-CHK-ADV    PIC X.                           CPWSXC3T
002100             15  XC3T-CHK-NO     PIC X(6).                        CPWSXC3T
002200         10  XC3T-TRAN-DATE      REDEFINES                        CPWSXC3T
002300             XC3T-CHK.                                            CPWSXC3T
002400             15  FILLER          PIC X.                           CPWSXC3T
002500             15  XC3T-TRAN-MONTH PIC XX.                          CPWSXC3T
002600             15  XC3T-TRAN-DAY   PIC XX.                          CPWSXC3T
002700             15  XC3T-TRAN-YEAR  PIC XX.                          CPWSXC3T
006100         10  XC3T-1-ELEMENTS.                                     36370967
006200             15  XC3T-1-ELEM-NO  PIC 9(4).                        36370967
006300             15  XC3T-1-BAL-TYPE PIC X.                           36370967
006400             15  XC3T-1-AMTX     PIC X(7).                        36370967
006500             15  XC3T-1-AMT9 REDEFINES                            36370967
006600                 XC3T-1-AMTX     PIC 9(5)V99.                     36370967
006700             15  XC3T-1-SIGN     PIC X.                           36370967
006800             15  XC3T-2-ELEM-NO  PIC 9(4).                        36370967
006900             15  XC3T-2-BAL-TYPE PIC X.                           36370967
007000             15  XC3T-2-AMTX     PIC X(7).                        36370967
007100             15  XC3T-2-AMT9 REDEFINES                            36370967
007200                 XC3T-2-AMTX     PIC 9(5)V99.                     36370967
007300             15  XC3T-2-SIGN     PIC X.                           36370967
007400             15  XC3T-3-ELEM-NO  PIC 9(4).                        36370967
007500             15  XC3T-3-BAL-TYPE PIC X.                           36370967
007600             15  XC3T-3-AMTX     PIC X(7).                        36370967
007700             15  XC3T-3-AMT9 REDEFINES                            36370967
007800                 XC3T-3-AMTX     PIC 9(5)V99.                     36370967
007900             15  XC3T-3-SIGN     PIC X.                           36370967
008000             15  XC3T-4-ELEM-NO  PIC 9(4).                        36370967
008100             15  XC3T-4-BAL-TYPE PIC X.                           36370967
008200             15  XC3T-4-AMTX     PIC X(7).                        36370967
008300             15  XC3T-4-AMT9 REDEFINES                            36370967
008400                 XC3T-4-AMTX     PIC 9(5)V99.                     36370967
008500             15  XC3T-4-SIGN     PIC X.                           36370967
008600         10  XC3T-ELEMENTS       REDEFINES                        36370967
008700             XC3T-1-ELEMENTS     OCCURS 4.                        36370967
008800*********10  XC3T-ELEMENTS       OCCURS 4.                        36370967
002900*************15  XC3T-ELEM-NO.                                    30950468
002901             15  XC3T-ELEM-NO    PIC 9(4).                        30950468
009100*****        15  FILLER          REDEFINES                        36370967
009200             15  XC3T-ELEM-NOX   REDEFINES                        36370967
002920                 XC3T-ELEM-NO.                                    30950468
003000                 20  XC3T-SEG-NO-1   PIC X.                       CPWSXC3T
003100                 20  XC3T-ELEM-NO-3  PIC 999.                     CPWSXC3T
003200                 20  XC3T-ELEM-NO-3X REDEFINES                    CPWSXC3T
003300                     XC3T-ELEM-NO-3  PIC XXX.                     CPWSXC3T
003400             15  XC3T-BAL-TYPE   PIC X.                           CPWSXC3T
003500             15  XC3T-AMTX       PIC X(7).                        CPWSXC3T
003600             15  XC3T-AMT9       REDEFINES                        CPWSXC3T
003700                 XC3T-AMTX       PIC 9(5)V99.                     CPWSXC3T
003800             15  XC3T-SIGN       PIC X.                           CPWSXC3T
003900         10  XC3T-QTR-CD         PIC X.                           CPWSXC3T
004000         10  XC3T-YEAR           PIC X.                           CPWSXC3T
004100         10  XC3T-XFOOTX         PIC X(7).                        CPWSXC3T
004200         10  XC3T-XFOOT9         REDEFINES                        CPWSXC3T
004300             XC3T-XFOOTX         PIC 9(5)V99.                     CPWSXC3T
004400         10  XC3T-XFOOT-SIGN     PIC X.                           CPWSXC3T
010810         10  FILLER              PIC X(18).                       32021138
004500         10  XC3T-FILLER-OP      PIC X(02).                       13190159
004600         10  XC3T-FILLER-COS     PIC X(20).                       13190159
004601*    THIS TRAN IMAGE USED ONLY FOR LA TRANSACACTIONS              30950468
004700     05  XC3T-LV-TRAN-IMAGE      REDEFINES                        30950468
004710         XC3T-TRAN-IMAGE.                                         30950468
004800         10  FILLER              PIC X(7).                        30950468
011500         10  XC3T-LV1-ELEMENTS.                                   36370967
011600             15  XC3T-LV1-ELEM-NO PIC 9(4).                       36370967
011700             15  XC3T-LV1-AMTX    PIC X(11).                      36370967
011800             15  XC3T-LV1-AMT9 REDEFINES                          36370967
011900                 XC3T-LV1-AMTX    PIC 9(5)V9(6).                  36370967
012000             15  XC3T-LV1-SIGN    PIC X.                          36370967
012100             15  XC3T-LV2-ELEM-NO PIC 9(4).                       36370967
012200             15  XC3T-LV2-AMTX    PIC X(11).                      36370967
012300             15  XC3T-LV2-AMT9 REDEFINES                          36370967
012400                 XC3T-LV2-AMTX    PIC 9(5)V9(6).                  36370967
012500             15  XC3T-LV2-SIGN    PIC X.                          36370967
012600             15  XC3T-LV3-ELEM-NO PIC 9(4).                       36370967
012700             15  XC3T-LV3-AMTX    PIC X(11).                      36370967
012800             15  XC3T-LV3-AMT9 REDEFINES                          36370967
012900                 XC3T-LV3-AMTX    PIC 9(5)V9(6).                  36370967
013000             15  XC3T-LV3-SIGN    PIC X.                          36370967
013100         10  XC3T-LV-ELEMENTS    REDEFINES                        36370967
013200             XC3T-LV1-ELEMENTS   OCCURS 3.                        36370967
013300*********10  XC3T-LV-ELEMENTS       OCCURS 3.                     36370967
005800             15  XC3T-LV-ELEM-NO PIC 9(4).                        30950468
013500             15  XC3T-LV-ELEM-NOX REDEFINES                       36370967
013600                 XC3T-LV-ELEM-NO PIC X(04).                       36370967
006400             15  XC3T-LV-AMTX    PIC X(11).                       30950468
006500             15  XC3T-LV-AMT9    REDEFINES                        30950468
006600                 XC3T-LV-AMTX    PIC 9(5)V9(6).                   30950468
006700             15  XC3T-LV-SIGN    PIC X.                           30950468
006800         10  XC3T-LV-QTR-CD      PIC X.                           30950468
006900         10  XC3T-LV-YEAR        PIC X.                           30950468
007000         10  XC3T-LV-XFOOTX      PIC X(11).                       30950468
007010         10  XC3T-LV-XFOOT9      REDEFINES                        30950468
007100             XC3T-LV-XFOOTX      PIC 9(5)V9(6).                   30950468
007300         10  XC3T-LV-XFOOT-SIGN  PIC X.                           30950468
014700*****    10  FILLER              PIC X(22).                       32021138
014710         10  FILLER              PIC X(40).                       32021138
007500*    THIS TRAN IMAGE USED ONLY FOR DA TRANSACACTIONS              37540566
007600     05  XC3T-DA-TRAN-IMAGE      REDEFINES                        37540566
007700         XC3T-TRAN-IMAGE.                                         37540566
007800         10  FILLER              PIC X(7).                        37540566
015200         10  XC3T-DA1-ELEMENTS.                                   36370967
015300             15  XC3T-DA1-ELEM-NO    PIC 9(4).                    36370967
015400             15  XC3T-DA1-BAL-TYPE   PIC X.                       36370967
015500             15  XC3T-DA1-AMTX       PIC X(08).                   36370967
015600             15  XC3T-DA1-AMT9 REDEFINES                          36370967
015700                 XC3T-DA1-AMTX       PIC 9(6)V9(2).               36370967
015800             15  XC3T-DA1-SIGN       PIC X.                       36370967
015900             15  XC3T-DA2-ELEM-NO    PIC 9(4).                    36370967
016000             15  XC3T-DA2-BAL-TYPE   PIC X.                       36370967
016100             15  XC3T-DA2-AMTX       PIC X(08).                   36370967
016200             15  XC3T-DA2-AMT9 REDEFINES                          36370967
016300                 XC3T-DA2-AMTX       PIC 9(6)V9(2).               36370967
016400             15  XC3T-DA2-SIGN       PIC X.                       36370967
016500             15  XC3T-DA3-ELEM-NO    PIC 9(4).                    36370967
016600             15  XC3T-DA3-BAL-TYPE   PIC X.                       36370967
016700             15  XC3T-DA3-AMTX       PIC X(08).                   36370967
016800             15  XC3T-DA3-AMT9 REDEFINES                          36370967
016900                 XC3T-DA3-AMTX       PIC 9(6)V9(2).               36370967
017000             15  XC3T-DA3-SIGN       PIC X.                       36370967
017100         10  XC3T-DA-ELEMENTS    REDEFINES                        36370967
017200             XC3T-DA1-ELEMENTS      OCCURS 3.                     36370967
017300*********10  XC3T-DA-ELEMENTS       OCCURS 3.                     36370967
008000             15  XC3T-DA-ELEM-NO PIC 9(4).                        37540566
017500*****        15  FILLER          REDEFINES                        36370967
017600             15  XC3T-DA-ELEM-NOX REDEFINES                       36370967
008002                 XC3T-DA-ELEM-NO.                                 37540566
008003                 20  XC3T-DA-SEG-NO-1   PIC X.                    37540566
008004                 20  XC3T-DA-ELEM-NO-3  PIC 999.                  37540566
008005                 20  XC3T-DA-ELEM-NO-3X REDEFINES                 37540566
008006                     XC3T-DA-ELEM-NO-3  PIC XXX.                  37540566
008007             15  XC3T-DA-BAL-TYPE   PIC X.                        37540566
008100             15  XC3T-DA-AMTX    PIC X(08).                       37540566
008200             15  XC3T-DA-AMT9    REDEFINES                        37540566
008300                 XC3T-DA-AMTX    PIC 9(6)V9(2).                   37540566
008400             15  XC3T-DA-SIGN    PIC X.                           37540566
008500         10  FILLER              PIC X(11).                       37540566
008700         10  XC3T-DA-XFOOTX      PIC X(08).                       37540566
008800         10  XC3T-DA-XFOOT9      REDEFINES                        37540566
008900             XC3T-DA-XFOOTX      PIC 9(6)V9(2).                   37540566
009000         10  XC3T-DA-XFOOT-SIGN  PIC X.                           37540566
019200*****    10  FILLER              PIC X(22).                       32021138
019300         10  FILLER              PIC X(40).                       32021138
