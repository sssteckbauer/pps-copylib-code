000100**************************************************************/   36480863
000200*  COPYMEMBER: UCWSPANM                                      */   36480863
000300*  RELEASE: ___0863______ SERVICE REQUEST(S): _____3648____  */   36480863
000400*  NAME:_______AAC_______ MODIFICATION DATE:  __01/27/94____ */   36480863
000500*  DESCRIPTION:                                              */   36480863
000600*     PAN SUBSYSTEM: PANQ EMAIL TRIGGER RECORD               */   36480863
000700*                                                            */   36480863
000800**************************************************************/   36480863
000900*01  UCWSPANM EXTERNAL.                                           UCWSPANM
001000     05  UCWSPANM-AREA   PIC X(256).                              UCWSPANM
001100     05  UCWSPANM-DATA REDEFINES UCWSPANM-AREA.                   UCWSPANM
001200         10  UCWSPANM-NOTICE-ID            PIC X(26).             UCWSPANM
001300         10  UCWSPANM-ADDRESS-NUM          PIC S9(04) COMP.       UCWSPANM
001400         10  FILLER                        PIC X(228).            UCWSPANM
001500***************    END OF SOURCE - UCWSPANM    *******************UCWSPANM
