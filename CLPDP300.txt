000010*==========================================================%      UCSD0006
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0006
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD0006
000040*=                                                        =%      UCSD0006
000050*==========================================================%      UCSD0006
000010*==========================================================%      DS795
000020*=    COPY MEMBER: CLPDP300                               =%      DS795
000030*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000040*=                                         CONVERSION     =%      DS795
000050*=    NAME: G. CHIU         MODIFICATION DATE: 05/05/94   =%      DS795
000060*=                                                        =%      DS795
000070*=    DESCRIPTION:                                        =%      DS795
000080*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000090*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
000091*==========================================================%      DS795
000060*==========================================================%      UCSD0006
000070*=                                                        =%      UCSD0006
000080*=    COPY MEMBER: CLPDP300                               =%      UCSD0006
000090*=    CHANGE #0006          PROJ. REQUEST: CONTROL REPORT =%      UCSD0006
000091*=    NAME: MARI MCGEE      MODIFICATION DATE: 04/26/91   =%      UCSD0006
000092*=                                                        =%      UCSD0006
000093*=    DESCRIPTION                                         =%      UCSD0006
000094*=     ADD SECTION TO PRINT-CONTROL-REPORT.               =%      UCSD0006
000095*=                                                        =%      UCSD0006
000096*==========================================================%      UCSD0006
00001 *                                                                 CLPDP300
00002 *      CONTROL REPORT 'PPP300CR'                                  CLPDP300
00003 *                                                                 CLPDP300
00004 *                                                                 CLPDP300
000500*PRINT-CONTROL-REPORT.                                            UCSD0006
000510 PRINT-CONTROL-REPORT SECTION.                                    UCSD0006
00006                                                                   CLPDP300
00007      OPEN OUTPUT CONTROLREPORT.                                   CLPDP300
00008                                                                   CLPDP300
00009      MOVE 'PPP300CR'             TO CR-HL1-RPT.                   CLPDP300
00010      MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP300
00011      WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP300
00012                                                                   CLPDP300
00013      MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP300
00014      WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP300
00015                                                                   CLPDP300
00016      MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP300
00017      MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP300
00018      MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP300
00019      MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP300
00020      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP300
00021                                                                   CLPDP300
00022      MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP300
00023      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP300
00024                                                                   CLPDP300
00025      MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP300
00026      MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP300
00027      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP300
00028                                                                   CLPDP300
00029      MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP300
00030      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP300
00031                                                                   CLPDP300
003110*****ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
003120     COPY CPPDTIME.                                               DS795
003120     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
00033      MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP300
00034      MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP300
00035      MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP300
00036      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP300
00037                                                                   CLPDP300
00038      MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP300
00039      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP300
00040 *                                                                 CLPDP300
00041 *  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP300
00042 *                                                                 CLPDP300
00043 *  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP300
00044 *  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP300
00045 *                                                                 CLPDP300
00046      MOVE 'TIMEFILE'             TO CR-DL9-FILE.                  CLPDP300
00047      MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP300
00048      MOVE TIME-FILE-RCDS-WRITTEN TO CR-DL9-VALUE.                 CLPDP300
00049      MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP300
00050      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP300
00051 *                                                                 CLPDP300
00053 *                                                                 CLPDP300
00054      MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP300
00055      WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP300
00056                                                                   CLPDP300
00057      CLOSE CONTROLREPORT.                                         CLPDP300
