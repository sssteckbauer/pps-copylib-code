000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXTTT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPWSXTTT
000200*01  XTTT-TITLE-CODE-P-TRAN.                                      30930413
000300     05  XTTT-CONTROL.                                            CPWSXTTT
000400         08  XTTT-ACD            PIC X   VALUE 'A'.               CPWSXTTT
000500         08  XTTT-TABLE-NO       PIC XX  VALUE '04'.              CPWSXTTT
000600         08  XTTT-TRAN-CODE      PIC X   VALUE 'P'.               CPWSXTTT
000700         08  FILLER              PIC X(2) VALUE '  '.             CPWSXTTT
000800         08  XTTT-TITLE-CODE     PIC 9(4).                        CPWSXTTT
000900     05  XTTT-RECORD-P.                                           CPWSXTTT
001000         08  XTTT-LOCATION       PIC X(2) VALUE SPACES.           CPWSXTTT
001100         08  XTTT-STEPS          PIC X(2) VALUE SPACES.           CPWSXTTT
001200         08  XTTT-MGMT-SAL-GRADE PIC X    VALUE SPACE.            CPWSXTTT
001300         08  XTTT-ANN-SAL-HRLY-RATE OCCURS 2.                     CPWSXTTT
001400             11  XTTT-RATE-CODE  PIC X.                           CPWSXTTT
001500             11  XTTT-MIN-ANNUAL PIC 9(6)V99.                     CPWSXTTT
001600             11  XTTT-MIN-HOURLY REDEFINES XTTT-MIN-ANNUAL        CPWSXTTT
001700                                 PIC 9(4)V9(4).                   CPWSXTTT
001800             11  XTTT-MAX-ANNUAL PIC 9(6)V99.                     CPWSXTTT
001900             11  XTTT-MAX-HOURLY REDEFINES XTTT-MAX-ANNUAL        CPWSXTTT
002000                                 PIC 9(4)V9(4).                   CPWSXTTT
002100         08  XTTT-ON-CALL-RATE   PIC 99V99.                       CPWSXTTT
002200         08  XTTT-ON-CALL-RATE-X REDEFINES  XTTT-ON-CALL-RATE     CPWSXTTT
002300                                 PIC X(4).                        CPWSXTTT
002400         08  XTTT-SHIFT-DIFF     OCCURS 2.                        CPWSXTTT
002500             11  XTTT-SHIFT-CODE PIC X.                           CPWSXTTT
002600             11  XTTT-SHIFT-RATE PIC 99V99.                       CPWSXTTT
002700         08  XTTT-HRS-IN-WEEK    PIC 99V99.                       CPWSXTTT
002800         08  XTTT-HRS-IN-WEEK-X  REDEFINES  XTTT-HRS-IN-WEEK      CPWSXTTT
002900                                 PIC X(4).                        CPWSXTTT
003000         08  FILLER              PIC X(13).                       CPWSXTTT
