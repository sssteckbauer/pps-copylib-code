000010*==========================================================%      UCSD0006
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0006
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD0006
000040*=                                                        =%      UCSD0006
000050*==========================================================%      UCSD0006
000100*==========================================================%      DS795
000200*=    COPY MEMBER: CLPDP960                               =%      DS795
000300*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000400*=                                         CONVERSION     =%      DS795
000500*=    NAME: G. CHIU         MODIFICATION DATE: 10/02/95   =%      DS795
000600*=                                                        =%      DS795
000700*=    DESCRIPTION:                                        =%      DS795
000800*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000900*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
001000*==========================================================%      DS795
000060*==========================================================%      UCSD0006
000070*=                                                        =%      UCSD0006
000080*=    COPY MEMBER: CLPDP960                               =%      UCSD0006
000090*=    CHANGE #0006          PROJ. REQUEST: CONTROL REPORT =%      UCSD0006
000091*=    NAME: MARI MCGEE      MODIFICATION DATE: 04/26/91   =%      UCSD0006
000092*=                                                        =%      UCSD0006
000093*=    DESCRIPTION                                         =%      UCSD0006
000094*=     ADD SECTION TO PRINT-CONTROL-REPORT.               =%      UCSD0006
000095*=                                                        =%      UCSD0006
000096*==========================================================%      UCSD0006
00001 *                                                                 CLPDP960
000200*      CONTROL REPORT PPP960CR                                    CLPDP960
00003 *                                                                 CLPDP960
00004 *                                                                 CLPDP960
000410*PRINT-CONTROL-REPORT.                                            UCSD0006
000420 PRINT-CONTROL-REPORT SECTION.                                    UCSD0006
00006                                                                   CLPDP960
00007      OPEN OUTPUT CONTROLREPORT.                                   CLPDP960
00008                                                                   CLPDP960
000900     MOVE 'PPP960CR'             TO CR-HL1-RPT.                   CLPDP960
00010      MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP960
00011      WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP960
00012                                                                   CLPDP960
00013      MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP960
00014      WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP960
00015                                                                   CLPDP960
00016      MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP960
00017      MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP960
00018      MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP960
00019      MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP960
00020      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP960
00021                                                                   CLPDP960
00022      MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP960
00023      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP960
00024                                                                   CLPDP960
00027      MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP960
00028      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP960
00029                                                                   CLPDP960
00030      MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP960
00031      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP960
00032                                                                   CLPDP960
003010*****ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
00033 *****MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP960
004300     COPY CPPDTIME.                                               DS795
004400     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
00034      MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP960
00035      MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP960
00036      MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP960
00037      WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP960
00038                                                                   CLPDP960
00039      MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP960
00040      WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP960
00041 *                                                                 CLPDP960
00042 *  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP960
00043 *                                                                 CLPDP960
00044 *  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP960
00045 *  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP960
00046 *                                                                 CLPDP960
00052 *                                                                 CLPDP960
00059      MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP960
00060      WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP960
00061                                                                   CLPDP960
00062      CLOSE CONTROLREPORT.                                         CLPDP960
