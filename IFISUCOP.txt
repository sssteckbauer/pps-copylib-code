      *01  LK-PARM-DATA.
           05  LK-IFIS-DATA.
               10  LK-IFIS-COA                     PIC  X(01).
               10  LK-IFIS-UNVRS-CODE              PIC  X(02).
               10  LK-IFIS-FOAPAL.
                   15  LK-IFIS-INDEX               PIC  X(10).
                   15  LK-IFIS-FUND                PIC  X(06).
                   15  LK-IFIS-ORGZN               PIC  X(06).
                   15  LK-IFIS-ACCT                PIC  X(06).
                       88  IFIS-ACCT-USE-BUDGET VALUE
                           '600000' '610000' '620000' '630000'
                           '640000' '650000' '660000' '670000'
                           '680000' '690000'.
                   15  LK-IFIS-PRGRM               PIC  X(06).
                   15  LK-IFIS-ACTVY               PIC  X(06).
                   15  LK-IFIS-LCTN                PIC  X(06).
               10  LK-IFIS-RULE.
                   15  LK-IFIS-RULE-CLASS-CODE     PIC  X(04).
                   15  LK-IFIS-PRCS-CODE           PIC  X(04).
               10  LK-SWITCHES.
                  15  LK-DEFAULT-SW                PIC  X(01).
                      88 DEFAULTS-WANTED         VALUE 'Y'.
                  15  LK-INDEX-SW                  PIC  X(01).
                      88 TRANSLATE-INDEX         VALUE 'Y'.
                  15  LK-REPORT-SW                 PIC  X(01).
                      88 ERROR-REPORT-WANTED     VALUE 'Y'.
                  15  LK-REPORT-NAME               PIC  X(01).
                  15  FILLER                       PIC  X(06).
           05  LK-UCOP-DATA.
               10  LK-UCOP-LOCATION.
                   15  LK-UCOP-MAJOR-LCTN          PIC  X(02).
                   15  LK-UCOP-UNIV-WIDE-IND       PIC  X(01).
                   15  LK-UCOP-ORG-R-LCTN          PIC  X(01).
               10  LK-UCOP-ACCT                    PIC  X(06).
               10  LK-UCOP-FUND                    PIC  X(05).
               10  LK-UCOP-SUB                     PIC  X(01).
               10  LK-UCOP-OBJECT                  PIC  X(04).
               10  LK-UCOP-6-OR-O-LCTN             PIC  X(01).
               10  LK-UCOP-TYPE-ENTRY-CODE         PIC  X(02).
               10  LK-ERROR-SEVERITY               PIC  X(01).
               10  LK-ERROR-TABLE                  OCCURS 6 TIMES.
                   15  LK-ERROR-NUMBER             PIC  9(06).
                   15  LK-ERROR-MESSAGE-TEXT       PIC  X(40).
           05  LK-FILE-OPERATION                   PIC  X(01).
               88  LK-FILE-OPEN                    VALUE '1'.
               88  LK-FILE-READ                    VALUE '2'.
               88  LK-FILE-CLOSE                   VALUE '3'.
