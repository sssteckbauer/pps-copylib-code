000100******************************************************************36090513
000200*  COPYMEMBER: CPWSXLVA                                          *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____       *36090513
000400*  NAME:__PXP___________  CREATION DATE:      __11/05/90__       *36090513
000500*  DESCRIPTION:                                                  *36090513
000600*    WS ARRAYS TO REPLACE XEDB SEGMENTS 8000, 8100-8300          *36090513
000700*    LOADED BY   PPLVAUTL SUBROUTINE CREATED FOR DB2 EDB         *36090513
000800*    CONVERSION                                                  *36090513
000810*                                                                *36090513
000811* ****************************************************************36090513
000900******************************************************************CPWSXLVA
000901* *  N.B. DATE FIELDS IN THIS ARRAY ARE NOT IN ISO FORM.         *CPWSXLVA
000902******************************************************************CPWSXLVA
001000*    COPYID=CPWSXLVA                                              CPWSXLVA
001010     05  LVPLANKEYS             OCCURS 7.                         CPWSXLVA
001100         10  LVPLAN-KEY.                                          CPWSXLVA
001200             15  TITLE-TYP                 PICTURE X.             CPWSXLVA
001300             15  TUC                       PICTURE XX.            CPWSXLVA
001400             15  AREP                      PICTURE X.             CPWSXLVA
001500             15  SHC                       PICTURE X.             CPWSXLVA
001600             15  DUC                       PICTURE X.             CPWSXLVA
001700         10  LVPLAN-CODE PICTURE X.                               CPWSXLVA
001800         10  LVPLAN-STARTDATE PICTURE S9(07) COMP-3.              CPWSXLVA
001900     05  ACRU-PER-INFO            OCCURS 13.                      CPWSXLVA
002000         10  ACRUPER-ENDDATE PICTURE S9(07) COMP-3.               CPWSXLVA
002100         10  ACRUPER-CYCLETYPE PICTURE X.                         CPWSXLVA
002200             88  ACRUPER-CYCLETYPE-ACRU-PER-BI VALUE 'B'.         CPWSXLVA
002300             88  ACRUPER-CYCLETYPE-ACRU-PER-MON VALUE 'M'.        CPWSXLVA
002400         10  ACRUPER-BYPASS PICTURE X.                            CPWSXLVA
002500             88  ACRUPER-BYPASS-ACRU-PER-BYPASS                   CPWSXLVA
002600                                                      VALUE 'X'.  CPWSXLVA
002700             88  ACRUPER-BYPASS-ACRU-PER-BYPAS                    CPWSXLVA
002800                                                      VALUE ' '.  CPWSXLVA
002900         10  ACRUPER-VAC-LOST                                     CPWSXLVA
003000                                         PICTURE S9(3)V99 COMP-3. CPWSXLVA
003100         10  ACRUPER-SICK-LOST                                    CPWSXLVA
003200                                         PICTURE S9(3)V99 COMP-3. CPWSXLVA
003300         10  ACRUPER-PTO-LOST                                     CPWSXLVA
003400                                         PICTURE S9(3)V99 COMP-3. CPWSXLVA
003500*                                                                 CPWSXLVA
003600*                                                                 CPWSXLVA
003700******************************************************************CPWSXLVA
003800*   L E A V E   H I S T O R I C A L   E N T R I E S               CPWSXLVA
003900*                  S E G M E N T   8 1 0 0   T H R U   8 5 0 0    CPWSXLVA
004000******************************************************************CPWSXLVA
004100*                                                                 CPWSXLVA
004200     05  LVH-SEG           OCCURS  5.                             CPWSXLVA
004300         10  LV-HIST-ACRU-PER       OCCURS 3.                     CPWSXLVA
004400             15  LV-HIST-ENTRY          OCCURS 4.                 CPWSXLVA
004500                 20  ACRUHIST-KEY PICTURE X(6).                   CPWSXLVA
004600                 20  ACRUHIST-PLAN PICTURE X.                     CPWSXLVA
004700                 20  ACRUHIST-HOURS                               CPWSXLVA
004800                                         PICTURE S9(3)V99 COMP-3. CPWSXLVA
004900                 20  ACRUHIST-HRSCODE PICTURE X.                  CPWSXLVA
005000                 20  ACRUHIST-VACHRS                              CPWSXLVA
005100                                         PICTURE S9(3)V99 COMP-3. CPWSXLVA
005200                 20  ACRUHIST-SICKHRSU                            CPWSXLVA
005300                                         PICTURE S9(3)V99 COMP-3. CPWSXLVA
005400                 20  ACRUHIST-PTOHRS                              CPWSXLVA
005500                                         PICTURE S9(3)V99 COMP-3. CPWSXLVA
