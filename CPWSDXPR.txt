000100**************************************************************/   36520886
000200*  COPYMEMBER: CPWSDXPR                                      */   36520886
000300*  RELEASE: ___0886______ SERVICE REQUEST(S): _____3652____  */   36520886
000400*  NAME:_______JLT_______ MODIFICATION DATE:  __03/30/94____ */   36520886
000500*  DESCRIPTION:                                              */   36520886
000600*    EXTERNAL LINKAGE INTERFACE USED BY PPDXPRPG AND ITS     */   36520886
000700*    ASSOCIATED CALLING MODULES (PPP750 & PPWIDOC).          */   36520886
000800**************************************************************/   36520886
000900*01* CPWSDXPR      EXTERNAL.                                      CPWSDXPR
001000     05  CPWSDXPR-EXT-AREA                     PIC X(1024).       CPWSDXPR
001100     05  CPWSDXPR-EXT-DATA REDEFINES CPWSDXPR-EXT-AREA.           CPWSDXPR
001200         10  CPWSDXPR-EMPLOYEE-ID                  PIC X(09).     CPWSDXPR
001300         10  CPWSDXPR-ACAD-APPTS-FOUND-IND     PIC X(01).         CPWSDXPR
001400             88  CPWSDXPR-ACAD-APPTS-FOUND   VALUE 'Y'.           CPWSDXPR
001500             88  CPWSDXPR-NO-ACAD-APPTS      VALUE 'N'.           CPWSDXPR
001600         10  CPWSDXPR-STAFF-APPTS-FOUND-IND    PIC X(01).         CPWSDXPR
001700             88  CPWSDXPR-STAFF-APPTS-FOUND  VALUE 'Y'.           CPWSDXPR
001800             88  CPWSDXPR-NO-STAFF-APPTS     VALUE 'N'.           CPWSDXPR
001900         10  CPWSDXPR-STUDENT-IND              PIC X(01).         CPWSDXPR
002000             88  CPWSDXPR-IS-STUDENT         VALUE 'Y'.           CPWSDXPR
002100             88  CPWSDXPR-NOT-STUDENT        VALUE 'N'.           CPWSDXPR
002200         10  CPWSDXPR-RETURN-CODE              PIC S9(04) COMP.   CPWSDXPR
002300             88 CPWSDXPR-RETURN-OK          VALUE ZERO.           CPWSDXPR
002400             88 CPWSDXPR-EMPL-NOT-FOUND     VALUE +100.           CPWSDXPR
002500             88 CPWSDXPR-SQL-ERROR          VALUE +101.           CPWSDXPR
002600             88 CPWSDXPR-PPPERUTL-ERROR     VALUE +102.           CPWSDXPR
002700         10  FILLER                            PIC X(1010).       CPWSDXPR
