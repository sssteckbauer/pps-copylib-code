000000**************************************************************/   16110865
000001*  COPYMEMBER: CPWSXGTZ                                      */   16110865
000002*  RELEASE: ___0865______ SERVICE REQUEST(S): ____11611____  */   16110865
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___12/13/93__  */   16110865
000004*  DESCRIPTION:                                              */   16110865
000005*  INCREASE GTN ENTRIES FROM  300 TO 999.                    */   16110865
000006*                                                            */   16110865
000007**************************************************************/   16110865
000100**************************************************************/   36220651
000200*  COPYMEMBER: CPWSXGTZ                                      */   36220651
000300*  REL#: ____0651______   SERVICE REQUEST NO(S)___3622_______*/   36220651
000400*  NAME ______KXK______   MODIFICATION DATE ____04/13/92_____*/   36220651
000500*  DESCRIPTION                                               */   36220651
000600*   - ADDED NEW INDICATOR (ICED) FOR COVERAGE EFFECTIVE DATE */   36220651
000700**************************************************************/   36220651
000800**************************************************************/   36100633
000900*  COPYMEMBER: CPWSXGTZ                                      */   36100633
001000*  REL#: _____0633_____   SERVICE REQUEST NO(S)___3610_______*/   36100633
001100*  NAME ______SRS______   MODIFICATION DATE ____07/31/91_____*/   36100633
001200*  DESCRIPTION                                               */   36100633
001300*   - ADDED NEW OVERRIDE DEDUCTION FOR ORCA PROCESSING       */   36100633
001400**************************************************************/   36100633
000100**************************************************************/   36190540
000200*  COPYMEMBER: CPWSXGTZ                                      */   36190540
000300*  REL#: 0540 REF: ____   SERVICE REQUEST NO(S)___3619_______*/   36190540
000400*  NAME ______JAG______   MODIFICATION DATE ____02/22/91_____*/   36190540
000500*  DESCRIPTION                                               */   36190540
000600*   - ADDED NEW INDICATORS FOR ON-LINE PRESENTATION OF GTN   */   36190540
000700*     NUMBERS ON IGTN, IDED, IRTR, AND IRET SCREENS.         */   36190540
000800**************************************************************/   36190540
000900**************************************************************/   40290540
001000*  PROGRAM: CPWSXGTZ                                         */   40290540
001100*  RELEASE: ___0540______ SERVICE REQUEST(S): _____4029____  */   40290540
001200*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___01/28/91__  */   40290540
001300*  DESCRIPTION:                                              */   40290540
001400*  - ADDED XGTZ-SET-INDICATOR                                */   40290540
001500*                                                            */   40290540
001600**************************************************************/   40290540
000100**************************************************************/   36030454
000200*  COPYMEMBER: CPWSXGTZ                                      */   36030454
000250*  RELEASE#__0454___  REF: ____  SERVICE REQUEST __3603__    */   36030454
000400*  NAME __CAS NAMOCOT__   MODIFICATION DATE ____11/20/89_____*/   36030454
000500*  DESCRIPTION                                               */   36030454
000600*   - COPIED FROM CPWSXGTN- DATA NAMES CHANGED TO PREFIX OF  */   36030454
000700*     XGTZ FROM XGTA, AND ARRAY OF 300 CREATED AS GROUP ITEM.*/   36030454
000900**************************************************************/   36030454
004400**************************************************************/   CPWSXGTZ
004500*    COPYID=CPWSXGTZ                                              CPWSXGTZ
004600*01  XGTZ-GROSS-TO-NET-RECORD.                                    CPWSXGTZ
004700*--------------------------------------------------------------   CPWSXGTZ
004800*               G R O S S - T O - N E T  R E C O R D          *   CPWSXGTZ
004900*--------------------------------------------------------------   CPWSXGTZ
004500***03  XGTZ-ITEMS OCCURS 300 TIMES DEPENDING ON IDC-MAX-NO-GTN.   16110865
004510   03  XGTZ-ITEMS OCCURS 999 TIMES DEPENDING ON IDC-MAX-NO-GTN.   16110865
005000     05  XGTZ-DELETE                 PIC X.                       CPWSXGTZ
005100     05  XGTZ-KEY.                                                CPWSXGTZ
005200         10  XGTZ-KEY-CONSTANT       PIC X(6).                    CPWSXGTZ
005300         10  XGTZ-PRIORITY           PIC X(4).                    CPWSXGTZ
005400         10  XGTZ-DEDUCTION          PIC 9(3).                    CPWSXGTZ
005500     05  XGTZ-PROCESS-DATA.                                       CPWSXGTZ
005600         10  XGTZ-MATCH-ELMT         PIC 9(3).                    CPWSXGTZ
005700         10  XGTZ-DESC               PIC X(15).                   CPWSXGTZ
005800         10  XGTZ-TYPE               PIC X.                       CPWSXGTZ
005900         10  XGTZ-GROUP              PIC X.                       CPWSXGTZ
006000*            THE GROUP CODES ARE DEFINED IN INSTALLATION          CPWSXGTZ
006100*            CONSTANTS, COPY MEMBER CPWSXIC2.                     CPWSXGTZ
006200         10  XGTZ-SCHEDULES.                                      CPWSXGTZ
006300*             THESE CODES DEFAULT TO "A" (TAKE FOR ALL PERIODS)   CPWSXGTZ
006400*             WHEN NO CODE IS SPECIFIED.                          CPWSXGTZ
006500             15  XGTZ-SM-SCHED-CODE  PIC X.                       CPWSXGTZ
006600             15  XGTZ-BI-SCHED-CODE  PIC X.                       CPWSXGTZ
006700             15  XGTZ-MN-SCHED-CODE  PIC X.                       CPWSXGTZ
006800**                                                                CPWSXGTZ
006900         10  XGTZ-BALANCE-DATA.                                   CPWSXGTZ
007000             15  XGTZ-YTD            PIC X.                       CPWSXGTZ
007100*             VALUE IS 'Y' (MAINTAIN A YTD), 'P' (MAINTAIN YTD    CPWSXGTZ
007200*             AND PRINT ON CHECK STUB) OR BLANK.                  CPWSXGTZ
007300             15  XGTZ-SUSPENSE       PIC X.                       CPWSXGTZ
007400*                 VALUE IS "S" (SUSPEND), "R" (SUSPEND AS A       CPWSXGTZ
007500*                 RECEIVABLE), OR BLANK.                          CPWSXGTZ
007600             15  XGTZ-QTD            PIC X.                       CPWSXGTZ
007700*                 VALUE IS "Q" (MAINTAIN A QTD) OR BLANK.         CPWSXGTZ
007800             15  XGTZ-DECLIN-BAL     PIC X.                       CPWSXGTZ
007900*                 VALUE IS "D" (MAINTAIN A DECLINING BALANCE)     CPWSXGTZ
008000*                 OR BLANK.                                       CPWSXGTZ
008100             15  XGTZ-ETD            PIC X.                       CPWSXGTZ
008200*                 VALUE IS "E" (MAINTAIN AN EMPLOYMENT-TO-DATE    CPWSXGTZ
008300*                 BALANCE) OR BLANK.                              CPWSXGTZ
008400             15  XGTZ-FYTD           PIC X.                       CPWSXGTZ
008500*                 VALUE IS "F" (MAINTAIN A FISCAL YTD BALANCE)    CPWSXGTZ
008600*                 OR BLANK.                                       CPWSXGTZ
008700             15  XGTZ-USER           PIC X.                       CPWSXGTZ
008800*                 VALUE IS "U" (MAINTAIN A USER-CONTROLLED BALANCECPWSXGTZ
008900*                 REQUIRING A SPECIAL CALCULATION ROUTINE) OR     CPWSXGTZ
009000*                 BLANK.                                          CPWSXGTZ
009100         10  FILLER                  PIC XX.                      CPWSXGTZ
009200         10  XGTZ-FULL-PART          PIC X.                       CPWSXGTZ
009300*             VALUES ARE "F"(FULL) AND "P"(PARTIAL) DEDUCTION.    CPWSXGTZ
009400         10  XGTZ-PRT-YTD-ON-CK     PIC X.                        CPWSXGTZ
009500*             THIS FIELD IS FILLED WITH A "P" WHEN THE INPUT      CPWSXGTZ
009600*             VALUE OF XGTZ-YTD IS EQUAL TO "P."                  CPWSXGTZ
009700         10  XGTZ-USAGE              PIC X.                       CPWSXGTZ
009800*             VALUE IS "F", "1", "P", "2", "H", "3", OR BLANK.    CPWSXGTZ
009900*                       OR "B", "R", "T"                          CPWSXGTZ
010000         10  XGTZ-BASE               PIC X.                       CPWSXGTZ
010100*             VALUES ARE:   "T" -- TOTAL GROSS                    CPWSXGTZ
010200*                           "W" -- FWT GROSS                      CPWSXGTZ
010300*                           "F" -- FICA GROSS                     CPWSXGTZ
010400*                           "S" -- STATE GROSS                    CPWSXGTZ
010500*                           "R" -- RETIREMENT GROSS               CPWSXGTZ
010600*                           "U" -- UNEMPLOYMENT INS GROSS        *CPWSXGTZ
010700*                           "X" -- SPECIAL RETIREMENT GROSS      *CPWSXGTZ
010800*                                                                 CPWSXGTZ
010900*             THE CODES AND TYPES OF GROSSES SHOULD REMAIN        CPWSXGTZ
011000*             CONSISTENT WITH THOSE USED IN THE DESCRIPTION OF   *CPWSXGTZ
011100*             SERVICES TABLE.                                    *CPWSXGTZ
011200*                                                                 CPWSXGTZ
011300*             ADDITIONAL BASES ARE NECESSARY FOR USAGES "H" AND   CPWSXGTZ
011400*             "3" WHICH REPRESENT DEDUCTIONS CALCULATED AS RATE   CPWSXGTZ
011500*             TIMES HOURS WORKED.  VALUES FOR THESE ARE:          CPWSXGTZ
011600*                                                                 CPWSXGTZ
011700*                            "H" -- TOTAL HOURS WORKED            CPWSXGTZ
011800*                            "G" -- REGULAR HOURS WORKED          CPWSXGTZ
011900*                                                                 CPWSXGTZ
012000         10  XGTZ-REDUCE-BASE        PIC X.                       CPWSXGTZ
012100*             VALUE IS  BLANK OR "R."                             CPWSXGTZ
012200         10  XGTZ-SP-CALC-NO         PIC XX.                      CPWSXGTZ
012300         10  XGTZ-SP-CALC-IND        PIC X.                       CPWSXGTZ
012400*             VALUES ARE "M"--MANDATORY AND "C"--CONDITIONAL.     CPWSXGTZ
012500         10  XGTZ-SP-UPDATE-NO       PIC XX.                      CPWSXGTZ
012600         10  XGTZ-STATUS             PIC X.                       CPWSXGTZ
012700         10  XGTZ-STOP-AT-TERM       PIC X.                       CPWSXGTZ
012800*             VALUES ARE "S" (STOP) OR BLANK (TAKE).              CPWSXGTZ
012900     05  XGTZ-VALU-RNG-EDIT          PIC X.                       CPWSXGTZ
013000     05  XGTZ-ACCEPTBL-VALUES.                                    CPWSXGTZ
013100         10  XGTZ-VALUE1             PIC 9(5)V99.                 CPWSXGTZ
013200         10  XGTZ-VALUE2             PIC 9(5)V99.                 CPWSXGTZ
013300     05  XGTZ-CON-EDIT               PIC X.                       CPWSXGTZ
013400     05  XGTZ-LIABILITY.                                          CPWSXGTZ
013500         10  XGTZ-DEDUCTION-CODE     PIC X(02).                   CPWSXGTZ
013600         10  FILLER                  PIC X(2).                    CPWSXGTZ
013700         10  XGTZ-LIABILITY-ACCT     PIC X(6).                    CPWSXGTZ
013800     05  XGTZ-RECEIVABLE-ACCT        PIC X(6).                    CPWSXGTZ
013900     05  XGTZ-PREPAY-ACCT            PIC X(6).                    CPWSXGTZ
014000     SKIP1                                                        CPWSXGTZ
014100     05  XGTZ-BEN-CODE               PIC X(1).                    CPWSXGTZ
014200     05  XGTZ-REDUCTION-INDS.                                     CPWSXGTZ
014300         10  XGTZ-REDUCTION-FWT      PIC X.                       CPWSXGTZ
014400         10  XGTZ-REDUCTION-SWT      PIC X.                       CPWSXGTZ
014500         10  XGTZ-REDUCTION-FICA     PIC X.                       CPWSXGTZ
014600         10  FILLER                  PIC X.                       CPWSXGTZ
014300*****                                                          CD 16110865
014800     05  XGTZ-COLL-BARGN-ELIG-LEVEL-IND  PIC X(01).               CPWSXGTZ
014900****     VALUES:                                                  CPWSXGTZ
015000****     BLANK - NONE(NO COLLECTIVE BARGAINING ELIGIBILITY        CPWSXGTZ
015100****                                             REQUIREMENT)     CPWSXGTZ
015200****       1   - EMPLOYEE UNIT CODE, EMPLOYEE REPRESENTATION CODE CPWSXGTZ
015300****       2   - TITLE UNIT CODE, APPOINTMENT REPRESENTATION CODE CPWSXGTZ
015400****       3   - TITLE UNIT CODE, APPOINTMENT REPRESENTATION CODE,CPWSXGTZ
015500****             TITLE SPECIAL HANDLING CODE                      CPWSXGTZ
015600****       4   - TITLE UNIT CODE, APPOINTMENT REPRESENTATION CODE,CPWSXGTZ
015700****             DISTRIBUTION UNIT CODE                           CPWSXGTZ
015800****       5   - TITLE UNIT CODE, APPOINTMENT REPRESENTATION CODE,CPWSXGTZ
015900****             TITLE SPECIAL HANDLING CODE, DISTRIBUTION UNIT   CPWSXGTZ
016000****             CODE                                             CPWSXGTZ
015700***********                                                    CD 16110865
016200     05  XGTZ-MAX-AMOUNT             PIC 9(5)V99.                 CPWSXGTZ
015900***********                                                    CD 16110865
016400     05  XGTZ-COLL-BARG-BEHAVIOR-CODE  PIC X(01).                 CPWSXGTZ
016500****     VALUES:                                                  CPWSXGTZ
016600****     BLANK - NONE(NO COLLECTIVE BARGAINING ELIGIBILITY        CPWSXGTZ
016700****                                             REQUIREMENT)     CPWSXGTZ
016800****       1   - ENROLL IF INELIGIBLE, DO NOT DE-ENROLL           CPWSXGTZ
016900****       2   - DO NOT ENROLL IF INELIGIBLE, DE-ENROLL IF INELIG CPWSXGTZ
017000****             IBLE                                             CPWSXGTZ
017100****       3   - DO NOT ENROLL IF INELIGIBLE, DO NOT DE-ENROLL IF CPWSXGTZ
017200****             INELIGIBLE                                       CPWSXGTZ
017300     05  XGTZ-BENEFIT-TYPE           PIC X(01).                   CPWSXGTZ
017400     05  XGTZ-BENEFIT-PLAN           PIC X(02).                   CPWSXGTZ
017100***********                                                    CD 16110865
015800     05  XGTZ-SET-INDICATOR          PIC X(01).                   40290540
015900****                                                              36190540
016000****  THE FOLLOWING FOUR INDICATORS ARE USED TO DETERMINE WHICH   36190540
016100****  GTN NUMBERS ARE TO APPEAR ON ON-LINE SCREENS.               36190540
016200****                                                              36190540
016300     05  XGTZ-IGTN-INDICATOR         PIC X(01).                   36190540
016400     05  XGTZ-IDED-INDICATOR         PIC X(01).                   36190540
016500     05  XGTZ-IRTR-INDICATOR         PIC X(01).                   36190540
016600     05  XGTZ-IRET-INDICATOR         PIC X(01).                   36190540
018100***********                                                    CD 16110865
018500     05  XGTZ-LIABILITY-LOC          PIC X(01).                   36220651
018600     05  XGTZ-LIABILITY-FUND         PIC X(05).                   36220651
018700     05  XGTZ-LIABILITY-SUB          PIC X(01).                   36220651
018800     05  XGTZ-LIABILITY-OBJ          PIC X(04).                   36220651
018900     05  XGTZ-OVRD-DED-X             PIC X(03).                   36100633
019000     05  XGTZ-OVRD-DED            REDEFINES                       36100633
019100         XGTZ-OVRD-DED-X             PIC 9(03).                   36100633
019200     05  XGTZ-ICED-INDICATOR         PIC X(01).                   36220651
019300     05  FILLER                      PIC X(84).                   36220651
019400***********                                                    CD 16110865
017600     SKIP1                                                        CPWSXGTZ
017700     05  XGTZ-LAST-UPDT-INFO.                                     CPWSXGTZ
017800         10  XGTZ-LAST-UPDT-ACTION   PIC X.                       CPWSXGTZ
017900         10  XGTZ-LAST-UPDT-DATE     PIC X(6).                    CPWSXGTZ
018000     SKIP3                                                        CPWSXGTZ
