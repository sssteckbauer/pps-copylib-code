000000**************************************************************/   48441325
000001*  COPYMEMBER: CPWSEHTX                                      */   48441325
000002*  RELEASE: ___1325______ SERVICE REQUEST(S): ____14844____  */   48441325
000003*  NAME:_______QUAN______ CREATION DATE:      ___11/09/00__  */   48441325
000004*  DESCRIPTION:                                              */   48441325
000005*  - TAX TRANSACTION ARRAY FOR HISTORY HTX TABLE (PPPHTX)   */    48441325
000006*    ON HDB DATABASE.                                       */    48441325
000008**************************************************************/   48441325
001000*    COPY MEMBER USED TO STORE THE HISTORY TAX TRANSACTION TABLE *CPWSEHTX
001300******************************************************************CPWSEHTX
001400*COPYID=CPWSEHTX                                                  CPWSEHTX
001500*                                                                 CPWSEHTX
001600*01  HTX-TRANSACION-ENTRIES-ARRAY    EXTERNAL.                    CPWSEHTX
001601     05  HTX-ARRAY-AREA               PIC X(32550).               CPWSEHTX
001602     05  FILLER REDEFINES HTX-ARRAY-AREA.                         CPWSEHTX
001700         07  HTX-ARRAY-ENTRY               OCCURS 150.            CPWSEHTX
001800             10  HTXA-KEY.                                        CPWSEHTX
001810                 15  EMPLID                PIC X(09).             CPWSEHTX
001900                 15  ITERATION-NUMBER      PIC X(03).             CPWSEHTX
002000                 15  SYSTEM-ENTRY-DATE     PIC X(10).             CPWSEHTX
002002                 15  SYSTEM-ENTRY-TIME     PIC X(08).             CPWSEHTX
002003             10  DELETE-FLAG               PIC X(01).             CPWSEHTX
002003             10  ERH-INCORRECT             PIC X(01).             CPWSEHTX
002004             10  TAX-PROCESSOR-ID          PIC X(25).             CPWSEHTX
002005             10  TAX-PROCESSOR-ID-C        PIC X(01).             CPWSEHTX
002006             10  W4-PROCESS-DATE           PIC X(10).             CPWSEHTX
002007             10  W4-PROCESS-DATE-C         PIC X(01).             CPWSEHTX
002008             10  DE4-PROCESS-DATE          PIC X(10).             CPWSEHTX
002009             10  DE4-PROCESS-DATE-C        PIC X(01).             CPWSEHTX
002010             10  FEDTAX-MARITAL            PIC X(01).             CPWSEHTX
002020             10  FEDTAX-MARITAL-C          PIC X(01).             CPWSEHTX
002030             10  FEDTAX-EXEMPT             PIC S9(04) USAGE COMP. CPWSEHTX
002040             10  FEDTAX-EXEMPT-C           PIC X(01).             CPWSEHTX
002041             10  MAXFED-EXEMPT             PIC S9(04) USAGE COMP. CPWSEHTX
002042             10  MAXFED-EXEMPT-C           PIC X(01).             CPWSEHTX
002043             10  FEDTAX-ADDTL-AMT          PIC S9(07)V99          CPWSEHTX
002044                                                     USAGE COMP-3.CPWSEHTX
002045             10  FEDTAX-ADDTL-AMT-C        PIC X(01).             CPWSEHTX
002046             10  FED-AT-NONRS-AMT          PIC S9(07)V99          CPWSEHTX
002047                                                     USAGE COMP-3.CPWSEHTX
002048             10  FED-AT-NONRS-AMT-C        PIC X(01).             CPWSEHTX
002049             10  STE-TAX-MARITAL           PIC X(01).             CPWSEHTX
002050             10  STE-TAX-MARITAL-C         PIC X(01).             CPWSEHTX
002051             10  STE-TAX-PERDED            PIC S9(04) USAGE COMP. CPWSEHTX
002052             10  STE-TAX-PERDED-C          PIC X(01).             CPWSEHTX
002053             10  STE-TAX-ITMDED            PIC S9(04) USAGE COMP. CPWSEHTX
002054             10  STE-TAX-ITMDED-C          PIC X(01).             CPWSEHTX
002055             10  STE-TAX-MAXEXMPT          PIC S9(04) USAGE COMP. CPWSEHTX
002056             10  STE-TAX-MAXEXMPT-C        PIC X(01).             CPWSEHTX
002057             10  STE-TAX-ADTL-AMT          PIC S9(07)V99          CPWSEHTX
002058                                                     USAGE COMP-3.CPWSEHTX
002059             10  STE-TAX-ADTL-AMT-C        PIC X(01).             CPWSEHTX
002059             10  HTX-FILLER                PIC X(100).            CPWSEHTX
002520***************     END OF SOURCE - CPWSEHTX     *****************CPWSEHTX
