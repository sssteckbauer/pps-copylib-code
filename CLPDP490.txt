000010*==========================================================%      DS795
000020*=    COPY MEMBER: CLPDP490                               =%      DS795
000030*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000040*=                                         CONVERSION     =%      DS795
000050*=    NAME: R. SWIDERSKI    MODIFICATION DATE: 04/25/94   =%      DS795
000060*=                                                        =%      DS795
000070*=    DESCRIPTION:                                        =%      DS795
000080*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000090*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
000091*==========================================================%      DS795
000100*                                                                 CLPDP490
000200*     CONTROL REPORT PPP490CR                                     CLPDP490
000300*                                                                 CLPDP490
000400*                                                                 CLPDP490
000410 DUMMY-SECTION SECTION.
000500 PRINT-CONTROL-REPORT.                                            CLPDP490
000600                                                                  CLPDP490
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP490
000800                                                                  CLPDP490
000900     MOVE 'PPP490CR'             TO CR-HL1-RPT.                   CLPDP490
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP490
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP490
001200                                                                  CLPDP490
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP490
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP490
001500                                                                  CLPDP490
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP490
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP490
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP490
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP490
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP490
002100                                                                  CLPDP490
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP490
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP490
002400                                                                  CLPDP490
002500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP490
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP490
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP490
002800                                                                  CLPDP490
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP490
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP490
003100                                                                  CLPDP490
003110*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
003120     COPY CPPDTIME.                                               DS795
003200     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP490
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP490
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP490
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP490
003700                                                                  CLPDP490
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP490
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP490
004000                                                                  CLPDP490
004100     MOVE 'FNLPAR.PREV'          TO CR-DL9-FILE.                  CLPDP490
004200     MOVE SO-READ                TO CR-DL9-ACTION.                CLPDP490
004300     MOVE PAR-CNT                TO CR-DL9-VALUE.                 CLPDP490
004400     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP490
004500     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP490
004600                                                                  CLPDP490
004700     MOVE 'TAPEFILE'             TO CR-DL9-FILE.                  CLPDP490
004800     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP490
004900     MOVE TAPE-RECS-WRITTEN      TO CR-DL9-VALUE.                 CLPDP490
005000     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP490
005100     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP490
005200                                                                  CLPDP490
005300     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP490
005400     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP490
005500                                                                  CLPDP490
005600     CLOSE CONTROLREPORT.                                         CLPDP490
317900                                                                  CLPDP490
