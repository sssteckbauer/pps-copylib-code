000101**************************************************************/   36430795
000200*  COPYMEMBER: CPWSELEM                                      */   36430795
000300*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____  */   36430795
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __07/15/93____ */   36430795
000500*  DESCRIPTION:                                              */   36430795
000600*  EXPAND OCCURENCE KEY TO 18 BYTES                          */   36430795
000800**************************************************************/   36430795
000100******************************************************************36190563
000200*  COPYMEMBER: CPWSELEM                                          *36190563
000300*  RELEASE: ____0563____  SERVICE REQUEST(S): ____3619____       *36190563
000400*  NAME:__C RIDLON______  CREATION DATE:      __04/29/91__       *36190563
000500*  DESCRIPTION:                                                  *36190563
000600*      USED BY ON-LINE PROGRAM PPWELEM                           *36190563
000700******************************************************************36190563
000800   05  WS-ELEM-TEST          PIC X(4).                            CPWSELEM
000900   05  WS-ELEM-TEST-NUM REDEFINES WS-ELEM-TEST PIC 9(4).          CPWSELEM
001000       88  WS-ACT-NUM        VALUES 0001.                         CPWSELEM
001100       88  WS-LAP-END-DT     VALUES 8022, 8028, 8034, 8040,       CPWSELEM
001200                                    8046, 8052, 8058, 8064,       CPWSELEM
001300                                    8070, 8076, 8082, 8088,       CPWSELEM
001400                                    8094.                         CPWSELEM
001500       88  WS-LPH-START-DT   VALUES 8003, 8006, 8009, 8012,       CPWSELEM
001600                                    8015, 8018, 8021.             CPWSELEM
001700       88  WS-APPT-RATE      VALUES                               CPWSELEM
001800                             2014, 2314, 2614,                    CPWSELEM
001900                             2914, 3214, 3514,                    CPWSELEM
002000                             3814, 4114, 4414.                    CPWSELEM
002100       88  WS-DIST-RATE      VALUES                               CPWSELEM
002200                 2055, 2085, 2125, 2155, 2185, 2225, 2255, 2285,  CPWSELEM
002300                 2355, 2385, 2425, 2455, 2485, 2525, 2555, 2585,  CPWSELEM
002400                 2655, 2685, 2725, 2755, 2785, 2825, 2855, 2885,  CPWSELEM
002500                 2955, 2985, 3025, 3055, 3085, 3125, 3155, 3185,  CPWSELEM
002600                 3255, 3285, 3325, 3355, 3385, 3425, 3455, 3485,  CPWSELEM
002700                 3555, 3585, 3625, 3655, 3685, 3725, 3755, 3785,  CPWSELEM
002800                 3855, 3885, 3925, 3955, 3985, 4025, 4055, 4085,  CPWSELEM
002900                 4155, 4185, 4225, 4255, 4285, 4325, 4355, 4385,  CPWSELEM
003000                 4455, 4485, 4525, 4555, 4585, 4625, 4655, 4685.  CPWSELEM
003100       88  WS-RATE-10        VALUES                               CPWSELEM
003200           2014, 2055, 2085, 2125, 2155, 2185, 2225, 2255, 2285.  CPWSELEM
003300       88  WS-RATE-20        VALUES                               CPWSELEM
003400           2314, 2355, 2385, 2425, 2455, 2485, 2525, 2555, 2585.  CPWSELEM
003500       88  WS-RATE-30        VALUES                               CPWSELEM
003600           2614, 2655, 2685, 2725, 2755, 2785, 2825, 2855, 2885.  CPWSELEM
003700       88  WS-RATE-40        VALUES                               CPWSELEM
003800           2914, 2955, 2985, 3025, 3055, 3085, 3125, 3155, 3185.  CPWSELEM
003900       88  WS-RATE-50        VALUES                               CPWSELEM
004000           3214, 3255, 3285, 3325, 3355, 3385, 3425, 3455, 3485.  CPWSELEM
004100       88  WS-RATE-60        VALUES                               CPWSELEM
004200           3514, 3555, 3585, 3625, 3655, 3685, 3725, 3755, 3785.  CPWSELEM
004300       88  WS-RATE-70        VALUES                               CPWSELEM
004400           3814, 3855, 3885, 3925, 3955, 3985, 4025, 4055, 4085.  CPWSELEM
004500       88  WS-RATE-80        VALUES                               CPWSELEM
004600           4114, 4155, 4185, 4225, 4255, 4285, 4325, 4355, 4385.  CPWSELEM
004700       88  WS-RATE-90        VALUES                               CPWSELEM
004800           4414, 4455, 4485, 4525, 4555, 4585, 4625, 4655, 4685.  CPWSELEM
004900       88  WS-APPT-NO        VALUES                               CPWSELEM
005000                             2001 THRU 2040,                      CPWSELEM
005100                             2301 THRU 2340,                      CPWSELEM
005200                             2601 THRU 2640,                      CPWSELEM
005300                             2901 THRU 2940,                      CPWSELEM
005400                             3201 THRU 3240,                      CPWSELEM
005500                             3501 THRU 3540,                      CPWSELEM
005600                             3801 THRU 3840,                      CPWSELEM
005700                             4101 THRU 4140,                      CPWSELEM
005800                             4401 THRU 4440.                      CPWSELEM
005900       88  WS-DIST-NO        VALUES                               CPWSELEM
006000                             2041 THRU 2065,                      CPWSELEM
006100                             2071 THRU 2095,                      CPWSELEM
006200                             2111 THRU 2135,                      CPWSELEM
006300                             2141 THRU 2165,                      CPWSELEM
006400                             2171 THRU 2195,                      CPWSELEM
006500                             2211 THRU 2235,                      CPWSELEM
006600                             2241 THRU 2265,                      CPWSELEM
006700                             2271 THRU 2295,                      CPWSELEM
006800                             2341 THRU 2365,                      CPWSELEM
006900                             2371 THRU 2395,                      CPWSELEM
007000                             2411 THRU 2435,                      CPWSELEM
007100                             2441 THRU 2465,                      CPWSELEM
007200                             2471 THRU 2495,                      CPWSELEM
007300                             2511 THRU 2535,                      CPWSELEM
007400                             2541 THRU 2565,                      CPWSELEM
007500                             2571 THRU 2595,                      CPWSELEM
007600                             2641 THRU 2665,                      CPWSELEM
007700                             2671 THRU 2695,                      CPWSELEM
007800                             2711 THRU 2735,                      CPWSELEM
007900                             2741 THRU 2765,                      CPWSELEM
008000                             2771 THRU 2795,                      CPWSELEM
008100                             2811 THRU 2835,                      CPWSELEM
008200                             2841 THRU 2865,                      CPWSELEM
008300                             2871 THRU 2895,                      CPWSELEM
008400                             2941 THRU 2965,                      CPWSELEM
008500                             2971 THRU 2995,                      CPWSELEM
008600                             3011 THRU 3035,                      CPWSELEM
008700                             3041 THRU 3065,                      CPWSELEM
008800                             3071 THRU 3095,                      CPWSELEM
008900                             3111 THRU 3135,                      CPWSELEM
009000                             3141 THRU 3165,                      CPWSELEM
009100                             3171 THRU 3195,                      CPWSELEM
009200                             3241 THRU 3265,                      CPWSELEM
009300                             3271 THRU 3295,                      CPWSELEM
009400                             3311 THRU 3335,                      CPWSELEM
009500                             3341 THRU 3365,                      CPWSELEM
009600                             3371 THRU 3395,                      CPWSELEM
009700                             3411 THRU 3435,                      CPWSELEM
009800                             3441 THRU 3465,                      CPWSELEM
009900                             3471 THRU 3495,                      CPWSELEM
010000                             3541 THRU 3565,                      CPWSELEM
010100                             3571 THRU 3595,                      CPWSELEM
010200                             3611 THRU 3635,                      CPWSELEM
010300                             3641 THRU 3665,                      CPWSELEM
010400                             3671 THRU 3695,                      CPWSELEM
010500                             3711 THRU 3735,                      CPWSELEM
010600                             3741 THRU 3765,                      CPWSELEM
010700                             3771 THRU 3795,                      CPWSELEM
010800                             3841 THRU 3865,                      CPWSELEM
010900                             3871 THRU 3895,                      CPWSELEM
011000                             3911 THRU 3935,                      CPWSELEM
011100                             3941 THRU 3965,                      CPWSELEM
011200                             3971 THRU 3995,                      CPWSELEM
011300                             4011 THRU 4035,                      CPWSELEM
011400                             4041 THRU 4065,                      CPWSELEM
011500                             4071 THRU 4095,                      CPWSELEM
011600                             4141 THRU 4165,                      CPWSELEM
011700                             4171 THRU 4195,                      CPWSELEM
011800                             4211 THRU 4235,                      CPWSELEM
011900                             4241 THRU 4265,                      CPWSELEM
012000                             4271 THRU 4295,                      CPWSELEM
012100                             4311 THRU 4335,                      CPWSELEM
012200                             4341 THRU 4365,                      CPWSELEM
012300                             4371 THRU 4395,                      CPWSELEM
012400                             4441 THRU 4465,                      CPWSELEM
012500                             4471 THRU 4495,                      CPWSELEM
012600                             4511 THRU 4535,                      CPWSELEM
012700                             4541 THRU 4565,                      CPWSELEM
012800                             4571 THRU 4595,                      CPWSELEM
012900                             4611 THRU 4635,                      CPWSELEM
013000                             4641 THRU 4665,                      CPWSELEM
013100                             4671 THRU 4695.                      CPWSELEM
013200       88  WS-APPT-10        VALUES                               CPWSELEM
013300                             2001 THRU 2040.                      CPWSELEM
013400       88  WS-APPT-20        VALUES                               CPWSELEM
013500                             2301 THRU 2340.                      CPWSELEM
013600       88  WS-APPT-30        VALUES                               CPWSELEM
013700                             2601 THRU 2640.                      CPWSELEM
013800       88  WS-APPT-40        VALUES                               CPWSELEM
013900                             2901 THRU 2940.                      CPWSELEM
014000       88  WS-APPT-50        VALUES                               CPWSELEM
014100                             3201 THRU 3240.                      CPWSELEM
014200       88  WS-APPT-60        VALUES                               CPWSELEM
014300                             3501 THRU 3540.                      CPWSELEM
014400       88  WS-APPT-70        VALUES                               CPWSELEM
014500                             3801 THRU 3840.                      CPWSELEM
014600       88  WS-APPT-80        VALUES                               CPWSELEM
014700                             4101 THRU 4140.                      CPWSELEM
014800       88  WS-APPT-90        VALUES                               CPWSELEM
014900                             4401 THRU 4440.                      CPWSELEM
015000       88  WS-DIST-11        VALUES                               CPWSELEM
015100                             2041 THRU 2065.                      CPWSELEM
015200       88  WS-DIST-12        VALUES                               CPWSELEM
015300                             2071 THRU 2095.                      CPWSELEM
015400       88  WS-DIST-13        VALUES                               CPWSELEM
015500                             2111 THRU 2135.                      CPWSELEM
015600       88  WS-DIST-14        VALUES                               CPWSELEM
015700                             2141 THRU 2165.                      CPWSELEM
015800       88  WS-DIST-15        VALUES                               CPWSELEM
015900                             2171 THRU 2195.                      CPWSELEM
016000       88  WS-DIST-16        VALUES                               CPWSELEM
016100                             2211 THRU 2235.                      CPWSELEM
016200       88  WS-DIST-17        VALUES                               CPWSELEM
016300                             2241 THRU 2265.                      CPWSELEM
016400       88  WS-DIST-18        VALUES                               CPWSELEM
016500                             2271 THRU 2295.                      CPWSELEM
016600       88  WS-DIST-21        VALUES                               CPWSELEM
016700                             2341 THRU 2365.                      CPWSELEM
016800       88  WS-DIST-22        VALUES                               CPWSELEM
016900                             2371 THRU 2395.                      CPWSELEM
017000       88  WS-DIST-23        VALUES                               CPWSELEM
017100                             2411 THRU 2435.                      CPWSELEM
017200       88  WS-DIST-24        VALUES                               CPWSELEM
017300                             2441 THRU 2465.                      CPWSELEM
017400       88  WS-DIST-25        VALUES                               CPWSELEM
017500                             2471 THRU 2495.                      CPWSELEM
017600       88  WS-DIST-26        VALUES                               CPWSELEM
017700                             2511 THRU 2535.                      CPWSELEM
017800       88  WS-DIST-27        VALUES                               CPWSELEM
017900                             2541 THRU 2565.                      CPWSELEM
018000       88  WS-DIST-28        VALUES                               CPWSELEM
018100                             2571 THRU 2595.                      CPWSELEM
018200       88  WS-DIST-31        VALUES                               CPWSELEM
018300                             2641 THRU 2665.                      CPWSELEM
018400       88  WS-DIST-32        VALUES                               CPWSELEM
018500                             2671 THRU 2695.                      CPWSELEM
018600       88  WS-DIST-33        VALUES                               CPWSELEM
018700                             2711 THRU 2735.                      CPWSELEM
018800       88  WS-DIST-34        VALUES                               CPWSELEM
018900                             2741 THRU 2765.                      CPWSELEM
019000       88  WS-DIST-35        VALUES                               CPWSELEM
019100                             2771 THRU 2795.                      CPWSELEM
019200       88  WS-DIST-36        VALUES                               CPWSELEM
019300                             2811 THRU 2835.                      CPWSELEM
019400       88  WS-DIST-37        VALUES                               CPWSELEM
019500                             2841 THRU 2865.                      CPWSELEM
019600       88  WS-DIST-38        VALUES                               CPWSELEM
019700                             2871 THRU 2895.                      CPWSELEM
019800       88  WS-DIST-41        VALUES                               CPWSELEM
019900                             2941 THRU 2965.                      CPWSELEM
020000       88  WS-DIST-42        VALUES                               CPWSELEM
020100                             2971 THRU 2995.                      CPWSELEM
020200       88  WS-DIST-43        VALUES                               CPWSELEM
020300                             3011 THRU 3035.                      CPWSELEM
020400       88  WS-DIST-44        VALUES                               CPWSELEM
020500                             3041 THRU 3065.                      CPWSELEM
020600       88  WS-DIST-45        VALUES                               CPWSELEM
020700                             3071 THRU 3095.                      CPWSELEM
020800       88  WS-DIST-46        VALUES                               CPWSELEM
020900                             3111 THRU 3135.                      CPWSELEM
021000       88  WS-DIST-47        VALUES                               CPWSELEM
021100                             3141 THRU 3165.                      CPWSELEM
021200       88  WS-DIST-48        VALUES                               CPWSELEM
021300                             3171 THRU 3195.                      CPWSELEM
021400       88  WS-DIST-51        VALUES                               CPWSELEM
021500                             3241 THRU 3265.                      CPWSELEM
021600       88  WS-DIST-52        VALUES                               CPWSELEM
021700                             3271 THRU 3295.                      CPWSELEM
021800       88  WS-DIST-53        VALUES                               CPWSELEM
021900                             3311 THRU 3335.                      CPWSELEM
022000       88  WS-DIST-54        VALUES                               CPWSELEM
022100                             3341 THRU 3365.                      CPWSELEM
022200       88  WS-DIST-55        VALUES                               CPWSELEM
022300                             3371 THRU 3395.                      CPWSELEM
022400       88  WS-DIST-56        VALUES                               CPWSELEM
022500                             3411 THRU 3435.                      CPWSELEM
022600       88  WS-DIST-57        VALUES                               CPWSELEM
022700                             3441 THRU 3465.                      CPWSELEM
022800       88  WS-DIST-58        VALUES                               CPWSELEM
022900                             3471 THRU 3495.                      CPWSELEM
023000       88  WS-DIST-61        VALUES                               CPWSELEM
023100                             3541 THRU 3565.                      CPWSELEM
023200       88  WS-DIST-62        VALUES                               CPWSELEM
023300                             3571 THRU 3595.                      CPWSELEM
023400       88  WS-DIST-63        VALUES                               CPWSELEM
023500                             3611 THRU 3635.                      CPWSELEM
023600       88  WS-DIST-64        VALUES                               CPWSELEM
023700                             3641 THRU 3665.                      CPWSELEM
023800       88  WS-DIST-65        VALUES                               CPWSELEM
023900                             3671 THRU 3695.                      CPWSELEM
024000       88  WS-DIST-66        VALUES                               CPWSELEM
024100                             3711 THRU 3735.                      CPWSELEM
024200       88  WS-DIST-67        VALUES                               CPWSELEM
024300                             3741 THRU 3765.                      CPWSELEM
024400       88  WS-DIST-68        VALUES                               CPWSELEM
024500                             3771 THRU 3795.                      CPWSELEM
024600       88  WS-DIST-71        VALUES                               CPWSELEM
024700                             3841 THRU 3865.                      CPWSELEM
024800       88  WS-DIST-72        VALUES                               CPWSELEM
024900                             3871 THRU 3895.                      CPWSELEM
025000       88  WS-DIST-73        VALUES                               CPWSELEM
025100                             3911 THRU 3935.                      CPWSELEM
025200       88  WS-DIST-74        VALUES                               CPWSELEM
025300                             3941 THRU 3965.                      CPWSELEM
025400       88  WS-DIST-75        VALUES                               CPWSELEM
025500                             3971 THRU 3995.                      CPWSELEM
025600       88  WS-DIST-76        VALUES                               CPWSELEM
025700                             4011 THRU 4035.                      CPWSELEM
025800       88  WS-DIST-77        VALUES                               CPWSELEM
025900                             4041 THRU 4065.                      CPWSELEM
026000       88  WS-DIST-78        VALUES                               CPWSELEM
026100                             4071 THRU 4095.                      CPWSELEM
026200       88  WS-DIST-81        VALUES                               CPWSELEM
026300                             4141 THRU 4165.                      CPWSELEM
026400       88  WS-DIST-82        VALUES                               CPWSELEM
026500                             4171 THRU 4195.                      CPWSELEM
026600       88  WS-DIST-83        VALUES                               CPWSELEM
026700                             4211 THRU 4235.                      CPWSELEM
026800       88  WS-DIST-84        VALUES                               CPWSELEM
026900                             4241 THRU 4265.                      CPWSELEM
027000       88  WS-DIST-85        VALUES                               CPWSELEM
027100                             4271 THRU 4295.                      CPWSELEM
027200       88  WS-DIST-86        VALUES                               CPWSELEM
027300                             4311 THRU 4335.                      CPWSELEM
027400       88  WS-DIST-87        VALUES                               CPWSELEM
027500                             4341 THRU 4365.                      CPWSELEM
027600       88  WS-DIST-88        VALUES                               CPWSELEM
027700                             4371 THRU 4395.                      CPWSELEM
027800       88  WS-DIST-91        VALUES                               CPWSELEM
027900                             4441 THRU 4465.                      CPWSELEM
028000       88  WS-DIST-92        VALUES                               CPWSELEM
028100                             4471 THRU 4495.                      CPWSELEM
028200       88  WS-DIST-93        VALUES                               CPWSELEM
028300                             4511 THRU 4535.                      CPWSELEM
028400       88  WS-DIST-94        VALUES                               CPWSELEM
028500                             4541 THRU 4565.                      CPWSELEM
028600       88  WS-DIST-95        VALUES                               CPWSELEM
028700                             4571 THRU 4595.                      CPWSELEM
028800       88  WS-DIST-96        VALUES                               CPWSELEM
028900                             4611 THRU 4635.                      CPWSELEM
029000       88  WS-DIST-97        VALUES                               CPWSELEM
029100                             4641 THRU 4665.                      CPWSELEM
029200       88  WS-DIST-98        VALUES                               CPWSELEM
029300                             4671 THRU 4695.                      CPWSELEM
029400   05  WS-NUMBER-SAVE REDEFINES WS-ELEM-TEST.                     CPWSELEM
029500       10  WS-NUMBER-PREF           PIC X.                        CPWSELEM
029600           88 WS-NUMBER-PREF-6      VALUE '6'.                    CPWSELEM
029700           88 WS-NUMBER-PREF-7      VALUE '7'.                    CPWSELEM
029800       10  WS-NUMBER-SUF            PIC X(3).                     CPWSELEM
029900   05  WS-KEY-SAVE.                                               CPWSELEM
030000       10  WS-KEY-PREF              PIC X.                        CPWSELEM
030100           88 WS-KEY-GTN-BAL        VALUE                         CPWSELEM
030200              'G', 'Y', 'S', 'Q', 'D', 'E', 'F', 'U'.             CPWSELEM
030300       10  WS-KEY-SUF               PIC X(3).                     CPWSELEM
030400           88 WS-KEY-GTN234-OK      VALUE SPACES, LOW-VALUES.     CPWSELEM
031300       10  FILLER                   PIC X(14).                    36430795
030500   05  WS-KEYOUT-SAVE REDEFINES WS-KEY-SAVE.                      CPWSELEM
030600       10  WS-KEYOUT-PREF           PIC X(3).                     CPWSELEM
030700       10  WS-KEYOUT-SUF            PIC X.                        CPWSELEM
030800   05  WS-KEY-AXD REDEFINES WS-KEY-SAVE.                          CPWSELEM
030900       10  WS-KEY-AXD1              PIC 9.                        CPWSELEM
031000           88 WS-KEY-APPT1-OK       VALUES 1 THRU 9.              CPWSELEM
031100       10  WS-KEY-AXD2              PIC 9.                        CPWSELEM
031200           88 WS-KEY-APPT2-OK       VALUE  ZERO.                  CPWSELEM
031300           88 WS-KEY-DIST2-OK       VALUES 1 THRU 8.              CPWSELEM
031400       10  WS-KEY-AXD34             PIC X(2).                     CPWSELEM
031500           88 WS-KEY-APPT34-OK      VALUES SPACES, LOW-VALUES.    CPWSELEM
031600   05  WS-KEY-ACT REDEFINES WS-KEY-SAVE.                          CPWSELEM
031700       10  WS-KEY-ACT12             PIC 99.                       CPWSELEM
032700           88 WS-KEY-ACT12-OK       VALUES 0 THRU 98.             36430795
032800*****      88 WS-KEY-ACT12-OK       VALUES 1 THRU 98.             36430795
031900       10  WS-KEY-ACT34             PIC 99.                       CPWSELEM
033000           88 WS-KEY-ACT34-OK       VALUES 0 THRU 99.             36430795
033100*****      88 WS-KEY-ACT34-OK       VALUES 1 THRU 99.             36430795
033200       10  WS-KEY-ACT56             PIC 99.                       36430795
033300           88 WS-KEY-ACT56-OK       VALUES 1 THRU 99.             36430795
