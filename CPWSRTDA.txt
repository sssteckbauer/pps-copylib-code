000100**************************************************************/   17810964
000200*  COPYMEMBER: CPWSRTDA                                      */   17810964
000300*  RELEASE: ___0964______ SERVICE REQUEST(S): ____11781____  */   17810964
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___02/23/95__  */   17810964
000500*  DESCRIPTION:                                              */   17810964
000600*  ADDED NEW ROUTINE TYPE 'S' FOR SPECIAL PROCESSES          */   17810964
000700*                                                            */   17810964
000800**************************************************************/   17810964
000102**************************************************************/   36430795
000103*  COPYMEMBER: CPWSRTDA                                      */   36430795
000104*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____  */   36430795
000105*  NAME:_______PXP_______ MODIFICATION DATE:  __07/15/93____ */   36430795
000106*  DESCRIPTION:                                              */   36430795
000107*  REMOVE UNUSED FILLER                                      */   36430795
000108**************************************************************/   36430795
000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWSXRTD                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*  - THIS COPYMEMBER DEFINES THE ARRAY FOR THE               *|*36410717
000900*|*        ROUTINE DEFINITION TABLE                            *|*36410717
001000*|**************************************************************|*36410717
001100*----------------------------------------------------------------*36410717
001200*                                                                 CPWSRTDA
001300***  COPYID=CPWSXRTD                                              CPWSRTDA
001400*                                                                 CPWSRTDA
001500*01  XRTD-ROUTINE-TABLE-ARRAY.                                    CPWSRTDA
001600******************************************************************CPWSRTDA
001700**  XRTD-RTA-TYPE HAS THE FOLLOWING VALUES FOR EACH LEVEL OF    **CPWSRTDA
001800**  HIGH ORDER OCCURS:                                          **CPWSRTDA
001900**     1 = M (MONTHLY MAINTENANCE)                              **CPWSRTDA
002000**     2 = Q (QUARTERLY MAINTENANCE)                            **CPWSRTDA
002100**     3 = Y (YEARLY MAINTENANCE)                               **CPWSRTDA
002200**     4 = F (FISCAL YEAR MAINTENANCE)                          **CPWSRTDA
002300**     5 = G (GROSS-TO-NET EDITS)                               **CPWSRTDA
002400**     6 = C (CONSISTENCY EDITS)                                **CPWSRTDA
002500**     7 = I (IMPLIED MAINTENANCE)                              **CPWSRTDA
002600**     8 = A (ACTION EDITS)                                     **CPWSRTDA
004200**     9 = S (SPECIAL PROCESSING MAINTENANCE)                   **17810964
002700******************************************************************CPWSRTDA
002800*                                                                 CPWSRTDA
004500*****05  XRTD-RTA-TYPE                 OCCURS 8 TIMES.            17810964
004600     05  XRTD-RTA-TYPE                 OCCURS 9 TIMES.            17810964
003000         10  XRTD-RTA-DATA             OCCURS 999 TIMES.          CPWSRTDA
      *****                                                          CD 17810964
003400             15  XRTD-STATUS           PIC X(01).                 CPWSRTDA
      *****                                                          CD 17810964
003900             15  XRTD-ROUTINE-NAME     PIC X(08).                 CPWSRTDA
004000             15  XRTD-VALID-ROUTINE-SW PIC X(01).                 CPWSRTDA
004100                 88  XRTD-VALID                  VALUE 'Y'.       CPWSRTDA
004200                 88  XRTD-NOT-VALID              VALUE 'N'.       CPWSRTDA
004300                 88  XRTD-STATUS-INVALID         VALUE 'S'.       CPWSRTDA
004400                 88  XRTD-DATE-INVALID           VALUE 'D'.       CPWSRTDA
004500*                                                                 CPWSRTDA
004600************  END OF COPYBOOK CPWSXRTD  **************************CPWSRTDA
