000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSXCRA                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*                                                            */   32021138
000006*  IMPLEMENTATION OF THE FLEXIBLE FULL ACCOUNTING UNIT (3202)*/   32021138
000007*                                                            */   32021138
000008**************************************************************/   32021138
000100**************************************************************/   30930413
000200*  COPY MODULE:  CPWSXCRA                                    */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000003*    COPYID=CPWSXCRA                                              CPWSXCRA
000002*01  XCRA-COST-TABLE.                                             30930413
000003*-----------------------------------------------------------------CPWSXCRA
000004*       R A N G E   C O S T I N G   T A B L E                     CPWSXCRA
000005*    TO BE USED AS A WORK AREA FOR CALLING OR AFTER BEING         CPWSXCRA
000006*    CALLED BY THE DATA BASE.  TABLE NO 19                        CPWSXCRA
000007*-----------------------------------------------------------------CPWSXCRA
000008     03  XCRA-DELETE                 PIC X.                       CPWSXCRA
000009     03  XCRA-KEY.                                                CPWSXCRA
000010         05  XCRA-KEY-FILL           PIC X(05).                   CPWSXCRA
000911****     05  XCRA-LOCATION           PIC X.                       32021138
000012         05  XCRA-ACTION             PIC X(02).                   CPWSXCRA
000914****     05  XCRA-FUND1              PIC X(5).                    32021138
000915         05  XCRA-FUND-GROUP-CODE    PIC X(04).                   32021138
000916**** 03  XCRA-FUND2                  PIC X(05).                   32021138
000917**** 03  XCRA-SUB-AREA.                                           32021138
000918****     05  FILLER                  PIC X.                       32021138
000919****     05  XCRA-SUB                PIC X.                       32021138
000930     03  XCRA-FAU                    PIC X(30).                   32021138
000020*                                                                 CPWSXCRA
