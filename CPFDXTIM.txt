000010*==========================================================%      UCSD0102
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0102
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD0102
000040*=                                                        =%      UCSD0102
000050*==========================================================%      UCSD0102
000060*==========================================================%      UCSD0102
000070*=    COPY MEMBER:  CPFDXTIM                              =%      UCSD0102
000080*=    CHANGE #UCSD0102      PROJ. REQUEST:  RELEASE 1138  =%      UCSD0102
000090*=    NAME:  G. CHIU        MODIFICATION DATE: 9/02/97    =%      UCSD0102
000091*=                                                        =%      UCSD0102
000092*=    DESCRIPTION:                                        =%      UCSD0102
000093*=    RETAIN THE OLD TIMEFILE FORMAT AND RECORD LENGTH.   =%      UCSD0102
000094*=                                                        =%      UCSD0102
000095*=                                                        =%      UCSD0102
000096*==========================================================%      UCSD0102
000060*==========================================================%      UCSD0023
000070*=    COPY MEMBER:  CPFDXTIM                              =%      UCSD0023
000080*=    CHANGE #UCSD0023      PROJ. REQUEST:  IBM OFFLOAD   =%      UCSD0023
000090*=    NAME:  J.E. MCCLANE   MODIFICATION DATE: 9/18/88    =%      UCSD0023
000091*=                                                        =%      UCSD0023
000092*=    DESCRIPTION:  EXPANDED INPUT RECORD LENGTH BY       =%      UCSD0023
000093*=    9  BYTES FOR USE BY THE TIMESHEET RECORD.           =%      UCSD0023
000094*=                                                        =%      UCSD0023
000095*=                                                        =%      UCSD0023
000096*==========================================================%      UCSD0023
000000**************************************************************/   32021138
000001*  COPYMEMBER: CPFDXTIM                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000007**************************************************************/   32021138
000097**************************************************************/   45020369
000098*  COPY MODULE:  CPFDXTIM                                    */   45020369
000099*  RELEASE # ___0369___   SERVICE REQUEST NO(S)____4502______*/   45020369
000400*  NAME ___C.JOHNEN____   MODIFICATION DATE ____08/02/88_____*/   45020369
000101*  DESCRIPTION                                               */   45020369
000102*  EXPANDED SIZE OF TIME FILE RECORD AS REQUIRED BY PPP305   */   45020369
000700*  (TIME SHEET REPRINT PROGRAM)                              */   45020369
000110**************************************************************/   13190159
000200*  COPY MODULE:  CPFDXTIM                                    */   13190159
000300*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
000400*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
000500*  DESCRIPTION                                               */   13190159
000600*  FOR PHASE 2 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190159
000700*  USER-CODED TRANSACTION INPUT RECORD LENGTHS WILL BE       */   13190159
000800*  EXPANDED BY 22 BYTES, AND RECORDS AND WORK AREAS          */   13190159
000900*  RELATING TO THE EDB FILE WILL BE EXPANDED AS WELL.        */   13190159
001000**************************************************************/   13190159
001100     SKIP2                                                        13190159
001200*    COPYID=CPFDXTIM                                              CPFDXTIM
002000*****                                                          CD 45020369
002100     BLOCK CONTAINS 0 RECORDS                                     45020369
001500*    RECORD CONTAINS 130 CHARACTERS                               45020369
002300*****RECORD CONTAINS 151 CHARACTERS                               32021138
002300*****RECORD CONTAINS 168 CHARACTERS                               UCSD0102
001510     RECORD CONTAINS 160 CHARACTERS                               UCSD0023
001600     LABEL RECORDS STANDARD.                                      CPFDXTIM
