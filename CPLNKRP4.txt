000000**************************************************************/   EFIX1049
000001*  COPYMEMBER: CPLNKRP4                                      */   EFIX1049
000002*  RELEASE: ___1049______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1049
000003*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___12/18/95__  */   EFIX1049
000004*  DESCRIPTION:                                              */   EFIX1049
000005*  - INCREASED FIELD LENGTHS OF RPT4-DOLLAR-ADJS-AUTO AND    */   EFIX1049
000006*    RPT4-DOLLAR-ADJS-CPA-IN FROM S9(7)V99 TO S9(9)V99.      */   EFIX1049
000007**************************************************************/   EFIX1049
000100**************************************************************/   36300699
000200*  COPYMENBER: CPLNKRP4                                      */   36300699
000300*  RELEASE: ____0699____  SERVICE REQUEST(S): ____3630____   */   36300699
000400*  NAME:____PXP___________MODIFICATION DATE:  __09/17/92__   */   36300699
000500*  DESCRIPTION:                                              */   36300699
000600*  - OCCURRENCE INCREASED TO ADD A NEW LINE TO RPT PPP3904   */   36300699
000700*  - ADD COUNTS FOR EDBCHANGE AND COM3940 FILES TO REPORT    */   36300699
000800**************************************************************/   36300699
000010******************************************************************37550521
000020*  COPY MEMBER: CPLNKRP4                                         *37550521
000030*  RELEASE # ___0521___   SERVICE REQUEST NO(S)___3755__________ *37550521
000040*  NAME __K.KELLER_____   CREATION DATE: _______11/26/90_____    *37550521
000050*  DESCRIPTION                                                   *37550521
000060*  - OCCURRENCE INCREASED TO ADD A NEW LINE TO RPT PPP3904       *37550521
000070******************************************************************37550521
000100******************************************************************36030473
000200*  COPY MEMBER: CPLNKRP4                                         *36030473
000300*  RELEASE # ___0473___   SERVICE REQUEST NO(S)___3603__________ *36030473
000400*  NAME __CAS NAMOCOT__   CREATION DATE: _______02/13/90_____    *36030473
000500*  DESCRIPTION                                                   *36030473
000600*     NEW LINKAGE ADDED                                          *36030473
000700*                                                                *36030473
000800******************************************************************36030473
000900*COPYID=CPLNKRP4                                                  CPLNKRP4
001000******************************************************************CPLNKRP4
001100*                        C P L N K R P 4                         *CPLNKRP4
001200******************************************************************CPLNKRP4
001300*                                                                *CPLNKRP4
001400*01  REPORT4-TOTALS.                                              CPLNKRP4
001500*                                                                 CPLNKRP4
001600******************************************************************CPLNKRP4
001700*      THE FOLLOWING FLAGS AND FIELDS ARE PASSED BY CALLER       *CPLNKRP4
001800******************************************************************CPLNKRP4
001900*                                                                 CPLNKRP4
002000     03  RPT4-SEG-WK-FL-CNT        PIC S9(7)      COMP-3.         CPLNKRP4
002100     03  RPT4-PAR-FL-CNT           PIC S9(7)      COMP-3.         CPLNKRP4
003700***  03  RPT4-DOLLAR-ADJS-AUTO     PIC S9(7)V99   COMP-3.         EFIX1049
003710     03  RPT4-DOLLAR-ADJS-AUTO     PIC S9(9)V99   COMP-3.         EFIX1049
003800***  03  RPT4-DOLLAR-ADJS-CPA-IN   PIC S9(7)V99   COMP-3.         EFIX1049
003810     03  RPT4-DOLLAR-ADJS-CPA-IN   PIC S9(9)V99   COMP-3.         EFIX1049
002400     03  RPT4-TABLE-MAX            PIC S9(4) COMP SYNC.           CPLNKRP4
002500     03  RPT4-DOLR-ADJ             PIC S9(4) COMP SYNC.           CPLNKRP4
002600     03  RPT4-LITS.                                               CPLNKRP4
002700         05  FILLER  PIC X(24).                                   CPLNKRP4
002800         05  FILLER  PIC X(24).                                   CPLNKRP4
002900         05  FILLER  PIC X(24).                                   CPLNKRP4
003000         05  FILLER  PIC X(24).                                   CPLNKRP4
003100         05  FILLER  PIC X(24).                                   CPLNKRP4
003200         05  FILLER  PIC X(24).                                   CPLNKRP4
003300         05  FILLER  PIC X(24).                                   CPLNKRP4
003400         05  FILLER  PIC X(24).                                   CPLNKRP4
003500         05  FILLER  PIC X(24).                                   CPLNKRP4
003600         05  FILLER  PIC X(24).                                   CPLNKRP4
003700         05  FILLER  PIC X(24).                                   CPLNKRP4
003800         05  FILLER  PIC X(24).                                   CPLNKRP4
003900         05  FILLER  PIC X(24).                                   CPLNKRP4
004000         05  FILLER  PIC X(24).                                   CPLNKRP4
004100         05  FILLER  PIC X(24).                                   CPLNKRP4
004200         05  FILLER  PIC X(24).                                   CPLNKRP4
004210         05  FILLER  PIC X(24).                                   37550521
005900         05  FILLER  PIC X(24).                                   36300699
006000         05  FILLER  PIC X(24).                                   36300699
004300     03  RPT4-LITS-INDS REDEFINES RPT4-LITS.                      CPLNKRP4
004400*********05  RPT4-LIT-IND       OCCURS 16.                        37550521
006300*****    05  RPT4-LIT-IND       OCCURS 17.                        36300699
006400         05  RPT4-LIT-IND       OCCURS 19.                        36300699
004500             07  RPT4-PRT-TRANS    PIC XX.                        CPLNKRP4
004600             07  RPT4-PRT-DESC     PIC X(20).                     CPLNKRP4
004700             07  RPT4-PRT-RCDS     PIC X.                         CPLNKRP4
004800             07  RPT4-PRT-DOLRS    PIC X.                         CPLNKRP4
004900*****03  RPT4-COUNTS OCCURS 16 TIMES.                             37550521
007000*****03  RPT4-COUNTS OCCURS 17 TIMES.                             36300699
007100     03  RPT4-COUNTS OCCURS 19 TIMES.                             36300699
005000         05  RPT4-RECORDS-OUT      PIC S9(7) COMP-3.              CPLNKRP4
005100         05  RPT4-DOLLARS-OUT      PIC S9(9)V9(6) COMP-3.         CPLNKRP4
