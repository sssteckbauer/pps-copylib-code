000000**************************************************************/   02381440
000001*  COPYMEMBER: CPWSXGCP                                      */   02381440
000002*  RELEASE: ___1440______ SERVICE REQUEST(S): 80238, 80251_  */   02381440
000003*  NAME:_BASKAR CHITRAVEL MODIFICATION DATE:  ___11/08/02__  */   02381440
000004*  DESCRIPTION:                                              */   02381440
000005*  - ADDED 88 LEVELS FOR HEALTH CARE REIMBURSEMENT ACCOUNT.  */   02381440
000006*    ALSO CHANGED VALUE OF 88 LEVEL FIELD XGCP-DEP-CARE-TD   */   02511440
000007*    FROM 225 TO 335 AS PER SR80251.                         */   02511440
000008**************************************************************/   02381440
000100**************************************************************/   16150894
000200*  COPYMEMBER: CPWSXGCP                                      */   16150894
000300*  RELEASE: ___0894______ SERVICE REQUEST(S): ____11615____  */   16150894
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___03/31/94__  */   16150894
000500*  DESCRIPTION:                                              */   16150894
000600*  - REMOVE UNUSED 88-LEVEL ENTRIES                          */   16150894
000700*                                                            */   16150894
000800**************************************************************/   16150894
000155**************************************************************/   06360698
000156*  COPYMEMBER: CPWSXGCP                                      */   06360698
000157*  RELEASE: ___0698______ SERVICE REQUEST(S): ____10636____  */   06360698
000158*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___09/11/92__  */   06360698
000159*  DESCRIPTION:                                              */   06360698
000160*  -  REMOVED  20 UNUSED FIELDS.                             */   06360698
000161*     XGCP-MUTUAL-FUND-1-FXD, XGCP-MUTUAL-FUND-1-PCT         */   06360698
000162*     XGCP-MUTUAL-FUND-2-FXD, XGCP-MUTUAL-FUND-2-PCT         */   06360698
000163*     XGCP-BENHAM-FUND, XGCP-SUP-SAV-TD,                     */   06360698
000164*     XGCP-SUP-SAV-NTD, XGCP-VAR-BND-TD,                     */   06360698
000165*     XGCP-VAR-BND-NTD, XGCP-VAR-EQTY-TD, XGCP-VAR-EQTY-NTD  */   06360698
000166*     XGCP-MULTI-ASSET-TD, XGCP-MULTI-ASSET-NTD,             */   06360698
000167*     XGCP-GUAR-INS-TD, XGCP-GUAR-INS-NTD,                   */   06360698
000168*     XGCP-MNY-MRKT-TD, XGCP-MNY-MRKT-NTD,                   */   06360698
000169*     XGCP-CALVERT-TD, XGCP-IRA-IDS,                         */   06360698
000170*     XGCP-IRA-WELLS.                                        */   06360698
000171*                                                            */   06360698
000172*  -  REMOVED PREVIOUS CD LINE                               */   06360698
000173*                                                            */   06360698
000174*  -  REMOVED PREVIOUS COMMENTED-OUT LINE, MARKED 'CD'       */   06360698
000175**************************************************************/   06360698
000100**************************************************************/   01700537
000200*  COPYMEMBER: CPWSXGCP                                      */   01700537
000300*  RELEASE: ____0537____  SERVICE REQUEST(S): ____0170____   */   01700537
000400*  NAME:    M. BAPTISTA_  MODIFICATION DATE:  __01/24/91__   */   01700537
000500*  DESCRIPTION:                                              */   01700537
000600*    - REMOVED 88-LEVEL XGCP-MED-PLUS.                       */   01700537
000700**************************************************************/   01700537
000800**************************************************************/   53590480
000900*  COPYMEMBER: CPWSXGCP                                      */   53590480
000300*  RELEASE: ____0480____  SERVICE REQUEST(S): ____5359____   */   53590480
000400*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __06/05/90__   */   53590480
000500*  DESCRIPTION:                                              */   53590480
000600*                                                            */   53590480
000700*    - ADDED 88 LEVELS FOR TAX-DEFERRED AND NON-TAX-DEFERRED */   53590480
000800*      MULTI-ASSET FUNDS                                     */   53590480
000900**************************************************************/   53590480
001000**************************************************************/   53010480
001100*  COPYMEMBER: CPWSXGCP                                      */   53010480
001200*  RELEASE: ____0480____  SERVICE REQUEST(S): ____5301____   */   53010480
001300*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __06/05/90__   */   53010480
001400*  DESCRIPTION:                                              */   53010480
001500*                                                            */   53010480
001600*    - DELETED CODE PREVIOUSLY COMMENTED-OUT AND MARKED 'CD' */   53010480
001700*    - ADDED 88 LEVEL FOR BENHAM FUND                        */   53010480
001800**************************************************************/   53010480
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXGCP                                      */   30930413
002430*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
002440*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
002450*  DESCRIPTION                                               */   30930413
002460*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
002470**************************************************************/   30930413
002500**************************************************************/   30450312
002600*  COPYID:   CPWSXGCP                                        */   30450312
002700*  RELEASE: ___0312___    SERVICE REQUEST:  _____3045______  */   30450312
002800*  NAME ___D. STORMAN__   MODIFICATION DATE ____08/24/87_____*/   30450312
002900*  DESCRIPTION                                               */   30450312
003000*                                                            */   30450312
003100*  ADDED 88-LEVEL TO XGCP-GTN-NUMBERS FOR MUTUAL-FUND-1 AND  */   30450312
003200*  MUTUAL-FUND-2.                                            */   30450312
003300**************************************************************/   30260288
003400*  COPYID:   CPWSXGCP                                        */   30260288
003500*  RELEASE: ___0288___    SERVICE REQUEST:  _____3026______  */   30260288
003600*  NAME __K.KELLER_____   MODIFICATION DATE ____04/09/87_____*/   30260288
003700*  DESCRIPTION                                               */   30260288
003800*  ADDED 88-LEVEL TO XGCP-GTN-NUMBER FOR UCRS SHORT TERM     */   30260288
003900*  LOAN AND UCRS LONG TERM LOAN                              */   30260288
004000**************************************************************/   30200268
004100*  COPYID:   CPWSXGCP                                        */   30200268
004200*  RELEASE: ___0268___    SERVICE REQUEST:  _____3020______  */   30200268
004300*  NAME ___DBS_________   MODIFICATION DATE ____01/15/87_____*/   30200268
004400*  DESCRIPTION                                               */   30200268
004500*                                                            */   30200268
004600*  ADDED 88-LEVEL TO XGCP-GTN-NUMBER FOR MED-PLUS.           */   30200268
004700**************************************************************/   13250220
004800*  COPYID:   CPWSXGCP                                        */   13250220
004900*  RLSE #___0220__ REF RLSE # _0191__ SERVICE REQ __1325_____*/   13250220
005000*  NAME ___SLB_________   MODIFICATION DATE ____06/30/86_____*/   13250220
005100*  DESCRIPTION                                               */   13250220
005200*                                                            */   13250220
005300*  CHANGED 292 TO 293 IN VALUE CLAUSE FOR CONDITION NAME     */   13250220
005400*  XGCP-MNY-MRKT-TD.                                         */   13250220
005500*                                                            */   13250220
005600**************************************************************/   13250220
005700     SKIP1                                                        13250220
005800**************************************************************/   13250202
005900*  COPYID:   CPWSXGCP                                        */   13250202
006000*  RLSE #___0202__ REF RLSE # 0191___ SERVICE REQ __1325_____*/   13250202
006100*  NAME ___SLB_________   MODIFICATION DATE ____04/04/86_____*/   13250202
006200*  DESCRIPTION                                               */   13250202
006300*  CHANGED TO REMOVE PICTURE CLAUSE FROM 01-LEVEL ENTRY      */   13250202
006400*  AND ADD 05-LEVEL DATA NAME.                               */   13250202
006500**************************************************************/   13250202
006600     SKIP1                                                        13250202
006700**************************************************************/   13250191
006800*  COPYID:   CPWSXGCP                                        */   13250191
006900*  RELEASE # __0191____   SERVICE REQUEST NO(S)___1325_______*/   13250191
007000*  NAME ___SLB_________   MODIFICATION DATE ____02/11/86_____*/   13250191
007100*  DESCRIPTION                                               */   13250191
007200*  COPYMEMBER CONTAINING GTN NUMBERS FOR VOLUNTARY SAVINGS   */   13250191
007300*  PROGRAMS AND EMPLOYEE ORGANIZATION DUES/INSURANCE         */   13250191
007400*  CAPTURED FOR FCP.                                         */   13250191
007500*                                                            */   13250191
007600*  THIS COPYMEMBER SHOULD BE KEPT UP-TO-DATE TO REFLECT      */   13250191
007700*  ACTUAL GTN NUMBERS USED BY A CAMPUS FOR THE DEDUCTIONS    */   13250191
007800*  LISTED IN THE 88-LEVEL ENTRIES.                           */   13250191
007900*                                                            */   13250191
008000**************************************************************/   13250191
008100*                                                                 13250191
008200*    COPYID=CPWSXGCP                                              CPWSXGCP
008300*                                                                 CPWSXGCP
008410*01  XGCP-DEDUCTION-NO.                                           30930413
008600     05  XGCP-GTN-NUMBER             PIC 9(03).                   13250202
012300*****    88  XGCP-EMP-ORG            VALUES 073 074 075 076       16150894
012400*****                                       077 080 082 083       16150894
012500*****                                       087 090 092 173       16150894
012600*****                                       174 181 182 183.      16150894
012610         88  XGCP-HCRA-TD            VALUE  338.                  02381440
012620         88  XGCP-DEP-CARE-TD        VALUE  335.                  02511440
012700*****                                                          CD 16150894
012800*****    88  XGCP-DEP-CARE-TD        VALUE  225.                  02511440
012900*****                                                          CD 16150894
011300         88  XGCP-SHORT-LOAN         VALUE  243.                  30260288
011400         88  XGCP-LONG-LOAN          VALUE  253.                  30260288
013200*****                                                          CD 16150894
011900     SKIP1                                                        CPWSXGCP
