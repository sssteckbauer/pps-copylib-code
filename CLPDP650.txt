000010*==========================================================%      UCSD0029
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0029
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD0029     =%      UCSD0029
000040*=                                                        =%      UCSD0029
000050*==========================================================%      UCSD0029
000060*==========================================================%      UCSD0029
000070*=    COPYMEMBER: CLPDP650                                =%      UCSD0029
000080*=    CHANGE # UCSD0029     PROJ. REQUEST: REL 449        =%      UCSD0029
000090*=    NAME: MARI MCGEE      MODIFICATION DATE: 03/11/90   =%      UCSD0029
000091*=                                                        =%      UCSD0029
000092*=    DESCRIPTION:                                        =%      UCSD0029
000093*=     DELETE UNUSED COUNTERS                             =%      UCSD0029
000095*=                                                        =%      UCSD0029
000096*==========================================================%      UCSD0029
000097*==========================================================%      DS795
000098*=    COPY MEMBER: CLPDP650                               =%      DS795
000099*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000100*=                                         CONVERSION     =%      DS795
000101*=    NAME: R. SWIDERSKI    MODIFICATION DATE: 04/25/94   =%      DS795
000102*=                                                        =%      DS795
000103*=    DESCRIPTION:                                        =%      DS795
000104*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000105*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
000106*==========================================================%      DS795
000100*                                                                 CLPDP650
000110*                                                                 CLPDP650
000200*      CONTROL REPORT PPP650CR                                    CLPDP650
000300*                                                                 CLPDP650
000400*                                                                 CLPDP650
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP650
000600                                                                  CLPDP650
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP650
000800                                                                  CLPDP650
000900     MOVE 'PPP650CR'             TO CR-HL1-RPT.                   CLPDP650
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP650
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP650
001200                                                                  CLPDP650
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP650
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP650
001500                                                                  CLPDP650
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP650
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP650
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP650
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP650
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP650
002100                                                                  CLPDP650
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP650
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP650
002400                                                                  CLPDP650
002500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP650
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP650
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP650
002800                                                                  CLPDP650
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP650
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP650
003100                                                                  CLPDP650
003110*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
003120     COPY CPPDTIME.                                               DS795
003200     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP650
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP650
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP650
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP650
003700                                                                  CLPDP650
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP650
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP650
004000*                                                                 CLPDP650
004100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP650
004200*                                                                 CLPDP650
004300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP650
004400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP650
004500*                                                                 CLPDP650
004510     MOVE 'EMPLOYEE'             TO CR-DL9-FILE.                  CLPDP650
004520     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP650
004530     MOVE EMPL-COUNT             TO CR-DL9-VALUE.                 CLPDP650
004540     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP650
004550     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP650
004560*                                                                 CLPDP650
004600*    MOVE 'ACTIVE  '             TO CR-DL9-FILE.                  UCSD0029
004700*    MOVE SO-READ                TO CR-DL9-ACTION.                UCSD0029
004800*    MOVE EMPL-COUNT-A           TO CR-DL9-VALUE.                 UCSD0029
004900*    MOVE CR-DET-LINE9           TO CONTROL-DATA.                 UCSD0029
005000*    WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 UCSD0029
005010*                                                                 UCSD0029
005020*    MOVE 'INACTIVE'             TO CR-DL9-FILE.                  UCSD0029
005030*    MOVE SO-READ                TO CR-DL9-ACTION.                UCSD0029
005040*    MOVE EMPL-COUNT-I           TO CR-DL9-VALUE.                 UCSD0029
005050*    MOVE CR-DET-LINE9           TO CONTROL-DATA.                 UCSD0029
005060*    WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 UCSD0029
005010*                                                                 CLPDP650
005200     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP650
005300     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP650
005400                                                                  CLPDP650
005500     CLOSE CONTROLREPORT.                                         CLPDP650
005600 PRINT-CONTROL-REPORT-EXIT.                                       CLPDP650
005700     EXIT.                                                        CLPDP650
