000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSLAFH                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    DCLGEN FOR THE PPPVLAFH VIEW                            */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSLAFH
001000* COBOL DECLARATION FOR TABLE PAYCXD.PPPVLAFH_LAFH               *CPWSLAFH
001100******************************************************************CPWSLAFH
001200*01  DCLPPPVLAFH-LAFH.                                            CPWSLAFH
001300     10 LAF-TITLE-TYPE       PIC X(1).                            CPWSLAFH
001400     10 LAF-TITLE-UNIT-CD    PIC X(2).                            CPWSLAFH
001500     10 LAF-AREP-CODE        PIC X(1).                            CPWSLAFH
001600     10 LAF-SHC              PIC X(1).                            CPWSLAFH
001700     10 LAF-DUC              PIC X(1).                            CPWSLAFH
001800     10 LAF-EFFECTIVE-DATE   PIC X(10).                           CPWSLAFH
001900     10 LAF-FUND-RANGE-LOW   PIC X(5).                            CPWSLAFH
002000     10 LAF-FUND-RANGE-HI    PIC X(5).                            CPWSLAFH
002100     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSLAFH
002200     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSLAFH
002300     10 LAF-UF-VAC           PIC S9V9999 USAGE COMP-3.            CPWSLAFH
002400     10 LAF-UF-SKL           PIC S9V9999 USAGE COMP-3.            CPWSLAFH
002500     10 LAF-UF-PTO           PIC S9V9999 USAGE COMP-3.            CPWSLAFH
002600     10 LAF-LV-RES-ACCT      PIC X(6).                            CPWSLAFH
002700     10 LAF-LAST-ACTION      PIC X(1).                            CPWSLAFH
002800     10 LAF-LAST-ACTION-DT   PIC X(10).                           CPWSLAFH
002900******************************************************************CPWSLAFH
003000* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 16      *CPWSLAFH
003100******************************************************************CPWSLAFH
