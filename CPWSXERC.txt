000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWSXERC                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    REWRITE FOR CONSOLIDATION OF MODIFICATIONS.             *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
000300*    COPYID=CPWSXERC                                              CPWSXERC
001200*01  X-ERROR-RECORD.                                              CPWSXERC
000300     05 X-ERROR-KEY19.                                            CPWSXERC
000400        10 X-ERROR-KEY15.                                         CPWSXERC
000500           15 X-ERROR-SS-NUM           PICTURE X(9).              CPWSXERC
001600           15 X-ERROR-EFF-DATE         PICTURE X(10).             CPWSXERC
000700        10 X-ERROR-FIELD-NUM.                                     CPWSXERC
000800           15 X-ERROR-SEGMENT          PICTURE 99 VALUE 00.       CPWSXERC
000900           15 X-ERROR-SER              PICTURE 99 VALUE 00.       CPWSXERC
002000     05 X-ERROR-COUNTER                PICTURE S9(7) COMP         CPWSXERC
001100                                       VALUE +0.                  CPWSXERC
002200     05  X-ERROR-SET-CNTRL-NUMX.                                  CPWSXERC
002300         10  X-ERROR-ACTION-SRT-KEY    PICTURE X(01).             CPWSXERC
002400         10  X-ERROR-ACTION-KEY        PICTURE X(02).             CPWSXERC
002500         10  FILLER                    PICTURE X(03).             CPWSXERC
001400     05 X-ERROR-ERROR-CODE.                                       CPWSXERC
001500        10 X-ERROR-ERROR-PROG-ID       PICTURE 99 VALUE 06.       CPWSXERC
001600        10 X-ERROR-ERROR-CODE-NO       PICTURE 999 VALUE 112.     CPWSXERC
001700     05 X-ERROR-SEVERITY               PICTURE X.                 CPWSXERC
001800     05 X-ERROR-BATCH-NO               PICTURE 9(3).              CPWSXERC
001900     05 X-ERROR-ORIGIN                 PICTURE XXX.               CPWSXERC
002000     05 X-ERROR-START-POS              PICTURE 999 VALUE 000.     CPWSXERC
002100     05 X-ERROR-LENGTH                 PICTURE 99 VALUE 05.       CPWSXERC
003400     05  X-ERROR-NO-TRIGGERS           PICTURE 9(04) COMP.        CPWSXERC
003500     05  X-ERROR-GTN-BAL-TYPE          PICTURE X(01).             CPWSXERC
002300     05 X-ERROR-DATA                   PICTURE X(30).             CPWSXERC
003700     05  X-ERROR-TRIGGER-DATA.                                    CPWSXERC
003800         10  X-ERROR-TRIGGERS          OCCURS 0 TO 60 DEPENDING   CPWSXERC
003900                                       ON X-ERROR-NO-TRIGGERS.    CPWSXERC
004000             15  X-ERROR-TRIGGER-TYPE  PIC X(01).                 CPWSXERC
004100             15  X-ERROR-TRIGGER-NUM   PIC 9(03).                 CPWSXERC
