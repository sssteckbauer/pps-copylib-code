000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXPAM                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000300*    COPYID=CPWSXPAM                                              CPWSXPAM
000200*01  PBP00-PARAM-SEGMENTS.                                        30930413
000300*-----------------------------------------------------------------CPWSXPAM
000400*              S Y S T E M  P A R A M E T E R S                  *CPWSXPAM
000500*-----------------------------------------------------------------CPWSXPAM
000600     03 PBP00-PARAM-DELETE       PICTURE X.                       CPWSXPAM
000700     03 PBP00-PARAM-KEY-AREA.                                     CPWSXPAM
000800         05  PBP00-PRM-VAL-CONSTANT  PIC X(11).                   CPWSXPAM
000900         05  PBP00-PRM-SEG-NO    PICTURE 9(2).                    CPWSXPAM
001000* THE KEY AREA OF THESE RECORDS CONTAINS '            2'          CPWSXPAM
001100*                                THROUGH '            9'          CPWSXPAM
001200* WITH DATA PARAMETERS (001 THRU 328) STORED 41 PER RECORD        CPWSXPAM
001300     03 PBP00-PARAMETERS-DATA    PICTURE S9(5)V9(4) COMP-3        CPWSXPAM
001400                                 OCCURS 41 TIMES.                 CPWSXPAM
001500     03 FILLER                   PICTURE X(5).                    CPWSXPAM
001600     SKIP1                                                        CPWSXPAM
