000010*==========================================================%      UCSD0006
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0006
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD0006
000040*=                                                        =%      UCSD0006
000050*==========================================================%      UCSD0006
000060*==========================================================%      UCSD0006
000070*=                                                        =%      UCSD0006
000080*=    COPY MEMBER: CLPDP711                               =%      UCSD0006
000090*=    CHANGE #0006          PROJ. REQUEST: CONTROL REPORT =%      UCSD0006
000091*=    NAME: MARI MCGEE      MODIFICATION DATE: 04/26/91   =%      UCSD0006
000092*=                                                        =%      UCSD0006
000093*=    DESCRIPTION                                         =%      UCSD0006
000094*=     ADD SECTION TO PRINT-CONTROL-REPORT.               =%      UCSD0006
000095*=                                                        =%      UCSD0006
000096*==========================================================%      UCSD0006
000100*                                                                 CLPDP711
000200*      CONTROL REPORT 'PPP711CR'                                  CLPDP711
000300*                                                                 CLPDP711
000400*                                                                 CLPDP711
000500*PRINT-CONTROL-REPORT.                                            UCSD0006
000510 PRINT-CONTROL-REPORT SECTION.                                    UCSD0006
000600                                                                  CLPDP711
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP711
000800                                                                  CLPDP711
000900     MOVE 'PPP711CR'             TO CR-HL1-RPT.                   CLPDP711
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP711
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP711
001200                                                                  CLPDP711
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP711
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP711
001500                                                                  CLPDP711
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP711
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP711
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP711
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP711
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP711
002100                                                                  CLPDP711
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP711
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP711
002400                                                                  CLPDP711
002500     MOVE XSCP-SPEC-CARD-REC     TO CR-DL5-CTL-CARD.              CLPDP711
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP711
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP711
002800                                                                  CLPDP711
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP711
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP711
003100                                                                  CLPDP711
003110     ACCEPT TIME-WORK-AREA FROM TIME.                             CLPDP711
003200*    MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP711
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP711
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP711
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP711
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP711
003700                                                                  CLPDP711
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP711
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP711
004000*                                                                 CLPDP711
004100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP711
004200*                                                                 CLPDP711
004300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP711
004400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP711
004500*                                                                 CLPDP711
004600     MOVE 'EMPLOYEE'             TO CR-DL9-FILE.                  CLPDP711
004700     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP711
004800     MOVE IO-FCP-EMPL            TO CR-DL9-VALUE.                 CLPDP711
004900     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP711
005000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP711
005100*                                                                 CLPDP711
005200*                                                                 CLPDP711
005300     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP711
005400     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP711
005500                                                                  CLPDP711
005600     CLOSE CONTROLREPORT.                                         CLPDP711
