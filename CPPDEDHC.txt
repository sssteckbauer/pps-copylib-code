000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPDEDHC                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_PHIL THOMPSON___ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  BASE FAU RELATED PROCEDURE CODE FOR ONLINE FUNCTION EDHC. */   32021138
000700*  THIS CODE ONLY WORKS WITH THE BASE PROGRAM PPWEDHC AND    */   32021138
000800*  BASE MAP PPEDHC0.                                         */   32021138
000900**************************************************************/   32021138
000900**************************************************************/   CPPDEDHC
066110 CPPD-INIT-ENTRY-FIELDS         SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
066600     MOVE UCCOMMON-ATR-ENTRY TO                                   CPPDEDHC
066600                          DE0230-XTAT-LOCA           (I)          CPPDEDHC
066700                          DE0230-XTAT-ACCTA          (I)          CPPDEDHC
066800                          DE0230-XTAT-COST-CENTERA   (I)          CPPDEDHC
066900                          DE0230-XTAT-FNDA           (I)          CPPDEDHC
067000                          DE0230-XTAT-PROJECT-CODEA  (I)          CPPDEDHC
067100                          DE0230-XTAT-SUBA           (I).         CPPDEDHC
066600     MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDHC
068900                          DE0230-XTAT-LOCC           (I)          CPPDEDHC
069000                          DE0230-XTAT-ACCTC          (I)          CPPDEDHC
069100                          DE0230-XTAT-COST-CENTERC   (I)          CPPDEDHC
069200                          DE0230-XTAT-FNDC           (I)          CPPDEDHC
069300                          DE0230-XTAT-PROJECT-CODEC  (I)          CPPDEDHC
069400                          DE0230-XTAT-SUBC           (I).         CPPDEDHC
066600     MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDHC
071200                          DE0230-XTAT-LOCH           (I)          CPPDEDHC
071300                          DE0230-XTAT-ACCTH          (I)          CPPDEDHC
071400                          DE0230-XTAT-COST-CENTERH   (I)          CPPDEDHC
071500                          DE0230-XTAT-FNDH           (I)          CPPDEDHC
071600                          DE0230-XTAT-PROJECT-CODEH  (I)          CPPDEDHC
071700                          DE0230-XTAT-SUBH           (I).         CPPDEDHC
000900**************************************************************/   CPPDEDHC
066110 CPPD-PROTECT-A-LINE            SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
066600     MOVE UCCOMMON-ATR-ENTRY-PRO TO                               CPPDEDHC
076100                          DE0230-XTAT-LOCA           (I)          CPPDEDHC
076200                          DE0230-XTAT-ACCTA          (I)          CPPDEDHC
076300                          DE0230-XTAT-COST-CENTERA   (I)          CPPDEDHC
076400                          DE0230-XTAT-FNDA           (I)          CPPDEDHC
076500                          DE0230-XTAT-PROJECT-CODEA  (I)          CPPDEDHC
076600                          DE0230-XTAT-SUBA           (I).         CPPDEDHC
000900**************************************************************/   CPPDEDHC
066110 CPPD-FORMAT-LINE-DATA          SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
089100     ADD +1 TO CPWSEDTH-CNT.                                      CPPDEDHC
089200     MOVE I TO CPWSEDTH-ELEM-SCREEN-OCCURNCE (CPWSEDTH-CNT).      CPPDEDHC
089300     MOVE XTAT-FAU OF CCWSTHFT-ITEM (I)                           CPPDEDHC
089400                   TO CPWSEDTH-TRN-DATA (CPWSEDTH-CNT).           CPPDEDHC
089500     MOVE 0230 TO CPWSEDTH-ELEM-NUMBER (CPWSEDTH-CNT).            CPPDEDHC
089600     SET CPWSEDTH-ELEM-REQUEST-DISPLAY (CPWSEDTH-CNT) TO TRUE.    CPPDEDHC
000900**************************************************************/   CPPDEDHC
066110 CPPD-PROCESS-INPUT-DATA        SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
111600     ADD +1 TO CPWSEDTH-CNT.                                      CPPDEDHC
111700     MOVE I TO  CPWSEDTH-ELEM-SCREEN-OCCURNCE (CPWSEDTH-CNT).     CPPDEDHC
111800     MOVE 0230 TO CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT).             CPPDEDHC
111900     MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDHC
112000                      DE0230-XTAT-LOCC (I)                        CPPDEDHC
114800                      DE0230-XTAT-ACCTC (I)                       CPPDEDHC
117600                      DE0230-XTAT-COST-CENTERC (I)                CPPDEDHC
120400                      DE0230-XTAT-FNDC (I)                        CPPDEDHC
123200                      DE0230-XTAT-PROJECT-CODEC (I)               CPPDEDHC
126000                      DE0230-XTAT-SUBC (I).                       CPPDEDHC
112100     MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDHC
112200                      DE0230-XTAT-LOCH (I)                        CPPDEDHC
115000                      DE0230-XTAT-ACCTH (I)                       CPPDEDHC
117800                      DE0230-XTAT-COST-CENTERH (I)                CPPDEDHC
120600                      DE0230-XTAT-FNDH (I)                        CPPDEDHC
123400                      DE0230-XTAT-PROJECT-CODEH (I)               CPPDEDHC
126200                      DE0230-XTAT-SUBH (I).                       CPPDEDHC
112800     MOVE XTAT-FAU OF CCWSTHFH-ITEM-HOLD (I) TO FAUR-FAU.         CPPDEDHC
112300     IF DE0230-XTAT-LOCL (I) > ZERO OR                            CPPDEDHC
112400        DE0230-XTAT-LOCF (I) = ERASE-EOF                          CPPDEDHC
112500        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDHC
112600        IF DE0230-XTAT-LOCF (I) = ERASE-EOF                       CPPDEDHC
112700           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDHC
113000           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDHC
113100                       DE0230-XTAT-LOCA (I)                       CPPDEDHC
113200        ELSE                                                      CPPDEDHC
113300           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDHC
113400                       DE0230-XTAT-LOCA (I)                       CPPDEDHC
113500           MOVE DE0230-XTAT-LOCI (I) TO FAUR-LOCATION             CPPDEDHC
113700        END-IF                                                    CPPDEDHC
114200     END-IF.                                                      CPPDEDHC
114300                                                                  CPPDEDHC
115100     IF DE0230-XTAT-ACCTL (I) > ZERO OR                           CPPDEDHC
115200        DE0230-XTAT-ACCTF (I) = ERASE-EOF                         CPPDEDHC
115300        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDHC
115400        IF DE0230-XTAT-ACCTF (I) = ERASE-EOF                      CPPDEDHC
115500           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDHC
115800           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDHC
115900                       DE0230-XTAT-ACCTA (I)                      CPPDEDHC
116000        ELSE                                                      CPPDEDHC
116100           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDHC
116200                       DE0230-XTAT-ACCTA (I)                      CPPDEDHC
116300           MOVE DE0230-XTAT-ACCTI (I) TO FAUR-ACCOUNT             CPPDEDHC
116500        END-IF                                                    CPPDEDHC
117000     END-IF.                                                      CPPDEDHC
117100                                                                  CPPDEDHC
117900     IF DE0230-XTAT-COST-CENTERL (I) > ZERO OR                    CPPDEDHC
118000        DE0230-XTAT-COST-CENTERF (I) = ERASE-EOF                  CPPDEDHC
118100        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDHC
118200        IF DE0230-XTAT-COST-CENTERF (I) = ERASE-EOF               CPPDEDHC
118300           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDHC
118600           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDHC
118700                       DE0230-XTAT-COST-CENTERA (I)               CPPDEDHC
118800        ELSE                                                      CPPDEDHC
118900           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDHC
119000                       DE0230-XTAT-COST-CENTERA (I)               CPPDEDHC
119100           MOVE DE0230-XTAT-COST-CENTERI (I) TO FAUR-COST-CENTER  CPPDEDHC
119300        END-IF                                                    CPPDEDHC
119800     END-IF.                                                      CPPDEDHC
119900                                                                  CPPDEDHC
120700     IF DE0230-XTAT-FNDL (I) > ZERO OR                            CPPDEDHC
120800        DE0230-XTAT-FNDF (I) = ERASE-EOF                          CPPDEDHC
120900        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDHC
121000        IF DE0230-XTAT-FNDF (I) = ERASE-EOF                       CPPDEDHC
121100           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDHC
121400           MOVE UCCOMMON-ATR-ENTRY TO FAUR-FUND                   CPPDEDHC
121600        ELSE                                                      CPPDEDHC
121700           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDHC
121800                       DE0230-XTAT-FNDA (I)                       CPPDEDHC
121900           MOVE DE0230-XTAT-FNDI (I) TO FAUR-FUND                 CPPDEDHC
122100        END-IF                                                    CPPDEDHC
122600     END-IF.                                                      CPPDEDHC
                                                                        CPPDEDHC
123500     IF DE0230-XTAT-PROJECT-CODEL (I) > ZERO OR                   CPPDEDHC
123600        DE0230-XTAT-PROJECT-CODEF (I) = ERASE-EOF                 CPPDEDHC
123700        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDHC
123800        IF DE0230-XTAT-PROJECT-CODEF (I) = ERASE-EOF              CPPDEDHC
123900           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDHC
124200           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDHC
124300                       DE0230-XTAT-PROJECT-CODEA (I)              CPPDEDHC
124400        ELSE                                                      CPPDEDHC
124500           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDHC
124600                       DE0230-XTAT-PROJECT-CODEA (I)              CPPDEDHC
124700           MOVE DE0230-XTAT-PROJECT-CODEI (I) TO FAUR-PROJECT-CODECPPDEDHC
124900        END-IF                                                    CPPDEDHC
125400     END-IF.                                                      CPPDEDHC
                                                                        CPPDEDHC
126300     IF DE0230-XTAT-SUBL (I) > ZERO OR                            CPPDEDHC
126400        DE0230-XTAT-SUBF (I) = ERASE-EOF                          CPPDEDHC
126500        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDHC
126600        IF DE0230-XTAT-SUBF (I) = ERASE-EOF                       CPPDEDHC
126700           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDHC
127000           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDHC
127100                       DE0230-XTAT-SUBA (I)                       CPPDEDHC
127200        ELSE                                                      CPPDEDHC
127300           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDHC
127400                       DE0230-XTAT-SUBA (I)                       CPPDEDHC
127500           MOVE DE0230-XTAT-SUBI (I) TO FAUR-SUB-ACCOUNT          CPPDEDHC
127700        END-IF                                                    CPPDEDHC
128200     END-IF.                                                      CPPDEDHC
128200                                                                  CPPDEDHC
102400     IF CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)                  CPPDEDHC
102500        MOVE FAUR-FAU TO CPWSEDTH-ENTERED-DATA(CPWSEDTH-CNT)      CPPDEDHC
102600     ELSE                                                         CPPDEDHC
102700        SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUE   CPPDEDHC
102800        MOVE FAUR-FAU TO CPWSEDTH-TRN-DATA(CPWSEDTH-CNT)          CPPDEDHC
102900     END-IF.                                                      CPPDEDHC
000900**************************************************************/   CPPDEDHC
066110 CPPD-MOVE-ONE-ELEMENT          SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
174800     IF CPWSEDTH-ELEM-REQUEST-EDIT(EDIT-SUB)                      CPPDEDHC
174900        MOVE CPWSEDTH-TRN-DATA(EDIT-SUB)                          CPPDEDHC
175000                               TO XTAT-FAU OF CCWSTHFT-ITEM (I)   CPPDEDHC
175100     END-IF.                                                      CPPDEDHC
000900**************************************************************/   CPPDEDHC
066110 CPPD-PROCESS-ONE-ELEMENT       SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
193700     IF CPWSEDTH-ELEM-ERRORS-FOUND(EDIT-SUB)                      CPPDEDHC
193800        MOVE CPWSEDTH-ELEM-SCREEN-OCCURNCE (EDIT-SUB) TO I        CPPDEDHC
194000        MOVE UCCOMMON-HLT-ERROR TO DE0230-XTAT-LOCH (I)           CPPDEDHC
194200        MOVE UCCOMMON-ATR-ERROR-MDT TO DE0230-XTAT-LOCA (I)       CPPDEDHC
194400        MOVE UCCOMMON-CLR-ERROR TO DE0230-XTAT-LOCC (I)           CPPDEDHC
194500        MOVE CPWSEDTH-SCREEN-DATA(EDIT-SUB)                       CPPDEDHC
194600                                    TO DE0230-XTAT-LOCO (I)       CPPDEDHC
194700        MOVE  -1 TO DE0230-XTAT-LOCL (I)                          CPPDEDHC
194800        SET ERRORS-FLAGGED TO TRUE                                CPPDEDHC
194900     END-IF.                                                      CPPDEDHC
000900**************************************************************/   CPPDEDHC
066110 CPPD-HIGHLIGHT-ELEMENTS        SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
275700     MOVE -1                 TO DE0230-XTAT-LOCL (WS-TRANS).      CPPDEDHC
275900     MOVE WS-ATR             TO DE0230-XTAT-LOCA (WS-TRANS).      CPPDEDHC
276100     MOVE UCCOMMON-CLR-ERROR TO DE0230-XTAT-LOCC (WS-TRANS).      CPPDEDHC
276300     MOVE UCCOMMON-HLT-ERROR TO DE0230-XTAT-LOCH (WS-TRANS).      CPPDEDHC
276400     SET ERRORS-FLAGGED TO TRUE.                                  CPPDEDHC
000900**************************************************************/   CPPDEDHC
066110 CPPD-MOVE-DATA-TO-SCREEN       SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
297500     MOVE CPWSPTRW-FAU TO FAUR-FAU.                               CPPDEDHC
297500     EVALUATE TRUE                                                CPPDEDHC
297600       WHEN (CPWSPTRW-HOME-DEPT-CODE NOT = SPACES)                CPPDEDHC
297700        AND (CPWSPTRW-HOME-DEPT-CODE NOT = LOW-VALUES)            CPPDEDHC
297800         MOVE 'HOME DEPARTMENT CODE:' TO SELECTION-TYPEO          CPPDEDHC
297900         MOVE CPWSPTRW-HOME-DEPT-CODE TO SELECTION-CRITERIAO      CPPDEDHC
298000       WHEN (FAUR-LOCATION = SPACES OR                            CPPDEDHC
298000             FAUR-LOCATION = LOW-VALUES)                          CPPDEDHC
298100        AND (FAUR-ACCOUNT      = SPACES OR                        CPPDEDHC
298100             FAUR-ACCOUNT      = LOW-VALUES)                      CPPDEDHC
298100        AND (FAUR-FUND      = SPACES OR                           CPPDEDHC
298100             FAUR-FUND      = LOW-VALUES)                         CPPDEDHC
298100        AND (FAUR-PROJECT-CODE = SPACES OR                        CPPDEDHC
298100             FAUR-PROJECT-CODE = LOW-VALUES)                      CPPDEDHC
298400        AND (FAUR-COST-CENTER NOT = SPACES AND                    CPPDEDHC
298400             FAUR-COST-CENTER NOT = LOW-VALUES)                   CPPDEDHC
298600         MOVE 'COST CENTER         :' TO SELECTION-TYPEO          CPPDEDHC
298700         MOVE FAUR-COST-CENTER        TO SELECTION-CRITERIAO      CPPDEDHC
298400       WHEN (FAUR-FAU NOT = SPACES)                               CPPDEDHC
298500        AND (FAUR-FAU NOT = LOW-VALUES)                           CPPDEDHC
298200         MOVE 'FULL ACCOUNTING UNIT:' TO SELECTION-TYPEO          CPPDEDHC
298300         MOVE CPWSPTRW-FAU            TO SELECTION-CRITERIAO      CPPDEDHC
298800     END-EVALUATE.                                                CPPDEDHC
000900**************************************************************/   CPPDEDHC
066110 CPPD-MOVE-VREDIT-DATA-TO-MAP   SECTION.                          CPPDEDHC
001100**************************************************************/   CPPDEDHC
311800     MOVE CPWSEDTH-SCREEN-DATA (ARRAY-SUB) TO FAUR-FAU.           CPPDEDHC
312200     MOVE FAUR-LOCATION     TO DE0230-XTAT-LOCO (I).              CPPDEDHC
312500     MOVE FAUR-ACCOUNT      TO DE0230-XTAT-ACCTO (I).             CPPDEDHC
312700     MOVE FAUR-COST-CENTER  TO DE0230-XTAT-COST-CENTERO (I).      CPPDEDHC
312800     MOVE FAUR-FUND         TO DE0230-XTAT-FNDO (I).              CPPDEDHC
313100     MOVE FAUR-PROJECT-CODE TO DE0230-XTAT-PROJECT-CODEO (I).     CPPDEDHC
313400     MOVE FAUR-SUB-ACCOUNT  TO DE0230-XTAT-SUBO (I).              CPPDEDHC
