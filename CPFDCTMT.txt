000100**************************************************************/   30871304
000200*  COPYMEMBER: CPFDCTMT                                      */   30871304
000300*  RELEASE: ___1304______ SERVICE REQUEST(S): _____3087____  */   30871304
000400*  NAME:_______WJG_______ CREATION DATE:      ___06/03/99__  */   30871304
000500*  DESCRIPTION:                                              */   30871304
000600*  - NEW BASE SYSTEM COPYMEMBER                              */   30871304
000700*  - FILE DESCRIPTION (FD) FOR CTL MASS CHANGE TRANSATION    */   30871304
000710*    FILE                                                    */   30871304
000800**************************************************************/   30871304
000900*    COPYID=CPFDCTMT                                              CDFDCTST
001000     BLOCK CONTAINS 0 RECORDS                                     CDFDCTST
001100     RECORD CONTAINS 80 CHARACTERS                                CDFDCTST
001200     RECORDING MODE IS F                                          CDFDCTST
001300     LABEL RECORDS ARE STANDARD.                                  CDFDCTST
