000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXSCP                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   13250211
000200*  COPYID:   CPWSXSCP                                        */   13250211
000300*  RLSE #___0211__ REF RLSE # 0191___ SERVICE REQ __1325_____*/   13250211
000400*  NAME ___SLB_________   MODIFICATION DATE ____05/01/86_____*/   13250211
000500*  DESCRIPTION                                               */   13250211
000600*  ADDED 1 BYTE TO LENGTH OF XSCP-PROG-ID TO ALLOW COMPLETE  */   13250211
000700*  PROGRAM NAME, E.G. PPP711, TO BE USED IN SPEC-CARD RECORD.*/   13250211
000800*  DELETED VALID-PROG-ID VALUE CLAUSE (BY COMMENT).          */   13250211
000900*                                                            */   13250211
001000**************************************************************/   13250211
001100     SKIP1                                                        13250211
001200**************************************************************/   13250191
001300*  COPYID:   CPWSXSCP                                        */   13250191
001400*  RELEASE # __0191____   SERVICE REQUEST NO(S)___1325_______*/   13250191
001500*  NAME ___SLB_________   MODIFICATION DATE ____01/27/86_____*/   13250191
001600*  DESCRIPTION                                               */   13250191
001700*  RECORD LAYOUT FOR PPP711, PPP712 AND PPP713 (FCP INTER-   */   13250191
001800*  FACE PROGRAMS) SPEC-CARD.                                 */   13250191
001900**************************************************************/   13250191
002000*                                                                 13250191
002100*    COPYID=CPWSXSCP                                              CPWSXSCP
002200*                                                                 CPWSXSCP
002300*01  XSCP-SPEC-CARD-REC.                                          30930413
002400*    05  XSCP-PROG-ID                PIC X(10).                   13250211
002500*        88  VALID-PROG-ID           VALUE 'PPP71-SPEC'.          13250211
002600     05  XSCP-PROG-ID                PIC X(11).                   13250211
002700     05  XSCP-EFF-DATE.                                           CPWSXSCP
002800         10  XSCP-EFF-DTE-MO-DA      PIC X(04).                   CPWSXSCP
002900             88  VALID-MO-DA         VALUES '0131' '0228' '0229'  CPWSXSCP
003000                                     '0331' '0430' '0531' '0630'  CPWSXSCP
003100                                     '0731' '0831' '0930' '1031'  CPWSXSCP
003200                                     '1130' '1231'.               CPWSXSCP
003300         10  XSCP-EFF-MO-DA REDEFINES XSCP-EFF-DTE-MO-DA.         CPWSXSCP
003400             15  XSCP-EFF-MONTH      PIC X(02).                   CPWSXSCP
003500             15  XSCP-EFF-DAY        PIC X(02).                   CPWSXSCP
003600         10  XSCP-EFF-YEAR           PIC X(02).                   CPWSXSCP
003700*    05  FILLER                      PIC X(64).                   13250211
003800     05  FILLER                      PIC X(63).                   13250211
003900     SKIP1                                                        CPWSXSCP
