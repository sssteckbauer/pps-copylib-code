000000**************************************************************/   36430875
000001*  COPYMEMBER: UCIDWSAS                                      */   36430875
000002*  RELEASE: ___0875______ SERVICE REQUEST(S): _____3643____  */   36430875
000003*  NAME:_______AAC_______ MODIFICATION DATE:  __02/24/94____ */   36430875
000004*  DESCRIPTION:                                              */   36430875
000005*    ADD OPTION TO ASSIGN WITHOUT CHECKING FOR DUP SSNS      */   36430875
000006*                                                            */   36430875
000007**************************************************************/   36430875
000000**************************************************************/   36430821
000001*  COPYMEMBER: UCIDWSAS                                      */   36430821
000002*  RELEASE: ___0821______ SERVICE REQUEST(S): _____3643____  */   36430821
000003*  NAME:_______STC_______ MODIFICATION DATE:  __10/05/93____ */   36430821
000004*  DESCRIPTION:                                              */   36430821
000005*       IID COMMON WORK EXTERNAL AREA                        */   36430821
000007*                                                            */   36430821
000008**************************************************************/   36430821
001000*01  UCIDWSAS-INTERFACE   EXTERNAL.                               UCIDWSAS
001100     03  UCIDWSAS-EXT-AREA                     PIC X(500).        UCIDWSAS
001200     03  UCIDWSAS-EXT-DATA   REDEFINES   UCIDWSAS-EXT-AREA.       UCIDWSAS
001300         05  UCIDWSAS-RETURN-STATUS            PIC X.             UCIDWSAS
001400             88  UCIDWSAS-FATAL-ERROR          VALUE 'Y'.         UCIDWSAS
001500             88  UCIDWSAS-NO-ERRORS            VALUE 'N'.         UCIDWSAS
001600         05  UCIDWSAS-ERROR-PROG               PIC X(08).         UCIDWSAS
001700         05  UCIDWSAS-ERROR-PARA               PIC X(04).         UCIDWSAS
001800         05  UCIDWSAS-ERROR-CODE               PIC 9(04).         UCIDWSAS
001900         05  UCIDWSAS-CALL-REQ                 PIC X(06).         UCIDWSAS
002000             88  UCIDWSAS-REQ-ASSN-UPDT        VALUE 'ASNUPD'.    UCIDWSAS
002100             88  UCIDWSAS-REQ-CONFIRMED-ASSN   VALUE 'CNFASN'.    UCIDWSAS
002200             88  UCIDWSAS-REQ-CONFIRMED-UPDT   VALUE 'CNFUPD'.    UCIDWSAS
002300             88  UCIDWSAS-REQ-PENDING-COMMIT   VALUE 'PNDCMT'.    UCIDWSAS
002400             88  UCIDWSAS-REQ-PENDING-DELETE   VALUE 'PNDDEL'.    UCIDWSAS
002500         05  UCIDWSAS-RETURN-STAT              PIC X(06).         UCIDWSAS
002600             88  UCIDWSAS-STAT-ASSIGNED        VALUE 'ASSND '.    UCIDWSAS
002700             88  UCIDWSAS-STAT-UPDATED         VALUE 'UPDTD '.    UCIDWSAS
002800             88  UCIDWSAS-STAT-RESOL-REQD      VALUE 'RESREQ'.    UCIDWSAS
002900             88  UCIDWSAS-STAT-DUP-SSN         VALUE 'DUPSSN'.    UCIDWSAS
003000             88  UCIDWSAS-STAT-PEND-COMMITTED  VALUE 'PNDCMT'.    UCIDWSAS
003100             88  UCIDWSAS-STAT-PEND-DELETED    VALUE 'PNDDEL'.    UCIDWSAS
003200             88  UCIDWSAS-STAT-NOT-FOUND       VALUE 'NOTFND'.    UCIDWSAS
003210             88  UCIDWSAS-STAT-BDERR           VALUE 'BDERR '.    UCIDWSAS
003220             88  UCIDWSAS-STAT-NAMERR          VALUE 'NAMERR'.    UCIDWSAS
003300         05  UCIDWSAS-PASSED-FIELDS.                              UCIDWSAS
003400             10 UCIDWSAS-PASSED-IID            PIC X(09).         UCIDWSAS
003500             10 UCIDWSAS-PASSED-SSN            PIC X(09).         UCIDWSAS
003600             10 UCIDWSAS-PASSED-BIRTHDATE      PIC X(10).         UCIDWSAS
003700             10 UCIDWSAS-PASSED-NAME-LAST      PIC X(30).         UCIDWSAS
003800             10 UCIDWSAS-PASSED-NAME-FIRST     PIC X(30).         UCIDWSAS
003900             10 UCIDWSAS-PASSED-NAME-MIDDLE    PIC X(30).         UCIDWSAS
004000             10 UCIDWSAS-PASSED-NAME-SUFFIX    PIC X(04).         UCIDWSAS
004100             10 UCIDWSAS-PASSED-CHANGED-BY     PIC X(08).         UCIDWSAS
004200             10 UCIDWSAS-PASSED-STAT           PIC X(01).         UCIDWSAS
004300             10 UCIDWSAS-PASSED-REF-IID        PIC X(09).         UCIDWSAS
004400             10 UCIDWSAS-PASSED-CHANGED-AT     PIC X(26).         UCIDWSAS
004500         05  UCIDWSAS-PASSED-SYS-FIELDS.                          UCIDWSAS
004600             10  UCIDWSAS-PASSED-SYS-NAME      PIC X(02).         UCIDWSAS
004700             10  UCIDWSAS-PASSED-SYS-IID       PIC X(09).         UCIDWSAS
004800         05  UCIDWSAS-RECORD-RET-COUNT         PIC S9(03) COMP-3. UCIDWSAS
004900         05  UCIDWSAS-RETURNED-FIELDS.                            UCIDWSAS
005000             10 UCIDWSAS-RET-IID               PIC X(09).         UCIDWSAS
005100             10 UCIDWSAS-RET-SSN               PIC X(09).         UCIDWSAS
005200             10 UCIDWSAS-RET-BIRTHDATE         PIC X(10).         UCIDWSAS
005300             10 UCIDWSAS-RET-NAME-LAST         PIC X(30).         UCIDWSAS
005400             10 UCIDWSAS-RET-NAME-FIRST        PIC X(30).         UCIDWSAS
005500             10 UCIDWSAS-RET-NAME-MIDDLE       PIC X(30).         UCIDWSAS
005600             10 UCIDWSAS-RET-NAME-SUFFIX       PIC X(04).         UCIDWSAS
005700             10 UCIDWSAS-RET-CHANGED-BY        PIC X(08).         UCIDWSAS
005800             10 UCIDWSAS-RET-STATUS            PIC X(01).         UCIDWSAS
005900             10 UCIDWSAS-RET-REF-IID           PIC X(09).         UCIDWSAS
006000             10 UCIDWSAS-RET-ASSIGNED-AT       PIC X(26).         UCIDWSAS
006100             10 UCIDWSAS-RET-CHANGED-AT        PIC X(26).         UCIDWSAS
006200         05  UCIDWSAS-CALL-WITH-SSN-BYPASS     PIC X(01).         36430875
006210             88 UCIDWSAS-BYPASS-SSN-CHECK      VALUE 'Y'.         36430875
006220             88 UCIDWSAS-PERFORM-SSN-CHECK     VALUE SPACE.       36430875
006300         05  FILLER                            PIC X(099).        36430875
006400****     05  FILLER                            PIC X(100).        36430875
