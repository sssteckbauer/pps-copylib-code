000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPDIPIC                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_PHIL THOMPSON___ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  BASE FAU RELATED PROCEDURE CODE FOR ONLINE FUNCTION IPIC. */   32021138
000700*  THIS CODE ONLY WORKS WITH THE BASE PROGRAM PPWIPIC AND    */   32021138
000800*  BASE MAP PPIPIC0.                                         */   32021138
000900**************************************************************/   32021138
006900**************************************************************    CPPDIPIC
007000*  MOVE DATA FROM CPWSPTRW TO THE MAP.                       *    CPPDIPIC
007100**************************************************************    CPPDIPIC
007200 CPPD-DISPLAY-DATA             SECTION.                           CPPDIPIC
007300                                                                  CPPDIPIC
007310     MOVE CPWSPTRW-FAU TO FAUR-FAU.                               CPPDIPIC
007320                                                                  CPPDIPIC
007400     EVALUATE TRUE                                                CPPDIPIC
007500       WHEN (FAUR-LOCATION NOT = SPACES AND LOW-VALUES)           CPPDIPIC
007600         OR (FAUR-ACCOUNT NOT =SPACES AND LOW-VALUES)             CPPDIPIC
007700         OR (FAUR-FUND NOT = SPACES AND LOW-VALUES)               CPPDIPIC
007800         OR (FAUR-PROJECT-CODE NOT = SPACES AND LOW-VALUES)       CPPDIPIC
007900         OR (FAUR-SUB-ACCOUNT NOT = SPACES AND LOW-VALUES)        CPPDIPIC
008000         MOVE 'FULL ACCOUNTING UNIT:' TO SELECTION-TYPEO          CPPDIPIC
008100         MOVE CPWSPTRW-FAU            TO SELECTION-CRITERIAO      CPPDIPIC
009790       WHEN (FAUR-COST-CENTER NOT = SPACES OR LOW-VALUES)         CPPDIPIC
009791         MOVE 'COST CENTER         :' TO SELECTION-TYPEO          CPPDIPIC
009792         MOVE FAUR-COST-CENTER        TO SELECTION-CRITERIAO      CPPDIPIC
009800     END-EVALUATE.                                                CPPDIPIC
