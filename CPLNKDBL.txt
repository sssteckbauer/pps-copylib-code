000000**************************************************************/   37540566
000001*  COPYMEMBER: CPLNKDBL                                      */   37540566
000002*  RELEASE: ___0566______ SERVICE REQUEST(S): _____3754____  */   37540566
000003*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___04/23/91__  */   37540566
000004*  DESCRIPTION:                                              */   37540566
000005*  - INCREASE SIZE OF GTN-AMT                                */   37540566
000006*                                                            */   37540566
000007**************************************************************/   37540566
000100******************************************************************36090486
000200*  COPYMEMBER: CPLNKDBL                                          *36090486
000300*  RELEASE: ____0486____  SERVICE REQUEST(S): ____3609____       *36090486
000400*  NAME:__P_PARKER______  CREATION DATE:      __07/16/90__       *36090486
000500*  DESCRIPTION:                                                  *36090486
000600*    LINKAGE FOR PPDBLUTL SUBROUTINE CREATED FOR DB2 EDB         *36090486
000700*    CONVERSION.  REPLACES CPWSDB60 FOR GROSS-TO-NET DATA.       *36090486
000900*                                                                *36090486
001000******************************************************************CPLNKDBL
001400*    COPYID=CPLNKDBL                                              CPLNKDBL
001500*01  PPDBLUTL-INTERFACE.                                          CPLNKDBL
001600*                                                                *CPLNKDBL
001700******************************************************************CPLNKDBL
001800*    P P D B L U T L   I N T E R F A C E                         *CPLNKDBL
001900******************************************************************CPLNKDBL
002000*                                                                *CPLNKDBL
002700     05  PPDBLUTL-ERROR-SW       PICTURE X(1).                    CPLNKDBL
002700     88  PPDBLUTL-ERROR VALUE 'Y'.                                CPLNKDBL
002700     05  PPDBLUTL-EMPLOYEE-ID   PICTURE X(9).                     CPLNKDBL
002710     05  PPDBLUTL-ROW-COUNT     PICTURE 9(3).                     CPLNKDBL
002800     05  PPDBLUTL-ENTRIES.                                        CPLNKDBL
002800     10  PPDBLUTL-ENTRY     OCCURS 100  TIMES.                    CPLNKDBL
002900         15  GTN-NUMBER          PICTURE 9(3).                    CPLNKDBL
003000         15  GTN-IND             PICTURE X.                       CPLNKDBL
003100*****    15  GTN-AMT             PICTURE S9(5)V99    COMP-3.      37540566
003200         15  GTN-AMT             PICTURE S9(7)V99    COMP-3.      37540566
