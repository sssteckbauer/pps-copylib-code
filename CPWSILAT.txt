000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSILAT                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*                                                            */   32021138
000006*  IMPLEMENTATION OF THE FLEXIBLE FULL ACCOUNTING UNIT (3202)*/   32021138
000007*                                                            */   32021138
000008**************************************************************/   32021138
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSILAT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*************************************************************/    30860356
000200*  COPYMEMBER:  CPWSILAT                                    */    30860356
000300*  RELEASE # ___0356____   SERVICE REQUEST NO(S)___3086___  */    30860356
000400*  NAME _____WEJ________   MODIFICATION DATE ___05/09/88__  */    30860356
000500*                                                           */    30860356
000600*  DESCRIPTION                                              */    30860356
000700*    - INITIAL RELEASE: RECORD DESCRIPTION FOR THE          */    30860356
000800*      LEAVE ACCRUAL TABLE'S INPUT TRANSACTION.             */    30860356
000900*************************************************************/    30860356
001000*    COPYID=CPWSILAT                                              CPWSILAT
001100*01  LATIN-LEAVE-ACCRUAL-DATA.                                    30930413
001200*-------------------------------------------------------------    CPWSILAT
001300*    L E A V E    A C C R U A L   T A B L E   R E C O R D   *     CPWSILAT
001400*-------------------------------------------------------------    CPWSILAT
001500     SKIP1                                                        CPWSILAT
001600     05  FILLER                      PIC X(01).                   CPWSILAT
001700     05  LATIN-CODE                  PIC X(02).                   CPWSILAT
001800         88  LEAVE-ACCRUAL-TRANS   VALUE '25'.                    CPWSILAT
001900     05  LATIN-TITLE-TYPE            PIC X(01).                   CPWSILAT
002000     05  LATIN-TITLE-UNIT-CD         PIC X(02).                   CPWSILAT
002100     05  LATIN-AREP-CD               PIC X(01).                   CPWSILAT
002200     05  LATIN-SPEC-HAND-CD          PIC 9(01).                   CPWSILAT
002300     05  LATIN-SPEC-HAND-CD-X    REDEFINES                        CPWSILAT
002400         LATIN-SPEC-HAND-CD          PIC X(01).                   CPWSILAT
002500     05  LATIN-DISTR-UNIT-CD         PIC 9(01).                   CPWSILAT
002600     05  LATIN-DISTR-UNIT-CD-X   REDEFINES                        CPWSILAT
002700         LATIN-DISTR-UNIT-CD         PIC X(01).                   CPWSILAT
002800     05  LATIN-EFF-DATE.                                          CPWSILAT
002900         10  LATIN-EFF-DT-MM         PIC 9(02).                   CPWSILAT
003000         10  LATIN-EFF-DT-YY         PIC 9(02).                   CPWSILAT
003100     05  LATIN-EFF-DATE-NUM  REDEFINES  LATIN-EFF-DATE            CPWSILAT
003200                                     PIC 9(04).                   CPWSILAT
003300     05  LATIN-LINE-NO               PIC X(01).                   CPWSILAT
003400     05  LATIN-LINE-NO-NUM  REDEFINES  LATIN-LINE-NO              CPWSILAT
003500                                     PIC 9(01).                   CPWSILAT
003600     05  LATIN-LINE1-DATA-UNIQUE.                                 CPWSILAT
003700         10  LATIN-THRESH-PERCNT     PIC 9V9(4).                  CPWSILAT
003800         10  LATIN-THRESH-PERCNT-X REDEFINES                      CPWSILAT
003900             LATIN-THRESH-PERCNT     PIC X(05).                   CPWSILAT
004000         10  LATIN-MAX-HRS-OVERIDE.                               CPWSILAT
004100             15  LATIN-MHO-VAC       PIC 9(4)V99.                 CPWSILAT
004200             15  LATIN-MHO-VAC-X   REDEFINES                      CPWSILAT
004300                 LATIN-MHO-VAC       PIC X(06).                   CPWSILAT
004400             15  LATIN-MHO-SKL       PIC 9(4)V99.                 CPWSILAT
004500             15  LATIN-MHO-SKL-X   REDEFINES                      CPWSILAT
004600                 LATIN-MHO-SKL       PIC X(06).                   CPWSILAT
004700             15  LATIN-MHO-PTO       PIC 9(4)V99.                 CPWSILAT
004800             15  LATIN-MHO-PTO-X   REDEFINES                      CPWSILAT
004900                 LATIN-MHO-PTO       PIC X(06).                   CPWSILAT
005000         10  LATIN-HRS-PER-DAY       PIC 99V99.                   CPWSILAT
005100         10  LATIN-HRS-PER-DAY-X   REDEFINES                      CPWSILAT
005200             LATIN-HRS-PER-DAY       PIC X(04).                   CPWSILAT
005300         10  LATIN-PRE-CALC-CD       PIC 9(02).                   CPWSILAT
005400         10  LATIN-PRE-CALC-CD-X   REDEFINES                      CPWSILAT
005500             LATIN-PRE-CALC-CD       PIC X(02).                   CPWSILAT
005600         10  LATIN-RT-SCHED-NO       PIC 9(03).                   CPWSILAT
005700         10  LATIN-RT-SCHED-NO-X   REDEFINES                      CPWSILAT
005800             LATIN-RT-SCHED-NO       PIC X(03).                   CPWSILAT
005900         10  LATIN-POST-CALC-CD      PIC 9(02).                   CPWSILAT
006000         10  LATIN-POST-CALC-CD-X  REDEFINES                      CPWSILAT
006100             LATIN-POST-CALC-CD      PIC X(02).                   CPWSILAT
006110         10  FILLER                  PIC X(32).                   32021138
006200     05  LATIN-LINE2-6-DATA-UNIQUE REDEFINES                      CPWSILAT
006300         LATIN-LINE1-DATA-UNIQUE.                                 CPWSILAT
006400****     10  FILLER                  PIC X(34).                   32021138
006510**** 05  LATIN-FUND-RANGE-DATA.                                   32021138
006600****     10  LATIN-FUND-RANGE.                                    32021138
006700****         15  LATIN-FR-LO         PIC 9(05).                   32021138
006800****         15  LATIN-FR-LO-X     REDEFINES                      32021138
006900****             LATIN-FR-LO         PIC X(05).                   32021138
007000****         15  LATIN-FR-HI         PIC 9(05).                   32021138
007100****         15  LATIN-FR-HI-X     REDEFINES                      32021138
007200****             LATIN-FR-HI         PIC X(05).                   32021138
007210         10  LATIN-FND-GROUP-CODE    PIC X(04).                   32021138
007300         10  LATIN-UTIL-FACTORS.                                  CPWSILAT
007400             15  LATIN-UF-VAC        PIC 9V9(4).                  CPWSILAT
007500             15  LATIN-UF-VAC-X    REDEFINES                      CPWSILAT
007600                 LATIN-UF-VAC        PIC X(05).                   CPWSILAT
007700             15  LATIN-UF-SKL        PIC 9V9(4).                  CPWSILAT
007800             15  LATIN-UF-SKL-X    REDEFINES                      CPWSILAT
007900                 LATIN-UF-SKL        PIC X(05).                   CPWSILAT
008000             15  LATIN-UF-PTO        PIC 9V9(4).                  CPWSILAT
008100             15  LATIN-UF-PTO-X    REDEFINES                      CPWSILAT
008200                 LATIN-UF-PTO        PIC X(05).                   CPWSILAT
008300****     10  LATIN-LV-RES-ACCT       PIC 9(06).                   32021138
008400****     10  LATIN-LV-RES-ACCT-X   REDEFINES                      32021138
008500****         LATIN-LV-RES-ACCT       PIC X(06).                   32021138
008600****     10  FILLER                  PIC X(01).                   32021138
008510         10  LATIN-LV-RES-FAU        PIC X(30).                   32021138
008600         10  FILLER                  PIC X(17).                   32021138
008700     SKIP3                                                        CPWSILAT
