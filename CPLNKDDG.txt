000000**************************************************************/   48881487
000001*  COPYMEMBER: CPLNKDDG                                      */   48881487
000002*  RELEASE: ___1487______ SERVICE REQUEST(S): ____14888____  */   48881487
000003*  NAME:__PHIL THOMPSON__ CREATION DATE:      ___04/28/03__  */   48881487
000004*  DESCRIPTION:                                              */   48881487
000005*    DEFINES LILNKAGE FOR CALLS TO PPDDGUTL.                 */   48881487
000007**************************************************************/   48881487
001000*    COPYID=CPLNKDDG                                              CPLNKDDG
001100*01  PPDDGUTL-INTERFACE.                                          CPLNKDDG
001200*                                                                *CPLNKDDG
001300******************************************************************CPLNKDDG
001400*    P P E S G U T 2   I N T E R F A C E                         *CPLNKDDG
001500******************************************************************CPLNKDDG
001600*                                                                *CPLNKDDG
001700     05  PPDDGUTL-ERROR-SW       PICTURE X(1).                    CPLNKDDG
001800     88  PPDDGUTL-ERROR          VALUE 'Y'.                       CPLNKDDG
001801     05  PPDDGUTL-DDG-FOUND-SW   PICTURE X(1).                    CPLNKDDG
001802     88  PPDDGUTL-DDG-FOUND      VALUE 'Y'.                       CPLNKDDG
001900     05  PPDDGUTL-EMPLOYEE-ID    PICTURE X(9).                    CPLNKDDG
