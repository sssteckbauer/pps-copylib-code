000100**************************************************************/   36030532
000200*  COPY MODULE: CPRPXPTT                                     */   36030532
000300*  RELEASE#__0532___  REF: ____  SERVICE REQUEST __3603__    */   36030532
000400*  NAME __J LUCAS______   MODIFICATION DATE ____09/19/90_____*/   36030532
000500*  DESCRIPTION                                               */   36030532
000600*   - MODIFICATIONS FOR R-HAND-DRAWN CHECKS                  */   36030532
000700**************************************************************/   36030532
000100**************************************************************/   37550521
000200*  COPYMEMBER: CPRPXPTT                                      */   37550521
000300*  RELEASE # ____0521____ SERVICE REQUEST NO(S)___3755_______*/   37550521
000400*  NAME ___________KXK_   MODIFICATION DATE ____11/27/90_____*/   37550521
000500*  DESCRIPTION                                               */   37550521
000600*   - CHANGED FOR SPLIT OUT OF FICA INTO OASDI AND MEDICARE. */   37550521
000700**************************************************************/   37550521
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPRPXPTT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPRPXPTT                                              CPRPXPTT
000200******************************************************************CPRPXPTT
000300*    THESE TITLES ARE USED TO IDENTIFY TRANSACTIONS               CPRPXPTT
000400*    ON THE -PAYROLL INPUT RECONCILIATION REPORT-.                CPRPXPTT
000500*            PP3800XX - PAYR038                                   CPRPXPTT
000600*            PP3900XX - PAYR051                                   CPRPXPTT
000700*            PP4000XX - PAYR061                                   CPRPXPTT
000800*            PP4100XX - PAYR071                                   CPRPXPTT
000900******************************************************************CPRPXPTT
001000*                                                                 CPRPXPTT
001100*01  XPTT-TRANSACTION-TITLES.                                     30930413
001200     03  XPTT-EXPENSE-TRANSFERS.                                  CPRPXPTT
001300         05  XPTT-EXPENSE-TRANSFERS-1     PIC X(17) VALUE         CPRPXPTT
001400             'EXPENSE TRANSFERS'.                                 CPRPXPTT
001500         05  XPTT-EXPENSE-TRANSFERS-2    PIC X(08) VALUE          CPRPXPTT
001600             '   DEBIT'.                                          CPRPXPTT
001700         05  XPTT-EXPENSE-TRANSFERS-3    PIC X(09) VALUE          CPRPXPTT
001800             '   CREDIT'.                                         CPRPXPTT
001900     03  XPTT-CANCELLATIONS.                                      CPRPXPTT
002000         05  XPTT-CANCELLATIONS-1        PIC X(13) VALUE          CPRPXPTT
002100             'CANCELLATIONS'.                                     CPRPXPTT
002200         05  XPTT-CANCELLATIONS-2        PIC X(14) VALUE          CPRPXPTT
002300             '   G-T-N INPUT'.                                    CPRPXPTT
002400         05  XPTT-CANCELLATIONS-3        PIC X(10) VALUE          CPRPXPTT
002500             '   NET PAY'.                                        CPRPXPTT
002600     03  XPTT-OVERPAYMENTS.                                       CPRPXPTT
002700         05  XPTT-OVERPAYMENTS-1         PIC X(12) VALUE          CPRPXPTT
002800             'OVERPAYMENTS'.                                      CPRPXPTT
002900         05  XPTT-OVERPAYMENTS-2         PIC X(14) VALUE          CPRPXPTT
003000             '   G-T-N INPUT'.                                    CPRPXPTT
003100         05  XPTT-OVERPAYMENTS-3         PIC X(10) VALUE          CPRPXPTT
003200             '   NET PAY'.                                        CPRPXPTT
003300     03  XPTT-HAND-DRAWNS.                                        CPRPXPTT
003400         05  XPTT-HAND-DRAWNS-1          PIC X(17) VALUE          CPRPXPTT
003500             'HAND-DRAWN CHECKS'.                                 CPRPXPTT
003600         05  XPTT-HAND-DRAWNS-2          PIC X(14) VALUE          CPRPXPTT
003700             '   G-T-N INPUT'.                                    CPRPXPTT
003800         05  XPTT-HAND-DRAWNS-3          PIC X(10) VALUE          CPRPXPTT
003900             '   NET PAY'.                                        CPRPXPTT
006100     03  XPTT-R-HAND-DRAWNS.                                      36030532
006200         05  XPTT-R-HAND-DRAWNS-1        PIC X(17) VALUE          36030532
006300             'R-HAND-DRAWN CHKS'.                                 36030532
006400         05  XPTT-R-HAND-DRAWNS-2        PIC X(14) VALUE          36030532
006500             '   G-T-N INPUT'.                                    36030532
006600         05  XPTT-R-HAND-DRAWNS-3        PIC X(10) VALUE          36030532
006700             '   NET PAY'.                                        36030532
004000     03  XPTT-HOURS-ADJ                  PIC X(17) VALUE          CPRPXPTT
004100         'HOURS ADJUSTMENTS'.                                     CPRPXPTT
004200     03  XPTT-DOLLAR-ADJ.                                         CPRPXPTT
004300         05  XPTT-DOLLAR-ADJ-1           PIC X(18) VALUE          CPRPXPTT
004400             'DOLLAR ADJUSTMENTS'.                                CPRPXPTT
004500         05  XPTT-DOLLAR-ADJ-2           PIC X(12) VALUE          CPRPXPTT
004600             '   FWT GROSS'.                                      CPRPXPTT
006100*********05  XPTT-DOLLAR-ADJ-3           PIC X(13) VALUE          37550521
006200*************'   FICA GROSS'.                                     37550521
006300         05  XPTT-DOLLAR-ADJ-3           PIC X(14) VALUE          37550521
006400             '   OASDI GROSS'.                                    37550521
004900         05  XPTT-DOLLAR-ADJ-4           PIC X(18) VALUE          CPRPXPTT
005000             '   STATE GROSS'.                                    CPRPXPTT
005100         05  XPTT-DOLLAR-ADJ-5           PIC X(20) VALUE          CPRPXPTT
005200             '   OTHER GROSSES'.                                  CPRPXPTT
005300         05  XPTT-DOLLAR-ADJ-6           PIC X(24) VALUE          CPRPXPTT
005400             '   TOTAL GROSS'.                                    CPRPXPTT
005500         05  XPTT-DOLLAR-ADJ-7           PIC X(17) VALUE          CPRPXPTT
005600             '   G-T-N ELEMENTS'.                                 CPRPXPTT
005700     03  XPTT-CURRENT-ACTIVITY           PIC X(16) VALUE          CPRPXPTT
005800         'CURRENT ACTIVITY'.                                      CPRPXPTT
005900     03  XPTT-CURRENT-HOURLY-PAY         PIC X(18) VALUE          CPRPXPTT
006000         'CURRENT HOURLY PAY'.                                    CPRPXPTT
006100     03  XPTT-LATE-PAY                   PIC X(08) VALUE          CPRPXPTT
006200         'LATE PAY'.                                              CPRPXPTT
006300     03  XPTT-ADDITIONAL-PAY             PIC X(14) VALUE          CPRPXPTT
006400         'ADDITIONAL PAY'.                                        CPRPXPTT
006500     03  XPTT-DISTRIBUTION-CHGS          PIC X(20) VALUE          CPRPXPTT
006600         'DISTRIBUTION CHANGES'.                                  CPRPXPTT
006700     03  XPTT-CURRENT-SALARY             PIC X(20) VALUE          CPRPXPTT
006800         'CURRENT SALARIED PAY'.                                  CPRPXPTT
006900     03  XPTT-REDUCTION-IN-PAY           PIC X(16) VALUE          CPRPXPTT
007000         'REDUCTION IN PAY'.                                      CPRPXPTT
007100     03  XPTT-GTN-INPUT.                                          CPRPXPTT
007200         05  XPTT-GTN-INPUT-1            PIC X(11) VALUE          CPRPXPTT
007300             'G-T-N INPUT'.                                       CPRPXPTT
007400         05  XPTT-GTN-INPUT-2            PIC X(22) VALUE          CPRPXPTT
007500             '   PREPAYMENTS ENTERED'.                            CPRPXPTT
007600         05  XPTT-GTN-INPUT-3            PIC X(27) VALUE          CPRPXPTT
007700             '   ONE-TIME DEDS & CONTRIBS'.                       CPRPXPTT
007800         05  XPTT-GTN-INPUT-4            PIC X(22) VALUE          CPRPXPTT
007900             '   ONE-TIMES SUSPENDED'.                            CPRPXPTT
008000         05  XPTT-GTN-INPUT-5            PIC X(23) VALUE          CPRPXPTT
008100             '   ONE-TIMES RECEIVABLE'.                           CPRPXPTT
008200         05  XPTT-GTN-INPUT-6            PIC X(22) VALUE          CPRPXPTT
008300             '   ONE-TIMES NOT TAKEN'.                            CPRPXPTT
008400         05  XPTT-GTN-INPUT-7            PIC X(10) VALUE          CPRPXPTT
008500             '   REFUNDS'.                                        CPRPXPTT
008600     03  XPTT-ADVANCES.                                           CPRPXPTT
008700         05  XPTT-ADVANCES-1             PIC X(08) VALUE          CPRPXPTT
008800             'ADVANCES'.                                          CPRPXPTT
008900         05  XPTT-ADVANCES-2             PIC X(13) VALUE          CPRPXPTT
009000             '   TOTAL PAID'.                                     CPRPXPTT
009100         05  XPTT-ADVANCES-3             PIC X(21) VALUE          CPRPXPTT
009200             '   RECORDED, NOT PAID'.                             CPRPXPTT
009300         05  XPTT-ADVANCES-4             PIC X(24) VALUE          CPRPXPTT
009400                'DEDUCTION MAINTENANCE'.                          CPRPXPTT
