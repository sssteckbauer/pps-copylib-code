000010*    COPYID=CPFDXBFL                                              CPFDXBFL
000020**************************************************************/   21450087
000030*  COPY MEMBER:  CPFDXBFL                                    */   21450087
000040*  RELEASE # ___0087___   SERVICE REQUEST NO(S) ____2145____  /   21450087
000050*  NAME _____JLT________   MODIFICATION DATE ___11/10/83_____*/   21450087
000060*  DESCRIPTION OF CHANGES                                    */   21450087
000070*   THE SUB-CBUC' CODE ADDED TO RECORD (SOURCE=DET#0255).    */   21450087
000080**************************************************************/   21450087
000090*                                                            */   21450087
000100     BLOCK CONTAINS 0 RECORDS                                     CPFDXBFL
000110     RECORD CONTAINS 200 CHARACTERS                               CPFDXBFL
000120     RECORDING MODE IS F                                          CPFDXBFL
000130     LABEL RECORDS ARE STANDARD.                                  CPFDXBFL
000140     SKIP1                                                        CPFDXBFL
000150 01  BUT-RECORD.                                                  CPFDXBFL
000160     05  BUT-OUT-EMP-ID                     PIC X(09).            CPFDXBFL
000170     05  BUT-OUT-CBUC                       PIC X(02).            CPFDXBFL
000180     05  BUT-OUT-HOME-DEPT                  PIC X(06).            CPFDXBFL
000190     05  BUT-OUT-EMP-NAME                   PIC X(26).            CPFDXBFL
000200     05  BUT-OUT-EMP-NAME-SFX               PIC X(04).            CPFDXBFL
000210     05  BUT-OUT-EMP-REL-CODE               PIC X(01).            CPFDXBFL
000220     05  BUT-OUT-SUB-CBUC                   PIC X(03).            21450087
000230     05  FILLER                             PIC X(27).            21450087
000240     05  BUT-OUT-DED       OCCURS  10 TIMES.                      CPFDXBFL
000250         10  BUT-OUT-DED-NO                 PIC X(03).            CPFDXBFL
000260         10  BUT-OUT-DED-AMT                PIC S9(5)V99.         CPFDXBFL
000270         10  BUT-OUT-DED-AMT-X  REDEFINES  BUT-OUT-DED-AMT        CPFDXBFL
000280                                            PIC X(07).            CPFDXBFL
000290     05  FILLER                             PIC X(22).            CPFDXBFL
000300     SKIP3                                                        CPFDXBFL
