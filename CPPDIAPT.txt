000500*==========================================================%      UCSDNNNN
000600*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSDNNNN
000700*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - NNNN         =%      UCSDNNNN
000800*==========================================================%      UCSDNNNN
000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPPDIAPT                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: D. CHILCOAT     MODIFICATION DATE: 09/09/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000800*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPDIAPT                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_PHIL THOMPSON___ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  BASE FAU RELATED PROCEDURE CODE FOR ONLINE FUNCTION IAPT. */   32021138
000700*  THIS CODE ONLY WORKS WITH THE BASE PROGRAM PPWIAPT AND    */   32021138
000800*  BASE MAP PPIAPT0.                                         */   32021138
000900**************************************************************/   32021138
039900 CPPD-INIT-SCREEN-LABEL    SECTION.                               PPWIAPT
000900**************************************************************/   32021138
048700     MOVE UCCOMMON-CLR-FLD-LABEL TO L-HEADER-4C.                  UCSD0102
048700*****MOVE UCCOMMON-CLR-FLD-LABEL TO L-FAUC.                       UCSD0102
048700     MOVE UCCOMMON-HLT-FLD-LABEL TO L-HEADER-4H.                  UCSD0102
048700*****MOVE UCCOMMON-HLT-FLD-LABEL TO L-FAUH.                       UCSD0102
000900**************************************************************/   32021138
039900 CPPD-INIT-SCREEN          SECTION.                               PPWIAPT
000900**************************************************************/   32021138
048700     MOVE UCCOMMON-CLR-NORM-DISPL TO INDEX-CODEC (DIST-SUB)       UCSD0102
048700*****MOVE UCCOMMON-CLR-NORM-DISPL TO FAUC (DIST-SUB).             UCSD0102
048700     MOVE UCCOMMON-HLT-NORM-DISPL TO INDEX-CODEH (DIST-SUB).      UCSD0102
048700*****MOVE UCCOMMON-HLT-NORM-DISPL TO FAUH (DIST-SUB).             UCSD0102
000900**************************************************************/   32021138
086200 CPPD-MOVE-DIS-TO-SCRN     SECTION.                               PPWIAPT
000900**************************************************************/   32021138
087200     MOVE FULL-ACCT-UNIT TO XFAU-FAU-UNFORMATTED.                 32021138
087300     MOVE CORR XFAU-FAU-UNFORMATTED TO XFAU-FAU-BLANK-FORMAT.     32021138
087800     MOVE XFAU-FAU-BLANK-FORMAT TO INDEX-CODEO (DIST-SUB).        UCSD0102
087800*****MOVE XFAU-FAU-BLANK-FORMAT TO FAUO (DIST-SUB).               UCSD0102
