000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXTTO                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000300*    COPYID=CPWSXTTO                                              CPWSXTTO
000200*01  XTTO-DELETE-TTL-REC.                                         30930413
000300     05  XTTO-CONTROL.                                            CPWSXTTO
000400         07  XTTO-ACD            PIC X.                           CPWSXTTO
000500         07  XTTO-DEL-TBL-NO     PIC XX        VALUE '04'.        CPWSXTTO
000600         07  XTTO-DEL-TRAN-CODE  PIC X.                           CPWSXTTO
000700         07  FILLER              PIC XX        VALUE '  '.        CPWSXTTO
000800         07  XTTO-DEL-TTL-CODE   PIC 9(4).                        CPWSXTTO
000900     05  FILLER                  PIC X(70)     VALUE SPACES.      CPWSXTTO
001000*                                                                 CPWSXTTO
001100 01  XTTO-TITLE-CODE-A-TRANS.                                     CPWSXTTO
001200     05  XTTO-A-CONTROL          PIC X(10).                       CPWSXTTO
001300     05  XTTO-USE                PIC X.                           CPWSXTTO
001400     05  XTTO-FULL-TITLE         PIC X(50).                       CPWSXTTO
001500     05  FILLER                  PIC X(19).                       CPWSXTTO
001600*                                                                 CPWSXTTO
001700 01  XTTO-TITLE-CODE-B-TRANS.                                     CPWSXTTO
001800     05  XTTO-B-CONTROL          PIC X(10).                       CPWSXTTO
001900     05  XTTO-FLSA-STATUS        PIC X.                           CPWSXTTO
002000     05  FILLER                  PIC XX.                          CPWSXTTO
002100     05  XTTO-FED-OCC            PIC X.                           CPWSXTTO
002200     05  XTTO-FED-OCC-SUB        PIC XX.                          CPWSXTTO
002300     05  XTTO-CLASS-OUTLINE      PIC X(3).                        CPWSXTTO
002400     05  XTTO-TITLE-ABBRV        PIC X(30).                       CPWSXTTO
002500     05  FILLER                  PIC X(31).                       CPWSXTTO
002600*                                                                 CPWSXTTO
002700 01  XTTO-TITLE-CODE-C-TRANS.                                     CPWSXTTO
002800     05  XTTO-C-CONTROL          PIC X(10).                       CPWSXTTO
002900     05  XTTO-RETIRE-CODE1       PIC X.                           CPWSXTTO
003000     05  XTTO-RETIRE-CODE2       PIC X.                           CPWSXTTO
003100     05  XTTO-RETIRE-CODE3       PIC X.                           CPWSXTTO
003200     05  FILLER                  PIC XX.                          CPWSXTTO
003300     05  XTTO-OLD-TTL-CODE       PIC X(4).                        CPWSXTTO
003400     05  XTTO-STATUS             PIC X.                           CPWSXTTO
003500     05  XTTO-STATUS-DATE        PIC X(6).                        CPWSXTTO
003600     05  XTTO-ACAD-GROUP         PIC X(3).                        CPWSXTTO
003700     05  XTTO-SABB               PIC X.                           CPWSXTTO
003800     05  XTTO-ACAD-SERV-LIM      PIC XX.                          CPWSXTTO
003900     05  XTTO-ACAD-APP-TYPE      PIC X.                           CPWSXTTO
004000     05  XTTO-ACAD-RANK          PIC X.                           CPWSXTTO
004100     05  XTTO-ACAD-INSTR-RANK    PIC X(4).                        CPWSXTTO
004200     05  XTTO-STAFF-GRP-CODE     PIC XX.                          CPWSXTTO
004300     05  FILLER                  PIC X(4).                        CPWSXTTO
004400     05  XTTO-DATE-LAST-REVD     PIC X(6).                        CPWSXTTO
004500     05  FILLER                  PIC X(30).                       CPWSXTTO
004600*                                                                 CPWSXTTO
