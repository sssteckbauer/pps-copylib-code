000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWSXRTD                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    INITIAL INSTALLATION OF COPYMEMBER.                     *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100*    COPYID=CPWSXRTD                                              CPWSXRTD
001200*01  XRTD-ROUTINE-DEFINITION-TABLE.                               CPWSXRTD
001300*-----------------------------------------------------------------CPWSXRTD
001400*         R O U T I N E   D E F I N I T I O N   T A B L E         CPWSXRTD
001500*-----------------------------------------------------------------CPWSXRTD
001600     03  XRTD-DELETE                PIC X.                        CPWSXRTD
001700     03  XRTD-KEY-AREA.                                           CPWSXRTD
001800         05  XRTD-KEY-CONSTANT      PIC X(5).                     CPWSXRTD
001900         05  XRTD-KEY-RTN-TYP-NUM.                                CPWSXRTD
002000             07  XRTD-KEY-RTN-TYPE  PIC X(1).                     CPWSXRTD
002100             07  XRTD-KEY-RTN-NUM   PIC 9(3).                     CPWSXRTD
002200         05  XRTD-KEY-FILLER        PIC X(4).                     CPWSXRTD
002300     03  XRTD-DATA-AREA.                                          CPWSXRTD
002400         05  XRTD-CALL-MODULE       PIC X(8).                     CPWSXRTD
002500         05  XRTD-MODULE-DESC       PIC X(30).                    CPWSXRTD
002600         05  XRTD-EFF-DATE          PIC X(10).                    CPWSXRTD
002700         05  XRTD-STRT-DATE         PIC X(10).                    CPWSXRTD
002800         05  XRTD-STOP-DATE         PIC X(10).                    CPWSXRTD
002900         05  XRTD-STATUS            PIC X(1).                     CPWSXRTD
003000         05  FILLER                 PIC X(141).                   CPWSXRTD
