000000**************************************************************/   16110865
000001*  COPYMEMBER: CPWSEBSA                                      */   16110865
000002*  RELEASE: ___0865______ SERVICE REQUEST(S): ____11611____  */   16110865
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___12/13/93__  */   16110865
000004*  DESCRIPTION:                                              */   16110865
000005*  INCREASE THE BENEFIT SEGMENT ENTRIES FROM 300 TO 999.     */   16110865
000006*                                                            */   16110865
000007**************************************************************/   16110865
000100**************************************************************/  *36220651
000200*  COPYMEMBER: CPWSEBSA                                      */  *36220651
000300*  RELEASE # ___0651_____ SERVICE REQUEST NO(S)___3622_______*/  *36220651
000400*  NAME ______KXK______   MODIFICATION DATE ____04/13/92_____*/  *36220651
000500*  DESCRIPTION                                               */  *36220651
000600*   - ADDED DED-EFF-DATE OT COPYLIB TO REFLECT ADDITION      */  *36220651
000600*     OF DED_EFF_DATE COLUMN TO BRS TABLE.                   */  *36220651
000700**************************************************************/  *36220651
000100**************************************************************/  *36090553
000200*  COPYMEMBER: CPWSEBSA                                      */  *36090553
000300*  RELEASE # ____0553____ SERVICE REQUEST NO(S)___3609_______*/  *36090553
000400*  NAME ______JAG______   MODIFICATION DATE ____04/08/91_____*/  *36090553
000500*  DESCRIPTION                                               */  *36090553
000600*   - MEMBER CREATED TO STORE THE BENEFIT   SEGMENT ARRAY.   */  *36090553
000700**************************************************************/  *36090553
000800*    COPYID=CPWSEBSA                                              CPWSEBSA
000900******************************************************************CPWSEBSA
001000*    COPY MEMBER USED TO STORE THE BENEFIT   SEGMENT ARRAY TABLE *CPWSEBSA
001100*    MAXIMUN ENTRIES OF TABLE ARE STORED IN PAYROLL CONSTANT (IDC*CPWSEBSA
001200*    MEMBER.  ELEMENT # ON DEDUCTION IS DIRECT POINTER TO TABLE  *CPWSEBSA
001300*    POSITION - THE 8 LEVELS IN SECOND OCCURANCE ARE DEFINED IN  *CPWSEBSA
001400*    AREA CALLED BALANCE-DATA-ARRAY - COPY ALSO HAS MAX ENTRIES  *CPWSEBSA
001500*    ON 6N00 SEGMENT & COUNT SUBSCRIPT AREAS                     *CPWSEBSA
001600******************************************************************CPWSEBSA
001700*                                                                 CPWSEBSA
001800*01  BENEFIT-SEGMENT-ARRAY.                                       CPWSEBSA
001900     05  FILLER                  PIC X(17).                       CPWSEBSA
002000*                                                                 CPWSEBSA
002100*     NOTE: THE CLEAR ARRAY AND SEGMENT ARRAY MUST BE THE SAME    CPWSEBSA
002200*           SIZE. AND MUST NOT BE LESS THAN THE MAXIMUM NUMBER OF CPWSEBSA
002300*           ENTRIES IN INSTALLATION CONSTANTS IDC-MAX-NO-GTN      CPWSEBSA
002400*                                                                 CPWSEBSA
002500     05  EBSA-BRSC-SEGMNT.                                        CPWSEBSA
002600*******07  EBSA-BRSC OCCURS 300 TIMES.                            16110865
002610       07  EBSA-BRSC OCCURS 999 TIMES.                            16110865
002700         10  EBSA-BRSC-AREA.                                      36220651
002800            15  EBSA-BRSC-ARRAY.                                  36220651
002900                20  EBSA-BRSC-TUC   PIC XX.                       36220651
003000                20  EBSA-BRSC-REP   PIC X.                        36220651
003100                20  EBSA-BRSC-SHC   PIC X.                        36220651
003200                20  EBSA-BRSC-DUC   PIC X.                        36220651
003300            15  EBSA-DED-EFF-DATE   PIC X(10).                    36220651
003200         10  FILLER              PIC X.                           CPWSEBSA
003300*                                                                 CPWSEBSA
003400     05  EBSA-UPDATED-ELEMENT-ARRAY.                              CPWSEBSA
003700*********10  EBSA-ELEMENT-UPDATED-SW OCCURS 300 TIMES PIC 9.      16110865
003800         10  EBSA-ELEMENT-UPDATED-SW OCCURS 999 TIMES PIC 9.      16110865
