000100**************************************************************/   36220651
000200*  COPYMEMBER: CPWSXBS2                                      */   36220651
000300*  RELEASE # ___0651_____ SERVICE REQUEST NO(S)___3622_______*/   36220651
000400*  NAME __________KXK__   MODIFICATION DATE ____04/13/92_____*/   36220651
000600*  DESCRIPTION                                               */   36220651
000700*   - ADDED DEDUCTION EFFECTIVE DATE TO REFLECT ADDITION     */   36220651
000700*     OF DED_EFF_DATE COLUMN TO BRS TABLE.                   */   36220651
000900**************************************************************/   36220651
000100**************************************************************/   36090564
000200*  COPYMEMBER: CPWSXBS2                                      */   36090564
000300*  RELEASE # ____0564____ SERVICE REQUEST NO(S)___3609_______*/   36090564
000400*  NAME __________KXK__   MODIFICATION DATE ____00/00/00_____*/   36090564
000600*  DESCRIPTION                                               */   36090564
000700*   - THIS IS A NEW COPYMEMBER FOR PERFORMANCE ENCHANCEMENT. */   36090564
000700*   * THIS COPYMEMBER DUPLICATES THE DEFINITIONS IN          */   36090564
000700*     COPYMEMBER CPWSXBSA.  WHENEVER EITHER COPY MEMBER IS   */   36090564
000700*     CHANGED, BOTH MUST BE CHANGED TO MAINTAIN SYNCHRON-    */   36090564
000700*     IZATION.                                               */   36090564
000900**************************************************************/   36090564
001000*    COPYID=CPWSXBS2                                              CPWSXBS2
001100*                                                                 CPWSXBS2
001200*01  BENEFIT-SEGMENT-ARRAY.                                       CPWSXBS2
001300     05  XBSA-SEGCNT             PIC S9(3)    COMP-3.             CPWSXBS2
001400     05  XBSA-ENTRY-CNT          PIC S9(3)    COMP-3.             CPWSXBS2
001500     05  XBSA-CHECK-SUB          PIC S9(3)    COMP.               CPWSXBS2
001600     05  XBSA-UNLOAD-SUB         PIC S9(3)    COMP.               CPWSXBS2
001700     05  XBSA-CLEAR-SUB          PIC S9(3)    COMP.               CPWSXBS2
001800     05  XBSA-LOAD-SUB           PIC  9(3)    COMP.               CPWSXBS2
001900*                                                                 CPWSXBS2
002000     05  XBSA-CLEAR-ENTRY        PIC X(5).                        CPWSXBS2
002100*                                                                 CPWSXBS2
002200     05  XBSA-BRSC-SEGMNT.                                        CPWSXBS2
002300       07 XBSA-BRSC              OCCURS 300  TIMES.               CPWSXBS2
002400         10  XBSA-BRSC-AREA.                                      36220651
002400           15  XBSA-BRSC-ARRAY.                                   36220651
002500               20  XBSA-BRSC-TUC   PIC XX.                        36220651
002600               20  XBSA-BRSC-REP   PIC X.                         36220651
002700               20  XBSA-BRSC-SHC   PIC X.                         36220651
002800               20  XBSA-BRSC-DUC   PIC X.                         36220651
002400           15  XBSA-DED-EFF-DATE   PIC X(10).                     36220651
002900         10  FILLER              PIC X.                           CPWSXBS2
003000*                                                                 CPWSXBS2
003100     05  XBSA-UPDATED-ELEMENT-ARRAY.                              CPWSXBS2
003200         10  XBSA-ELEMENT-UPDATED-SW OCCURS 300 TIMES PIC 9.      CPWSXBS2
