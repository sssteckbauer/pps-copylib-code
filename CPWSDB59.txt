000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSDB59                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPWSDB59                                              CPWSDB59
000200*01  XDBS-BOND-INFORMATION.                                       30930413
000300******************************************************************CPWSDB59
000400*       B O N D    I N F O R M A T I O N    S E G M E N T        *CPWSDB59
000500******************************************************************CPWSDB59
000600     03  XDBS-DELETE-59              PICTURE X.                   CPWSDB59
000700     03  XDBS-KEY-XX.                                             CPWSDB59
000800         05  XDBS-ID-NUMBER-XX       PICTURE X(9).                CPWSDB59
000900         05  XDBS-SEGMENT-XX         PICTURE X(4).                CPWSDB59
001000*           (NONE OF THE ABOVE FIELDS SHOULD BE SPECIFICALLY     *CPWSDB59
001100*            ADDRESSED UNLESS THE 0100--WHICH CONTAINS UNIQUE    *CPWSDB59
001200*            DATA NAMES--IS NOT BEING USED.                      *CPWSDB59
001300*                                                                *CPWSDB59
001400     03  XDBS-BOND-INFORMTN-ELEMENTS.                             CPWSDB59
001500         05  XDBS-BOND-PURCHS-PRICE  PICTURE S9(3)V99 COMP-3.     CPWSDB59
001600         05  XDBS-BOND-OWNER         PICTURE X(30).               CPWSDB59
001700         05  XDBS-BOND-OWNER-SS-NO   PICTURE X(9).                CPWSDB59
001800         05  XDBS-BOND-OWNER-ADDR.                                CPWSDB59
001900             07  XDBS-BOND-OWNR-STR  PICTURE X(30).               CPWSDB59
002000             07  XDBS-BOND-OWNR-CITY PICTURE X(13).               CPWSDB59
002100             07  XDBS-BOND-OWNR-ST   PICTURE XX.                  CPWSDB59
002200             07  XDBS-BOND-OWNR-ZIP  PICTURE X(5).                CPWSDB59
002300         05  XDBS-CO-OWN-OR-BENEFIC  PICTURE X.                   CPWSDB59
002400         05  XDBS-CO-OWN-BENEF-NAME  PICTURE X(30).               CPWSDB59
002500         05  XDBS-CO-OWN-BENEF-SS    PICTURE X(9).                CPWSDB59
002600*                                                                 CPWSDB59
002700     03  FILLER                      PICTURE X(76).               CPWSDB59
002800*                                                                 CPWSDB59
002900*                                                                 CPWSDB59
