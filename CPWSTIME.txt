000101**************************************************************/   36430795
000200*  COPYMEMBER: CPWSTIME                                      */   36430795
000300*  RELEASE: ___0795______ SERVICE REQUEST(S): _____3643____  */   36430795
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __07/15/93____ */   36430795
000500*  DESCRIPTION:                                              */   36430795
000600*  CONVERT TO DUAL USE TIME AQUIRE; CALL CEELOCT             */   36430795
000700**************************************************************/   36430795
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSTIME                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - NEW MEMBER CREATED FOR CONVERSION FROM OS/VS COBOL     */   30930413
000700*     TO VS COBOL II.                                        */   30930413
000900**************************************************************/   30930413
000100***************************************************************   CPWSTIME
000200*  COPY MODULE:  CPWSTIME                                     *   CPWSTIME
000300*  REMARKS: PRE-EDIT-TIME FORMAT (HHMMSSHS)                   *   CPWSTIME
000500***************************************************************   CPWSTIME
000700     05  PRE-EDIT-TIME.                                           CPWSTIME
000710         10  TIME-HHMMSS.                                         CPWSTIME
000800             15  TIME-HH         PICTURE X(02)  VALUE SPACE.      CPWSTIME
000900             15  TIME-MM         PICTURE X(02)  VALUE SPACE.      CPWSTIME
001000             15  TIME-SS         PICTURE X(02)  VALUE SPACE.      CPWSTIME
001010         10  TIME-HS             PICTURE X(02)  VALUE SPACE.      CPWSTIME
001700                                                                  CPWSTIME
002700     05  TCEELOCT-DATE.                                           36430795
002800         10  TCEELOCT-PGM-NAME   PICTURE X(08)  VALUE 'CEELOCT'.  36430795
002900         10  TCEELOCT-L          PICTURE S9(9) COMP.              36430795
003000         10  TCEELOCT-S          COMP-2.                          36430795
003100         10  TCEELOCT-G          PICTURE X(17).                   36430795
003200         10  TCEELOCT-FC         PICTURE X(12).                   36430795
