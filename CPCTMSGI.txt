000000**************************************************************/   30871401
000001*  COPYMEMBER: CPCTMSGI                                      */   30871401
000002*  RELEASE: ___1401______ SERVICE REQUEST(S): _____3087____  */   30871401
000003*  NAME:_____SRS_________ CREATION DATE:      ___03/20/02__  */   30871401
000004*  DESCRIPTION:                                              */   30871401
000005*  - NEW COPY MEMBER FOR MESSAGE TABLE INPUT                 */   30871401
000007**************************************************************/   30871401
006400*    COPYID=CPCTMSGI                                              CPCTMSGI
011500*01  MESSAGE-TABLE-INPUT.                                         CPCTMSGI
019630     05 MSGI-NUMBER.                                              CPCTMSGI
019640        10 MSGI-PROG-ID                  PIC XX.                  CPCTMSGI
019640        10 MSGI-MSG-NO                   PIC XXX.                 CPCTMSGI
019670     05 MSGI-DATA-1.                                              CPCTMSGI
019730        10 MSGI-REFERENCE                PIC X.                   CPCTMSGI
019730        10 MSGI-SEVERITY                 PIC X.                   CPCTMSGI
019730        10 MSGI-TURNAROUND               PIC X.                   CPCTMSGI
019730        10 MSGI-BATCH-SEVERITY           PIC X.                   CPCTMSGI
019730        10 MSGI-ONLIN-SEVERITY           PIC X.                   CPCTMSGI
019730        10 MSGI-TEXT                     PIC X(65).               CPCTMSGI
019670     05 MSGI-DATA-2.                                              CPCTMSGI
019730        10 MSGI-ROUTING-CODES OCCURS 3   PIC X.                   CPCTMSGI
