000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXOCR                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100*    COPYID=CPWSXOCR                                              CPWSXOCR
000200*01  XOCR-CHRON-REC.                                              30930413
000300     03 XOCR-KEY.                                                 CPWSXOCR
000400        05 XOCR-ID-NUM             PIC X(9).                      CPWSXOCR
000500        05 XOCR-EFF-DT             PIC X(6).                      CPWSXOCR
000600        05 XOCR-ENT-DT             PIC X(6).                      CPWSXOCR
000700        05 XOCR-SEQ-NO             PIC X(2).                      CPWSXOCR
000800        05 XOCR-BAT-ORG            PIC X(6).                      CPWSXOCR
000900       03 FILLER.                                                 CPWSXOCR
001000        05 XOCR-ACTIONS.                                          CPWSXOCR
001100           07 XOCR-ACT1            PIC XX.                        CPWSXOCR
001200           07 FILLER               PIC X(18).                     CPWSXOCR
001300     03 XOCR-STRING-SIZE         PIC 9(5) COMP-3.                 CPWSXOCR
001400     03 XOCR-DATA.                                                CPWSXOCR
001500        05 XOCR-ENTRY PIC X OCCURS 9 TO 1458                      CPWSXOCR
001600              DEPENDING ON XOCR-STRING-SIZE.                      CPWSXOCR
