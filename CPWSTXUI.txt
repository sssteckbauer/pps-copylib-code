000000**************************************************************/   48571379
000001*  COPYMEMBER: CPWSTXUI                                      */   48571379
000002*  RELEASE: ___1379______ SERVICE REQUEST(S): ____14857____  */   48571379
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___10/15/01__  */   48571379
000004*  DESCRIPTION:                                              */   48571379
000005*  ADDED  EMPLOYEE NAME FIELDS FOR EMPLOYEE  FIRST NAME,     */   48571379
000006*  MIDDLE NAME, AND LAST NAME.                               */   48571379
000007**************************************************************/   48571379
000000**************************************************************/   48121223
000001*  COPYMEMBER: CPWSTXUI                                      */   48121223
000002*  RELEASE: ___1223______ SERVICE REQUEST(S): ____14812____  */   48121223
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___12/02/98__  */   48121223
000004*  DESCRIPTION:                                              */   48121223
000005*  -  ADDED YTD DPI GROSS TO UI EXTRACT RECORD.              */   48121223
000007**************************************************************/   48121223
000000**************************************************************/   34161098
000001*  COPYMEMBER: CPWSTXUI                                      */   34161098
000002*  RELEASE: ___1098______ SERVICE REQUEST(S): ____13416____  */   34161098
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___10/16/96__  */   34161098
000004*  DESCRIPTION:                                              */   34161098
000005*  -  ADDED QTD CALIF STATE WITHHOLDING TAXABLE GROSS,       */   34161098
000006*     EXECUTIVE LIFE INSURANCE IMPUTED INCOME, YTD OTHER     */   34161098
000007*     INCOME, AND YTD NON-CASH FRINGE BENEFITS.              */   34161098
000008**************************************************************/   34161098
000000**************************************************************/   17860963
000001*  COPYMEMBER: CPWSTXUI                                      */   17860963
000002*  RELEASE: ___0963______ SERVICE REQUEST(S): ____11786____  */   17860963
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___01/26/95__  */   17860963
000004*  DESCRIPTION:                                              */   17860963
000005*  -  ADDED QTD CALIF STATE WITHHOLDING TAX.                 */   17860963
000005*  -  ADDED TOTALS FOR EMPLOYEES WHO HAVE UI WAGES AND       */   17860963
000005*           TOTALS FOR EMPLOYEES WHO HAVE CA SWT TAXES ONLY. */   17860963
004300*  -  REMOVED TXUI-TLR-YTD-WAGES - NO LONGER USED (REL 683). */   17860963
000007**************************************************************/   17860963
000000**************************************************************/   45230683
000001*  COPYMEMBER: CPWSTXUI                                      */   45230683
000002*  RELEASE: ___0683______ SERVICE REQUEST(S): _____4523____  */   45230683
000003*  NAME:___H. TRUONG_____ MODIFICATION DATE:  ___05/22/92__  */   45230683
000004*  DESCRIPTION:                                              */   45230683
000005*  -REPLACED UNUSED FIELD WITH FILLER, TXUI-EMP-YTD-WAGES.   */   45230683
000006*                                                            */   45230683
000007**************************************************************/   45230683
000100**************************************************************/   45380437
000200*  COPYMEMBER: CPWSTXUI                                      */   45380437
000300*  RELEASE: ____0437____  SERVICE REQUEST(S): ____4538____   */   45380437
000400*                                             ____4546____   */   45380437
000500*                                             ____5135____   */   45380437
000600*                                             ____5137____   */   45380437
000700*                                             ____5143____   */   45380437
000800*  NAME:    __J.WILCOX__  CREATION DATE:      __11/08/89__   */   45380437
000900*  DESCRIPTION:                                              */   45380437
001000*                                                            */   45380437
001100*    - INITIAL RELEASE OF COPYMEMBER PROVIDING RECORD        */   45380437
001200*      LAYOUT FOR THE TAX REPORTING UNEMPLOYMENT INSURANCE   */   45380437
001300*      (UI) EXTRACT FILE.                                    */   45380437
001400**************************************************************/   45380437
001500*01  TXUI-EXTRACT-RECORD.                                         CPWSTXUI
001600     05  TXUI-EMP-ID                       PIC X(09).             CPWSTXUI
001700         88  TXUI-HEADER-RECORD              VALUE ZEROES.        CPWSTXUI
001800         88  TXUI-TRAILER-RECORD             VALUE ALL '9'.       CPWSTXUI
001900     SKIP1                                                        CPWSTXUI
002000     05  TXUI-EMP-DATA.                                           CPWSTXUI
002100         10  TXUI-EMP-NAME                 PIC X(26).             CPWSTXUI
004331         10  TXUI-EMP-FIRST-NAME           PIC X(30).             48571379
004332         10  TXUI-EMP-MIDDLE-NAME          PIC X(30).             48571379
004333         10  TXUI-EMP-LAST-NAME            PIC X(30).             48571379
002200         10  TXUI-EMP-SSN                  PIC X(09).             CPWSTXUI
002400         10  TXUI-EMP-QTD-WAGES            PIC S9(07)V99 COMP-3.  CPWSTXUI
002500         10  TXUI-EMP-QTD-REDUCTIONS       PIC S9(07)V99 COMP-3.  CPWSTXUI
002510         10  TXUI-EMP-QTD-CA-SWT           PIC S9(07)V99 COMP-3.  17860963
004336         10  TXUI-EMP-QTD-CA-SWT-GRS       PIC S9(07)V99 COMP-3.  34161098
004337         10  TXUI-EMP-YTD-EXECLIFE-INC     PIC S9(07)V99 COMP-3.  34161098
004338         10  TXUI-EMP-YTD-OTHER-INCOME     PIC S9(07)V99 COMP-3.  34161098
004339         10  TXUI-EMP-YTD-NONCASH-FRINGE   PIC S9(07)V99 COMP-3.  34161098
004340         10  TXUI-EMP-YTD-DPI-GROSS        PIC S9(07)V99 COMP-3.  48121223
002600         10  TXUI-EMP-EXCEPTION-FLAG       PIC X(01).             CPWSTXUI
002700             88  TXUI-EMP-EXCEPTION          VALUE 'Y'.           CPWSTXUI
002800     SKIP1                                                        CPWSTXUI
002900     05  TXUI-HDR-DATA                     REDEFINES              CPWSTXUI
003000         TXUI-EMP-DATA.                                           CPWSTXUI
003100         10  TXUI-HDR-RPT-PERIOD.                                 CPWSTXUI
003200             15  TXUI-HDR-QUARTER          PIC 9(01).             CPWSTXUI
003300             15  TXUI-HDR-YEAR             PIC 9(02).             CPWSTXUI
003400         10  TXUI-HDR-CREATION-DATE        PIC X(08).             CPWSTXUI
003500         10  TXUI-HDR-CREATION-TIME        PIC X(08).             CPWSTXUI
003600         10  TXUI-HDR-FILE-VERSION         PIC 9(02).             CPWSTXUI
004354*****                                                          CD 48571379
004355*****    10  FILLER                        PIC X(55).             48571379
004356         10  FILLER                        PIC X(145).            48571379
003800     SKIP1                                                        CPWSTXUI
003900     05  TXUI-TLR-DATA                     REDEFINES              CPWSTXUI
004000         TXUI-EMP-DATA.                                           CPWSTXUI
004100         10  TXUI-TLR-EMPLOYEE-COUNT       PIC S9(07)    COMP-3.  CPWSTXUI
004100         10  TXUI-TLR-EMP-UI-CT            PIC S9(07)    COMP-3.  17860963
004100         10  TXUI-TLR-EMP-CASWT-CT         PIC S9(07)    COMP-3.  17860963
004200         10  TXUI-TLR-EXCEPTION-COUNT      PIC S9(07)    COMP-3.  CPWSTXUI
004400         10  TXUI-TLR-QTD-WAGES            PIC S9(11)V99 COMP-3.  CPWSTXUI
004500         10  TXUI-TLR-QTD-REDUCTIONS       PIC S9(11)V99 COMP-3.  CPWSTXUI
004501         10  TXUI-TLR-QTD-CA-SWT           PIC S9(11)V99 COMP-3.  17860963
004502         10  TXUI-TLR-QTD-CA-SWT-GRS       PIC S9(11)V99 COMP-3.  34161098
004800*****                                                          CD 48571379
005000*****    10  FILLER                        PIC X(32).             48571379
005100         10  FILLER                        PIC X(122).            48571379
