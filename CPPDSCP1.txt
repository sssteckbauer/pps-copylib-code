000100**************************************************************/   36190541
000200*  COPYMEMBER: CPPDSCP1                                      */   36190541
000300*  RELEASE: ____0541____  SERVICE REQUEST(S): ____3619____   */   36190541
000400*  NAME:    _C._RIDLION_  CREATION DATE:      __02/28/91__   */   36190541
000500*  DESCRIPTION:                                              */   36190541
000600*    - NEW FOR EDB ONLINE INQUIRY                            */   36190541
000700**************************************************************/   36190541
000800**********************************************************        CPPDSCP1
000900* THIS PERFORM IS USED TO TEST IF THE CURRENT FUNCTION IS         CPPDSCP1
001000* THE USERS SCRIPT.                                               CPPDSCP1
001100**********************************************************        CPPDSCP1
001200                                                                  CPPDSCP1
001300     PERFORM VARYING SCRIPT-SUB FROM 1 BY 1                       CPPDSCP1
001400                                UNTIL SCRIPT-SUB > 36             CPPDSCP1
001500                                OR SCRIPT-FND                     CPPDSCP1
001600        IF CA-SCRIPT-SEQ (SCRIPT-SUB) = WS-THIS-FUNCTION          CPPDSCP1
001700           MOVE 'Y' TO WS-SCRIPT-FND-FLAG                         CPPDSCP1
001800           MOVE    SCRIPT-SUB  TO CA-SCRIPT-POSN                  CPPDSCP1
001900        END-IF                                                    CPPDSCP1
002000     END-PERFORM.                                                 CPPDSCP1
002100                                                                  CPPDSCP1
002200     IF NOT SCRIPT-FND                                            CPPDSCP1
002300        MOVE  ZERO   TO CA-SCRIPT-POSN                            CPPDSCP1
002400     END-IF.                                                      CPPDSCP1
002500                                                                  CPPDSCP1
