000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNF015                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_P. THOMPSON_____ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*    Initial release of linkage copymember between calling   */   32021138
000700*    program and module PPFAU015.                            */   32021138
000800**************************************************************/   32021138
000900*                                                             *   CPLNF015
001000*  This copymember is one of the Full Accounting Unit modules *   CPLNF015
001100*  which campuses may need to modify to accomodate their own  *   CPLNF015
001200*  Chart of Accounts structure.                               *   CPLNF015
001300*                                                             *   CPLNF015
001400*  In particular, depending upon the need for validation at   *   CPLNF015
001500*  various levels, campuses may need fewer or more levels of  *   CPLNF015
001600*  validation failure.                                        *   CPLNF015
001700*                                                             *   CPLNF015
001800*  The base version indicates one possible level of failure:  *   CPLNF015
001900*    Level 1 - the Account or Fund is missing.                *   CPLNF015
001900*              Per the failure, a PPPMSG number will be passed*   CPLNF015
001900*              via failure text; the number of returned       *   CPLNF015
001900*              messages will be in F015-MSG-COUNT.            *   CPLNF015
002100***************************************************************   CPLNF015
002200*                                                                 CPLNF015
002300*01  PPFAU011-INTERFACE.                                          CPLNF015
002400    05  F015-INPUTS.                                              CPLNF015
002500        10  F015-FAU                     PIC X(30).               CPLNF015
002600    05  F015-OUTPUTS.                                             CPLNF015
002700        10  F015-RETURN-VALUE            PIC X(01).               CPLNF015
002800            88  F015-FAU-VALID             VALUE '0'.             CPLNF015
002900            88  F015-INVALID-LEVEL-1       VALUE '1'.             CPLNF015
003000            88  F015-ROUTINE-FAILURE       VALUE 'X'.             CPLNF015
003200        10  F015-FAILURE-TEXT            PIC X(30).               CPLNF015
003200        10  F015-MSG-COUNT               PIC S9(4) COMP.          CPLNF015
003200        10  F015-MESSAGES.                                        CPLNF015
003200            15  FILLER OCCURS 2 TIMES.                            CPLNF015
002500                20  F015-MSG             PIC X(05).               CPLNF015
