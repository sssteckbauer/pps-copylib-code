000100**************************************************************/   73151304
000200*  COPYMEMBER: CPWSXCHR                                      */   73151304
000300*  RELEASE: ____1304____  SERVICE REQUEST(S): ____3087____   */   73151304
000400*  NAME:    __J.WILCOX__  MODIFICATION DATE:  __04/20/90__   */   73151304
000500*  DESCRIPTION:                                              */   73151304
000600*                                                            */   73151304
000700*    - REPLACEMENT COPYMEMBER WITH CONTROL FILE MAINTENANCE  */   73151304
000800*      REPORT HEADINGS MODIFIED FOR THE PPP010 REWRITE       */   73151304
000900*      PROJECT.                                              */   73151304
001000**************************************************************/   73151304
000100*    COPYID=CPWSXCHR                                              CPWSXCHR
000200******************************************************************CPWSXCHR
000300*            CONTROL FILE MAINTENANCE HEADING FORMATS             CPWSXCHR
000400******************************************************************CPWSXCHR
001500*01  CTH-CTL-TABLE-MAINT-HEADERS.                                 CPWSXCHR
001600     05  CTH-RPT-HD1.                                             CPWSXCHR
001700         10  FILLER             PICTURE X      VALUE SPACE.       CPWSXCHR
001800         10  CTH-RPT-ID         PICTURE X(8)   VALUE SPACES.      CPWSXCHR
001900         10  FILLER             PICTURE X      VALUE '/'.         CPWSXCHR
002000         10  CTH-PROG-ID        PICTURE X(8)   VALUE SPACES.      CPWSXCHR
002100         10  FILLER             PICTURE X      VALUE '/'.         CPWSXCHR
002200         10  CTH-COMPILE-DATE                  VALUE SPACES.      CPWSXCHR
002300             15  CTH-COMPILE-MM PICTURE 99.                       CPWSXCHR
002400             15  CTH-COMPILE-DD PICTURE 99.                       CPWSXCHR
002500             15  CTH-COMPILE-YY PICTURE 99.                       CPWSXCHR
002600         10  FILLER             PICTURE X(10)  VALUE SPACES.      CPWSXCHR
002700         10  CTH-RPT-INST       PICTURE X(64)  VALUE SPACES.      CPWSXCHR
002800         10  FILLER             PICTURE X(13)  VALUE SPACES.      CPWSXCHR
002900         10  FILLER             PICTURE X(14)  VALUE 'Page No.'.  CPWSXCHR
003000         10  CTH-PAGE-NO        PICTURE ZZZ,ZZ9.                  CPWSXCHR
003100     05 CTH-RPT-HD2.                                              CPWSXCHR
003200         10  FILLER             PICTURE X      VALUE SPACE.       CPWSXCHR
003300         10  CTH-RPT-RETN       PICTURE X(34)  VALUE              CPWSXCHR
003400             'RETN: See Rpts Disp Schedule/Dist.'.                CPWSXCHR
003500         10  FILLER             PICTURE X(20)  VALUE SPACES.      CPWSXCHR
003600         10  CTH-FUNC-AREA      PICTURE X(24)  VALUE              CPWSXCHR
003700             'Control File Maintenance'.                          CPWSXCHR
003800         10  FILLER             PICTURE X(33)  VALUE SPACES.      CPWSXCHR
003900         10  FILLER             PICTURE X(13)  VALUE 'Run Date'.  CPWSXCHR
004000         10  CTH-RPT-DATE       PICTURE X(8)   VALUE SPACES.      CPWSXCHR
004100     05  CTH-RPT-HD3.                                             CPWSXCHR
004200         10  FILLER             PICTURE X      VALUE SPACE.       CPWSXCHR
004300         10  CTH-RPT-INFO.                                        CPWSXCHR
004400             15 CTH-RPT-COMMENT PICTURE X(24)  VALUE SPACES.      CPWSXCHR
004500             15 CTH-MAINT-DATE  PICTURE X(8)   VALUE SPACES.      CPWSXCHR
004600         10  FILLER             PICTURE X(14)  VALUE SPACES.      CPWSXCHR
004700         10  CTH-RPT-TTL        PICTURE X(40)  VALUE SPACES.      CPWSXCHR
004800         10  FILLER             PICTURE X(25)  VALUE SPACES.      CPWSXCHR
004900         10  FILLER             PICTURE X(13)  VALUE 'Run Time'.  CPWSXCHR
005000         10  CTH-RPT-TIME       PICTURE X(8)   VALUE SPACES.      CPWSXCHR
