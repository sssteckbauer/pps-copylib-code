000100**************************************************************/   36430976
000200*  COPYMEMBER: CPPDQFED                                      */   36430976
000300*  RELEASE # ____0976____ SERVICE REQUEST NO(S)___3643_______*/   36430976
000400*  NAME ______JLT______   MODIFICATION DATE ____04/18/95_____*/   36430976
000500*  DESCRIPTION:                                              */   36430976
000600*  - GENERIC PROCEDURE DIVISION CODE FOR PPXXXUPD MODULES    */   36430976
000700*    WHICH ARE ASSOCIATED WITH SET TRANSACTION DATA ELEMENTS */   36430976
000800*    BUT WHICH HAVE A DESCENDING UTL FETCH ORDER WHICH IS    */   36430976
000900*    NOT THE OCCURRENCE KEY.                                 */   36430976
001000**************************************************************/   36430976
001100*    COPYID=CPPDQFED                                              CPPDQFED
001200*                                                                 CPPDQFED
001300 9400-QUICK-FETCH  SECTION.                                       CPPDQFED
001400*                                                                 CPPDQFED
001500* * COUNT NUMBER OF ROWS IN THE ORIGINAL ARRAY                    CPPDQFED
001600     PERFORM                                                      CPPDQFED
001700      VARYING ORIG-X FROM +1 BY +1                                CPPDQFED
001800       UNTIL (ORIG-X > IDC-MAX-NO-000)                            CPPDQFED
001900          OR (000-OCCURRENCE-KEY                                  CPPDQFED
002000                      OF 000-ARRAY-HOLD (ORIG-X)                  CPPDQFED
002100            = 000-OCCURRENCE-KEY                                  CPPDQFED
002200                      OF INIT-000-ARRAY (1))                      CPPDQFED
002300     END-PERFORM.                                                 CPPDQFED
002400* * COUNT NUMBER OF ROWS IN THE UPDATED ARRAY                     CPPDQFED
002500     PERFORM                                                      CPPDQFED
002600      VARYING UPDT-X  FROM ORIG-X  BY +1                          CPPDQFED
002700       UNTIL (UPDT-X > IDC-MAX-NO-000)                            CPPDQFED
002800          OR (000-OCCURRENCE-KEY                                  CPPDQFED
002900                      OF 000-ARRAY (UPDT-X)                       CPPDQFED
003000            = 000-OCCURRENCE-KEY                                  CPPDQFED
003100                      OF INIT-000-ARRAY (1))                      CPPDQFED
003200     END-PERFORM.                                                 CPPDQFED
003300     SUBTRACT +1  FROM UPDT-X.                                    CPPDQFED
003400     IF UPDT-X  NOT > ZERO                                        CPPDQFED
003500* * *   THE EMPLOYEE HAS NO ROWS (BOTH ORIG AND UPDATED ARRAYS    CPPDQFED
003600* * *   SHOULD ALREADY BE EQUAL TO INITIAL VALUES                 CPPDQFED
003700         GO TO 9400-EXIT                                          CPPDQFED
003800     END-IF.                                                      CPPDQFED
003900     SUBTRACT +1  FROM ORIG-X.                                    CPPDQFED
004000* * SORT THE UPDATED ARRAY ENTRIES TO SIMULATE THE NORMAL RETURN  CPPDQFED
004100* * ORDER AS IF THEY WERE BEING RETURNED FROM THE "UTL" FETCH.    CPPDQFED
004200* * AS ORDER IS ESTABLISHED, THE UPDATED ROW IS MOVED TO ITS NEW  CPPDQFED
004300* * POSITION IN THE ORIGINAL ARRAY.                               CPPDQFED
004400     MOVE HIGH-VALUES TO LAST-LOADED-SRT-KEY                      CPPDQFED
004500     MOVE ZERO        TO RECEIVE-X.                               CPPDQFED
004600     SET     COMPLETE-SW-OFF    TO TRUE.                          CPPDQFED
004700     PERFORM  UNTIL COMPLETE-SW-ON                                CPPDQFED
004800        MOVE HIGH-VALUES TO NEXT-LOWEST-SRT-KEY                   CPPDQFED
004900        PERFORM VARYING TEMP-X  FROM +1 BY +1                     CPPDQFED
005000                UNTIL   TEMP-X > UPDT-X                           CPPDQFED
005100           IF      000-OCCURRENCE-KEY                             CPPDQFED
005200                        OF 000-ARRAY (TEMP-X)                     CPPDQFED
005300             NOT = 000-OCCURRENCE-KEY                             CPPDQFED
005400                        OF INIT-000-ARRAY (1)                     CPPDQFED
005500             MOVE  000-DATE                                       CPPDQFED
005600                        OF 000-ARRAY (TEMP-X)                     CPPDQFED
005700                           TO WORK-SRT-KEY-HI-ORDER               CPPDQFED
005800             MOVE  000-OCCURRENCE-KEY                             CPPDQFED
005900                        OF 000-ARRAY (TEMP-X)                     CPPDQFED
006000                           TO WORK-SRT-KEY-LO-ORDER               CPPDQFED
006100             IF WORK-SRT-KEY                                      CPPDQFED
006200                 < LAST-LOADED-SRT-KEY                            CPPDQFED
006300                IF NEXT-LOWEST-SRT-KEY = HIGH-VALUES              CPPDQFED
006400                OR WORK-SRT-KEY                                   CPPDQFED
006500                             >  NEXT-LOWEST-SRT-KEY               CPPDQFED
006600                   MOVE WORK-SRT-KEY                              CPPDQFED
006700                             TO NEXT-LOWEST-SRT-KEY               CPPDQFED
006800                   MOVE TEMP-X  TO NEXT-LOWEST-X                  CPPDQFED
006900                END-IF                                            CPPDQFED
007000             END-IF                                               CPPDQFED
007100           END-IF                                                 CPPDQFED
007200        END-PERFORM                                               CPPDQFED
007300        IF NEXT-LOWEST-SRT-KEY = HIGH-VALUES                      CPPDQFED
007400           SET COMPLETE-SW-ON  TO TRUE                            CPPDQFED
007500        ELSE                                                      CPPDQFED
007600           ADD +1  TO RECEIVE-X                                   CPPDQFED
007700           IF RECEIVE-X > IDC-MAX-NO-000                          CPPDQFED
007800              SET PPXXXUPD-ERROR   TO TRUE                        CPPDQFED
007900              SET COMPLETE-SW-ON   TO TRUE                        CPPDQFED
008000              MOVE IDC-MAX-NO-000  TO RECEIVE-X                   CPPDQFED
008100           END-IF                                                 CPPDQFED
008200           MOVE 000-ARRAY-ENTRY                                   CPPDQFED
008300                        OF 000-ARRAY (NEXT-LOWEST-X)              CPPDQFED
008400             TO 000-ARRAY-ENTRY                                   CPPDQFED
008500                        OF 000-ARRAY-HOLD (RECEIVE-X)             CPPDQFED
008600           MOVE NEXT-LOWEST-SRT-KEY TO LAST-LOADED-SRT-KEY        CPPDQFED
008700        END-IF                                                    CPPDQFED
008800     END-PERFORM.                                                 CPPDQFED
008900     MOVE RECEIVE-X    TO PP000UTL-ROW-COUNT.                     CPPDQFED
009000* * IF THE NEW COUNT OF NON-INITIAL ENTRIES NOW RESIDING IN THE   CPPDQFED
009100* * ORIGINAL ARRAY IS LESS THAN THE ORIGINAL COUNT, PAD THE       CPPDQFED
009200* * REMAINING ENTRIES WITH INITIAL ROW VALUES.                    CPPDQFED
009300     IF RECEIVE-X  < ORIG-X                                       CPPDQFED
009400        PERFORM  UNTIL RECEIVE-X  NOT < ORIG-X                    CPPDQFED
009500          ADD +1   TO RECEIVE-X                                   CPPDQFED
009600          MOVE 000-ARRAY-ENTRY                                    CPPDQFED
009700                        OF INIT-000-ARRAY (1)                     CPPDQFED
009800            TO 000-ARRAY-ENTRY                                    CPPDQFED
009900                        OF 000-ARRAY-HOLD (RECEIVE-X)             CPPDQFED
010000        END-PERFORM                                               CPPDQFED
010100     END-IF.                                                      CPPDQFED
010200     MOVE 000-ARRAY-HOLD     TO 000-ARRAY.                        CPPDQFED
010300     IF PPXXXUPD-DIAGNOSTICS-ON                                   CPPDQFED
010400         DISPLAY A-STND-PROG-ID '  '                              CPPDQFED
010500                 'PPXXXUPD-INTERFACE: ' PPXXXUPD-INTERFACE        CPPDQFED
010600     END-IF.                                                      CPPDQFED
010700 9400-EXIT.    EXIT.                                              CPPDQFED
010800* *        END OF COPY PROCEDURE                                  CPPDQFED
