000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPLNKCHD                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*  - THIS COPYMEMBER DEFINES THE LINKAGE TO/FROM PPKEYCHD    *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100*                                                                 CPLNKCHD
001200*01  KCHD-KEY-CHANGE-DELETION-LINKAGE.                            CPLNKCHD
001300     05  KCHD-REQUESTING-PROCESS           PIC X(02).             CPLNKCHD
001400         88  KCHD-BATCH-CALL                         VALUE 'BA'.  CPLNKCHD
001500         88  KCHD-ONLINE-CALL                        VALUE 'OL'.  CPLNKCHD
001600     05  KCHD-ACTION                       PIC X(01).             CPLNKCHD
001700         88  KCHD-VALID-ACTION    VALUE 'C' 'D' '1' '2'.          CPLNKCHD
001800         88  KCHD-CHANGE                         VALUE 'C'.       CPLNKCHD
001900         88  KCHD-DELETE                         VALUE 'D'.       CPLNKCHD
002000         88  KCHD-NAME-CHG-REC                   VALUE '1'.       CPLNKCHD
002100         88  KCHD-PRIOR-NAME-CHG-REC             VALUE '2'.       CPLNKCHD
002200     05  KCHD-FROM-ID-NUMBER               PIC X(09).             CPLNKCHD
002300     05  KCHD-TO-ID-NUMBER                 PIC X(09).             CPLNKCHD
002400     05  KCHD-NEW-NAME                     PIC X(26).             CPLNKCHD
002500     05  KCHD-EFF-DATE                     PIC X(06).             CPLNKCHD
002600*                                                                 CPLNKCHD
002700*****************   END OF COPYBOOK CPLNKCHD *********************CPLNKCHD
