000010**************************************************************/   DU039
000020*  COPY MEMBER: PARQMRYS                                     */   DU039
000030*  CHANGE #DU039            PROJECT REQUEST:   DU-039        */   DU039
000040*  NAME: MARI MCGEE         MODIFICATION DATE: 01/29/93      */   DU039
000050*                                                            */   DU039
000060*  DESCRIPTION:                                              */   DU039
000070*    CONVERSION TO IFIS CHART-OF-ACCOUNTS CONVENTIONS.       */   DU039
000090*                                                            */   DU039
000091**************************************************************/   DU039
000100******************************************************************PARQMRYS
000200*                                                                *PARQMRYS
000300*                P A R Q M R Y S                                 *PARQMRYS
000400*                                                                *PARQMRYS
000500*    THIS MEMBER DEFINES THE YEAR END EXTRACT FILE THAT IS USED  *PARQMRYS
000600*    BY THE PAR SYSTEM YEAR END REPORTING.  THREE TYPE RECORDS   *PARQMRYS
000700*    ARE ON THIS FILE:                                           *PARQMRYS
000800*    TYPE 1        TITLE CODE RECORD                             *PARQMRYS
000900*                         KEY IS CAMPUS CODE                     *PARQMRYS
001000*                                RECORD TYPE '1'                 *PARQMRYS
001100*                                TITLE CODE                      *PARQMRYS
001200*                                EMPLOYEE ID                     *PARQMRYS
001300*                                DISTRIBUTION LOCATION           *PARQMRYS
001400*    TYPE 2        ACCOUNT NUMBER RECORD                         *PARQMRYS
001500*                         KEY IS CAMPUS CODE                     *PARQMRYS
001600*                                RECORD TYPE '2'                 *PARQMRYS
001700*                                ACCOUNT NUMBER                  *PARQMRYS
001800*                                FUND TYPE                       *PARQMRYS
001900*                                FUND NUMBER                     *PARQMRYS
002000*                                EMPLOYEE ID                     *PARQMRYS
002100*                                DISTRIBUTION LOCATION           *PARQMRYS
002200*    TYPE 3        DEPARTMENT NUMBER RECORD                      *PARQMRYS
002300*                         KEY IS CAMPUS CODE                     *PARQMRYS
002400*                                RECORD TYPE '3'                 *PARQMRYS
002500*                                DEPARTMENT NUMBER               *PARQMRYS
002600*                                FUND TYPE                       *PARQMRYS
002700*                                FUND NUMBER                     *PARQMRYS
002800*                                FUNCTION CODE                   *PARQMRYS
002900*                                EMPLOYEE ID                     *PARQMRYS
003000*                                DISTRIBUTION LOCATION           *PARQMRYS
003100*                                                                *PARQMRYS
003200* NOTE: THIS COPY MEMBER WAS CHANGED 5/2/83 TO ACCOMODATE        *PARQMRYS
003300*       THE ADDITION OF SPA AND THE CHANGE TO QUARTERLY          *PARQMRYS
003400*       ONLY PROCESSING. 4 BYTES WERE ADDED AND TYPE-PAR-YS      *PARQMRYS
003500*       WAS CHANGED TO PROF,NON-PROF FROM MONTHLY,QUARTERLY      *PARQMRYS
003600*       NINE MONTH WAS NOT CHANGED.  (TMF)                       *PARQMRYS
003700*                                                                *PARQMRYS
003800******************************************************************PARQMRYS
003900                                                                  PARQMRYS
004000*01  EXTRACT-RCD-YEAR-END-YS.                                     PARQMRYS
004100                                                                  PARQMRYS
004200     05  KEY-YS.                                                  PARQMRYS
004300         10  CAMPUS-CODE-YS       PIC 99.                         PARQMRYS
004400         10  RCD-TYPE-YS          PIC X.                          PARQMRYS
004500             88  TITL-CD-RCD-YS   VALUE '1'.                      PARQMRYS
004600             88  ACCOUNT-RCD-YS   VALUE '2'.                      PARQMRYS
004700             88  DEPT-RCD-YS      VALUE '3'.                      PARQMRYS
004800         10  TITLE-CODE-YS        PIC 9(4).                       PARQMRYS
004900         10  FILLER               PIC X(9).                       PARQMRYS
005000         10  EMPLOYEE-ID-YS       PIC 9(9).                       PARQMRYS
005100         10  DIST-LOCTN-YS        PIC 9(6).                       PARQMRYS
005200     05  FILLER                   REDEFINES KEY-YS.               PARQMRYS
005300         10  FILLER               PIC X(3).                       PARQMRYS
005400*        10  ACCT-NUM-YS          PIC 9(6).                       DU039
005500*        10  ACCT-FUN-TYPE-YS     PIC 99.                         DU039
005600*        10  ACCT-FUN-NUM-YS      PIC 9(5).                       DU039
005610         10  IFIS-ACCT-INDEX-YS   PIC X(7).                       DU039
005620         10  IFIS-FUND-YS         PIC 9(5).                       DU039
005630         10  IFIS-FUND-X-YS       PIC X(1).                       DU039
005700         10  FILLER               PIC X(15).                      PARQMRYS
005800     05  FILLER                   REDEFINES KEY-YS.               PARQMRYS
005900         10  FILLER               PIC X(3).                       PARQMRYS
006000         10  DEPT-NUM-YS          PIC 9(4).                       PARQMRYS
006100         10  DEPT-FUN-TYPE-YS     PIC 99.                         PARQMRYS
006200         10  DEPT-FUN-NUM-YS      PIC 9(5).                       PARQMRYS
006300         10  DEPT-FUNCTN-CD-YS    PIC 99.                         PARQMRYS
006400         10  FILLER               PIC X(15).                      PARQMRYS
006500                                                                  PARQMRYS
006600     05  TYPE-PAR-YS              PIC X.                          PARQMRYS
006700         88  NON-PROF-YS          VALUE '2'.                      PARQMRYS
006800         88  PROF-YS              VALUE '1'.                      PARQMRYS
006900         88  NINE-MO-PAR-YS       VALUE 'N'.                      PARQMRYS
007000     05  RCD-DATE-YS              PIC 9(4).                       PARQMRYS
007100     05  FILLER                   REDEFINES RCD-DATE-YS.          PARQMRYS
007200         10  DATE-YY-YS           PIC 99.                         PARQMRYS
007300         10  DATE-MM-YS           PIC 99.                         PARQMRYS
007400                                                                  PARQMRYS
007500     05  DOLLAR-AMOUNTS-YS        COMP-3.                         PARQMRYS
007600         10  INST-DR-YS           PIC S9(7).                      PARQMRYS
007700         10  SPON-RES-YS          PIC S9(7).                      PARQMRYS
007800         10  COST-SHR-YS          PIC S9(7).                      PARQMRYS
007900         10  PUBLIC-SRVC-YS       PIC S9(7).                      PARQMRYS
008000         10  DA-YS                PIC S9(7).                      PARQMRYS
008100         10  GA-YS                PIC S9(7).                      PARQMRYS
008200         10  SPA-YS               PIC S9(7).                      PARQMRYS
008300         10  OTHER-YS             PIC S9(7).                      PARQMRYS
008400         10  TOTAL-YS             PIC S9(7).                      PARQMRYS
