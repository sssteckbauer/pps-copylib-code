000100**************************************************************/   13190159
000200*  COPY MODULE:  CPFDXTHF                                    */   13190159
000300*  RELEASE # ___0159___   SERVICE REQUEST NO(S)____1319______*/   13190159
000400*  NAME ___JAL_________   MODIFICATION DATE ____09/20/85_____*/   13190159
000500*  DESCRIPTION                                               */   13190159
000600*  FOR PHASE 2 OF THE PAYROLL FILE EXPANSION PROJECT,        */   13190159
000700*  USER-CODED TRANSACTION INPUT RECORD LENGTHS WILL BE       */   13190159
000800*  EXPANDED BY 22 BYTES, AND RECORDS AND WORK AREAS          */   13190159
000900*  RELATING TO THE EDB FILE WILL BE EXPANDED AS WELL.        */   13190159
001000**************************************************************/   13190159
001100     SKIP2                                                        13190159
001200*    COPYID=CPFDXTHF                                              CPFDXTHF
001300     BLOCK CONTAINS 0 RECORDS                                     CPFDXTHF
001400*    RECORD CONTAINS 134 CHARACTERS                               13190159
001500     RECORD CONTAINS 156 CHARACTERS                               13190159
001600     LABEL RECORDS STANDARD.                                      CPFDXTHF
