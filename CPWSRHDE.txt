000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSRHDE                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*     COBOL DEFINITION OF PPPHDE ROW                         */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSRHDE
001000* COBOL DECLARATION FOR TABLE PPPVHDE1_HDE                       *CPWSRHDE
001100******************************************************************CPWSRHDE
001200*01  DCLPPPVHDE1-HDE.                                             CPWSRHDE
001300     10 HDE-TBL-NAME         PIC X(18).                           CPWSRHDE
001400     10 HDE-COL-NAME         PIC X(18).                           CPWSRHDE
001500     10 HDE-ACCUM-IND        PIC X(1).                            CPWSRHDE
001600     10 HDE-DATA-TYPE        PIC X(1).                            CPWSRHDE
001700     10 HDE-DATA-USE         PIC X(1).                            CPWSRHDE
001800     10 HDE-DECIMAL          PIC S9(4) USAGE COMP.                CPWSRHDE
001900     10 HDE-EDB-DET-NO       PIC X(4).                            CPWSRHDE
002000     10 HDE-LENGTH           PIC S9(4) USAGE COMP.                CPWSRHDE
002100     10 HDE-OBS-DATE         PIC X(10).                           CPWSRHDE
002200     10 HDE-OBS-FLAG         PIC X(1).                            CPWSRHDE
002300     10 HDE-PERIOD-IND       PIC X(1).                            CPWSRHDE
002400     10 HDE-PERIOD-ROUT      PIC X(2).                            CPWSRHDE
002500     10 HDE-PURGE-IND        PIC X(1).                            CPWSRHDE
002600******************************************************************CPWSRHDE
002700* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 13      *CPWSRHDE
002800******************************************************************CPWSRHDE
