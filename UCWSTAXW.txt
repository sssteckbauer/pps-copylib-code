000000**************************************************************/   48441334
000001*  COPYMEMBER: UCWSTAXW                                      */   48441334
000002*  RELEASE: ___1334______ SERVICE REQUEST(S): ____14844____  */   48441334
000003*  NAME:_______QUAN______ CREATION DATE:      ___01/18/01__  */   48441334
000004*  DESCRIPTION:                                              */   48441334
000005*     HISTORY TAX DOCUMENT WORK AREA                         */   48441334
000007*                                                            */   48441334
000008**************************************************************/   48441334
001800*01  UCWSTAXW EXTERNAL.                                           UCWSTAXW
001900     05  UCWSTAXW-AREA   PIC X(2048).                             UCWSTAXW
002000     05  UCWSTAXW-DATA-REDF  REDEFINES UCWSTAXW-AREA.             UCWSTAXW
002100         10  UCWSTAXW-INIT-IND             PIC X(16).             UCWSTAXW
002200             88  UCWSTAXW-INITIALIZED          VALUE              UCWSTAXW
002300                                           '>>$$UCWSTAXW$$<<'.    UCWSTAXW
002400         10  FILLER                        PIC X(04).             UCWSTAXW
002600         10  UCWSTAXW-HTX-KEY.                                    UCWSTAXW
002600             15  UCWSTAXW-EMPLOYEE-ID      PIC X(09).             UCWSTAXW
002600             15  UCWSTAXW-ITERATION-NUMBER PIC X(03).             UCWSTAXW
002600             15  UCWSTAXW-SYSTEM-ENTRY-DATE PIC X(10).            UCWSTAXW
002600             15  UCWSTAXW-SYSTEM-ENTRY-TIME PIC X(08).            UCWSTAXW
003100         10  UCWSTAXW-QUEUE-NAME           PIC X(08).             UCWSTAXW
003900         10  UCWSTAXW-SEL-ADDRESSEE        PIC X(50).             UCWSTAXW
004600         10  UCWSTAXW-TAXDOC-STATUS-FLAG   PIC X.                 UCWSTAXW
004700             88  UCWSTAXW-NO-TAXDOC-ERROR      VALUE 'N'.         UCWSTAXW
004700             88  UCWSTAXW-TAXDOC-ERROR         VALUE 'Y'.         UCWSTAXW
005500         10  UCWSTAXW-PROGRAM-WORK-AREA    PIC X(1939).           UCWSTAXW
005600***************    END OF SOURCE - UCWSTAXW    *******************UCWSTAXW
