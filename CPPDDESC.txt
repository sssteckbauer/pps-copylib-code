000100**************************************************************/   36270637
000200*  COPYLIB: CPPDDESC                                         */   36270637
000300*  RELEASE # ___0637___   SERVICE REQUEST NO(S)___3627_______*/   36270637
000400*  NAME __C RIDLON_____   MODIFICATION DATE ____02/14/92_____*/   36270637
000500*  DESCRIPTION                                               */   36270637
000600*                                                            */   36270637
000700*        CPPDDESC IS A NEW COPYMEMBER FOR RELEASE #0637      */   36270637
000800*                                                            */   36270637
000900*                                                            */   36270637
001000**************************************************************/   36270637
001100 9190-GET-RCD-TYPE-DESC.                                          CPPDDESC
001200****************************************************************  CPPDDESC
001300* THIS PARAGRAPH PROVIDES A SHORT DESCRIPTION FOR THE PAR      *  CPPDDESC
001400* RECORD TYPE.                                                 *  CPPDDESC
001500****************************************************************  CPPDDESC
001600     EVALUATE TRUE                                                CPPDDESC
001700       WHEN  CA-UNIQUE-RECORD-TYPE = '85'                         CPPDDESC
001800             MOVE 'CUR-ACTIVITY'   TO RECORD-TYPEO                CPPDDESC
001900       WHEN  CA-UNIQUE-RECORD-TYPE = '65'                         CPPDDESC
002000             MOVE 'EXPENSE-TRFR'   TO RECORD-TYPEO                CPPDDESC
002100       WHEN  CA-UNIQUE-RECORD-TYPE = '70'                         CPPDDESC
002200             MOVE 'CANCELLATION'   TO RECORD-TYPEO                CPPDDESC
002300       WHEN  CA-UNIQUE-RECORD-TYPE = '75'                         CPPDDESC
002400             MOVE 'OVERPAYMENT '   TO RECORD-TYPEO                CPPDDESC
002500       WHEN  CA-UNIQUE-RECORD-TYPE = '80'                         CPPDDESC
002600             MOVE 'HAND-DRAWN  '   TO RECORD-TYPEO                CPPDDESC
002700       WHEN  CA-UNIQUE-RECORD-TYPE = '82'                         CPPDDESC
002800             MOVE 'R-HAND-DRAWN'   TO RECORD-TYPEO                CPPDDESC
002900       WHEN  OTHER                                                CPPDDESC
003000             MOVE 'UNKNOWN TYPE'   TO RECORD-TYPEO                CPPDDESC
003100     END-EVALUATE.                                                CPPDDESC
