000010**************************************************************/   36370967
000020*  COPYMEMBER: CPWSXIC1                                      */   36370967
000030*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3637____  */   36370967
000040*  NAME:________JLT______ MODIFICATION DATE:  ___03/10/95__  */   36370967
000050*  DESCRIPTION:                                              */   36370967
000060*   - ADDED NEW TIME REPORT CODES FOR ONLINE TIME SHEET      */   36370967
000070*     GENERATION.                                            */   36370967
000090**************************************************************/   36370967
000100**************************************************************/   37440550
000200*  COPYMEMBER: CPWSXIC1                                      */   37440550
000300*  RELEASE: ___0550______ SERVICE REQUEST(S): _____3744____  */   37440550
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___03/27/91__  */   37440550
000500*  DESCRIPTION:                                              */   37440550
000600*   - CHANGED INVALID TRANSACTION CODE INDICATOR TO 19       */   37440550
000700*     REFLECTING THE ADDITION OF NEW TRANSACTION CODE LA     */   37440550
000710*     WHICH HAS TRANSACTION CODE INDICATOR NUMBER 18         */   37440550
000800**************************************************************/   37440550
000001**************************************************************/   36030532
000002*  COPY MODULE: CPWSXIC1                                     */   36030532
000003*  RELEASE#__0532___  REF: ____  SERVICE REQUEST __3603__    */   36030532
000004*  NAME __J LUCAS______   MODIFICATION DATE ____09/19/90_____*/   36030532
000005*  DESCRIPTION                                               */   36030532
000006*   - MODIFICATIONS FOR RUSH CHECKS                          */   36030532
000007*        ADDED 5 MORE ENTRIES TO IDC-INP-TRAN-CD-ENTRIES     */   36030532
000008*        FOR TRANSACTIONS R1 THRU R5                         */   36030532
000009**************************************************************/   36030532
000010**************************************************************/   30950481
000020*  COPYMEMBER: CPWSXIC1                                      */   30950481
000030*  RELEASE # ____0481____ SERVICE REQUEST NO(S)___3095_______*/   30950481
000031*  REF REL # ____0468____                                    */   30950481
000040*  NAME __M. BAPTISTA__   MODIFICATION DATE ____06/20/90_____*/   30950481
000050*  DESCRIPTION                                               */   30950481
000060*   - INCREASED TRAN TABLE FIELD SIZE AND OCCURS BY 1 THAT   */   30950481
000070*     SHOULD HAVE BEEN DONE IN REL 468 WHEN THE LA TRANS     */   30950481
000080*     ENTRY WAS ADDED TO THE TABLE.                          */   30950481
000090**************************************************************/   30950481
000100**************************************************************/   30950468
000200*  COPYMEMBER: CPWSXIC1                                      */   30950468
000300*  RELEASE # ____0468____ SERVICE REQUEST NO(S)___3095_______*/   30950468
000400*  NAME __M. BAPTISTA__   MODIFICATION DATE ____03/20/90_____*/   30950468
000500*  DESCRIPTION                                               */   30950468
000600*   - ADDED ONE MORE ENTRY TO IDC-INP-TRAN-CD-ENTRIES TO BE  */   30950468
000700*     USED IN CONTROLLING THE PROCESSING OF THE LA TRANS IN  */   30950468
000800*     PROGRAMS 350, 360 AND 380.                             */   30950468
000900**************************************************************/   30950468
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXIC1                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   14520339
000200*  COPYMEMBER: CPWSXIC1                                      */   14520339
000300*  RELEASE # ____0339____ SERVICE REQUEST NO(S)____1452______*/   14520339
000400*  NAME ______JLT______   MODIFICATION DATE ____01/09/88_____*/   14520339
000500*  DESCRIPTION                                               */   14520339
000600*   - TURN ON INDICATORS SO THAT PRINTED 'TE' EXCEPTION PAY  */   14520339
000700*     IS ALSO INCLUDE ON TIME FILE.                          */   14520339
000800**************************************************************/   14520339
000900**************************************************************/   73440138
001000*  COPYMEMBER:   CPWSXIC1                                    */   73440138
001100*  RELEASE # ___0138_____ SERVICE REQUEST NO(S) ____7344___  */   73440138
001200*  NAME ___M.NISHI_____   MODIFICATION DATE ____12/18/84_____*/   73440138
001300*  DESCRIPTION                                               */   73440138
001400*  INCREASE THE IDC-BATCH-NO-RANGE-HI  FROM 200 TO 800       */   73440138
001500*    SO THAT UP TO 800 BATCHES CAN BE RUN THROUGH            */   73440138
001600*    THE COMPUTE CYCLE.  UP TO 4 BSR RECORDS ARE POSSIBLE    */   73440138
001700*    WITH 200 OCCURANCES ON EACH RECORD.                     */   73440138
001800*    (AFFECTS:  PPP340, PPP360, PPP380 AND CPWSXIC1)         */   73440138
001900**************************************************************/   73440138
002000     SKIP3                                                        73440138
002100*    COPYID=CPWSXIC1                                              CPWSXIC1
002200*01  PAYROLL-PROCESS-INST-CONST-1.                                30930413
002300******************************************************************CPWSXIC1
002400* INSTALLATION CONSTANTS USED EXCLUSIVELY BY PROGRAMS 30 THROUGH  CPWSXIC1
002500* 38. PROGRAMS 30 AND 36 ALSO USE CPWSXIC2.                       CPWSXIC1
002600******************************************************************CPWSXIC1
002700     05  IDC-NO-OF-VALID-CYCLES  PIC S9(5)       VALUE +8.        CPWSXIC1
002800     05  IDC-PAY-CYCLES.                                          CPWSXIC1
002900**     * D A N G E R *    CHECK OUT B1-B2  PROCESSING AL + JANE   CPWSXIC1
003000         10  IDC-PAY-CYCLE-1     PIC X(35)      VALUE             CPWSXIC1
003100             'WW1W1INVALID SCHEDULE CODE         '.               CPWSXIC1
003200         10  IDC-PAY-CYCLE-2     PIC X(35)      VALUE             CPWSXIC1
003300             'WW2W1INVALID SCHEDULE CODE         '.               CPWSXIC1
003400         10  IDC-PAY-CYCLE-3     PIC X(35)      VALUE             CPWSXIC1
003500             'B13W2BIWEEKLY ARREARS              '.               CPWSXIC1
003600         10  IDC-PAY-CYCLE-4     PIC X(35)      VALUE             CPWSXIC1
003700             'B24W2BIWEEKLY ARREARS              '.               CPWSXIC1
003800         10  IDC-PAY-CYCLE-5     PIC X(35)      VALUE             CPWSXIC1
003900             'SM5M3SEMI-MONTHLY ARREARS          '.               CPWSXIC1
004000         10  IDC-PAY-CYCLE-6     PIC X(35)      VALUE             CPWSXIC1
004100             'SS6M3INVALID SCHEDULE CODE         '.               CPWSXIC1
004200         10  IDC-PAY-CYCLE-7     PIC X(35)      VALUE             CPWSXIC1
004300             'MO7M4MONTHLY CURRENT               '.               CPWSXIC1
004400         10  IDC-PAY-CYCLE-8     PIC X(35)      VALUE             CPWSXIC1
004500             'MA8M4MONTHLY ARREARS               '.               CPWSXIC1
004600     05  FILLER                  REDEFINES IDC-PAY-CYCLES.        CPWSXIC1
004700         10  IDC-PAY-CYCLE-ARRAY OCCURS 8.                        CPWSXIC1
004800             15  IDC-PAY-CYCLE-TYPE.                              CPWSXIC1
004900                 17  IDC-PAY-CYCLE-CODE  PIC X.                   CPWSXIC1
005000                 17  FILLER              PIC X.                   CPWSXIC1
005100             15  IDC-PAY-CYCLE-POSN  PIC 9.                       CPWSXIC1
005200             15  IDC-PAY-CYCLE-MODE  PIC X.                       CPWSXIC1
005300             15  IDC-PAY-CYCLE-DEDN-SCHED PIC 9.                  CPWSXIC1
005400             15  IDC-PAY-CYCLE-NAME  PIC X(30).                   CPWSXIC1
005500     05  IDC-SPECIAL-CYCLE       PIC XX          VALUE 'XX'.      CPWSXIC1
005600     05  IDC-PERIOD-END-CYCLE    PIC XX          VALUE 'NA'.
005700**  THE ABOVE FUNCTION OF THE AUTOMATIC SETTING OF PERIODIC
005800**   MAINTENANCE INDICATORS WORKS ONLY AT CAMPUSES WHERE
005900**   THERE ARE NO WEEKLY BASED PAY CYCLES ELECTED.  THEREFORE,
006000**   THE FUNCTION HAS BEEN CRIPPLED BY THE VALUE 'NA'. SEE JIM T.
006100**   I.E., INDICATORS MUST BE USER CONTROLED VIA PPP020.
006200**
006300     05  IDC-BATCH-NO-RANGE.                                      CPWSXIC1
006400         10  IDC-BATCH-NO-RANGE-LOW  PIC 999     VALUE 1.         CPWSXIC1
006500         10  IDC-BATCH-NO-RANGE-HI   PIC 999     VALUE 800.       73440138
006600     05  IDC-TIME-RPT-DATA.                                       CPWSXIC1
006700         10  IDC-FMT-01-MAX-LINES    PIC 99    VALUE 15.          CPWSXIC1
006800         10  IDC-FMT-02-MAX-LINES    PIC 99    VALUE 18.          CPWSXIC1
006900         10  IDC-TIME-RPT-CTL-NO-DATA.                            CPWSXIC1
007000             15  IDC-NO-OF-PREFIXES                               CPWSXIC1
007100                                 PIC S9(5) COMP SYNC VALUE +15.   CPWSXIC1
007200             15  IDC-PREFIXES-STRING                              CPWSXIC1
007300**     * D A N G E R *    DON'T TOUCH THESE UNLESS YOU LIKE BUGS  CPWSXIC1
007400                                 PIC X(15) VALUE                  CPWSXIC1
007500                 'BEFGHNQRSTVWXYZ'.                               CPWSXIC1
007600             15  IDC-PREFIXES-STRING-REDEF REDEFINES              CPWSXIC1
007700                 IDC-PREFIXES-STRING.                             CPWSXIC1
007800                 20  IDC-PREFIX  PIC X     OCCURS 15.             CPWSXIC1
007900     05  IDC-RATE-CD-DATA.                                        CPWSXIC1
008000         10  IDC-NO-OF-RATE-CDS  PIC S9(5) VALUE +3.              CPWSXIC1
008100         10  IDC-RATE-CODES.                                      CPWSXIC1
008200             15  FILLER          PIC X(32) VALUE                  CPWSXIC1
008300                 'APANNUAL'.                                      CPWSXIC1
008400             15  FILLER          PIC X(32) VALUE                  CPWSXIC1
008500                 'HHHOURLY'.                                      CPWSXIC1
008600             15  FILLER          PIC X(32) VALUE                  CPWSXIC1
008700                 'BPBY-AGREEMENT'.                                CPWSXIC1
008800         10  FILLER REDEFINES IDC-RATE-CODES.                     CPWSXIC1
008900             15  IDC-RATE-CD-ARRAY OCCURS 3.                      CPWSXIC1
009000**     * D A N G E R *    AL  CHECK OUT IN TIME FILE GENERATION   CPWSXIC1
009100                 20  IDC-APPT-RATE-CD PIC X.                      CPWSXIC1
009200                 20  IDC-SORT-RATE-CD PIC X.                      CPWSXIC1
009300                 20  IDC-RATE-CD-NAME PIC X(30).                  CPWSXIC1
009400     05  IDC-TIME-RPTG-CD-DATA.                                   CPWSXIC1
013800         10  IDC-NO-OF-TIME-RPTG-CDS PIC S9(5) VALUE +11.         36370967
013810******** 10  IDC-NO-OF-TIME-RPTG-CDS PIC S9(5) VALUE +9.          36370967
009600         10  IDC-TIME-RPTG-CODES.                                 CPWSXIC1
009700             15  FILLER      PIC X(34) VALUE                      CPWSXIC1
009800                 'AYYYNPOSITIVE                     '.            CPWSXIC1
009900             15  FILLER      PIC X(34) VALUE                      CPWSXIC1
010000                 'PYYYNPOSITIVE                     '.            CPWSXIC1
010100             15  FILLER      PIC X(34) VALUE                      CPWSXIC1
010200                 'SYYYYPOSITIVE                     '.            CPWSXIC1
010300             15  FILLER      PIC X(34) VALUE                      CPWSXIC1
010400                 'CYYYYPOSITIVE                     '.            CPWSXIC1
010500             15  FILLER      PIC X(34) VALUE                      CPWSXIC1
010600                 'NYYNNPOSITIVE--NOT TIME-LISTED    '.            CPWSXIC1
014910             15  FILLER      PIC X(34) VALUE                      36370967
014920                 'ZNNNNPOSITIVE--ONLINE-LISTED      '.            36370967
010700             15  FILLER      PIC X(34) VALUE                      CPWSXIC1
010800*****************'TNYYNEXCEPTION                    '.            14520339
010900                 'TYYYNEXCEPTION                    '.            14520339
011000             15  FILLER      PIC X(34) VALUE                      CPWSXIC1
011100*****************'LNYYNEXCEPTION                    '.            14520339
011200                 'LYYYNEXCEPTION                    '.            14520339
011300             15  FILLER          PIC X(34) VALUE                  CPWSXIC1
011400                 'ENNNNEXCEPTION--NOT TIME-LISTED   '.            CPWSXIC1
015710             15  FILLER          PIC X(34) VALUE                  36370967
015720                 'RNNNNEXCEPTION--ONLINE-LISTED     '.            36370967
011500             15  FILLER          PIC X(34)     VALUE              CPWSXIC1
011600                 'WNNNNWITHOUT SALARY               '.            CPWSXIC1
011700         10  FILLER REDEFINES IDC-TIME-RPTG-CODES.                CPWSXIC1
016100             15  IDC-TIME-RPTG-CD-ARRAY  OCCURS 11.               36370967
016110***********  15  IDC-TIME-RPTG-CD-ARRAY  OCCURS 9.                36370967
011900                 20  IDC-TIME-RPTG-CD    PIC X.                   CPWSXIC1
012000                 20  IDC-TIME-FILE-EXTRACT   PIC X.               CPWSXIC1
012100                 20  IDC-COMPUTE-TIME    PIC X.                   CPWSXIC1
012200                 20  IDC-TIME-RPT-PRINT  PIC X.                   CPWSXIC1
012300                 20  IDC-TIME-RPT-MTPL-LNS   PIC X.               CPWSXIC1
012400                 20  IDC-TIME-RPTG-NAME      PIC X(29).           CPWSXIC1
012500     05  IDC-MONTHS-ON-TIME-RPT-W-O-PAY PIC 99 VALUE 3.           CPWSXIC1
012600     05  IDC-MAX-NO-MLTP-LNS     PIC S9(3)   COMP-3  VALUE +6.    CPWSXIC1
012700     05  IDC-INPUT-TRAN-CD-DATA.                                  CPWSXIC1
012800*********10  IDC-INP-TRAN-CD-TABLE-SIZE  PIC S9(4) VALUE +39.     30950481
014410*********10  IDC-INP-TRAN-CD-TABLE-SIZE  PIC S9(4) VALUE +40.     36030532
014420         10  IDC-INP-TRAN-CD-TABLE-SIZE  PIC S9(4) VALUE +45.     36030532
012900         10  IDC-INP-TRAN-CD-ENTRIES.                             CPWSXIC1
013000* THESE ENTRIES MAY APPEAR IN ANY ORDER AS LONG AS THE INTEGRITY  CPWSXIC1
013100* OF THE INDIVIDUAL ENTRIES IS NOT VIOLATED.                      CPWSXIC1
013200             15 FILLER PIC X(15) VALUE 'TX40101108T0101'.         CPWSXIC1
013300*************15 FILLER PIC X(15) VALUE 'TE45202112T0203'.         14520339
013400             15 FILLER PIC X(15) VALUE 'TE45102112T0203'.         14520339
013500             15 FILLER PIC X(15) VALUE 'LX40204109T0101'.         CPWSXIC1
013600             15 FILLER PIC X(15) VALUE 'RX40206111T0102'.         CPWSXIC1
013700             15 FILLER PIC X(15) VALUE 'AP40205110T0101'.         CPWSXIC1
013800             15 FILLER PIC X(15) VALUE 'E105408215A0304'.         CPWSXIC1
013900             15 FILLER PIC X(15) VALUE 'E205408215A0305'.         CPWSXIC1
014000             15 FILLER PIC X(15) VALUE 'E305408215A0306'.         CPWSXIC1
014100             15 FILLER PIC X(15) VALUE 'AD60709119A0719'.         CPWSXIC1
014200             15 FILLER PIC X(15) VALUE 'C120511304A0407'.         CPWSXIC1
014300             15 FILLER PIC X(15) VALUE 'C220511304A0410'.         CPWSXIC1
014400             15 FILLER PIC X(15) VALUE 'C320511304A0412'.         CPWSXIC1
014500             15 FILLER PIC X(15) VALUE 'C420511304A0414'.         CPWSXIC1
014600             15 FILLER PIC X(15) VALUE 'C520511304A0416'.         CPWSXIC1
014700             15 FILLER PIC X(15) VALUE 'O125610305A0408'.         CPWSXIC1
014800             15 FILLER PIC X(15) VALUE 'O225610305A0410'.         CPWSXIC1
014900             15 FILLER PIC X(15) VALUE 'O325610305A0412'.         CPWSXIC1
015000             15 FILLER PIC X(15) VALUE 'O425610305A0414'.         CPWSXIC1
015100             15 FILLER PIC X(15) VALUE 'O525610305A0416'.         CPWSXIC1
015200             15 FILLER PIC X(15) VALUE 'H130511306A0409'.         CPWSXIC1
015300             15 FILLER PIC X(15) VALUE 'H230511306A0411'.         CPWSXIC1
015400             15 FILLER PIC X(15) VALUE 'H330511306A0413'.         CPWSXIC1
015500             15 FILLER PIC X(15) VALUE 'H430511306A0415'.         CPWSXIC1
015600             15 FILLER PIC X(15) VALUE 'H530511306A0417'.         CPWSXIC1
015700             15 FILLER PIC X(15) VALUE 'RA35712107A0520'.         CPWSXIC1
015800             15 FILLER PIC X(15) VALUE 'PS55713118A0721'.         CPWSXIC1
015900             15 FILLER PIC X(15) VALUE 'DS55714116A0722'.         CPWSXIC1
016000             15 FILLER PIC X(15) VALUE 'RF55715117A0723'.         CPWSXIC1
017700             15 FILLER PIC X(15) VALUE 'LA11718127A0728'.         30950468
016100             15 FILLER PIC X(15) VALUE 'HA10717102A0724'.         CPWSXIC1
016200             15 FILLER PIC X(15) VALUE 'DA15716103A0725'.         CPWSXIC1
016300             15 FILLER PIC X(15) VALUE 'SB20911120A0627'.         CPWSXIC1
016400             15 FILLER PIC X(15) VALUE 'UB20911121A0626'.         CPWSXIC1
016500             15 FILLER PIC X(15) VALUE 'FT50712101A0718'.         CPWSXIC1
016600             15 FILLER PIC X(15) VALUE 'ST50712122A0718'.         CPWSXIC1
016700             15 FILLER PIC X(15) VALUE 'BA01800124 9999'.         CPWSXIC1
016800             15 FILLER PIC X(15) VALUE 'BD01300100C9999'.         CPWSXIC1
016900             15 FILLER PIC X(15) VALUE 'BR01300124C9999'.         CPWSXIC1
018610             15 FILLER PIC X(15) VALUE 'R133511326A0409'.         36030532
018620             15 FILLER PIC X(15) VALUE 'R233511326A0411'.         36030532
018630             15 FILLER PIC X(15) VALUE 'R333511326A0413'.         36030532
018640             15 FILLER PIC X(15) VALUE 'R433511326A0415'.         36030532
018650             15 FILLER PIC X(15) VALUE 'R533511326A0417'.         36030532
017000* THE NEXT ENTRY IS FOR CORRECTION TRANSACTIONS                   CPWSXIC1
017100             15 FILLER.                                           CPWSXIC1
017200                20 FILLER PIC XX VALUE HIGH-VALUES.               CPWSXIC1
017300                20 FILLER PIC X(13) VALUE '00300000C9999'.        CPWSXIC1
017400* THE NEXT ENTRY IS FOR INVALID TRANSACTION CODES.                CPWSXIC1
017500             15 FILLER.                                           CPWSXIC1
017600                20 FILLER PIC XX VALUE LOW-VALUES.                CPWSXIC1
022800*****           20 FILLER PIC X(9) VALUE '99718123 '.             37440550
022900                20 FILLER PIC X(9) VALUE '99719123 '.             37440550
017800                20 FILLER PIC X(4) VALUE '9999'.                  CPWSXIC1
017900         10  FILLER REDEFINES IDC-INP-TRAN-CD-ENTRIES.            CPWSXIC1
018000*************15 FILLER OCCURS 39.                                 30950481
019710*************15 FILLER OCCURS 40.                                 36030532
019720             15 FILLER OCCURS 45.                                 36030532
018100                20 IDC-INP-TRAN-CD          PIC XX.               CPWSXIC1
018200                20 IDC-TRAN-SEQ-CD          PIC XX.               CPWSXIC1
018300                20 IDC-35-FORMAT-SELECT     PIC 9.                CPWSXIC1
018400                20 IDC-36-EDIT-SELECT       PIC 99.               CPWSXIC1
018500                20 IDC-36-COLLECT-SET       PIC 9.                CPWSXIC1
018600                20 IDC-36-COUNTS            PIC 99.               CPWSXIC1
018700                20 IDC-LEGAL-BATCH-FOR-TRAN PIC X.                CPWSXIC1
018800                20 IDC-38-FORMAT-SELECT     PIC 99.               CPWSXIC1
018900                20 IDC-38-PROCESS-SELECT    PIC 99.               CPWSXIC1
019000     05  IDC-MONTHS-AFTER-TERM.                                   CPWSXIC1
019100         10  IDC-RA-W-RATE-ADJ-MOS   PIC 99   VALUE 12.           CPWSXIC1
019200         10  IDC-LX-AP-RA-MOS        PIC 99   VALUE  3.           CPWSXIC1
019300         10  IDC-RF-MOS              PIC 99   VALUE  3.           CPWSXIC1
