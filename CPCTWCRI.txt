000000**************************************************************/   30871465
000001*  COPYMEMBER: CPCTWCRI                                      */   30871465
000002*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000003*  NAME:_____SRS_________ CREATION DATE:      ___02/11/03__  */   30871465
000004*  DESCRIPTION:                                              */   30871465
000005*  - NEW COPY MEMBER FOR WORKERS COMP TABLE(WCR) INPUT       */   30871465
000007**************************************************************/   30871465
000008*    COPYID=CPCTWCRI                                              CPCTWCRI
010000*01  WORKERS-COMP-WCR-TABLE-INPUT.                                CPCTWCRI
010100     05 WCRI-LOC-CODE                    PIC X.                   CPCTWCRI
010200     05 WCRI-RECORD-TYPE                 PIC X.                   CPCTWCRI
010300     05 WCRI-ACCT-OR-FUND-1              PIC X(06).               CPCTWCRI
010400     05 WCRI-ACCT-OR-FUND-2              PIC X(06).               CPCTWCRI
010500     05 WCRI-RATE.                                                CPCTWCRI
010600        10 WCRI-RATE-9                   PIC 9V9999.              CPCTWCRI
