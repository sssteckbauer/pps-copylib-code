000100**************************************************************/   36300699
000200*  COPYMENBER: CPWSXPPI                                      */   36300699
000300*  RELEASE: ____0699____  SERVICE REQUEST(S): ____3630____   */   36300699
000400*  NAME:____PXP___________CREATION DATE:      __09/17/92__   */   36300699
000500*  DESCRIPTION:                                              */   36300699
000600*  - NEW PAYROLL PROCESS ID FILE RECORD LAYOUT               */   36300699
000700**************************************************************/   36300699
000800*    COPYID=CPWSXPPI                                              CPWSXPPI
000900*01  XPPI-RECORD.                                                 CPWSXPPI
001000     05  XPPI-PPI                PIC X(08).                       CPWSXPPI
