000000**************************************************************/   30871405
000001*  COPYMEMBER: CPCTBULI                                      */   30871405
000002*  RELEASE: ___1405______ SERVICE REQUEST(S): _____3087____  */   30871405
000003*  NAME:_____SRS_________ CREATION DATE:      ___04/16/02__  */   30871405
000004*  DESCRIPTION:                                              */   30871405
000005*  - NEW COPY MEMBER FOR BARGAINING UNIT BUL TABLE INPUT     */   30871405
000007**************************************************************/   30871405
000008*    COPYID=CPCTBULI                                              CPCTBULI
010000*01  BARG-UNIT-BUL-TABLE-INPUT.                                   CPCTBULI
010100     05 BULI-BUC                         PIC XX.                  CPCTBULI
010200     05 BULI-REP                         PIC X.                   CPCTBULI
010300     05 BULI-SHC                         PIC X.                   CPCTBULI
010400        88  BULI-VALID-SHC               VALUES SPACE             CPCTBULI
010500                                               'A' THRU 'Z'       CPCTBULI
010600                                               '0' THRU '9'.      CPCTBULI
010700     05 BULI-DISTRIBUTION                PIC X.                   CPCTBULI
010800        88  BULI-VALID-DISTRIBUTION      VALUES SPACE             CPCTBULI
010900                                               'A' THRU 'Z'       CPCTBULI
011100                                               '0' THRU '9'.      CPCTBULI
011200     05 BULI-LEAVE-CODE                  PIC X.                   CPCTBULI
010900        88  BULI-VALID-LEAVE-CODE        VALUE 'A' THRU 'H'       CPCTBULI
011100                                               'J' 'K'.           CPCTBULI
011300     05 BULI-LEAVE-ESC-MIN-X.                                     CPCTBULI
011300        10 BULI-LEAVE-ESC-MIN            PIC 9999.                CPCTBULI
