000000**************************************************************/   69321592
000001*  COPYMEMBER: CPCTNGLI                                      */   69321592
000002*  RELEASE: ___1592______ SERVICE REQUEST(S): ____16932____  */   69321592
000003*  NAME:_______QUAN______ CREATION DATE:      ___06/17/04__  */   69321592
000004*  DESCRIPTION:                                              */   69321592
000005*  - NEW COPY MEMBER FOR NACHA GTN LINK TABLE (NGL).         */   69321592
000007**************************************************************/   69321592
006400*    COPYID=CPCTNGLI                                              CPCTNGLI
011500*01  NACHA-GTN-ORG-LINK-INPUT.                                    CPCTNGLI
019630     05 NGLI-LINK                        PIC 99.                  CPCTNGLI
019630     05 NGLI-LINKX   REDEFINES                                    CPCTNGLI
019630        NGLI-LINK                        PIC XX.                  CPCTNGLI
019670     05 NGLI-GTN                         PIC X(03).               CPCTNGLI
