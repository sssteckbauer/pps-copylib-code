000000**************************************************************/   15400936
000001*  COPYMEMBER: CPLN142P                                      */   15400936
000002*  RELEASE: ___0936______ SERVICE REQUEST(S): ____11540____  */   15400936
000003*  NAME:_______QUAN______ CREATION DATE:      ___09/01/94__  */   15400936
000004*  DESCRIPTION:                                              */   15400936
001100*    - INITIAL RELEASE OF COPYMEMBER FOR LINKAGE BETWEEN     */   15400936
001200*      CALLING PROGRAMS AND PP1042SP.                        */   15400936
001300**************************************************************/   15400936
002700*01  PP1042SP-INTERFACE.                                          CPLN142P
004000     05  TXP-CALL-TYPE                     PIC X(01).             CPLN142P
004001         88 TXP-INITIAL-CALL                 VALUE 'I'.           CPLN142P
004002         88 TXP-EMPLOYEE-CALL                VALUE 'E'.           CPLN142P
004010     05  TXP-RETURN-FLAG                   PIC 9(01).             CPLN142P
004100         88  TXP-RETURN-NORMAL               VALUE ZERO.          CPLN142P
004110         88  TXP-RETURN-FINAL-EDB            VALUE  1.            CPLN142P
004300*---------> FOLLOWING RETURN CODES ARE ERRORS                     CPLN142P
004330         88  TXP-RETURN-BAD-CALL             VALUE 2.             CPLN142P
004331         88  TXP-RETURN-ERROR-DB2            VALUE 3.             CPLN142P
004332*---------> LIST ALL ERROR RETURN CODES HERE                      CPLN142P
004340         88  TXP-RETURN-ERRORS               VALUES 2,            CPLN142P
004350                                                    3.            CPLN142P
004370     05  TXP-RETURN-SQLCODE                PIC S9(04) COMP.       CPLN142P
