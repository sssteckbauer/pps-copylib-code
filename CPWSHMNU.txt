000100**************************************************************/   36290827
000200*  COPYMEMBER: CPWSHMNU                                      */   36290827
000300*  RELEASE: ___0827______ SERVICE REQUEST(S): _____3629____  */   36290827
000400*  NAME:_______DXS_______ MODIFICATION DATE:  __10/21/93____ */   36290827
000500*                                                            */   36290827
000600*  THIS IS A REPLACEMENT OF COPY MEMBER CPWSHMNU             */   36290827
000700*                                                            */   36290827
000800*  DESCRIPTION: CURSOR POSITION VALIDATION AND CURSOR        */   36290827
000900*    POSITION SELECTION VALUES FOR THE                       */   36290827
001000*    PRIMARY ERH SYSTEM (IHDB) MENU.                         */   36290827
001100**************************************************************/   36290827
001200 01  FILLER.                                                      CPWSHMNU
001300     05  WS-EIBCPOSN            PIC S9(4)  VALUE ZEROS.           CPWSHMNU
001400         88 FUNC-SET            VALUES 401,                       CPWSHMNU
001500                                       408,                       CPWSHMNU
001600                                       443,                       CPWSHMNU
001700                                       450,                       CPWSHMNU
001800                                       481,                       CPWSHMNU
001900                                       488,                       CPWSHMNU
002000                                       523,                       CPWSHMNU
002100                                       530,                       CPWSHMNU
002200                                       561,                       CPWSHMNU
002300                                       568,                       CPWSHMNU
002400                                       603,                       CPWSHMNU
002500                                       610,                       CPWSHMNU
002600                                       641,                       CPWSHMNU
002700                                       648,                       CPWSHMNU
002800                                       683,                       CPWSHMNU
002900                                       690,                       CPWSHMNU
003000                                       721,                       CPWSHMNU
003100                                       728,                       CPWSHMNU
003200                                       763,                       CPWSHMNU
003300                                       770,                       CPWSHMNU
003400                                       801,                       CPWSHMNU
003500                                       808,                       CPWSHMNU
003600                                       843,                       CPWSHMNU
003700                                       850,                       CPWSHMNU
003800                                       881,                       CPWSHMNU
003900                                       888,                       CPWSHMNU
004000                                       923,                       CPWSHMNU
004100                                       930,                       CPWSHMNU
004200                                       961,                       CPWSHMNU
004300                                       968,                       CPWSHMNU
004400                                       1003,                      CPWSHMNU
004500                                       1010,                      CPWSHMNU
004600                                       1041,                      CPWSHMNU
004700                                       1048,                      CPWSHMNU
004800                                       1083,                      CPWSHMNU
004900                                       1090,                      CPWSHMNU
005000                                       1121,                      CPWSHMNU
005100                                       1128,                      CPWSHMNU
005200                                       1163,                      CPWSHMNU
005300                                       1170,                      CPWSHMNU
005400                                       1201,                      CPWSHMNU
005500                                       1208,                      CPWSHMNU
005600                                       1243,                      CPWSHMNU
005700                                       1250,                      CPWSHMNU
005800                                       1320.                      CPWSHMNU
005900         88 FUNC-IATN               VALUES 401.                   CPWSHMNU
006000         88 FUNC-UATN               VALUES 408.                   CPWSHMNU
006100         88 FUNC-IHNR               VALUES 443.                   CPWSHMNU
006200         88 FUNC-UHNR               VALUES 450.                   CPWSHMNU
006300         88 FUNC-IAPN               VALUES 481.                   CPWSHMNU
006400         88 FUNC-UAPN               VALUES 488.                   CPWSHMNU
006500         88 FUNC-ILCR               VALUES 523.                   CPWSHMNU
006600         88 FUNC-ULCR               VALUES 530.                   CPWSHMNU
006700         88 FUNC-IAWD               VALUES 561.                   CPWSHMNU
006800         88 FUNC-UAWD               VALUES 568.                   CPWSHMNU
006900         88 FUNC-ILEV               VALUES 603.                   CPWSHMNU
007000         88 FUNC-ULEV               VALUES 610.                   CPWSHMNU
007100         88 FUNC-IBA1               VALUES 641.                   CPWSHMNU
007200         88 FUNC-UBA1               VALUES 648.                   CPWSHMNU
007300         88 FUNC-ILIC               VALUES 683.                   CPWSHMNU
007400         88 FUNC-ULIC               VALUES 690.                   CPWSHMNU
007500         88 FUNC-IBA2               VALUES 721.                   CPWSHMNU
007600         88 FUNC-UBA2               VALUES 728.                   CPWSHMNU
007700         88 FUNC-IMBR               VALUES 763.                   CPWSHMNU
007800         88 FUNC-UMBR               VALUES 770.                   CPWSHMNU
007900         88 FUNC-IBKG               VALUES 801.                   CPWSHMNU
008000         88 FUNC-UBKG               VALUES 808.                   CPWSHMNU
008100         88 FUNC-IMTH               VALUES 843.                   CPWSHMNU
008200         88 FUNC-UMTH               VALUES 850.                   CPWSHMNU
008300         88 FUNC-IBN1               VALUES 881.                   CPWSHMNU
008400         88 FUNC-UBN1               VALUES 888.                   CPWSHMNU
008500         88 FUNC-IOFF               VALUES 923.                   CPWSHMNU
008600         88 FUNC-UOFF               VALUES 930.                   CPWSHMNU
008700         88 FUNC-IBN2               VALUES 961.                   CPWSHMNU
008800         88 FUNC-UBN2               VALUES 968.                   CPWSHMNU
008900         88 FUNC-IQTR               VALUES 1003.                  CPWSHMNU
009000         88 FUNC-UQTR               VALUES 1010.                  CPWSHMNU
009100         88 FUNC-ICBG               VALUES 1041.                  CPWSHMNU
009200         88 FUNC-UCBG               VALUES 1048.                  CPWSHMNU
009300         88 FUNC-ISAB               VALUES 1083.                  CPWSHMNU
009400         88 FUNC-USAB               VALUES 1090.                  CPWSHMNU
009500         88 FUNC-IDST               VALUES 1121.                  CPWSHMNU
009600         88 FUNC-UDST               VALUES 1128.                  CPWSHMNU
009700         88 FUNC-ISVC               VALUES 1163.                  CPWSHMNU
009800         88 FUNC-USVC               VALUES 1170.                  CPWSHMNU
009900         88 FUNC-IFSC               VALUES 1201.                  CPWSHMNU
010000         88 FUNC-UFSC               VALUES 1208.                  CPWSHMNU
010100         88 FUNC-IYRL               VALUES 1243.                  CPWSHMNU
010200         88 FUNC-UYRL               VALUES 1250.                  CPWSHMNU
010300         88 FUNC-ILOK               VALUES 1320.                  CPWSHMNU
