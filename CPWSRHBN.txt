000000**************************************************************/   01911440
000001*  COPYMEMBER: CPWSRHBN                                      */   01911440
000002*  RELEASE: ___1440______ SERVICE REQUEST(S): ____80191____  */   01911440
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___11/08/02__  */   01911440
000004*  DESCRIPTION:                                              */   01911440
000005*  - ADDED MEDICAL CONTRIBUTION BASE CURRENT YEAR (EDB 0289) */   01911440
000007**************************************************************/   01911440
000100**************************************************************/   48661418
000200*  COPYMEMBER: CPWSRHBN                                      */   48661418
000300*  RELEASE: ___1418______ SERVICE REQUEST(S): ____14866____  */   48661418
000400*  NAME:_______JLT_______ MODIFICATION DATE:  ___06/20/02__  */   48661418
000500*  DESCRIPTION:                                              */   48661418
000600*  - ADDED STATE DECLARATION OF DOMESTIC PARTNER (EDB 0288). */   48661418
000700**************************************************************/   48661418
000000**************************************************************/   54541281
000001*  COPYMEMBER: CPWSRHBN                                      */   54541281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15454____  */   54541281
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/22/99__  */   54541281
000004*  DESCRIPTION:                                              */   54541281
000005*  -  ADDED HEALTH COVERAGE END DATE (EDB 0300),             */   54541281
000006*           DENTAL COVERAGE END DATE (EDB 0271),             */   54541281
000007*           VISION COVERAGE END DATE (EDB 0346),             */   54541281
000008*           LEGAL  COVERAGE END DATE (EDB 0380)              */   54541281
000009**************************************************************/   54541281
000000**************************************************************/   36260771
000001*  COPYMEMBER: CPWSRHBN                                      */   36260771
000002*  RELEASE: ___0771______ SERVICE REQUEST(S): _____3626____  */   36260771
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___05/20/93__  */   36260771
000004*  DESCRIPTION:                                              */   36260771
000005*    ADD EMPLOYEE COVERAGE EFFECTIVE DATES                   */   36260771
000006*                                                            */   36260771
000007**************************************************************/   36260771
000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSRHBN                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    DCLGEN FOR THE PPPVHBN1 VIEW                            */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSRHBN
001000* COBOL DECLARATION FOR TABLE PPPVHBN1_HBN                       *CPWSRHBN
001100******************************************************************CPWSRHBN
001200*01  DCLPPPVHBN1-HBN.                                             CPWSRHBN
001300     10 EMPLID               PIC X(9).                            CPWSRHBN
001400     10 ITERATION-NUMBER     PIC X(3).                            CPWSRHBN
001500     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSRHBN
001600     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSRHBN
001700     10 DELETE-FLAG          PIC X(1).                            CPWSRHBN
001800     10 ERH-INCORRECT        PIC X(1).                            CPWSRHBN
001900     10 ADD-COVERAGE         PIC X(3).                            CPWSRHBN
002000     10 ADD-COVERAGE-C       PIC X(1).                            CPWSRHBN
002100     10 ADD-COVEFFDATE       PIC X(10).                           CPWSRHBN
002200     10 ADD-COVEFFDATE-C     PIC X(1).                            CPWSRHBN
002300     10 ADD-PRINC-SUM        PIC X(3).                            CPWSRHBN
002400     10 ADD-PRINC-SUM-C      PIC X(1).                            CPWSRHBN
002500     10 AUTO-HOM-DEDUCT      PIC X(1).                            CPWSRHBN
002600     10 AUTO-HOM-DEDUCT-C    PIC X(1).                            CPWSRHBN
002700     10 BELI-ASSIGNED        PIC X(1).                            CPWSRHBN
002800     10 BELI-ASSIGNED-C      PIC X(1).                            CPWSRHBN
002900     10 BELI-CHANGEDATE      PIC X(10).                           CPWSRHBN
003000     10 BELI-CHANGEDATE-C    PIC X(1).                            CPWSRHBN
003100     10 BELI-CONFLICTDATE    PIC X(10).                           CPWSRHBN
003200     10 BELI-CONFLICTDT-C    PIC X(1).                            CPWSRHBN
003300     10 BELI-DERIVED         PIC X(1).                            CPWSRHBN
003400     10 BELI-DERIVED-C       PIC X(1).                            CPWSRHBN
003500     10 BELI-EFF-DATE        PIC X(10).                           CPWSRHBN
003600     10 BELI-EFF-DATE-C      PIC X(1).                            CPWSRHBN
003700     10 CURR-BEN-COVERAGE    PIC X(1).                            CPWSRHBN
003800     10 CURR-BEN-CVG-C       PIC X(1).                            CPWSRHBN
003900     10 DENTAL-COVERAGE      PIC X(3).                            CPWSRHBN
004000     10 DENTAL-COVERAGE-C    PIC X(1).                            CPWSRHBN
004100     10 DENTAL-COVEFFDATE    PIC X(10).                           CPWSRHBN
004200     10 DENTAL-COVEFFDT-C    PIC X(1).                            CPWSRHBN
004300     10 DENTAL-OPTOUT        PIC X(1).                            CPWSRHBN
004400     10 DENTAL-OPTOUT-C      PIC X(1).                            CPWSRHBN
004500     10 DENTAL-PLAN          PIC X(2).                            CPWSRHBN
004600     10 DENTAL-PLAN-C        PIC X(1).                            CPWSRHBN
004700     10 DEPLIFE-COVEFFDATE   PIC X(10).                           CPWSRHBN
004800     10 DEPLIFE-COVEFFDT-C   PIC X(1).                            CPWSRHBN
004900     10 DEP-LIFE-IND         PIC X(1).                            CPWSRHBN
005000     10 DEP-LIFE-IND-C       PIC X(1).                            CPWSRHBN
005100     10 EPD-SALARY-BASE      PIC S99999V USAGE COMP-3.            CPWSRHBN
005200     10 EPD-SALARY-BASE-C    PIC X(1).                            CPWSRHBN
005300     10 EPD-COVEFFDATE       PIC X(10).                           CPWSRHBN
005400     10 EPD-COVEFFDATE-C     PIC X(1).                            CPWSRHBN
005500     10 EPD-WAIT-PERIOD      PIC X(3).                            CPWSRHBN
005600     10 EPD-WAIT-PERIOD-C    PIC X(1).                            CPWSRHBN
005700     10 EXEC-LIFECHGDATE     PIC X(10).                           CPWSRHBN
005800     10 EXEC-LIFECHGDATE-C   PIC X(1).                            CPWSRHBN
005900     10 EXEC-LIFEFLAG        PIC X(1).                            CPWSRHBN
006000     10 EXEC-LIFEFLAG-C      PIC X(1).                            CPWSRHBN
006100     10 EXEC-LIFESALARY      PIC S9(4) USAGE COMP.                CPWSRHBN
006200     10 EXEC-LIFESALARY-C    PIC X(1).                            CPWSRHBN
006300     10 MED-COVERCODE        PIC X(3).                            CPWSRHBN
006400     10 MED-COVERCODE-C      PIC X(1).                            CPWSRHBN
006500     10 MED-COVEFFDATE       PIC X(10).                           CPWSRHBN
006600     10 MED-COVEFFDATE-C     PIC X(1).                            CPWSRHBN
006700     10 MED-OPTOUT           PIC X(1).                            CPWSRHBN
006800     10 MED-OPTOUT-C         PIC X(1).                            CPWSRHBN
006900     10 MED-PLAN             PIC X(2).                            CPWSRHBN
007000     10 MED-PLAN-C           PIC X(1).                            CPWSRHBN
007100     10 INS-REDUCT-IND       PIC X(1).                            CPWSRHBN
007200     10 INS-REDUCT-IND-C     PIC X(1).                            CPWSRHBN
007300     10 LEGAL-COVERAGE       PIC X(3).                            CPWSRHBN
007400     10 LEGAL-COVERAGE-C     PIC X(1).                            CPWSRHBN
007500     10 LEGAL-COVEFFDATE     PIC X(10).                           CPWSRHBN
007600     10 LEGAL-COVEFFDATE-C   PIC X(1).                            CPWSRHBN
007700     10 LEGAL-PLAN           PIC X(2).                            CPWSRHBN
007800     10 LEGAL-PLAN-C         PIC X(1).                            CPWSRHBN
007900     10 LIFEINS-PLAN         PIC X(1).                            CPWSRHBN
008000     10 LIFEINS-PLAN-C       PIC X(1).                            CPWSRHBN
008100     10 LIFE-EFFDATE         PIC X(10).                           CPWSRHBN
008200     10 LIFE-EFFDATE-C       PIC X(1).                            CPWSRHBN
008300     10 LIFE-SALARY-BASE     PIC S9(4) USAGE COMP.                CPWSRHBN
008400     10 LIFE-SALARY-BASE-C   PIC X(1).                            CPWSRHBN
008500     10 LIFE-UCPAIDAMT       PIC X(3).                            CPWSRHBN
008600     10 LIFE-UCPAIDAMT-C     PIC X(1).                            CPWSRHBN
008700     10 UCDT-CODE            PIC X(1).                            CPWSRHBN
008800     10 UCDT-CODE-C          PIC X(1).                            CPWSRHBN
008900     10 OPT-COVERAGE         PIC X(3).                            CPWSRHBN
009000     10 OPT-COVERAGE-C       PIC X(1).                            CPWSRHBN
009100     10 OPT-COVEFFDATE       PIC X(10).                           CPWSRHBN
009200     10 OPT-COVEFFDATE-C     PIC X(1).                            CPWSRHBN
009300     10 OPTICAL-OPTOUT       PIC X(1).                            CPWSRHBN
009400     10 OPTICAL-OPTOUT-C     PIC X(1).                            CPWSRHBN
009500     10 OPT-PLAN             PIC X(2).                            CPWSRHBN
009600     10 OPT-PLAN-C           PIC X(1).                            CPWSRHBN
009700     10 LIFE-UCPD-EFFDATE    PIC X(10).                           CPWSRHBN
009800     10 LIFE-UCPD-EFFDT-C    PIC X(1).                            CPWSRHBN
009900     10 EXEC-LIFE-EFFDATE    PIC X(10).                           CPWSRHBN
010000     10 EXEC-LIFE-EFFDT-C    PIC X(1).                            CPWSRHBN
010100     10 UCDT-COVEFFDATE      PIC X(10).                           CPWSRHBN
010200     10 UCDT-COVEFFDATE-C    PIC X(1).                            CPWSRHBN
010210     10 EMP-HLTH-COVEFFDT    PIC X(10).                           36260771
010211     10 EMP-HLTH-CVEFDT-C    PIC X(1).                            36260771
010220     10 EMP-DENTL-COVEFFDT   PIC X(10).                           36260771
010221     10 EMP-DENTL-CVEFDT-C   PIC X(1).                            36260771
010230     10 EMP-VIS-COVEFFDT     PIC X(10).                           36260771
010231     10 EMP-VIS-CVEFDT-C     PIC X(1).                            36260771
010240     10 EMP-LEGAL-COVEFFDT   PIC X(10).                           36260771
010250     10 EMP-LEGAL-CVEFDT-C   PIC X(1).                            36260771
010260     10 HEALTH-COVEND-DATE   PIC X(10).                           54541281
010270     10 HEALTH-CVEND-DTE-C   PIC X(1).                            54541281
010280     10 DENTAL-COVEND-DATE   PIC X(10).                           54541281
010290     10 DENTAL-CVEND-DTE-C   PIC X(1).                            54541281
010291     10 VISION-COVEND-DATE   PIC X(10).                           54541281
010292     10 VISION-CVEND-DTE-C   PIC X(1).                            54541281
010293     10 LEGAL-COVEND-DATE    PIC X(10).                           54541281
010294     10 LEGAL-CVEND-DTE-C    PIC X(1).                            54541281
014400     10 STATE-DECLAR-DPI     PIC X(1).                            48661418
014500     10 STATE-DECLAR-DPI-C   PIC X(1).                            48661418
014510     10 MED-CONT-BASE-CUR    PIC S9(4) USAGE COMP.                01911440
014520     10 MED-CONT-BASE-CR-C   PIC X(1).                            01911440
014600*****                                                          CD 01911440
