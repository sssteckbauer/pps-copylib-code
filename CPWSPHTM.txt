000100**************************************************************/   52131415
000200*  COPYMEMBER: CPWSPHTM                                      */   52131415
000300*  RELEASE: ___1415______ SERVICE REQUEST(S): ____15213____  */   52131415
000400*  NAME:_______MLG_______ MODIFICATION DATE:  __06/10/02____ */   52131415
000500*  DESCRIPTION:                                              */   52131415
000600*    INTERFACE TO NOTIFICATION HTML GENERATORS               */   52131415
000800**************************************************************/   52131415
000900*01* CPWSPHTM     EXTERNAL.                                       CPWSPHTM
001000     05  CPWSPHTM-AREA   PIC X(1599488).                          CPWSPHTM
001100     05  CPWSPHTM-DATA REDEFINES CPWSPHTM-AREA.                   CPWSPHTM
001200         10  CPWSPHTM-LINES.                                      CPWSPHTM
001210             15 FILLER OCCURS 5300.                               CPWSPHTM
001220                20  CPWSPHTM-LINE-SUBTYPE  PIC X(01).             CPWSPHTM
001230                20  CPWSPHTM-LINE          PIC X(300).            CPWSPHTM
001600         10  CPWSPHTM-LINES-RETURNED       PIC 9(04) COMP.        CPWSPHTM
001700         10  CPWSPHTM-MAX-LINES            PIC 9(04) COMP.        CPWSPHTM
001900         10  FILLER                        PIC X(4184).           CPWSPHTM
002000***************  END OF COPYMEMBER CPWSPHTM   ******************  CPWSPHTM
