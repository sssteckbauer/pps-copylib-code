000010*==========================================================%      UCSD0016
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0016
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD0016     =%      UCSD0016
000040*=                                                        =%      UCSD0016
000050*==========================================================%      UCSD0016
000060*==========================================================%      UCSD0016
000070*=    COPY MEMBER: CPWSPSHR                               =%      UCSD0016
000080*=    CHANGE # 00016        PROJ. REQUEST:                =%      UCSD0016
000090*=    NAME: MARI MCGEE      MODIFICATION DATE: 03/18/87   =%      UCSD0016
000091*=                                                        =%      UCSD0016
000092*=    DESCRIPTION     CHANGE TITLE FROM SYSTEMWIDE TO     =%      UCSD0016
000093*=                    SAN DIEGO                           =%      UCSD0016
000095*=                                                        =%      UCSD0016
000096*==========================================================%      UCSD0016
000100**************************************************************/   36300699
000200*  COPYMENBER: CPWSPSHR                                      */   36300699
000300*  RELEASE: ____0699____  SERVICE REQUEST(S): ____3630____   */   36300699
000400*  NAME:____PXP___________CREATION DATE:      __09/17/92__   */   36300699
000500*  DESCRIPTION:                                              */   36300699
000600*  - ADD PAYROLL PROCESS ID                                  */   36300699
000700**************************************************************/   36300699
000100**************************************************************/   37260489
000200*  COPYMENBER: CPWSPSHR                                      */   37260489
000300*  RELEASE: ____0489____  SERVICE REQUEST(S): ____3726____   */   37260489
000400*  NAME:____JOANNE LOE____CREATION DATE:      __07/31/89__   */   37260489
000500*  DESCRIPTION:                                              */   37260489
000600*  - CORRECTED CENTERING OF STND-FUNC-AREA                   */   37260489
000700**************************************************************/   37260489
000097**************************************************************/   30930413
000098*  COPYMEMBER: CPWSPSHR                                      */   30930413
000099*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000100*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000101*  DESCRIPTION                                               */   30930413
000102*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000103**************************************************************/   30930413
000110*    COPYID=CPWSPSHR                                              CPWSPSHR
000120*01  STND-RPT-HD.                                                 30930413
000300     02  STND-RPT-HD1.                                            CPWSPSHR
000400       05  CARRIAGE-CTL-CHAR     PIC X           VALUE SPACE.     CPWSPSHR
000500       05  STND-RPT-ID           PIC X(7)        VALUE SPACES.    CPWSPSHR
000600       05  FILLER                PIC X           VALUE '/'.       CPWSPSHR
000700       05  STND-PROG-ID          PIC X(15)       VALUE SPACES.    CPWSPSHR
000800       05  STND-RPT-INST.                                         CPWSPSHR
000900         10  FILLER              PIC X(27)       VALUE SPACES.    CPWSPSHR
001000         10  FILLER              PICTURE X(40)   VALUE            CPWSPSHR
004600*****      'UNIVERSITY OF CALIFORNIA-SYSTEMWIDE'.                 UCSD0016
001110           'UNIVERSITY OF CALIFORNIA-SAN DIEGO '.                 UCSD0016
001200         10  FILLER              PICTURE X(21)   VALUE SPACES.    CPWSPSHR
001300       05  STND-RPT-INST1    REDEFINES                            CPWSPSHR
001400           STND-RPT-INST.                                         CPWSPSHR
001500         10  FILLER              PIC X(52).                       CPWSPSHR
001600         10  STND-CAMPUS-NAME    PIC X(20).                       CPWSPSHR
001700         10  FILLER              PIC X(16).                       CPWSPSHR
001800       05  FILLER                PICTURE X(17)   VALUE            CPWSPSHR
001900           'PAGE NO.'.                                            CPWSPSHR
002000       05  STND-PAGE-NO          PIC 9(4)        VALUE 0.         CPWSPSHR
002100     02  STND-RPT-HD2.                                            CPWSPSHR
002200       05  CARRIAGE-CTL-CHAR     PIC X           VALUE SPACES.    CPWSPSHR
002300       05  STND-RPT-RETN         PICTURE X(51)   VALUE            CPWSPSHR
002400           'RETN: SEE RPTS DISP SCHEDULE/DIST.'.                  CPWSPSHR
002500       05  STND-FUNC-AREA        PIC X(30)       VALUE            CPWSPSHR
004700*****                                                          CD 36300699
004100             '        PAYROLL PROCESSING'.                        37260489
002700       05  FILLER                PICTURE X(30)   VALUE SPACES.    CPWSPSHR
002800       05  FILLER                PICTURE X(13)   VALUE            CPWSPSHR
002900           'RUN DATE'.                                            CPWSPSHR
003000       05  STND-RPT-DATE         PIC X(8)        VALUE SPACES.    CPWSPSHR
003100     02  STND-RPT-HD3.                                            CPWSPSHR
003200       05  CARRIAGE-CTL-CHAR     PIC X           VALUE SPACE.     CPWSPSHR
003300       05  STND-RPT-COMMENT.                                      CPWSPSHR
003400         10  STND-RPT-END-DATE   PICTURE X(22)   VALUE            CPWSPSHR
003500             'PAY PERIOD END DATE'.                               CPWSPSHR
003600         10  STND-RPT-END-MONTH  PICTURE XX      VALUE SPACES.    CPWSPSHR
003700         10  FILLER              PICTURE X       VALUE '/'.       CPWSPSHR
003800         10  STND-RPT-END-DAY    PICTURE XX      VALUE SPACES.    CPWSPSHR
003900         10  FILLER              PICTURE X       VALUE '/'.       CPWSPSHR
004000         10  STND-RPT-END-YR     PICTURE XX      VALUE SPACES.    CPWSPSHR
004100         10  FILLER              PICTURE XX      VALUE SPACES.    CPWSPSHR
004200         10  STND-RPT-CYCLES     PICTURE X(12)   VALUE SPACES.    CPWSPSHR
004300         10  FILLER              REDEFINES STND-RPT-CYCLES.       CPWSPSHR
004400           15  FILLER            OCCURS 4.                        CPWSPSHR
004500             20  STND-RPT-CYCLE  PICTURE XX.                      CPWSPSHR
004600             20  FILLER          PICTURE X.                       CPWSPSHR
004700         10  FILLER              PICTURE X(02)   VALUE SPACES.    CPWSPSHR
004800       05  STND-RPT-TTL.                                          CPWSPSHR
004900         10  STND-RPT-TTL-RDF    PICTURE X(50)   VALUE SPACES.    CPWSPSHR
007200*****  05  FILLER                PICTURE X(15)   VALUE SPACES.    36300699
007300       05  STND-PPI-TTL          PICTURE X(05)   VALUE SPACES.    36300699
007400       05  STND-PPI              PICTURE X(08)   VALUE SPACES.    36300699
007500       05  FILLER                PICTURE X(02)   VALUE SPACES.    36300699
005100       05  STND-DATE2-TTL        PICTURE X(13)   VALUE            CPWSPSHR
005200             'CHECK DATE'.                                        CPWSPSHR
005300       05  STND-DATE2.                                            CPWSPSHR
005400         10  STND-DATE2-MONTH    PIC XX          VALUE SPACES.    CPWSPSHR
005500         10  FILLER              PIC X           VALUE '/'.       CPWSPSHR
005600         10  STND-DATE2-DAY      PIC XX          VALUE SPACES.    CPWSPSHR
005700         10  FILLER              PIC X           VALUE '/'.       CPWSPSHR
005800         10  STND-DATE2-YEAR     PIC XX          VALUE SPACES.    CPWSPSHR
005900     SKIP2                                                        CPWSPSHR
