000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPWSXESP                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/27/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    EXPANDED FUND TO SIX CHARACTERS.                    =%      UCSD0102
000800*=                                                        =%      UCSD0102
000900*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXESP                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   45040363
000200*  COPY MODULE:  CPWSXESP                                    */   45040363
000300*  RELEASE # ___0363___   SERVICE REQUEST NO(S) ____4504____ */   45040363
000400*  NAME ___K.KELLER____   MODIFICATION DATE ____07/22/88_____*/   45040363
000500*                                                            */   45040363
000600*  DESCRIPTION                                               */   45040363
000700*       ADDED FOR EMPLOYEE SUPPORT TABLE RATES.  DESCRIBES   */   45040363
000800*       RECORDS CONTAINED IN CONTROL TABLE 27 (ESP).         */   45040363
000900**************************************************************/   45040363
001000*    COPYID=CPWSXESP                                              CPWSXESP
001100*01  XESP-SUP-RATE-TABLE-RECORD.                                  30930413
001200*-----------------------------------------------------------------CPWSXESP
001300*       E M P L O Y E E    S U P P O R T   P R O G R A M          CPWSXESP
001400*       R A T E   T A B L E   R E C O R D                         CPWSXESP
001500*-----------------------------------------------------------------CPWSXESP
001600     05 XESP-DELETE                  PIC X(01).                   CPWSXESP
001700     05 XESP-KEY.                                                 CPWSXESP
001800         10  XESP-KEY-CONSTANT       PIC X(04).                   CPWSXESP
001900         10  FILLER                  PIC X(02).                   CPWSXESP
002000         10  XESP-SUP-ID-KEY.                                     CPWSXESP
002100             15  XESP-SUP-LOC-CODE   PIC X(01).                   CPWSXESP
002200             15  XESP-SUP-ID-TYPE    PIC X(01).                   CPWSXESP
002300             15  XESP-SUP-SEQ-NO     PIC X(02).                   CPWSXESP
002400         10  FILLER                  PIC X(03).                   CPWSXESP
002500     05  XESP-ACCT-FUND-AREA         PIC X(12).                   CPWSXESP
002600     05  XESP-ACCT-RANGE-DATA REDEFINES XESP-ACCT-FUND-AREA.      CPWSXESP
002700         10  XESP-LO-ACCT            PIC X(06).                   CPWSXESP
002800         10  XESP-HI-ACCT            PIC X(06).                   CPWSXESP
002900     05  XESP-FUND-RANGE-DATA REDEFINES XESP-ACCT-FUND-AREA.      CPWSXESP
003000*****    10  XESP-LO-FUND            PIC X(05).                   UCSD0102
003100*****    10  FILLER                  PIC X(01).                   UCSD0102
003200*****    10  XESP-HI-FUND            PIC X(05).                   UCSD0102
003300*****    10  FILLER                  PIC X(01).                   UCSD0102
003000         10  XESP-LO-FUND            PIC X(06).                   UCSD0102
003000         10  XESP-HI-FUND            PIC X(06).                   UCSD0102
003400     05  XESP-ACCT-FUND-DATA REDEFINES XESP-ACCT-FUND-AREA.       UCSD0102
003500         10  XESP-ACCT               PIC X(06).                   CPWSXESP
003600         10  XESP-FUND               PIC X(06).                   UCSD0102
003600*****    10  XESP-FUND               PIC X(05).                   UCSD0102
003700*****    10  FILLER                  PIC X(01).                   UCSD0102
003800     05  FILLER                      PIC X(08).                   CPWSXESP
003900     05  XESP-SUP-RATE               PIC 9V9999.                  CPWSXESP
004000     05  FILLER                      PIC X(178).                  CPWSXESP
004100****************************************************************/ CPWSXESP
004200*  COMMON UPDATE AREA                                          */ CPWSXESP
004300****************************************************************/ CPWSXESP
004400     05  XESP-LAST-ACTION            PIC X(01).                   CPWSXESP
004500     05  XESP-LAST-UPDATE            PIC X(06).                   CPWSXESP
