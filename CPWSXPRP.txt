000100**************************************************************/   45310516
000200*  COPYMEMBER: CPWSXPRP                                      */   45310516
000300*  RELEASE # ____0516__   SERVICE REQUEST NO(S)___4531_______*/   45310516
000400*  NAME __M. BAPTISTA__   MODIFICATION DATE ____08/01/90_____*/   45310516
000500*  DESCRIPTION                                               */   45310516
000600*   - ADDED PRORATION PERCENTAGE AND MAX HOURS TO TYPE 1 REC.*/   45310516
000700**************************************************************/   45310516
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXPRP                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000200**************************************************************/   SUP10152
000200****************************************************************/ 13760241
000300* COPYMEMBER:    CPWSXPRP                                      */ 13760241
000400* REL: ______241_____   SERVICE REQUESTS:  1376, 1379          */ 13760241
000500* NAME: ___J.A.L._____  MODIFICATION DATE:___10/10/86________  */ 13760241
000600* DESCRIPTION:                                                 */ 13760241
000700*                                                              */ 13760241
000800* SERV REQUEST 1376    SPECIAL PAYMENTS                        */ 13760241
000900* SERV REQUEST 1379    DOS MAPPING                             */ 13760241
001000*  AND MISCELLANEOUS FIXES                                     */ 13760241
001100*                                                              */ 13760241
001200* FOUR RECORD  TYPES ARE ADDED TO PRP TABLE TO ACCOMMODATE     */ 13760241
001300* SPECIAL PAYMENTS AND DOS MAPPING                             */ 13760241
001400*    TYPE 4: DOS MAP IND, DOS DEFAULT IND, DOS DEFAULT CODE    */ 13760241
001500*    TYPE 5: DOS MAPPING: OLD DOS CODE/NEW DOS CODE            */ 13760241
001600*    TYPE 6: DOS HIRE DATE/SEPARATION DATE                     */ 13760241
001700*    TYPE 7: ELIGIBILITY FIRST/LAST DATES                      */ 13760241
001800*                                                            */   13760241
001900**************************************************************/   13760241
002000*  COPY MODULE:  CPWSXPRP                                    */   SUP10152
002100*  RELEASE # ___0152___   SERVICE REQUEST NO(S) ____2198____ */   SUP10152
002200*  NAME __M.NISHI _____   MODIFICATION DATE ____07/22/85_____*/   SUP10152
002300*  DESCRIPTION                                               */   SUP10152
002400*       AN ADDTION OF A DATE TO COMPARE XEDB-0113-EMPLOYMENT */   SUP10152
002500*       -DATE FOR THE PRIOR PERIOD PROCESS HAS BEEN MADE SO  */   SUP10152
002600*       THAT AN ADDED TEST CAN BE MADE FOR ELIGIBILITY.      */   SUP10152
002700*                                                            */   SUP10152
002800**************************************************************/   SUP10152
002900**************************************************************/   21980149
003000*  COPY MODULE:  CPWSXPRP                                    */   21980149
003100*  RELEASE # ___0149___   SERVICE REQUEST NO(S) ____2198____ */   21980149
003200*  NAME __P.JAMES______   MODIFICATION DATE ____06/01/85_____*/   21980149
003300*  DESCRIPTION                                               */   21980149
003400*       THIS IS A NEW TABLE TO BE USED FOR RANGE ADJUSTMENT  */   21980149
003500*       PROCESSING.  IT IS CALLED THE PRIOR PERIOD TABLE     */   21980149
003600*       (TABLE 20) TO BE USED BY PROGRAM PPP930 ONLY.        */   21980149
003700*                                                            */   21980149
003800**************************************************************/   21980149
003900*    COPYID=CPWSXPRP                                              CPWSXPRP
004000*01  XPRP-RECORD.                                                 30930413
004100*-----------------------------------------------------------------CPWSXPRP
004200*       PRIOR PERIOD WORK AREA FOR RECORD TYPE 1, 2, 3            CPWSXPRP
004300*-----------------------------------------------------------------CPWSXPRP
004400         05  XPRP-DELETE             PIC X.                       CPWSXPRP
004500         05  XPRP-KEY.                                            CPWSXPRP
004600             10  XPRP-KEY-FILL       PIC X(05).                   CPWSXPRP
004700             10  XPRP-REC-TYPE       PIC X(01).                   CPWSXPRP
004800             10  XPRP-FILL           PIC X(07).                   CPWSXPRP
004900         05  XPRP-DATA.                                           CPWSXPRP
005000             10  XPRP-PROCESS-IND         PIC X(01).              13760241
005100*** PAYMENTS ARE TO BE MADE FOR THESE PAR EARNING END DATES:      13760241
005200             10  XPRP-MONTH-BEGIN-DTE.                            CPWSXPRP
005300                 15  XPRP-MONTH-BEGIN-YY  PIC 9(2).               CPWSXPRP
005400                 15  XPRP-MONTH-BEGIN-MM  PIC 9(2).               CPWSXPRP
005500                 15  XPRP-MONTH-BEGIN-DD  PIC 9(2).               CPWSXPRP
007000             10  XPRP-MONTH-BEGIN-PCT     PIC 9(3).               45310516
007100             10  XPRP-MONTH-BEGIN-HOURS   PIC 9(3).               45310516
005600             10  XPRP-MONTH-END-DTE.                              CPWSXPRP
005700                 15  XPRP-MONTH-END-YY    PIC 9(2).               CPWSXPRP
005800                 15  XPRP-MONTH-END-MM    PIC 9(2).               CPWSXPRP
005900                 15  XPRP-MONTH-END-DD    PIC 9(2).               CPWSXPRP
007600             10  XPRP-MONTH-END-PCT       PIC 9(3).               45310516
007700             10  XPRP-MONTH-END-HOURS     PIC 9(3).               45310516
007800***                                                               45310516
007900*** THOUGH THE FOLLOWING FIELDS ARE NAMED WITH '-BI-' THEY        45310516
008000*** ARE ALSO USED FOR SEMI-MONTHLY DATE RANGES.                   45310516
008100             10  XPRP-BI-OR-SM-IND        PIC X(1).               45310516
006000             10  XPRP-BI-BEGIN-DTE.                               CPWSXPRP
006100                 15  XPRP-BI-BEGIN-YY     PIC 9(2).               CPWSXPRP
006200                 15  XPRP-BI-BEGIN-MM     PIC 9(2).               CPWSXPRP
006300                 15  XPRP-BI-BEGIN-DD     PIC 9(2).               CPWSXPRP
008600             10  XPRP-BI-BEGIN-PCT        PIC 9(3).               45310516
008700             10  XPRP-BI-BEGIN-HOURS      PIC 9(3).               45310516
006400             10  XPRP-BI-END-DTE.                                 CPWSXPRP
006500                 15  XPRP-BI-END-YY       PIC 9(2).               CPWSXPRP
006600                 15  XPRP-BI-END-MM       PIC 9(2).               CPWSXPRP
006700                 15  XPRP-BI-END-DD       PIC 9(2).               CPWSXPRP
009200             10  XPRP-BI-END-PCT          PIC 9(3).               45310516
009300             10  XPRP-BI-END-HOURS        PIC 9(3).               45310516
009400*****                                                          CD 45310516
009500             10  FILLER                   PIC X(153).             45310516
009600*****        10  FILLER                   PIC X(178).             45310516
009700*****                                                          CD 45310516
007500         05  XPRP-DATA2 REDEFINES XPRP-DATA.                      CPWSXPRP
009900*****                                                          CD 45310516
007700             10  XPRP-DOS-INDICATOR       PIC X(1).               CPWSXPRP
010100*****                                                          CD 45310516
008200             10  FILLER                   PIC X(202).             13760241
008300         05  XPRP-DATA-3  REDEFINES XPRP-DATA.                    13760241
008400             10  XPRP-DOS OCCURS 50 TIMES PIC X(03).              13760241
008500             10  FILLER                   PIC X(53).              13760241
008600                                                                  13760241
008700*** DOS MAPPING/DOS DEFAULT CRITERIA FOLLOW:                      13760241
008800         05  XPRP-DATA-4  REDEFINES XPRP-DATA.                    13760241
008900             10  XPRP-DOS-MAPPING-IND     PIC X(01).              13760241
009000             10  XPRP-DOS-DEFAULT-IND     PIC X(01).              13760241
009100             10  XPRP-RA-DOS              PIC X(03).              13760241
009200             10  FILLER                   PIC X(198).             13760241
009300         05  XPRP-DATA-5 REDEFINES XPRP-DATA.                     13760241
009400             10  XPRP-OLD-NEW OCCURS 33 TIMES.                    13760241
009500                 15  XPRP-OLD-DOS         PIC X(03).              13760241
009600                 15  XPRP-NEW-DOS         PIC X(03).              13760241
011700             10  FILLER                   PIC X(5).               45310516
011800*****        10  FILLER                   PIC X(4).               45310516
009800                                                                  13760241
009900*---- SPECIAL PAYMENTS OPTIONAL ELIGIBILITY CRITERIA FOLLOW ----- 13760241
010000                                                                  13760241
010100         05  XPRP-DATA-6  REDEFINES XPRP-DATA.                    13760241
010200*** ELIGIBILITY BASED ON HIRE/SEPARATION DATES (INCLUSIVE)        13760241
010300             10  XPRP-HIRE-ELIG-DATE.                             13760241
010400                 15  XPRP-HIRE-ELIG-YY    PIC 9(02).              13760241
010500                 15  XPRP-HIRE-ELIG-MM    PIC 9(02).              13760241
010600                 15  XPRP-HIRE-ELIG-DD    PIC 9(02).              13760241
010700             10  XPRP-SEP-ELIG-DATE.                              13760241
010800                 15  XPRP-SEP-ELIG-YY     PIC 9(02).              13760241
010900                 15  XPRP-SEP-ELIG-MM     PIC 9(02).              13760241
011000                 15  XPRP-SEP-ELIG-DD     PIC 9(02).              13760241
011100             10  FILLER                   PIC X(191).             13760241
011200         05  XPRP-DATA-7  REDEFINES XPRP-DATA.                    13760241
011300                                                                  13760241
011400*** ELIGIBILITY BASED ON FIRST/LAST PAYMENT DATES                 13760241
011500*** MUST HAVE PAR EARNINGS WITH FOLLOWING DATES TO BE ELIGIBLE    13760241
011600***   FOR THE SPECIAL PAYMENTS MADE FOR PAY PERIOD SPECIFIED IN   13760241
011700***   TYPE 1 RECORD                                               13760241
011800             10  XPRP-PAR-MO-FIRST-DATE.                          13760241
011900                 15  XPRP-PAR-MO-FIRST-YY PIC 9(02).              13760241
012000                 15  XPRP-PAR-MO-FIRST-MM PIC 9(02).              13760241
012100                 15  XPRP-PAR-MO-FIRST-DD PIC 9(02).              13760241
012200             10  XPRP-PAR-SM-FIRST-DATE.                          13760241
012300                 15  XPRP-PAR-SM-FIRST-YY PIC 9(02).              13760241
012400                 15  XPRP-PAR-SM-FIRST-MM PIC 9(02).              13760241
012500                 15  XPRP-PAR-SM-FIRST-DD PIC 9(02).              13760241
012600             10  XPRP-PAR-BW-FIRST-DATE.                          13760241
012700                 15  XPRP-PAR-BW-FIRST-YY PIC 9(02).              13760241
012800                 15  XPRP-PAR-BW-FIRST-MM PIC 9(02).              13760241
012900                 15  XPRP-PAR-BW-FIRST-DD PIC 9(02).              13760241
013000             10  XPRP-PAR-MO-LAST-DATE.                           13760241
013100                 15  XPRP-PAR-MO-LAST-YY  PIC 9(02).              13760241
013200                 15  XPRP-PAR-MO-LAST-MM  PIC 9(02).              13760241
013300                 15  XPRP-PAR-MO-LAST-DD  PIC 9(02).              13760241
013400             10  XPRP-PAR-SM-LAST-DATE.                           13760241
013500                 15  XPRP-PAR-SM-LAST-YY  PIC 9(02).              13760241
013600                 15  XPRP-PAR-SM-LAST-MM  PIC 9(02).              13760241
013700                 15  XPRP-PAR-SM-LAST-DD  PIC 9(02).              13760241
013800             10  XPRP-PAR-BW-LAST-DATE.                           13760241
013900                 15  XPRP-PAR-BW-LAST-YY  PIC 9(02).              13760241
014000                 15  XPRP-PAR-BW-LAST-MM  PIC 9(02).              13760241
014100                 15  XPRP-PAR-BW-LAST-DD  PIC 9(02).              13760241
014200             10  FILLER                   PIC X(167).             13760241
014300*                                                                 13760241
014400*  LAST UPDATE INFO. ADDED FOR CONSISTENCY:                       13760241
014500*                                                                 13760241
014600         05  XPRP-LAST-UPDT-INFO.                                 13760241
014700             10  XPRP-LAST-UPDT-ACTION    PIC X(01).              13760241
014800             10  XPRP-LAST-UPDT-DATE      PIC X(06).              13760241
014900*                 FORMAT  YYMMDD.                                 13760241
