000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPFAU02                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______JLT_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*   Initial release of Procedure Division copymember which   */   32021138
000700*   is used by program PPP520 in conjunction with copymember */   32021138
000800*   CPWSFAUE.                                                */   32021138
000900**************************************************************/   32021138
001000*                                                             *   CPPFAU02
001100*  This copymember is one of the Full Accounting Unit modules *   CPPFAU02
001200*  which campuses may need to modify to accomodate their own  *   CPPFAU02
001300*  Chart of Accounts structure.                               *   CPPFAU02
001400*                                                             *   CPPFAU02
001500***************************************************************   CPPFAU02
001600*                                                                 CPPFAU02
001700***************************************************************** CPPFAU02
001800*  ADJUST FAU FOR BLANK ELEMENTS AS NECESSARY (E.G., LOCATION)    CPPFAU02
001900*  AS CONTAINED ON TYPE 4 AND 5 EDR RECORDS AFTER FAU RETRIEVAL   CPPFAU02
002000*  FROM GTN TABLE.                                                CPPFAU02
002100***************************************************************** CPPFAU02
002200                                                                  CPPFAU02
002300        MOVE FAUE-FAU-CHAR30      TO FAUE-FAU-UNFORMATTED         CPPFAU02
002400        IF FAUE-FAU-LOCATION  =  SPACES                           CPPFAU02
002500            MOVE LOCATION-1CHR        TO FAUE-FAU-LOCATION        CPPFAU02
002600        END-IF                                                    CPPFAU02
002700        MOVE FAUE-FAU-UNFORMATTED TO FAUE-FAU-CHAR30              CPPFAU02
002800                                                                  CPPFAU02
002900************** END OF COPY CPPFAU02 ***************************   CPPFAU02
