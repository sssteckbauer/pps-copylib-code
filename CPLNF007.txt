000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNF007                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_____J.WILCOX____ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*    Initial release of linkage copymember between calling   */   32021138
000700*    program and modules PPFAU007 and PPFAU008.              */   32021138
000800**************************************************************/   32021138
000900*                                                             *   CPLNF007
001000*  This copymember is one of the Full Accounting Unit modules *   CPLNF007
001100*  which campuses may need to modify to accomodate their own  *   CPLNF007
001200*  Chart of Accounts structure.                               *   CPLNF007
001300*                                                             *   CPLNF007
001400*  In particular, depending upon the need for validation at   *   CPLNF007
001500*  various levels, campuses may need fewer or more levels of  *   CPLNF007
001600*  validation failure.                                        *   CPLNF007
001700*                                                             *   CPLNF007
001800*  The base version indicates one possible level of failure:  *   CPLNF007
001900*    Level 1 - the provided Location-Account combination was  *   CPLNF007
002000*              not found on the AFP Account file.             *   CPLNF007
002100***************************************************************   CPLNF007
002200*                                                                 CPLNF007
002300*01  PPFAU007-INTERFACE.                                          CPLNF007
002400    05  F007-INPUTS.                                              CPLNF007
002500        10  F007-FAU                     PIC X(30).               CPLNF007
002600    05  F007-OUTPUTS.                                             CPLNF007
002700        10  F007-RETURN-VALUE            PIC X(01).               CPLNF007
002800            88  F007-FAU-VALID             VALUE '0'.             CPLNF007
002900            88  F007-INVALID-LEVEL-1       VALUE '1'.             CPLNF007
003000            88  F007-ROUTINE-FAILURE       VALUE 'X'.             CPLNF007
003100        10  F007-TITLE                   PIC X(35).               CPLNF007
003200        10  F007-FAILURE-TEXT            PIC X(30).               CPLNF007
