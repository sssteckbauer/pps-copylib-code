000000**************************************************************/   28521025
000001*  COPYMEMBER: CPLNKBEL                                      */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/01/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    INCLUDED THE CENTURY IN THE DATE STRUCTURE.             */   28521025
000007**************************************************************/   28521025
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPLNKBEL                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   30920381
000200*  COPYLIB:  CPLNKBEL                                        */   30920381
000300*  REL#:  0381   REF: NONE  SERVICE REQUESTS:   ____3092____ */   30920381
000400*  NAME ___J. WILCOX___       CREATION DATE ____11/10/88_____*/   30920381
000500*  DESCRIPTION                                               */   30920381
000600*    NEW COPY MEMBER - LINKAGE BETWEEN CALLING PROGRAMS AND  */   30920381
000700*                      MODULE PPBENBEL (BENEFITS ELIGIBILITY */   30920381
000800*                      DERIVATION)                           */   30920381
000900*                                                            */   30920381
001000**************************************************************/   30920381
001100*COPYID=CPLNKBEL                                                  CPLNKBEL
001200******************************************************************CPLNKBEL
001300*                        C P L N K B E L                         *CPLNKBEL
001400******************************************************************CPLNKBEL
001500*                                                                *CPLNKBEL
001600*01  PPPBELI-INTERFACE.                                           30930413
001700*                                                                 CPLNKBEL
001800******************************************************************CPLNKBEL
001900*      THE FOLLOWING FLAGS AND FIELDS ARE PASSED BY CALLER       *CPLNKBEL
002000******************************************************************CPLNKBEL
002100*                                                                 CPLNKBEL
002200     05  PPPBELI-CALL-FLAG           PIC X(01).                   CPLNKBEL
002300         88  PPPBELI-INITIALIZE-CALL             VALUE 'I'.       CPLNKBEL
002400         88  PPPBELI-DERIVE-CALL                 VALUE 'D'.       CPLNKBEL
002500*                                                                 CPLNKBEL
002600     05  PPPBELI-AS-OF-DATE.                                      CPLNKBEL
002700*****    10  PPPBELI-AS-OF-YY        PIC 99.                      28521025
002700         10  PPPBELI-AS-OF-CCYY      PIC 9999.                    28521025
002700         10  FILLER REDEFINES PPPBELI-AS-OF-CCYY.                 28521025
002701             15  PPPBELI-AS-OF-CC    PIC 99.                      28521025
002710             15  PPPBELI-AS-OF-YY    PIC 99.                      28521025
002800         10  PPPBELI-AS-OF-MM        PIC 99.                      CPLNKBEL
002900*                                                                 CPLNKBEL
003000     05  PPPBELI-AVG-HRS-PER-WK      PIC 999V99.                  CPLNKBEL
003100         88  PPPBELI-AVG-IS-VALID                VALUE 000.01     CPLNKBEL
003200                                                 THRU  999.99.    CPLNKBEL
003300         88  PPPBELI-AVG-IS-INVALID              VALUE 000.00.    CPLNKBEL
003400*                                                                 CPLNKBEL
003500******************************************************************CPLNKBEL
003600*      THE FOLLOWING FLAGS AND FIELDS ARE RETURNED TO CALLER     *CPLNKBEL
003700******************************************************************CPLNKBEL
003800*                                                                 CPLNKBEL
003900     05  PPPBELI-RETURN-STATUS-FLAG  PIC 99.                      CPLNKBEL
004000         88  PPPBELI-NORMAL-STATUS               VALUE  ZERO.     CPLNKBEL
004100         88  PPPBELI-ERROR-STATUS                VALUES 01 THRU   CPLNKBEL
004200                                                        99.       CPLNKBEL
004300         88  PPPBELI-IOCTL-ERROR                 VALUES 01 THRU   CPLNKBEL
004400                                                        30.       CPLNKBEL
004500         88  PPPBEIL-INVALID-CALL                VALUE  31.       CPLNKBEL
004600         88  PPPBELI-INVALID-CTL-STATUS          VALUE  32.       CPLNKBEL
004700         88  PPPBELI-TITLE-TABLE-OVERFLOW        VALUE  33.       CPLNKBEL
004800         88  PPPBELI-APPT-TABLE-OVERFLOW         VALUE  34.       CPLNKBEL
004900*                                                                 CPLNKBEL
005000     05  PPPBELI-DERIVED-BELI         PIC X(01).                  CPLNKBEL
005100*                                                                *CPLNKBEL
005200***********> END OF CPLNKBEL <************************************CPLNKBEL
