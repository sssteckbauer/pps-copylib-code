000000**************************************************************/   48441325
000001*  COPYMEMBER: CPPDIIDX                                      */   48441325
000002*  RELEASE: ___1325______ SERVICE REQUEST(S): ____14844____  */   48441325
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___11/10/00__  */   48441325
000004*  DESCRIPTION:                                              */   48441325
000005*     ADDED HISTORY TAX TRANSACTION DATE (HTX)               */   48441325
000007**************************************************************/   48441325
000100**************************************************************/   36290827
000200*  COPYMEMBER: CPPDIIDX                                      */   36290827
000300*  RELEASE: ____0827____  SERVICE REQUEST(S): ____3629____   */   36290827
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __10/21/93__   */   36290827
000500*  DESCRIPTION:                                              */   36290827
000600*     ADDED LAYOFF SENIORITY CREDIT DATE (LCR)               */   36290827
000700**************************************************************/   36290827
000800**************************************************************/   36450793
000900*  COPYMEMBER: CPPDIIDX                                      */   36450793
001000*  RELEASE: ___0793______ SERVICE REQUEST(S): _____3645____  */   36450793
001100*  NAME:_______DDM_______ MODIFICATION DATE:  __07/09/93____ */   36450793
001200*  DESCRIPTION: INITIALIZE DATES IN PPPIDX ROW.              */   36450793
001300**************************************************************/   36450793
001400     PERFORM                                                      CPPDIIDX
001500     MOVE '0001-01-01'     TO PPPAPN-C                            CPPDIIDX
001600                              PPPATN-C                            CPPDIIDX
001700                              PPPAWD-C                            CPPDIIDX
001800                              PPPBAS-C                            CPPDIIDX
001900                              PPPCBG-C                            CPPDIIDX
002000                              PPPDST-C                            CPPDIIDX
002100                              PPPFSC-C                            CPPDIIDX
002200                              PPPHBN-C                            CPPDIIDX
002300                              PPPHNR-C                            CPPDIIDX
002400                              PPPLEV-C                            CPPDIIDX
002500                              PPPLIC-C                            CPPDIIDX
002600                              PPPMTH-C                            CPPDIIDX
002700                              PPPQTR-C                            CPPDIIDX
002800                              PPPSVC-C                            CPPDIIDX
002900                              PPPYRL-C                            CPPDIIDX
003000                              PPPSAB-C                            CPPDIIDX
003100                              PPPOFF-C                            CPPDIIDX
003200                              PPPHDP-C                            CPPDIIDX
003300                              PPPBKG-C                            CPPDIIDX
003400                              PPPLCR-C                            36290827
003410                              PPPHTX-C                            48441325
003500     END-PERFORM.                                                 CPPDIIDX
