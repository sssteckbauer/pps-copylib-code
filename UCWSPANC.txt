000010**************************************************************/   52131415
000020*  COPYMEMBER: UCWSPANC                                      */   52131415
000030*  RELEASE: ___1415______ SERVICE REQUEST(S): ____15213____  */   52131415
000040*  NAME:_______MLG_______ MODIFICATION DATE:  __06/10/02____ */   52131415
000050*  DESCRIPTION:                                              */   52131415
000060*     ADDED NEW FIELDS FOR PAN ENHANCEMENT.                  */   52131415
000070*                                                            */   52131415
000080**************************************************************/   52131415
000100**************************************************************/   36480863
000200*  COPYMEMBER: UCWSPANC                                      */   36480863
000300*  RELEASE: ___0863______ SERVICE REQUEST(S): _____3648____  */   36480863
000400*  NAME:_______AAC_______ MODIFICATION DATE:  __01/27/94____ */   36480863
000500*  DESCRIPTION:                                              */   36480863
000600*     PAN SUBSYSTEM: UCPANMGR INTERFACE CONTROL/AREA         */   36480863
000700*                                                            */   36480863
000800**************************************************************/   36480863
000900*01  UCWSPANC EXTERNAL.                                           UCWSPANC
001000     05  UCWSPANC-AREA   PIC X(5120).                             52131415
001010*****05  UCWSPANC-AREA   PIC X(1024).                             52131415
001100     05  UCWSPANC-DATA REDEFINES UCWSPANC-AREA.                   UCWSPANC
001200         10  UCWSPANC-REQUEST              PIC X.                 UCWSPANC
001201             88  UCWSPANC-REQ-GENERATE     VALUE 'G'.             UCWSPANC
001202             88  UCWSPANC-REQ-COMMIT       VALUE 'C'.             UCWSPANC
001203             88  UCWSPANC-REQ-BACKOUT      VALUE 'B'.             UCWSPANC
002200         10  UCWSPANC-REQ-ORIG-USERID      PIC X(08).             UCWSPANC
002300         10  UCWSPANC-REQ-ORIG-USER-DEPT   PIC X(06).             UCWSPANC
002700         10  UCWSPANC-REQ-TEXT-Q-NAME      PIC X(08).             UCWSPANC
002800         10  UCWSPANC-REQ-ADDR-Q-NAME      PIC X(08).             UCWSPANC
003000         10  UCWSPANC-REQ-APPLICATION-ID   PIC X(03).             UCWSPANC
003100         10  UCWSPANC-REQ-TRANSACTION-ID   PIC X(04).             UCWSPANC
003200         10  UCWSPANC-REQ-APPL-TIMESTAMP   PIC X(26).             UCWSPANC
003300         10  UCWSPANC-REQ-TRAN-DESC-APPL   PIC X(79).             UCWSPANC
003400         10  UCWSPANC-REQ-RESOURCE-TYPE    PIC X(08).             UCWSPANC
003500         10  UCWSPANC-REQ-RESOURCE-KEY     PIC X(44).             UCWSPANC
003600         10  UCWSPANC-REQ-NOTICE-ID        PIC X(26).             UCWSPANC
003700         10  UCWSPANC-RETURN-STATUS        PIC X.                 UCWSPANC
003800             88  UCWSPANC-RET-SUCCESS      VALUE 'S'.             UCWSPANC
003801             88  UCWSPANC-RET-NOTFOUND     VALUE 'N'.             UCWSPANC
003802             88  UCWSPANC-RET-PARM-ERROR   VALUE 'P'.             UCWSPANC
003803             88  UCWSPANC-RET-OTHERERR     VALUE 'E'.             UCWSPANC
003804         10  UCWSPANC-FEEDBACK-CODE        PIC X(12).             UCWSPANC
003805         10  UCWSPANC-RET-NOTICE-ID        PIC X(26).             UCWSPANC
003806         10  UCWSPANC-REQ-HTML-Q-NAME      PIC X(08).             52131415
003807         10  UCWSPANC-REQ-EMPLOYEE-ID      PIC X(09).             52131415
003808         10  UCWSPANC-REQ-EMPLOYEE-NAME    PIC X(26).             52131415
003809         10  UCWSPANC-REQ-EMP-LAST-NAME    PIC X(40).             52131415
003810         10  UCWSPANC-REQ-EMP-FIRST-NAME   PIC X(40).             52131415
003811         10  UCWSPANC-REQ-EMP-MID-NAME     PIC X(40).             52131415
003812         10  UCWSPANC-REQ-TITLE-CODE       PIC X(04).             52131415
003813         10  UCWSPANC-REQ-TITLE-DESC       PIC X(30).             52131415
003814         10  UCWSPANC-REQ-ACTION-CODES.                           52131415
003815             15  FILLER OCCURS 10.                                52131415
003816                 20  UCWSPANC-REQ-ACTION-CODE  PIC X(02).         52131415
003817                 20  UCWSPANC-REQ-ACTION-DESC  PIC X(30).         52131415
003818         10  UCWSPANC-REQ-DEPT-CODES.                             52131415
003819             15  FILLER OCCURS 100.                               52131415
003820                 20  UCWSPANC-REQ-DEPT-CODE    PIC X(06).         52131415
003821                 20  UCWSPANC-REQ-DEPT-DESC    PIC X(30).         52131415
003822         10  FILLER                        PIC X(743).            52131415
003830*********10  FILLER                        PIC X(764).            52131415
003900***************    END OF SOURCE - UCWSPANC    *******************UCWSPANC
