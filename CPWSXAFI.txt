000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXAFI                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   14030225
000200*  COPY MODULE:  CPWSXAFI                                    */   13250191
000300*  RELEASE # __225_______ SERVICE REQUEST NO(S)____1403______*/   14030225
000400*  NAME ___DBS_________   MODIFICATION DATE ____07/22/86_____*/   14030225
000500*  DESCRIPTION                                               */   14030225
000600*  ADDED NEW FIELD XAFI-AR-DEPT-CD TO SUPPORT MULTIPLE HOME  */   14030225
000700*  DEPARTMENTS.                                              */   14030225
000800**************************************************************/   13250191
000900*  COPY MODULE:  CPWSXAFI                                    */   13250191
001000*  RELEASE # ___0191___   SERVICE REQUEST NO(S) __1325______ */   13250191
001100*  NAME _____SLB_______   MODIFICATION DATE ____02/20/86_____*/   13250191
001200*  DESCRIPTION                                               */   13250191
001300*   ADDED UC LOCATION CODE 3 (XAFI-UC-LOC3) AND 19 BYTES OF  */   13250191
001400*   FILLER TO XAFI-ACCT-RECORD AND INCREASED SIZE OF XAFI-   */   13250191
001500*   NUMBER-OF-CALLS.                                         */   13250191
001600**************************************************************/   13250191
001700     SKIP3                                                        13250191
001800*01  ACCOUNT-FUND-INTERFACE          COPY CPXAFISF.               CPWSXAFI
001900******************************************************************CPWSXAFI
002000*    COPYID=CPWSXAFI                                              CPWSXAFI
002100*                                                                 CPWSXAFI
002200******************************************************************CPWSXAFI
002300*                                                                 CPWSXAFI
002400*                ACCOUNT-FUND PROFILE INTERFACE                   CPWSXAFI
002500*                                                                 CPWSXAFI
002600* THIS INTERFACE AREA IN CORE PROVIDES A COMMON AREA FOR COMMUNI- CPWSXAFI
002700*  CATION BETWEEN SYSTEM APPLICATION PROGRAMS AND THE CALLED      CPWSXAFI
002800*  MODULE PPINAFP TO ACCESS THE ACCOUNT FILE, THE FUND FILE       CPWSXAFI
002900*  AND THE ACCOUNT-FUND FILE.                                     CPWSXAFI
003000*                                                                 CPWSXAFI
003100*  THREE LEVELS OF CRITERIA MUST BE ESTABLISHED BY THE CALLING    CPWSXAFI
003200*  PROGRAM PRIOR TO THE CALL:                                     CPWSXAFI
003300*        1-  NOMINAL KEY INFORMATION BASED ON:                    CPWSXAFI
003400*                LOCATION#, ACCOUNT# AND/OR FUND#                 CPWSXAFI
003500*        2-  FILE SELECTION CRITERIA FOR:                         CPWSXAFI
003600*                ACCT-FILE, FUND-FILE, AND/OR ACCT-FUND-FILE      CPWSXAFI
003700*        3-  FILE ACCESS FUNCTION:                                CPWSXAFI
003800*                OPEN, READ OR CLOSE                              CPWSXAFI
003900*                                                                 CPWSXAFI
004000*  THESE CRITERIA ARE VALIDATED. INVALID CRITERIA RETURN A        CPWSXAFI
004100*  MESSAGE CODE TO THE CALLING PROGRAM, WHICH MUST PROCESS        CPWSXAFI
004200*  THAT MESSAGE AND PROVIDE APPROPRIATE ACTION.  THE INVALID      CPWSXAFI
004300*  CRITERIA CAUSE THE CALLED MODULE TO RETURN CONTROL TO          CPWSXAFI
004400*  THE CALLING PROGRAM.  VALID CRITERIA PERMIT THE SELECTED       CPWSXAFI
004500*  FILES TO BE ACCESSED, WHICH EXCEPT FOR THE INITIAL OPEN AND    CPWSXAFI
004600*  THE TERMINAL CLOSE, WILL BE RESTRICTED TO RANDOM READS.        CPWSXAFI
004700*                                                                 CPWSXAFI
004800*  THE CALLING PROGRAM MUST CALL PPINAFP AT LEAST THREE TIMES     CPWSXAFI
004900*  IF ANY OF THE FILES ARE TO BE ACCESSED.  THE FIRST CALL        CPWSXAFI
005000*  MUST PASS OPEN AS THE ACCESS FUNCTION; NO KEYS INTERFACE       CPWSXAFI
005100*  OR FILE SELECTION CRITERIA IS REQUIRED; AND THIS CALL          CPWSXAFI
005200*  SHOULD BE PERFORMED DURING NORMAL FILE OPENING PROCEDURES.     CPWSXAFI
005300*                                                                 CPWSXAFI
005400*  THE NEXT CALL(S) MUST PASS ALL THREE TYPES OF CRITERIA, SET    CPWSXAFI
005500*  UP IN THE INTERFACE MODULE BEFORE THE CALL IS EXECUTED.        CPWSXAFI
005600*  MINIMALLY, THE LOCATION MUST BE ESTABLISHED FOR THE NOMINAL    CPWSXAFI
005700*  KEY INFORMATION, AND READ IS THE ACCESS FUNCTION.              CPWSXAFI
005800*  THE CRITERIA FOR KEYS INTEFACE AND ACCESS FUNCTION HAVE        CPWSXAFI
005900*  ALREADY BEEN EXPLAINED.  TO SET UP FILE SELECTION CRITERIA,    CPWSXAFI
006000*  THE CALLING PROGRAM MUST FOR EXAMPLE MOVE XAFI-SEEK TO         CPWSXAFI
006100*  XAFI-SEEK-ACCT OR MOVE XAFI-NO-SEEK TO XAFI-SEEK-FUND          CPWSXAFI
006200*  TO THE FIELD(S) IN THE FILE SELECTION FIELD FOR THE FILES      CPWSXAFI
006300*  DESIRED.                                                       CPWSXAFI
006400*                                                                 CPWSXAFI
006500*  THE SELECTED READS ARE PERFORMED, SETTING UP RETURN STATUSES.  CPWSXAFI
006600*  IF THE RECORD IS SUCCESSFULLY READ, A CODE INDICATING A        CPWSXAFI
006700*  SUCCESSFUL GET IS MOVED TO THE RETURN STATUS FOR THAT RECORD   CPWSXAFI
006800*  AND THE REQUIRED INFORMATION IS MOVED FROM THE FILE TO THE     CPWSXAFI
006900*  INTERFACE ATTRIBUTES AREA.  IF THE READ PRODUCES AN            CPWSXAFI
007000*  INVALID KEY, A CODE INDICATING THE GET WAS NOT SUCCESSFUL IS   CPWSXAFI
007100*  MOVED TO THE RETURN STATUS.                                    CPWSXAFI
007200*                                                                 CPWSXAFI
007300*  THE KEY TO THE WHOLE MODULE REVOLVES AROUND A SHORTHAND        CPWSXAFI
007400*  CODE ESTABLISHED USING 88-LEVEL CONDITIONS FOR FILE SELECTION  CPWSXAFI
007500*  CRITERIA AND FOR RETURN STATUS.                                CPWSXAFI
007600*                                                                 CPWSXAFI
007700*  AFTER CONTROL IS RETURNED TO THE CALLING PROGRAM, THE PROGRAM  CPWSXAFI
007800*  MUST PERFORM TWO VALIDATION CHECKS:                            CPWSXAFI
007900*        1-  IF ERROR CODE IS GREATER THAN ZERO                   CPWSXAFI
008000*        2-  IF RETURN STATUS IS EQUAL TO FILE SELECTION CRITERIA CPWSXAFI
008100*                                                                 CPWSXAFI
008200*  AN ERROR CODE GREATER THAN ZERO INDICATES A PROGRAMMING OR     CPWSXAFI
008300*  AN OPERATIONAL ERROR WAS RETURNED, AND NO FURTHER              CPWSXAFI
008400*  CRITERIA VALIDATION IS PROCESSED IN THAT CALL.  THE VALUE      CPWSXAFI
008500*  OF THE ERROR CODE CAN BE USED TO GENERATE A PRINTED ERROR      CPWSXAFI
008600*  MESSAGE IN THE PROGRAM BY PERFORMING A SEPARATE CALL (PPMSSG). CPWSXAFI
008700*                                                                 CPWSXAFI
008800*  WHEN THE RETURN STATUS IS EQUAL TO THE FILE SELECTION CRITERIA CPWSXAFI
008900*  THE CALLING PROGRAM PROCEEDS WITH NORMAL PROCESSING.           CPWSXAFI
009000*                                                                 CPWSXAFI
009100*  THE FINAL CALL MUST PASS CLOSE AS THE ACCESS FUNCTION; NO      CPWSXAFI
009200*  KEYS INTERFACE OR FILE SELECTION CRITERIA IS REQUIRED; AND     CPWSXAFI
009300*  THIS CALL SHOULD BE PERFORMED DURING NORMAL FILE CLOSING       CPWSXAFI
009400*  PROCEDURES                                                     CPWSXAFI
009500*                                                                 CPWSXAFI
009600*                            *****                                CPWSXAFI
009700*                                                                 CPWSXAFI
009800*  IN THIS INTERFACE AREA, FIELDS PERTAINING TO                   CPWSXAFI
009900*  ACCOUNT FILE ARE PREFIXED BY ACCT OR AR (FOR ACCT-RECORD),     CPWSXAFI
010000*  FIELDS PERTAINING TO THE FUND FILE ARE PREFIXED BY             CPWSXAFI
010100*  FUND OR FR (FOR FUND-RECORD), AND FIELDS PERTAINING TO         CPWSXAFI
010200*  THE ACCOUNT-FUND FILE ARE PREFIXED BY ACCT-FUND OR             CPWSXAFI
010300*  AFR (FOR ACCT-FUND-RECORD).                                    CPWSXAFI
010400*  WHENEVER POSSIBLE, REFERENCES TO THE ACCOUNT FILE PRECEDE      CPWSXAFI
010500*  REFERENCES TO THE FUND FILE, AND REFERENCES TO THE             CPWSXAFI
010600*  FUND FILE IN TURN PRECEDE REFERENCES TO THE ACCOUNT-FUND FILE. CPWSXAFI
010700     EJECT                                                        CPWSXAFI
010800*01  ACCOUNT-FUND-INTERFACE      SYNC.                            13250191
010900*01  ACCOUNT-FUND-INTERFACE.                                      30930413
011000*    COPYID=CPWSXAFI                                              CPWSXAFI
011100     SKIP2                                                        CPWSXAFI
011200*  THE CALLING PROGRAM SUPPLIES THE INFORMATION BELOW TO ESTABLISHCPWSXAFI
011300*  THE NOMINAL KEY FOR THE THREE (POSSIBLE) ACCOUNT FUND PROFILE  CPWSXAFI
011400*  RECORDS DESCRIBED FURTHER BELOW.                               CPWSXAFI
011500*                                                                 CPWSXAFI
011600     05  XAFI-KEYS-INTERFACE.                                     CPWSXAFI
011700         07  XAFI-LOC                PIC X       VALUE SPACE.     CPWSXAFI
011800         07  XAFI-ACCT               PIC X(6)    VALUE SPACES.    CPWSXAFI
011900         07  XAFI-FUND               PIC X(5)    VALUE SPACES.    CPWSXAFI
012000     SKIP2                                                        CPWSXAFI
012100*  THE CALLING PROGRAM MUST ALSO SUPPLY THE BELOW INFORMATION,    CPWSXAFI
012200*  WHICH DETERMINES WHICH FILES ARE TO BE ACCESSED.               CPWSXAFI
012300*                                                                 CPWSXAFI
012400     05  XAFI-FILE-SELECTION-CRITERIA.                            CPWSXAFI
012500         08  XAFI-SEEK-ACCT          PIC X.                       CPWSXAFI
012600             88  XAFI-SEEK-ACCT-REC          VALUE 'Y'.           CPWSXAFI
012700         08  XAFI-SEEK-FUND          PIC X.                       CPWSXAFI
012800             88  XAFI-SEEK-FUND-REC          VALUE 'Y'.           CPWSXAFI
012900         08  XAFI-SEEK-ACCT-FUND     PIC X.                       CPWSXAFI
013000             88  XAFI-SEEK-ACCT-FUND-REC     VALUE 'Y'.           CPWSXAFI
013100*                                                                 CPWSXAFI
013200*  THE REDEFINES AREA BELOW PROVIDES A SHORTHAND MANNER           CPWSXAFI
013300*  TO DETERMINE SELECTION CRITERIA.                               CPWSXAFI
013400     05  FILLER REDEFINES XAFI-FILE-SELECTION-CRITERIA   PIC XXX. CPWSXAFI
013500         88  XAFI-GET-AR             VALUE 'YUU'.                 CPWSXAFI
013600         88  XAFI-GET-FR             VALUE 'UYU'.                 CPWSXAFI
013700         88  XAFI-GET-AFR            VALUE 'UUY'.                 CPWSXAFI
013800         88  XAFI-GET-AR-AFR         VALUE 'YUY'.                 CPWSXAFI
013900         88  XAFI-GET-FR-AFR         VALUE 'UYY'.                 CPWSXAFI
014000         88  XAFI-GET-AR-FR          VALUE 'YYU'.                 CPWSXAFI
014100         88  XAFI-GET-AR-FR-AFR      VALUE 'YYY'.                 CPWSXAFI
014200         88  XAFI-CLOSE-FILES        VALUE 'UUU'.                 CPWSXAFI
014300     SKIP2                                                        CPWSXAFI
014400*  THIS IS THE THIRD PARAMETER TO BE SUPPLIED FROM THE CALLING    CPWSXAFI
014500*  PROGRAM TO ESTABLISH THE ACCESS FUNCTION OF THE THREE          CPWSXAFI
014600*  INPUT FILES.                                                   CPWSXAFI
014700     05  XAFI-ACCESS-FUNCTION        PIC X(5).                    CPWSXAFI
014800         88  XAFI-FIRST-CALL         VALUE 'OPEN '.               CPWSXAFI
014900         88  XAFI-READ-FUNCT         VALUE 'READ '.               CPWSXAFI
015000         88  XAFI-LAST-CALL          VALUE 'CLOSE'.               CPWSXAFI
015100     EJECT                                                        CPWSXAFI
015200     05  XAFI-RESPONSE.                                           CPWSXAFI
015300*  THE BELOW LITERALS MAY BE USED FOR CONDITION CHECKS OPTIONALLY CPWSXAFI
015400*  WITH XAFI-FILE-SELECTION-CRITERIA, DESCRIBED PREVIOUSLY.       CPWSXAFI
015500         07  XAFI-SEEK               PIC X       VALUE 'Y'.       CPWSXAFI
015600         07  XAFI-NO-SEEK            PIC X       VALUE 'U'.       CPWSXAFI
015700*                                                                 CPWSXAFI
015800*  THE BELOW LITERALS MAY BE USED FOR CONDITION CHECKS            CPWSXAFI
015900*  OPTIONALLY WITH XAFI-RETURN-STATUS, DESCRIBED LATER.           CPWSXAFI
016000         07  XAFI-GET-SUCCESSFUL     PIC X       VALUE 'Y'.       CPWSXAFI
016100         07  XAFI-GET-NOT-SUCCESSFUL PIC X       VALUE 'N'.       CPWSXAFI
016200         07  XAFI-UNREQUESTED        PIC X       VALUE 'U'.       CPWSXAFI
016300     SKIP2                                                        CPWSXAFI
016400     SKIP2                                                        CPWSXAFI
016500*  THE DATA IN THE DESCRIPTIONS BELOW IS MOVED THERE IN PPINAFP   CPWSXAFI
016600*  FROM THE FILES ACCESSED, AND THEN IT  IS PASSED BACK TO THE    CPWSXAFI
016700*  CALLING PROGRAM BY THIS INTERFACE MODULE.                      CPWSXAFI
016800     SKIP2                                                        CPWSXAFI
016900     05  XAFI-ACCT-RECORD.                                        CPWSXAFI
017000         07  XAFI-AR-KEY.                                         CPWSXAFI
017100             09  FILLER              PIC X           VALUE SPACE. CPWSXAFI
017200             09  XAFI-AR-LOC         PIC X           VALUE SPACE. CPWSXAFI
017300             09  XAFI-AR-ACCT        PIC X(6)        VALUE SPACES.CPWSXAFI
017400             09  FILLER              PIC X           VALUE SPACE. CPWSXAFI
017500         07  XAFI-AR-ATTRIBUTES.                                  CPWSXAFI
017600             09  XAFI-AR-TITLE       PIC X(35)       VALUE SPACES.CPWSXAFI
017700             09  XAFI-AR-MAIL-GLPR70 PIC X(12)       VALUE SPACES.CPWSXAFI
017800             09  XAFI-AR-MAIL-PAF    PIC X(12)       VALUE SPACES.CPWSXAFI
017900             09  XAFI-AR-MAIL-PRELIST  PIC X(12)     VALUE SPACES.CPWSXAFI
018000             09  XAFI-AR-UC-LOC3     PIC X(01)       VALUE SPACE. 13250191
018100* CHANGE TO SUPPORT MULTIPLE HOME DEPARTMENTS
018200*****        09  FILLER              PIC X(19)       VALUE SPACES.14030225
018300             09  XAFI-AR-DEPT-CD     PIC X(6)        VALUE SPACES.14030225
018400             09  FILLER              PIC X(13)       VALUE SPACES.14030225
018500     SKIP2                                                        CPWSXAFI
018600     05  XAFI-FUND-RECORD.                                        CPWSXAFI
018700         07  XAFI-FR-KEY.                                         CPWSXAFI
018800             09  FILLER              PIC X           VALUE SPACE. CPWSXAFI
018900             09  XAFI-FR-LOC         PIC X           VALUE SPACE. CPWSXAFI
019000             09  XAFI-FR-FUND        PIC X(5)        VALUE SPACES.CPWSXAFI
019100             09  FILLER              PIC X           VALUE SPACE. CPWSXAFI
019200         07  XAFI-FR-ATTRIBUTES.                                  CPWSXAFI
019300             09  XAFI-FR-TITLE       PIC X(35)       VALUE SPACES.CPWSXAFI
019400     SKIP2                                                        CPWSXAFI
019500     05  XAFI-ACCT-FUND-RECORD.                                   CPWSXAFI
019600         07  XAFI-AFR-KEY.                                        CPWSXAFI
019700             09  FILLER              PIC X           VALUE SPACE. CPWSXAFI
019800             09  XAFI-AFR-LOC        PIC X           VALUE SPACE. CPWSXAFI
019900             09  XAFI-AFR-ACCT       PIC X(6)        VALUE SPACES.CPWSXAFI
020000             09  FILLER              PIC X           VALUE SPACE. CPWSXAFI
020100             09  XAFI-AFR-FUND       PIC X(5)        VALUE SPACES.CPWSXAFI
020200             09  FILLER              PIC X           VALUE SPACE. CPWSXAFI
020300         07  XAFI-AFR-ATTRIBUTES.                                 CPWSXAFI
020400             09  XAFI-AFR-MAIL-GLPR70  PIC X(12)     VALUE SPACES.CPWSXAFI
020500             09  XAFI-AFR-MAIL-CODE    PIC X(12)     VALUE SPACES.CPWSXAFI
020600         09  XAFI-AFR-SUB-BUDGET-ACCOUNTS  PIC X(75) VALUE SPACES.CPWSXAFI
020700         09  FILLER REDEFINES XAFI-AFR-SUB-BUDGET-ACCOUNTS.       CPWSXAFI
020800               11  FILLER    OCCURS 15 TIMES.                     CPWSXAFI
020900                 13  FILLER      PIC X.                           CPWSXAFI
021000                 13  XAFI-AFR-SUB-ACCT   PIC X.                   CPWSXAFI
021100               11  XAFI-AFR-COST-CENTERS PIC X(3) OCCURS 15.      CPWSXAFI
021200     SKIP3                                                        CPWSXAFI
021300*  THIS MODULE SUPPLIES THIS RETURN STATUS INFORMATION TO THE     CPWSXAFI
021400*  ATTEMPTED.  IMMEDIATELY FOLLOWING THE CALL STATEMENT IN THE    CPWSXAFI
021500*  CALLING PROGRAM BASED ON THE SUCCESS OF THE OPERATION          CPWSXAFI
021600*  CALLING PROGRAM, A TEST SHOULD BE PERFORMED TO VERIFY THAT     CPWSXAFI
021700*  THE RETURN STATUS IS EQUAL TO THE FILE SELECTION CRITERIA.     CPWSXAFI
021800*  IF NOT, A SEPARATE ROUTINE MUST BE PERFORMED TO DETERMINE      CPWSXAFI
021900*  THE SEVERITY RESULTING FROM THIS DISPARITY AND THE ACTION      CPWSXAFI
022000*  TO BE TAKEN.                                                   CPWSXAFI
022100*                                                                 CPWSXAFI
022200*  THE STATUS OF THE REQUESTED OPERATION IS RETURNED BELOW:       CPWSXAFI
022300     05  XAFI-RETURN-STATUS.                                      CPWSXAFI
022400         07  XAFI-AR-RETURN-STATUS   PIC X.                       CPWSXAFI
022500             88  XAFI-AR-FOUND           VALUE 'Y'.               CPWSXAFI
022600             88  XAFI-AR-NOT-FOUND       VALUE 'N'.               CPWSXAFI
022700             88  XAFI-AR-UNREQUESTED     VALUE 'U'.               CPWSXAFI
022800         07  XAFI-FR-RETURN-STATUS   PIC X.                       CPWSXAFI
022900             88  XAFI-FR-FOUND           VALUE 'Y'.               CPWSXAFI
023000             88  XAFI-FR-NOT-FOUND       VALUE 'N'.               CPWSXAFI
023100             88  XAFI-FR-UNREQUESTED     VALUE 'U'.               CPWSXAFI
023200         07  XAFI-AFR-RETURN-STATUS  PIC X.                       CPWSXAFI
023300             88  XAFI-AFR-FOUND          VALUE 'Y'.               CPWSXAFI
023400             88  XAFI-AFR-NOT-FOUND      VALUE 'N'.               CPWSXAFI
023500             88  XAFI-AFR-UNREQUESTED    VALUE 'U'.               CPWSXAFI
023600*                                                                 CPWSXAFI
023700*  THE REDEFINES AREA BELOW PROVIDES A SHORTHAND MANNER           CPWSXAFI
023800*  TO ESTABLISH RETURN INFORMATION.                               CPWSXAFI
023900*                                                                 CPWSXAFI
024000     05  FILLER REDEFINES XAFI-RETURN-STATUS     PIC XXX.         CPWSXAFI
024100*    THE VALUES BELOW REPRESENT VALID SEEK-FIND COMBINATIONS      CPWSXAFI
024200         88  XAFI-RETURN-STATUS-OK                   VALUE        CPWSXAFI
024300             'YUU' 'UYU' 'UUY' 'YUY' 'UYY' 'YYU' 'YYY'.           CPWSXAFI
024400         88  XAFI-AR-OK                  VALUE 'YUU'.             CPWSXAFI
024500         88  XAFI-FR-OK                  VALUE 'UYU'.             CPWSXAFI
024600         88  XAFI-AFR-OK                 VALUE 'UUY'.             CPWSXAFI
024700         88  XAFI-AR-AFR-OK              VALUE 'YUY'.             CPWSXAFI
024800         88  XAFI-FR-AFR-OK              VALUE 'UYY'.             CPWSXAFI
024900         88  XAFI-AR-FR-OK               VALUE 'YYU'.             CPWSXAFI
025000         88  XAFI-AR-FR-AFR-OK           VALUE 'YYY'.             CPWSXAFI
025100***  THE VALUES BELOW REPRESENT COMBINATIONS WHERE THERE WAS:     CPWSXAFI
025200*    ONE FILE SOUGHT BUT NONE RETRIEVED:                          CPWSXAFI
025300         88  XAFI-NO-AR                  VALUE 'NUU'.             CPWSXAFI
025400         88  XAFI-NO-FR                  VALUE 'UNU'.             CPWSXAFI
025500         88  XAFI-NO-AFR                 VALUE 'UUN'.             CPWSXAFI
025600*    TWO FILES SOUGHT, ONLY ONE FILE RETRIEVED:                   CPWSXAFI
025700         88  XAFI-AR-NO-FR               VALUE 'YNU'.             CPWSXAFI
025800         88  XAFI-AR-NO-AFR              VALUE 'YUN'.             CPWSXAFI
025900         88  XAFI-FR-NO-AR               VALUE 'NYU'.             CPWSXAFI
026000         88  XAFI-FR-NO-AFR              VALUE 'UYN'.             CPWSXAFI
026100         88  XAFI-AFR-NO-AR              VALUE 'NUY'.             CPWSXAFI
026200         88  XAFI-AFR-NO-FR              VALUE 'UNY'.             CPWSXAFI
026300*    TWO FILES SOUGHT, NEITHER FILE FOUND:                        CPWSXAFI
026400         88  XAFI-NO-FR-NO-AFR           VALUE 'UNN'.             CPWSXAFI
026500         88  XAFI-NO-AR-NO-AFR           VALUE 'NUN'.             CPWSXAFI
026600         88  XAFI-NO-AR-NO-FR            VALUE 'NNU'.             CPWSXAFI
026700*    THREE FILES SOUGHT, TWO FILES RETRIEVED:                     CPWSXAFI
026800         88  XAFI-AR-FR-NO-AFR           VALUE 'YYN'.             CPWSXAFI
026900         88  XAFI-AR-AFR-NO-FR           VALUE 'YNY'.             CPWSXAFI
027000         88  XAFI-FR-AFR-NO-AR           VALUE 'NYY'.             CPWSXAFI
027100*    THREE FILES SOUGHT, ONLY ONE FILE RETURNED:                  CPWSXAFI
027200         88  XAFI-AR-NO-FR-NO-AFR        VALUE 'YNN'.             CPWSXAFI
027300         88  XAFI-FR-NO-AR-NO-AFR        VALUE 'NYN'.             CPWSXAFI
027400         88  XAFI-AFR-NO-AR-NO-FR        VALUE 'NNY'.             CPWSXAFI
027500*    THREE FILES SOUGHT, NONE RETURNED:                           CPWSXAFI
027600         88  XAFI-NO-AR-NO-FR-NO-AFR     VALUE 'NNN'.             CPWSXAFI
027700*                                                                 CPWSXAFI
027800*                                                                 CPWSXAFI
027900*  THE FOLLOWING PREVIOUSLY DEFINED LITERALS MAY BE USED FOR      CPWSXAFI
028000*  CONDITION CHECKS WITH XAFI-RETURN-STATUS:                      CPWSXAFI
028100*        88  XAFI-GET-SUCCESSFUL     VALUE 'Y'.                   CPWSXAFI
028200*        88  XAFI-GET-NOT-SUCCESSFUL VALUE 'N'.                   CPWSXAFI
028300*        88  XAFI-UNREQUESTED        VALUE 'U'.                   CPWSXAFI
028400     SKIP2                                                        CPWSXAFI
028500     SKIP2                                                        CPWSXAFI
028600*  THE FOLLOWING FIELDS PASS MISCELLANEOUS INFORMATION BACK       CPWSXAFI
028700*  AND FORTH BETWEEN CALLING PROGRAM AND CALLED MODULE            CPWSXAFI
028800*    05  XAFI-NUMBER-OF-CALLS        PIC S9(5)  COMP  VALUE ZERO. 13250191
028900     05  XAFI-NUMBER-OF-CALLS        PIC S9(7) COMP-3 VALUE ZERO. 13250191
029000     05  XAFI-ERROR-CODE             PIC 9(5)    VALUE ZERO.      CPWSXAFI
