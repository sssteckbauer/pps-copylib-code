000100**************************************************************/   50461237
000200*  COPYMEMBER: CPWSTXMO                                      */   50461237
000300*  RELEASE: ___1237______ SERVICE REQUEST(S): ____15046____  */   50461237
000400*  NAME:_______QUAN______ CREATION DATE:      ___03/02/99__  */   50461237
000500*  DESCRIPTION:                                              */   50461237
000600*  - RECORD LAYOUT OF TX  MONTHLY CHANGE FILE.               */   50461237
000700*    ANNUAL FILE USES THE SAME RECORD STRUCTURE.             */   50461237
000800**************************************************************/   50461237
001000*    COPYID=CPWSTXMO                                              CPWSTXMO
001100     SKIP1                                                        CPWSTXMO
001200*                                                                *CPWSTXMO
001300*01  XTXA-MONTH-ANNUAL-RECORD.                                    CPWSTXMO
001400******************************************************************CPWSTXMO
001500*                  T X    F I L E                                 CPWSTXMO
001600******************************************************************CPWSTXMO
031144*----> NOTE: TESTING OF THE PROPER PLACEMENT OF THE TABS SHOULD BECPWSTXMO
031144*---->       MADE BY EXPORTING THE OUTPUT FILE TO THE PC, OPEN THECPWSTXMO
031144*---->       FILE IN A SPREADSHEET (I.E., EXCEL).                 CPWSTXMO
031144*---->                                                            CPWSTXMO
031144*---->       WHEN ADDING NEW FIELDS TO THIS COPYMEMBER, MAKE SURE CPWSTXMO
031145*----> THAT THE EXISTING FIELDS XTXA-:TYPE:-END-REC-FLAG (DETAIL) CPWSTXMO
011010*---->                          XTXA-:TYPE:-TXTRL-END-REC (HEADER)CPWSTXMO
011010*---->                          XTXA-:TYPE:-TRLR-END-REC (TRAILER)CPWSTXMO
011010*----> ARE ALWAYS THE VERY LAST FIELDS OF THE DETAIL, HEADER, AND CPWSTXMO
011010*----> TRAILER RECORDS TO PREVENT THE FTP PROCESS FROM TRUNCATING CPWSTXMO
011010*----> BLANKS TOWARD THE END OF THE RECORDS.                      CPWSTXMO
011146*----*                                                            CPWSTXMO
001700      03  XTXA-:TYPE:.                                            CPWSTXMO
001800          05  XTXA-:TYPE:-UPDATE-CODE       PIC X(01).            CPWSTXMO
001900              88 XTXA-:TYPE:-ADD                VALUE 'A'.        CPWSTXMO
002000              88 XTXA-:TYPE:-CHANGE             VALUE 'C'.        CPWSTXMO
002100              88 XTXA-:TYPE:-DELETE             VALUE 'D'.        CPWSTXMO
002210          05  XTXA-:TYPE:-FILL1             PIC X(01) VALUE X'05'.CPWSTXMO
002300          05  XTXA-:TYPE:-KEY.                                    CPWSTXMO
002400              07  XTXA-:TYPE:-LOC-CODE          PIC X(02).        CPWSTXMO
002510              07  XTXA-:TYPE:-FILL2         PIC X(01) VALUE X'05'.CPWSTXMO
002600              07  XTXA-:TYPE:-EMP-NAME          PIC X(26).        CPWSTXMO
002710              07  XTXA-:TYPE:-FILL3         PIC X(01) VALUE X'05'.CPWSTXMO
002800              07  XTXA-:TYPE:-NAMESUFFIX        PIC X(04).        CPWSTXMO
002910              07  XTXA-:TYPE:-FILL4         PIC X(01) VALUE X'05'.CPWSTXMO
003000              07  XTXA-:TYPE:-ID-NO             PIC X(09).        CPWSTXMO
003100          05  XTXA-:TYPE:-DATA.                                   CPWSTXMO
003200              07  XTXA-:TYPE:-FILL5         PIC X(01) VALUE X'05'.CPWSTXMO
003300              07  XTXA-:TYPE:-ACTION            PIC X(13).        CPWSTXMO
003410              07  XTXA-:TYPE:-FILL6         PIC X(01) VALUE X'05'.CPWSTXMO
003500              07  XTXA-:TYPE:-EMP-NAME-DATA     PIC X(26).        CPWSTXMO
003610              07  XTXA-:TYPE:-FILL7         PIC X(01) VALUE X'05'.CPWSTXMO
003700              07  XTXA-:TYPE:-NAMESUFFIX-DATA   PIC X(04).        CPWSTXMO
003810              07  XTXA-:TYPE:-FILL8         PIC X(01) VALUE X'05'.CPWSTXMO
003900              07  XTXA-:TYPE:-ADDRESS-LINE1         PIC X(30).    CPWSTXMO
004010              07  XTXA-:TYPE:-FILL9         PIC X(01) VALUE X'05'.CPWSTXMO
004100              07  XTXA-:TYPE:-ADDRESS-LINE2         PIC X(30).    CPWSTXMO
004210              07  XTXA-:TYPE:-FILL10        PIC X(01) VALUE X'05'.CPWSTXMO
004300              07  XTXA-:TYPE:-ADDRESS-CITY         PIC X(21).     CPWSTXMO
004410              07  XTXA-:TYPE:-FILL11        PIC X(01) VALUE X'05'.CPWSTXMO
004500              07  XTXA-:TYPE:-ADDRESS-STATE         PIC X(02).    CPWSTXMO
004610              07  XTXA-:TYPE:-FILL12        PIC X(01) VALUE X'05'.CPWSTXMO
004700              07  XTXA-:TYPE:-ADDRESS-ZIP           PIC X(09).    CPWSTXMO
004810              07  XTXA-:TYPE:-FILL13        PIC X(01) VALUE X'05'.CPWSTXMO
004900              07  XTXA-:TYPE:-HIRE-DATE             PIC X(10).    CPWSTXMO
005010              07  XTXA-:TYPE:-FILL14        PIC X(01) VALUE X'05'.CPWSTXMO
005100              07  XTXA-:TYPE:-DEPT-ADDRESS          PIC X(30).    CPWSTXMO
005210              07  XTXA-:TYPE:-FILL15        PIC X(01) VALUE X'05'.CPWSTXMO
005300              07  XTXA-:TYPE:-DEPT-NAME             PIC X(30).    CPWSTXMO
005410              07  XTXA-:TYPE:-FILL16        PIC X(01) VALUE X'05'.CPWSTXMO
005500              07  XTXA-:TYPE:-LOA-BEGIN-DATE        PIC X(10).    CPWSTXMO
005610              07  XTXA-:TYPE:-FILL17        PIC X(01) VALUE X'05'.CPWSTXMO
005700              07  XTXA-:TYPE:-LOA-RETURN-DATE       PIC X(10).    CPWSTXMO
005810              07  XTXA-:TYPE:-FILL18        PIC X(01) VALUE X'05'.CPWSTXMO
005900              07  XTXA-:TYPE:-LOA-TYPE-CODE         PIC X(02).    CPWSTXMO
006010              07  XTXA-:TYPE:-FILL19        PIC X(01) VALUE X'05'.CPWSTXMO
006100              07  XTXA-:TYPE:-LOA-TYPE-DESC         PIC X(30).    CPWSTXMO
006210              07  XTXA-:TYPE:-FILL20        PIC X(01) VALUE X'05'.CPWSTXMO
006300              07  XTXA-:TYPE:-SEPARATE-DATE         PIC X(10).    CPWSTXMO
006410              07  XTXA-:TYPE:-FILL21        PIC X(01) VALUE X'05'.CPWSTXMO
006500              07  XTXA-:TYPE:-SEPARATE-DESC         PIC X(30).    CPWSTXMO
006610              07  XTXA-:TYPE:-FILL22        PIC X(01) VALUE X'05'.CPWSTXMO
007200*                                                                 CPWSTXMO
007300          05  XTXA-:TYPE:-NO-TITLES                 PIC 9(02).    CPWSTXMO
007400          05  XTXA-:TYPE:-NO-TITLES-X     REDEFINES               CPWSTXMO
007500              XTXA-:TYPE:-NO-TITLES                 PIC X(02).    CPWSTXMO
007600*                                                                 CPWSTXMO
007700          05  XTXA-:TYPE:-APPT-ARRAY.                             CPWSTXMO
007800              07  XTXA-:TYPE:-APPT-ENTRIES  OCCURS 9 TIMES.       CPWSTXMO
007900                  09  XTXA-:TYPE:-APPT-FILLER-1     PIC X(01).    CPWSTXMO
008000                  09  XTXA-:TYPE:-TITLE-CODE        PIC X(04).    CPWSTXMO
008100                  09  XTXA-:TYPE:-APPT-FILLER-2     PIC X(01).    CPWSTXMO
008200                  09  XTXA-:TYPE:-ABRV-TITLE-NAME   PIC X(30).    CPWSTXMO
008300                  09  XTXA-:TYPE:-APPT-FILLER-3     PIC X(01).    CPWSTXMO
008400                  09  XTXA-:TYPE:-PAY-RATE      PIC -9(06).99.    CPWSTXMO
008500                  09  XTXA-:TYPE:-PAY-RATE-X  REDEFINES           CPWSTXMO
008600                      XTXA-:TYPE:-PAY-RATE           PIC X(10).   CPWSTXMO
008700                  09  XTXA-:TYPE:-APPT-FILLER-4      PIC X(01).   CPWSTXMO
008800                  09  XTXA-:TYPE:-PRCNT-FULLTIME     PIC -9.99.   CPWSTXMO
008900                  09  XTXA-:TYPE:-PRCNT-FULLTIME-X REDEFINES      CPWSTXMO
009000                      XTXA-:TYPE:-PRCNT-FULLTIME     PIC X(05).   CPWSTXMO
008300                  09  XTXA-:TYPE:-APPT-FILLER-5     PIC X(01).    CPWSTXMO
008200                  09  XTXA-:TYPE:-APPT-TYPE         PIC X(01).    CPWSTXMO
009010                                                                  CPWSTXMO
009020          05  XTXA-:TYPE:-END-REC-FLAG      PIC X(01).            CPWSTXMO
009100                                                                  CPWSTXMO
009200      03  XTXA-:TYPE:-CNTRL-DATA     REDEFINES                    CPWSTXMO
009300          XTXA-:TYPE:.                                            CPWSTXMO
009400          05  FILLER                                PIC X(01).    CPWSTXMO
009500          05  XTXA-:TYPE:-CNTRL-FILLER-1            PIC X(01).    CPWSTXMO
009600          05  XTXA-:TYPE:-CNTRL-KEY.                              CPWSTXMO
009700              07  XTXA-:TYPE:-CNTRL-LOC-CODE    PIC X(02).        CPWSTXMO
009500              07  XTXA-:TYPE:-CNTRL-FILLER-2    PIC X(01).        CPWSTXMO
009800              07  XTXA-:TYPE:-CNTRL-EMP-NAME    PIC X(26).        CPWSTXMO
009800              07  XTXA-:TYPE:-CNTRL-FILLER-3    PIC X(01).        CPWSTXMO
009800              07  XTXA-:TYPE:-CNTRL-NAMESUFFIX  PIC X(04).        CPWSTXMO
009800              07  XTXA-:TYPE:-CNTRL-FILLER-4    PIC X(01).        CPWSTXMO
009800              07  XTXA-:TYPE:-CNTRL-EMP-ID      PIC X(09).        CPWSTXMO
010200          05  XTXA-:TYPE:-CNTRL-FILLER-5        PIC X(05).        CPWSTXMO
010300          05  XTXA-:TYPE:-CNTRL-FILLER-6        PIC X(01).        CPWSTXMO
010400          05  XTXA-:TYPE:-CNTRL-TRANS-DATE          PIC 9(08).    CPWSTXMO
010500          05  XTXA-:TYPE:-CNTRL-TRANS-DATE-X REDEFINES            CPWSTXMO
010600              XTXA-:TYPE:-CNTRL-TRANS-DATE.                       CPWSTXMO
010700              07  XTXA-:TYPE:-CNTRL-TRANS-MM         PIC 9(02).   CPWSTXMO
010800              07  XTXA-:TYPE:-CNTRL-TRANS-DD         PIC 9(02).   CPWSTXMO
010900              07  XTXA-:TYPE:-CNTRL-TRANS-CCYY       PIC 9(04).   CPWSTXMO
011000          05  FILLER                                 PIC X(798).  CPWSTXMO
011010          05  XTXA-:TYPE:-CNTRL-END-REC         PIC X(01).        CPWSTXMO
011100                                                                  CPWSTXMO
011200      03  XTXA-:TYPE:-TRAILER-DATA  REDEFINES                     CPWSTXMO
011300          XTXA-:TYPE:.                                            CPWSTXMO
011400          05  FILLER                          PIC X(01).          CPWSTXMO
011500          05  XTXA-:TYPE:-TRLR-FILLER-1       PIC X(01).          CPWSTXMO
011600          05  XTXA-:TYPE:-TRLR-KEY.                               CPWSTXMO
011700              07  XTXA-:TYPE:-TRLR-LOC-CODE       PIC X(02).      CPWSTXMO
011500              07  XTXA-:TYPE:-TRLR-FILLER-2       PIC X(01).      CPWSTXMO
011700              07  XTXA-:TYPE:-TRLR-EMP-NAME       PIC X(26).      CPWSTXMO
011800              07  XTXA-:TYPE:-TRLR-FILLER-3       PIC X(01).      CPWSTXMO
011700              07  XTXA-:TYPE:-TRLR-NAMESUFFIX     PIC X(04).      CPWSTXMO
011800              07  XTXA-:TYPE:-TRLR-FILLER-4       PIC X(01).      CPWSTXMO
011700              07  XTXA-:TYPE:-TRLR-EMP-ID         PIC X(09).      CPWSTXMO
012200          05  XTXA-:TYPE:-TRLR-FILLER-5       PIC X(01).          CPWSTXMO
012200          05  XTXA-:TYPE:-TRLR-FILLER-6       PIC X(04).          CPWSTXMO
012300          05  XTXA-:TYPE:-TRLR-FILLER-7       PIC X(01).          CPWSTXMO
012400          05  XTXA-:TYPE:-TRLR-REC-CT         PIC 9(09).          CPWSTXMO
012500          05  FILLER                          PIC X(797).         CPWSTXMO
012510          05  XTXA-:TYPE:-TRLR-END-REC        PIC X(01).          CPWSTXMO
012600*                                                                 CPWSTXMO
