000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPOTVTRN                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    REWRITE FOR CONSOLIDATION OF MODIFICATIONS.             *|*36410717
000900*|*    ENLARGE THE NEW-EFF-DATE FOR ISO-DATE                   *|*36410717
001000*|*    CHANGE NEW-COUNTER FROM COMP-3 TO COMP                  *|*36410717
001100*|*    SWAP NEW-DATA AND NEW-CON-EDITS                         *|*36410717
001200*|*    RENAME NEW-CON-EDITS TO NEW-TRIGGER-DATA                *|*36410717
001300*|*    ENLARGE NEW-CON-EDITS TO A VARIABLE AREA FROM 0 TO 60   *|*36410717
001400*|*    ADD NEW-NO-TRIGGERS PIC 9(04) COMP.   THIS DETERNIMES   *|*36410717
001500*|*    THE SIZE OF THE NEW-TRIGGER-DATA.                       *|*36410717
001600*|*    ADD NEW-GTN-BAL-TYPE.   THIS WILL BE USED TO DETERMINE  *|*36410717
001700*|*    THE TYPE OF BALANCE BEING PROCESSED.                    *|*36410717
001800*|**************************************************************|*36410717
001900*----------------------------------------------------------------*36410717
002000*    COPYID=CPOTVTRN                                              CPOTVTRN
002100*01  NEW-RECORD.                                                  CPOTVTRN
002200     03 NEW-FIXED.                                                CPOTVTRN
002300     05 NEW-KEY19.                                                CPOTVTRN
002400        10 NEW-KEY15.                                             CPOTVTRN
002500           15 NEW-SS-NUM            PICTURE X(9).                 CPOTVTRN
002600           15 NEW-EFF-DATE          PICTURE X(10).                CPOTVTRN
002700        10 NEW-FIELD-NUM.                                         CPOTVTRN
002800           15 NEW-SEGMENT           PICTURE 99.                   CPOTVTRN
002900           15 NEW-SER               PICTURE 99.                   CPOTVTRN
003000        10 NEW-DED-ELMT REDEFINES   NEW-FIELD-NUM.                CPOTVTRN
003100           15 NEW-ELMT-POS1         PICTURE X.                    CPOTVTRN
003200           15 NEW-ELMT-POS234       PICTURE 999.                  CPOTVTRN
003300     05 NEW-COUNTER                 PICTURE S9(7) COMP.           CPOTVTRN
003400     05 NEW-SET-CNTRL-NUM.                                        CPOTVTRN
003500        10 FILLER                   PICTURE X(01).                CPOTVTRN
003600        10 NEW-ACTION-KEY           PICTURE X(02).                CPOTVTRN
003700        10 FILLER                   PICTURE X(03).                CPOTVTRN
003800     05 NEW-ERROR-CODE              PICTURE 9(5).                 CPOTVTRN
003900     05 NEW-SEVERITY                PICTURE X.                    CPOTVTRN
004000     05 NEW-BATCH-NO                PICTURE 9(3).                 CPOTVTRN
004100     05 NEW-ORIGIN                   PICTURE XXX.                 CPOTVTRN
004200     05 NEW-START-POS               PICTURE 999.                  CPOTVTRN
004300     05 NEW-LENGTH                  PICTURE 99.                   CPOTVTRN
004400     05  NEW-NO-TRIGGERS            PICTURE 9(04) COMP.           CPOTVTRN
004500     05  NEW-GTN-BAL-TYPE           PICTURE X(01).                CPOTVTRN
004600     05 NEW-DATA-FIELD.                                           CPOTVTRN
004700        10  NEW-DATA                PICTURE X  OCCURS 30 TIMES.   CPOTVTRN
004800     03 NEW-VARIABLE.                                             CPOTVTRN
004900     05  NEW-TRIGGER-DATA.                                        CPOTVTRN
005000         10  NEW-TRIGGERS           OCCURS 0 TO 60                CPOTVTRN
005100                                    DEPENDING ON NEW-NO-TRIGGERS. CPOTVTRN
005200             15  NEW-TRIGGER-TYPE   PIC X(01).                    CPOTVTRN
005300             15  NEW-TRIGGER-NUM    PIC 9(03).                    CPOTVTRN
005400*                                                                 CPOTVTRN
005500******************************************************************CPOTVTRN
