000100**************************************************************/   36480863
000200*  COPYMEMBER: UCWSPANT                                      */   36480863
000300*  RELEASE: ___0863______ SERVICE REQUEST(S): _____3648____  */   36480863
000400*  NAME:_______AAC_______ MODIFICATION DATE:  __01/27/94____ */   36480863
000500*  DESCRIPTION:                                              */   36480863
000600*     PAN SUBSYSTEM: ADDRESS RECORD LAYOUT                   */   36480863
000700*                                                            */   36480863
000800**************************************************************/   36480863
000900*01  UCWSPANT.                                                    UCWSPANT
001200     05  UCWSPANT-DATA.                                           UCWSPANT
001201         10 UCWSPANT-EVENT-CODE            PIC X(04).             UCWSPANT
001202         10 UCWSPANT-SUB-EVENT-CODE        PIC X(04).             UCWSPANT
001210         10 UCWSPANT-TEXT-LINE             PIC X(79).             UCWSPANT
004300***************    END OF SOURCE - UCWSPANT    *******************UCWSPANT
