000100******************************************************************40280572
000200*  COPYMEMBER: CPLNKSP2                                          *40280572
000300*  RELEASE: ____0572____  SERVICE REQUEST(S): ____4028____       *40280572
000400*  NAME:__PXP___________  CREATION DATE:      ___05/29/91_       *40280572
000500*  DESCRIPTION:                                                  *40280572
000600*    LINKAGE FOR PPSPPUTL SUBROUTINE CREATED FOR DB2 EDB         *40280572
000700*    CONVERSION.                                                 *40280572
000800*                                                                *40280572
000900******************************************************************CPLNKSP2
001000*    COPYID=CPLNKSPP                                              CPLNKSP2
001100*01  PPSPPUTL-INTERFACE.                                          CPLNKSP2
001200*                                                                *CPLNKSP2
001300******************************************************************CPLNKSP2
001400*    P P S P P U T L   I N T E R F A C E                         *CPLNKSP2
001500******************************************************************CPLNKSP2
001600*                                                                *CPLNKSP2
001700     05  PPSPPUTL-ERROR-SW       PICTURE X(1).                    CPLNKSP2
001800     88  PPSPPUTL-ERROR VALUE 'Y'.                                CPLNKSP2
001900     05  PPSPPUTL-EMPLOYEE-ID   PICTURE X(9).                     CPLNKSP2
002000     05  PPSPPUTL-ROW-COUNT     PICTURE 9(3).                     CPLNKSP2
