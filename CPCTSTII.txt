000000**************************************************************/   30871460
000001*  COPYMEMBER: CPCTSTII                                      */   30871460
000002*  RELEASE: ___1460______ SERVICE REQUEST(S): _____3087____  */   30871460
000003*  NAME:_____SRS_________ CREATION DATE:      ___01/22/03__  */   30871460
000004*  DESCRIPTION:                                              */   30871460
000005*  - NEW COPY MEMBER FOR STATE TAX TABLE INPUT (PPPSTI)      */   30871460
000007**************************************************************/   30871460
000008*    COPYID=CPCTSTII                                              CPCTSTII
000009*01  STI-TABLE-INPUT.                                             CPCTSTII
000100     05  STII-KEY.                                                CPCTSTII
000110         10 STII-PERIOD-TYPE             PIC X.                   CPCTSTII
000230     05  STII-AMT.                                                CPCTSTII
000240         10 STII-AMT-N                   PIC S9(5).               CPCTSTII
