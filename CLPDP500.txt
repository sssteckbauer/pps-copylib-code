000010*==========================================================%      UCSD0006
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0006
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD0006
000040*=                                                        =%      UCSD0006
000050*==========================================================%      UCSD0006
000060*==========================================================%      UCSD0006
000070*=                                                        =%      UCSD0006
000080*=    COPY MEMBER: CLPDP500                               =%      UCSD0006
000090*=    CHANGE #0006          PROJ. REQUEST: CONTROL REPORT =%      UCSD0006
000091*=    NAME: MARI MCGEE      MODIFICATION DATE: 04/26/91   =%      UCSD0006
000092*=                                                        =%      UCSD0006
000093*=    DESCRIPTION                                         =%      UCSD0006
000094*=     ADD SECTION TO PRINT-CONTROL-REPORT.               =%      UCSD0006
000095*=                                                        =%      UCSD0006
000096*==========================================================%      UCSD0006
000100*                                                                 CLPDP500
000200*       CONTROL REPORT PPP500CR                                   CLPDP500
000300*                                                                 CLPDP500
000400*                                                                 CLPDP500
000410*PRINT-CONTROL-REPORT.                                            UCSD0006
000420 PRINT-CONTROL-REPORT SECTION.                                    UCSD0006
000600                                                                  CLPDP500
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP500
000800                                                                  CLPDP500
000900     MOVE 'PPP500CR'             TO CR-HL1-RPT.                   CLPDP500
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP500
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP500
001200                                                                  CLPDP500
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP500
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP500
001500                                                                  CLPDP500
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP500
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP500
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP500
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP500
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP500
002100                                                                  CLPDP500
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP500
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP500
002400                                                                  CLPDP500
002500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP500
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP500
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP500
002800                                                                  CLPDP500
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP500
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP500
003100                                                                  CLPDP500
003110     ACCEPT TIME-WORK-AREA FROM TIME.                             CLPDP500
003200*    MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP500
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP500
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP500
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP500
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP500
003700                                                                  CLPDP500
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP500
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP500
004000                                                                  CLPDP500
004100     MOVE 'FNLPAR.PREV'          TO CR-DL9-FILE.                  CLPDP500
004200     MOVE SO-READ                TO CR-DL9-ACTION.                CLPDP500
004300     MOVE 200-PAR-RECORDS-READ   TO CR-DL9-VALUE.                 CLPDP500
004400     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP500
004500     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP500
004600                                                                  CLPDP500
004700     MOVE 'CONBEN '              TO CR-DL9-FILE.                  CLPDP500
004800     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP500
004900     MOVE 200-CONBEN-RECORDS-WRITTEN  TO CR-DL9-VALUE.            CLPDP500
005000     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP500
005100     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP500
005200                                                                  CLPDP500
005300     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP500
005400     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP500
005500                                                                  CLPDP500
005600     CLOSE CONTROLREPORT.                                         CLPDP500
