000100**************************************************************/   05490748
000200*  COPYMEMBER: CPWSELOF                                      */   05490748
000300*  RELEASE: ____0748____  SERVICE REQUEST(S): ____0549____   */   05490748
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __03/05/93__   */   05490748
000500*  DESCRIPTION:                                              */   05490748
000600*   ADDED LAYOFF-NOTICE-DATE TO THE LOF OCCURRENCE KEY.      */   05490748
000700**************************************************************/   05490748
000100**************************************************************/   36290694
000200*  COPYMEMBER: CPWSELOF                                      */   36290694
000300*  RELEASE: ____0694____  SERVICE REQUEST(S): ____3629____   */   36290694
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __08/07/92__   */   36290694
000500*  DESCRIPTION:                                              */   36290694
000700*    -LAYOFF TABLE ARRAY FOR LOF TABLE OF PPPEDB             */   36290694
000900**************************************************************/   36290694
000910*    COPYID=CPWSELOF                                              CPWSELOF
001000******************************************************************CPWSELOF
001001*   COPY MEMBER USED TO STORE THE LAYOFF ARRAY TABLE.            *CPWSELOF
001002*   MAXIMUM ENTRIES OF TABLE IS STORED IN PAYROLL CONSTANT       *CPWSELOF
001003*   (IDC) MEMBER.                                                *CPWSELOF
001004******************************************************************CPWSELOF
001100*01  LAYOFF-ARRAY.                                                CPWSELOF
001200     05  LOF-ARRAY                         OCCURS 10.             CPWSELOF
001210         10  LOF-OCCURRENCE-KEY.                                  CPWSELOF
001300             15  LOF-TITLE-CODE    PIC X(4).                      CPWSELOF
001400             15  LOF-DEPT-CODE     PIC X(6).                      CPWSELOF
002600             15  LOF-NOTICE-DATE   PIC X(10).                     05490748
001410         10  FILLER.                                              CPWSELOF
001500             15  LOF-UNIT-CODE     PIC X(6).                      CPWSELOF
001600             15  LOF-ADC-CODE      PIC X(01).                     CPWSELOF
003000*****        15  LOF-NOTICE-DATE   PIC X(10).                     05490748
001701         10  LOF-SALARY-DATA.                                     CPWSELOF
001702             15  LOF-SALARY        PIC S9(6)V9999  COMP-3.        CPWSELOF
001703             15  LOF-SALARY-IND    PIC X(01).                     CPWSELOF
001704             15  LOF-PERCENT-TIME  PIC S9V99       COMP-3.        CPWSELOF
001705         10  LOF-REHIRE-DATA.                                     CPWSELOF
001800             15  LOF-REHIRE-BGN-DATE            PIC X(10).        CPWSELOF
001900             15  LOF-REHIRE-EXP-DATE            PIC X(10).        CPWSELOF
002000             15  LOF-REHIRE-SUSP-BGN-TERM-DATE  PIC X(10).        CPWSELOF
002100             15  LOF-REHIRE-SUSP-END-DATE       PIC X(10).        CPWSELOF
002200             15  LOF-REHIRE-SUSP-TERM-RSN       PIC X(01).        CPWSELOF
002201         10  LOF-RECALL-DATA.                                     CPWSELOF
002202             15  LOF-RECALL-BGN-DATE            PIC X(10).        CPWSELOF
002203             15  LOF-RECALL-EXP-DATE            PIC X(10).        CPWSELOF
002204             15  LOF-RECALL-SUSP-BGN-TERM-DATE  PIC X(10).        CPWSELOF
002205             15  LOF-RECALL-SUSP-END-DATE       PIC X(10).        CPWSELOF
002206             15  LOF-RECALL-SUSP-TERM-RSN       PIC X(01).        CPWSELOF
002207******************************************************************CPWSELOF
