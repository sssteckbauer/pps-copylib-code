000800*==========================================================%      UCSD6413
000200*=    COPYBOOK:CLWSXDTM                                   =%      UCSD6413
000300*=    CHANGE   #UCSD6413    PROJ. REQUEST: UCSD6413       =%      UCSD6413
000400*=    NAME:    J. VANN      MODIFICATION DATE: 10/23/14   =%      UCSD6413
000500*=                                                        =%      UCSD6413
000600*=    DESCRIPTION:                                        =%      UCSD6413
000700*=    MODIFY COPYBOOK CLWSXDTM TO REDEFINE HASH TOTAL     =%      UCSD6413
000700*=    FIELDS AS ALPANUMERIC TO BE USED IN ERROR MESSAGE   =%      UCSD6413
000700*=    LINE IF NEEDED.                                     =%      UCSD6413
000900*=                                                        =%      UCSD6413
008000*==========================================================%      UCSD6413
000100 01  DTIM-DEPT-TIME-TRANSMITTAL.                                  CLWSXDTM
000200     05  DTIM-RECORD-CONTROL.                                     CLWSXDTM
000300         10  DTIM-DOC-ID.                                         CLWSXDTM
000400             15  DTIM-SOURCE-ID          PIC X(4) .               CLWSXDTM
000500             15  DTIM-DOC-NUMBER         PIC 9(2) .               CLWSXDTM
000600         10  DTIM-RECORD-SEQUENCE    PIC 9(4) .                   CLWSXDTM
000700         10  DTIM-RECORD-TYPE        PIC X(1) .                   CLWSXDTM
000800             88  DTIM-RECORD-HEADER      VALUE  '1' .             CLWSXDTM
000900             88  DTIM-RECORD-DETAIL      VALUE  '5' .             CLWSXDTM
001000             88  DTIM-RECORD-TRAILER     VALUE  '9' .             CLWSXDTM
001100     05  DTIM-RECORD-BODY        PIC X(69) .                      CLWSXDTM
001200     05  DTIM-HEADER-BODY    REDEFINES  DTIM-RECORD-BODY .        CLWSXDTM
001300         10  DTIM-SOURCE-TYPE        PIC X(1) .                   CLWSXDTM
001400         10  DTIM-SOURCE-REFERENCE   PIC X(10) .                  CLWSXDTM
001500         10  DTIM-POST-PAY-SCHED     PIC X(2) .                   CLWSXDTM
001600         10  DTIM-POST-PAY-END-DATE-FULL.                         CLWSXDTM
001700             15  DTIM-POST-PAY-END-CN    PIC X(2) .               CLWSXDTM
001800             15  DTIM-POST-PAY-END-DATE.                          CLWSXDTM
001900                 20  DTIM-POST-PAY-END-YR    PIC X(2) .           CLWSXDTM
002000                 20  DTIM-POST-PAY-END-MO    PIC X(2) .           CLWSXDTM
002100                 20  DTIM-POST-PAY-END-DA    PIC X(2) .           CLWSXDTM
002200         10  DTIM-DOC-DESCRIPTION    PIC X(40) .                  CLWSXDTM
002300         10  DTIM-DOC-REFERENCE-DATE-FULL.                        CLWSXDTM
002400             15  DTIM-DOC-REFERENCE-CN   PIC X(2) .               CLWSXDTM
002500             15  DTIM-DOC-REFERENCE-DATE.                         CLWSXDTM
002600                 20  DTIM-DOC-REFERENCE-YR   PIC X(2) .           CLWSXDTM
002700                 20  DTIM-DOC-REFERENCE-MO   PIC X(2) .           CLWSXDTM
002800                 20  DTIM-DOC-REFERENCE-DA   PIC X(2) .           CLWSXDTM
002900     05  DTIM-DETAIL-BODY    REDEFINES  DTIM-RECORD-BODY .        CLWSXDTM
003000         10  DTIM-EMPLOYEENO         PIC 9(9) .                   CLWSXDTM
003100         10  DTIM-PAY-PER-END-DATE-FULL.                          CLWSXDTM
003200             15  DTIM-PAY-PER-END-CN     PIC X(2) .               CLWSXDTM
003300             15  DTIM-PAY-PER-END-DATE.                           CLWSXDTM
003400                 20  DTIM-PAY-PER-END-YR     PIC X(2) .           CLWSXDTM
003500                 20  DTIM-PAY-PER-END-MO     PIC X(2) .           CLWSXDTM
003600                 20  DTIM-PAY-PER-END-DA     PIC X(2) .           CLWSXDTM
003700         10  DTIM-TITLE-CODE         PIC 9(4) .                   CLWSXDTM
003800         10  DTIM-IFIS-INDEX         PIC X(7) .                   CLWSXDTM
003900         10  DTIM-IFIS-SUB           PIC X(1) .                   CLWSXDTM
004000         10  DTIM-WORK-STUDY-PROG    PIC X(1) .                   CLWSXDTM
004100         10  DTIM-PAY-RATE           PIC 9(5)V9(4) .              CLWSXDTM
004200         10  DTIM-PAY-RATE-BASE      PIC X(1) .                   CLWSXDTM
004300         10  DTIM-PAY-RATE-ADJUST    PIC X(1) .                   CLWSXDTM
004400         10  DTIM-DOS                PIC X(3) .                   CLWSXDTM
004500         10  DTIM-TIME               PIC 9(3)V9(2) .              CLWSXDTM
004600         10  DTIM-TIME-TYPE          PIC X(1) .                   CLWSXDTM
004700         10  DTIM-TIME-SIGN          PIC X(1) .                   CLWSXDTM
004800         10  DTIM-DETAIL-EXPANSION   PIC X(8) .                   CLWSXDTM
004900         10  DTIM-TRANSACTION-GROUP  PIC X(4) .                   CLWSXDTM
005000         10  DTIM-DEPT-REFERENCE     PIC X(6) .                   CLWSXDTM
005100     05  DTIM-TRAILER-BODY   REDEFINES  DTIM-RECORD-BODY .        CLWSXDTM
005200         10  DTIM-RECORD-COUNT       PIC 9(4) .                   CLWSXDTM
005300         10  DTIM-HASH-EMP-NO        PIC 9(13) .                  CLWSXDTM
005400         10  DTIM-HASH-TITLE-CODE    PIC 9(8) .                   CLWSXDTM
005500         10  DTIM-HASH-PAY-RATE      PIC 9(11)V9(2) .             CLWSXDTM
005600         10  DTIM-HASH-PAY-RATEX     REDEFINES                    UCSD6413
005600             DTIM-HASH-PAY-RATE      PIC X(13).                   UCSD6413
005600         10  DTIM-HASH-TIME          PIC 9(7)V9(2) .              CLWSXDTM
005600         10  DTIM-HASH-TIMEX         REDEFINES                    UCSD6413
005600             DTIM-HASH-TIME          PIC X(9).                    UCSD6413
005700         10  DTIM-TRAILER-EXPANSION  PIC X(22) .                  CLWSXDTM
