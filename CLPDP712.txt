000100*==========================================================%      DS795
000200*=    COPY MEMBER: CLPDP712                               =%      DS795
000300*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000400*=                                         CONVERSION     =%      DS795
000500*=    NAME: R. SWIDERSKI    MODIFICATION DATE: 05/12/94   =%      DS795
000600*=                                                        =%      DS795
000700*=    DESCRIPTION:                                        =%      DS795
000800*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000900*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
001000*==========================================================%      DS795
000100*                                                                 CLPDP712
000200*      CONTROL REPORT 'PPP712CR'                                  CLPDP712
000300*                                                                 CLPDP712
000400*                                                                 CLPDP712
001500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP712
000600                                                                  CLPDP712
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP712
000800                                                                  CLPDP712
000900     MOVE 'PPP712CR'             TO CR-HL1-RPT.                   CLPDP712
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP712
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP712
001200                                                                  CLPDP712
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP712
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP712
001500                                                                  CLPDP712
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP712
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP712
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP712
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP712
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP712
002100                                                                  CLPDP712
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP712
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP712
002400                                                                  CLPDP712
002500     MOVE XSCP-SPEC-CARD-REC     TO CR-DL5-CTL-CARD.              CLPDP712
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP712
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP712
002800                                                                  CLPDP712
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP712
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP712
003100                                                                  CLPDP712
004200*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
004300     COPY CPPDTIME.                                               DS795
004400     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP712
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP712
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP712
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP712
003700                                                                  CLPDP712
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP712
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP712
004000*                                                                 CLPDP712
004100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP712
004200*                                                                 CLPDP712
004300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP712
004400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP712
004500*                                                                 CLPDP712
004600     MOVE 'APPOINTMENT'          TO CR-DL9-FILE.                  CLPDP712
004700     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP712
004800     MOVE IO-FCP-APPT            TO CR-DL9-VALUE.                 CLPDP712
004900     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP712
005000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP712
005100*                                                                 CLPDP712
005200*  FOLLOWING FILE NOT CREATED AS OF RELEASE 268                   CLPDP712
005300*                                                                 CLPDP712
005400*    MOVE 'BCS-DISTR'            TO CR-DL9-FILE.                  CLPDP712
005500*    MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP712
005600*    MOVE IO-BCS-DIST            TO CR-DL9-VALUE.                 CLPDP712
005700*    MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP712
005800*    WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP712
005900*                                                                 CLPDP712
006000*                                                                 CLPDP712
006100     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP712
006200     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP712
006300                                                                  CLPDP712
006400     CLOSE CONTROLREPORT.                                         CLPDP712
