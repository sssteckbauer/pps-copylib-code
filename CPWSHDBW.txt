000100**************************************************************/   36570961
000200*  COPYMEMBER: CPWSHDBW                                      */   36570961
000300*  RELEASE: ___0961______ SERVICE REQUEST(S): _____3657____  */   36570961
000400*  NAME:_______DZL_______ MODIFICATION DATE:  __02/14/95____ */   36570961
000500*  DESCRIPTION:                                              */   36570961
000600*    ADDED FIELDS FOR SUPPORT OF PERSONNEL HISTORY (HR)      */   36570961
000700*    SCREENS                                                 */   36570961
000800**************************************************************/   36570961
000100**************************************************************/   36430944
000200*  COPYLIB: CPWSHDBW                                         */   36430944
000300*  RELEASE # ___0944___   SERVICE REQUEST NO(S)____3643______*/   36430944
000400*  NAME __MLG__________   MODIFICATION DATE ____11/17/94_____*/   36430944
000500*  DESCRIPTION                                               */   36430944
000600*                                                            */   36430944
000700*       HDB INQUIRY COMMON WORK EXTERNAL AREA                */   36430944
000800*                                                            */   36430944
000900**************************************************************/   36430944
001000*01  CPWSHDBW EXTERNAL.                                           CPWSHDBW
001100     05  CPWSHDBW-AREA   PIC X(1024).                             CPWSHDBW
001200     05  CPWSHDBW-DATA REDEFINES CPWSHDBW-AREA.                   CPWSHDBW
001300         10  CPWSHDBW-INIT-IND             PIC X(16).             CPWSHDBW
001400             88  CPWSHDBW-INITIALIZED          VALUE              CPWSHDBW
001500                                           '>>$$CPWSHDBW$$<<'.    CPWSHDBW
001510         10  CPWSHDBW-KEY-DATA.                                   CPWSHDBW
001600             15 CPWSHDBW-BAS-EFF-DATE      PIC X(10).             CPWSHDBW
001700             15 CPWSHDBW-BAS-EFF-TIME      PIC X(10).             CPWSHDBW
001741             15 CPWSHDBW-INCORRECT-FLAG    PIC X(01).             CPWSHDBW
001742                 88  CPWSHDBW-INCORRECT-SET-ON     VALUE 'X'.     CPWSHDBW
001743                 88  CPWSHDBW-INCORRECT-SET-OFF    VALUE SPACES.  CPWSHDBW
001800         10  CPWSHDBW-NEXT-EFF-DATE        PIC X(10).             CPWSHDBW
001900         10  CPWSHDBW-NEXT-EFF-TIME        PIC X(10).             CPWSHDBW
002000         10  CPWSHDBW-PREV-EFF-DATE        PIC X(10).             CPWSHDBW
002100         10  CPWSHDBW-PREV-EFF-TIME        PIC X(10).             CPWSHDBW
002500         10  CPWSHDBW-VIEW-INCOR-IND       PIC X(01).             CPWSHDBW
002600             88 CPWSHDBW-VIEW-INCOR            VALUE 'Y'.         CPWSHDBW
002700             88 CPWSHDBW-DONT-VIEW-INCOR       VALUE 'N'.         CPWSHDBW
002800         10  CPWSHDBW-ORIG-INCORRECT-FLAG  PIC X(01).             CPWSHDBW
002900         10  CPWSHDBW-FLAG-STATUS-SW       PIC X(01).             CPWSHDBW
003000             88 CPWSHDBW-FLAG-UNCHANGED        VALUE '1'.         CPWSHDBW
003100             88 CPWSHDBW-FLAG-CHANGED          VALUE '2'.         CPWSHDBW
003200             88 CPWSHDBW-FLAG-IN-ERROR         VALUE '3'.         CPWSHDBW
003300         10  CPWSHDBW-DB2-TABLE            PIC X(18).             CPWSHDBW
003400         10  CPWSHDBW-DB2-COLUMN           PIC X(18).             CPWSHDBW
003500         10  CPWSHDBW-SAVE-APPT-NUM        PIC S9(04) COMP.       CPWSHDBW
003510         10  CPWSHDBW-SAVE-DIST-NUM        PIC S9(04) COMP.       CPWSHDBW
003511         10  CPWSHDBW-SAVE-DIST-NUM-1      PIC S9(04) COMP.       CPWSHDBW
003513         10  CPWSHDBW-SAVE-TITLE-CODE      PIC X(04).             CPWSHDBW
003514         10  CPWSHDBW-SAVE-DEPT-CODE       PIC X(06).             CPWSHDBW
003515         10  CPWSHDBW-SAVE-NOTICE-DATE     PIC X(10).             CPWSHDBW
003516         10  CPWSHDBW-SAVE-TUC             PIC X(02).             CPWSHDBW
003517         10  CPWSHDBW-SAVE-APPT-REP-CODE   PIC X(01).             CPWSHDBW
003520         10  CPWSHDBW-ACTION-CODE          PIC X(02).             CPWSHDBW
003521         10  CPWSHDBW-PREV-APPT-NUM        PIC S9(04) COMP.       CPWSHDBW
003522         10  CPWSHDBW-PREV-DIST-NUM        PIC S9(04) COMP.       CPWSHDBW
003523         10  CPWSHDBW-PREV-ACT-CODE        PIC X(02).             CPWSHDBW
003524         10  CPWSHDBW-PREV-TITLE-CODE      PIC X(04).             CPWSHDBW
003525         10  CPWSHDBW-PREV-DEPT-CODE       PIC X(06).             CPWSHDBW
003526         10  CPWSHDBW-PREV-NOTICE-DATE     PIC X(10).             CPWSHDBW
003527         10  CPWSHDBW-PREV-TUC             PIC X(02).             CPWSHDBW
003528         10  CPWSHDBW-PREV-APPT-REP-CODE   PIC X(01).             CPWSHDBW
003529         10  CPWSHDBW-NEXT-APPT-NUM        PIC S9(04) COMP.       CPWSHDBW
003530         10  CPWSHDBW-NEXT-DIST-NUM        PIC S9(04) COMP.       CPWSHDBW
003531         10  CPWSHDBW-NEXT-ACT-CODE        PIC X(02).             CPWSHDBW
003532         10  CPWSHDBW-NEXT-TITLE-CODE      PIC X(04).             CPWSHDBW
003533         10  CPWSHDBW-NEXT-DEPT-CODE       PIC X(06).             CPWSHDBW
003534         10  CPWSHDBW-NEXT-NOTICE-DATE     PIC X(10).             CPWSHDBW
003535         10  CPWSHDBW-NEXT-TUC             PIC X(02).             CPWSHDBW
003536         10  CPWSHDBW-NEXT-APPT-REP-CODE   PIC X(01).             CPWSHDBW
003537         10  CPWSHDBW-LAST-EMPLID          PIC X(9).              CPWSHDBW
003538         10  CPWSHDBW-TABLE        OCCURS 6 TIMES                 CPWSHDBW
003539                                   INDEXED BY  CPWSHDBW-INDEX-1.  CPWSHDBW
003540             15 CPWSHDBW-EFF-DATE          PIC X(10).             CPWSHDBW
003541             15 CPWSHDBW-EFF-TIME          PIC X(10).             CPWSHDBW
003542             15 CPWSHDBW-ERH-OCC           PIC X(01).             CPWSHDBW
003543                 88  CPWSHDBW-INCOR-SET-ON     VALUE 'X'.         CPWSHDBW
003544                 88  CPWSHDBW-INCOR-SET-OFF    VALUE SPACES.      CPWSHDBW
003545             15 CPWSHDBW-NEW-ERH-OCC       PIC X(01).             CPWSHDBW
003557             15 CPWSHDBW-BKGRND-CHECK-CODE PIC S9(04) COMP.       CPWSHDBW
003558             15 CPWSHDBW-AWARD             PIC S9(04) COMP.       CPWSHDBW
003559             15 CPWSHDBW-LIC-CERT          PIC X(03).             CPWSHDBW
003560             15 CPWSHDBW-HDP-NO            PIC X(02).             CPWSHDBW
003561             15 CPWSHDBW-HNR-DATE          PIC X(10).             CPWSHDBW
003562             15 CPWSHDBW-HNR-TYPE          PIC X(04).             CPWSHDBW
003563             15 CPWSHDBW-MODIFY-FLAG       PIC X(01).             CPWSHDBW
003564                 88  CPWSHDBW-MODIFY-FLAG-OFF  VALUE SPACE.       CPWSHDBW
003565                 88  CPWSHDBW-MODIFY-FLAG-ON   VALUE 'X'.         CPWSHDBW
003566         10  CPWSHDBW-BKGRND-CHECK-CD      PIC S9(04) COMP.       CPWSHDBW
003567         10  CPWSHDBW-NEXT-BKGRND-CHECK-CD PIC S9(04) COMP.       CPWSHDBW
003568         10  CPWSHDBW-PREV-BKGRND-CHECK-CD PIC S9(04) COMP.       CPWSHDBW
003569         10  CPWSHDBW-AWARD-ID             PIC S9(04) COMP.       CPWSHDBW
003570         10  CPWSHDBW-PREV-AWARD-ID        PIC S9(04) COMP.       CPWSHDBW
003571         10  CPWSHDBW-NEXT-AWARD-ID        PIC S9(04) COMP.       CPWSHDBW
003572         10  CPWSHDBW-LICENSE-CERT         PIC X(03).             CPWSHDBW
003573         10  CPWSHDBW-PREV-LICENSE-CERT    PIC X(03).             CPWSHDBW
003574         10  CPWSHDBW-NEXT-LICENSE-CERT    PIC X(03).             CPWSHDBW
003575         10  CPWSHDBW-HDP-NUM              PIC X(02).             CPWSHDBW
003576         10  CPWSHDBW-PREV-HDP-NUM         PIC X(02).             CPWSHDBW
003577         10  CPWSHDBW-NEXT-HDP-NUM         PIC X(02).             CPWSHDBW
003578         10  CPWSHDBW-HONOR-DATE           PIC X(10).             CPWSHDBW
003579         10  CPWSHDBW-HONOR-TYPE           PIC X(04).             CPWSHDBW
003580         10  CPWSHDBW-PREV-HONOR-TYPE      PIC X(04).             CPWSHDBW
003581         10  CPWSHDBW-PREV-HONOR-DATE      PIC X(10).             CPWSHDBW
003582         10  CPWSHDBW-NEXT-HONOR-TYPE      PIC X(04).             CPWSHDBW
003583         10  CPWSHDBW-NEXT-HONOR-DATE      PIC X(10).             CPWSHDBW
003584         10  CPWSHDBW-PAST-PRESENT-SW      PIC X(01).             CPWSHDBW
003585         10  CPWSHDBW-ASOF-DATE            PIC X(10).             CPWSHDBW
003586         10  CPWSHDBW-XLOK-TOP-EFF-DATE    PIC X(10).             CPWSHDBW
003587         10  CPWSHDBW-XLOK-BOT-EFF-DATE    PIC X(10).             CPWSHDBW
003588         10  CPWSHDBW-LAST-DATE            PIC X(10).             CPWSHDBW
003589         10  CPWSHDBW-PREV-FUNCTION        PIC X(04).             CPWSHDBW
011100         10  CPWSHDBW-REDISPLAY-PASTP-SW   PIC X(01).             36570961
011200             88 CPWSHDBW-REDISPLAY-PASTP       VALUE 'Y'.         36570961
011300             88 CPWSHDBW-DONT-REDISPLAY-PASTP  VALUE 'N'.         36570961
011400         10  CPWSHDBW-ITERATION-NUMBER     PIC X(03).             36570961
011500         10  CPWSHDBW-CURRENT-FUNC         PIC X(04).             36570961
011600         10  CPWSHDBW-CURRENT-ASOF-DATE    PIC X(10).             36570961
011700         10  CPWSHDBW-CURRENT-ASOF-PAGE    PIC 9(02).             36570961
011800         10  CPWSHDBW-NEW-ASOF-DATE        PIC X(10).             36570961
011900         10  CPWSHDBW-PREV-PP-SW           PIC X(01).             36570961
012000         10  CPWSHDBW-PREV-INCOR-FLAG      PIC X(01).             36570961
012100         10  CPWSHDBW-PP-SW-CHG-SW         PIC X(01).             36570961
012200             88 CPWSHDBW-PP-SW-CHANGED         VALUE 'Y'.         36570961
012300             88 CPWSHDBW-PP-SW-UNCHANGED       VALUE 'N'.         36570961
012400         10  CPWSHDBW-INCOR-FLAG-CHG-SW    PIC X(01).             36570961
012500             88 CPWSHDBW-INCOR-CHANGED         VALUE 'Y'.         36570961
012600             88 CPWSHDBW-INCOR-UNCHANGED       VALUE 'N'.         36570961
012700         10  CPWSHDBW-FILLER               PIC X(386).            36570961
012800*****    10  CPWSHDBW-FILLER               PIC X(420).            36570961
003600***************    END OF SOURCE - CPWSHDBW    *******************CPWSHDBW
