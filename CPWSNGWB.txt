000001**************************************************************/   52131415
000002*  COPYMEMBER: CPWSNGWB                                      */   52131415
000003*  RELEASE: ___1415______ SERVICE REQUEST(S): ____15213____  */   52131415
000004*  NAME:_______MLG_______ MODIFICATION DATE:  __06/10/02____ */   52131415
000005*  DESCRIPTION:                                              */   52131415
000006*     ADDED ERROR CODE FOR HTML OVERFLOW                     */   52131415
000007*                                                            */   52131415
000008**************************************************************/   52131415
000000**************************************************************/   73151304
000001*  COPYMEMBER: CPWSNGWB                                      */   73151304
000002*  RELEASE: ___1304______ SERVICE REQUEST(S): ____17315____  */   73151304
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___06/21/00__  */   73151304
000004*  DESCRIPTION:                                              */   73151304
000005*                                                            */   73151304
000006*  ADDED ERROR CODE FOR PPTCTUTL CALLS                       */   73151304
000007*                                                            */   73151304
000008**************************************************************/   73151304
000000**************************************************************/   25991140
000001*  COPYMEMBER: CPWSNGWB                                      */   25991140
000002*  RELEASE: ___1140______ SERVICE REQUEST(S): ____12599____  */   25991140
000003*  NAME:______M SANO_____ MODIFICATION DATE:  ___05/20/97__  */   25991140
000004*  DESCRIPTION:                                              */   25991140
000005*  - ADD PAN EVENT PIEC                                      */   25991140
000007**************************************************************/   25991140
000100**************************************************************/   42631089
000200*  COPYMEMBER: CPWSNGWB                                      */   42631089
000300*  RELEASE: ___1089______ SERVICE REQUEST(S): ___14263_____  */   42631089
000400*  NAME:_______JLT_______ MODIFICATION DATE:  __10/08/96____ */   42631089
000500*  DESCRIPTION:                                              */   42631089
000600*  - ADD 88 LEVEL ERROR TO INDICATE THAT ABEND TYPE OF ERROR */   42631089
000600*    HAS BEEN RETURNED FROM THE ACCOUNT/FUND PROFILE MODULE. */   42631089
000700**************************************************************/   42631089
000100**************************************************************/   28601064
000200*  COPYMEMBER: CPWSNGWB                                      */   28601064
000300*  RELEASE: ___1064______ SERVICE REQUEST(S): ____12860____  */   28601064
000400*  NAME:_______JLT_______ MODIFICATION DATE:  __04/19/96____ */   28601064
000500*  DESCRIPTION:                                              */   28601064
000600*  - ADD THF EVENTS 'EDRA', 'EDLA', 'EDDD'.                  */   28601064
000700**************************************************************/   28601064
000000**************************************************************/   36370967
000001*  COPYMEMBER: CPWSNGWB                                      */   36370967
000002*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3637____  */   36370967
000003*  NAME:_______MLG_______ MODIFICATION DATE:  __03/10/95____ */   36370967
000004*  DESCRIPTION:                                              */   36370967
000005*    ADD THF AND OPTR EVENTS                                 */   36370967
000007**************************************************************/   36370967
000100**************************************************************/   36490863
000200*  COPYMEMBER: CPWSNGWB                                      */   36490863
000300*  RELEASE: ___0863______ SERVICE REQUEST(S): _____3649____  */   36490863
000400*  NAME:_______JLT_______ MODIFICATION DATE:  __01/27/94____ */   36490863
000500*  DESCRIPTION:                                              */   36490863
000600*      INITIAL INSTALLATION OF COPYMEMBER.                   */   36490863
000700**************************************************************/   36490863
000800*01* CPWSNGWB      EXTERNAL.                                      CPWSNGWB
000900     05  CPWSNGWB-EVENT                    PIC X(04).             CPWSNGWB
001000        88  CPWSNGWB-EVENT-OK VALUE 'HIRE'                        CPWSNGWB
005148                                    'APPT'                        CPWSNGWB
005150                                    'CITZ'                        CPWSNGWB
005152                                    'FICA'                        CPWSNGWB
005154                                    'BENE'                        CPWSNGWB
005156                                    'LAYO'                        CPWSNGWB
005158                                    'INDL'                        CPWSNGWB
005160                                    'KEYC'                        CPWSNGWB
005162                                    'LABR'                        CPWSNGWB
005164                                    'LEAV'                        CPWSNGWB
005166                                    'LVRT'                        CPWSNGWB
005168                                    'SABB'                        CPWSNGWB
005170                                    'SEPR'                        CPWSNGWB
005171                                    'EDAP'                        36370967
005172                                    'EDFT'                        36370967
005173                                    'EDLR'                        36370967
005174                                    'EDTL'                        36370967
005175                                    'EDTS'                        36370967
005176                                    'EDTM'                        36370967
004300                                    'EDRA'                        28601064
004400                                    'EDLA'                        28601064
004500                                    'EDDD'                        28601064
004510                                    'PIEC'                        25991140
005180                                    'WSPC'.                       CPWSNGWB
005181     05  CPWSNGWB-EMPLOYEE-ID              PIC X(09).             CPWSNGWB
005182     05  CPWSNGWB-CONVERTED-EMP-NAME       PIC X(26).             CPWSNGWB
005183     05  CPWSNGWB-RETURN-CODE          PIC S9(04) COMP.           CPWSNGWB
005184         88 CPWSNGWB-RETURN-OK          VALUE ZERO.               CPWSNGWB
005185         88 CPWSNGWB-CEEDATE-ERROR      VALUE +101.               CPWSNGWB
005186         88 CPWSNGWB-EVNT-OVRFLO-ERROR  VALUE +102.               CPWSNGWB
005187         88 CPWSNGWB-TEXT-OVRFLO-ERROR  VALUE +103.               CPWSNGWB
005188         88 CPWSNGWB-ADDR-OVRFLO-ERROR  VALUE +104.               CPWSNGWB
005189         88 CPWSNGWB-ZERO-ADDRESSES-ER  VALUE +105.               CPWSNGWB
005190         88 CPWSNGWB-INVALID-EVENT-ERR  VALUE +106.               CPWSNGWB
005191         88 CPWSNGWB-CALL-FAILED-ERROR  VALUE +107.               CPWSNGWB
005710         88 CPWSNGWB-HTML-OVRFLO-ERROR  VALUE +108.               52131415
005192         88 CPWSNGWB-SQL-ERROR          VALUE +201.               CPWSNGWB
005193         88 CPWSNGWB-CTT-ERROR          VALUE +202.               CPWSNGWB
005194         88 CPWSNGWB-DOS-ERROR          VALUE +203.               CPWSNGWB
006000         88 CPWSNGWB-AFP-MODULE-ERR     VALUE +204.               42631089
006020         88 CPWSNGWB-TCTUTL-ERROR       VALUE +205.               73151304
005195         88 CPWSNGWB-LOGIC-ERROR-1      VALUE +301.               CPWSNGWB
005196         88 CPWSNGWB-LOGIC-ERROR-2      VALUE +302.               CPWSNGWB
005197         88 CPWSNGWB-LOGIC-ERROR-3      VALUE +303.               CPWSNGWB
005198         88 CPWSNGWB-LOGIC-ERROR-4      VALUE +304.               CPWSNGWB
005199     05  CPWSNGWB-ABEND-REF1               PIC X(40).             CPWSNGWB
005200     05  CPWSNGWB-ABEND-REF2               PIC X(60).             CPWSNGWB
005201     05  CPWSNGWB-UNLOAD-TEXT-OR-ADDR      PIC X(01).             CPWSNGWB
005202         88 CPWSNGWB-UNLOAD-TEXT        VALUE 'T'.                CPWSNGWB
005203         88 CPWSNGWB-UNLOAD-ADDR        VALUE 'A'.                CPWSNGWB
005204     05  CPWSNGWB-UNIQUE-DD-NAME           PIC X(08).             CPWSNGWB
005205*  *    "CPWSNGWB-UNIQUE-DD-NAME" REPRESENTS A UNIQUE KEY FOR   * CPWSNGWB
005206*  *    THE UPDATE IN PROGRESS.  IN CICS MODE, IT IS A COMBIN-  * CPWSNGWB
005207*  *    ATION OF SUBSYSTEM AND TERMINAL ID; IN BATCH, IT CAN    * CPWSNGWB
005208*  *    BE ANY VALUE.                                           * CPWSNGWB
005209     05  FILLER                            PIC X(32).             CPWSNGWB
