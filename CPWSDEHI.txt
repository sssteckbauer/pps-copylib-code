000010**************************************************************/   36430795
000020*  COPYLIB: CPWSDEHI                                         */   36430795
000030*  RELEASE # ___0795___   SERVICE REQUEST NO(S)____3643______*/   36430795
000040*  NAME __MLG__________   MODIFICATION DATE ____07/15/93_____*/   36430795
000050*  DESCRIPTION                                               */   36430795
000060*                                                            */   36430795
000070*       FIELD PROTECTION AND BUNDLE HIGHLIGHTING ARRAY       */   36430795
000090*                                                            */   36430795
000091**************************************************************/   36430795
000100*01  CPWSDEHI  EXTERNAL.                                          CPWSDEHI
000110     05  CPWSDEHI-EXT-AREA                 PIC X(20480).          CPWSDEHI
000120     05  CPWSDEHI-EXT-DATA REDEFINES CPWSDEHI-EXT-AREA.           CPWSDEHI
000200         10  CPWSDEHI-DATA-ELEMENT-ARRAY OCCURS 9999.             CPWSDEHI
000210             15  CPWSDEHI-PROTECT-IND      PIC X.                 CPWSDEHI
000300             15  CPWSDEHI-BUNDLE-IND       PIC X.                 CPWSDEHI
000400                 88  CPWSDEHI-BUNDLE-IND-ON    VALUE 'Y'.         CPWSDEHI
000500         10  CPWSDEHI-FILLER           PIC X(482).                CPWSDEHI
