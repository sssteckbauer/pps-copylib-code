000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWRSAVB                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*  - THIS COPYMEMBER DEFINES THE SAVE AREA USED IN USER12    *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100***  COPYID=CPWRSAVB                                              CPWRSAVB
001200*                                                                 CPWRSAVB
001300*01  SAVED-VALUES.                                                CPWRSAVB
001400     03  SAVE-WOS                   PIC X(9).                     CPWRSAVB
001500     03  SAVE-WOS-R REDEFINES                                     CPWRSAVB
001600         SAVE-WOS.                                                CPWRSAVB
001700         07 SAVE-APPT-WOS OCCURS 9  PIC X.                        CPWRSAVB
001800     03  SAVE-PRIMARY-TITLE         PIC X(04).                    CPWRSAVB
001900     03  SAVE-JOB-GROUP-ID          PIC X(03).                    CPWRSAVB
002000     03  SAVE-LAST-ACTION           PIC 99.                       CPWRSAVB
002100     03  SAVE-LAST-ACT-OTHR         PIC X(8).                     CPWRSAVB
002200     03  SAVE-EMPLMNT-STAT          PIC X.                        CPWRSAVB
002300     03  SAVE-UI-ELIG-CODE          PIC X.                        CPWRSAVB
002400     03  SAVE-PRI-PAY-SCHED         PIC X(2).                     CPWRSAVB
002500     03  SAVE-0128-FED-TX-EXEMPT    PIC 9(3).                     CPWRSAVB
002600     03  SAVE-0131-ST-TX-PER-DED    PIC 9(3).                     CPWRSAVB
002700     03  SAVE-0132-ST-TX-ITM-DED    PIC 9(3).                     CPWRSAVB
002800     03  SAVE-ADDNL-FWT             PIC 9(5)V99.                  CPWRSAVB
002900     03  SAVE-ADDNL-SWT             PIC 9(5)V99.                  CPWRSAVB
003000     03  SAVE-0122-RETR-SYS-CODE    PIC X.                        CPWRSAVB
003100     03  SAVE-0129-DCP-PLAN-CODE    PIC X.                        CPWRSAVB
003200     03  SAVE-0123-NDI-CODE         PIC X(01).                    CPWRSAVB
003300     03  SAVE-0330-LI-UC-PAID       PIC X(3).                     CPWRSAVB
003400     03  SAVE-0140-SEPARATION-DATE  PIC X(10).                    CPWRSAVB
003500     03  SAVE-0138-LOA-DATE         PIC X(10).                    CPWRSAVB
003600     03  SAVE-0188-DEP-LI-DATE      PIC X(10).                    CPWRSAVB
003700     03  SAVE-0272-DNTL-PLAN        PIC X(2).                     CPWRSAVB
003800     03  SAVE-0273-DNTL-COVR        PIC X(3).                     CPWRSAVB
003900     03  SAVE-0274-DNTL-DATE        PIC X(10).                    CPWRSAVB
004000     03  SAVE-0275-LI-SAL           PIC 9(3).                     CPWRSAVB
004100     03  SAVE-0276-LI-PLAN          PIC X.                        CPWRSAVB
004200     03  SAVE-0277-LI-DATE          PIC X(10).                    CPWRSAVB
004300     03  SAVE-0278-DEP-LI-IND       PIC X.                        CPWRSAVB
004400     03  SAVE-0280-AD-D-SAL         PIC 9(3).                     CPWRSAVB
004500     03  SAVE-0281-AD-D-COVR        PIC X.                        CPWRSAVB
004600     03  SAVE-0282-AD-D-DATE        PIC X(10).                    CPWRSAVB
004700     03  SAVE-0231-EPD-WAIT         PIC 9(3).                     CPWRSAVB
004800     03  SAVE-0232-EPD-SAL          PIC 9(5).                     CPWRSAVB
004900     03  SAVE-0233-EPD-DATE         PIC X(10).                    CPWRSAVB
005000     03  SAVE-0240-PRI-STAT-CODE    PIC X(02).                    CPWRSAVB
005100     03  SAVE-0241-PRI-STAT-DATE    PIC X(10).                    CPWRSAVB
005200     03  SAVE-0242-SEC-STAT-CODE    PIC X(02).                    CPWRSAVB
005300     03  SAVE-0243-SEC-STAT-DATE    PIC X(10).                    CPWRSAVB
005400     03  SAVE-0292-HLTH-PLAN        PIC X(2).                     CPWRSAVB
005500     03  SAVE-0293-HLTH-COVR        PIC X(3).                     CPWRSAVB
005600     03  SAVE-0294-HLTH-DATE        PIC X(10).                    CPWRSAVB
005700     03  SAVE-0347-OPTC-PLAN        PIC X(2).                     CPWRSAVB
005800     03  SAVE-0348-OPTC-COVR        PIC X(3).                     CPWRSAVB
005900     03  SAVE-0349-OPTC-DATE        PIC X(10).                    CPWRSAVB
006000     03  SAVE-0353-LEGAL-PLAN       PIC X(2).                     CPWRSAVB
006100     03  SAVE-0354-LEGAL-COVR       PIC X(3).                     CPWRSAVB
006200     03  SAVE-0355-LEGAL-DATE       PIC X(10).                    CPWRSAVB
006300     03  SAVE-0358-EXEC-LIFECHNG    PIC X(01).                    CPWRSAVB
006400     03  SAVE-0359-EXEC-LIFECHGDT   PIC X(10).                    CPWRSAVB
006500     03  SAVE-0360-BELI-IND         PIC X(01).                    CPWRSAVB
006600     03  SAVE-0451-UCPDLF-DATE      PIC X(10).                    CPWRSAVB
006700     03  SAVE-0452-EXECLF-DATE      PIC X(10).                    CPWRSAVB
006800     03  SAVE-0453-NDI-DATE         PIC X(10).                    CPWRSAVB
006900     03  SAVE-CMPR-NDI-DATE         PIC X(10).                    CPWRSAVB
007000     03  SAVE-0482-TRIP-PERCENT     PIC 9(04).                    CPWRSAVB
007100     03  SAVE-0483-TRIP-DURATION    PIC 9(04).                    CPWRSAVB
007200     03  SAVE-0484-TRIP-BEGIN-DATE  PIC X(10).                    CPWRSAVB
