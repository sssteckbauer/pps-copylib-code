000100**************************************************************/   69201534
000200*  COPYMEMBER: CPCTCTTI                                      */   69201534
000300*  RELEASE: ___1534______ SERVICE REQUEST(S): ____16920____  */   69201534
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___10/31/03__  */   69201534
000500*  DESCRIPTION:                                              */   69201534
000600*  - Added new "obsolete code value column".                 */   69201534
000700**************************************************************/   69201534
000000**************************************************************/   30871401
000001*  COPYMEMBER: CPCTCTTI                                      */   30871401
000002*  RELEASE: ___1401______ SERVICE REQUEST(S): _____3087____  */   30871401
000003*  NAME:_____SRS_________ CREATION DATE:      ___03/20/02__  */   30871401
000004*  DESCRIPTION:                                              */   30871401
000005*  - NEW COPY MEMBER FOR CODE TRANSLATION TABLE (CTT) INPUT  */   30871401
000007**************************************************************/   30871401
000008*    COPYID=CPCTCTTI                                              CPCTCTTI
010000*01  CODE-TRANS-CTT-TABLE-INPUT.                                  CPCTCTTI
010100     05 CTTI-DATABASE-ID                 PIC XXX.                 CPCTCTTI
010200     05 CTTI-DATA-ELEM-NO                PIC X(06).               CPCTCTTI
010300     05 CTTI-CODE-VALUE                  PIC X(06).               CPCTCTTI
010400     05 CTTI-TRANS-LEN.                                           CPCTCTTI
010500        10 CTTI-TRANS-LEN-9              PIC 99.                  CPCTCTTI
002200*****05 CTTI-CODE-TRANS                  PIC X(58).               69201534
002300     05 CTTI-CODE-TRANS                  PIC X(57).               69201534
002400     05 CTTI-VALUE-OBSOLETE              PIC X(01).               69201534
