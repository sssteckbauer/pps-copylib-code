000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWSXNRA                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*                                                            *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
001100*    COPYID=CPWSXNRA                                              CPWSXNRA
001200                                                                  CPWSXNRA
001300*01  PPNRARPT-INTERFACE.                                          CPWSXNRA
001400                                                                  CPWSXNRA
001500*    03  PPNRARPT-ACTION                PIC  X.                   CPWSXNRA
001600     03  PPNRARPT-ACTION                PIC  X(05).               CPWSXNRA
001700                                                                  CPWSXNRA
001800     03  PPNRARPT-RECORD-COUNTS.                                  CPWSXNRA
001900         05  PPNRARPT-TT-RECS-TOT            PIC S9(7) COMP-3.    CPWSXNRA
002000         05  PPNRARPT-TT-RECS-MO             PIC S9(5) COMP-3.    CPWSXNRA
002100         05  PPNRARPT-TT-RECS-YR             PIC S9(5) COMP-3.    CPWSXNRA
002200                                                                  CPWSXNRA
002300     03  PPNRARPT-VARIABLE-DATA.                                  CPWSXNRA
002400         05  PPNRARPT-TT-SORT-CODE-KEY       PIC X(1).            CPWSXNRA
002500         05  PPNRARPT-TT-ACTION-MSG          PIC X(1).            CPWSXNRA
