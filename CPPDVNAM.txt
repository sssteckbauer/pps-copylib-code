000000**************************************************************/   32201186
000001*  COPYMEMBER: CPPDVNAM                                      */   32201186
000002*  RELEASE: ___1186______ SERVICE REQUEST(S): ____13220____  */   32201186
000003*  NAME:_______QUAN______ CREATION DATE:      ___04/15/98__  */   32201186
000004*  DESCRIPTION:                                              */   32201186
000005*    PROCEDURE STATEMENTS USED TO CONVERT THE SINGLE 26 BYTE */   32201186
000006*    EMPLOYEE NAME FIELD TO INDIVIDUAL FIRST NAME, LAST NAME,*/   32201186
000007*    AND MIDDLE INITIAL FIELDS.                              */   32201186
000008*    NOTE:  THIS COPYMEMBER IS USED IN CONJUNCTION WITH      */   32201186
000009*           COPYMEMBER CPWSVNAM.                             */   32201186
000010*                                                            */   32201186
000020**************************************************************/   32201186
001010**--------------------------------------------------------------  CPPDVNAM
001100*                                                                 CPPDVNAM
001110*NOTE: IF *NOT* PASSING NAME-SUFFIX, MOVE SPACES TO               CPPDVNAM
001111*          VNAM-NAME-SUFFIX.                                      CPPDVNAM
001120*                                                                 CPPDVNAM
001200*      <<< EXAMPLE OF USAGE >>>                                   CPPDVNAM
001300*                                                                 CPPDVNAM
001500*--  MOVE EMP-NAME     OF PER-ROW  TO VNAM-NAME26.                CPPDVNAM
001510*--  MOVE NAME-SUFFIX  OF PER-ROW  TO VNAM-NAME-SUFFIX.           CPPDVNAM
001600*--  PERFORM VNAM-CONVERT-NAME.                                   CPPDVNAM
001700*--  MOVE VNAM-CONVERTED-FIRST-NAME  TO DESIRED-FIRST-NAME.       CPPDVNAM
001710*--  MOVE VNAM-CONVERTED-LAST-NAME   TO DESIRED-LAST-NAME.        CPPDVNAM
001720*--  MOVE VNAM-CONVERTED-MIDDLE-INIT TO DESIRED-MID-INITIAL.      CPPDVNAM
001800*                                                                 CPPDVNAM
001900*      <<< END EXAMPLE OF USAGE >>>                               CPPDVNAM
002000*                                                                 CPPDVNAM
002100**--------------------------------------------------------------  CPPDVNAM
002200 VNAM-CONVERT-NAME.                                               CPPDVNAM
002300*-------------------------------------------------------------    CPPDVNAM
002310     MOVE SPACES TO VNAM-CONVERTED-NAME                           CPPDVNAM
002320                    VNAM-CONVERTED-FIRST-NAME                     CPPDVNAM
002330                    VNAM-CONVERTED-LAST-NAME                      CPPDVNAM
002340                    VNAM-CONVERTED-MIDDLE-INIT.                   CPPDVNAM
002341     SET VNAM-LAST-NAME TO TRUE.                                  CPPDVNAM
002350                                                                  CPPDVNAM
002400     IF VNAM-NAME26   = SPACES                                    CPPDVNAM
002600         GO TO VNAM-EXIT                                          CPPDVNAM
002700     END-IF.                                                      CPPDVNAM
002710                                                                  CPPDVNAM
002900     INSPECT  VNAM-NAME26 REPLACING ALL LOW-VALUES BY SPACES.     CPPDVNAM
002910     INSPECT  VNAM-NAME-SUFFIX                                    CPPDVNAM
002911                          REPLACING ALL LOW-VALUES BY SPACES.     CPPDVNAM
002920                                                                  CPPDVNAM
003000     PERFORM WITH TEST BEFORE                                     CPPDVNAM
003100         VARYING VNAM-STRT-X FROM +1 BY +1                        CPPDVNAM
003200           UNTIL VNAM-STRT-X > +26                                CPPDVNAM
003300              OR VNAM-NAME26-BYTE (VNAM-STRT-X) = ','             CPPDVNAM
003400     END-PERFORM.                                                 CPPDVNAM
003410                                                                  CPPDVNAM
003500     IF VNAM-STRT-X > +26                                         CPPDVNAM
003600*---> NO COMMMA; NAME LIKE 'CHARRO'                               CPPDVNAM
003600*---> THIS CAN ONLY HAPPEN IN BATCH PROCESSING - NAME IS MOVED    CPPDVNAM
003600*---> TO LAST NAME FIELD.                                         CPPDVNAM
003611        SET VNAM-NO-LAST-NAME TO TRUE                             CPPDVNAM
003620        MOVE ZERO TO VNAM-STRT-X                                  CPPDVNAM
007420        MOVE VNAM-NAME26 TO VNAM-CONVERTED-LAST-NAME              CPPDVNAM
007420        GO TO VNAM-EXIT                                           CPPDVNAM
003900     END-IF.                                                      CPPDVNAM
003910                                                                  CPPDVNAM
002341     IF VNAM-LAST-NAME                                            CPPDVNAM
004000        COMPUTE VNAM-LAST-NAME-STOP = VNAM-STRT-X - 1             CPPDVNAM
004000     END-IF.                                                      CPPDVNAM
004010                                                                  CPPDVNAM
004100*---> POSITION THE START INDX AT 1ST CHARACTER AFTER THE COMMA    CPPDVNAM
004200     COMPUTE VNAM-STRT-X     = VNAM-STRT-X + 1.                   CPPDVNAM
004210                                                                  CPPDVNAM
004300*---> ALLOW FOR BLANK(S) BETWEEN COMMA AND FIRST NAME             CPPDVNAM
004400     PERFORM WITH TEST BEFORE                                     CPPDVNAM
004500         VARYING VNAM-STRT-X FROM VNAM-STRT-X BY +1               CPPDVNAM
004600           UNTIL VNAM-STRT-X > +26                                CPPDVNAM
004700            OR VNAM-NAME26-BYTE (VNAM-STRT-X) NOT = ' '           CPPDVNAM
004800     END-PERFORM.                                                 CPPDVNAM
004810                                                                  CPPDVNAM
004900*---> MOVE FIRST NAME                                             CPPDVNAM
004910     MOVE SPACES TO VNAM-CONVERTED-NAME.                          CPPDVNAM
005000     MOVE ZERO  TO VNAM-RECEIVE-X.                                CPPDVNAM
005200     PERFORM WITH TEST BEFORE                                     CPPDVNAM
005300         VARYING VNAM-STRT-X FROM VNAM-STRT-X BY +1               CPPDVNAM
005400           UNTIL VNAM-STRT-X > +26                                CPPDVNAM
005510              OR VNAM-NAME26-BYTE (VNAM-STRT-X) = ' '             CPPDVNAM
006300           ADD +1    TO VNAM-RECEIVE-X                            CPPDVNAM
006400           MOVE VNAM-NAME26-BYTE (VNAM-STRT-X)                    CPPDVNAM
006500                     TO VNAM-CONVRTD-NAME-BYTE (VNAM-RECEIVE-X)   CPPDVNAM
006700     END-PERFORM.                                                 CPPDVNAM
006701     MOVE VNAM-CONVERTED-NAME TO VNAM-CONVERTED-FIRST-NAME.       CPPDVNAM
006710                                                                  CPPDVNAM
006720     IF VNAM-STRT-X > +26                                         CPPDVNAM
003600        IF VNAM-LAST-NAME-STOP < +26                              CPPDVNAM
003600*-------> NO FIRST NAME FOLLOWING COMMA; NAME LIKE 'PRINCE,'      CPPDVNAM
003600*-------> SINGULAR NAME PLACED IN LAST NAME FIELD                 CPPDVNAM
007410           STRING VNAM-NAME26 (1:VNAM-LAST-NAME-STOP)             CPPDVNAM
007411                                 DELIMITED BY SIZE                CPPDVNAM
007412               ' '               DELIMITED BY SIZE                CPPDVNAM
007413               VNAM-NAME-SUFFIX  DELIMITED BY SIZE                CPPDVNAM
007420           INTO VNAM-CONVERTED-LAST-NAME                          CPPDVNAM
007420        END-IF                                                    CPPDVNAM
003600                                                                  CPPDVNAM
006730        GO TO VNAM-EXIT                                           CPPDVNAM
006731     END-IF.                                                      CPPDVNAM
006732                                                                  CPPDVNAM
006733     MOVE ZERO TO VNAM-RECEIVE-X                                  CPPDVNAM
006735*---> POSITION THE START INDX AT 1ST CHARACTER AFTER THE BLANKS   CPPDVNAM
006736*---> TO POINT TO THE FIRST LETTER IN MIDDLE NAME                 CPPDVNAM
006737     COMPUTE VNAM-STRT-X     = VNAM-STRT-X + 1.                   CPPDVNAM
006738     MOVE SPACES TO VNAM-CONVERTED-NAME.                          CPPDVNAM
006740                                                                  CPPDVNAM
006734     SET VNAM-FINISHED-SW-OFF    TO TRUE.                         CPPDVNAM
006750     PERFORM WITH TEST BEFORE                                     CPPDVNAM
006760         VARYING VNAM-STRT-X FROM VNAM-STRT-X BY +1               CPPDVNAM
006770           UNTIL VNAM-STRT-X > +26                                CPPDVNAM
006780              OR VNAM-FINISHED-SW-ON                              CPPDVNAM
006781           IF VNAM-NAME26-BYTE (VNAM-STRT-X) NOT = ' '            CPPDVNAM
006790              ADD +1   TO VNAM-RECEIVE-X                          CPPDVNAM
006791              MOVE VNAM-NAME26-BYTE (VNAM-STRT-X)                 CPPDVNAM
006792                       TO VNAM-CONVRTD-NAME-BYTE (VNAM-RECEIVE-X) CPPDVNAM
006793              SET VNAM-FINISHED-SW-ON TO TRUE                     CPPDVNAM
006794           END-IF                                                 CPPDVNAM
006795     END-PERFORM.                                                 CPPDVNAM
006796     MOVE VNAM-CONVRTD-NAME-BYTE (1)                              CPPDVNAM
006797                     TO VNAM-CONVERTED-MIDDLE-INIT.               CPPDVNAM
007310                                                                  CPPDVNAM
007320     IF VNAM-LAST-NAME                                            CPPDVNAM
007400*---> NOW MOVE LAST NAME IF AVAILABLE                             CPPDVNAM
007410        STRING VNAM-NAME26 (1:VNAM-LAST-NAME-STOP)                CPPDVNAM
007411                                 DELIMITED BY SIZE                CPPDVNAM
007412               ' '               DELIMITED BY SIZE                CPPDVNAM
007413               VNAM-NAME-SUFFIX  DELIMITED BY SIZE                CPPDVNAM
007420        INTO VNAM-CONVERTED-LAST-NAME                             CPPDVNAM
007430     END-IF.                                                      CPPDVNAM
008310*                                                                 CPPDVNAM
008400 VNAM-EXIT.        EXIT.                                          CPPDVNAM
008500*                                                                 CPPDVNAM
