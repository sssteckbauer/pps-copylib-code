000100**************************************************************/   74611376
000200*  COPYMEMBER: CPWSXEXP                                      */   74611376
000300*  RELEASE: ___1376______ SERVICE REQUEST(S): ____17461____  */   74611376
000400*  NAME:___STEINITZ______ MODIFICATION DATE:  ___10/31/01__  */   74611376
000500*  DESCRIPTION:                                              */   74611376
000600*  - ADDED 2 BYTES FOR SUB-LOCATION TO 50 OCCURRENCES.       */   74611376
000700**************************************************************/   74611376
000000**************************************************************/   52191347
000001*  COPYMEMBER: CPWSXEXP                                      */   52191347
000002*  RELEASE: ___1347______ SERVICE REQUEST(S): ____15219____  */   52191347
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___04/24/01__  */   52191347
000004*  DESCRIPTION:                                              */   52191347
000005*  - ADDED "FACULTY SUMMER SALARY" BENEFIT, LENGTH 4 TO      */   52191347
000006*    TYPE 2 RECORD.                                          */   52191347
000007**************************************************************/   52191347
000100**************************************************************/   48421333
000200*  COPYMEMBER: CPWSXEXP                                      */   48421333
000300*  RELEASE # __1333______ SERVICE REQUEST NO(S)__14842_______*/   48421333
000400*  NAME ________JLT____   MODIFICATION DATE ____02/12/01_____*/   48421333
000500*  DESCRIPTION                                               */   48421333
000600*   - NEW COPY STATEMENT TO DEFINE THE TYPE "2" EXPENSE      */   48421333
000700*     DISTRIBUTION WORK (EDW) RECORD FOR USE IN FD. THIS     */   48421333
000800*     COPYMEMBER IS ASSOCIATED WITH CPFDXEDF AND CPWSXEDR.   */   48421333
000900**************************************************************/   48421333
001000     SKIP2                                                        CPWSXEXP
001100*    COPYID=CPWSXEXP                                              CPWSXEXP
001200*                                                                 CPWSXEXP
001300*01  XEXP-DISTR-TYPE2-3.                                          CPWSXEXP
001400     03  FILLER                      PIC X(112).                  CPWSXEXP
001500     03  XEXP-TYPE2-3-NO-DISTR       PIC 99.                      CPWSXEXP
001600     03  XEXP-DISTR-VARIABLE         OCCURS                       CPWSXEXP
001700                                     0   TO  50  TIMES            CPWSXEXP
001800                                     DEPENDING   ON               CPWSXEXP
001900                                     XEXP-TYPE2-3-NO-DISTR.       CPWSXEXP
002000*****    05  FILLER                  PIC X(147).                  52191347
003600***      05  FILLER                  PIC X(151).                  74611376
003700         05  FILLER                  PIC X(153).                  74611376
002100                                                                  CPWSXEXP
