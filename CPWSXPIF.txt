000100**************************************************************/   36290724
000200*  COPYMEMBER: CPWSXPIF                                      */   36290724
000300*  RELEASE: ____0724____  SERVICE REQUEST(S): ____3629____   */   36290724
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __12/11/92__   */   36290724
000500*  DESCRIPTION:                                              */   36290724
000600*   ADDED LAYOFF UNIT CODE TABLE (LUC) KEY                   */   36290724
000700**************************************************************/   36290724
000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSXPIF                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    INTERFACE AREA FOR THE CONTROL DATA BASE QUERY MODULE   */   36330704
000700*                                                            */   36330704
000800*                                                            */   36330704
000900**************************************************************/   36330704
001000*------------------------------------------------------------*    CPWSXPIF
001100*-----------------------  CPWSXPIF  -------------------------*    CPWSXPIF
001200*------------------------------------------------------------*    CPWSXPIF
001300*01  CPWSXPIF.                                                    CPWSXPIF
001400*****  REQUEST INFORMATION ***********************************    CPWSXPIF
001500     05  :TAG:-INTERFACE-AREA.                                    CPWSXPIF
001600         10  :TAG:-TABLE-REQUEST           PIC  X(03).            CPWSXPIF
001700         10  :TAG:-PROCESSING-REQUEST      PIC  X(01).            CPWSXPIF
001800             88  :TAG:-OPEN-REQUEST        VALUE 'O'.             CPWSXPIF
001900             88  :TAG:-SEQ-REQUEST         VALUE 'S'.             CPWSXPIF
002000             88  :TAG:-RANDOM-REQUEST      VALUE 'R'.             CPWSXPIF
002100             88  :TAG:-CLOSE-REQUEST       VALUE 'C'.             CPWSXPIF
002200         10  :TAG:-CONTROL-KEY             PIC  X(26).            CPWSXPIF
002300         10  :TAG:-PPPBRA-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
002400             15  :TAG:-PPPBRA-CBUC         PIC  X(02).            CPWSXPIF
002500             15  :TAG:-PPPBRA-REP          PIC  X(01).            CPWSXPIF
002600             15  :TAG:-PPPBRA-SHC          PIC  X(01).            CPWSXPIF
002700             15  :TAG:-PPPBRA-DUC          PIC  X(01).            CPWSXPIF
002800             15  :TAG:-PPPBRA-PRIN-SUM     PIC S9(03) COMP-3.     CPWSXPIF
002900             15  FILLER                    PIC  X(19).            CPWSXPIF
003000         10  :TAG:-PPPBRD-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
003100             15  :TAG:-PPPBRD-CBUC         PIC  X(02).            CPWSXPIF
003200             15  :TAG:-PPPBRD-REP          PIC  X(01).            CPWSXPIF
003300             15  :TAG:-PPPBRD-SHC          PIC  X(01).            CPWSXPIF
003400             15  :TAG:-PPPBRD-DUC          PIC  X(01).            CPWSXPIF
003500             15  :TAG:-PPPBRD-PLN          PIC  X(02).            CPWSXPIF
003600             15  :TAG:-PPPBRD-CVG          PIC  X(03).            CPWSXPIF
003700             15  FILLER                    PIC  X(16).            CPWSXPIF
003800         10  :TAG:-PPPBRE-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
003900             15  :TAG:-PPPBRE-CBUC         PIC  X(02).            CPWSXPIF
004000             15  :TAG:-PPPBRE-REP          PIC  X(01).            CPWSXPIF
004100             15  :TAG:-PPPBRE-SHC          PIC  X(01).            CPWSXPIF
004200             15  :TAG:-PPPBRE-DUC          PIC  X(01).            CPWSXPIF
004300             15  :TAG:-PPPBRE-MIN-AGE      PIC S9(04) COMP.       CPWSXPIF
004400             15  FILLER                    PIC  X(19).            CPWSXPIF
004500         10  :TAG:-PPPBRG-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
004600             15  :TAG:-PPPBRG-CBUC         PIC  X(02).            CPWSXPIF
004700             15  :TAG:-PPPBRG-REP          PIC  X(01).            CPWSXPIF
004800             15  :TAG:-PPPBRG-SHC          PIC  X(01).            CPWSXPIF
004900             15  :TAG:-PPPBRG-DUC          PIC  X(01).            CPWSXPIF
005000             15  :TAG:-PPPBRG-GTN-NO       PIC  X(03).            CPWSXPIF
005100             15  FILLER                    PIC  X(18).            CPWSXPIF
005200         10  :TAG:-PPPBRH-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
005300             15  :TAG:-PPPBRH-CBUC         PIC  X(02).            CPWSXPIF
005400             15  :TAG:-PPPBRH-REP          PIC  X(01).            CPWSXPIF
005500             15  :TAG:-PPPBRH-SHC          PIC  X(01).            CPWSXPIF
005600             15  :TAG:-PPPBRH-DUC          PIC  X(01).            CPWSXPIF
005700             15  :TAG:-PPPBRH-PLN          PIC  X(02).            CPWSXPIF
005800             15  :TAG:-PPPBRH-CVG          PIC  X(03).            CPWSXPIF
005900             15  FILLER                    PIC  X(16).            CPWSXPIF
006000         10  :TAG:-PPPBRJ-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
006100             15  :TAG:-PPPBRJ-CBUC         PIC  X(02).            CPWSXPIF
006200             15  :TAG:-PPPBRJ-REP          PIC  X(01).            CPWSXPIF
006300             15  :TAG:-PPPBRJ-SHC          PIC  X(01).            CPWSXPIF
006400             15  :TAG:-PPPBRJ-DUC          PIC  X(01).            CPWSXPIF
006500             15  :TAG:-PPPBRJ-PLN          PIC  X(02).            CPWSXPIF
006600             15  :TAG:-PPPBRJ-CVG          PIC  X(03).            CPWSXPIF
006700             15  FILLER                    PIC  X(16).            CPWSXPIF
006800         10  :TAG:-PPPBRL-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
006900             15  :TAG:-PPPBRL-CBUC         PIC  X(02).            CPWSXPIF
007000             15  :TAG:-PPPBRL-REP          PIC  X(01).            CPWSXPIF
007100             15  :TAG:-PPPBRL-SHC          PIC  X(01).            CPWSXPIF
007200             15  :TAG:-PPPBRL-DUC          PIC  X(01).            CPWSXPIF
007300             15  :TAG:-PPPBRL-MIN-AGE      PIC S9(04) COMP.       CPWSXPIF
007400             15  FILLER                    PIC  X(19).            CPWSXPIF
007500         10  :TAG:-PPPBRO-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
007600             15  :TAG:-PPPBRO-CBUC         PIC  X(02).            CPWSXPIF
007700             15  :TAG:-PPPBRO-REP          PIC  X(01).            CPWSXPIF
007800             15  :TAG:-PPPBRO-SHC          PIC  X(01).            CPWSXPIF
007900             15  :TAG:-PPPBRO-DUC          PIC  X(01).            CPWSXPIF
008000             15  :TAG:-PPPBRO-PLN          PIC  X(02).            CPWSXPIF
008100             15  :TAG:-PPPBRO-CVG          PIC  X(03).            CPWSXPIF
008200             15  FILLER                    PIC  X(16).            CPWSXPIF
008300         10  :TAG:-PPPBRR-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
008400             15  :TAG:-PPPBRR-CBUC         PIC  X(02).            CPWSXPIF
008500             15  :TAG:-PPPBRR-REP          PIC  X(01).            CPWSXPIF
008600             15  :TAG:-PPPBRR-SHC          PIC  X(01).            CPWSXPIF
008700             15  :TAG:-PPPBRR-DUC          PIC  X(01).            CPWSXPIF
008800             15  :TAG:-PPPBRR-ENTRY-NO     PIC S9(04) COMP.       CPWSXPIF
008900             15  FILLER                    PIC  X(19).            CPWSXPIF
009000         10  :TAG:-PPPBUB-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
009100             15  :TAG:-PPPBUB-BUC          PIC  X(02).            CPWSXPIF
009200             15  :TAG:-PPPBUB-REP          PIC  X(01).            CPWSXPIF
009300             15  :TAG:-PPPBUB-SHC          PIC  X(01).            CPWSXPIF
009400             15  :TAG:-PPPBUB-DIST         PIC  X(01).            CPWSXPIF
009500             15  :TAG:-PPPBUB-TYPE         PIC  X(01).            CPWSXPIF
009600             15  :TAG:-PPPBUB-PLN          PIC  X(02).            CPWSXPIF
009700             15  FILLER                    PIC  X(18).            CPWSXPIF
009800         10  :TAG:-PPPBUD-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
009900             15  :TAG:-PPPBUD-BUC          PIC  X(02).            CPWSXPIF
010000             15  :TAG:-PPPBUD-DIST         PIC  X(01).            CPWSXPIF
010100             15  :TAG:-PPPBUD-ACCT-1       PIC  X(06).            CPWSXPIF
010200             15  :TAG:-PPPBUD-ACCT-2       PIC  X(06).            CPWSXPIF
010300             15  FILLER                    PIC  X(11).            CPWSXPIF
010400         10  :TAG:-PPPBUG-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
010500             15  :TAG:-PPPBUG-BUC          PIC  X(02).            CPWSXPIF
010600             15  :TAG:-PPPBUG-REP          PIC  X(01).            CPWSXPIF
010700             15  :TAG:-PPPBUG-SHC          PIC  X(01).            CPWSXPIF
010800             15  :TAG:-PPPBUG-DIST         PIC  X(01).            CPWSXPIF
010900             15  :TAG:-PPPBUG-GTN-NO       PIC  X(03).            CPWSXPIF
011000             15  FILLER                    PIC  X(18).            CPWSXPIF
011100         10  :TAG:-PPPBUS-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
011200             15  :TAG:-PPPBUS-BUC          PIC  X(02).            CPWSXPIF
011300             15  :TAG:-PPPBUS-SHC          PIC  X(01).            CPWSXPIF
011400             15  :TAG:-PPPBUS-DIST         PIC  X(01).            CPWSXPIF
011500             15  FILLER                    PIC  X(22).            CPWSXPIF
011600         10  :TAG:-PPPBUT-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
011700             15  :TAG:-PPPBUT-BUC          PIC  X(02).            CPWSXPIF
011800             15  FILLER                    PIC  X(24).            CPWSXPIF
011900         10  :TAG:-PPPDOS-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
012000             15  :TAG:-PPPDOS-EARN-TYPE    PIC  X(03).            CPWSXPIF
012100             15  FILLER                    PIC  X(23).            CPWSXPIF
012200         10  :TAG:-PPPGTN-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
012300             15  :TAG:-PPPGTN-PRIORITY     PIC  X(04).            CPWSXPIF
012400             15  :TAG:-PPPGTN-NO           PIC  X(03).            CPWSXPIF
012500             15  FILLER                    PIC  X(19).            CPWSXPIF
012600         10  :TAG:-PPPHME-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
012700             15  :TAG:-PPPHME-DEPT         PIC  X(06).            CPWSXPIF
012800             15  :TAG:-PPPHME-LOC          PIC  X(02).            CPWSXPIF
012900             15  FILLER                    PIC  X(18).            CPWSXPIF
013000         10  :TAG:-PPPLAF-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
013100             15  :TAG:-PPPLAF-TITLE-TYPE   PIC  X(01).            CPWSXPIF
013200             15  :TAG:-PPPLAF-TITLE-UNIT   PIC  X(02).            CPWSXPIF
013300             15  :TAG:-PPPLAF-AREP         PIC  X(01).            CPWSXPIF
013400             15  :TAG:-PPPLAF-SHC          PIC  X(01).            CPWSXPIF
013500             15  :TAG:-PPPLAF-DUC          PIC  X(01).            CPWSXPIF
013600             15  :TAG:-PPPLAF-EFFDT        PIC  X(10).            CPWSXPIF
013700             15  :TAG:-PPPLAF-LOW          PIC  X(05).            CPWSXPIF
013800             15  :TAG:-PPPLAF-HIGH         PIC  X(05).            CPWSXPIF
013900         10  :TAG:-PPPLAT-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
014000             15  :TAG:-PPPLAT-TITLE-TYPE   PIC  X(01).            CPWSXPIF
014100             15  :TAG:-PPPLAT-TITLE-UNIT   PIC  X(02).            CPWSXPIF
014200             15  :TAG:-PPPLAT-AREP         PIC  X(01).            CPWSXPIF
014300             15  :TAG:-PPPLAT-SHC          PIC  X(01).            CPWSXPIF
014400             15  :TAG:-PPPLAT-DUC          PIC  X(01).            CPWSXPIF
014500             15  :TAG:-PPPLAT-EFFDT        PIC  X(10).            CPWSXPIF
014600             15  FILLER                    PIC  X(10).            CPWSXPIF
014700         10  :TAG:-PPPLRR-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
014800             15  :TAG:-PPPLRR-SCHED-NO     PIC S9(03) COMP-3.     CPWSXPIF
014900             15  :TAG:-PPPLRR-PLN          PIC  X(01).            CPWSXPIF
015000             15  :TAG:-PPPLRR-PAY-CYCLE    PIC  X(01).            CPWSXPIF
015100             15  :TAG:-PPPLRR-TYPE         PIC  X(01).            CPWSXPIF
015200             15  :TAG:-PPPLRR-EFFDT        PIC  X(10).            CPWSXPIF
015300             15  :TAG:-PPPLRR-RECORD       PIC  X(01).            CPWSXPIF
015400             15  :TAG:-PPPLRR-HRS-EARN     PIC S9(03)V99 COMP-3.  CPWSXPIF
015500             15  FILLER                    PIC  X(07).            CPWSXPIF
015600         10  :TAG:-PPPLRT-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
015700             15  :TAG:-PPPLRT-SCHED-NO     PIC S9(03) COMP-3.     CPWSXPIF
015800             15  :TAG:-PPPLRT-PLN          PIC  X(01).            CPWSXPIF
015900             15  :TAG:-PPPLRT-PAY-CYCLE    PIC  X(01).            CPWSXPIF
016000             15  :TAG:-PPPLRT-TYPE         PIC  X(01).            CPWSXPIF
016100             15  :TAG:-PPPLRT-EFFDT        PIC  X(10).            CPWSXPIF
016200             15  FILLER                    PIC  X(11).            CPWSXPIF
016300         10  :TAG:-PPPPRM-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
016400             15  :TAG:-PPPPRM-PRM-NO       PIC S9(04) COMP.       CPWSXPIF
016500             15  FILLER                    PIC  X(24).            CPWSXPIF
016600         10  :TAG:-PPPTTL-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXPIF
016700             15  :TAG:-PPPTTL-TITLE        PIC  X(04).            CPWSXPIF
016800             15  FILLER                    PIC  X(22).            CPWSXPIF
017600         10  :TAG:-PPPLUC-KEY REDEFINES :TAG:-CONTROL-KEY.        36290724
017700             15  :TAG:-PPPLUC-LUC          PIC  X(06).            36290724
017800             15  FILLER                    PIC  X(20).            36290724
016900*****  RETURN INFORMATION ************************************    CPWSXPIF
017000       10  :TAG:-RETURN-KEY.                                      CPWSXPIF
017100           15  :TAG:-RETURN-CTL-KEY        PIC  X(26).            CPWSXPIF
017200       10  :TAG:-RETURN-STATUS.                                   CPWSXPIF
017300           15  :TAG:-COMPLETION-CODE       PIC  X(01).            CPWSXPIF
017400               88  :TAG:-OK                VALUE '0'.             CPWSXPIF
017500               88  :TAG:-INVALID-KEY       VALUE '1'.             CPWSXPIF
017600               88  :TAG:-DB2-ERROR         VALUE '2'.             CPWSXPIF
017700               88  :TAG:-INVALID-REQUEST   VALUE '3'.             CPWSXPIF
017800               88  :TAG:-INVALID-TABLE     VALUE '4'.             CPWSXPIF
017900               88  :TAG:-OTHER-ERROR       VALUE '9'.             CPWSXPIF
018000           15  :TAG:-DB2-MSG-NUMBER        PIC S9(03).            CPWSXPIF
018100*------------------------------------------------------------*    CPWSXPIF
018200*------------------   END OF CPWSXPIF    --------------------*    CPWSXPIF
018300*------------------------------------------------------------*    CPWSXPIF
