000000**************************************************************/   69321592
000001*  COPYMEMBER: CPWSRNGL                                      */   69321592
000002*  RELEASE: ___1592______ SERVICE REQUEST(S): ____16932____  */   69321592
000003*  NAME:_______QUAN______ CREATION DATE:      ___06/16/04__  */   69321592
000004*  DESCRIPTION:                                              */   69321592
000005*  - New copy member containing NACHA GTN Link Array.        */   69321592
000007**************************************************************/   69321592
000008*    COPYID=CPWSRNGL                                              CPWSRNGL
000009*01  XNGO-NACHA-GTN-LNK-ARRAY.                                    CPWSRNGL
000051*                                                                 CPWSRNGL
000051*--> Fields referenced in application programs                    CPWSRNGL
000014     03  XNGO-MAX-LNK-ENTRIES-ALLOWED  PIC S9(4)  COMP SYNC.      CPWSRNGL
000020     03  XNGO-MAX-LNK-ENTRIES-LOADED   PIC S9(4)  COMP SYNC.      CPWSRNGL
000051*                                                                 CPWSRNGL
006700     03  XNGO-ORG-LINKS-IN-USE         PIC 9(04) COMP.            CPWSRNGL
000051*                                                                 CPWSRNGL
000020     03  XNGO-GTN-ON-DDG-TABLE-SW      PIC X(01).                 CPWSRNGL
000070         88  XNGO-GTN-ON-DDG-TABLE          VALUE 'Y'.            CPWSRNGL
000051*                                                                 CPWSRNGL
000030*****************************************************************/CPWSRNGL
000040*     -  Array of NACHA Organization GTN/Link data from PPPNGL. * CPWSRNGL
000050*****************************************************************/CPWSRNGL
009800                                                                  CPWSRNGL
006000******************************************************************CPWSRNGL
006640*    Organization GTN/Link Pairs defined on PPPNGL Table         *CPWSRNGL
006000******************************************************************CPWSRNGL
009710     03  XNGO-GTN-ORG-LINK-ROWS-NGL.                              CPWSRNGL
009800         07  XNGO-GTN-ORG-LINK-ITEMS                              CPWSRNGL
010040                         OCCURS 0 TO 99  TIMES                    CPWSRNGL
010050                         DEPENDING ON XNGO-MAX-LNK-ENTRIES-LOADED CPWSRNGL
010060                         INDEXED BY XNGO-LINK-INDEX.              CPWSRNGL
010100             09  XNGO-GTN            PIC 9(03).                   CPWSRNGL
010100             09  XNGO-GTN-X  REDEFINES                            CPWSRNGL
010100                 XNGO-GTN            PIC X(03).                   CPWSRNGL
015126             09  XNGO-ORG-ROW-LINK   pic 9(04) COMP.              CPWSRNGL
010200             09  XNGO-DDG-GTN        PIC X(01).                   CPWSRNGL
