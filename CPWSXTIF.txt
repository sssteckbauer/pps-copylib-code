000000**************************************************************/   28201068
000001*  COPYMEMBER: CPWSXTIF                                      */   28201068
000002*  RELEASE: ___1068______ SERVICE REQUEST(S): ____12820____  */   28201068
000003*  NAME:______M SANO_____ MODIFICATION DATE:  ___06/14/96__  */   28201068
000004*  DESCRIPTION:                                              */   28201068
000005*   ADDED BARGAINING UNIT LEAVE TABLE (BUL) KEY              */   28201068
000007**************************************************************/   28201068
000100**************************************************************/   36290724
000200*  COPYMEMBER: CPWSXTIF                                      */   36290724
000300*  RELEASE: ____0724____  SERVICE REQUEST(S): ____3629____   */   36290724
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __12/11/92__   */   36290724
000500*  DESCRIPTION:                                              */   36290724
000600*   ADDED LAYOFF UNIT CODE TABLE (LUC) KEY                   */   36290724
000700**************************************************************/   36290724
000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSXTIF                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    INTERFACE AREA FOR THE CONTROL DATA BASE QUERY MODULE   */   36330704
000700*                                                            */   36330704
000800*                                                            */   36330704
000900**************************************************************/   36330704
001000*------------------------------------------------------------*    CPWSXTIF
001100*-----------------------  CPWSXTIF  -------------------------*    CPWSXTIF
001200*------------------------------------------------------------*    CPWSXTIF
001300*01  CPWSXTIF.                                                    CPWSXTIF
001400*****  REQUEST INFORMATION ***********************************    CPWSXTIF
001500     05  :TAG:-INTERFACE-AREA.                                    CPWSXTIF
001600         10  :TAG:-TABLE-REQUEST           PIC  X(03).            CPWSXTIF
001700         10  :TAG:-PROCESSING-REQUEST      PIC  X(01).            CPWSXTIF
001800             88  :TAG:-SELECT-REQUEST      VALUE 'S'.             CPWSXTIF
001900             88  :TAG:-UPDATE-REQUEST      VALUE 'U'.             CPWSXTIF
002000             88  :TAG:-INSERT-REQUEST      VALUE 'I'.             CPWSXTIF
002100             88  :TAG:-DELETE-REQUEST      VALUE 'D'.             CPWSXTIF
002200         10  :TAG:-DATE.                                          CPWSXTIF
002300             15  :TAG:-DATE-CC             PIC  X(02).            CPWSXTIF
002400             15  :TAG:-DATE-YY             PIC  X(02).            CPWSXTIF
002500             15  :TAG:-DATE-FILL-1         PIC  X(01).            CPWSXTIF
002600             15  :TAG:-DATE-MM             PIC  X(02).            CPWSXTIF
002700             15  :TAG:-DATE-FILL-2         PIC  X(01).            CPWSXTIF
002800             15  :TAG:-DATE-DD             PIC  X(02).            CPWSXTIF
002900         10  :TAG:-TIME.                                          CPWSXTIF
003000             15  :TAG:-TIME-HH             PIC  X(02).            CPWSXTIF
003100             15  :TAG:-TIME-FILL-1         PIC  X(01).            CPWSXTIF
003200             15  :TAG:-TIME-MM             PIC  X(02).            CPWSXTIF
003300             15  :TAG:-TIME-FILL-2         PIC  X(01).            CPWSXTIF
003400             15  :TAG:-TIME-SS             PIC  X(02).            CPWSXTIF
003500         10  :TAG:-CONTROL-KEY             PIC  X(26).            CPWSXTIF
003600         10  :TAG:-PPPBRA-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
003700             15  :TAG:-PPPBRA-CBUC         PIC  X(02).            CPWSXTIF
003800             15  :TAG:-PPPBRA-REP          PIC  X(01).            CPWSXTIF
003900             15  :TAG:-PPPBRA-SHC          PIC  X(01).            CPWSXTIF
004000             15  :TAG:-PPPBRA-DUC          PIC  X(01).            CPWSXTIF
004100             15  :TAG:-PPPBRA-PRIN-SUM     PIC S9(03) COMP-3.     CPWSXTIF
004200             15  FILLER                    PIC  X(19).            CPWSXTIF
004300         10  :TAG:-PPPBRD-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
004400             15  :TAG:-PPPBRD-CBUC         PIC  X(02).            CPWSXTIF
004500             15  :TAG:-PPPBRD-REP          PIC  X(01).            CPWSXTIF
004600             15  :TAG:-PPPBRD-SHC          PIC  X(01).            CPWSXTIF
004700             15  :TAG:-PPPBRD-DUC          PIC  X(01).            CPWSXTIF
004800             15  :TAG:-PPPBRD-PLN          PIC  X(02).            CPWSXTIF
004900             15  :TAG:-PPPBRD-CVG          PIC  X(03).            CPWSXTIF
005000             15  FILLER                    PIC  X(16).            CPWSXTIF
005100         10  :TAG:-PPPBRE-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
005200             15  :TAG:-PPPBRE-CBUC         PIC  X(02).            CPWSXTIF
005300             15  :TAG:-PPPBRE-REP          PIC  X(01).            CPWSXTIF
005400             15  :TAG:-PPPBRE-SHC          PIC  X(01).            CPWSXTIF
005500             15  :TAG:-PPPBRE-DUC          PIC  X(01).            CPWSXTIF
005600             15  :TAG:-PPPBRE-MIN-AGE      PIC S9(04) COMP.       CPWSXTIF
005700             15  FILLER                    PIC  X(19).            CPWSXTIF
005800         10  :TAG:-PPPBRG-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
005900             15  :TAG:-PPPBRG-CBUC         PIC  X(02).            CPWSXTIF
006000             15  :TAG:-PPPBRG-REP          PIC  X(01).            CPWSXTIF
006100             15  :TAG:-PPPBRG-SHC          PIC  X(01).            CPWSXTIF
006200             15  :TAG:-PPPBRG-DUC          PIC  X(01).            CPWSXTIF
006300             15  :TAG:-PPPBRG-GTN-NO       PIC  X(03).            CPWSXTIF
006400             15  FILLER                    PIC  X(18).            CPWSXTIF
006500         10  :TAG:-PPPBRH-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
006600             15  :TAG:-PPPBRH-CBUC         PIC  X(02).            CPWSXTIF
006700             15  :TAG:-PPPBRH-REP          PIC  X(01).            CPWSXTIF
006800             15  :TAG:-PPPBRH-SHC          PIC  X(01).            CPWSXTIF
006900             15  :TAG:-PPPBRH-DUC          PIC  X(01).            CPWSXTIF
007000             15  :TAG:-PPPBRH-PLN          PIC  X(02).            CPWSXTIF
007100             15  :TAG:-PPPBRH-CVG          PIC  X(03).            CPWSXTIF
007200             15  FILLER                    PIC  X(16).            CPWSXTIF
007300         10  :TAG:-PPPBRJ-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
007400             15  :TAG:-PPPBRJ-CBUC         PIC  X(02).            CPWSXTIF
007500             15  :TAG:-PPPBRJ-REP          PIC  X(01).            CPWSXTIF
007600             15  :TAG:-PPPBRJ-SHC          PIC  X(01).            CPWSXTIF
007700             15  :TAG:-PPPBRJ-DUC          PIC  X(01).            CPWSXTIF
007800             15  :TAG:-PPPBRJ-PLN          PIC  X(02).            CPWSXTIF
007900             15  :TAG:-PPPBRJ-CVG          PIC  X(03).            CPWSXTIF
008000             15  FILLER                    PIC  X(16).            CPWSXTIF
008100         10  :TAG:-PPPBRL-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
008200             15  :TAG:-PPPBRL-CBUC         PIC  X(02).            CPWSXTIF
008300             15  :TAG:-PPPBRL-REP          PIC  X(01).            CPWSXTIF
008400             15  :TAG:-PPPBRL-SHC          PIC  X(01).            CPWSXTIF
008500             15  :TAG:-PPPBRL-DUC          PIC  X(01).            CPWSXTIF
008600             15  :TAG:-PPPBRL-MIN-AGE      PIC S9(04) COMP.       CPWSXTIF
008700             15  FILLER                    PIC  X(19).            CPWSXTIF
008800         10  :TAG:-PPPBRO-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
008900             15  :TAG:-PPPBRO-CBUC         PIC  X(02).            CPWSXTIF
009000             15  :TAG:-PPPBRO-REP          PIC  X(01).            CPWSXTIF
009100             15  :TAG:-PPPBRO-SHC          PIC  X(01).            CPWSXTIF
009200             15  :TAG:-PPPBRO-DUC          PIC  X(01).            CPWSXTIF
009300             15  :TAG:-PPPBRO-PLN          PIC  X(02).            CPWSXTIF
009400             15  :TAG:-PPPBRO-CVG          PIC  X(03).            CPWSXTIF
009500             15  FILLER                    PIC  X(16).            CPWSXTIF
009600         10  :TAG:-PPPBRR-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
009700             15  :TAG:-PPPBRR-CBUC         PIC  X(02).            CPWSXTIF
009800             15  :TAG:-PPPBRR-REP          PIC  X(01).            CPWSXTIF
009900             15  :TAG:-PPPBRR-SHC          PIC  X(01).            CPWSXTIF
010000             15  :TAG:-PPPBRR-DUC          PIC  X(01).            CPWSXTIF
010100             15  :TAG:-PPPBRR-ENTRY-NO     PIC S9(04) COMP.       CPWSXTIF
010200             15  FILLER                    PIC  X(19).            CPWSXTIF
010300         10  :TAG:-PPPBUB-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
010400             15  :TAG:-PPPBUB-BUC          PIC  X(02).            CPWSXTIF
010500             15  :TAG:-PPPBUB-REP          PIC  X(01).            CPWSXTIF
010600             15  :TAG:-PPPBUB-SHC          PIC  X(01).            CPWSXTIF
010700             15  :TAG:-PPPBUB-DIST         PIC  X(01).            CPWSXTIF
010800             15  :TAG:-PPPBUB-TYPE         PIC  X(01).            CPWSXTIF
010900             15  :TAG:-PPPBUB-PLN          PIC  X(02).            CPWSXTIF
011000             15  FILLER                    PIC  X(18).            CPWSXTIF
011100         10  :TAG:-PPPBUD-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
011200             15  :TAG:-PPPBUD-BUC          PIC  X(02).            CPWSXTIF
011300             15  :TAG:-PPPBUD-DIST         PIC  X(01).            CPWSXTIF
011400             15  :TAG:-PPPBUD-ACCT-1       PIC  X(06).            CPWSXTIF
011500             15  :TAG:-PPPBUD-ACCT-2       PIC  X(06).            CPWSXTIF
011600             15  FILLER                    PIC  X(11).            CPWSXTIF
011700         10  :TAG:-PPPBUG-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
011800             15  :TAG:-PPPBUG-BUC          PIC  X(02).            CPWSXTIF
011900             15  :TAG:-PPPBUG-REP          PIC  X(01).            CPWSXTIF
012000             15  :TAG:-PPPBUG-SHC          PIC  X(01).            CPWSXTIF
012100             15  :TAG:-PPPBUG-DIST         PIC  X(01).            CPWSXTIF
012200             15  :TAG:-PPPBUG-GTN-NO       PIC  X(03).            CPWSXTIF
012300             15  FILLER                    PIC  X(18).            CPWSXTIF
013010         10  :TAG:-PPPBUL-KEY REDEFINES :TAG:-CONTROL-KEY.        28201068
013020             15  :TAG:-PPPBUL-BUC          PIC  X(02).            28201068
013030             15  :TAG:-PPPBUL-REP          PIC  X(01).            28201068
013040             15  :TAG:-PPPBUL-SHC          PIC  X(01).            28201068
013050             15  :TAG:-PPPBUL-DIST         PIC  X(01).            28201068
013060             15  :TAG:-PPPBUL-LEAVE-CODE   PIC  X(01).            28201068
013080             15  FILLER                    PIC  X(20).            28201068
012400         10  :TAG:-PPPBUS-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
012500             15  :TAG:-PPPBUS-BUC          PIC  X(02).            CPWSXTIF
012600             15  :TAG:-PPPBUS-SHC          PIC  X(01).            CPWSXTIF
012700             15  :TAG:-PPPBUS-DIST         PIC  X(01).            CPWSXTIF
012800             15  FILLER                    PIC  X(22).            CPWSXTIF
012900         10  :TAG:-PPPBUT-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
013000             15  :TAG:-PPPBUT-BUC          PIC  X(02).            CPWSXTIF
013100             15  FILLER                    PIC  X(24).            CPWSXTIF
013200         10  :TAG:-PPPDOS-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
013300             15  :TAG:-PPPDOS-EARN-TYPE    PIC  X(03).            CPWSXTIF
013400             15  FILLER                    PIC  X(23).            CPWSXTIF
013500         10  :TAG:-PPPGTN-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
013600             15  :TAG:-PPPGTN-NO           PIC  X(03).            CPWSXTIF
013700             15  FILLER                    PIC  X(23).            CPWSXTIF
013800         10  :TAG:-PPPHME-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
013900             15  :TAG:-PPPHME-DEPT         PIC  X(06).            CPWSXTIF
014000             15  :TAG:-PPPHME-LOC          PIC  X(02).            CPWSXTIF
014100             15  FILLER                    PIC  X(18).            CPWSXTIF
014200         10  :TAG:-PPPLAF-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
014300             15  :TAG:-PPPLAF-TITLE-TYPE   PIC  X(01).            CPWSXTIF
014400             15  :TAG:-PPPLAF-TITLE-UNIT   PIC  X(02).            CPWSXTIF
014500             15  :TAG:-PPPLAF-AREP         PIC  X(01).            CPWSXTIF
014600             15  :TAG:-PPPLAF-SHC          PIC  X(01).            CPWSXTIF
014700             15  :TAG:-PPPLAF-DUC          PIC  X(01).            CPWSXTIF
014800             15  :TAG:-PPPLAF-EFFDT        PIC  X(10).            CPWSXTIF
014900             15  :TAG:-PPPLAF-LOW          PIC  X(05).            CPWSXTIF
015000             15  :TAG:-PPPLAF-HIGH         PIC  X(05).            CPWSXTIF
015100         10  :TAG:-PPPLAT-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
015200             15  :TAG:-PPPLAT-TITLE-TYPE   PIC  X(01).            CPWSXTIF
015300             15  :TAG:-PPPLAT-TITLE-UNIT   PIC  X(02).            CPWSXTIF
015400             15  :TAG:-PPPLAT-AREP         PIC  X(01).            CPWSXTIF
015500             15  :TAG:-PPPLAT-SHC          PIC  X(01).            CPWSXTIF
015600             15  :TAG:-PPPLAT-DUC          PIC  X(01).            CPWSXTIF
015700             15  :TAG:-PPPLAT-EFFDT        PIC  X(10).            CPWSXTIF
015800             15  FILLER                    PIC  X(10).            CPWSXTIF
015900         10  :TAG:-PPPLRR-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
016000             15  :TAG:-PPPLRR-SCHED-NO     PIC S9(03) COMP-3.     CPWSXTIF
016100             15  :TAG:-PPPLRR-PLN          PIC  X(01).            CPWSXTIF
016200             15  :TAG:-PPPLRR-PAY-CYCLE    PIC  X(01).            CPWSXTIF
016300             15  :TAG:-PPPLRR-TYPE         PIC  X(01).            CPWSXTIF
016400             15  :TAG:-PPPLRR-EFFDT        PIC  X(10).            CPWSXTIF
016500             15  :TAG:-PPPLRR-RECORD       PIC  X(01).            CPWSXTIF
016600             15  :TAG:-PPPLRR-HRS-EARN     PIC S9(03)V99 COMP-3.  CPWSXTIF
016700             15  FILLER                    PIC  X(07).            CPWSXTIF
016800         10  :TAG:-PPPLRT-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
016900             15  :TAG:-PPPLRT-SCHED-NO     PIC S9(03) COMP-3.     CPWSXTIF
017000             15  :TAG:-PPPLRT-PLN          PIC  X(01).            CPWSXTIF
017100             15  :TAG:-PPPLRT-PAY-CYCLE    PIC  X(01).            CPWSXTIF
017200             15  :TAG:-PPPLRT-TYPE         PIC  X(01).            CPWSXTIF
017300             15  :TAG:-PPPLRT-EFFDT        PIC  X(10).            CPWSXTIF
017400             15  FILLER                    PIC  X(11).            CPWSXTIF
017500         10  :TAG:-PPPPRM-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
017600             15  :TAG:-PPPPRM-PRM-NO       PIC S9(04) COMP.       CPWSXTIF
017700             15  FILLER                    PIC  X(24).            CPWSXTIF
017800         10  :TAG:-PPPTTL-KEY REDEFINES :TAG:-CONTROL-KEY.        CPWSXTIF
017900             15  :TAG:-PPPTTL-TITLE        PIC  X(04).            CPWSXTIF
018000             15  FILLER                    PIC  X(22).            CPWSXTIF
018800         10  :TAG:-PPPLUC-KEY REDEFINES :TAG:-CONTROL-KEY.        36290724
018900             15  :TAG:-PPPLUC-LUC          PIC  X(06).            36290724
019000             15  FILLER                    PIC  X(20).            36290724
018100*****  RETURN INFORMATION ************************************    CPWSXTIF
018200       10  :TAG:-RETURN-KEY.                                      CPWSXTIF
018300           15  :TAG:-RETURN-DATE           PIC  X(10).            CPWSXTIF
018400           15  :TAG:-RETURN-TIME           PIC  X(08).            CPWSXTIF
018500           15  :TAG:-RETURN-CTL-KEY        PIC  X(26).            CPWSXTIF
018600       10  :TAG:-RETURN-STATUS.                                   CPWSXTIF
018700           15  :TAG:-COMPLETION-CODE       PIC  X(01).            CPWSXTIF
018800               88  :TAG:-OK                VALUE '0'.             CPWSXTIF
018900               88  :TAG:-INVALID-KEY       VALUE '1'.             CPWSXTIF
019000               88  :TAG:-DB2-ERROR         VALUE '2'.             CPWSXTIF
019100               88  :TAG:-INVALID-REQUEST   VALUE '3'.             CPWSXTIF
019200               88  :TAG:-INVALID-TABLE     VALUE '4'.             CPWSXTIF
019300               88  :TAG:-OTHER-ERROR       VALUE '9'.             CPWSXTIF
019400           15  :TAG:-DB2-MSG-NUMBER        PIC S9(03).            CPWSXTIF
019500*------------------------------------------------------------*    CPWSXTIF
019600*------------------   END OF CPWSXTIF    --------------------*    CPWSXTIF
019700*------------------------------------------------------------*    CPWSXTIF
