000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPLNKESU                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/27/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000800*=                                                        =%      UCSD0102
000900*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/  *36280739
000200*  COPYMEMBER: CPLNKESU                                      */  *36280739
000300*  RELEASE: ____0739____  SERVICE REQUEST(S): ____3628B___   */  *36280739
000400*  NAME: ______JLT______  CREATION DATE: _______02/04/93__   */  *36280739
000500*  DESCRIPTION:                                              */  *36280739
000600*  - PPBENESP CONTROL LINKAGE                                */  *36280739
000700**************************************************************/  *36280739
000800******************************************************************CPLNKESU
000900*      LINKAGE AREA FOR MODULE  P P B E N E S P                   CPLNKESU
001000******************************************************************CPLNKESU
001100*01* EMPL-SUPP-RATE-TABLES.                                       CPLNKESU
001200*                                                                 CPLNKESU
001300     03  ESP-INDXS-MAX                 PIC S9(4) COMP.            CPLNKESU
001400     03  EMPL-SUPP-PASSED-IN.                                     CPLNKESU
001500         05  ESP-TYPE-CALL             PIC X(1).                  CPLNKESU
001500             88  ESP-LOAD-CALL       VALUE 'L'.                   CPLNKESU
001500             88  ESP-FIND-RATE-CALL  VALUE 'F'.                   CPLNKESU
001500         05  ESP-SRCH-LOC              PIC X(1).                  CPLNKESU
001600         05  ESP-SRCH-ACCT             PIC X(6).                  CPLNKESU
001700*****    05  ESP-SRCH-FUND             PIC X(5).                  UCSD0102
001700         05  ESP-SRCH-FUND             PIC X(6).                  UCSD0102
001800     03  EMPL-SUPP-RETURNED-OUT.                                  CPLNKESU
001900         05  ESP-INDX1                 PIC S9(4) COMP.            CPLNKESU
002000         05  ESP-INDX2                 PIC S9(4) COMP.            CPLNKESU
002100         05  ESP-INDX3                 PIC S9(4) COMP.            CPLNKESU
002200         05  ESP-INDX4                 PIC S9(4) COMP.            CPLNKESU
002200         05  ESP-SUB                   PIC S9(4) COMP.            CPLNKESU
002300         05  ESP-TBL-RATE              PIC 9V9(4).                CPLNKESU
002400         05  ESP-ERROR-CODE            PIC 9.                     CPLNKESU
002500*  *      ERROR CODE '0' INDICATES NORMAL RETURN                  CPLNKESU
002600*  *      ERROR CODE '1' INDICATES DEFAULT RATES USED             CPLNKESU
002700*  *      ERROR CODE '2' INDICATES A NEGATIVE SQL ERROR           CPLNKESU
002800*  *      ERROR CODE '3' INDICATES NO DEFAULT RATES               CPLNKESU
002900*  *      ERROR CODE '4' INDICATES WRK TBL SIZE MUST BE INCREASED CPLNKESU
003000     03  EMPL-SUPP-TABLE1.                                        CPLNKESU
003100         05  ESP-ENTRIES1                 PIC 999.                CPLNKESU
003200         05  EMPL-SUP1           OCCURS  100   TIMES.             CPLNKESU
003300             10  ESP-LOC1                 PIC X(01).              CPLNKESU
003400             10  ESP-ACCT-FUND-AREA1.                             CPLNKESU
003500                 15  FILLER               PIC X(12).              CPLNKESU
003600             10  ESP-RATE1                PIC 9(01)V9(04).        CPLNKESU
003700             10  ESP-TYP1-AMT             PIC S9(07)V9(02).       CPLNKESU
003800     03  EMPL-SUPP-TABLE2.                                        CPLNKESU
003900         05  ESP-ENTRIES2                 PIC 999.                CPLNKESU
004000         05  EMPL-SUP2           OCCURS  100   TIMES.             CPLNKESU
004100             10  ESP-LOC2                 PIC X(01).              CPLNKESU
004200             10  ESP-ACCT-FUND-AREA2.                             CPLNKESU
004300                 15  ESP-LO-ACCT          PIC X(06).              CPLNKESU
004400                 15  ESP-HI-ACCT          PIC X(06).              CPLNKESU
004500             10  ESP-RATE2                PIC 9(01)V9(04).        CPLNKESU
004600             10  ESP-TYP2-AMT             PIC S9(07)V9(02).       CPLNKESU
004700     03  EMPL-SUPP-TABLE3.                                        CPLNKESU
004800         05  ESP-ENTRIES3                 PIC 999.                CPLNKESU
004900         05  EMPL-SUP3           OCCURS  100   TIMES.             CPLNKESU
005000             10  ESP-LOC3                 PIC X(01).              CPLNKESU
005100             10  ESP-ACCT-FUND-AREA3.                             CPLNKESU
005200*****            15  ESP-LO-FUND          PIC X(05).              UCSD0102
005300*****            15  FILLER               PIC X(01).              UCSD0102
005400*****            15  ESP-HI-FUND          PIC X(05).              UCSD0102
005500*****            15  FILLER               PIC X(01).              UCSD0102
005200                 15  ESP-LO-FUND          PIC X(06).              UCSD0102
005400                 15  ESP-HI-FUND          PIC X(06).              UCSD0102
005600             10  ESP-RATE3                PIC 9(01)V9(04).        CPLNKESU
005700             10  ESP-TYP3-AMT             PIC S9(07)V9(02).       CPLNKESU
005800     03  EMPL-SUPP-TABLE4.                                        CPLNKESU
005900         05  ESP-ENTRIES4                 PIC 999.                CPLNKESU
006000         05  EMPL-SUP4           OCCURS  100   TIMES.             CPLNKESU
006100             10  ESP-LOC4                 PIC X(01).              CPLNKESU
006200             10  ESP-ACCT-FUND-AREA4.                             CPLNKESU
006300                 15  ESP-ACCT             PIC X(06).              CPLNKESU
006400                 15  ESP-FUND             PIC X(06).              UCSD0102
006400*****            15  ESP-FUND             PIC X(05).              UCSD0102
006500*****            15  FILLER               PIC X(01).              UCSD0102
006600             10  ESP-RATE4                PIC 9(01)V9(04).        CPLNKESU
006700             10  ESP-TYP4-AMT             PIC S9(07)V9(02).       CPLNKESU
006800     03  ESP-PRINT-TABLE.                                         CPLNKESU
006900         05  ESP-PRINT-TABLE-MAX          PIC  9(04).             CPLNKESU
007000         05  ESP-PRINT-TOTAL              PIC S9(09)V9(02).       CPLNKESU
007100         05  EMPL-SUPPRT          OCCURS  100  TIMES.             CPLNKESU
007200             10  ESP-KEY.                                         CPLNKESU
007300                 15  ESP-KEY-LOCATION     PIC  X(01).             CPLNKESU
007400                 15  ESP-KEY-TYPE         PIC  X(01).             CPLNKESU
007500                 15  ESP-KEY-SEQUENCE     PIC  X(01).             CPLNKESU
007600             10  ESP-TABLE-SUB            PIC S9(04)  COMP.       CPLNKESU
007700*                                                                 CPLNKESU
007800************   END OF COPY MEMBER  ****************************** CPLNKESU
