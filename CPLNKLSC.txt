000100**************************************************************/   36290827
000200*  COPYMEMBER: CPLNKLSC                                      */   36290827
000300*  RELEASE: ____0827____  SERVICE REQUEST(S): ____3629____   */   36290827
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __10/21/93__   */   36290827
000500*  DESCRIPTION:                                              */   36290827
000600*   LINKAGE FOR PPLSCUTL SUBROUTINE CREATED FOR DB2 EDB      */   36290827
000700******************************************************************CPLNKLSC
000800*    COPYID=CPLNKLSC                                              CPLNKLSC
000900******************************************************************CPLNKLSC
001000*01  PPLSCUTL-INTERFACE.                                          CPLNKLSC
001100*                                                                *CPLNKLSC
001200******************************************************************CPLNKLSC
001300*    P P L S C U T L   I N T E R F A C E                         *CPLNKLSC
001400******************************************************************CPLNKLSC
001500*                                                                *CPLNKLSC
001600     05  PPLSCUTL-ERROR-SW       PICTURE  X(01).                  CPLNKLSC
001700         88  PPLSCUTL-ERROR                          VALUE 'Y'.   CPLNKLSC
001800     05  PPLSCUTL-EMPLOYEE-ID    PICTURE  X(09).                  CPLNKLSC
001900     05  PPLSCUTL-ROW-COUNT      PICTURE S9(04)      COMP.        CPLNKLSC
002000******************************************************************CPLNKLSC
