000000**************************************************************/   32021138
000001*  COPYMEMBER: CPFDGSRR                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000002*  REVISED: _10/27/97                                        */   32021138
000003*  NAME:_P._THOMPSON_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*  - MODIFIED FOR FULL ACCOUNTING UNIT.                      */   32021138
000007**************************************************************/   32021138
000100******************************************************************36560949
000200*  COPYMEMBER: CPFDGSRR                                          *36560949
000300*  RELEASE: ____0949____  SERVICE REQUEST(S): ____3656____       *36560949
000400*  NAME:    __JYL_______  MODIFICATION DATE:  __12/14/94__       *36560949
000500*  DESCRIPTION:                                                  *36560949
000600*   FD STATEMENT FRO GRADUATE STUDENT FILE                       *36560949
000700******************************************************************36560949
000800*    COPYID=CPFDGSRR                                              CPFDGSRR
000900     BLOCK CONTAINS 0 RECORDS                                     CPFDGSRR
001000*****RECORD CONTAINS 724  CHARACTERS                              32021138
001010     RECORD CONTAINS 740  CHARACTERS                              32021138
001100     RECORDING MODE IS F                                          CPFDGSRR
001200     LABEL RECORDS ARE STANDARD.                                  CPFDGSRR
001300     SKIP1                                                        CPFDGSRR
001400*01  GRAD-STUDENT-RECS                PIC X(724).                 32021138
001400 01  GRAD-STUDENT-RECS                PIC X(740).                 32021138
