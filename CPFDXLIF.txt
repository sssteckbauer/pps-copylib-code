000100**************************************************************/   45260390
000200*  COPYMEMBER: CPFDXRPT                                      */   45260390
000300*  RELEASE # ____0390____ SERVICE REQUEST NO(S)__4526________*/   45260390
000400*  NAME ______DXJ______   MODIFICATION DATE ____11/13/88_____*/   45260390
000500*                                                            */   45260390
000600*  DESCRIPTION                                               */   45260390
000700*  -NEW COPY MEMBER                                          */   45260390
000800*  -CONTAINS EMPLOYEE IMPUTED INCOME DATA FOR EXECUTIVE LIFE */   45260390
000900*   INSURANCE PROGRAM.                                       */   45260390
001000**************************************************************/   45260390
001100*    COPYID=CPFDXRPT                                              CPFDXRPT
001200     BLOCK  CONTAINS   0 RECORDS                                  CPFDXRPT
001300     RECORD CONTAINS 280 CHARACTERS                               CPFDXRPT
001400     RECORDING  MODE  IS  F                                       CPFDXRPT
001500     LABEL  RECORDS  ARE  STANDARD.                               CPFDXRPT
001600                                                                  CPFDXRPT
001700 01  IMPUTED-INCOME-RECORD               PIC X(280).              CPFDXRPT
