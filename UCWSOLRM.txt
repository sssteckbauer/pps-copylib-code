000000**************************************************************/   48681411
000001*  COPYMEMBER: UCWSOLRM                                      */   48681411
000002*  RELEASE: ___1411______ SERVICE REQUEST(S): ____14868____  */   48681411
000003*  NAME:_______QUAN______ CREATION DATE:      ___04/24/02__  */   48681411
000004*  DESCRIPTION:                                              */   48681411
000005*     OLR DATA FOR EMAIL TRIGGER RECORD (QUEUE NAME OLRQ)    */   48681411
000008**************************************************************/   48681411
000900*01  UCWSOLRM EXTERNAL.                                           UCWSOLRM
001000     05  UCWSOLRM-AREA   PIC X(512).                              UCWSOLRM
001100     05  UCWSOLRM-DATA REDEFINES UCWSOLRM-AREA.                   UCWSOLRM
001300         10  UCWSOLRM-OLRC-EMPLOYEE-ID          PIC X(09).        UCWSOLRM
001301         10  UCWSOLRM-OLRC-EMPLOYEE-NAME        PIC X(26).        UCWSOLRM
001302         10  UCWSOLRM-OLRC-PROCESS-DATE         PIC X(08).        UCWSOLRM
001303         10  UCWSOLRM-OLRC-CHECK-AVAIL-DATE     PIC X(08).        UCWSOLRM
001304         10  UCWSOLRM-OLRC-COMMENT-LINE1        PIC X(72).        UCWSOLRM
001305         10  UCWSOLRM-OLRC-COMMENT-LINE2        PIC X(72).        UCWSOLRM
001306         10  UCWSOLRM-OLRC-COMMENT-LINE3        PIC X(72).        UCWSOLRM
001307         10  UCWSOLRM-OLRC-EMAIL-ADDRESS        PIC X(73).        UCWSOLRM
001308         10  UCWSOLRM-OLRC-CHARACTER     REDEFINES                UCWSOLRM
001309             UCWSOLRM-OLRC-EMAIL-ADDRESS        PIC X(01)         UCWSOLRM
001310                                                OCCURS 73 TIMES.  UCWSOLRM
001400         10  FILLER                             PIC X(172).       UCWSOLRM
001500***************    END OF SOURCE - UCWSOLRM    *******************UCWSOLRM
