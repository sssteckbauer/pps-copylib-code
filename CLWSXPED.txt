      *--------------------------------------------------------------
      *  LOCAL COPYMEMBER FOR UCSD
      *
      *--------------------------------------------------------------
      *==========================================================%      UCSDX036
      *=    COPYMEMBER: CLWSXPED                                =%      UCSDX036
      *=    CHANGE #UCSDX036      PROJ. REQUEST: PP6-36         =%      UCSDX036
      *=    NAME: R. MERRYMAN     MODIFICATION DATE: 07/10/16   =%      UCSDX036
      *=                                                        =%      UCSDX036
      *=    DESCRIPTION:                                        =%      UCSDX036
      *=                                                        =%      UCSDX036
      *=    AS PART OF R2230, ADD THE FOLLOWING TWO NEW BENEFIT =%      UCSDX036
      *=    FIELDS AND TWO EXPANSION FIELDS:                    =%      UCSDX036
      *=                                                        =%      UCSDX036
      *=       PED-BEN-2016-SUPPL                               =%      UCSDX036
      *=       PED-BEN-2016-DC                                  =%      UCSDX036
      *=       PED-BEN-EXPANSION-1                              =%      UCSDX036
      *=       PED-BEN-EXPANSION-2                              =%      UCSDX036
      *=                                                        =%      UCSDX036
      *==========================================================%      UCSDX035
      *=    COPYMEMBER: CLWSXPED                                =%      UCSDX035
      *=    CHANGE #UCSDX035      PROJ. REQUEST: PP6-35         =%      UCSDX035
      *=    NAME: L. WANN         MODIFICATION DATE: 10/18/11   =%      UCSDX035
      *=                                                        =%      UCSDX035
      *=    DESCRIPTION:                                        =%      UCSDX035
      *=    ADD PED-BEN-UCRP-SUPP                               =%      UCSDX035
      *=                                                        =%      UCSDX035
000100*==========================================================%      DS1347
000200*=    COPYMEMBER: CLWSXPED                                =%      DS1347
000300*=    CHANGE # DS1347       PROJ. REQUEST: REL 1347       =%      DS1347
000400*=    NAME: G. CHIU         MODIFICATION DATE: 01/23/03   =%      DS1347
000500*=                                                        =%      DS1347
000600*=    DESCRIPTION:                                        =%      DS1347
000500*=    ADD PED-FCSS ELEMENT                                =%      DS1347
000500*=                                                        =%      DS1347
000100*==========================================================%      DS1347
000100*==========================================================%      DS1077
000200*=    COPYMEMBER: CLWSXPED                                =%      DS1077
000300*=    CHANGE # UCSD0127     PROJ. REQUEST: DSE REQUEST    =%      DS1077
000400*=    NAME: G. CHIU         MODIFICATION DATE: 11/11/99   =%      DS1077
000500*=                                                        =%      DS1077
000600*=    DESCRIPTION:                                        =%      DS1077
000500*=    ADD PED-CBUC ELEMENT                                =%      DS1077
000500*=                                                        =%      DS1077
000100*==========================================================%      DS1077
000100*==========================================================%      DS1077
000200*=    COPYMEMBER: CLWSXPED                                =%      DS1077
000300*=    CHANGE # DS1077       PROJ. REQUEST: RELEASE 1077   =%      DS1077
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/29/96   =%      DS1077
000500*=                                                        =%      DS1077
000600*=    DESCRIPTION: 8/29/96                                =%      DS1077
000500*=    ADD PED-IAP TO BENEFIT ARRAY.                       =%      DS1077
000500*=                                                        =%      DS1077
000100*==========================================================%      DS1077
000010*==========================================================%      DT595
000020*=    COPYMEMBER: CLWSXPED                                =%      DT595
000030*=    CHANGE # DT595        PROJ. REQUEST: DT595          =%      DT595
000040*=    NAME: MARI MCGEE      MODIFICATION DATE: 02/06/91   =%      DT595
000050*=                                                        =%      DT595
000060*=    DESCRIPTION:                                        =%      DT595
000070*=     CHANGE  PED-DNTL-ALT, PED-VISN-ALT, PED-UCRS-PLAN7,=%      DT595
000080*=     PED-MGT-2PC  TO FILLER. (OP REL598, CCID REL595)   =%      DT595
000090*==========================================================%      DT595
000100*==========================================================%      DT033
000200*=    COPYMEMBER: CLWSXPED                                =%      DT033
000300*=    CHANGE # DT-033       PROJ. REQUEST: DT-033         =%      DT033
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/30/91   =%      DT033
000500*=                                                        =%      DT033
000600*=    DESCRIPTION:                                        =%      DT033
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/29/96   =%      DT033
000500*=                                                        =%      DT033
000600*=    DESCRIPTION: 8/29/96                                =%      DT033
000500*=    ADD PED-IAP TO OUTPUT FILE XPED (RELEASE 1077).     =%      DT033
000500*=                                                        =%      DT033
000600*=    DESCRIPTION:                                        =%      DT033
000700*=     IFIS DATA ELEMENTS WERE ADDED - INDEX, ORGANIZATION=%      DT033
000800*=     FUND-EXPAND AND PROGRAM.                           =%      DT033
000800*=     REPLACE ACCOUNT-TITLE WITH INDEX-TITLE.            =%      DT033
000900*==========================================================%      DT033
000100*==========================================================%      DS413
000200*=    COPYMEMBER: CLWSXPED                                =%      DS413
000300*=    CHANGE # DS413        PROJ. REQUEST: DS413          =%      DS413
000400*=    NAME: MARI MCGEE      MODIFICATION DATE: 07/19/89   =%      DS413
000500*=                                                        =%      DS413
000600*=    DESCRIPTION:                                        =%      DS413
000700*=     COBOL II CONVERSION                                =%      DS413
000800*=                                                        =%      DS413
000900*==========================================================%      DS413
007500*    COPYID=CLWSXPED                                              CLWSXPED
007600     SKIP1                                                        CLWSXPED
007700*01  PAYROLL-EXP-DIST-RECORD.                                     DS413
007800     03  PED-ID-NO                       PIC X(9).                CLWSXPED
007900     03  PED-NAME                        PIC X(26).               CLWSXPED
007901     03  PED-HOME-DEPT                   PIC X(6).                CLWSXPED
007910     03  PED-RETR-PLN                    PIC X.                   CLWSXPED
007911     03  PED-FICA-ELIG                   PIC X(1).                CLWSXPED
007912     03  PED-LEAVE-ELIG                  PIC X(1).                CLWSXPED
007920     03  FILLER                          PIC X(10).               CLWSXPED
008000     03  PED-ERNG-PP-END-DATE.                                    CLWSXPED
008100         05  PED-ERNG-PP-END-YEAR        PIC 9(2).                CLWSXPED
008200         05  PED-ERNG-PP-END-MONTH       PIC 9(2).                CLWSXPED
008300         05  PED-ERNG-PP-END-DAY         PIC 9(2).                CLWSXPED
008400     03  PED-PAY-CYCLE                   PIC X.                   CLWSXPED
008500     03  FILLER                          PIC X(7).                CLWSXPED
008600     03  PED-KEY-AD-LOCATION.                                     CLWSXPED
008700         05  LOC-FILLER                  PIC X.                   CLWSXPED
008800         05  PED-LOC                     PIC X.                   CLWSXPED
008900     03  PED-ACCT                        PIC X(6).                CLWSXPED
009100     03  PED-FUND                        PIC X(5).                CLWSXPED
009300     03  PED-SUB                         PIC X(1).                CLWSXPED
009400     03  PED-OBJ-CODE                    PIC X(4).                CLWSXPED
004200*    03  FILLER                          PIC X(27).               DT033
004300     03  PED-IFIS-INFO.                                           DT033
004400         05  PED-IFIS-ACCOUNT-INDEX      PIC X(7).                DT033
004500         05  PED-IFIS-ORGANIZATION       PIC X(6).                DT033
004600         05  PED-IFIS-FUND-GROUP.                                 DT033
004700             10  PED-IFIS-FUND             PIC X(5).              DT033
004800             10  PED-IFIS-FUND-EXPANSION   PIC X(1).              DT033
004900         05  PED-IFIS-PROGRAM            PIC X(6).                DT033
005000     03  FILLER                          PIC X(2).                DT033
009600     03  PED-TRANS-END-DATE.                                      CLWSXPED
009700         05  PED-TRANS-END-YR            PIC 9(2).                CLWSXPED
009800         05  PED-TRANS-END-MO            PIC 9(2).                CLWSXPED
009900         05  PED-TRANS-END-DA            PIC 9(2).                CLWSXPED
010000     03  PED-ADJ-CODE                    PIC X.                   CLWSXPED
010100     03  PED-DOS                         PIC X(3).                CLWSXPED
010200     03  PED-TITLE                       PIC X(4).                CLWSXPED
025200     03  PED-TIME-HOURS                  PIC S9(3)V9(2).          CLWSXPED
025300     03  PED-TIME-PERCENT                PIC S9V9(4).             CLWSXPED
025310     03  PED-RATE-TYPE                   PIC X.                   CLWSXPED
025400     03  PED-RATE                        PIC S9(5)V9(4).          CLWSXPED
025700     03  PED-GRS-AMT                     PIC S9(5)V9(2).          CLWSXPED
025720     03  PED-EARN-EMP-REL-CODE           PIC X.                   CLWSXPED
025730     03  PED-EARN-APPT-TYPE-CODE         PIC X.                   CLWSXPED
025740     03  PED-CBUC                        PIC X(2).                UCSD0127
025740     03  FILLER                          PIC X(4).                UCSD0127
025740*****03  FILLER                          PIC X(6).                UCSD0127
025800     03  PED-BENEFITS-AMOUNTS.                                    CLWSXPED
025900         05  PED-RETR-MTCH               PIC S9(5)V9(2).          CLWSXPED
026000         05  PED-FICA                    PIC S9(5)V9(2).          CLWSXPED
026010         05  PED-MEDICR                  PIC S9(5)V9(2).          CLWSXPED
026400         05  PED-HEALTH                  PIC S9(5)V9(2).          CLWSXPED
      *****    05  PED-ANN-HEALTH              PIC S9(5)V9(2).          CLWSXPED
               05  PED-OPEB                    PIC S9(5)V9(2).          CLWSXPED
026600         05  PED-LI                      PIC S9(5)V9(2).          CLWSXPED
026700         05  PED-NDI                     PIC S9(5)V9(2).          CLWSXPED
026800         05  PED-WC                      PIC S9(5)V9(2).          CLWSXPED
026900         05  PED-UI                      PIC S9(5)V9(2).          CLWSXPED
027000         05  PED-DENTAL                  PIC S9(5)V9(2).          CLWSXPED
027400         05  PED-VISION                  PIC S9(5)V9(2).          CLWSXPED
027410         05  PED-LEGAL                   PIC S9(5)V9(2).          CLWSXPED
027700         05  PED-ESP                     PIC S9(5)V9(2).          CLWSXPED
027701         05  PED-CORE-MEDICAL            PIC S9(5)V9(2).          CLWSXPED
027702         05  PED-CORE-LIFE               PIC S9(5)V9(2).          CLWSXPED
027710         05  PED-IAP                     PIC S9(5)V9(2).          DS1077
027710         05  PED-IAP-OFFSET              PIC S9(5)V9(2).          DS1077
027713         05  PED-FCSS                    PIC S9(5)V9(2).          DS1347
      ********
      *  UCSD MOD - JXC - 02-01-05
      *           - ADDED FOUR FIELDS RELATED TO POST DOC (R1615)
      *             AND INCREASED OCCURS ACCORDIANINGLY
      *           - JXC - 08-04-05 - ADDED BEN-ADM (R1653)
      *           - EBT - 11-14-05 - ADDED BEN-SMS (R1670)
      ********
               05  PED-PSBPDIS                 PIC S9(5)V9(2).          JXCR1615
               05  PED-PSBPLIF                 PIC S9(5)V9(2).          JXCR1615
               05  PED-PSBPBRK                 PIC S9(5)V9(2).          JXCR1615
               05  PED-PSBPWCW                 PIC S9(5)V9(2).          JXCR1615
               05  PED-BEN-ADM                 PIC S9(5)V9(2).          JXCR1653
               05  PED-BEN-SMS                 PIC S9(5)V9(2).          EBTR1670
027711*****    05  FILLER                      PIC S9(5)V9(2).          DS1347
      *        05  PED-FILLER1                 PIC S9(5)V9(2).          CLWSXPED
               05  PED-BEN-UCRP-SUPP           PIC S9(5)V9(2).          UCSDX035
               05  PED-BEN-2016-SUPPL          PIC S9(5)V9(2).          UCSDX036
               05  PED-BEN-2016-DC             PIC S9(5)V9(2).          UCSDX036
               05  PED-BEN-EXPANSION-1         PIC S9(5)V9(2).          UCSDX036
               05  PED-BEN-EXPANSION-2         PIC S9(5)V9(2).          UCSDX036
027730         05  PED-TOTAL-BENEFITS          PIC S9(5)V9(2).          CLWSXPED
027800     03  PED-REBENE-AMT          REDEFINES                        CLWSXPED
      *****    PED-BENEFITS-AMOUNTS OCCURS         21  TIMES.           CLWSXPED
      *****    PED-BENEFITS-AMOUNTS OCCURS         25  TIMES.           JXCR1615
      *****    PED-BENEFITS-AMOUNTS OCCURS         26  TIMES.           JXCR1653
               PED-BENEFITS-AMOUNTS OCCURS         30  TIMES.           UCSDX036
028300         05  PED-BENE-AMT                PIC S9(5)V9(2).          CLWSXPED
028600     03  PED-PROCESS-YRMO.                                        CLWSXPED
028700         05  PED-PROCESS-YR              PIC XX.                  CLWSXPED
028800         05  PED-PROCESS-MO              PIC XX.                  CLWSXPED
028900     03  PED-PAGE-COUNT                  PIC 9(5).                CLWSXPED
029000     03  PED-LINE-NO                     PIC 9(2).                CLWSXPED
009600*    03  PED-ACCOUNT-TITLE               PIC X(30).               DT033
009700     03  PED-INDEX-TITLE                 PIC X(30).               DT033
029200     03  PED-FUND-TITLE                  PIC X(30).               CLWSXPED
029300     03  PED-DEPT-TITLE                  PIC X(30).               CLWSXPED
029400     03  PED-TITLE-DESC                  PIC X(30).               CLWSXPED
029500     03  FILLER                          PIC X(10).               CLWSXPED
