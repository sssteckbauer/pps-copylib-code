000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER:                                        =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: D. CHILCOAT     MODIFICATION DATE: 09/09/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000900*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPDEDTM                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_PHIL THOMPSON___ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  BASE FAU RELATED PROCEDURE CODE FOR ONLINE FUNCTION EDTM. */   32021138
000700*  THIS CODE ONLY WORKS WITH THE BASE PROGRAM PPWEDTM AND    */   32021138
000800*  BASE MAP PPEDTM0.                                         */   32021138
000900**************************************************************/   32021138
034300 CPPD-POSITION-CURSOR          SECTION.                           CPPDEDTM
034400******************************************************************CPPDEDTM
034500*  POSITION THE CURSOR AT THE APPROPRIATE PLACE IN THE EDITING   *CPPDEDTM
034600*  PROCESS.                                                      *CPPDEDTM
034700******************************************************************CPPDEDTM
035500*****MOVE -1 TO DE0230-LOCL (2).                                  UCSD0102
035500     MOVE -1 TO DE0230-IFIS-INDEXL (2).                           UCSD0102
035600                                                                  CPPDEDTM
038100 CPPD-INIT-CONSTANT-FIELDS     SECTION.                           CPPDEDTM
038200******************************************************************CPPDEDTM
038300*  SET ATTRIBUTES FOR NON-ENTERABLE FIELDS.                      *CPPDEDTM
038400******************************************************************CPPDEDTM
040500     PERFORM VARYING I FROM 1 BY 1 UNTIL I > 2                    CPPDEDTM
040600         MOVE UCCOMMON-ATR-FLD-LABEL TO L-FAUA (I)                CPPDEDTM
041300         MOVE UCCOMMON-CLR-FLD-LABEL TO L-FAUC (I)                CPPDEDTM
042000         MOVE UCCOMMON-HLT-FLD-LABEL TO L-FAUH (I)                CPPDEDTM
042600     END-PERFORM.                                                 CPPDEDTM
042610                                                                  CPPDEDTM
042900*****MOVE UCCOMMON-ATR-ENTRY-PRO TO  DE0230-LOCA   (1)            UCSD0102
043000*****                                DE0230-ACCTA  (1)            UCSD0102
043100*****                                DE0230-CCA    (1)            UCSD0102
043200*****                                DE0230-FUNDA  (1)            UCSD0102
043300*****                                DE0230-PCA    (1)            UCSD0102
043301     MOVE UCCOMMON-ATR-ENTRY-PRO TO  DE0230-IFIS-INDEXA (1)       UCSD0102
043400                                     DE0230-SUBA   (1).           CPPDEDTM
044000                                                                  CPPDEDTM
044100 CPPD-INIT-ENTRY-FIELDS        SECTION.                           CPPDEDTM
044200******************************************************************CPPDEDTM
044300*  SET ATTRIBUTES FOR ENTERABLE FIELDS.                          *CPPDEDTM
044400******************************************************************CPPDEDTM
044500                                                                  CPPDEDTM
042900*****MOVE UCCOMMON-ATR-ENTRY-PRO TO  DE0230-LOCA   (1)            UCSD0102
043000*****                                DE0230-ACCTA  (1)            UCSD0102
043100*****                                DE0230-CCA    (1)            UCSD0102
043200*****                                DE0230-FUNDA  (1)            UCSD0102
043300*****                                DE0230-PCA    (1)            UCSD0102
043301     MOVE UCCOMMON-ATR-ENTRY-PRO TO  DE0230-IFIS-INDEXA (1)       UCSD0102
045200                                     DE0230-SUBA   (1).           CPPDEDTM
045300                                                                  CPPDEDTM
045400      EVALUATE TRUE                                               CPPDEDTM
045600        WHEN WS-PROTECT-ALL                                       CPPDEDTM
042900           MOVE UCCOMMON-ATR-ENTRY-PRO TO                         CPPDEDTM
216540*****                                DE0230-LOCA   (2)            UCSD0102
216550*****                                DE0230-ACCTA  (2)            UCSD0102
216560*****                                DE0230-CCA    (2)            UCSD0102
216570*****                                DE0230-FUNDA  (2)            UCSD0102
216580*****                                DE0230-PCA    (2)            UCSD0102
216590                                     DE0230-IFIS-INDEXA (2)       UCSD0102
047200                                     DE0230-SUBA   (2)            CPPDEDTM
048700     END-EVALUATE.                                                CPPDEDTM
049900                                                                  CPPDEDTM
050000     PERFORM VARYING I FROM 1 BY 1 UNTIL I > 2                    CPPDEDTM
042900*****  MOVE UCCOMMON-CLR-ENTRY TO DE0230-LOCC   (I)               UCSD0102
043000*****                             DE0230-ACCTC  (I)               UCSD0102
043100*****                             DE0230-CCC    (I)               UCSD0102
043200*****                             DE0230-FUNDC  (I)               UCSD0102
043300*****                             DE0230-PCC    (I)               UCSD0102
043301       MOVE UCCOMMON-CLR-ENTRY TO DE0230-IFIS-INDEXC (I)          UCSD0102
051000                                  DE0230-SUBC (I)                 CPPDEDTM
051500*****  MOVE UCCOMMON-HLT-ENTRY TO DE0230-LOCH (I)                 UCSD0102
051600*****                             DE0230-ACCTH (I)                UCSD0102
051700*****                             DE0230-CCH  (I)                 UCSD0102
051800*****                             DE0230-FUNDH (I)                UCSD0102
051900*****                             DE0230-PCH  (I)                 UCSD0102
043301       MOVE UCCOMMON-HLT-ENTRY TO DE0230-IFIS-INDEXH (I)          UCSD0102
052000                                  DE0230-SUBH (I)                 CPPDEDTM
052100     END-PERFORM.                                                 CPPDEDTM
052700                                                                  CPPDEDTM
052800 CPPD-EDIT-STEP-TOE-OK-ATR     SECTION.                           CPPDEDTM
052900**************************************************************    CPPDEDTM
053000* SET UP ATTRIBUTES ONCE VALID TOE INFO HAS BEEN INPUT       *    CPPDEDTM
053100**************************************************************    CPPDEDTM
053900     MOVE CPWSETWK-DE0230-FAU (2) TO FAUR-FAU.                    CPPDEDTM
053910*****IF FAUR-LOCATION > SPACES                                    UCSD0102
054000*****   MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-LOCA (2)            UCSD0102
054100*****ELSE                                                         UCSD0102
054200*****   MOVE UCCOMMON-ATR-ENTRY TO DE0230-LOCA (2)                UCSD0102
054300*****END-IF.                                                      UCSD0102
054400*****IF FAUR-ACCOUNT > SPACES                                     UCSD0102
054500*****   MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-ACCTA (2)           UCSD0102
054600*****ELSE                                                         UCSD0102
054700*****   MOVE UCCOMMON-ATR-ENTRY TO DE0230-ACCTA (2)               UCSD0102
054800*****END-IF.                                                      UCSD0102
054900*****IF FAUR-COST-CENTER > SPACES                                 UCSD0102
055000*****   MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-CCA (2)             UCSD0102
055100*****ELSE                                                         UCSD0102
055200*****   MOVE UCCOMMON-ATR-ENTRY TO DE0230-CCA (2)                 UCSD0102
055300*****END-IF.                                                      UCSD0102
055400*****IF FAUR-FUND > SPACES                                        UCSD0102
055500*****   MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-FUNDA (2)           UCSD0102
055600*****ELSE                                                         UCSD0102
055700*****   MOVE UCCOMMON-ATR-ENTRY TO DE0230-FUNDA (2)               UCSD0102
055800*****END-IF.                                                      UCSD0102
055900*****IF FAUR-PROJECT-CODE > SPACES                                UCSD0102
056000*****   MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-PCA (2)             UCSD0102
056100*****ELSE                                                         UCSD0102
056200*****   MOVE UCCOMMON-ATR-ENTRY TO DE0230-PCA (2)                 UCSD0102
056300*****END-IF.                                                      UCSD0102
056400     IF FAUR-INDEX > SPACES                                       UCSD0102
056500        MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-IFIS-INDEXA (2)     UCSD0102
056600     ELSE                                                         UCSD0102
056700        MOVE UCCOMMON-ATR-ENTRY TO DE0230-IFIS-INDEXA (2)         UCSD0102
056800     END-IF.                                                      UCSD0102
056400     IF FAUR-SUB-ACCOUNT > SPACES                                 CPPDEDTM
056500        MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-SUBA (2)            CPPDEDTM
056600     ELSE                                                         CPPDEDTM
056700        MOVE UCCOMMON-ATR-ENTRY TO DE0230-SUBA (2)                CPPDEDTM
056800     END-IF.                                                      CPPDEDTM
058400                                                                  CPPDEDTM
058500 CPPD-PROTECT-TOP              SECTION.                           CPPDEDTM
058600**************************************************************    CPPDEDTM
058700*  PROTECT ALL FIELDS IN THE TOP PORTION OF THE SCREEN       *    CPPDEDTM
058800**************************************************************    CPPDEDTM
058900                                                                  CPPDEDTM
059000     MOVE UCCOMMON-ATR-ENTRY-MDT-PRO TO                           CPPDEDTM
059700*****                          DE0230-LOCA   (1)                  CPPDEDTM
059800*****                          DE0230-ACCTA  (1)                  CPPDEDTM
059900*****                          DE0230-CCA    (1)                  CPPDEDTM
060000*****                          DE0230-FUNDA  (1)                  CPPDEDTM
060100*****                          DE0230-PCA    (1)                  CPPDEDTM
060101                               DE0230-IFIS-INDEXA (1)             UCSD0102
060200                               DE0230-SUBA   (1).                 CPPDEDTM
060800                                                                  CPPDEDTM
060900 CPPD-PROTECT-BOTTOM           SECTION.                           CPPDEDTM
061000**************************************************************    CPPDEDTM
061100*  PROTECT ALL FIELDS IN THE BOTTOM PORTION OF THE SCREEN    *    CPPDEDTM
061200**************************************************************    CPPDEDTM
061300                                                                  CPPDEDTM
061400     MOVE UCCOMMON-ATR-ENTRY-PRO TO                               CPPDEDTM
061500*****                          DE0230-LOCA   (2)                  UCSD0102
061600*****                          DE0230-ACCTA  (2)                  UCSD0102
061700*****                          DE0230-CCA    (2)                  UCSD0102
061800*****                          DE0230-FUNDA  (2)                  UCSD0102
061900*****                          DE0230-PCA    (2)                  UCSD0102
217010                               DE0230-IFIS-INDEXA (2)             UCSD0102
062000                               DE0230-SUBA   (2).                 CPPDEDTM
062800                                                                  CPPDEDTM
062900 CPPD-SET-UPD-ATTRIBUTES       SECTION.                           CPPDEDTM
063000**************************************************************    CPPDEDTM
063100*  SET MDTS ON FOR FIELDS WHICH HAVE DATA                    *    CPPDEDTM
063200**************************************************************    CPPDEDTM
063300                                                                  CPPDEDTM
063800     EVALUATE TRUE                                                CPPDEDTM
063900       WHEN CPWSETWK-EDIT-STEP-CYC-OK                             CPPDEDTM
065100         MOVE CPWSETWK-DE0230-FAU (2) TO FAUR-FAU                 CPPDEDTM
065110*****    IF FAUR-LOCATION > SPACES                                UCSD0102
065200*****       MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-LOCA (2)        UCSD0102
065300*****    END-IF                                                   UCSD0102
065400*****    IF FAUR-ACCOUNT > SPACES                                 UCSD0102
065500*****       MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-ACCTA (2)       UCSD0102
065600*****    END-IF                                                   UCSD0102
065700*****    IF FAUR-COST-CENTER > SPACES                             UCSD0102
065800*****       MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-CCA (2)         UCSD0102
065900*****    END-IF                                                   UCSD0102
066000*****    IF FAUR-FUND > SPACES                                    UCSD0102
066100*****       MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-FUNDA (2)       UCSD0102
066200*****    END-IF                                                   UCSD0102
066300*****    IF FAUR-PROJECT-CODE > SPACES                            UCSD0102
066400*****       MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-PCA (2)         UCSD0102
066500*****    END-IF                                                   UCSD0102
066600         IF FAUR-INDEX > SPACES                                   UCSD0102
066700            MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-IFIS-INDEXA (2) UCSD0102
066800         END-IF                                                   UCSD0102
066600         IF FAUR-SUB-ACCOUNT > SPACES                             CPPDEDTM
066700            MOVE UCCOMMON-ATR-ENTRY-MDT TO DE0230-SUBA (2)        CPPDEDTM
066800         END-IF                                                   CPPDEDTM
067600     END-EVALUATE.                                                CPPDEDTM
067700                                                                  CPPDEDTM
069900 CPPD-FORMAT-LINE-DATA         SECTION.                           CPPDEDTM
070000**************************************************************    CPPDEDTM
070100*  MOVE ARRAY DATA TO CPWSEDTH FOR PPVRTHFO                  *    CPPDEDTM
070200**************************************************************    CPPDEDTM
072400                                                                  CPPDEDTM
072500     ADD +1 TO CPWSEDTH-CNT.                                      CPPDEDTM
072600     MOVE I TO CPWSEDTH-ELEM-SCREEN-OCCURNCE (CPWSEDTH-CNT).      CPPDEDTM
072601     MOVE 0230 TO CPWSEDTH-ELEM-NUMBER (CPWSEDTH-CNT).            CPPDEDTM
072610     MOVE SPACES TO FAUR-FAU.                                     CPPDEDTM
072700     MOVE CPWSETWK-DE0230-FAU (I) TO                              CPPDEDTM
076300                      CPWSEDTH-TRN-DATA (CPWSEDTH-CNT).           CPPDEDTM
076500     SET CPWSEDTH-ELEM-REQUEST-DISPLAY (CPWSEDTH-CNT) TO TRUE.    CPPDEDTM
082000                                                                  CPPDEDTM
082100 CPPD-PROCESS-INPUT-DATA       SECTION.                           CPPDEDTM
082200**************************************************************    CPPDEDTM
082300*  DETERMINE WHETHER SCREEN FIELDS HAVE BEEN ENTERED         *    CPPDEDTM
082400*  AND FORMAT CPWSEDTH WITH ENTERED DATA.                    *    CPPDEDTM
082500**************************************************************    CPPDEDTM
082600                                                                  CPPDEDTM
082700     PERFORM CPPD-SCREEN-ARRAY VARYING I FROM 1 BY 1              CPPDEDTM
082800             UNTIL I IS GREATER THAN 2.                           CPPDEDTM
082900                                                                  CPPDEDTM
089700                                                                  CPPDEDTM
089800 CPPD-SCREEN-ARRAY             SECTION.                           CPPDEDTM
089900**************************************************************    CPPDEDTM
090000*  DETERMINE WHETHER SCREEN ARRAY FIELDS HAVE BEEN ENTERED   *    CPPDEDTM
090100*  AND FORMAT CPWSEDTH WITH ENTERED DATA.                    *    CPPDEDTM
090200**************************************************************    CPPDEDTM
090300                                                                  CPPDEDTM
090400     ADD +1 TO CPWSEDTH-CNT                                       CPPDEDTM
090500     MOVE I TO  CPWSEDTH-ELEM-SCREEN-OCCURNCE (CPWSEDTH-CNT)      CPPDEDTM
090600     MOVE 0230 TO CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT)              CPPDEDTM
090700     MOVE CPWSETWK-DE0230-FAU (I) TO FAUR-FAU.                    CPPDEDTM
090900                                                                  CPPDEDTM
091000*****MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDTM
091100*****                 DE0230-LOCC (I)                             CPPDEDTM
091200*****MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDTM
091300*****                 DE0230-LOCH (I)                             CPPDEDTM
091400*****IF DE0230-LOCL (I) > ZERO         OR                         CPPDEDTM
091500*****   DE0230-LOCF (I) = ERASE-EOF                               CPPDEDTM
091600*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDTM
091700*****   IF DE0230-LOCF (I) = ERASE-EOF                            CPPDEDTM
091800*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDTM
092000*****      MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDTM
092100*****                  DE0230-LOCA (I)                            CPPDEDTM
092200*****   ELSE                                                      CPPDEDTM
092300*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDTM
092400*****                  DE0230-LOCA (I)                            CPPDEDTM
092500*****      MOVE DE0230-LOCI (I) TO FAUR-LOCATION                  CPPDEDTM
092600*****   END-IF                                                    CPPDEDTM
092700*****END-IF.                                                      CPPDEDTM
092800*****                                                             CPPDEDTM
092900*****MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDTM
093000*****                 DE0230-ACCTC (I)                            CPPDEDTM
093100*****MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDTM
093200*****                 DE0230-ACCTH (I)                            CPPDEDTM
093300*****IF DE0230-ACCTL (I) > ZERO         OR                        CPPDEDTM
093400*****   DE0230-ACCTF (I) = ERASE-EOF                              CPPDEDTM
093500*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDTM
093600*****   IF DE0230-ACCTF (I) = ERASE-EOF                           CPPDEDTM
093700*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDTM
093900*****      MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDTM
094000*****                  DE0230-ACCTA (I)                           CPPDEDTM
094100*****   ELSE                                                      CPPDEDTM
094200*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDTM
094300*****                  DE0230-ACCTA (I)                           CPPDEDTM
094400*****      MOVE DE0230-ACCTI (I) TO FAUR-ACCOUNT                  CPPDEDTM
094500*****   END-IF                                                    CPPDEDTM
094600*****END-IF.                                                      CPPDEDTM
094700*****                                                             CPPDEDTM
094800*****MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDTM
094900*****                 DE0230-CCC (I)                              CPPDEDTM
095000*****MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDTM
095100*****                 DE0230-CCH (I)                              CPPDEDTM
095200*****IF DE0230-CCL (I) > ZERO                  OR                 CPPDEDTM
095300*****   DE0230-CCF (I) = ERASE-EOF                                CPPDEDTM
095400*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDTM
095500*****   IF DE0230-CCF (I) = ERASE-EOF                             CPPDEDTM
095600*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDTM
095800*****      MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDTM
095900*****                  DE0230-CCA (I)                             CPPDEDTM
096000*****   ELSE                                                      CPPDEDTM
096100*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDTM
096200*****                  DE0230-CCA (I)                             CPPDEDTM
096300*****      MOVE DE0230-CCI (I) TO FAUR-COST-CENTER                CPPDEDTM
096400*****   END-IF                                                    CPPDEDTM
096500*****END-IF.                                                      CPPDEDTM
096600*****                                                             CPPDEDTM
096700*****MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDTM
096800*****                 DE0230-FUNDC (I)                            CPPDEDTM
096900*****MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDTM
097000*****                 DE0230-FUNDH (I)                            CPPDEDTM
097100*****IF DE0230-FUNDL (I) > ZERO        OR                         CPPDEDTM
097200*****   DE0230-FUNDF (I) = ERASE-EOF                              CPPDEDTM
097300*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDTM
097400*****   IF DE0230-FUNDF (I) = ERASE-EOF                           CPPDEDTM
097500*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDTM
097700*****      MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDTM
097800*****                  DE0230-FUNDA (I)                           CPPDEDTM
097900*****   ELSE                                                      CPPDEDTM
098000*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDTM
098100*****                  DE0230-FUNDA (I)                           CPPDEDTM
098200*****      MOVE DE0230-FUNDI (I) TO FAUR-FUND                     CPPDEDTM
098300*****   END-IF                                                    CPPDEDTM
098400*****END-IF.                                                      CPPDEDTM
098500*****                                                             CPPDEDTM
098600*****MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDTM
098700*****                 DE0230-PCC (I)                              CPPDEDTM
098800*****MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDTM
098900*****                 DE0230-PCH (I)                              CPPDEDTM
099000*****IF DE0230-PCL (I) > ZERO                   OR                CPPDEDTM
099100*****   DE0230-PCF (I) = ERASE-EOF                                CPPDEDTM
099200*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDTM
099300*****   IF DE0230-PCF (I) = ERASE-EOF                             CPPDEDTM
099400*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDTM
099600*****      MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDTM
099700*****                  DE0230-PCA (I)                             CPPDEDTM
099800*****   ELSE                                                      CPPDEDTM
099900*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDTM
100000*****                  DE0230-PCA (I)                             CPPDEDTM
100100*****      MOVE DE0230-PCI (I) TO                FAUR-PROJECT-CODECPPDEDTM
100200*****   END-IF                                                    CPPDEDTM
100300*****END-IF.                                                      CPPDEDTM
100400                                                                  CPPDEDTM
100500     MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDTM
100600                      DE0230-IFIS-INDEXC (I)                      CPPDEDTM
100700     MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDTM
100800                      DE0230-IFIS-INDEXH (I)                      CPPDEDTM
100900     IF DE0230-IFIS-INDEXL (I) > 0  OR                            CPPDEDTM
101000        DE0230-IFIS-INDEXF (I) = ERASE-EOF                        CPPDEDTM
101100        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDTM
101200        IF DE0230-IFIS-INDEXF (I) = ERASE-EOF                     CPPDEDTM
101300           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDTM
101500           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDTM
101600                       DE0230-IFIS-INDEXA (I)                     CPPDEDTM
101700        ELSE                                                      CPPDEDTM
101800           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDTM
101900                       DE0230-IFIS-INDEXA (I)                     CPPDEDTM
217920           MOVE DE0230-IFIS-INDEXI (I) TO FAUR-INDEX              CPPDEDTM
217920           MOVE DE0230-SUBI (I)        TO FAUR-SUB-ACCOUNT        CPPDEDTM
217930           MOVE FAUR-FAU               TO F001-FAU                UCSD0102
271142           CALL PGM-PPFAU001 USING PPFAU001-INTERFACE             UCSD0102
217950           IF F001-FAU-VALID OR F001-INVALID-LEVEL-2              UCSD0102
330335              MOVE F001-FAU        TO CPWSFAUR                    UCSD0102
330337           ELSE                                                   UCSD0102
330339              MOVE SPACES          TO FAUR-FUND                   UCSD0102
330339                                      FAUR-ORGANIZATION           UCSD0102
330339                                      FAUR-PROGRAM                UCSD0102
330339                                      FAUR-LOCATION               UCSD0102
330340           END-IF                                                 UCSD0102
102100        END-IF                                                    CPPDEDTM
102200     END-IF.                                                      CPPDEDTM
102300                                                                  CPPDEDTM
100500     MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDEDTM
100600                      DE0230-SUBC (I)                             CPPDEDTM
100700     MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDEDTM
100800                      DE0230-SUBH (I)                             CPPDEDTM
100900     IF DE0230-SUBL (I) > ZERO         OR                         CPPDEDTM
101000        DE0230-SUBF (I) = ERASE-EOF                               CPPDEDTM
101100        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDEDTM
101200        IF DE0230-SUBF (I) = ERASE-EOF                            CPPDEDTM
101300           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDEDTM
101500           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDEDTM
101600                       DE0230-SUBA (I)                            CPPDEDTM
101700        ELSE                                                      CPPDEDTM
101800           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDEDTM
101900                       DE0230-SUBA (I)                            CPPDEDTM
102000           MOVE DE0230-SUBI (I) TO FAUR-SUB-ACCOUNT               CPPDEDTM
102100        END-IF                                                    CPPDEDTM
102200     END-IF.                                                      CPPDEDTM
102300                                                                  CPPDEDTM
102400     IF CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)                  CPPDEDTM
102500        MOVE FAUR-FAU TO CPWSEDTH-ENTERED-DATA(CPWSEDTH-CNT)      CPPDEDTM
102600     ELSE                                                         CPPDEDTM
102700        SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUE   CPPDEDTM
102800        MOVE FAUR-FAU TO CPWSEDTH-TRN-DATA(CPWSEDTH-CNT)          CPPDEDTM
102900     END-IF.                                                      CPPDEDTM
109100                                                                  CPPDEDTM
111200 CPPD-MOVE-ONE-ELEMENT         SECTION.                           CPPDEDTM
111300**************************************************************    CPPDEDTM
111400*  MOVE DATA FROM CPWSEDTH TO CPWSETWK                       *    CPPDEDTM
111500**************************************************************    CPPDEDTM
111600                                                                  CPPDEDTM
112000     IF CPWSEDTH-ELEM-REQUEST-EDIT(EDIT-SUB)                      CPPDEDTM
112100        MOVE CPWSEDTH-TRN-DATA (EDIT-SUB) TO                      CPPDEDTM
112110                                CPWSETWK-DE0230-FAU (I)           CPPDEDTM
115300     END-IF.                                                      CPPDEDTM
121700                                                                  CPPDEDTM
121800 CPPD-PROCESS-ONE-ELEMENT      SECTION.                           CPPDEDTM
121900**************************************************************    CPPDEDTM
122000*  FOR EACH ELEMENT IN ERROR, SET HIGHLIGHTING AND CURSOR    *    CPPDEDTM
122100**************************************************************    CPPDEDTM
122200                                                                  CPPDEDTM
122600     IF CPWSEDTH-ELEM-ERRORS-FOUND(EDIT-SUB)                      CPPDEDTM
122700        MOVE CPWSEDTH-ELEM-SCREEN-OCCURNCE (EDIT-SUB) TO I        CPPDEDTM
122900*****   MOVE UCCOMMON-HLT-ERROR TO DE0230-LOCH (I)                UCSD0102
123100*****   MOVE UCCOMMON-ATR-ERROR-MDT TO DE0230-LOCA (I)            UCSD0102
123300*****   MOVE UCCOMMON-CLR-ERROR TO DE0230-LOCC (I)                UCSD0102
290800        IF UCCOMMON-MSG-NUMBER = 'AU002'                          UCSD0102
290900           MOVE UCCOMMON-HLT-ERROR TO DE0230-IFIS-INDEXH (I)      UCSD0102
291000           MOVE UCCOMMON-ATR-ERROR-MDT TO DE0230-IFIS-INDEXA (I)  UCSD0102
291100           MOVE UCCOMMON-CLR-ERROR TO DE0230-IFIS-INDEXC (I)      UCSD0102
291200           MOVE  -1 TO DE0230-IFIS-INDEXL (I)                     UCSD0102
291300        ELSE                                                      UCSD0102
291400        IF UCCOMMON-MSG-NUMBER = 'AU001'                          UCSD0102
291500           MOVE UCCOMMON-HLT-ERROR TO DE0230-SUBH (I)             UCSD0102
291600           MOVE UCCOMMON-ATR-ERROR-MDT TO DE0230-SUBA (I)         UCSD0102
291700           MOVE UCCOMMON-CLR-ERROR TO DE0230-SUBC (I)             UCSD0102
291800           MOVE  -1 TO DE0230-SUBL (I)                            UCSD0102
291900        END-IF                                                    UCSD0102
292000        END-IF                                                    UCSD0102
123500*****   MOVE CPWSEDTH-SCREEN-DATA (EDIT-SUB) TO DE0230-LOCO (I)   UCSD0102
218280        MOVE CPWSEDTH-SCREEN-DATA (EDIT-SUB) TO FAUR-FAU          UCSD0102
218290        MOVE FAUR-INDEX         TO DE0230-IFIS-INDEXO (I)         UCSD0102
123600*****   MOVE  -1 TO DE0230-LOCL (I)                               UCSD0102
123700        SET ERRORS-FLAGGED TO TRUE                                CPPDEDTM
123800     END-IF.                                                      CPPDEDTM
143100                                                                  CPPDEDTM
143200 CPPD-CHECK-TOE                SECTION.                           CPPDEDTM
143300**************************************************************    CPPDEDTM
143400*  ATTEMPT TO READ THE STARTING AND ENDING TOE RECORDS.      *    CPPDEDTM
143500*  IF THEY'RE BOTH FOUND, THEN CHECK THAT THE FAU'S ARE THE  *    CPPDEDTM
143600*  SAME.                                                     *    CPPDEDTM
143700**************************************************************    CPPDEDTM
143800     MOVE TOE-FAU TO CPWSETWK-DE0230-FAU (1).                     CPPDEDTM
143900     MOVE TOE-FAU TO FAUR-FAU.                                    CPPDEDTM
145700*****MOVE FAUR-LOCATION TO DE0230-LOCO (1).                       UCSD0102
145900*****MOVE FAUR-ACCOUNT TO DE0230-ACCTO (1).                       UCSD0102
146100*****MOVE FAUR-COST-CENTER TO DE0230-CCO (1).                     UCSD0102
146300*****MOVE FAUR-FUND TO DE0230-FUNDO (1).                          UCSD0102
146500*****MOVE FAUR-PROJECT-CODE TO DE0230-PCO (1).                    UCSD0102
146700     MOVE FAUR-INDEX TO DE0230-IFIS-INDEXO (1).                   UCSD0102
146700     MOVE FAUR-FUND TO DE0230-IFIS-FUNDO (1).                     UCSD0102
146700     MOVE FAUR-SUB-ACCOUNT TO DE0230-SUBO (1).                    CPPDEDTM
173800                                                                  CPPDEDTM
173900 CPPD-HIGHLIGHT-ELEMENTS       SECTION.                           CPPDEDTM
174000**************************************************************    CPPDEDTM
174100*  HIGHLIGHT FIELDS IN ERROR                                 *    CPPDEDTM
174200**************************************************************    CPPDEDTM
176100                                                                  CPPDEDTM
176400*****MOVE UCCOMMON-HLT-ERROR TO DE0230-LOCH (1).                  UCSD0102
176600*****MOVE UCCOMMON-ATR-ERROR-MDT-PRO TO DE0230-LOCA (1).          UCSD0102
176800*****MOVE UCCOMMON-CLR-ERROR TO DE0230-LOCC (1).                  UCSD0102
176900*****MOVE -1 TO DE0230-LOCL (1).                                  UCSD0102
294840*****SET ERRORS-FLAGGED TO TRUE.                                  UCSD0102
294900                                                                  UCSD0102
294910     IF UCCOMMON-MSG-NUMBER = '35047'                             UCSD0102
296400        MOVE UCCOMMON-HLT-ERROR TO DE0230-IFIS-INDEXH (2)         UCSD0102
296600        MOVE WS-ATR-BOT TO DE0230-IFIS-INDEXA (2)                 UCSD0102
296800        MOVE UCCOMMON-CLR-ERROR TO DE0230-IFIS-INDEXC (2)         UCSD0102
295500        MOVE -1 TO DE0230-IFIS-INDEXL (2)                         UCSD0102
295501     ELSE
294910     IF UCCOMMON-MSG-NUMBER NOT = '35079'                         UCSD0102
295000        MOVE UCCOMMON-HLT-ERROR TO DE0230-IFIS-INDEXH (1)         UCSD0102
295100        MOVE UCCOMMON-ATR-ERROR-MDT-PRO TO DE0230-IFIS-INDEXA (1) UCSD0102
295300        MOVE UCCOMMON-CLR-ERROR TO DE0230-IFIS-INDEXC (1)         UCSD0102
295500        MOVE -1 TO DE0230-IFIS-INDEXL (1)                         UCSD0102
295700                                                                  CPPDEDTS
295800******************************************************************CPPDEDTS
295900* PPEXPTRN ADDS 2 TO THE DATA ELEMENT NUMBER FOR THE SECOND      *CPPDEDTS
296000* OCCURRENCE.                                                    *CPPDEDTS
296100******************************************************************CPPDEDTS
296200                                                                  CPPDEDTS
186600*****MOVE UCCOMMON-HLT-ERROR TO DE0230-LOCH (2).                  UCSD0102
190000*****MOVE WS-ATR-BOT TO DE0230-LOCA (2).                          UCSD0102
190200*****MOVE UCCOMMON-CLR-ERROR TO DE0230-LOCC (2).                  UCSD0102
190300*****MOVE -1 TO DE0230-LOCL (2).                                  UCSD0102
296400        MOVE UCCOMMON-HLT-ERROR TO DE0230-IFIS-INDEXH (2)         UCSD0102
296600        MOVE WS-ATR-BOT TO DE0230-IFIS-INDEXA (2)                 UCSD0102
296800        MOVE UCCOMMON-CLR-ERROR TO DE0230-IFIS-INDEXC (2)         UCSD0102
297000        MOVE -1 TO DE0230-IFIS-INDEXL (2)                         UCSD0102
297110     ELSE                                                         UCSD0102
297120        MOVE -1 TO DE0230-IFIS-INDEXL (2)                         UCSD0102
297121     END-IF                                                       UCSD0102
190400     SET ERRORS-FLAGGED TO TRUE.                                  CPPDEDTM
199600                                                                  CPPDEDTM
199700 CPPD-MOVE-VREDIT-DATA-TO-MAP  SECTION.                           CPPDEDTM
199800**************************************************************    CPPDEDTM
199900*  MOVE SCREEN DATA RETURNED BY PPVRTHFO TO THE MAP AREA     *    CPPDEDTM
200000**************************************************************    CPPDEDTM
200500                                                                  CPPDEDTM
200700     MOVE CPWSEDTH-SCREEN-DATA (EDIT-SUB) TO FAUR-FAU.            CPPDEDTM
200800*****MOVE FAUR-LOCATION TO DE0230-LOCO (I).                       UCSD0102
201200*****MOVE FAUR-ACCOUNT TO DE0230-ACCTO (I).                       UCSD0102
201600*****MOVE FAUR-COST-CENTER TO DE0230-CCO (I).                     UCSD0102
202000*****MOVE FAUR-FUND TO DE0230-FUNDO (I).                          UCSD0102
202400*****MOVE FAUR-PROJECT-CODE TO DE0230-PCO (I).                    UCSD0102
202800     MOVE FAUR-INDEX TO DE0230-IFIS-INDEXO (I).                   UCSD0102
202800     MOVE FAUR-FUND TO DE0230-IFIS-FUNDO (I).                     UCSD0102
202800     MOVE FAUR-SUB-ACCOUNT TO DE0230-SUBO (I).                    CPPDEDTM
218600***************    END OF SOURCE - PPWEDTM    ******************* PPWEDTM
