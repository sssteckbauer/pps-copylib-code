000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSXCPA                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000005*    SINCE THE PHYSICAL TABLE IS BEING UNLOADED AND          */   32021138
000005*    REFERENCED, RATHER THAN USING A VIEW, THE DELETED       */   32021138
000005*    COLUMN GTN-CON-EDIT MUST BE RESTORED IN ORDER TO ALIGN  */   32021138
000005*    THE DATA CORRECTLY.                                     */   32021138
000007**************************************************************/   32021138
000100**************************************************************/   36410724
000200*  COPYMEMBER: CPWSGTNH                                      */   36410724
000300*  REL#: __0724_________  SERVICE REQUEST NO(S)___3641_______*/   36410724
000400*  REF REL: ___0717_____                                     */   36410724
000500*  NAME ____PXP________   MODIFICATION DATE ____12/11/92_____*/   36410724
000600*  DESCRIPTION                                               */   36410724
000700*   - ADDED CON-EDIT ROUTINE NUMBER.                         */   36410724
000800*     PER ERROR REPORT 874                                   */   36410724
000900**************************************************************/   36410724
001000**************************************************************/   36330704
001100*  COPYMEMBER: CPWSGTNH                                      */   36330704
001200*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
001300*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
001400*  DESCRIPTION:                                              */   36330704
001500*    DCLGEN FOR THE PPPVGTNH VIEW                            */   36330704
001600*                                                            */   36330704
001700**************************************************************/   36330704
001800******************************************************************CPWSGTNH
001900* COBOL DECLARATION FOR TABLE PAYCXD.PPPVGTNH_GTNH               *CPWSGTNH
002000******************************************************************CPWSGTNH
002100*01  DCLPPPVGTNH-GTNH.                                            CPWSGTNH
002200     10 GTN-PRIORITY         PIC X(4).                            CPWSGTNH
002300     10 GTN-DEDUCTION        PIC X(3).                            CPWSGTNH
002400     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSGTNH
002500     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSGTNH
002600     10 GTN-MATCH-ELEMENT    PIC X(3).                            CPWSGTNH
002700     10 GTN-DESCRIPTION      PIC X(15).                           CPWSGTNH
002800     10 GTN-TYPE             PIC X(1).                            CPWSGTNH
002900     10 GTN-GROUP            PIC X(1).                            CPWSGTNH
003000     10 GTN-SM-SCHED-CODE    PIC X(1).                            CPWSGTNH
003100     10 GTN-BI-SCHED-CODE    PIC X(1).                            CPWSGTNH
003200     10 GTN-MN-SCHED-CODE    PIC X(1).                            CPWSGTNH
003300     10 GTN-YTD              PIC X(1).                            CPWSGTNH
003400     10 GTN-SUSPENSE         PIC X(1).                            CPWSGTNH
003500     10 GTN-QTD              PIC X(1).                            CPWSGTNH
003600     10 GTN-DECLINING-BAL    PIC X(1).                            CPWSGTNH
003700     10 GTN-ETD              PIC X(1).                            CPWSGTNH
003800     10 GTN-FYTD             PIC X(1).                            CPWSGTNH
003900     10 GTN-USER             PIC X(1).                            CPWSGTNH
004000     10 GTN-FULL-PART        PIC X(1).                            CPWSGTNH
004100     10 GTN-PRT-YTD-ON-CK    PIC X(1).                            CPWSGTNH
004200     10 GTN-USAGE            PIC X(1).                            CPWSGTNH
004300     10 GTN-BASE             PIC X(1).                            CPWSGTNH
004400     10 GTN-REDUCE-BASE      PIC X(1).                            CPWSGTNH
004500     10 GTN-SP-CALC-NO       PIC X(2).                            CPWSGTNH
004600     10 GTN-SP-CALC-IND      PIC X(1).                            CPWSGTNH
004700     10 GTN-SP-UPDATE-NO     PIC X(2).                            CPWSGTNH
004800     10 GTN-STATUS           PIC X(1).                            CPWSGTNH
004900     10 GTN-STOP-AT-TERM     PIC X(1).                            CPWSGTNH
005000     10 GTN-VALUE-RNG-EDIT   PIC X(1).                            CPWSGTNH
005100     10 GTN-VALUE1           PIC S99999V99 USAGE COMP-3.          CPWSGTNH
005200     10 GTN-VALUE2           PIC S99999V99 USAGE COMP-3.          CPWSGTNH
005300*****10 GTN-CON-EDIT         PIC X(1).                            36410724
005300     10 GTN-CON-EDIT         PIC X(1).                            32021138
005400     10 GTN-DEDUCTION-CODE   PIC X(2).                            CPWSGTNH
005500     10 GTN-LIABILITY-ACCT   PIC X(6).                            CPWSGTNH
005600     10 GTN-RECEVBL-ACCT     PIC X(6).                            CPWSGTNH
005700     10 GTN-PREPAY-ACCT      PIC X(6).                            CPWSGTNH
005800     10 GTN-BEN-CODE         PIC X(1).                            CPWSGTNH
005900     10 GTN-REDUCTION-FWT    PIC X(1).                            CPWSGTNH
006000     10 GTN-REDUCTION-SWT    PIC X(1).                            CPWSGTNH
006100     10 GTN-REDUCTION-FICA   PIC X(1).                            CPWSGTNH
006200     10 GTN-CB-ELIG-IND      PIC X(1).                            CPWSGTNH
006300     10 GTN-MAX-AMOUNT       PIC S99999V99 USAGE COMP-3.          CPWSGTNH
006400     10 GTN-CB-BEHAV-CODE    PIC X(1).                            CPWSGTNH
006500     10 GTN-BENEFIT-TYPE     PIC X(1).                            CPWSGTNH
006600     10 GTN-BENEFIT-PLAN     PIC X(2).                            CPWSGTNH
006700     10 GTN-SET-INDICATOR    PIC X(1).                            CPWSGTNH
006800     10 GTN-IGTN-INDICATOR   PIC X(1).                            CPWSGTNH
006900     10 GTN-IDED-INDICATOR   PIC X(1).                            CPWSGTNH
007000     10 GTN-IRTR-INDICATOR   PIC X(1).                            CPWSGTNH
007100     10 GTN-IRET-INDICATOR   PIC X(1).                            CPWSGTNH
007200     10 GTN-LIABILITY-LOC    PIC X(1).                            CPWSGTNH
007300     10 GTN-LIABILITY-FUND   PIC X(5).                            CPWSGTNH
007400     10 GTN-LIABILITY-SUB    PIC X(1).                            CPWSGTNH
007500     10 GTN-LIABILITY-OBJ    PIC X(4).                            CPWSGTNH
007600     10 GTN-OVRD-DEDUCTION   PIC X(3).                            CPWSGTNH
007700     10 GTN-ICED-INDICATOR   PIC X(1).                            CPWSGTNH
007800     10 GTN-LAST-ACTION      PIC X(1).                            CPWSGTNH
007900     10 GTN-LAST-ACTION-DT   PIC X(10).                           CPWSGTNH
008000     10 GTN-CON-EDIT-RTN     PIC X(3).                            36410724
008100******************************************************************CPWSGTNH
008200* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 58      *CPWSGTNH
008300******************************************************************CPWSGTNH
