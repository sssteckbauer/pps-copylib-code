000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNF101                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______JLT_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*    Initial release of linkage copymember between calling   */   32021138
000700*    program (PPP520) and module PPFAU101.                   */   32021138
000800**************************************************************/   32021138
000900*                                                             *   CPLNF101
001000*  This copymember is one of the Full Accounting Unit modules *   CPLNF101
001100*  which campuses may need to modify to accomodate their own  *   CPLNF101
001200*  Chart of Accounts structure.                               *   CPLNF101
001300*                                                             *   CPLNF101
001400***************************************************************   CPLNF101
001500*                                                                 CPLNF101
001600*01  PPFAU101-INTERFACE.                                          CPLNF101
001700     05  F101-INPUT.                                              CPLNF101
001800*                                                                 CPLNF101
001900         10  F101-WCR-TYP-CALL-IND          PIC X(01).            CPLNF101
002000             88  F101-WCR-TYP-CALL-GET-RATE           VALUE 'R'.  CPLNF101
002100             88  F101-WCR-TYP-CALL-BUMP-TOT           VALUE 'T'.  CPLNF101
002200             88  F101-WCR-TYP-CALL-PRINT-RPT          VALUE 'P'.  CPLNF101
002300         10  F101-WCR-IN-FAU                PIC X(30).            CPLNF101
002400         10  F101-WCR-AMOUNT    COMP-3   PIC S9(05)V99.           CPLNF101
002500*                                                                 CPLNF101
002600      05  F101-OUTPUT.                                            CPLNF101
002700*                                                                 CPLNF101
002800         10  F101-WCR-PRINT-COMPLETE-IND    PIC X(01).            CPLNF101
002900             88  F101-WCR-PRINT-NOT-COMPLETE          VALUE 'N'.  CPLNF101
003000             88  F101-WCR-PRINT-COMPLETE              VALUE 'Y'.  CPLNF101
003100         10  F101-WCR-RETURN-IND            PIC X(01).            CPLNF101
003200             88  F101-WCR-RETURN-OK                   VALUE ' '.  CPLNF101
003300             88  F101-WCR-ERR-LOAD-PROBLEM            VALUE 'L'.  CPLNF101
003400             88  F101-WCR-ERR-NO-DEFAULT              VALUE 'N'.  CPLNF101
003500             88  F101-WCR-WRN-DEFAULT-USED            VALUE 'D'.  CPLNF101
003600         10  F101-WCR-TBL-RATE  COMP-3   PIC S9V9(04).            CPLNF101
003700         10  F101-WCR-RETURN-PRINT-DATA.                          CPLNF101
003800             15  F101-WCR-RETURN-PRINT-LINE   PIC X(132).         CPLNF101
003900             15  F101-WCR-RETURN-PRT-HEADER4  PIC X(132).         CPLNF101
004000             15  F101-WCR-RETURN-PRT-HEADER5  PIC X(132).         CPLNF101
004100*                                                                 CPLNF101
004200*  *  *  *  *                                                     CPLNF101
