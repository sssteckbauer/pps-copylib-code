000100**************************************************************/  *36070535
000200*  COPYMEMBER: CPWSRACT                                      */  *36070535
000300*  RELEASE: ____0535____  SERVICE REQUEST(S): ____3607____   */  *36070535
000400*  NAME:  ______JAG_____  CREATION DATE: ____02/06/91_____   */  *36070535
000500*  DESCRIPTION:                                              */  *36070535
000600*  - WORKING-STORAGE LAYOUT OF APP TABLE ROW.                */  *36070535
000700**************************************************************/  *36070535
000800*    COPYID=CPWSRACT                                              CPWSRACT
000900*01  ACT-ROW.                                                     CPWSRACT
001000     10  EMPLOYEE-ID          PIC X(9).                           CPWSRACT
001100     10  APPT-NUM             PIC S9(4) USAGE COMP.               CPWSRACT
001200     10  DIST-NUM             PIC S9(4) USAGE COMP.               CPWSRACT
001300     10  ACTION-CODE          PIC X(2).                           CPWSRACT
001400     10  ACTION-EFFDATE       PIC X(10).                          CPWSRACT
