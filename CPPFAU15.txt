000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPFAU15                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______JLT_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*   Initial release of Procedure Division copymember which   */   32021138
000700*   is used by program PPP530 in conjunction with copymembers*/   32021138
000800*   CPWSXFAU.                                                */   32021138
000900**************************************************************/   32021138
001000*                                                             *   CPPFAU15
001100*  This copymember is one of the Full Accounting Unit modules *   CPPFAU15
001200*  which campuses may need to modify to accomodate their own  *   CPPFAU15
001300*  Chart of Accounts structure.                               *   CPPFAU15
001400*                                                             *   CPPFAU15
001500***************************************************************   CPPFAU15
001600*                                                                 CPPFAU15
001700***************************************************************** CPPFAU15
001800*  Apply the Sub-account implied by the WX-BUDGETARY-SUB          CPPFAU15
001900*  subscript associated with the WSP Budgetary Offset Array.      CPPFAU15
002000***************************************************************** CPPFAU15
002100                                                                  CPPFAU15
002200     IF WX-BUDGETARY-SUB  = 1                                     CPPFAU15
002300       MOVE '0'   TO XFAU-FAU-ACCOUNT-SUB OF XFAU-FAU-UNFORMATTED CPPFAU15
002400     ELSE                                                         CPPFAU15
002500       IF WX-BUDGETARY-SUB  = 2                                   CPPFAU15
002600         MOVE '1' TO XFAU-FAU-ACCOUNT-SUB OF XFAU-FAU-UNFORMATTED CPPFAU15
002700       ELSE                                                       CPPFAU15
002800         MOVE '2' TO XFAU-FAU-ACCOUNT-SUB OF XFAU-FAU-UNFORMATTED CPPFAU15
002900       END-IF                                                     CPPFAU15
003000     END-IF                                                       CPPFAU15
003100                                                                  CPPFAU15
003200************** END OF COPY CPPFAU15 ***************************   CPPFAU15
