000100*==========================================================%      ULWSEDSA
000200*=    COPYMEMBER: ULWSEDSA                                =%      ULWSEDSA
000300*=    CHANGE # 0056         PROJ. REQUEST: 0056           =%      ULWSEDSA
000400*=    NAME: DELIA PERKINS   MODIFICATION DATE: 10/19/95   =%      ULWSEDSA
000500*=                                                        =%      ULWSEDSA
000600*=    EDSA FUNCTION AREA                                  =%      ULWSEDSA
000700*=                                                        =%      ULWSEDSA
000800*==========================================================%      ULWSEDSA
000900*    COPYID=ULWSEDSA                                              ULWSEDSA
001000******************************************************************ULWSEDSA
001100*   COPY MEMBER USED TO STORE THE DSA SECURITY INFO.             *ULWSEDSA
001200******************************************************************ULWSEDSA
001300*01  ULWSEDSA-AREA                     EXTERNAL.                  ULWSEDSA
007701     05  ULWSEDSA-AREA   PIC X(1024).                             ULWSEDSA
007702     05  ULWSEDSA-DATA REDEFINES ULWSEDSA-AREA.                   ULWSEDSA
007703         10  ULWSEDSA-INIT-IND             PIC X(16).             ULWSEDSA
007704             88  ULWSEDSA-INITIALIZED          VALUE              ULWSEDSA
007705                                           '>>$$ULWSEDSA$$<<'.    ULWSEDSA
007706         10  ULWSEDSA-ENTERED-USERID       PIC X(08).             ULWSEDSA
007707         10  ULWSEDSA-USERID               PIC X(08).             ULWSEDSA
007708         10  ULWSEDSA-TOTAL-PAGES          PIC S9(04) COMP.       ULWSEDSA
007709         10  ULWSEDSA-TOTAL-ENTRIES        PIC S9(04) COMP.       ULWSEDSA
007710         10  ULWSEDSA-ENTERED-EMPLOYEE-NO  PIC X(09).             ULWSEDSA
007720         10  ULWSEDSA-FILLER               PIC X(979).            ULWSEDSA
