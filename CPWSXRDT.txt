000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXRDT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000600*    COPYID=CPWSXRDT                                              CPWSXRDT
000200*01  XRDT-REPORT-DESCRIPTION-TABLE.                               30930413
000300*-----------------------------------------------------------------CPWSXRDT
000400*        R E P O R T  D E S C R I P T I O N  T A B L E            CPWSXRDT
000500*-----------------------------------------------------------------CPWSXRDT
000600     03  XRDT-DELETE         PIC X.                               CPWSXRDT
000700     03  XRDT-KEY.                                                CPWSXRDT
000800         05  XRDT-KEY-CONSTANT   PIC X(4).                        CPWSXRDT
000900         05  XRDT-REPORT-ID      PIC XX.                          CPWSXRDT
001000         05  XRDT-LINE-NO        PIC 9(3).                        CPWSXRDT
001100         05  XRDT-STRT-POS       PIC 9(3).                        CPWSXRDT
001200         05  XRDT-FILLER1        PIC X.                           CPWSXRDT
001300     03  XRDT-DATA.                                               CPWSXRDT
001400         05  XRDT-ELEMENT        PIC X(4).                        CPWSXRDT
001500         05  XRDT-TRANS-EDIT     PIC 99.                          CPWSXRDT
001600         05  XRDT-TRANS-LNTH     PIC 9(3).                        CPWSXRDT
001700         05  XRDT-DEFAULT        PIC X(30).                       CPWSXRDT
001800         05  FILLER              PIC X(171).                      CPWSXRDT
001900     SKIP2                                                        CPWSXRDT
