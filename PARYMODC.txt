000100******************************************************************PARYMODC
000200*                                                                *PARYMODC
000300*                P A R Y M O D C                                 *PARYMODC
000400*                                                                *PARYMODC
000500*    THIS MEMBER DEFINES THE COMMON MODULE COMMUNICATION AREA    *PARYMODC
000600*    WITHIN THE PAR SYSTEM.  IN CALLING MODULES THIS AREA IS     *PARYMODC
000700*    DEFINED IN WORKING STORAGE.   IN CALLED MODULES THIS AREA   *PARYMODC
000800*    IS IN LINKAGE SECTION.                                      *PARYMODC
000900*                                                                *PARYMODC
001000******************************************************************PARYMODC
001100                                                                  PARYMODC
001200*01  MODULE-COMMON-AREA.                                          PARYMODC
001300                                                                  PARYMODC
001400     05  TEST-SWITCH-LS           PIC X.                          PARYMODC
001500         88  TEST-RUN             VALUE 'Y'.                      PARYMODC
001600         88  TEST-REPORT          VALUE 'Y' 'Z'.                  PARYMODC
001700                                                                  PARYMODC
001800     05  RUN-DATE-LS              PIC 9(6).                       PARYMODC
001900     05  FILLER                   REDEFINES RUN-DATE-LS.          PARYMODC
002000         10  RUN-DATE-MM-LS       PIC 99.                         PARYMODC
002100         10  RUN-DATE-DD-LS       PIC 99.                         PARYMODC
002200         10  RUN-DATE-YY-LS       PIC 99.                         PARYMODC
002300                                                                  PARYMODC
002400*    05  TYPE-OF-RUN-LS           PIC X.                          PARYMODC
002500*        88  MONTHLY-RUN          VALUE 'M'.                      PARYMODC
002600*        88  QUARTERLY-RUN        VALUE 'Q'.                      PARYMODC
002700                                                                  PARYMODC
002800     05  TYPE-OF-CALL-LS          PIC X.                          PARYMODC
002900         88  INITIALIZATION       VALUE 'I'.                      PARYMODC
003000         88  PROCESS              VALUE 'P'.                      PARYMODC
003100         88  TERMINATION          VALUE 'T'.                      PARYMODC
003200                                                                  PARYMODC
003300     05  LOCATION-CODE-LS         PIC 99.                         PARYMODC
003400*    05  RETURN-CODE              PIC 99.                         PARYMODC
003500     05  RETURN-DATE-LS           PIC X(18).                      PARYMODC
003600     05  QUARTER-LS               PIC 9.                          PARYMODC
003700     05  FISCAL-YEAR-LS           PIC 99.                         PARYMODC
