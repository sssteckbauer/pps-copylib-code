      *************************************************************     00000100
      *****  COPYBOOK NAME: PPLTALX                                     00001000
      *****  STANDARD TALX RECORD LAYOUTS                               00002000
      *************************************************************     00000100
      *****                                                             00003000
       01  REC000-HEADER.                                               00010000
           05  R000-RECTYPE           PIC X(15) VALUE '000HEADER'.
           05  FILLER                 PIC X(91) VALUE ' '.              00020001
           05  R000-VERSION           PIC X(05) VALUE '02.00'.          00020001
           05  R000-SOURCE-ID         PIC X(05) VALUE '14655'.          00050000
           05  R000-SOURCE-TYPE       PIC X(20) VALUE 'EMPLOYER'.       00060000
           05  R000-SUB-CODE          PIC X(01) VALUE 'I'.              00080000
           05  FILLER                 PIC X(06) VALUE ' '.              00100000
                                                                        00110000
       01  REC202-EMPLPIM.                                              00120000
           05  R202-RECTYPE           PIC X(15) VALUE '202EMPLPIM'.
           05  R202-COCODE            PIC X(16) VALUE '14655'.          00020001
           05  R202-SSN               PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(64) VALUE ' '.              00050000
           05  R202-USER-ID           PIC X(64) VALUE ' ' .             00060000
           05  R202-ACCT-NBR          PIC X(04) VALUE 'UG68'.           00080000
           05  R202-AS-OF-DATE        PIC X(08) VALUE ' '.              00080000
           05  R202-FIRST-NAME        PIC X(64) VALUE ' '.              00080000
           05  R202-MID-NAME          PIC X(64) VALUE ' '.              00080000
           05  R202-LAST-NAME         PIC X(64) VALUE ' '.              00080000
           05  FILLER                 PIC X(17) VALUE ' '.              00080000
           05  R202-EMPL-PIN          PIC X(08) VALUE ' '.              00080000
           05  R202-DIR-ACCESS-FLAG   PIC X(01) VALUE ' '.              00080000
           05  R202-LOCATION          PIC X(12) VALUE 'UCSD'.           00080000
           05  R202-JOB-TITLE         PIC X(64) VALUE ' '.              00080000
           05  R202-STATUS-FLAG       PIC X(01) VALUE ' '.              00080000
           05  R202-RECENT-HIRE-DATE  PIC X(08) VALUE ' '.              00080000
           05  R202-STATUS-TYPE       PIC X(01) VALUE ' '.              00080000
           05  R202-ORIG-HIRE-DATE    PIC X(08) VALUE ' '.              00080000
           05  R202-TERM-DATE         PIC X(08) VALUE ' '.              00080000
           05  FILLER                 PIC X(02) VALUE ' '.              00080000
           05  R202-TERM-REASON       PIC X(10) VALUE ' '.              00080000
           05  R202-LAST-DAY-DATE     PIC X(08) VALUE ' '.              00080000
           05  R202-WORK-STATE        PIC X(02) VALUE ' '.              00080000
           05  R202-WORK-LOCATION     PIC X(20) VALUE ' '.              00080000
           05  R202-WORK-LOC-FEIN     PIC X(15) VALUE ' '.              00080000
           05  FILLER                 PIC X(15) VALUE ' '.              00080000
           05  R202-ADJ-HIRE-DATE     PIC X(08) VALUE ' '.              00080000
           05  FILLER                 PIC X(05) VALUE ' '.              00080000
           05  R202-PAY-FREQ          PIC X(02) VALUE ' '.              00080000
           05  FILLER                 PIC X(11) VALUE ' '.              00080000
                                                                        00220000
       01  REC210-EMPL-PAYINFO.                                         00230000
           05  R210-RECTYPE           PIC X(15) VALUE '210EMPLPAYINFO'.
           05  R210-COCODE            PIC X(16) VALUE '14655'.          00020001
           05  R210-SSN               PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(64) VALUE ' '.              00050000
           05  R210-FEIN              PIC X(15) VALUE ' '.              00020001
           05  R210-LOCATION          PIC X(12) VALUE 'UCSD'.           00020001
           05  R210-MASTER-REC-FLAG   PIC X(01) VALUE 'N'.              00020001
           05  R210-AS-OF-DATE        PIC X(08) VALUE ' '.              00020001
           05  R210-AVG-HRS-WORKED    PIC 9(03)     VALUE ZERO.         00020001
           05  FILLER                 PIC X(247) VALUE ' '.             00020001
           05  R210-PAY-RATE          PIC 9(8).9999 VALUE ZERO.         00020001
           05  R210-PAY-RATE-CD       PIC X(02) VALUE ' '.              00020001
           05  FILLER                 PIC X(10) VALUE ' '.              00020001
           05  R210-GROSS-SALARY-DATA-YEAR-1.                           00020001
               10  R210-YEAR          PIC X(08) VALUE ' '.              00020001
               10  R210-BASE-PAY-AMT  PIC 9(08).99 VALUE ZERO.          00020001
               10  R210-OVERTIME-AMT  PIC X(11) VALUE ' '.              00020001
               10  R210-BONUS-AMT     PIC X(11) VALUE ' '.              00020001
               10  R210-COMM-AMT      PIC X(11) VALUE ' '.              00020001
               10  R210-OTHER-AMT     PIC 9(08).99 VALUE ZERO.          00020001
               10  R210-TOTAL-AMT     PIC 9(10).99 VALUE ZERO.          00020001
           05  R210-GROSS-SALARY-DATA-YEAR-2.                           00020001
               10  R210-YEAR2         PIC X(08) VALUE ' '.              00020001
               10  R210-BASE-PAY-AMT2 PIC 9(08).99 VALUE ZERO.          00020001
               10  R210-OVERTIME-AMT2 PIC X(11) VALUE ' '.              00020001
               10  R210-BONUS-AMT2    PIC X(11) VALUE ' '.              00020001
               10  R210-COMM-AMT2     PIC X(11) VALUE ' '.              00020001
               10  R210-OTHER-AMT2    PIC 9(08).99 VALUE ZERO.          00020001
               10  R210-TOTAL-AMT2    PIC 9(10).99 VALUE ZERO.          00020001
           05  R210-GROSS-SALARY-DATA-YEAR-3.                           00020001
               10  R210-YEAR3         PIC X(08) VALUE ' '.              00020001
               10  R210-BASE-PAY-AMT3 PIC 9(08).99 VALUE ZERO.          00020001
               10  R210-OVERTIME-AMT3 PIC X(11) VALUE ' '.              00020001
               10  R210-BONUS-AMT3    PIC X(11) VALUE ' '.              00020001
               10  R210-COMM-AMT3     PIC X(11) VALUE ' '.              00020001
               10  R210-OTHER-AMT3    PIC 9(08).99 VALUE ZERO.          00020001
               10  R210-TOTAL-AMT3    PIC 9(10).99 VALUE ZERO.          00020001
           05  FILLER                 PIC X(07) VALUE ' '.              00020001
                                                                        00220000
       01  REC220-EMPL-PAYDTL.                                          00230000
           05  R220-RECTYPE           PIC X(15) VALUE '220EMPLPAYDTL'.
           05  R220-COCODE            PIC X(16) VALUE '14655'.          00020001
           05  R220-SSN               PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(64) VALUE ' '.              00050000
           05  R220-LOCATION          PIC X(12) VALUE ' '.              00020001
           05  R220-PAY-FREQ          PIC X(02) VALUE ' '.              00020001
           05  R220-FEIN              PIC X(15) VALUE ' '.              00020001
           05  FILLER                 PIC X(15) VALUE ' '.              00050000
           05  R220-STATE             PIC X(02) VALUE ' '.              00020001
           05  R220-LOCATION-CD       PIC X(20) VALUE ' '.              00020001
           05  FILLER                 PIC X(123) VALUE ' '.             00050000
           05  R220-PAYPRD-END-DATE   PIC X(08) VALUE ' '.              00020001
           05  FILLER                 PIC X(08) VALUE ' '.              00020001
           05  R220-PAY-DATE          PIC X(08) VALUE ' '.              00020001
           05  FILLER                 PIC X(11) VALUE ' '.              00020001
           05  R220-PAY-TYPE-ALL      PIC X(01) VALUE 'A'.              00020001
           05  R220-GROSS-PAY-ALL     PIC 9(08).99 VALUE ZERO.          00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-ACCT-NBR          PIC X(04) VALUE 'UG68'.           00020001
           05  R220-PAY-TYPE-R        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-R       PIC 9(08).99 VALUE ZERO.          00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-O        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-O       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-V        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-V       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-S        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-S       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-H        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-H       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-B        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-B       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-E        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-E       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-P        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-P       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-W        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-W       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-K        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-K       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-C        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-C       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-T        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-T       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(19) VALUE ' '.              00020001
           05  R220-PAY-TYPE-M        PIC X(01) VALUE ' '.              00020001
           05  R220-GROSS-PAY-M       PIC X(11) VALUE ' '.              00020001
           05  FILLER                 PIC X(01) VALUE ' '.              00020001
                                                                        00220000
       01  REC999-FOOTER.                                               00230000
           05  R999-RECTYPE           PIC X(015) VALUE '999FOOTER'.
           05  FILLER                 PIC X(397) VALUE ' '.
                                                                        00220000
       01  TALX-FILE-RECORD.                                            00230000
           05  FILLER                 PIC X(750) VALUE ' '.
