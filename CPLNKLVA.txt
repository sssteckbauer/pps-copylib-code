000100******************************************************************36090513
000200*  COPYMEMBER: CPLNKLVA                                          *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____       *36090513
000400*  NAME:__P_PARKER______  CREATION DATE:      __11/05/90__       *36090513
000500*  DESCRIPTION:                                                  *36090513
000600*    LINKAGE FOR PPLVAUTL SUBROUTINE CREATED FOR DB2 EDB         *36090513
000700*    CONVERSION.                                                 *36090513
000800*                                                                *36090513
000900******************************************************************CPLNKLVA
001000*    COPYID=CPLNKLVA                                              CPLNKLVA
001100*01  PPLVAUTL-INTERFACE.                                          CPLNKLVA
001200     05  PPLVAUTL-EMPLOYEE-ID             PIC X(09).              CPLNKLVA
001300     05  PPLVAUTL-ERROR-INDICATORS.                               CPLNKLVA
001400         10  PPLVAUTL-ERROR-SW            PIC X(01).              CPLNKLVA
001500             88  PPLVAUTL-ERROR VALUE 'Y'.                        CPLNKLVA
