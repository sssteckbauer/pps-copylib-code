000100**************************************************************/   48411571
000200*  COPYMEMBER: CPLNQPAR                                      */   48411571
000300*  RELEASE: ___1571______ SERVICE REQUEST(S): ____14841____  */   48411571
000400*  NAME:______RIDER______ CREATION DATE:      ___04/30/04__  */   48411571
000500*  DESCRIPTION:                                              */   48411571
000600*    PAR EXPANSION:                                          */   48411571
000700*    LINKAGE FOR SUBPROGRAM PPSEQPAR.                        */   48411571
000800**************************************************************/   48411571
002700*                                                                 CPLNQPAR
002800*01  PPSEQPAR-INTERFACE.                                          CPLNQPAR
002900    05  PPSEQPAR-INPUTS.                                          CPLNQPAR
003000        10  PPSEQPAR-ACTION              PIC X(08).               CPLNQPAR
003001            88  PPSEQPAR-ACTION-OPEN-IN  VALUE 'OPEN IN '.        CPLNQPAR
003002            88  PPSEQPAR-ACTION-OPEN-I-O VALUE 'OPEN I-O'.        CPLNQPAR
003003            88  PPSEQPAR-ACTION-OPEN-OUT VALUE 'OPEN OUT'.        CPLNQPAR
003004            88  PPSEQPAR-ACTION-READ     VALUE 'READ    '.        CPLNQPAR
003005            88  PPSEQPAR-ACTION-READ-FIX VALUE 'READ FIX'.        CPLNQPAR
003006            88  PPSEQPAR-ACTION-WRITE    VALUE 'WRITE   '.        CPLNQPAR
003007            88  PPSEQPAR-ACTION-REWRITE  VALUE 'REWRITE '.        CPLNQPAR
003008            88  PPSEQPAR-ACTION-CLOSE    VALUE 'CLOSE   '.        CPLNQPAR
003010        10  PPSEQPAR-FILE                PIC X(08).               CPLNQPAR
003022            88  PPSEQPAR-FILE-CPA        VALUE 'CPA     '.        CPLNQPAR
003023            88  PPSEQPAR-FILE-PAYAUDIT   VALUE 'PAYAUDIT'.        CPLNQPAR
003024            88  PPSEQPAR-FILE-PRELMPAR   VALUE 'PRELMPAR'.        CPLNQPAR
003093            88  PPSEQPAR-FILE-CALPAR1    VALUE 'CALPAR1 '.        CPLNQPAR
003094            88  PPSEQPAR-FILE-CALPAR2    VALUE 'CALPAR2 '.        CPLNQPAR
003100    05  PPSEQPAR-OUTPUTS.                                         CPLNQPAR
003110        10  PPSEQPAR-STATUS              PIC X(08).               CPLNQPAR
003120            88  PPSEQPAR-STATUS-EOF      VALUE 'EOF     '.        CPLNQPAR
003130            88  PPSEQPAR-STATUS-OK       VALUE '        '.        CPLNQPAR
003140            88  PPSEQPAR-STATUS-ACT-ERR  VALUE 'ACTION  '.        CPLNQPAR
003150            88  PPSEQPAR-STATUS-REQ-ERR  VALUE 'REQUEST '.        CPLNQPAR
003160            88  PPSEQPAR-STATUS-FMT-ERR  VALUE 'FORMAT  '.        CPLNQPAR
