000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPWSXWSI                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/27/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    EXPANDED FUND FROM FIVE CHARACTERS TO SIX CHARACTERS=%      UCSD0102
000800*=                                                        =%      UCSD0102
000900*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   17891060
000200*  COPYMEMBER: CPWSXWSI                                      */   17891060
000300*  RELEASE: ___1060______ SERVICE REQUEST(S): ____11789____  */   17891060
000400*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___04/09/96__  */   17891060
000500*  DESCRIPTION:                                              */   17891060
000600*  - ERROR REPORT 1381:                                      */   17891060
000700*    ADD FUNCTION 88 VALUE FOR WSP FUND CHECK                */   17891060
000800**************************************************************/   17891060
000100**************************************************************/   36060628
000200*  COPYMEMBER: CPWSXWSI                                      */   36060628
000300*  RELEASE # ____0628____ SERVICE REQUEST NO(S)___3606_______*/   36060628
000400*  NAME __ STEINITZ ___   DATE CREATED: ________01/10/92_____*/   36060628
000500*  DESCRIPTION                                               */   36060628
000600*   - DEFINES INTERFACE TO PPWSPUTL                          */   36060628
000700**************************************************************/   36060628
000800*COPYID=CPWSXWSI                                                  CPWSXWSI
000900**************************************************************/   CPWSXWSI
001000*01  WORK-STUDY-INTERFACE.                                        CPWSXWSI
001100     05  XWSI-FUNCTION               PIC  X(01).                  CPWSXWSI
001200         88  XWSI-FIND-PGM                           VALUE 'F'.   CPWSXWSI
001300         88  XWSI-GET-APPROVAL                       VALUE 'A'.   CPWSXWSI
001400         88  XWSI-CHECK-FUND                         VALUE 'K'.   CPWSXWSI
002300         88  XWSI-CHECK-FUND-WSP                     VALUE 'W'.   17891060
001500         88  XWSI-LOAD-VALUES                        VALUE 'L'.   CPWSXWSI
001600     05  XWSI-NEEDED-DATE-YMD.                                    CPWSXWSI
001700         10  XWSI-NEEDED-YEAR        PIC  X(02).                  CPWSXWSI
001800         10  XWSI-NEEDED-MONTH       PIC  X(02).                  CPWSXWSI
001900         10  XWSI-NEEDED-DAY         PIC  X(02).                  CPWSXWSI
002000     05  XWSI-STATUS                 PIC  X(01).                  CPWSXWSI
002100     05  XWSI-WSP-CODE               PIC  X(01).                  CPWSXWSI
002200     05  XWSI-FUND-NEEDED            PIC  X(06).                  UCSD0102
002200*****05  XWSI-FUND-NEEDED            PIC  X(05).                  UCSD0102
002300     05  XWSI-RETURN-AREA.                                        CPWSXWSI
002400         10  XWSI-RETURN-CODE        PIC  X(01).                  CPWSXWSI
002500             88  XWSI-OK                             VALUE SPACE. CPWSXWSI
002600             88  XWSI-ABEND                          VALUE 'Z'.   CPWSXWSI
002700             88  XWSI-DATE-TOO-EARLY                 VALUE 'D'.   CPWSXWSI
002800             88  XWSI-DATE-TOO-LATE                  VALUE 'E'.   CPWSXWSI
002900             88  XWSI-NOT-FOUND                      VALUE 'N'.   CPWSXWSI
003000             88  XWSI-FUND-FOUND                     VALUE 'U'.   CPWSXWSI
003100             88  XWSI-FUNC-ERROR                     VALUE 'F'.   CPWSXWSI
003200         10  XWSI-WSP-TBL-SLOT       PIC S9(04)          COMP.    CPWSXWSI
003300         10  XWSI-FUND-SUB           PIC S9(04)          COMP.    CPWSXWSI
003400         10  XWSI-PROGRAM-NAME       PIC  X(30).                  CPWSXWSI
003500         10  XWSI-APPROP-ACCT        PIC  X(06).                  CPWSXWSI
003600         10  XWSI-GL-DESC            PIC  X(18).                  CPWSXWSI
003700         10  XWSI-BENEFITS-IND       PIC  X(01).                  CPWSXWSI
003800         10  XWSI-PGM-END-DATE-YMD   PIC  X(06).                  CPWSXWSI
003900         10  XWSI-CATEG-CODE         PIC  X(01).                  CPWSXWSI
004000         10  XWSI-BEGIN-DATE-YMD     PIC  X(06).                  CPWSXWSI
004100         10  XWSI-FUND-NUMBER        PIC  X(05).                  CPWSXWSI
004200         10  XWSI-SPLIT-PCT          PIC S9(01)V9(04)    COMP-3.  CPWSXWSI
004300     SKIP1                                                        CPWSXWSI
004400***** END OF CPWSXWSI ****************************************/   CPWSXWSI
