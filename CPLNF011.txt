000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNF011                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_P. THOMPSON_____ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*    Initial release of linkage copymember between calling   */   32021138
000700*    program and module PPFAU011.                            */   32021138
000800**************************************************************/   32021138
000900*                                                             *   CPLNF011
001000*  This copymember is one of the Full Accounting Unit modules *   CPLNF011
001100*  which campuses may need to modify to accomodate their own  *   CPLNF011
001200*  Chart of Accounts structure.                               *   CPLNF011
001300*                                                             *   CPLNF011
001400*  In particular, depending upon the need for validation at   *   CPLNF011
001500*  various levels, campuses may need fewer or more levels of  *   CPLNF011
001600*  validation failure.                                        *   CPLNF011
001700*                                                             *   CPLNF011
001800*  The base version indicates one possible level of failure:  *   CPLNF011
001900*    Level 1 - the Location is not valid campus or Systemwide,*   CPLNF011
001900*              the account is zeroes or the fund is zeros, or *   CPLNF011
001900*              the sub-account is not valid.                  *   CPLNF011
001900*              Per the failure, a PPPMSG number will be passed*   CPLNF011
001900*              via failure text; the number of returned       *   CPLNF011
001900*              messages will be in F011-MSG-COUNT.            *   CPLNF011
002100***************************************************************   CPLNF011
002200*                                                                 CPLNF011
002300*01  PPFAU011-INTERFACE.                                          CPLNF011
002400    05  F011-INPUTS.                                              CPLNF011
002500        10  F011-FAU                     PIC X(30).               CPLNF011
002500        10  F011-REQUESTOR               PIC X(02).               CPLNF011
002600    05  F011-OUTPUTS.                                             CPLNF011
002700        10  F011-RETURN-VALUE            PIC X(01).               CPLNF011
002800            88  F011-FAU-VALID             VALUE '0'.             CPLNF011
002900            88  F011-INVALID-LEVEL-1       VALUE '1'.             CPLNF011
003000            88  F011-ROUTINE-FAILURE       VALUE 'X'.             CPLNF011
003200        10  F011-FAILURE-TEXT            PIC X(30).               CPLNF011
003200        10  F011-RETURNED-FAU            PIC X(30).               CPLNF011
003200        10  F011-MSG-COUNT               PIC S9(4) COMP.          CPLNF011
003200        10  F011-MESSAGES.                                        CPLNF011
003200            15  FILLER OCCURS 4 TIMES.                            CPLNF011
002500                20  F011-MSG             PIC X(05).               CPLNF011
