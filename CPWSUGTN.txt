000100**************************************************************/   71921294
000200*  COPYMEMBER: CPWSUGTN                                      */   71921294
000300*  RELEASE: ___1294______ SERVICE REQUEST(S): ____17192____  */   71921294
000400*  NAME:_______JLT_______ MODIFICATION DATE:  ___02/10/00__  */   71921294
000500*  DESCRIPTION:                                              */   71921294
000600*  - INITIAL INSTALLATION OF COPYMEMBER.                     */   71921294
000700**************************************************************/   71921294
000800*                                                            */   CPWSUGTN
000900*    COPYID=CPWSUGTN                                              CPWSUGTN
001000*                                                            */   CPWSUGTN
001100*    THIS COPYMEMBER IS USED AS A WORK AREA TO STORE ALL     */   CPWSUGTN
001200*    GTN ENTRIES FOR THE EMPLOYEE WHICH ARE ASSOCIATED WITH  */   CPWSUGTN
001300*    THE BARGAINING UNIT 'PPPBUF' TABLE.                     */   CPWSUGTN
001400**************************************************************/   CPWSUGTN
001500                                                                  CPWSUGTN
001600*01  UGTN-UNIT-GTN-TABLE.                                         CPWSUGTN
001700                                                                  CPWSUGTN
001800*----> THE SET OF ALL GTNS FOR A BUF UNIT (OR UNITS IF ONE OR     CPWSUGTN
001900*----> MORE ARE "LINKED" TOGETHER) FOR WHICH THE EMPLOYEE         CPWSUGTN
002000*----> HAS A "G" BALANCE PRESENT OR FOR WHICH A "G" BALANCE       CPWSUGTN
002100*----> WAS JUST DELETED                                           CPWSUGTN
002200                                                                  CPWSUGTN
002300     05  UNIT-GTN-X       PIC S9(3) COMP-3 VALUE 0.               CPWSUGTN
002400     05  UNIT-GTNS-LOADED PIC S9(3) COMP-3 VALUE 0.               CPWSUGTN
002500     05  UNIT-GTNS-MAX    PIC S9(3) COMP-3 VALUE 100.             CPWSUGTN
002600     05  UNIT-GTN-TBL.                                            CPWSUGTN
002700         10  UNIT-GTN-ENTRY               OCCURS 100.             CPWSUGTN
002800             15  UNIT-GTN-DED-NO    PIC 9(03).                    CPWSUGTN
002900             15  UNIT-GTN-FLG       PIC X(01).                    CPWSUGTN
003000                 88 UNIT-GTN-FLG-UDUE     VALUE 'U'.              CPWSUGTN
003100                 88 UNIT-GTN-FLG-AGENCY-FEE VALUE 'F'.            CPWSUGTN
003200                 88 UNIT-GTN-FLG-CHARITY  VALUE 'C'.              CPWSUGTN
003300             15  UNIT-GTN-HAS-SW    PIC X(01).                    CPWSUGTN
003400                 88  UNIT-GTN-HAS-OFF-DEL VALUE 'D'.              CPWSUGTN
003500                 88  UNIT-GTN-HAS-ON      VALUE 'Y'.              CPWSUGTN
003600                 88  UNIT-GTN-HAS-ON-NEW  VALUE 'A'.              CPWSUGTN
003700             15  UNIT-GTN-CORR-APPT PIC 9(02).                    CPWSUGTN
003800             15  UNIT-GTN-BUFT-INDEX PIC 9(04).                   CPWSUGTN
003900     05  WRK-UNIT-GTN-ENTRY.                                      CPWSUGTN
004000         10  WRK-GTN-999        PIC 9(03).                        CPWSUGTN
004100         10  WRK-BUF-GTN-FLG    PIC X(01).                        CPWSUGTN
004200         10  WRK-HAS-SW         PIC X(01).                        CPWSUGTN
004300                                                                  CPWSUGTN
004400*            <<<<  END OF CPWSUGTN  >>>>                          CPWSUGTN
