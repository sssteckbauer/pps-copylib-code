000100*    COPYID=CPOTXUCS                                              CPOTXUCS
000200                             IBM-370.                             CPOTXUCS
000300                                                                  CPOTXUCS
000400*                                                                *CPOTXUCS
000500******************************************************************CPOTXUCS
000600*  ****                                                    ****  *CPOTXUCS
000700*  ****  CONFIDENTIAL INFORMATION                          ****  *CPOTXUCS
000800*  ****                                                    ****  *CPOTXUCS
000900*  ****  THIS PROGRAM IS CONFIDENTIAL AND IS THE PROPERTY  ****  *CPOTXUCS
001000*  ****  OF THE UNIVERSITY OF CALIFORNIA; THIS PROGRAM IS  ****  *CPOTXUCS
001100*  ****  NOT TO BE COPIED, REPRODUCED, OR TRANSMITTED IN   ****  *CPOTXUCS
001200*  ****  ANY FORM, BY ANY MEANS, IN WHOLE OR PART, WITHOUT ****  *CPOTXUCS
001300*  ****  THE WRITTEN PERMISSION FROM THE BOARD OF REGENTS, ****  *CPOTXUCS
001400*  ****  UNIVERSITY OF CALIFORNIA.                         ****  *CPOTXUCS
001500*  ****                                                    ****  *CPOTXUCS
001600*  ***************************************************************CPOTXUCS
001700*                                                                *CPOTXUCS
