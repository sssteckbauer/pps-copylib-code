000100******************************************************************36070535
000200*  COPYMEMBER: CPLNKACA                                          *36070535
000300*  RELEASE: ____0535____  SERVICE REQUEST(S): ____3607____       *36070535
000400*  NAME:  ______JAG_____  CREATION DATE:      __02/06/91__       *36070535
000500*  DESCRIPTION:                                                  *36070535
000600*    LINKAGE FOR PPACAUTL SUBROUTINE.                            *36070535
000700******************************************************************CPLNKACA
000800*    COPYID=CPLNKACA                                              CPLNKACA
000900*01  PPACAUTL-INTERFACE.                                          CPLNKACA
001000*                                                                *CPLNKACA
001100******************************************************************CPLNKACA
001200*    P P A X D U T L   I N T E R F A C E                         *CPLNKACA
001300******************************************************************CPLNKACA
001400*                                                                *CPLNKACA
001500     05  PPACAUTL-ERROR-SW       PICTURE X(1).                    CPLNKACA
001600     88  PPACAUTL-ERROR VALUE 'Y'.                                CPLNKACA
001700     05  PPACAUTL-EMPLOYEE-ID   PICTURE X(9).                     CPLNKACA
