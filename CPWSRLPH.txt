000100**************************************************************/  *36090513
000200*  COPYMEMBER: CPWSRLPH                                      */  *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____   */  *36090513
000400*  NAME: ______JAG______  CREATION DATE: ____11/05/90_____   */  *36090513
000500*  DESCRIPTION:                                              */  *36090513
000600*  - WORKING-STORAGE LAYOUT OF LPH TABLE ROW.                */  *36090513
000700**************************************************************/  *36090513
000800*    COPYID=CPWSRLPH                                              CPWSRLPH
000810*01  LPH-ROW.                                                     CPWSRLPH
000900     10 EMPLOYEE-ID          PIC X(9).                            CPWSRLPH
001000     10 LVPLAN-KEY           PIC X(6).                            CPWSRLPH
001100     10 LVPLAN-STARTDATE     PIC X(10).                           CPWSRLPH
001200     10 LVPLAN-CODE          PIC X(1).                            CPWSRLPH
001300******************************************************************CPWSRLPH
