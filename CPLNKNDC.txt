000000**************************************************************/   36490863
000002*  COPYMEMBER: CPLNKNDC                                      */   36490863
000004*  RELEASE: ___0863______ SERVICE REQUEST(S): _____3649____  */   36490863
000005*  NAME:_______DDM_______ MODIFICATION DATE:  __01/27/94____ */   36490863
000006*  DESCRIPTION: LINKAGE FOR NEW PPNDCUTL SUBROUTINE          */   36490863
000007*    FOR DB2 UPDATE COMPLEX, CREATED FOR PAN IMPLEMENTATION. */   36490863
000008******************************************************************36490863
000900*    COPYID=CPLNKNDC                                              CPLNKNDC
001000*01  PPNDCUTL-INTERFACE.                                          CPLNKNDC
001100*                                                                 CPLNKNDC
001200***************************************************************** CPLNKNDC
001300*    P P N D C U T L   I N T E R F A C E                          CPLNKNDC
001400***************************************************************** CPLNKNDC
001500*                                                                 CPLNKNDC
001600     05  PPNDCUTL-ERROR-SW       PICTURE  X(01).                  CPLNKNDC
001700         88  PPNDCUTL-ERROR                         VALUE 'Y'.    CPLNKNDC
001800     05  PPNDCUTL-EMPLOYEE-ID    PICTURE  X(09).                  CPLNKNDC
001900     05  PPNDCUTL-ROW-COUNT      PICTURE S9(04)     COMP.         CPLNKNDC
