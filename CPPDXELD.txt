000000**************************************************************/   28521025
000001*  COPYMEMBER: CPPDXELD                                      */   28521025
000002*  RELEASE: ___1025______ SERVICE REQUEST(S): ____12852____  */   28521025
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___08/01/95__  */   28521025
000004*  DESCRIPTION:                                              */   28521025
000005*  - DATE CONVERSION PROJECT:                                */   28521025
000006*    RENAMED ISO-ZERO-DATE TO   XDTS-ISO-ZERO-DATE.          */   28521025
000005*  - REFERENCE CPPDXDC3 FOR DATE CONVERSIONS.                */   28521025
000007**************************************************************/   28521025
000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPPDXELD                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    EDIT LEAVE DATES                                        *|*36410717
000900*|*    INCLUDED IN PROGRAMS PPEA007 AND PPEC129                *|*36410717
001000*|**************************************************************|*36410717
001100*----------------------------------------------------------------*36410717
001200*XXXX-EDIT-LEAVE-DATES.                                           CPPDXELD
001300                                                                  CPPDXELD
001400*****IF  LOA-BEGIN-DATE = ISO-ZERO-DATE                           28521025
001410     IF  LOA-BEGIN-DATE = XDTS-ISO-ZERO-DATE                      28521025
001500         ADD 1                     TO KMTA-CTR                    CPPDXELD
001600         MOVE M08009               TO KMTA-MSG-NUMBER   (KMTA-CTR)CPPDXELD
001700         MOVE 6                    TO KMTA-LENGTH       (KMTA-CTR)CPPDXELD
001800*****    MOVE LOA-BEGIN-DATE       TO DB2-DATE                    28521025
001900*****    PERFORM CONVERT-DB2-DATE                                 28521025
002000*****    MOVE WSX-STD-DATE         TO KMTA-DATA-FIELD   (KMTA-CTR)28521025
002010         MOVE LOA-BEGIN-DATE       TO XDC3-ISO-DATE               28521025
002020         PERFORM XDC3-CONVERT-ISO-TO-STD                          28521025
002030         MOVE XDC3-STD-DATE        TO KMTA-DATA-FIELD   (KMTA-CTR)28521025
002100         MOVE E0137                TO KMTA-FIELD-NUM    (KMTA-CTR)CPPDXELD
002200         MOVE WS-ROUTINE-TYPE      TO KMTA-ROUTINE-TYPE (KMTA-CTR)CPPDXELD
002300         MOVE WS-ROUTINE-NUM       TO KMTA-ROUTINE-NUM  (KMTA-CTR)CPPDXELD
002400     END-IF.                                                      CPPDXELD
002500                                                                  CPPDXELD
002600*****IF  LOA-RETURN-DATE = ISO-ZERO-DATE                          28521025
002610     IF  LOA-RETURN-DATE = XDTS-ISO-ZERO-DATE                     28521025
002700         ADD 1                     TO KMTA-CTR                    CPPDXELD
002800         MOVE M08010               TO KMTA-MSG-NUMBER   (KMTA-CTR)CPPDXELD
002900*****    MOVE LOA-RETURN-DATE      TO DB2-DATE                    28521025
003000*****    PERFORM CONVERT-DB2-DATE                                 28521025
003100*****    MOVE WSX-STD-DATE         TO KMTA-DATA-FIELD   (KMTA-CTR)28521025
003110         MOVE LOA-RETURN-DATE      TO XDC3-ISO-DATE               28521025
003120         PERFORM XDC3-CONVERT-ISO-TO-STD                          28521025
003130         MOVE XDC3-STD-DATE        TO KMTA-DATA-FIELD   (KMTA-CTR)28521025
003200         MOVE E0138                TO KMTA-FIELD-NUM    (KMTA-CTR)CPPDXELD
003300         MOVE 6                    TO KMTA-LENGTH       (KMTA-CTR)CPPDXELD
003400         MOVE WS-ROUTINE-TYPE      TO KMTA-ROUTINE-TYPE (KMTA-CTR)CPPDXELD
003500         MOVE WS-ROUTINE-NUM       TO KMTA-ROUTINE-NUM  (KMTA-CTR)CPPDXELD
003600     END-IF.                                                      CPPDXELD
003700                                                                  CPPDXELD
003800     IF      LOA-BEGIN-DATE  NOT <  LOA-RETURN-DATE               CPPDXELD
003900*****    AND LOA-BEGIN-DATE  NOT =  ISO-ZERO-DATE                 28521025
004000*****    AND LOA-RETURN-DATE NOT =  ISO-ZERO-DATE                 28521025
004010         AND LOA-BEGIN-DATE  NOT =  XDTS-ISO-ZERO-DATE            28521025
004020         AND LOA-RETURN-DATE NOT =  XDTS-ISO-ZERO-DATE            28521025
004100         ADD 1                     TO KMTA-CTR                    CPPDXELD
004200         MOVE M08011               TO KMTA-MSG-NUMBER   (KMTA-CTR)CPPDXELD
004300*****    MOVE LOA-BEGIN-DATE       TO DB2-DATE                    28521025
004400*****    PERFORM CONVERT-DB2-DATE                                 28521025
004500*****    MOVE WSX-STD-DATE         TO KMTA-DATA-FIELD   (KMTA-CTR)28521025
004510         MOVE LOA-BEGIN-DATE       TO XDC3-ISO-DATE               28521025
004520         PERFORM XDC3-CONVERT-ISO-TO-STD                          28521025
004530         MOVE XDC3-STD-DATE        TO KMTA-DATA-FIELD   (KMTA-CTR)28521025
004600         MOVE E0137                TO KMTA-FIELD-NUM    (KMTA-CTR)CPPDXELD
004700         MOVE 6                    TO KMTA-LENGTH       (KMTA-CTR)CPPDXELD
004800         MOVE WS-ROUTINE-TYPE      TO KMTA-ROUTINE-TYPE (KMTA-CTR)CPPDXELD
004900         MOVE WS-ROUTINE-NUM       TO KMTA-ROUTINE-NUM  (KMTA-CTR)CPPDXELD
005000                                                                  CPPDXELD
005100         ADD 1                     TO KMTA-CTR                    CPPDXELD
005200         MOVE M08011               TO KMTA-MSG-NUMBER   (KMTA-CTR)CPPDXELD
005300*****    MOVE LOA-RETURN-DATE      TO DB2-DATE                    28521025
005400*****    PERFORM CONVERT-DB2-DATE                                 28521025
005500*****    MOVE WSX-STD-DATE         TO KMTA-DATA-FIELD   (KMTA-CTR)28521025
005510         MOVE LOA-RETURN-DATE      TO XDC3-ISO-DATE               28521025
005520         PERFORM XDC3-CONVERT-ISO-TO-STD                          28521025
005530         MOVE XDC3-STD-DATE        TO KMTA-DATA-FIELD   (KMTA-CTR)28521025
005600         MOVE E0138                TO KMTA-FIELD-NUM    (KMTA-CTR)CPPDXELD
005700         MOVE 6                    TO KMTA-LENGTH       (KMTA-CTR)CPPDXELD
005800         MOVE WS-ROUTINE-TYPE      TO KMTA-ROUTINE-TYPE (KMTA-CTR)CPPDXELD
005900         MOVE WS-ROUTINE-NUM       TO KMTA-ROUTINE-NUM  (KMTA-CTR)CPPDXELD
006000     END-IF.                                                      CPPDXELD
006100                                                                  CPPDXELD
006200******************************************************************CPPDXELD
006300**    E N D   O F   C O P Y   M E M B E R - - - CPPDXELD - - -   *CPPDXELD
006400******************************************************************CPPDXELD
