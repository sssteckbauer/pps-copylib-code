000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWSPGTA                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*  - THIS COPYMEMBER DEFINES THE ARRAY FOR THE               *|*36410717
000900*|*        PROCESSING GROUP TABLE                              *|*36410717
001000*|**************************************************************|*36410717
001100*----------------------------------------------------------------*36410717
001200*    COPYID=CPWSPGTA                                              CPWSPGTA
001300*    THIS MODULE WILL BE USED TO STORE THE PROCESSING GROUPS      CPWSPGTA
001400*                                                                 CPWSPGTA
001500*01  XPGT-PROCESSING-GROUP-TABLE-ARRAY.                           CPWSPGTA
001600     05  XPGT-ARRAY-CTR                    PIC S9(03).            CPWSPGTA
001700     05  XPGT-PROCESS-ID                   PIC X(02).             CPWSPGTA
001800     05  XPGT-DATA OCCURS 200 TIMES.                              CPWSPGTA
001900         10  XPGT-PROCESSING-GROUP         PIC 9(03).             CPWSPGTA
002000         10  XPGT-ROUTINE-TYPE             PIC X(01).             CPWSPGTA
002100         10  XPGT-ROUTINE-NUMBER           PIC 9(03).             CPWSPGTA
002200         10  XPGT-ROUTINE-SEQUENCE         PIC 9(04).             CPWSPGTA
002300         10  XPGT-PROCESS-TRIGGERS OCCURS 60 TIMES.               CPWSPGTA
002400             15  XPGT-TRIGGER-TYPE         PIC X(01).             CPWSPGTA
002500             15  XPGT-TRIGGER-NUMBER       PIC 9(03).             CPWSPGTA
002600*                                                                 CPWSPGTA
002700*****************   END OF COPYBOOK CPWSPGTA *********************CPWSPGTA
