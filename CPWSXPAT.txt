000100**************************************************************/   36070542
000200*  COPYMEMBER: CPWSXPAT                                      */   36070542
000300*  RELEASE # ____0542____ SERVICE REQUEST NO(S)___3607_______*/   36070542
000400*  NAME __J_LUCAS______   MODIFICATION DATE ____03/04/91_____*/   36070542
000500*  DESCRIPTION                                               */   36070542
000600*   -ADDED DESCRIPTIONS FOR NEW ACTION CODES                 */   36070542
000700**************************************************************/   36070542
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXPAT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000600*    COPYID=CPWSXPAT                                              CPWSXPAT
000200*    *************************************************************CPWSXPAT
000300*            PERSONNEL ACTION DESCRIPTION TABLE                   CPWSXPAT
000400*    *************************************************************CPWSXPAT
000500*01  PERSONNEL-ACTIONS.                                           30930413
000600     05  FILLER  PIC X(21) VALUE 'INITIAL EMPLOYMENT'.            CPWSXPAT
000700     05  FILLER  PIC X(21) VALUE 'EMPL. WITH PRIOR SERV'.         CPWSXPAT
002200*****05  FILLER  PIC X(21) VALUE 'CHANGE IN PAY RATE'.            36070542
002300     05  FILLER  PIC X(21) VALUE SPACES.                          36070542
000900     05  FILLER  PIC X(21) VALUE 'MERIT INCREASE'.                CPWSXPAT
002500*****05  FILLER  PIC X(21) VALUE 'JOB RECLASSIFICATION'.          36070542
002600     05  FILLER  PIC X(21) VALUE SPACES.                          36070542
001100     05  FILLER  PIC X(21) VALUE 'SEPARATION'.                    CPWSXPAT
001200     05  FILLER  PIC X(21) VALUE 'LEAVE WITH PAY'.                CPWSXPAT
001300     05  FILLER  PIC X(21) VALUE 'LEAVE WITHOUT PAY'.             CPWSXPAT
001400     05  FILLER  PIC X(21) VALUE 'RETURN FROM LEAVE'.             CPWSXPAT
001500     05  FILLER  PIC X(21) VALUE 'PROMOTION'.                     CPWSXPAT
001600     05  FILLER  PIC X(21) VALUE 'DEMOTION'.                      CPWSXPAT
003300*****05  FILLER  PIC X(21) VALUE 'TITLE TRANSFER'.                36070542
003400     05  FILLER  PIC X(21) VALUE 'LATERAL TRANSFER'.              36070542
001800     05  FILLER  PIC X(21) VALUE 'ADDITIONAL EMPLOYMENT'.         CPWSXPAT
003600*****05  FILLER  PIC X(21) VALUE 'DIFF. SERIES/N SERIES'.         36070542
002000     05  FILLER  PIC X(21) VALUE 'ACADEMIC STATUS CHG'.           CPWSXPAT
003800     05  FILLER  PIC X(21) VALUE 'CHANGE IN VISA STATUS'.         CPWSXPAT
002100     05  FILLER  PIC X(21) VALUE 'CHANGE TO % FULL TIME'.         CPWSXPAT
002200     05  FILLER  PIC X(21) VALUE 'APPOINTMENT RENEWAL'.           CPWSXPAT
002300     05  FILLER  PIC X(21) VALUE 'CHANGE IN FUND SOURCE'.         CPWSXPAT
002400     05  FILLER  PIC X(21) VALUE 'DEPARTMENTAL TRANSFER'.         CPWSXPAT
004300*****05  FILLER  PIC X(21) VALUE 'ERROR CORRECTION'.              36070542
004400     05  FILLER  PIC X(21) VALUE 'OTHER EMPLOYEE LEVEL'.          36070542
002600     05  FILLER  PIC X(21) VALUE 'INTERCAMPUS ACTION'.            CPWSXPAT
004600*****05  FILLER  PIC X(21) VALUE 'OTHER'.                         36070542
004700     05  FILLER  PIC X(21) VALUE SPACES.                          36070542
004800*****05  FILLER  PIC X(21) VALUE  SPACES.                         36070542
004900     05  FILLER  PIC X(21) VALUE 'CHANGE IN BELI'.                36070542
005000*****05  FILLER  PIC X(21) VALUE  SPACES.                         36070542
005100     05  FILLER  PIC X(21) VALUE 'LEAVE PLAN CODE CHNGE'.         36070542
005200*****05  FILLER  PIC X(21) VALUE  SPACES.                         36070542
005300     05  FILLER  PIC X(21) VALUE 'MASS TITLE CODE CHGE'.          36070542
005400*****05  FILLER  PIC X(21) VALUE  SPACES.                         36070542
005500     05  FILLER  PIC X(21) VALUE 'ADD STIPEND'.                   36070542
005600*****05  FILLER  PIC X(21) VALUE  SPACES.                         36070542
005700     05  FILLER  PIC X(21) VALUE 'MERIT MARK-UP'.                 36070542
003300     05  FILLER  PIC X(21) VALUE 'RANGE ADJUSTMENT'.              CPWSXPAT
005900*****05  FILLER  PIC X(21) VALUE 'MASS DATA CHANGE'.              36070542
006000     05  FILLER  PIC X(21) VALUE SPACES.                          36070542
003500     05  FILLER  PIC X(21) VALUE 'MASS PURGE'.                    CPWSXPAT
006200     05  FILLER  PIC X(21) VALUE SPACES.                          36070542
006300     05  FILLER  PIC X(21) VALUE SPACES.                          36070542
006400     05  FILLER  PIC X(21) VALUE 'MANUAL RANGE ADJ'.              36070542
006500     05  FILLER  PIC X(21) VALUE 'TERMINAL APPT'.                 36070542
006600     05  FILLER  PIC X(21) VALUE 'SYSTEMWIDE TRANSFER'.           36070542
006700     05  FILLER  PIC X(21) VALUE 'TRANSFER STATE/LAB'.            36070542
006800     05  FILLER  PIC X(21) VALUE 'RECLASS UPWARD'.                36070542
006900     05  FILLER  PIC X(21) VALUE 'RECLASS DOWNWARD'.              36070542
007000     05  FILLER  PIC X(21) VALUE 'RECLASS LATERAL'.               36070542
007100     05  FILLER  PIC X(21) VALUE 'OTHER APPT. LEVEL'.             36070542
007200     05  FILLER  PIC X(21) VALUE 'SIX MONTH INCREASE'.            36070542
007300     05  FILLER  PIC X(21) VALUE 'EXCEPTIONAL INCREASE'.          36070542
007400     05  FILLER  PIC X(21) VALUE 'OTHER CHG IN PAY RATE'.         36070542
007500     05  FILLER  PIC X(21) VALUE 'CASUAL INCREASE'.               36070542
007600     05  FILLER  PIC X(21) VALUE 'NEGOTIATED SALARY CHG'.         36070542
007700     05  FILLER  PIC X(21) VALUE 'CHG IN PERCENT TIME'.           36070542
007800     05  FILLER  PIC X(21) VALUE 'MASS ACCT/FUND CHG'.            36070542
007900     05  FILLER  PIC X(21) VALUE 'ADD STIPEND'.                   36070542
008000     05  FILLER  PIC X(21) VALUE 'MASS DUC CHANGE'.               36070542
008100     05  FILLER  PIC X(21) VALUE 'OTHER DIST LEVEL'.              36070542
008200     05  FILLER  PIC X(21) VALUE 'MED CENTER TRANSFER'.           36070542
008300     05  FILLER  PIC X(21) VALUE 'CASUAL TO CAREER'.              36070542
008400     05  FILLER  PIC X(21) VALUE 'PHASED RETIREMENT'.             36070542
003600*                                                                 CPWSXPAT
