000010**************************************************************/   36430795
000020*  COPYLIB: UCWSABND                                         */   36430795
000030*  RELEASE # ___0795___   SERVICE REQUEST NO(S)____3643______*/   36430795
000040*  NAME __MLG__________   MODIFICATION DATE ____07/15/93_____*/   36430795
000050*  DESCRIPTION                                               */   36430795
000060*                                                            */   36430795
000070*       ABEND PROGRAM INTERFACE EXTERNAL AREA - DATA IS SET  */   36430795
000080*       BY THE ABENDING PROGRAM AND PASSED TO UCABEND        */   36430795
000090*                                                            */   36430795
000091*       PROCEDURE DIVISION COPYLIB MEMBER UCPDABND IS THE    */   36430795
000092*       COMPANION PIECE TO THIS ONE.                         */   36430795
000093*                                                            */   36430795
000094**************************************************************/   36430795
000100*01  UCWSABND  EXTERNAL.                                          UCWSABND
000200     05  UCWSABND-EXT-AREA                 PIC X(1024).           UCWSABND
000300     05  UCWSABND-EXT-DATA REDEFINES UCWSABND-EXT-AREA.           UCWSABND
000310         10  UCWSABND-PGM-CALL-NAME        PIC X(08).             UCWSABND
000400         10  UCWSABND-MSG-DATA             PIC X(70).             UCWSABND
000410         10  UCWSABND-ABEND-AREA.                                 UCWSABND
000500             15  UCWSABND-ABEND-DATA       PIC X(80) OCCURS 2.    UCWSABND
000600         10  UCWSABND-ABENDING-PGM         PIC X(08).             UCWSABND
000700         10  UCWSABND-FILLER               PIC X(778).            UCWSABND
000800***************    END OF SOURCE - UCWSABND    *******************UCWSABND
