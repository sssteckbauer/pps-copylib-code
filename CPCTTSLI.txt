000000**************************************************************/   73151304
000001*  COPYMEMBER: CPCTTSLI                                      */   73151304
000002*  RELEASE: ___1304______ SERVICE REQUEST(S): ____17315____  */   73151304
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___06/05/00__  */   73151304
000004*  DESCRIPTION:                                              */   73151304
000005*  - NEW COPY MEMBER FOR DATA INPUT TO THE NEW TITLE SUB     */   73151304
000006*    LOCATION TABLE, PPPTSL                                  */   73151304
000007**************************************************************/   73151304
006400*    COPYID=CPCTTSLI                                              CPCTTSLI
011500*01  TITLE-CODE-TSL-TABLE-INPUT.                                  CPCTTSLI
019630     05  TSLI-TITLE-CODE                 PIC X(04).               CPCTTSLI
019631     05  TSLI-EFFECTIVE-DATE             PIC X(08).               CPCTTSLI
019670     05  TSLI-SUB-LOCATION               PIC X(02).               CPCTTSLI
019730     05  TSLI-PAY-REP-CODE               PIC X(03).               CPCTTSLI
019740     05  TSLI-OVRTM-EXMPT-CD             PIC X(01).               CPCTTSLI
019750     05  TSLI-FLSA-STATUS-CD             PIC X(01).               CPCTTSLI
019760     05  TSLI-SIX-MONTH-CD               PIC X(01).               CPCTTSLI
019761     05  TSLI-RATE-LOOKUP-CD             PIC X(01).               CPCTTSLI
019770     05  TSLI-JOB-GROUP-ID               PIC X(03).               CPCTTSLI
019780     05  TSLI-UPDT-SOURCE                PIC X(08).               CPCTTSLI
