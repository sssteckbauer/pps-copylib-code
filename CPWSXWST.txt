000100**************************************************************/   36060628
000200*  COPYMEMBER: CPWSXWST                                      */   36060628
000300*  RELEASE # ____0628____ SERVICE REQUEST NO(S)___3606_______*/   36060628
000400*  NAME __ STEINITZ ___   MODIFICATION DATE ____01/10/92_____*/   36060628
000500*  DESCRIPTION                                               */   36060628
000600*   - ADDED CATEGORY CODE (FED, UNIV, STATE)                 */   36060628
000700*   - FORMATTED COBOL CONSISTENTLY; CORRECTED SPELLING       */   36060628
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXWST                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   14790352
000200*  COPY MODULE:  WSPTABLE                                    */   14790352
000300*  RELEASE # ___0352___   SERVICE REQUEST NO(S) ____1479____ */   14790352
000400*  NAME ___C.JOHNEN____   MODIFICATION DATE ____02/29/88_____*/   14790352
000500*                                                            */   14790352
000600*  DESCRIPTION:                                              */   14790352
000700*       NEW COPY MODULE.                                     */   14790352
000800*       ADDED FOR WORK STUDY PROGRAMS.  DESCRIBES TABLE      */   14790352
000900*       LOADED FROM  CONTROL TABLE 24 (WSP) RECORDS          */   14790352
001000*       AND DIFFERENT FIELDS USED WITH THAT TABLE.           */   14790352
001100**************************************************************/   14790352
002600*    COPYID=CPWSXWST                                              36060628
002700*    COPYID=WSPTABLE                                              36060628
001300*01  WSPTABLE.                                                    30930413
001400*-----------------------------------------------------------------CPWSXWST
001500*       WORK STUDY PROGRAM TABLE RECORD.                          CPWSXWST
001600*-----------------------------------------------------------------CPWSXWST
003200     03  WSP-TBL                 OCCURS 26.                       36060628
003300       05  WSP-DELETE                PICTURE  X(01).              36060628
003400       05  WSP-KEY.                                               36060628
003500         10  WSP-KEY-CONSTANT        PICTURE  X(04).              36060628
003600         10  FILLER                  PICTURE  X(08).              36060628
003700         10  WSP-PROGRAM-CODE        PICTURE  X(01).              36060628
002300       05  WSP-PROGRAM-DATA.                                      CPWSXWST
003900         10  WSP-PROGRAM-NAME        PICTURE  X(30).              36060628
004000         10  WSP-APPROP-ACCT         PICTURE  X(06).              36060628
004100         10  WSP-GL-DESC             PICTURE  X(18).              36060628
004200         10  WSP-BENEFITS-IND        PICTURE  X(01).              36060628
004300*************88  WSP-CHARGE-PROVISION                  VALUE 'Y'. 36060628
004400*************88  WSP-CHARGE-WS-FUND                    VALUE 'N'. 36060628
004500         10  WSP-PROGRAM-END-DATE    PICTURE  X(06).              36060628
003100******   FORMAT = YYMMDD                                          CPWSXWST
003200******   999999 = INDEFINITE                                      CPWSXWST
004800         10  WSP-CATEG-CODE          PICTURE  X(01).              36060628
004900             88  WSP-FEDERAL                           VALUE 'F'. 36060628
005000             88  WSP-UNIVERSITY                        VALUE 'U'. 36060628
005100             88  WSP-STATE                             VALUE 'S'. 36060628
005200         10  FILLER                  PICTURE  X(12).              36060628
005300*********10  FILLER                  PICTURE  X(13).              36060628
005400       05  WSP-VAR-NR                PICTURE  9(01).              36060628
005500*****    INDICATES NUMBER OF ACTIVE FUND DATA OCCURRENCES.        36060628
003600       05  WSP-FUND.                                              CPWSXWST
005700         10  WSP-FUND-DATA                             OCCURS 8.  36060628
005800             15  WSP-FUND-NUMBER     PICTURE  X(05).              36060628
005900             15  WSP-SPLIT-PERCENTAGE                             36060628
006000                                     PICTURE  9(01)V9(04).        36060628
006100             15  WSP-BEGIN-DATE      PICTURE  X(06).              36060628
004100******  FORMAT = YYMMDD                                           CPWSXWST
004200       05  WSP-UPDATE-INFO.                                       CPWSXWST
006400         10  WSP-LAST-ACTION         PICTURE  X(01).              36060628
006500         10  WSP-LAST-UPDATE         PICTURE  9(06).              36060628
004500******  FORMAT = YYMMDD                                           CPWSXWST
006700***************************************************************** 36060628
006800****   REMAINING LINES OF PRE-REL 0628 VERSION HAVE BEEN DELETED* 36060628
006900***************************************************************** 36060628
