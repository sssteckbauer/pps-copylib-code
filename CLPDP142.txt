000100*==========================================================%      UCSD0006
000200*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD0006
000300*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD0006     =%      UCSD0006
000400*==========================================================%      UCSD0006
000500*==========================================================%      UCSD0006
000600*=    COPYMEMBER: CLPDP120                                =%      UCSD0006
000700*=    CHANGE # 0006         PROJ. REQUEST:                =%      UCSD0006
000800*=    NAME: JIM PIERCE      MODIFICATION DATE: 04/13/91   =%      UCSD0006
000900*=                                                        =%      UCSD0006
001000*=    DESCRIPTION:                                        =%      UCSD0006
001100*=     ADD TOT-CHANGE-RCS-IN  TO CONTROL REPORT           =%      UCSD0006
001200*==========================================================%      UCSD0006
001300*                                                                 CLPDP120
000100*                                                                 CLPDP142
000200*      CONTROL REPORT PPP142CR                                    CLPDP142
000300*                                                                 CLPDP142
000400*                                                                 CLPDP142
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP142
000600                                                                  CLPDP142
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP142
000800                                                                  CLPDP142
000900     MOVE 'PPP142CR'             TO CR-HL1-RPT.                   CLPDP142
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP142
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP142
001200                                                                  CLPDP142
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP142
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP142
001500                                                                  CLPDP142
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP142
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP142
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP142
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP142
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP142
002100                                                                  CLPDP142
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP142
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP142
002400                                                                  CLPDP142
002500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP142
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP142
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP142
002800                                                                  CLPDP142
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP142
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP142
003100                                                                  CLPDP142
003110     ACCEPT TIME-WORK-AREA FROM TIME.                             CLPDP142
003200*    MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP142
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP142
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP142
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP142
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP142
003700                                                                  CLPDP142
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP142
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP142
004000*                                                                 CLPDP142
004100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP142
004200*                                                                 CLPDP142
004300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP142
004400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP142
004500*                                                                 CLPDP142
004600     MOVE 'SEG CHANGE FILE'      TO CR-DL9-FILE.                  CLPDP142
004700     MOVE ' READ'                TO CR-DL9-ACTION.                CLPDP142
004800     MOVE UPDATE-COUNT           TO CR-DL9-VALUE.                 CLPDP142
004900     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP142
005000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP142
006500*                                                                 UCSD0006
006600     MOVE 'EDB CHANGE FILE'      TO CR-DL9-FILE.                  UCSD0006
006700     MOVE ' READ'                TO CR-DL9-ACTION.                UCSD0006
006800     MOVE TOT-CHANGE-RCS-IN      TO CR-DL9-VALUE.                 UCSD0006
006900     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 UCSD0006
007000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 UCSD0006
005100*                                                                 CLPDP142
005200*                                                                 CLPDP142
005300*                                                                 CLPDP142
005400     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP142
005500     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP142
005600                                                                  CLPDP142
005700     CLOSE CONTROLREPORT.                                         CLPDP142
