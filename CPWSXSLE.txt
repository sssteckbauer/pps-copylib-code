000010**************************************************************/   63381723
000020*  COPYMEMBER: CPWSXSLE                                      */   63381723
000030*  RELEASE: ___1723______ SERVICE REQUEST(S): ____16338____  */   63381723
000040*  NAME:______RIDER______ CREATION DATE:      ___08/31/06__  */   63381723
000050*  DESCRIPTION:                                              */   63381723
000060*  - SLIS INTERFACE RECORD FOR THE PAR EARNINGS DATA.        */   63381723
000070*                                                            */   63381723
000080**************************************************************/   63381723
000100*01  XSLE-RECORD.                                                 CPWSXSLE
000200     05  XSLE-LOCATION               PIC X(2).                    CPWSXSLE
000300     05  XSLE-ID-NO                  PIC X(9).                    CPWSXSLE
000400     05  XSLE-PAY-CYCLE              PIC X(2).                    CPWSXSLE
000500     05  XSLE-PAY-END-DATE           PIC X(10).                   CPWSXSLE
000600     05  XSLE-PRI-GRS-CTL            PIC 9(3).                    CPWSXSLE
000700     05  XSLE-TRANS-SEQ-CODE         PIC X(2).                    CPWSXSLE
000800     05  XSLE-ENTRY-SEQ-NO           PIC 9(3).                    CPWSXSLE
000900     05  XSLE-TTL-CD                 PIC X(4).                    CPWSXSLE
001000     05  XSLE-APPT-TYPE              PIC X(1).                    CPWSXSLE
001100     05  XSLE-DIST-NO                PIC X(2).                    CPWSXSLE
001200     05  XSLE-UC-LOC-2               PIC X(1).                    CPWSXSLE
001300     05  XSLE-UC-LOC-3               PIC X(1).                    CPWSXSLE
001400     05  XSLE-ACCOUNT                PIC X(6).                    CPWSXSLE
001500     05  XSLE-FUND                   PIC X(5).                    CPWSXSLE
001600     05  XSLE-SUB                    PIC X(2).                    CPWSXSLE
001700     05  XSLE-SAU-CODE               PIC X(1).                    CPWSXSLE
001800     05  XSLE-FAU                    PIC X(30).                   CPWSXSLE
001900     05  XSLE-DOS                    PIC X(3).                    CPWSXSLE
002000     05  XSLE-DOS-DESCRIPTION        PIC X(25).                   CPWSXSLE
002100     05  XSLE-DOS-FCP-DOS            PIC X(2).                    CPWSXSLE
002200     05  XSLE-PERIOD-END-DATE        PIC X(10).                   CPWSXSLE
002300     05  XSLE-PAYRATE                PIC -9(5).9(4).              CPWSXSLE
002400     05  XSLE-RATE-TYPE              PIC X(1).                    CPWSXSLE
002500     05  XSLE-RATE-CODE              PIC X(1).                    CPWSXSLE
002600     05  XSLE-EARN-AMT               PIC -9(5).99.                CPWSXSLE
002700     05  XSLE-TIME-HRS               PIC -9(3).99.                CPWSXSLE
002800     05  XSLE-EARN-PCT               PIC -9.9(4).                 CPWSXSLE
002900     05  XSLE-INTRANS-CD             PIC X(2).                    CPWSXSLE
003000     05  XSLE-EARN-UCRS-IND          PIC X(1).                    CPWSXSLE
003100     05  XSLE-ACC-MGE-B              PIC X(1).                    CPWSXSLE
003200     05  FILLER                      PIC X(36).                   CPWSXSLE
