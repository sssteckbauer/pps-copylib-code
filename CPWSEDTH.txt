000000**************************************************************/   36370967
000001*  COPYMEMBER: CPWSEDTH                                      */   36370967
000002*  RELEASE: ___0967______ SERVICE REQUEST(S): _____3637____  */   36370967
000003*  NAME:_______PXP_______ MODIFICATION DATE:  __03/10/95____ */   36370967
000004*  DESCRIPTION:                                              */   36370967
000600*    INTERFACE BETWEEN THF     ENTRY/UPDATE SCREEN PROCESSORS*/   36370967
000700*    AND PPVRTHFO PROGRAM.                                   */   36370967
000800**************************************************************/   36370967
000900*01  CPWSEDTH   EXTERNAL.                                         CPWSEDTH
001000     05  CPWSEDTH-EXT-AREA                 PIC X(22528).          CPWSEDTH
001100     05  CPWSEDTH-EXT-DATA REDEFINES CPWSEDTH-EXT-AREA.           CPWSEDTH
001200         10  CPWSEDTH-CNT                  PIC 9(03).             CPWSEDTH
001300         10  CPWSEDTH-DATA-ARRAY OCCURS 200 TIMES.                CPWSEDTH
001400             15  CPWSEDTH-ELEM-NUMBER      PIC 9(04).             CPWSEDTH
001500             15  CPWSEDTH-ELEM-REQUEST-SW  PIC X(01).             CPWSEDTH
001600                 88  CPWSEDTH-ELEM-REQUEST-DISPLAY VALUE '1'.     CPWSEDTH
001700                 88  CPWSEDTH-ELEM-REQUEST-EDIT    VALUE '2'.     CPWSEDTH
001800             15  CPWSEDTH-ERASE-EOF-SW     PIC X(01).             CPWSEDTH
001900                 88 CPWSEDTH-ERASE-EOF-YES     VALUE 'Y'.         CPWSEDTH
002000             15  CPWSEDTH-ENTERED-DATA     PIC X(30).             CPWSEDTH
002100             15  CPWSEDTH-ELEM-STATUS-CODE PIC X(01).             CPWSEDTH
002200                 88  CPWSEDTH-ELEM-NO-ERRORS   VALUE '0'.         CPWSEDTH
002300                 88  CPWSEDTH-ELEM-ERRORS-FOUND VALUE             CPWSEDTH
002400                                                '1', '2', '3'.    CPWSEDTH
002500                 88  CPWSEDTH-ELEM-EDIT-ERROR   VALUE '1'.        CPWSEDTH
002600                 88  CPWSEDTH-DECL-BAL-ERROR    VALUE '2'.        CPWSEDTH
002700                 88  CPWSEDTH-GTN-NO-ERROR      VALUE '3'.        CPWSEDTH
002800             15  CPWSEDTH-TRN-DATA         PIC X(30).             CPWSEDTH
002900             15  FILLER REDEFINES CPWSEDTH-TRN-DATA.              CPWSEDTH
003000                 20  CPWSEDTH-TRN-DATA-NUM PIC 9(09)V9(06).       CPWSEDTH
003100                 20  FILLER                PIC X(15).             CPWSEDTH
003200             15  CPWSEDTH-SCREEN-DATA.                            CPWSEDTH
003300                 20  CPWSEDTH-SCREEN-DATA-BYTE                    CPWSEDTH
003400                                           PIC X(01) OCCURS 30.   CPWSEDTH
003500             15  CPWSEDTH-RELATED-DATA     PIC X(01).             CPWSEDTH
003600             15  CPWSEDTH-OCCURRENCE-INFO  PIC X(01).             CPWSEDTH
004600             15  FILLER                    PIC X(03).             CPWSEDTH
004700             15  CPWSEDTH-ELEM-SCREEN-OCCURNCE PIC 9(02).         CPWSEDTH
004810             15  CPWSEDTH-ELEM-FILLER      PIC X(05).             CPWSEDTH
004900         10  CPWSEDTH-FILLER               PIC X(725).            CPWSEDTH
005000***************    END OF SOURCE - CPWSEDTH    *******************CPWSEDTH
