000100*                                                                 CLPDP743
000200*      CONTROL REPORT 'PPP743CR'                                  CLPDP743
000300*                                                                 CLPDP743
000400*                                                                 CLPDP743
000500 PRINT-CONTROL-REPORT.                                            CLPDP743
000600                                                                  CLPDP743
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP743
000800                                                                  CLPDP743
000900     MOVE 'PPP743CR'             TO CR-HL1-RPT.                   CLPDP743
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP743
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP743
001200                                                                  CLPDP743
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP743
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP743
001500                                                                  CLPDP743
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP743
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP743
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP743
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP743
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP743
002100                                                                  CLPDP743
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP743
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP743
002400                                                                  CLPDP743
002500     MOVE WS-SPEC-RECORD         TO CR-DL5-CTL-CARD.              CLPDP743
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP743
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP743
002800                                                                  CLPDP743
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP743
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP743
003100                                                                  CLPDP743
003110     ACCEPT TIME-WORK-AREA FROM TIME.                             CLPDP743
003200*    MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP743
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP743
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP743
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP743
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP743
003700                                                                  CLPDP743
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP743
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP743
004500*                                                                 CLPDP743
004600     MOVE 'EDB RECORDS'          TO CR-DL9-FILE.                  CLPDP743
004700     MOVE SO-READ                TO CR-DL9-ACTION.                CLPDP743
004800     MOVE EDB-RECORDS-IN         TO CR-DL9-VALUE.                 CLPDP743
004900     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP743
005000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP743
004500*                                                                 CLPDP743
005300     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP743
005400     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP743
005500                                                                  CLPDP743
005600     CLOSE CONTROLREPORT.                                         CLPDP743
