000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPPFAU08                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: G. CHIU         MODIFICATION DATE: 08/27/97   =%      UCSD0102
000400*=    NAME: G. CHIU         MODIFICATION DATE: 12/11/98   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000800*=    GTN TABLE ONLY CARRIES ACCOUNT INSTEAD OF FULL FAU, =%      UCSD0102
000900*=    ONLY ACCOUNT SHOULD BE REARCHED.                    =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPFAU08                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______JLT_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*   Initial release of Procedure Division copymember which   */   32021138
000700*   is used by program PPP520.                               */   32021138
000800**************************************************************/   32021138
000900*                                                             *   CPPFAU08
001000*  This copymember is one of the Full Accounting Unit modules *   CPPFAU08
001100*  which campuses may need to modify to accomodate their own  *   CPPFAU08
001200*  Chart of Accounts structure.                               *   CPPFAU08
001300*                                                             *   CPPFAU08
001400***************************************************************   CPPFAU08
001500*                                                                 CPPFAU08
001600****************************************************************  CPPFAU08
001700*  SET UP SPECIAL CONTROL REFERENCES (I.E., EMP ID OR NAME)       CPPFAU08
001800*  FOR GENERAL LEDGER (E.G., HAND-DRAWN CHECKS)                   CPPFAU08
001900****************************************************************  CPPFAU08
002000                                                                  CPPFAU08
002100****************************************************************  CPPFAU08
002200*  NOTE THAT PRIOR TO GENERIC FAU, THESE C,O,H INDICATORS WERE    CPPFAU08
002300*  MOVED TO THE OLD SUB-ACCOUNT FILLER POSITION (I.E.,            CPPFAU08
002400*  "SRT-SUB-CDE2"). THIS POSITION (SYN., "SUB-FILLER" ON XEDR)    CPPFAU08
002500*  WOULD THEN BE BLANKED OUT ON THE PPP530 TYPE 4 READ. NOW,      CPPFAU08
002600*  THE REPLACEMENT (I.E., SRT-KEY-AD-TYP4-TEMP-REF) FIELD IS      CPPFAU08
002700*  IGNORED ONCE THE SORT HAS BEEN COMPLETED.                      CPPFAU08
002800****************************************************************  CPPFAU08
002900*****IF  CAMPUS-SF                                                CPPFAU08
002900     IF  CAMPUS-SD                                                UCSD0102
003000         IF XPAR-OVERPAYMENT                                      CPPFAU08
003100             MOVE 'O'           TO  SRT-KEY-AD-TYP4-TEMP-REF      CPPFAU08
003200             MOVE XPAR-NAME-26  TO  SRT-4-NAME                    CPPFAU08
003200             MOVE XPAR-ID-NO          TO TEMP-MPP-ID              UCSD0102
003200             MOVE TEMP-MPP-ID-6DIGITS TO SRT-4-REF-NO             UCSD0102
003300         ELSE                                                     CPPFAU08
003400             IF XPAR-HAND-DRAWN-CHECKS                            CPPFAU08
003500                 MOVE 'H'       TO  SRT-KEY-AD-TYP4-TEMP-REF      CPPFAU08
003600                 MOVE XPAR-NAME-26      TO  SRT-4-NAME            CPPFAU08
003200                 MOVE XPAR-ID-NO          TO TEMP-MPP-ID          UCSD0102
003200                 MOVE TEMP-MPP-ID-6DIGITS TO SRT-4-REF-NO         UCSD0102
003700             ELSE                                                 CPPFAU08
003800                 IF XPAR-R-HAND-DRAWN-CHECKS                      CPPFAU08
003900                     MOVE 'R'   TO  SRT-KEY-AD-TYP4-TEMP-REF      CPPFAU08
004000                     MOVE XPAR-NAME-26  TO  SRT-4-NAME            CPPFAU08
003200                     MOVE XPAR-ID-NO          TO TEMP-MPP-ID      UCSD0102
003200                     MOVE TEMP-MPP-ID-6DIGITS TO SRT-4-REF-NO     UCSD0102
004100                 ELSE                                             CPPFAU08
004200                     PERFORM                                      CPPFAU08
004300                      VARYING  AE-I  FROM 1  BY 1                 CPPFAU08
004400                        UNTIL  AE-I  > AET-COUNT                  CPPFAU08
004500                         OR SRT-KEY-FAU (12:6)                    UCSD0102
004600                                = AET-ACCT-NR (AE-I)              UCSD0102
004500*****                    OR SRT-KEY-FAU                           UCSD0102
004600*****                           = AET-SPECIAL-REF-FAU (AE-I)      UCSD0102
004700                     END-PERFORM                                  CPPFAU08
004800                     IF AE-I  NOT > AET-COUNT                     CPPFAU08
004900                         MOVE XPAR-NAME-26   TO  SRT-4-NAME       CPPFAU08
003200                         MOVE XPAR-ID-NO          TO TEMP-MPP-ID  UCSD0102
003200                         MOVE TEMP-MPP-ID-6DIGITS TO SRT-4-REF-NO UCSD0102
005000                     END-IF                                       CPPFAU08
005100                     IF XPAR-CANCELLATION                         CPPFAU08
005200                        MOVE 'C' TO SRT-KEY-AD-TYP4-TEMP-REF      CPPFAU08
005300                     END-IF                                       CPPFAU08
005400                 END-IF                                           CPPFAU08
005500             END-IF                                               CPPFAU08
005600         END-IF                                                   CPPFAU08
005700     END-IF.                                                      CPPFAU08
005800*                                                                 CPPFAU08
005900************** END OF COPY CPPFAU08 ***************************   CPPFAU08
