000100*    ** FD FOR 'OLD' FORMAT OF FICA (941A) TAPE * NOT USED        CPFDXTOB
000200*    COPYID=CPFDXTOB                                              CPFDXTOB
000300     LABEL RECORDS STANDARD                                       CPFDXTOB
000400     RECORDING MODE IS F                                          CPFDXTOB
000500     BLOCK CONTAINS 10 RECORDS                                    CPFDXTOB
000600     DATA RECORD IS TAPEOUT-RCDB.                                 CPFDXTOB
000700     SKIP1                                                        CPFDXTOB
000800 01  TAPEOUT-RCDB                   PIC   X(160).                 CPFDXTOB
