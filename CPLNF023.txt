000000**************************************************************/   32021138
000001*  COPYMEMBER: CPLNF023                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:____BILL GAYLE___ CREATION DATE:      ___05/08/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*                                                            */   32021138
000006*  Initial release of linkage copymember between calling     */   32021138
000007*  program and module PPFAU023                               */   32021138
000008*                                                            */   32021138
000009**************************************************************/   32021138
000900*                                                            */   CPLNF023
001000*  This copymember is one of the Full Accounting Unit modules*/   CPLNF023
001100*  which each campus may need to modify to accommodate its   */   CPLNF023
001200*  Chart of Accounts structure.                              */   CPLNF023
001300*                                                            */   CPLNF023
002100**************************************************************/   CPLNF023
002300*01  PPFAU023-INTERFACE.                                          CPLNF023
002400    05  F023-INPUTS.                                              CPLNF023
002500        10  F023-FAU-IN                  PIC X(30).               CPLNF023
002620    05  F023-OUTPUTS.                                             CPLNF023
002630        10  F023-FAU-OUT                 PIC X(30).               CPLNF023
002730        10  F023-FAILURE-CODE            PIC S9(04) COMP SYNC.    CPLNF023
002740        10  F023-FAILURE-TEXT            PIC X(35).               CPLNF023
