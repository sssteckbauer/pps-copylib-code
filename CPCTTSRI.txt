000000**************************************************************/   73151304
000001*  COPYMEMBER: CPCTTSRI                                      */   73151304
000002*  RELEASE: ___1304______ SERVICE REQUEST(S): ____17315____  */   73151304
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___06/05/00__  */   73151304
000004*  DESCRIPTION:                                              */   73151304
000005*  - NEW COPY MEMBER FOR DATA INPUT TO THE NEW SHIFT RATE    */   73151304
000006*    TABLE, PPPTSR.                                          */   73151304
000007**************************************************************/   73151304
006400*    COPYID=CPCTTSRI                                              CPCTTSRI
011500*01  TITLE-CODE-TSR-TABLE-INPUT.                                  CPCTTSRI
019631     05  TSRI-TITLE-CODE                 PIC X(04).               CPCTTSRI
019632     05  TSRI-SUB-LOCATION               PIC X(02).               CPCTTSRI
019633     05  TSRI-PAY-REP-CODE               PIC X(03).               CPCTTSRI
019634     05  TSRI-EFFECTIVE-DATE             PIC X(08).               CPCTTSRI
019635     05  TSRI-END-DATE                   PIC X(08).               CPCTTSRI
019636     05  TSRI-RANGE-ADJ-ID               PIC X(12).               CPCTTSRI
019637     05  TSRI-SHIFT-TYPE                 PIC X(02).               CPCTTSRI
019638     05  TSRI-RATE-TYPE                  PIC X(01).               CPCTTSRI
019639     05  TSRI-SHIFT-RATE                 PIC X(09).               CPCTTSRI
019640     05  TSRI-UPDT-SOURCE                PIC X(08).               CPCTTSRI
