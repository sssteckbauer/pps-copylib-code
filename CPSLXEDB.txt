000100*    COPYID=CPSLXEDB                                              CPSLXEDB
000200******************************************************************13340158
000300*  COPYMEMBER: CPSLXEDB                                          *13340158
000400*  RELEASE # ___0158___   SERVICE REQUEST NO(S) 1334             *13340158
000500*  NAME ___JAG_________   MODIFICATION DATE ____07/15/85_____    *13340158
000600*  DESCRIPTION                                                   *13340158
000700*    THIS COPYMEMBER WAS MODIFIED FOR VSAM. THE OLD ISAM         *13340158
000800*    CONFIGURATION WAS LEFT IN PLACE, IN COMMENTS.               *13340158
000900******************************************************************13340158
001000************************** FOR ISAM ******************************13340158
001100*        ASSIGN TO DA-I-EDB                                       13340158
001200*        ACCESS MODE IS SEQUENTIAL                                13340158
001300*        NOMINAL KEY IS PBP-MASTER-KEY                            13340158
001400*        RECORD KEY IS PBPEDB-KEY-AREA.                           13340158
001500************************** FOR VSAM ******************************13340158
001600         ASSIGN TO VSAM-EDB                                       13340158
001700         ORGANIZATION IS INDEXED                                  13340158
001800         ACCESS MODE IS SEQUENTIAL                                13340158
001900         RECORD KEY IS PBPEDB-KEY-AREA                            13340158
002000         FILE STATUS IS VSAM-STATUS.                              13340158
