000000**************************************************************/   30871424
000001*  COPYMEMBER: CPCTJGTI                                      */   30871424
000002*  RELEASE: ___1424______ SERVICE REQUEST(S): _____3087____  */   30871424
000003*  NAME:_____SRS_________ CREATION DATE:      ___07/30/02__  */   30871424
000004*  DESCRIPTION:                                              */   30871424
000005*  NEW COPY MEMBER FOR JOB GROUP ID TITLE CODE TABLE INPUT   */   30871424
000007**************************************************************/   30871424
000008*    COPYID=CPCTJGTI                                              CPCTJGTI
010000*01  JOB-GROUP-TITL-JGT-TABLE-INPUT.                              CPCTJGTI
010100     05 JGTI-TITLE-CODE                  PIC XXXX.                CPCTJGTI
010110     05 JGTI-HOME-DEPT                   PIC X(6).                CPCTJGTI
010120     05 JGTI-JOB-GROUP-ID                PIC XXX.                 CPCTJGTI
010120     05 JGTI-EE06-CATEGORY               PIC X.                   CPCTJGTI
010130        88 JGTI-VALID-EE06-CATEGORY      VALUES '1' '2' '3' '6'   CPCTJGTI
                                                      '7' '8' '9'.      CPCTJGTI
