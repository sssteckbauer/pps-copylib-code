000100**************************************************************/   36210607
000200*  COPYMEMBER: CPWSXJGD                                      */   36210607
000300*  RELEASE: ____0607____  SERVICE REQUEST(S): ____3621____   */   36210607
000400*  NAME:    __J.LUCAS___  CREATION DATE:      __10/14/91__   */   36210607
000500*  DESCRIPTION:                                              */   36210607
000600*                                                            */   36210607
000700*    - NEW COPYMEMBER PROVIDING RECORD LAYOUT FOR JOB GROUP  */   36210607
000800*      IDENTIFIER DESCRIPTION TABLE                          */   36210607
000900**************************************************************/   36210607
001000*    COPYID=CPWSXJGD                                              CPWSXJGD
001100*01  XJGD-JGID-DESC-RECORD.                                       CPWSXJGD
001200*-----------------------------------------------------------------CPWSXJGD
001300* J O B   G R O U P   I D   D E S C R I P T I O N   T A B L E     CPWSXJGD
001400*-----------------------------------------------------------------CPWSXJGD
001500     05 XJGD-DELETE                  PIC X(01).                   CPWSXJGD
001600     05 XJGD-KEY.                                                 CPWSXJGD
001700         10  XJGD-KEY-CONSTANT       PIC X(04).                   CPWSXJGD
001800         10  XJGD-KEY-JGID           PIC X(03).                   CPWSXJGD
001900         10  FILLER                  PIC X(06).                   CPWSXJGD
002000     05  XJGD-FULL-DESC              PIC X(50).                   CPWSXJGD
002100     05  XJGD-ABBRV-DESC             PIC X(20).                   CPWSXJGD
002200     05  XJGD-LAST-ACTION            PIC X(01).                   CPWSXJGD
002300     05  XJGD-LAST-ACTION-DATE       PIC X(06).                   CPWSXJGD
002400     05  FILLER                      PIC X(133).                  CPWSXJGD
