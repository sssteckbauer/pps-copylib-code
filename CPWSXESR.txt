000000**************************************************************/   48881487
000001*  COPYMEMBER: CPWSXESR                                      */   48881487
000002*  RELEASE: ___1487______ SERVICE REQUEST(S): ____14888____  */   48881487
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___04/28/03__  */   48881487
000004*  DESCRIPTION:                                              */   48881487
000005*    ADDED FORMATTING FOR DG TRANSACTION                     */   48881487
000007**************************************************************/   48881487
000000**************************************************************/   02381440
000001*  COPYMEMBER: CPWSXESR                                      */   02381440
000002*  RELEASE: ___1440______ SERVICE REQUEST(S): _80238, 80251  */   02381440
000003*  NAME:____J KENNEDY____ MODIFICATION DATE:  ___11/08/02__  */   02381440
000004*  DESCRIPTION:                                              */   02381440
000005*                                                            */   02381440
000006*    ADDED LABELS FOR HCRA ELEMENTS 338                      */   02381440
000006*    ADDED USER-ANNUAL AMOUNT FOR DEP CARE                   */   02511440
000007*                                                            */   02381440
000008**************************************************************/   02381440
000000**************************************************************/   48441325
000001*  COPYMEMBER: CPWSXESR                                      */   48441325
000002*  RELEASE: ___1325______ SERVICE REQUEST(S): ____14844____  */   48441325
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___10/18/00__  */   48441325
000004*  DESCRIPTION:                                              */   48441325
000005*    ADDED LABELS FOR EDB 0127, 0128, 6010, 6011,            */   48441325
000006*    0130, 0131, AND 0132                                    */   48441325
000007**************************************************************/   48441325
000000**************************************************************/   54191261
000001*  COPYMEMBER: CPWSXESR                                      */   54191261
000002*  RELEASE: ___1261______ SERVICE REQUEST(S): ____15419____  */   54191261
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___11/21/99__  */   54191261
000004*  DESCRIPTION:                                              */   54191261
000005*                                                            */   54191261
000006*  WORKING STORAGE DEFINITIONS ADDED FOR FORMATTING OF       */   54191261
000007*  REJECTED NAME, ADDRESS AND RELEASE FLAG TRANSACTIONS      */   54191261
000008*                                                            */   54191261
000009**************************************************************/   54191261
000000**************************************************************/   54181253
000001*  COPYMEMBER: CPWSXESR                                      */   54181253
000002*  RELEASE: ___1253______ SERVICE REQUEST(S): _15418, 15419  */   54181253
000003*  NAME:_______WJG_______ CREATION DATE:      ___07/01/99__  */   54181253
000004*  DESCRIPTION:                                              */   54181253
000005*                                                            */   54181253
000006*                                                            */   54181253
000007**************************************************************/   54181253
006100*    COPYID=CPWSXESR                                              54181253
006110******************************************************************54181253
006210*01  XESR-REJECTED-ACTIVITY-TEXT.                                 CPWSXESR
006400     02  XESR-MAJOR-PLAN-TEXT.                                    CPWSXESR
006500     05  XESR-MAJ-LABEL    PIC X(14) VALUE SPACE.                 CPWSXESR
006600         88  XESR-MED-PLAN-REJ   VALUE  'MEDICAL PLAN: '.         CPWSXESR
006700         88  XESR-DEN-PLAN-REJ   VALUE  'DENTAL PLAN:  '.         CPWSXESR
006800         88  XESR-VIS-PLAN-REJ   VALUE  'VISION PLAN:  '.         CPWSXESR
006900         88  XESR-LEG-PLAN-REJ   VALUE  'LEGAL PLAN:   '.         CPWSXESR
007000     05  XESR-MAJ-CD       PIC X(02).                             CPWSXESR
007100     05  FILLER            PIC X(02) VALUE SPACE.                 CPWSXESR
007200     05  XESR-MAJ-CED-LBL  PIC X(04) VALUE SPACE.                 CPWSXESR
007300         88  XESR-MED-CED-REJ              VALUE   'MED '.        CPWSXESR
007400         88  XESR-DEN-CED-REJ              VALUE   'DEN '.        CPWSXESR
007500         88  XESR-VIS-CED-REJ              VALUE   'VIS '.        CPWSXESR
007600         88  XESR-LEG-CED-REJ              VALUE   'LEG '.        CPWSXESR
007700     05  FILLER                 PIC X(05) VALUE SPACE.            CPWSXESR
007800         88  XESR-CED-LABEL-ON            VALUE    'CED: '.       CPWSXESR
007900     05  XESR-MAJ-COV-EFFDT     PIC X(06).                        CPWSXESR
008000     05  FILLER                 PIC X(02) VALUE SPACE.            CPWSXESR
008100     05  XESR-MED-PCP-LBL       PIC X(05) VALUE SPACE.            CPWSXESR
008200         88  XESR-MED-PCP-REJ   VALUE     'PCP: '.                CPWSXESR
008300     05  XESR-MED-PROV-ID       PIC X(10).                        CPWSXESR
008400****                                                           CD 48441325
008410     05  FILLER                 PIC X(82) VALUE SPACE.            54191261
008500*---          EMPLOYEE PAID DISABILITY                            CPWSXESR
008600  02 XESR-EMP-PD-DIS-TEXT.                                        CPWSXESR
008700     05  XESR-EPD-LABEL          PIC X(26)                        CPWSXESR
008800         VALUE 'EMPLOYEE PAID DISABILITY: '.                      CPWSXESR
008900     05  XESR-EPD-WAIT-PERIOD    PIC ZZ9.                         CPWSXESR
009000     05  FILLER                  PIC X(05) VALUE SPACE.           CPWSXESR
009010     05  XESR-EPD-CED-LBL        PIC X(05) VALUE 'CED: '.         CPWSXESR
009100     05  XESR-EPD-COV-EFFDT      PIC X(06).                       CPWSXESR
009200     05  FILLER                  PIC X(77) VALUE SPACE.           CPWSXESR
009300                                                                  CPWSXESR
009400*---          EMPLOYEE PAID LIFE INSURANCE                        CPWSXESR
009500  02 XESR-EMP-PD-LIFEINS-TEXT.                                    CPWSXESR
009600     05  XESR-EP-LIFE-LABEL          PIC X(26) VALUE              CPWSXESR
009700         'EMPLOYEE PAID LIFE:       ' .                           CPWSXESR
009800     05  XESR-EP-LIFE-CD             PIC X(01).                   CPWSXESR
009900     05  FILLER                      PIC X(08) VALUE SPACE.       CPWSXESR
010000     05  XESR-EP-LIFE-CED-LBL        PIC X(05) VALUE 'CED: '.     CPWSXESR
010100     05  XESR-EP-LIFE-COV-EFFDT      PIC X(06).                   CPWSXESR
010200     05  FILLER                      PIC X(86) VALUE SPACE.       CPWSXESR
010300                                                                  CPWSXESR
010400*---          TIP ENROLLMENT                                      CPWSXESR
010500  02 XESR-TIP-TEXT.                                               CPWSXESR
010600     05  XESR-TIP-LABEL              PIC X(14) VALUE              CPWSXESR
010700         'TIP:         '.                                         CPWSXESR
010800     05  XESR-TIP-CD                 PIC X(01).                   CPWSXESR
010900     05  FILLER                      PIC X(117) VALUE SPACE.      CPWSXESR
011000                                                                  CPWSXESR
011100*---          DEPENDENT LIFE INSURANCE                            CPWSXESR
011200  02 XESR-DEP-LIFEINS-TEXT.                                       CPWSXESR
011300     05  XESR-DEP-LIFE-LABEL         PIC X(26) VALUE              CPWSXESR
011400         'DEPENDENT LIFE:           '.                            CPWSXESR
011500     05  XESR-DEP-LIFE-CD            PIC X(01).                   CPWSXESR
011600     05  FILLER                      PIC X(08) VALUE SPACE.       CPWSXESR
011700     05  XESR-DEP-LIFE-CED-LBL       PIC X(05) VALUE 'CED: '.     CPWSXESR
011800     05  XESR-DEP-LIFE-COV-EFFDT     PIC X(06).                   CPWSXESR
011900     05  FILLER                      PIC X(86) VALUE SPACE.       CPWSXESR
012000*---          HEALTH PLAN OPT OUT                                 CPWSXESR
012100  02 XESR-MAJ-OPTOUT-TEXT.                                        CPWSXESR
012200     05  XESR-MAJ-OPT-LABEL   PIC X(14) VALUE SPACE.              CPWSXESR
012300         88  XESR-MED-OPT-REJ  VALUE 'MEDICAL PLAN: '.            CPWSXESR
012400         88  XESR-DEN-OPT-REJ  VALUE 'DENTAL PLAN:  '.            CPWSXESR
012500         88  XESR-VIS-OPT-REJ  VALUE 'VISION PLAN:  '.            CPWSXESR
012600     05  XESR-MAJ-OPT-IND     PIC X(07) VALUE 'OPT OUT'.          CPWSXESR
012700     05  FILLER               PIC X(111) VALUE SPACE.             CPWSXESR
012800*---          AD & D PLAN                                         CPWSXESR
012900  02 XESR-AD-D-TEXT.                                              CPWSXESR
013000     05  XESR-AD-D-LABEL     PIC X(10) VALUE 'AD&D PLAN '.        CPWSXESR
013100     05  XESR-AD-D-AMT-LBL   PIC X(08) VALUE 'AMOUNT: '.          CPWSXESR
013200     05  XESR-AD-D-AMT       PIC ZZZ9.                            CPWSXESR
013300     05  FILLER              PIC X(02).                           CPWSXESR
013400     05  XESR-AD-D-TYPE-LBL  PIC X(11) VALUE 'AD&D TYPE: '.       CPWSXESR
013500     05  XESR-AD-D-TYPE-CD   PIC X(01).                           CPWSXESR
013600     05  FILLER              PIC X(03).                           CPWSXESR
013700     05  XESR-AD-D-CED-LBL   PIC X(05) VALUE 'CED: '.             CPWSXESR
013800     05  XESR-AD-D-COV-EFFDT PIC X(06).                           CPWSXESR
013900     05  FILLER              PIC X(82) VALUE SPACE.               CPWSXESR
013920                                                                  CPWSXESR
014300*---          DEPCARE ENROLLMENTS                                 CPWSXESR
014400  02 XESR-DEPCARE-TEXT.                                           CPWSXESR
014500     05  XESR-DC-LABEL       PIC X(16) VALUE 'DEPCARE AMOUNT: '.  CPWSXESR
014600*****                                                          CD 48441325
014610     05  FILLER              PIC X(01) VALUE SPACE.               CPWSXESR
014700     05  XESR-DC-AMOUNT      PIC ZZZZZ9.99.                       CPWSXESR
014800     05  FILLER              PIC X(05) VALUE SPACE.               CPWSXESR
014900     05  XESR-DC-CED-LBL     PIC X(05) VALUE 'CED: '.             CPWSXESR
015000     05  XESR-DC-COV-EFFDT   PIC X(06).                           CPWSXESR
015100     05  FILLER              PIC X(05) VALUE SPACE.               02511440
015230     05  XESR-DC-LABEL-U     PIC X(16) VALUE 'DC AMOUNT-U: '.     02511440
015250     05  FILLER              PIC X(01) VALUE SPACE.               02511440
015260     05  XESR-DC-AMOUNT-U    PIC ZZZZZ9.99.                       02511440
015291     05  FILLER              PIC X(59) VALUE SPACE.               02511440
015291*****05  FILLER              PIC X(90) VALUE SPACE.               02511440
015200                                                                  02381440
015210*---          HCRA ENROLLMENTS                                    02381440
015220  02 XESR-HCRA-TEXT.                                              02381440
015230     05  XESR-HCRA-LABEL-G   PIC X(18) VALUE 'HCRA AMOUNT-G: '.   02381440
015250     05  FILLER              PIC X(01) VALUE SPACE.               02381440
015260     05  XESR-HCRA-AMOUNT-G  PIC ZZZZZ9.99.                       02381440
015270     05  FILLER              PIC X(05) VALUE SPACE.               02381440
015280     05  XESR-HCRA-CED-LBL   PIC X(05) VALUE 'CED: '.             02381440
015290     05  XESR-HCRA-COV-EFFDT PIC X(06).                           02381440
015270     05  FILLER              PIC X(05) VALUE SPACE.               02381440
015230     05  XESR-HCRA-LABEL-U   PIC X(18) VALUE 'HCRA AMOUNT-U: '.   02381440
015250     05  FILLER              PIC X(01) VALUE SPACE.               02381440
015260     05  XESR-HCRA-AMOUNT-U  PIC ZZZZZ9.99.                       02381440
015291     05  FILLER              PIC X(55) VALUE SPACE.               02381440
015292                                                                  02381440
015300*---          DEPENDENT ENROLLMENTS                               CPWSXESR
015400  02 XESR-DEPENDENT-TEXT.                                         CPWSXESR
016100     05 XESR-DEP-TRANS-TYPE    PIC X(14).                         CPWSXESR
016200        88 XESR-DEP-ADD        VALUE 'DEPENDENT ADD:'.            CPWSXESR
016300        88 XESR-DEP-CHG        VALUE 'DEPENDENT CHG:'.            CPWSXESR
016400     05 FILLER                 PIC X(01).                         CPWSXESR
016500     05 XESR-DEP-NUM           PIC X(02).                         CPWSXESR
016600     05 FILLER                 PIC X(01).                         CPWSXESR
016700     05 XESR-DEP-NAME          PIC X(26).                         CPWSXESR
016800     05 FILLER                 PIC X(01).                         CPWSXESR
016900     05 XESR-DEP-REL           PIC X(01).                         CPWSXESR
017000     05 FILLER                 PIC X(01).                         CPWSXESR
017100     05 XESR-DEP-BIRTH-DATE    PIC X(06).                         CPWSXESR
017200     05 FILLER                 PIC X(01).                         CPWSXESR
017300     05 XESR-DEP-SEX-CODE      PIC X(01).                         CPWSXESR
017400     05 FILLER                 PIC X(01).                         CPWSXESR
017500     05 XESR-DEP-SSN           PIC X(09).                         CPWSXESR
017600     05 FILLER                 PIC X(01).                         CPWSXESR
017700     05 XESR-DEP-MED-SW        PIC X(03).                         CPWSXESR
017800        88 XESR-REJ-DEP-MED    VALUE 'MED'.                       CPWSXESR
017900     05 FILLER                 PIC X(01).                         CPWSXESR
018000     05 XESR-DEP-DEN-SW        PIC X(03).                         CPWSXESR
018100        88 XESR-REJ-DEP-DEN    VALUE 'DEN'.                       CPWSXESR
018200     05 FILLER                 PIC X(01).                         CPWSXESR
018300     05 XESR-DEP-VIS-SW        PIC X(03).                         CPWSXESR
018400        88 XESR-REJ-DEP-VIS    VALUE 'VIS'.                       CPWSXESR
018500     05 FILLER                 PIC X(01).                         CPWSXESR
018600     05 XESR-DEP-LEG-SW        PIC X(03).                         CPWSXESR
018700        88 XESR-REJ-DEP-LEG    VALUE 'LEG'.                       CPWSXESR
018800     05 FILLER                 PIC X(25).                         CPWSXESR
019000                                                                  CPWSXESR
019100*---          ANTICIPATED RETIREMENT DATE                         CPWSXESR
019200  02 XESR-ANTCP-RET-DATE-TEXT.                                    CPWSXESR
019300     05  XESR-ANTCP-RET-LABEL PIC X(24)                           CPWSXESR
019310                        VALUE 'ANTICIPATED RETIRE DATE:'.         CPWSXESR
019400     05  FILLER              PIC X(01)  VALUE SPACE.              CPWSXESR
019500     05  XESR-ANTCP-RET-DATE PIC X(06).                           CPWSXESR
019900     05  FILLER              PIC X(101) VALUE SPACES.             CPWSXESR
020000                                                                  CPWSXESR
020100*---          DCP PRETAX PLAN CODE                                CPWSXESR
020200  02 XESR-DCP-PRETAX-TEXT.                                        CPWSXESR
020300     05  XESR-DCP-PLNCD-LABEL PIC X(21)                           CPWSXESR
020400                        VALUE 'DCP PRETAX PLAN CODE:'.            CPWSXESR
020500     05  FILLER              PIC X(05)  VALUE SPACES.             CPWSXESR
020600     05  XESR-DCP-PRETX-PLAN PIC X(01).                           CPWSXESR
020700     05  FILLER              PIC X(105) VALUE SPACES.             CPWSXESR
020800                                                                  CPWSXESR
020900*---          DEDUCTION AMOUNT                                    CPWSXESR
021000  02 XESR-DEDUCTION-TEXT.                                         CPWSXESR
021100     05  XESR-DED-GTN-LABEL  PIC X(04)                            CPWSXESR
021200                        VALUE 'GTN:'.                             CPWSXESR
021300     05  FILLER              PIC X(01)  VALUE SPACE.              CPWSXESR
021400     05  XESR-DED-GTN-LABEL.                                      CPWSXESR
021410         07  XESR-DED-GTN-NUM PIC X(03).                          CPWSXESR
021420         07  FILLER          PIC X(01)  VALUE '-'.                CPWSXESR
021430         07  XESR-DED-GTN-TYP PIC X(01).                          CPWSXESR
021440     05  FILLER              PIC X(02)  VALUE SPACES.             CPWSXESR
021500     05  XESR-GTN-DESCR      PIC X(15).                           CPWSXESR
021600     05  XESR-DED-AMOUNT     PIC ZZZZ9.99.                        CPWSXESR
021700     05  FILLER              PIC X(02)  VALUE SPACES.             CPWSXESR
021800     05  XESR-DED-EFFDT-LBL  PIC X(10)  VALUE 'EFF DATE: '.       CPWSXESR
021900     05  XESR-DED-EFF-DATE   PIC X(06).                           CPWSXESR
022000     05  FILLER              PIC X(85)  VALUE SPACES.             CPWSXESR
022100  02 XESR-REJECT-TEXT.                                            54191261
022200     05  XESR-REJECT-LABLE   PIC X(24).                           54191261
022210         88  XESR-FIRST-NAME                                      54191261
022211             VALUE 'FIRST NAME:             '.                    54191261
022220         88  XESR-MIDDLE-NAME                                     54191261
022230             VALUE 'MIDDLE NAME:            '.                    54191261
022240         88  XESR-LAST-NAME                                       54191261
022250             VALUE 'LAST NAME:              '.                    54191261
022260         88  XESR-NAMESUFFIX                                      54191261
022270             VALUE 'NAME SUFFIX:            '.                    54191261
022280         88  XESR-ADDRESS-LINE1                                   54191261
022290             VALUE 'ADDRESS LINE 1:         '.                    54191261
022300         88  XESR-ADDRESS-LINE2                                   54191261
022400             VALUE 'ADDRESS LINE 2:         '.                    54191261
022500         88  XESR-ADDRESS-CITY                                    54191261
022501             VALUE 'CITY:                   '.                    54191261
022502         88  XESR-ADDRESS-STATE                                   54191261
022503             VALUE 'STATE:                  '.                    54191261
022504         88  XESR-ADDRESS-ZIP                                     54191261
022505             VALUE 'ZIP CODE:               '.                    54191261
022506         88  XESR-FOREIGN-ADDR-IND                                54191261
022507             VALUE 'FOREIGN ADDRESS CODE:   '.                    54191261
022508         88  XESR-FAD-PROVINCE                                    54191261
022509             VALUE 'FOREIGN PROVINCE:       '.                    54191261
022510         88  XESR-FAD-POSTAL-CODE                                 54191261
022511             VALUE 'POSTAL CODE:            '.                    54191261
022512         88  XESR-FAD-COUNTRY                                     54191261
022513             VALUE 'FOREIGN COUNTRY:        '.                    54191261
022514         88  XESR-HOME-PHONE                                      54191261
022515             VALUE 'HOME PHONE:             '.                    54191261
022516         88  XESR-SPOUSE-NAME                                     54191261
022517             VALUE 'SPOUSE NAME:            '.                    54191261
022518         88  XESR-EMP-ORG-ADDR-RLSE                               54191261
022519             VALUE 'EMPL ORG ADDRESS DISC:  '.                    54191261
022520         88  XESR-EMP-ORG-PHONE-RLSE                              54191261
022521             VALUE 'EMPL ORG PHONCE DISC:   '.                    54191261
022522         88  XESR-HOME-ADDR-RLSE                                  54191261
022523             VALUE 'HOME ADDRESS RELEASE:   '.                    54191261
022524         88  XESR-HOME-PHONE-RLSE                                 54191261
022525             VALUE 'HOME PHONE RELEASE:     '.                    54191261
022526         88  XESR-SPOUSE-NAME-RLSE                                54191261
022527             VALUE 'SPOUSE NAME RELEASE:    '.                    54191261
022530     05  XESR-VALUE          PIC X(60).                           54191261
022540  02 XESR-REJECT-TAX-DATA-TEXT.                                   48441325
022541*---          WITHHOLDING ALLOWANCES                              48441325
022550     05  XESR-REJECT-TAX-DATA-LABELS  PIC X(41).                  48441325
022575         88  XESR-W4-STATE-PERS-DEDUCTION                         48441325
022576             VALUE 'STATE REGULAR WITHHOLDING ALLOWANCES:    '.   48441325
022577         88  XESR-W4-STATE-ITEM-DEDUCTION                         48441325
022578             VALUE 'STATE ADDITIONAL WITHHOLDING ALLOWANCES: '.   48441325
022579         88  XESR-W4-FED-EXEMPT                                   48441325
022580             VALUE 'FEDERAL PERSONAL ALLOWANCES:             '.   48441325
022590     05  XESR-VALUE-TAX-DATA PIC X(03).                           48441325
022591     05  XESR-VALUE-FILLER   PIC X(88) VALUE SPACES.              48441325
022592  02 XESR-REJECT-TAX-MARITAL-TEXT.                                48441325
022593*---          TAX MARITAL STATUS                                  48441325
022594     05  XESR-REJECT-TAX-MARITAL-LABELS  PIC X(25).               48441325
022595         88  XESR-W4-FED-MARITAL-STAT                             48441325
022596             VALUE 'FEDERAL MARITAL STATUS:  '.                   48441325
022597         88  XESR-W4-STATE-MARITAL-STAT                           48441325
022598             VALUE 'STATE MARITAL STATUS:    '.                   48441325
022604     05  XESR-VALUE-MARITAL-DATA PIC X(03).                       48441325
022605     05  XESR-VALUE-MARITAL-FILLER  PIC X(104) VALUE SPACES.      48441325
022606  02 XESR-REJECT-CONFIRM-NBR-TEXT.                                48441325
022607*---          WEB APPLICATION CONFIRMATION NUMBER                 48441325
022608     05  XESR-REJECT-CONFIRM-NBR-LABELS  PIC X(21).               48441325
022609         88  XESR-X2-CONFIRM-NBR                                  48441325
022610             VALUE 'CONFIRMATION NUMBER: '.                       48441325
022612     05  XESR-VALUE-CONFIRM-NBR PIC X(25).                        48441325
022613     05  XESR-VALUE-CONFIRM-FILLER  PIC X(86) VALUE SPACES.       48441325
022630  02 XESR-REJECT-ADTL-TAX-DATA-TEXT.                              48441325
022640*---          ADDITIONAL TAX WITHHOLDINGS                         48441325
022700     05  XESR-REJECT-ADTL-TAX-DATA-LABS    PIC X(39).             48441325
023400         88  XESR-X1-ADTL-FEDTAX-DEDUCTION                        48441325
023500             VALUE 'FEDERAL ADDITIONAL TAX WITHHOLDING:    '.     48441325
023600         88  XESR-X1-ADTL-STETAX-DEDUCTION                        48441325
023700             VALUE 'STATE ADDITIONAL TAX WITHHOLDING:      '.     48441325
023800     05  XESR-VALUE-ADTL-TAX-AMT  PIC ZZZZ9.99.                   48441325
023900     05  XESR-VALUE-ADTL-FILLER   PIC X(85) VALUE SPACES.         48441325
024000  02 XESR-REJECT-DG-DATA-TEXT.                                    48881487
024100*---          DIRECT DEPOSIT GTN DATA (PPPDDG TABLE)              48881487
024200     05  XESR-REJECT-DG-GTN-LAB            PIC X(09)              48881487
024201                                    VALUE 'DDG GTN: '.            48881487
024210     05  XESR-REJECT-DG-GTN                PIC X(03).             48881487
024220     05  FILLER                            PIC X(03) VALUE SPACES.48881487
024221     05  XESR-REJECT-DG-TRANS-LAB          PIC X(09)              48881487
024222                                    VALUE 'TRANSIT: '.            48881487
024223     05  XESR-REJECT-DG-TRANSIT            PIC X(09).             48881487
024224     05  FILLER                            PIC X(03) VALUE SPACES.48881487
024225     05  XESR-REJECT-DG-ACCT-NUM-LAB       PIC X(10)              48881487
024226                                    VALUE 'ACCT NUM: '.           48881487
024227     05  XESR-REJECT-DG-ACCT-NUM           PIC X(17).             48881487
024228     05  FILLER                            PIC X(03) VALUE SPACES.48881487
024229     05  XESR-REJECT-DG-ACCT-TYP-LAB       PIC X(11)              48881487
024230                                    VALUE 'ACCT TYPE: '.          48881487
024240     05  XESR-REJECT-DG-ACCT-TYPE          PIC X(01).             48881487
024250     05  FILLER                            PIC X(54) VALUE SPACES.48881487
