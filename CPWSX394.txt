000000**************************************************************/   52191347
000001*  COPYMEMBER: CPWSX394                                      */   52191347
000002*  RELEASE: ___1347______ SERVICE REQUEST(S): ____15219____  */   52191347
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___03/19/01__  */   52191347
000004*  DESCRIPTION:                                              */   52191347
000005*  - INCREASED THE OCCURS OF THE DOLLAR ARRAY TO 57.         */   52191347
000008**************************************************************/   52191347
000000**************************************************************/   52121330
000001*  COPYMEMBER: CPWSX394                                      */   52121330
000002*  RELEASE: ___1330______ SERVICE REQUEST(S): ____15212____  */   52121330
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___02/01/01__  */   52121330
000004*  DESCRIPTION:                                              */   52121330
000005*  - INCREASED THE OCCURS OF THE HOUR ARRAY TO 68 FOR        */   52121330
000006*    ADDED HOURS TOWARD CAREER ELIGIBILITY. INCREASED SIZE   */   52121330
000007*    OF COM3940-PRE-PPP390-DATA AND COM3940-PRE-PPP390-DATA. */   52121330
000008**************************************************************/   52121330
000000**************************************************************/   52101313
000001*  COPYMEMBER: CPWSX394                                      */   52101313
000002*  RELEASE: ___1313______ SERVICE REQUEST(S): ____15210____  */   52101313
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___11/15/00__  */   52101313
000004*  DESCRIPTION:                                              */   52101313
000006*  - INCREASED THE OCCURS OF THE HOUR ARRAY TO 55 FOR        */   52101313
000005*    ADDED HOURS TOWARD BENEFITS ELIGIBILITY. INCREASED SIZE */   52101313
000016*    OF COM3940-PRE-PPP390-DATA AND COM3940-PRE-PPP390-DATA. */   52101313
000000**************************************************************/   17850950
000001*  COPYMEMBER: CPWSX394                                      */   17850950
000002*  RELEASE: ___0950______ SERVICE REQUEST(S): ____11785____  */   17850950
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___11/16/94__  */   17850950
000004*  DESCRIPTION:                                              */   17850950
000005*  - INCREASED SIZE OF PRE- AND POST- PPP390-DATA AND THE    */   17850950
000006*    OCCURS CLAUSE OF THE DOLLAR BALANCE ARRAY TO 50 FOR     */   17850950
000007*    NEW DOLLAR BALANCE YTD-EXCL-MOVE-EXP PLUS 4 FILLER      */   17850950
000008*    GROSSES.                                                */   17850950
000009**************************************************************/   17850950
000100**************************************************************/   07780654
000200*  COPYMEMBER: CPWSX394                                      */   07780654
000300*  RELEASE: ___0654______ SERVICE REQUEST(S): ____10778____  */   07780654
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___04/09/92__  */   07780654
000500*  DESCRIPTION:                                              */   07780654
000600*  - INCREASED SIZE OF PRE- AND POST- PPP390-DATA AND THE    */   07780654
000700*    OCCURS CLAUSE OF THE DOLLAR BALANCE ARRAY.              */   07780654
000800**************************************************************/   07780654
000100**************************************************************/   37640580
000200*  COPYMEMBER: CPWSX394                                      */   37640580
000300*  RELEASE: ___0580______ SERVICE REQUEST(S): _____3764____  */   37640580
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___05/21/91__  */   37640580
000500*  DESCRIPTION:                                              */   37640580
000600*  - REPLACEMENT COPYLIB MEMBER TO SIMPLIFY ADDITION OF      */   37640580
000700*    DOLLAR OR HOURS ELEMENTS.                               */   37640580
000710*  - ADDED 5 BYTES FOR NEW FYTD-RET-GROSS DOLLAR FIELD.      */   37640580
000800**************************************************************/   37640580
000100**************************************************************/   01900580
000200*  COPYMEMBER: CPWSX394                                      */   01900580
000300*  RELEASE: ___0580______ SERVICE REQUEST(S): ____10190____  */   01900580
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___05/21/91__  */   01900580
000500*  DESCRIPTION:                                              */   01900580
000710*  - ADDED 5 BYTES FOR NEW YTD-SFHBR-GROSS DOLLAR FIELD.     */   01900580
000800**************************************************************/   01900580
001100*                                                                 CPWSX394
003400******************************************************************CPWSX394
001100*    COPYMEMBER DEFINING LINKAGE RECORD PASSED BETWEEN PPP390    *CPWSX394
001200*    AND PPP400.                                                 *CPWSX394
001300*                                                                *CPWSX394
001400*    NOTES:                                                      *CPWSX394
001500*    (1) LENGTH OF ...-PPP390-DATA MUST BE EQUAL TO THE          *CPWSX394
001600*        LENGTH OF THE STRUCTURE IN COPYMEMBER CPLNKHDA          *CPWSX394
001700*    (2) IN ORDER TO ADD NEW DOLLAR OR HOURS ELEMENTS:           *CPWSX394
001800*        - INCREASE THE SIZE OF BOTH COM3940-PRE-PPP390-DATA AND *CPWSX394
001900*          COM3940-POST-PPP390-DATA BY 5 BYTES FOR EACH NEW      *CPWSX394
002000*          HOUR OR DOLLAR BEING ADDED                            *CPWSX394
002100*        - INCREASE THE OCCURS CLAUSE ON HOUR-BALANCE-ARRAY BY   *CPWSX394
002200*          ONE FOR EACH NEW HOUR ELEMENT BEING ADDED             *CPWSX394
002300*        - INCREASE THE OCCURS CLAUSE ON DOLLAR-BALANCE-ARRAY BY *CPWSX394
002400*          ONE FOR EACH NEW DOLLAR ELEMENT BEING ADDED           *CPWSX394
002500******************************************************************CPWSX394
002600     SKIP1                                                        CPWSX394
002700*    COPYID=CPWSX394                                              CPWSX394
002800*01  COM3940-RECORD.                                              CPWSX394
004500*****                                                          CD 52191347
004823*****03  COM3940-PRE-PPP390-DATA      PIC X(600).                 52191347
004824*****03  COM3940-POST-PPP390-DATA     PIC X(600).                 52191347
004825     03  COM3940-PRE-PPP390-DATA      PIC X(635).                 52191347
004826     03  COM3940-POST-PPP390-DATA     PIC X(635).                 52191347
003100     03  FILLER                       REDEFINES                   CPWSX394
003200         COM3940-POST-PPP390-DATA.                                CPWSX394
003300         05  PPHDAUTL-ERROR-SW        PIC X(1).                   CPWSX394
003400             88  PPHDAUTL-ERROR         VALUE 'Y'.                CPWSX394
003500         05  EMPLOYEE-ID              PIC X(9).                   CPWSX394
003600******************************************************************CPWSX394
003500*****HOUR BALANCE ARRAY                                          *CPWSX394
003600******************************************************************CPWSX394
003900         05  HOUR-BALANCE-WORK.                                   CPWSX394
028600*****                                                          CD 52191347
005820             10  HOUR-BALANCE-ARRAY   OCCURS 68 TIMES.            52121330
004100                 15  HRS-BAL          PIC S9(7)V99 COMP-3.        CPWSX394
004900******************************************************************CPWSX394
005000*****DOLLAR BALANCE ARRAY                                        *CPWSX394
005100******************************************************************CPWSX394
004500         05  DOLLAR-BALANCE-WORK.                                 CPWSX394
028609*****        10  DOLLAR-BALANCE-ARRAY OCCURS 50 TIMES.            52191347
028610             10  DOLLAR-BALANCE-ARRAY OCCURS 57 TIMES.            52191347
004700                 15  DOLLAR-BAL       PIC S9(7)V99 COMP-3.        CPWSX394
