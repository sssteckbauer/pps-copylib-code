000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSGSRX                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:__P. THOMPSON____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*  - MODIFIED FOR FULL ACCOUNTING UNIT.                      */   32021138
000007**************************************************************/   32021138
000100**************************************************************/  *36560949
000200*  COPYMEMBER: CPWSGSRX                                      */  *36560949
000300*  RELEASE: ____0949____  SERVICE REQUEST(S): ____3656____   */  *36560949
000400*  NAME: ______JYL______  CREATION DATE: ____12/14/94_____   */  *36560949
000500*  DESCRIPTION:                                              */  *36560949
000600*  - EXTERNAL AREA FOR GRADUATE STUDENT REPORTING.           */  *36560949
000700*  - THE LENGTH OF THE EMP-DATA, APPT-DATA AND DIST-DATA     */  *36560949
000800*    FIELDS CORRESPONDS TO THE LENGTH OF EACH OF THESE       */  *36560949
000900*    FIELDS IN COPYMEMBER CPWSGSRR.                          */  *36560949
001000**************************************************************/  *36560949
001100*    COPYID=CPWSGSRX                                              CPWSGSRX
001200*01  GRADUATE-STUDENT-RECORDS  EXTERNAL.                          CPWSGSRX
001300*****03 CPWSGSRX-EXTERNAL-AREA       PIC X(21504).                32021138
001310     03 CPWSGSRX-EXTERNAL-AREA       PIC X(22656).                32021138
001400     03 CPWSGSRX-EXTERNAL-DATA REDEFINES CPWSGSRX-EXTERNAL-AREA.  CPWSGSRX
001500        05 CPWSGSRX-ERROR-CODE       PIC 9(02).                   CPWSGSRX
001600           88  GSRX-RETURN-OK        VALUE ZERO.                  CPWSGSRX
001700           88  GSRX-SQLCODE-ERROR    VALUE 99.                    CPWSGSRX
001800        05 CPWSGSRX-APPT-COUNT       PIC S9(03) COMP-3.           CPWSGSRX
001900        05 CPWSGSRX-DIST-COUNT       PIC S9(03) COMP-3.           CPWSGSRX
002000        05 CPWSGSRX-PRIOR-EMP-ID     PIC X(09).                   CPWSGSRX
002100        05 CPWSGSRX-KEY-CHG-FLAG     PIC X.                       CPWSGSRX
002200           88  GSRX-KEYCHG           VALUE 'K'.                   CPWSGSRX
002300        05 CPWSGSRX-DATA.                                         CPWSGSRX
002400           07 CPWSGSRX-EMP-DATA      PIC X(220).                  CPWSGSRX
002500           07 CPWSGSRX-APPT-DATA     OCCURS  9 TIMES PIC X(245).  CPWSGSRX
002600*****      07 CPWSGSRX-DIST-DATA     OCCURS 72 TIMES PIC X(259).  32021138
002610           07 CPWSGSRX-DIST-DATA     OCCURS 72 TIMES PIC X(275).  32021138
002700        05 CPWSGSRX-FILLER           PIC X(415).                  CPWSGSRX
