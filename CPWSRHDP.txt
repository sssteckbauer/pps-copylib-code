000000**************************************************************/   54541281
000001*  COPYMEMBER: CPWSRHDP                                      */   54541281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15454____  */   54541281
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/22/99__  */   54541281
000004*  DESCRIPTION:                                              */   54541281
000005*  -  ADDED DEPENDENT MEDICAL COVERAGE END DATE (EDB 0659),  */   54541281
000006*           DEPENDENT DENTAL  COVERAGE END DATE (EDB 0656),  */   54541281
000007*           DEPENDENT VISION  COVERAGE END DATE (EDB 0657),  */   54541281
000008*           DEPENDENT LEGAL   COVERAGE END DATE (EDB 0658),  */   54541281
000009*                                                            */   54541281
000010**************************************************************/   54541281
000000**************************************************************/   36260771
000001*  COPYMEMBER: CPWSRHDP                                      */   36260771
000002*  RELEASE: ___0771______ SERVICE REQUEST(S): _____3626____  */   36260771
000003*  NAME:__PST____________ CREATION DATE:      ___05/20/93__  */   36260771
000004*  DESCRIPTION:                                              */   36260771
000005*  - WORKING-STORAGE LAYOUT OF PPPHDP TABLE ROW.             */   36260771
000007*                                                            */   36260771
000008**************************************************************/   36260771
004200*    COPYID=CPWSRHDP                                              CPWSRHDP
004300*01  HDP-ROW.                                                     CPWSRHDP
005600     10 EMPLID               PIC X(9).                            CPWSRHDP
005700     10 ITERATION-NUMBER     PIC X(3).                            CPWSRHDP
005800     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSRHDP
005900     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSRHDP
006000     10 HDP-NUM              PIC X(2).                            CPWSRHDP
006100     10 DELETE-FLAG          PIC X(1).                            CPWSRHDP
006200     10 ERH-INCORRECT        PIC X(1).                            CPWSRHDP
006300     10 HDP-NAME             PIC X(26).                           CPWSRHDP
006400     10 HDP-NAME-C           PIC X(1).                            CPWSRHDP
006500     10 HDP-BIRTH-DATE       PIC X(10).                           CPWSRHDP
006600     10 HDP-BIRTH-DATE-C     PIC X(1).                            CPWSRHDP
006700     10 HDP-REL-TO-EMP       PIC X(1).                            CPWSRHDP
006800     10 HDP-REL-TO-EMP-C     PIC X(1).                            CPWSRHDP
006900     10 HDP-SSN              PIC X(9).                            CPWSRHDP
007000     10 HDP-SSN-C            PIC X(1).                            CPWSRHDP
007100     10 HDP-SEX-CODE         PIC X(1).                            CPWSRHDP
007200     10 HDP-SEX-CODE-C       PIC X(1).                            CPWSRHDP
007300     10 HDP-DISABLED-CODE    PIC X(1).                            CPWSRHDP
007400     10 HDP-DISABLED-CD-C    PIC X(1).                            CPWSRHDP
007500     10 HDP-HLTH-COVEFFDT    PIC X(10).                           CPWSRHDP
007600     10 HDP-HLTH-CVEFDT-C    PIC X(1).                            CPWSRHDP
007700     10 HDP-DENTL-COVEFFDT   PIC X(10).                           CPWSRHDP
007800     10 HDP-DENTL-CVEFDT-C   PIC X(1).                            CPWSRHDP
007900     10 HDP-VIS-COVEFFDT     PIC X(10).                           CPWSRHDP
008000     10 HDP-VIS-CVEFDT-C     PIC X(1).                            CPWSRHDP
008100     10 HDP-LEGAL-COVEFFDT   PIC X(10).                           CPWSRHDP
008200     10 HDP-LEGAL-CVEFDT-C   PIC X(1).                            CPWSRHDP
008300     10 HDP-HLTH-COVENDDT    PIC X(10).                           54541281
008400     10 HDP-HLTH-CVENDDT-C   PIC X(1).                            54541281
008500     10 HDP-DENTL-COVENDDT   PIC X(10).                           54541281
008600     10 HDP-DEN-CVENDDT-C    PIC X(1).                            54541281
008700     10 HDP-VIS-COVENDDT     PIC X(10).                           54541281
008800     10 HDP-VIS-CVENDDT-C    PIC X(1).                            54541281
008900     10 HDP-LEGAL-COVENDDT   PIC X(10).                           54541281
009000     10 HDP-LEGL-CVENDDT-C   PIC X(1).                            54541281
