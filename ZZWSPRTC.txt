000100**************************************************************/   36190541
000200*  COPYMEMBER: ZZWSPRTC                                      */   36190541
000300*  RELEASE: ____0541____  SERVICE REQUEST(S): ____3619____   */   36190541
000400*  NAME:    _C._RIDLION_  CREATION DATE:      __02/28/91__   */   36190541
000500*  DESCRIPTION:                                              */   36190541
000600*    - NEW FOR EDB ONLINE INQUIRY                            */   36190541
000700**************************************************************/   36190541
000800************ PRINT START AREA 3000 BYTES*****************         ZZWSPRTC
000900   05  START-C-USER-ID           PIC X(08).                       ZZWSPRTC
001000   05  START-C-MAP               PIC X(07).                       ZZWSPRTC
001100   05  START-C-NUM-COPIES        PIC 99.                          ZZWSPRTC
001200   05  START-C-SCREEN-AREA       PIC X(2500).                     ZZWSPRTC
001300   05  FILLER                    PIC X(483).                      ZZWSPRTC
