000100*----------------------------------------------------------------*36410717
000200*|**************************************************************|*36410717
000300*|* COPYMEMBER: CPWSXDDE                                       *|*36410717
000400*|*  RELEASE: __0717_______ SERVICE REQUEST(S): _____3641____  *|*36410717
000500*|*  NAME:______UCOP_______ MODIFICATION DATE:  ___11/19/92__  *|*36410717
000600*|*  DESCRIPTION:                                              *|*36410717
000700*|*  - EDB MAINTENANCE RE-ENGINEERING PROJECT                  *|*36410717
000800*|*    REWRITE FOR CONSOLIDATION OF MODIFICATIONS.             *|*36410717
000900*|**************************************************************|*36410717
001000*----------------------------------------------------------------*36410717
000200*    COPYID=CPWSXDDE                                              CPWSXDDE
000200******************************************************************CPWSXDDE
001300**  THIS RECORD IS USED FOR DEDUCTION ELEMENT MAINTENANCE IN    **CPWSXDDE
001400**  ORDER TO SIMULATE STANDARD DATA ELEMENT TABLE PROCESSING    **CPWSXDDE
000500******************************************************************CPWSXDDE
001600*01  XDDE-DEDCTN-DATA-ELMT-REC.                                   CPWSXDDE
001700     03  XDDE-DELETE                PIC X.                        CPWSXDDE
000800     03  XDDE-KEY.                                                CPWSXDDE
001900         05  XDDE-K-FILL            PIC X(9)  VALUE SPACES.       CPWSXDDE
001000         05  XDDE-K-ELEM-NO.                                      CPWSXDDE
002100             07  XDDE-ELEM-1        PIC X     VALUE '6'.          CPWSXDDE
002200             07  XDDE-ELEM-234      PIC 999  VALUE ZEROS.         CPWSXDDE
002300     03  XDDE-CK-DIGT               PIC X     VALUE ZERO.         CPWSXDDE
002400     03  XDDE-SET-TRANS-KEY-POSN    PIC 9(01).                    CPWSXDDE
002500     03  XDDE-SET-TRANS-MANDATORY   PIC X(01).                    CPWSXDDE
002600     03  XDDE-SET-TRANS-MSG-ROUTE   PIC X(01).                    CPWSXDDE
002700     03  XDDE-SET-TRANS-MSG-NOTPYRL PIC X(01).                    CPWSXDDE
002800     03  XDDE-LEGAL-UPDT            PIC 9     VALUE ZERO.         CPWSXDDE
002900     03  XDDE-CRD-STRT              PIC 99.                       CPWSXDDE
003000     03  XDDE-DATA-TYPE             PIC 9     VALUE 1.            CPWSXDDE
003100     03  XDDE-EXT-LNGH              PIC 99    VALUE 07.           CPWSXDDE
003200     03  XDDE-TABLE-EDIT-RTN        PIC S9(3) COMP-3.             CPWSXDDE
003300     03  XDDE-MAINT-BAL-TYPE        PIC X.                        CPWSXDDE
003400     03  XDDE-DEC-PNT               PIC 9     VALUE 2.            CPWSXDDE
003500     03  XDDE-ELEM-NAME             PIC X(8)  VALUE 'DEDUCTN '.   CPWSXDDE
003600     03  XDDE-NAME-FIL              PIC X(7)  VALUE SPACES.       CPWSXDDE
003700     03  XDDE-DATE-EDIT             PIC X     VALUE SPACES.       CPWSXDDE
003800     03  XDDE-RANGE-EDIT-START      PIC S9(4) COMP.               CPWSXDDE
003900     03  XDDE-EDIT-AREA             PIC X(93) VALUE SPACES.       CPWSXDDE
002600     03  XDDE-VALUE-EDIT  REDEFINES XDDE-EDIT-AREA.               CPWSXDDE
004200         05  XDDE-VALUE1            PIC 9(7).                     CPWSXDDE
004400         05  XDDE-VALUE2            PIC 9(7).                     CPWSXDDE
004500         05  XDDE-VAL-FIL           PIC X(79).                    CPWSXDDE
003200     03  XDDE-RANGE-EDIT  REDEFINES XDDE-EDIT-AREA.               CPWSXDDE
004800         05  XDDE-RANGE1            PIC 9(7).                     CPWSXDDE
004900         05  XDDE-RANGE2            PIC 9(7).                     CPWSXDDE
005000         05  XDDE-RNG-FIL           PIC X(79).                    CPWSXDDE
005100     03  XDDE-CON-EDIT-ROUTINES.                                  CPWSXDDE
005200         05  XDDE-CON-EDIT-RTN      PIC S9(3) COMP-3  OCCURS 10.  CPWSXDDE
005300     03  XDDE-GTN-EDIT-ROUTINES.                                  CPWSXDDE
005400         05  XDDE-GTN-EDIT-RTN      PIC S9(3) COMP-3  OCCURS 5.   CPWSXDDE
005500     03  XDDE-IMP-MAINT-ROUTINES.                                 CPWSXDDE
005600         05  XDDE-IMP-MAINT-RTN     PIC S9(3) COMP-3  OCCURS 24.  CPWSXDDE
005700     03  XDDE-DB-TBL-ID             PIC X(6).                     CPWSXDDE
004000     SKIP2                                                        CPWSXDDE
