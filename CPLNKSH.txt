000000**************************************************************/   52191347
000001*  COPYMEMBER: CPLNKSH                                       */   52191347
000002*  RELEASE: ___1347______ SERVICE REQUEST(S): ____15219____  */   52191347
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___03/16/01__  */   52191347
000004*  DESCRIPTION:                                              */   52191347
000005*  - ADDED FIELD FOR STORING THE DCP CASUAL GROSS LIMIT      */   52191347
000006*    USED IN CALCULATING THE DCP CASUAL DEDUCTION.           */   52191347
000009**************************************************************/   52191347
000100**************************************************************/   01900583
000200*  COPYMEMBER: CPLNKSH                                       */   01900583
000300*  RELEASE: ___0583______ SERVICE REQUEST(S): ____10190____  */   01900583
000400*  NAME:_____J.WILCOX____ CREATION DATE:      ___06/21/91__  */   01900583
000500*  DESCRIPTION:                                              */   01900583
000600*  - NEW LINKAGE COPYMEMBER FOR THE SAFE HARBOR (DCP CASUALS)*/   01900583
000700*    CALCULATION MODULE 'PPBENSH'.                           */   01900583
000800**************************************************************/   01900583
000900*    COPYID=CPLNKSH                                               CPLNKSH
001000*01  PPBENSH-INTERFACE-DATA.                                      CPLNKSH
001100     05  KSH-DATA-PASSED-IN.                                      CPLNKSH
001200         10  KSH-REDUCTIONS-SH               PIC S9(5)V99 COMP-3. CPLNKSH
001300         10  KSH-MAX-SH-GROSS                PIC S9(5)V99 COMP-3. CPLNKSH
001310         10  KSH-SFHR-DCP-GROSS-LIMIT        PIC S9(7)V99 COMP-3. 52191347
001400     05  KSH-DATA-PASSED-IN-FOR-UPDATE.                           CPLNKSH
001500         10  KSH-YTD-SH-GROSS                PIC S9(7)V99 COMP-3. CPLNKSH
001600         10  KSH-PAR-SH-GROSS                PIC S9(7)V99 COMP-3. CPLNKSH
001700     05  KSH-DATA-RETURNED.                                       CPLNKSH
001800         10  KSH-RETURN-STATUS-FLAG          PIC X(01).           CPLNKSH
001900             88  KSH-RETURN-STATUS-NORMAL      VALUE '0'.         CPLNKSH
002000             88  KSH-RETURN-STATUS-ABORTING    VALUE '1'.         CPLNKSH
002100         10  KSH-SH-DED-TO-TAKE              PIC S9(5)V99 COMP-3. CPLNKSH
002200         10  KSH-YTD-SH-GROSS-ADJ            PIC S9(7)V99 COMP-3. CPLNKSH
