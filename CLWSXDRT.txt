000100 01  DRET-DEPT-TIME-TRANSMITTAL.                                  CLWSXDRT
000200     05  DRET-RECORD-CONTROL.                                     CLWSXDRT
000300         10  DRET-DOC-ID.                                         CLWSXDRT
000400             15  DRET-SOURCE-ID          PIC X(4) .               CLWSXDRT
000500             15  DRET-DOC-NUMBER         PIC 9(2) .               CLWSXDRT
000600         10  DRET-RECORD-SEQUENCE    PIC 9(4) .                   CLWSXDRT
000700         10  DRET-RECORD-TYPE        PIC X(1) .                   CLWSXDRT
000800             88  DRET-RECORD-HEADER      VALUE  '1' .             CLWSXDRT
000900             88  DRET-RECORD-DETAIL      VALUE  '5' .             CLWSXDRT
001000             88  DRET-RECORD-TRAILER     VALUE  '9' .             CLWSXDRT
001100     05  DRET-RECORD-BODY        PIC X(89) .                      CLWSXDRT
001200     05  DRET-HEADER-BODY    REDEFINES  DRET-RECORD-BODY .        CLWSXDRT
001300         10  DRET-SOURCE-TYPE        PIC X(1) .                   CLWSXDRT
001400         10  DRET-SOURCE-REFERENCE   PIC X(10) .                  CLWSXDRT
001500         10  DRET-POST-PAY-SCHED     PIC X(2) .                   CLWSXDRT
001600         10  DRET-POST-PAY-END-DATE-FULL.                         CLWSXDRT
001700             15  DRET-POST-PAY-END-CN    PIC X(2) .               CLWSXDRT
001800             15  DRET-POST-PAY-END-DATE.                          CLWSXDRT
001900                 20  DRET-POST-PAY-END-YR    PIC X(2) .           CLWSXDRT
002000                 20  DRET-POST-PAY-END-MO    PIC X(2) .           CLWSXDRT
002100                 20  DRET-POST-PAY-END-DA    PIC X(2) .           CLWSXDRT
002200         10  DRET-DOC-DESCRIPTION    PIC X(40) .                  CLWSXDRT
002300         10  DRET-DOC-REFERENCE-DATE-FULL.                        CLWSXDRT
002400             15  DRET-DOC-REFERENCE-CN   PIC X(2) .               CLWSXDRT
002500             15  DRET-DOC-REFERENCE-DATE.                         CLWSXDRT
002600                 20  DRET-DOC-REFERENCE-YR   PIC X(2) .           CLWSXDRT
002700                 20  DRET-DOC-REFERENCE-MO   PIC X(2) .           CLWSXDRT
002800                 20  DRET-DOC-REFERENCE-DA   PIC X(2) .           CLWSXDRT
002900         10  DRET-HEADER-RETURN-INFO.                             CLWSXDRT
003000             15  DRET-DOC-RETURN-DISPO   PIC X(1) .               CLWSXDRT
003100             15  DRET-DOC-RETURN-ERRNO   PIC 9(1) .               CLWSXDRT
003200             15  DRET-DOC-RETURN-ERRCODE PIC X(2)  OCCURS  3 .    CLWSXDRT
003300             15  DRET-DOC-RETURN-DATE-FULL.                       CLWSXDRT
003400                 20  DRET-DOC-RETURN-CN      PIC X(2) .           CLWSXDRT
003500                 20  DRET-DOC-RETURN-DATE.                        CLWSXDRT
003600                     25  DRET-DOC-RETURN-YR      PIC X(2) .       CLWSXDRT
003700                     25  DRET-DOC-RETURN-MO      PIC X(2) .       CLWSXDRT
003800                     25  DRET-DOC-RETURN-DA      PIC X(2) .       CLWSXDRT
003900             15  DRET-DOC-RETURN-TIME.                            CLWSXDRT
004000                 20  DRET-DOC-RETURN-HR      PIC X(2) .           CLWSXDRT
004100                 20  DRET-DOC-RETURN-MN      PIC X(2) .           CLWSXDRT
004200     05  DRET-DETAIL-BODY    REDEFINES  DRET-RECORD-BODY .        CLWSXDRT
004300         10  DRET-EMPLOYEENO         PIC 9(9) .                   CLWSXDRT
004400         10  DRET-PAY-PER-END-DATE-FULL.                          CLWSXDRT
004500             15  DRET-PAY-PER-END-CN     PIC X(2) .               CLWSXDRT
004600             15  DRET-PAY-PER-END-DATE.                           CLWSXDRT
004700                 20  DRET-PAY-PER-END-YR     PIC X(2) .           CLWSXDRT
004800                 20  DRET-PAY-PER-END-MO     PIC X(2) .           CLWSXDRT
004900                 20  DRET-PAY-PER-END-DA     PIC X(2) .           CLWSXDRT
005000         10  DRET-TITLE-CODE         PIC 9(4) .                   CLWSXDRT
005100         10  DRET-IFIS-INDEX         PIC X(7) .                   CLWSXDRT
005200         10  DRET-IFIS-SUB           PIC X(1) .                   CLWSXDRT
005300         10  DRET-WORK-STUDY-PROG    PIC X(1) .                   CLWSXDRT
005400         10  DRET-PAY-RATE           PIC 9(5)V9(4) .              CLWSXDRT
005500         10  DRET-PAY-RATE-BASE      PIC X(1) .                   CLWSXDRT
005600         10  DRET-PAY-RATE-ADJUST    PIC X(1) .                   CLWSXDRT
005700         10  DRET-DOS                PIC X(3) .                   CLWSXDRT
005800         10  DRET-TIME               PIC 9(3)V9(2) .              CLWSXDRT
005900         10  DRET-TIME-TYPE          PIC X(1) .                   CLWSXDRT
006000         10  DRET-TIME-SIGN          PIC X(1) .                   CLWSXDRT
006100         10  DRET-DETAIL-EXPANSION   PIC X(8) .                   CLWSXDRT
006200         10  DRET-TRANSACTION-GROUP  PIC X(4) .                   CLWSXDRT
006300         10  DRET-DEPT-REFERENCE     PIC X(6) .                   CLWSXDRT
006400         10  DRET-DETAIL-RETURN-INFO.                             CLWSXDRT
006500             15  DRET-DET-RETURN-DISPO   PIC X(1) .               CLWSXDRT
006600             15  DRET-DET-RETURN-ERRNO   PIC 9(1) .               CLWSXDRT
006700             15  DRET-DET-RETURN-ERRCODE PIC X(2)  OCCURS  9 .    CLWSXDRT
006800     05  DRET-TRAILER-BODY   REDEFINES  DRET-RECORD-BODY .        CLWSXDRT
006900         10  DRET-RECORD-COUNT       PIC 9(4) .                   CLWSXDRT
007000         10  DRET-HASH-EMP-NO        PIC 9(13) .                  CLWSXDRT
007100         10  DRET-HASH-TITLE-CODE    PIC 9(8) .                   CLWSXDRT
007200         10  DRET-HASH-PAY-RATE      PIC 9(11)V9(2) .             CLWSXDRT
007300         10  DRET-HASH-TIME          PIC 9(7)V9(2) .              CLWSXDRT
007400         10  DRET-TRAILER-EXPANSION  PIC X(22) .                  CLWSXDRT
007500         10  DRET-TRAILER-RETURN-INFO.                            CLWSXDRT
007600             15  DRET-TRL-RETURN-DISPO   PIC X(1) .               CLWSXDRT
007700             15  DRET-TRL-RETURN-ERRNO   PIC 9(1) .               CLWSXDRT
007800             15  DRET-TRL-RETURN-ERRCODE PIC X(2)  OCCURS  3 .    CLWSXDRT
007900             15  DRET-TRL-RETURN-REJ-NO  PIC 9(4) .               CLWSXDRT
008000             15  DRET-TRL-RETURN-ACC-NO  PIC 9(4) .               CLWSXDRT
008100             15  DRET-TRL-RETURN-WRN-NO  PIC 9(4) .               CLWSXDRT
