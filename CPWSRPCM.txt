000001**************************************************************/   48661418
000002*  COPYMEMBER: CPWSRPCM                                      */   48661418
000003*  RELEASE: ___1418______ SERVICE REQUEST(S): ____14866____  */   48661418
000004*  NAME:_______JLT_______ MODIFICATION DATE:  ___06/20/02__  */   48661418
000005*  DESCRIPTION:                                              */   48661418
000006*  - ADDED YTD SWT DPI GROSS.                                */   48661418
000007**************************************************************/   48661418
000000**************************************************************/   48631400
000001*  COPYMEMBER: CPWSRPCM                                      */   48631400
000002*  RELEASE: ___1400______ SERVICE REQUEST(S): ____14863____  */   48631400
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___03/01/02__  */   48631400
000004*  DESCRIPTION:                                              */   48631400
000005*  - ADDED YTD EIC EXEMPT GROSS.                             */   48631400
000007**************************************************************/   48631400
000000**************************************************************/   52191347
000001*  COPYMEMBER: CPWSRPCM                                      */   52191347
000002*  RELEASE: ___1347______ SERVICE REQUEST(S): ____15219____  */   52191347
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___03/09/01__  */   52191347
000004*  DESCRIPTION:                                              */   52191347
000005*  -  ADDED FACULTY SUMMER SALARY DCP GROSS AND              */   52191347
000006*     SAFE HARBOR DCP GROSS                                  */   52191347
000007**************************************************************/   52191347
000000**************************************************************/   52121330
000001*  COPYMEMBER: CPWSRPCM                                      */   52121330
000002*  RELEASE: ___1330______ SERVICE REQUEST(S): ____15212____  */   52121330
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___02/01/01__  */   52121330
000004*  DESCRIPTION:                                              */   52121330
000005*  -  ADDED HOURS TOWARD CAREER ELIGIBILITY                  */   52121330
000007**************************************************************/   52121330
000000**************************************************************/   52101313
000001*  COPYMEMBER: CPWSRPCM                                      */   52101313
000002*  RELEASE: ___1313______ SERVICE REQUEST(S): ____15210____  */   52101313
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___11/15/00__  */   52101313
000004*  DESCRIPTION:                                              */   52101313
000005*  -  ADDED HOURS TOWARD BENEFITS ELIGIBILITY                */   52101313
000007**************************************************************/   52101313
000000**************************************************************/   48311287
000001*  COPYMEMBER: CPWSRPCM                                      */   48311287
000002*  RELEASE: ___1287______ SERVICE REQUEST(S): ____14831____  */   48311287
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___06/01/00__  */   48311287
000004*  DESCRIPTION:                                              */   48311287
000005*  - ADDED FOUR BENEFIT LAST ACTION MONTH FIELDS             */   48311287
000007**************************************************************/   48311287
000000**************************************************************/   48071211
000001*  COPYMEMBER: CPWSRPCM                                      */   48071211
000002*  RELEASE: ___1211______ SERVICE REQUEST(S): ____14807____  */   48071211
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___10/13/98__  */   48071211
000004*  DESCRIPTION:                                              */   48071211
000005*    ADDED PARKING REDUCTION TAKEN                           */   48071211
000005*          TRANSIT REDUCTION TAKEN                           */   48071211
000007**************************************************************/   48071211
000001**************************************************************/   32181188
000002*  COPYMEMBER: CPWSRBEN                                      */   32181188
000004*  RELEASE: ___1188______ SERVICE REQUEST(S): ____13218____  */   32181188
000005*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___06/01/98__  */   32181188
000006*  DESCRIPTION:                                              */   32181188
000008*   ADDED YTD GROSS FOR DOMESTIC PARTNER IMPUTED INCOME      */   32181188
000009**************************************************************/   32181188
000000**************************************************************/   32041155
000001*  COPYMEMBER: CPWSRPCM                                      */   32041155
000002*  RELEASE: ___1155______ SERVICE REQUEST(S): ____13204____  */   32041155
000003*  NAME:_______JLT_______ MODIFICATION DATE:  ___12/01/97__  */   32041155
000004*  DESCRIPTION:                                              */   32041155
000005*  -  ADDED YTD-ESL-GROSS AND YTD-WCR-REFUND.                */   32041155
000008**************************************************************/   32041155
000000**************************************************************/   34161098
000001*  COPYMEMBER: CPWSRPCM                                      */   34161098
000002*  RELEASE: ___1098______ SERVICE REQUEST(S): ____13416____  */   34161098
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___10/16/96__  */   34161098
000004*  DESCRIPTION:                                              */   34161098
000005*  -  ADDED QTD-SWT-GROSS                                    */   34161098
000008**************************************************************/   34161098
000000**************************************************************/   17850950
000001*  COPYMEMBER: CPWSRPCM                                      */   17850950
000002*  RELEASE: ___0950______ SERVICE REQUEST(S): ____11785____  */   17850950
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___11/16/94__  */   17850950
000004*  DESCRIPTION:                                              */   17850950
000005*  -  ADDED YTD-EXCL-MOVE-EXP                                */   17850950
000006*                                                            */   17850950
000007**************************************************************/   17850950
000100**************************************************************/   17040930
000200*  COPYMEMBER: CPWSRPCM                                      */   17040930
000300*  RELEASE: ___0930______ SERVICE REQUEST(S): ____11704____  */   17040930
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___08/31/94__  */   17040930
000500*  DESCRIPTION:                                              */   17040930
000600*  - ADDED 12 FIELDS FOR HOLDING HOURS ON PAY STATUS         */   17040930
000700*                                                            */   17040930
000800**************************************************************/   17040930
000100**************************************************************/   15360842
000200*  COPYMEMBER: CPWSRPCM                                      */   15360842
000300*  RELEASE: ___0842______ SERVICE REQUEST(S): _____11536____ */   15360842
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___11/04/93__  */   15360842
000500*  DESCRIPTION:                                              */   15360842
000600*  - ADDED 6 NEW COLUMNS FOR OTHER STATE WITHHOLDING TAX     */   15360842
000700*    GROSSES AND NAMES                                       */   15360842
000800**************************************************************/   15360842
000000**************************************************************/   36450775
000001*  COPYMEMBER: CPWSRPCM                                      */   36450775
000002*  RELEASE: ___0775______ SERVICE REQUEST(S): _____3645____  */   36450775
000003*  NAME:_______DDM_______ MODIFICATION DATE:  __06/01/93____ */   36450775
000004*  DESCRIPTION: ADD 3 NEW COLUMNS                            */   36450775
000005*    FOR EDB ENTRY/UPDATE IMPLEMENTATION.                    */   36450775
000100**************************************************************/   07780654
000200*  COPYMEMBER: CPWSRPCM                                      */   07780654
000300*  RELEASE: ___0654______ SERVICE REQUEST(S): _____0778____  */   07780654
000400*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___04/09/92__  */   07780654
000500*  DESCRIPTION:                                              */   07780654
000600*    ADDED FYTD-FWT-GROSS                                    */   07780654
000700*                                                            */   07780654
000800**************************************************************/   07780654
000000**************************************************************/   37640580
000001*  COPYMEMBER: CPWSRPCM                                      */   37640580
000002*  RELEASE: ___0580______ SERVICE REQUEST(S): _____3764____  */   37640580
000003*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___05/17/91__  */   37640580
000004*  DESCRIPTION:                                              */   37640580
000005*    ADDED FYTD-RET-GROSS                                    */   37640580
000007**************************************************************/   37640580
000000**************************************************************/   01900580
000001*  COPYMEMBER: CPWSRPCM                                      */   01900580
000002*  RELEASE: ___0580______ SERVICE REQUEST(S): ____10190____  */   01900580
000003*  NAME:_____M. SANO_____ MODIFICATION DATE:  ___05/17/91__  */   01900580
000004*  DESCRIPTION:                                              */   01900580
000005*    ADDED YTD-SFHBR-GROSS                                   */   01900580
000007**************************************************************/   01900580
000100**************************************************************/  *36090513
000200*  COPYMEMBER: CPWSRPCM                                      */  *36090513
000300*  RELEASE: ____0513____  SERVICE REQUEST(S): ____3609____   */  *36090513
000400*  NAME: ______JAG______  CREATION DATE: ____11/05/90_____   */  *36090513
000500*  DESCRIPTION:                                              */  *36090513
000600*  - WORKING-STORAGE LAYOUT OF PCM TABLE ROW.                */  *36090513
000700**************************************************************/  *36090513
000800*    COPYID=CPWSRPCM                                              CPWSRPCM
000900*01  PCM-ROW.                                                     CPWSRPCM
001000     10 EMPLOYEE-ID          PIC X(9).                            CPWSRPCM
001100     10 PAYSTAT-CURR-MO      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
001200     10 PAYSTAT-JAN          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
001300     10 PAYSTAT-FEB          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
001400     10 PAYSTAT-MAR          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
001500     10 PAYSTAT-APR          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
001600     10 PAYSTAT-MAY          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
001700     10 PAYSTAT-JUN          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
001800     10 PAYSTAT-JUL          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
001900     10 PAYSTAT-AUG          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002000     10 PAYSTAT-SEP          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002100     10 PAYSTAT-OCT          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002200     10 PAYSTAT-NOV          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002300     10 PAYSTAT-DEC          PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002400     10 YTD-TOTAL-GROSS      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002500     10 YTD-NONTAX-GROSS     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002600     10 YTD-FICA-GROSS       PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002700     10 YTD-RET-GROSS        PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002800     10 YTD-SWT-GROSS        PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
002900     10 YTD-TAX-FRINGE       PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003000     10 YTD-CETA-GROSS       PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003100     10 QTD-CETA-GROSS       PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003200     10 QTD-FICA-GROSS       PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003300     10 YTD-UI-GROSS         PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003400     10 QTD-UI-GROSS         PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003500     10 YTD-MED-GROSS        PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003600     10 QTD-MED-GROSS        PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003700     10 YTD-FWT-GROSS        PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003800     10 QTD-FWT-GROSS        PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
003900     10 YTD-OTHER-INCOME     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
004000     10 PRI-PAY-SCHED        PIC X(2).                            CPWSRPCM
004100     10 MIN-RCD-FLAG         PIC X(1).                            CPWSRPCM
004200     10 LAST-PAY-DATE        PIC X(10).                           CPWSRPCM
004300     10 PRI-GROSS-CTL        PIC X(3).                            CPWSRPCM
004400     10 LAST-GTN-ACT         PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
004500     10 CHK-DISP-CODE        PIC X(1).                            CPWSRPCM
004600     10 RET-GROSS-CURRMO     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
004700     10 RET-GROSS-PRIOR1     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
004800     10 RET-GROSS-PRIOR2     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
004900     10 RET-GROSS-PRIOR3     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005000     10 RET-GROSS-PRIOR4     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005100     10 RED-GROSS-CURRMO     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005200     10 RED-GROSS-PRIOR1     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005300     10 RED-GROSS-PRIOR2     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005400     10 RED-GROSS-PRIOR3     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005500     10 RED-GROSS-PRIOR4     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005600     10 RET-RATE-CURRMO      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005700     10 RET-RATE-PRIOR1      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005800     10 RET-RATE-PRIOR2      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
005900     10 RET-RATE-PRIOR3      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006000     10 RET-RATE-PRIOR4      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006100     10 YTD-TT-GROSS         PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006200     10 YTD-ALT-TT-GROSS     PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006300     10 ETD-TT-GROSS         PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006400     10 FCP-TST-TOT-GRS      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006500     10 FCP-TST-TAX-GRS      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006600     10 DCP-RATE-CURRMO      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006700     10 DCP-RATE-PRIOR1      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006800     10 DCP-RATE-PRIOR2      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
006900     10 DCP-RATE-PRIOR3      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
007000     10 DCP-RATE-PRIOR4      PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
007100     10 YTD-NDIP-GROSS       PIC S9999999V99 USAGE COMP-3.        CPWSRPCM
007200     10 FYTD-RET-GROSS       PIC S9999999V99 USAGE COMP-3.        37640580
007200     10 YTD-SFHBR-GROSS      PIC S9999999V99 USAGE COMP-3.        01900580
009600     10 FYTD-FWT-GROSS       PIC S9999999V99 USAGE COMP-3.        07780654
009700     10 F-YTD-ADDL-COMP      PIC S9999999V99 USAGE COMP-3.        36450775
009800     10 C-YTD-REG-PAY        PIC S9999999V99 USAGE COMP-3.        36450775
009900     10 C-YTD-ADDL-COMP-SM   PIC S9999999V99 USAGE COMP-3.        36450775
011400     10 YTD-OSWT-GROSS-1     PIC S9999999V99 USAGE COMP-3.        15360842
011500     10 YTD-OSWT-GROSS-2     PIC S9999999V99 USAGE COMP-3.        15360842
011600     10 YTD-OSWT-GROSS-3     PIC S9999999V99 USAGE COMP-3.        15360842
011700     10 YTD-OSWT-NAME-1      PIC X(02).                           15360842
011800     10 YTD-OSWT-NAME-2      PIC X(02).                           15360842
011900     10 YTD-OSWT-NAME-3      PIC X(02).                           15360842
012800     10 HOLD-PAYSTAT-01      PIC S9999999V99 USAGE COMP-3.        17040930
012900     10 HOLD-PAYSTAT-02      PIC S9999999V99 USAGE COMP-3.        17040930
013000     10 HOLD-PAYSTAT-03      PIC S9999999V99 USAGE COMP-3.        17040930
013100     10 HOLD-PAYSTAT-04      PIC S9999999V99 USAGE COMP-3.        17040930
013200     10 HOLD-PAYSTAT-05      PIC S9999999V99 USAGE COMP-3.        17040930
013300     10 HOLD-PAYSTAT-06      PIC S9999999V99 USAGE COMP-3.        17040930
013400     10 HOLD-PAYSTAT-07      PIC S9999999V99 USAGE COMP-3.        17040930
013500     10 HOLD-PAYSTAT-08      PIC S9999999V99 USAGE COMP-3.        17040930
013600     10 HOLD-PAYSTAT-09      PIC S9999999V99 USAGE COMP-3.        17040930
013700     10 HOLD-PAYSTAT-10      PIC S9999999V99 USAGE COMP-3.        17040930
013800     10 HOLD-PAYSTAT-11      PIC S9999999V99 USAGE COMP-3.        17040930
013900     10 HOLD-PAYSTAT-12      PIC S9999999V99 USAGE COMP-3.        17040930
014000     10 YTD-EXCL-MOVE-EXP    PIC S9999999V99 USAGE COMP-3.        17850950
014100     10 QTD-SWT-GROSS        PIC S9999999V99 USAGE COMP-3.        34161098
014100     10 YTD-ESL-GROSS        PIC S9999999V99 USAGE COMP-3.        32041155
014100     10 YTD-WCR-REFUND       PIC S9999999V99 USAGE COMP-3.        32041155
014100     10 YTD-DPI-GROSS        PIC S9999999V99 USAGE COMP-3.        32181188
014104     10 PARK-RED-TAKEN       PIC S9(05)V99   USAGE COMP-3.        48071211
014104     10 TRAN-RED-TAKEN       PIC S9(05)V99   USAGE COMP-3.        48071211
014106     10 MED-CON-ACT-MONTH    PIC S9(02)      USAGE COMP-3.        48311287
014107     10 DEN-CON-ACT-MONTH    PIC S9(02)      USAGE COMP-3.        48311287
014108     10 VIS-CON-ACT-MONTH    PIC S9(02)      USAGE COMP-3.        48311287
014109     10 LEG-CON-ACT-MONTH    PIC S9(02)      USAGE COMP-3.        48311287
014109     10 HRS-BEN-ELIG-TOT     PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-CUR     PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-01      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-02      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-03      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-04      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-05      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-06      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-07      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-08      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-09      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-10      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-11      PIC S9(07)V99   USAGE COMP-3.        52101313
014109     10 HRS-BEN-ELIG-12      PIC S9(07)V99   USAGE COMP-3.        52101313
014124     10 HRS-CAR-ELIG-TOT     PIC S9(07)V99   USAGE COMP-3.        52121330
014125     10 HRS-CAR-ELIG-CUR     PIC S9(07)V99   USAGE COMP-3.        52121330
014126     10 HRS-CAR-ELIG-01      PIC S9(07)V99   USAGE COMP-3.        52121330
014127     10 HRS-CAR-ELIG-02      PIC S9(07)V99   USAGE COMP-3.        52121330
014128     10 HRS-CAR-ELIG-03      PIC S9(07)V99   USAGE COMP-3.        52121330
014129     10 HRS-CAR-ELIG-04      PIC S9(07)V99   USAGE COMP-3.        52121330
014130     10 HRS-CAR-ELIG-05      PIC S9(07)V99   USAGE COMP-3.        52121330
014131     10 HRS-CAR-ELIG-06      PIC S9(07)V99   USAGE COMP-3.        52121330
014132     10 HRS-CAR-ELIG-07      PIC S9(07)V99   USAGE COMP-3.        52121330
014133     10 HRS-CAR-ELIG-08      PIC S9(07)V99   USAGE COMP-3.        52121330
014134     10 HRS-CAR-ELIG-09      PIC S9(07)V99   USAGE COMP-3.        52121330
014135     10 HRS-CAR-ELIG-10      PIC S9(07)V99   USAGE COMP-3.        52121330
014136     10 HRS-CAR-ELIG-11      PIC S9(07)V99   USAGE COMP-3.        52121330
014137     10 HRS-CAR-ELIG-12      PIC S9(07)V99   USAGE COMP-3.        52121330
014138     10 FTD-FCSS-DCP-GROSS   PIC S9(07)V99   USAGE COMP-3.        52191347
014139     10 FTD-SFHR-DCP-GROSS   PIC S9(07)V99   USAGE COMP-3.        52191347
014140     10 YTD-EIC-EXPT-GROSS   PIC S9(07)V99   USAGE COMP-3.        48631400
014140     10 YTD-SWT-DPI-GROSS    PIC S9(07)V99   USAGE COMP-3.        48661418
014141     10 RCPM-FILLER          PIC X(406).                          48661418
014141**** 10 RCPM-FILLER          PIC X(411).                          48661418
028600*****                                                          CD 48661418
