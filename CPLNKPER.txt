000001**************************************************************/   14240308
000002*  COPYLIB:  CPLNKPER                                        */   14240308
000003*  REL#:  0308   REF: 0278  SERVICE REQUESTS:   ____1424____ */   14240308
000004*  NAME ___JAG_________   MODIFICATION DATE ____08/05/87_____*/   14240308
000005*  DESCRIPTION                                               */   14240308
000006*    NEW LINKAGE ADDED                                       */   14240308
000007*                                                            */   14240308
000010**************************************************************/   14240308
000100**************************************************************/   14240278
000200*  COPYLIB:  CPLNKPER                                        */   14240278
000300*  REL#:  0278   REF: NONE  SERVICE REQUESTS:   ____1424____ */   14240278
000400*  NAME ___BMB_________   MODIFICATION DATE ____01/26/87_____*/   14240278
000500*  DESCRIPTION                                               */   14240278
000600*    NEW LINKAGE ADDED                                       */   14240278
000700*                                                            */   14240278
000800**************************************************************/   14240278
000900*COPYID=CPLNKPER                                                  CPLNKPER
001000******************************************************************CPLNKPER
001100*                        C P L N K P E R                         *CPLNKPER
001200******************************************************************CPLNKPER
001300*                                                                 CPLNKPER
001400*01  PPBENPER-INTERFACE.                                          CPLNKPER
001500*                                                                 CPLNKPER
001600******************************************************************CPLNKPER
001700*      THE FOLLOWING FLAGS AND FIELDS ARE PASSED BY CALLER       *CPLNKPER
001800******************************************************************CPLNKPER
001900*                                                                 CPLNKPER
002000     05  KPER-FICA-ELIG-CODE         PIC X(01).                   CPLNKPER
002100         88  KPER-FICA-ELIG                    VALUE 'E'.         CPLNKPER
002200*                                                                 CPLNKPER
002300     05  KPER-RETIRE-SYS-CODE        PIC X(01).                   CPLNKPER
002400         88  KPER-RETIRE-PERS                  VALUE 'P'.         CPLNKPER
002500*                                                                 CPLNKPER
002600     05  KPER-DED-PAY-SCHED-CODE     PIC X(02).                   CPLNKPER
002700*****    88  KPER-VALID-PAY-SCHED         VALUE 'BW' 'SM' 'MO'.   14240308
002710         88  KPER-VALID-PAY-SCHED    VALUE 'MA' 'BW' 'SM' 'MO'.   14240308
002800         88  KPER-PAY-BIWEEKLY                 VALUE 'BW'.        CPLNKPER
002900         88  KPER-PAY-SEMI-MONTHLY             VALUE 'SM'.        CPLNKPER
003000*****    88  KPER-PAY-MONTHLY                  VALUE 'MO'.        14240308
003010         88  KPER-PAY-MONTHLY                  VALUE 'MO' 'MA'.   14240308
003100*                                                                 CPLNKPER
003200     05  KPER-RETIREMENT-GROSS       PIC S9(07)V9(02).            CPLNKPER
003300*                                                                 CPLNKPER
003400     05  KPER-RETIRE-GROSS-AMOUNTS.                               CPLNKPER
003500         10  KPER-CUR-MO-RETIRE-GROSS                             CPLNKPER
003600                                     PIC S9(07)V9(02).            CPLNKPER
003700         10  KPER-1-MO-RETIRE-GROSS  PIC S9(07)V9(02).            CPLNKPER
003800         10  KPER-2-MO-RETIRE-GROSS  PIC S9(07)V9(02).            CPLNKPER
003900         10  KPER-3-MO-RETIRE-GROSS  PIC S9(07)V9(02).            CPLNKPER
004000         10  KPER-4-MO-RETIRE-GROSS  PIC S9(07)V9(02).            CPLNKPER
004100     05  KPER-RETIRE-GROSS-TABLE REDEFINES                        CPLNKPER
004200         KPER-RETIRE-GROSS-AMOUNTS.                               CPLNKPER
004300         10  KPER-MO-RETIRE-GROSS    OCCURS 5 TIMES               CPLNKPER
004400                                     PIC S9(07)V9(02).            CPLNKPER
004500*                                                                 CPLNKPER
004600******************************************************************CPLNKPER
004700*      THE FOLLOWING FLAGS AND FIELDS ARE RETURNED TO CALLER     *CPLNKPER
004800******************************************************************CPLNKPER
004900*                                                                 CPLNKPER
005000     05  KPER-RETURN-STATUS-FLAG     PIC X(01).                   CPLNKPER
005100         88  KPER-RETURN-STATUS-NORMAL           VALUE '0'.       CPLNKPER
005200         88  KPER-RETURN-STATUS-ABORTING         VALUE '1'.       CPLNKPER
005300*                                                                 CPLNKPER
005400     05  KPER-NOT-ENROLLED-IN-PER-FLAG PIC X(01).                 CPLNKPER
005500         88  KPER-ENROLLED-IN-PER                VALUE '0'.       CPLNKPER
005600         88  KPER-NOT-ENROLLED-IN-PER            VALUE '1'.       CPLNKPER
005700*                                                                 CPLNKPER
005800     05  KPER-INVALID-LOOKUP-ARG-FLAG PIC X(01).                  CPLNKPER
005900         88  KPER-INVALID-LOOKUP-ARG             VALUE '1'.       CPLNKPER
006000*                                                                 CPLNKPER
006100     05  KPER-RETURN-FIELDS.                                      CPLNKPER
006200         10  KPER-CUR-MO-RETIRE-RATE PIC S9(05)V9(4).             CPLNKPER
006300         10  KPER-PERS-COMP-GROSS    PIC S9(05)V9(4).             CPLNKPER
006400         10  KPER-PERS-EXEMPT-AMOUNT PIC S9(05)V9(4).             CPLNKPER
006500         10  KPER-CUR-MO-RETIRE-GROSS-ADJ                         CPLNKPER
006600                                     PIC S9(07)V9(02).            CPLNKPER
006700*                                                                 CPLNKPER
006800         10  KPER-MO-PORT-DED-AMTS.                               CPLNKPER
006900             15  KPER-CUR-MO-PORT-DED                             CPLNKPER
007000                                     PIC S9(07)V9(02).            CPLNKPER
007100             15  KPER-1-MO-PORT-DED  PIC S9(07)V9(02).            CPLNKPER
007200             15  KPER-2-MO-PORT-DED  PIC S9(07)V9(02).            CPLNKPER
007300             15  KPER-3-MO-PORT-DED  PIC S9(07)V9(02).            CPLNKPER
007400             15  KPER-4-MO-PORT-DED  PIC S9(07)V9(02).            CPLNKPER
007500         10  KPER-MO-PORT-DED-TABLE REDEFINES                     CPLNKPER
007600             KPER-MO-PORT-DED-AMTS.                               CPLNKPER
007700             15  KPER-MO-PORT-DED    OCCURS 5 TIMES               CPLNKPER
007800                                     PIC S9(07)V9(02).            CPLNKPER
007900*                                                                 CPLNKPER
008000         10  KPER-TOT-PERS-RETIRE-DEDS                            CPLNKPER
008100                                     PIC S9(05)V9(02).            CPLNKPER
