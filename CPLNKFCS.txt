000100**************************************************************/   14240278
000200*  COPYLIB:  CPLNKFCS                                        */   14240278
000300*  REL#:  0278   REF: NONE  SERVICE REQUESTS:   ____1424____ */   14240278
000400*  NAME ___BMB_________   MODIFICATION DATE ____01/12/87_____*/   14240278
000500*  DESCRIPTION                                               */   14240278
000600*   ADDED NEW LINKAGE                                        */   14240278
000700*                                                            */   14240278
000800**************************************************************/   14240278
000900*COPYID=CPLNKFCS                                                  CPLNKFCS
001000******************************************************************CPLNKFCS
001100*                        C P L N K F C S                         *CPLNKFCS
001200******************************************************************CPLNKFCS
001300*                                                                *CPLNKFCS
001400*01  PPBENFCS-INTERFACE.                                          CPLNKFCS
001500*                                                                 CPLNKFCS
001600******************************************************************CPLNKFCS
001700*      THE FOLLOWING FLAGS AND FIELDS ARE PASSED BY CALLER       *CPLNKFCS
001800******************************************************************CPLNKFCS
001900*                                                                 CPLNKFCS
002000     05  KFCS-ACTION-FLAG            PIC X(01).                   CPLNKFCS
002100         88  KFCS-ACTION-INITIALIZING            VALUE '1'.       CPLNKFCS
002200         88  KFCS-ACTION-RATE-RETRIEVAL          VALUE '2'.       CPLNKFCS
002300         88  KFCS-ACTION-PREMIUM-CALC            VALUE '3'.       CPLNKFCS
002400*                                                                 CPLNKFCS
002500     05  KFCS-SYSTEM-CODE            PIC X(01).                   CPLNKFCS
002600         88  KFCS-VALID-SYSTEM-CODE              VALUE IS 'F'.    CPLNKFCS
002700*                                                                 CPLNKFCS
002800     05  KFCS-RETIREMENT-GROSS       PIC 9(7)V9(2).               CPLNKFCS
002900         88  KFCS-INVALID-RETIREMENT-GROSS       VALUE IS ZERO.   CPLNKFCS
003000*                                                                 CPLNKFCS
003100******************************************************************CPLNKFCS
003200*      THE FOLLOWING FLAGS AND FIELDS ARE RETURNED TO CALLER     *CPLNKFCS
003300******************************************************************CPLNKFCS
003400*                                                                 CPLNKFCS
003500     05  KFCS-RETURN-STATUS-FLAG     PIC X(01).                   CPLNKFCS
003600         88  KFCS-RETURN-STATUS-NORMAL           VALUE '0'.       CPLNKFCS
003700         88  KFCS-RETURN-STATUS-ABORTING         VALUE '1'.       CPLNKFCS
003800*                                                                 CPLNKFCS
003900     05  KFCS-INVALID-LOOKUP-ARG-FLAG PIC X(01).                  CPLNKFCS
004000         88  KFCS-INVALID-LOOKUP-ARG             VALUE '1'.       CPLNKFCS
004100*                                                                 CPLNKFCS
004200     05  KFCS-RETURN-FIELDS.                                      CPLNKFCS
004300         10  KFCS-RATE-AMOUNT        PIC 9(5)V9(2).               CPLNKFCS
004400         10  KFCS-DED-AMOUNT         PIC 9(7)V9(2).               CPLNKFCS
