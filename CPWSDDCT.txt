000000**************************************************************/   05971536
000001*  COPYMEMBER: CPWSDDCT                                      */   05971536
000002*  RELEASE: ___1536______ SERVICE REQUEST(S): ____80597____  */   05971536
000003*  NAME:__S. ISAACS______ MODIFICATION DATE:  ___10/01/03__  */   05971536
000004*  DESCRIPTION:                                              */   05971536
000005*  - Added foreign address record to carrier file.           */   05971536
000006*      Record type '04'.                                     */   05971536
000011**************************************************************/   05971536
000000**************************************************************/   51251295
000001*  COPYMEMBER: CPWSDDCT                                      */   51251295
000002*  RELEASE: ___1295______ SERVICE REQUEST(S): ____15125____  */   51251295
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___07/17/00__  */   51251295
000004*  DESCRIPTION:                                              */   51251295
000005*  - Deleted the Medical/Dental Provider ID field, as such,  */   51251295
000005*    since PPP560 no longer moves this data to the employee  */   51251295
000005*    record. However, the record position must be reserved   */   51251295
000005*    because the IVR New Hire and Open Enrollment processes  */   51251295
000005*    will continue to provide this data in a matching record */   51251295
000005*    layout.                                                 */   51251295
000007**************************************************************/   51251295
000000**************************************************************/   48311287
000001*  COPYMEMBER: CPWSDDCT                                      */   48311287
000002*  RELEASE: ___1287______ SERVICE REQUEST(S): ____14831____  */   48311287
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___06/01/00__  */   48311287
000004*  DESCRIPTION:                                              */   48311287
000005*  - Added new Type 03 record for adjustments from the       */   48311287
000006*    Consolidated Billing process.                           */   48311287
000007**************************************************************/   48311287
000000**************************************************************/   54551281
000001*  COPYMEMBER: CPWSDDCT                                      */   54551281
000002*  RELEASE: ___1281______ SERVICE REQUEST(S): ____15455____  */   54551281
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___06/09/99__  */   54551281
000004*  DESCRIPTION:                                              */   54551281
000005*  - Added Employee Benefits Coverage End Date to Employee   */   54551281
000006*    record. Added Dependent Coverage End date to Dependent  */   54551281
000007*    record.                                                 */   54551281
000008*  - Added century (2 bytes) to all dates.                   */   54551281
000008*  - Premium Amount field defined on dependent record is     */   54551281
000008*    removed. The Premiums field on dependent record was     */   54551281
000008*    never populated; premium is already recorded on the     */   54551281
000008*    employee carrier record.                                */   54551281
000009**************************************************************/   54551281
000100**************************************************************/   32841147
000200*  COPYMEMBER: CPWSDDCT                                      */   32841147
000300*  RELEASE: ___1147______ SERVICE REQUEST(S): ____13284____  */   32841147
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___10/20/97__  */   32841147
000500*  DESCRIPTION:                                              */   32841147
000600*  - Added 2-character location code to positions 243-244    */   32841147
000700*    of the Employee and Dependent records.                  */   32841147
000800**************************************************************/   32841147
000100**************************************************************/   42371094
000200*  COPYMEMBER: CPWSDDCT                                      */   42371094
000300*  RELEASE: ___1094______ SERVICE REQUEST(S): ____14237____  */   42371094
000400*  NAME:______M SANO_____ MODIFICATION DATE:  ___10/21/96__  */   42371094
000500*  DESCRIPTION:                                              */   42371094
000600*  - REPLACED FILLER SPACE WITH RESERVED FIELDS              */   42371094
000700**************************************************************/   42371094
000000**************************************************************/   05540801
000001*  COPYMEMBER: CPWSDDCT                                      */   05540801
000002*  RELEASE: ___0801______ SERVICE REQUEST(S): ____10554____  */   05540801
000003*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___08/06/93__  */   05540801
000004*  DESCRIPTION:                                              */   05540801
000005*  -  NEW RECORD LAYOUTS FOR THE DEDUCTION (CARRIER) RECORD  */   05540801
000010**************************************************************/   05540801
001800*    COPYID=CPWSDDCT                                              CPWSDDCT
002000     05  XDED-EMP-DATA-REC.                                       CPWSDDCT
002000         10  XDED-REC-TYPE             PIC  X(02)  VALUE SPACE.   CPWSDDCT
002010         10  XDED-PLAN-CODE            PIC  X(02)  VALUE SPACE.   CPWSDDCT
002100         10  XDED-ID-NO                PIC  X(09)  VALUE SPACE.   CPWSDDCT
002200         10  XDED-SOC-SEC-NR           PIC  9(09)  VALUE ZERO.    CPWSDDCT
002810*****                                                          CD 48311287
002910         10  XDED-EMPL-NAME            PIC  X(30)  VALUE SPACE.   54551281
002500         10  XDED-EMPL-NAME-SUFFIX     PIC  X(04)  VALUE SPACE.   CPWSDDCT
003010*****                                                          CD 48311287
003110         10  XDED-BIRTH-DATE           PIC  X(08)  VALUE SPACE.   54551281
002700         10  XDED-SEX                  PIC  X(01)  VALUE SPACE.   CPWSDDCT
002800         10  XDED-EMPLMNT-STAT         PIC  X(01)  VALUE SPACE.   CPWSDDCT
002900         10  XDED-SEP-REASON           PIC  X(02)  VALUE SPACE.   CPWSDDCT
003000         10  XDED-PERM-STRT-1          PIC  X(30)  VALUE SPACE.   CPWSDDCT
003100         10  XDED-PERM-STRT-2          PIC  X(30)  VALUE SPACE.   CPWSDDCT
003200         10  XDED-PERM-CITY            PIC  X(21)  VALUE SPACE.   CPWSDDCT
003300         10  XDED-PERM-STATE           PIC  X(02)  VALUE SPACE.   CPWSDDCT
003400         10  XDED-PERM-ZIP-CODE        PIC  X(09)  VALUE SPACE.   CPWSDDCT
003500         10  XDED-COVR-CODE            PIC  X(03)  VALUE SPACE.   CPWSDDCT
003600         10  XDED-PREMIUM-AMOUNT       PIC S9(05)V9(02)           CPWSDDCT
003700                                                   VALUE ZERO.    CPWSDDCT
003800         10  XDED-PRI-PAY-SCHED        PIC  X(02)  VALUE SPACE.   CPWSDDCT
003900         10  XDED-HLTH-DE-ENROLL-IND   PIC  X(01)  VALUE SPACE.   CPWSDDCT
004410*****                                                          CD 48311287
004610         10  XDED-EMPLOYMENT-DATE      PIC  X(08)  VALUE SPACE.   54551281
004620         10  XDED-SEPARATION-DATE      PIC  X(08)  VALUE SPACE.   54551281
004200         10  XDED-RETIRE               PIC  9(01)  VALUE ZERO.    CPWSDDCT
004300         10  XDED-EMP-REL-CODE         PIC  X(01)  VALUE SPACE.   CPWSDDCT
004600         10  XDED-EMP-UNIT-CODE        PIC  X(02)  VALUE SPACE.   CPWSDDCT
004700         10  XDED-EMP-SPCL-HNDLG-CODE  PIC  X(01)  VALUE SPACE.   CPWSDDCT
004800         10  XDED-EMP-DIST-UNIT-CODE   PIC  X(01)  VALUE SPACE.   CPWSDDCT
004900         10  XDED-EMP-COVERAGE-IND     PIC  X(01)  VALUE SPACE.   CPWSDDCT
005210*****                                                          CD 48311287
005410         10  XDED-COV-EFFDATE          PIC  X(08)  VALUE SPACE.   54551281
005420         10  XDED-EMP-COV-EFFDATE      PIC  X(08)  VALUE SPACE.   54551281
005500*****    10  XDED-PROVIDER-ID          PIC  X(26)  VALUE SPACE.   51251295
005600         10  XDED-RESERVED-4           PIC  X(26)  VALUE SPACE.   51251295
004900         10  XDED-RESERVED-1           PIC  X(10)  VALUE SPACE.   42371094
005000         10  XDED-RESERVED-2           PIC  X(01)  VALUE SPACE.   42371094
006001         10  XDED-COVERAGE-END-DATE    PIC  X(08)  VALUE SPACE.   54551281
006002         10  FILLER                    PIC  X(02)  VALUE SPACE.   54551281
006003         10  XDED-RESERVED-3           PIC  X(10)  VALUE SPACE.   54551281
006100         10  XDED-LOCATION-CODE        PIC  X(02)  VALUE SPACE.   32841147
005100         10  XDED-SORT-KEY             PIC  X(02)  VALUE SPACE.   CPWSDDCT
005300*                                                                 CPWSDDCT
002000     05  XDED-DEP-DATA-REC.                                       CPWSDDCT
005410*                                                                 CPWSDDCT
005500         10  XDED-REDE-REC-TYPE        PIC  X(02)  VALUE SPACE.   CPWSDDCT
005600         10  XDED-REDE-PLAN-CODE       PIC  X(02)  VALUE SPACE.   CPWSDDCT
005700         10  XDED-REDE-ID-NO           PIC  X(09)  VALUE SPACE.   CPWSDDCT
005800         10  XDED-REDE-SOC-SEC-NR      PIC  9(09)  VALUE ZERO.    CPWSDDCT
006000         10  XDED-REDE-DEP-NUM         PIC  X(02)  VALUE SPACE.   CPWSDDCT
006100         10  XDED-REDE-DEP-NAME        PIC  X(26)  VALUE SPACE.   CPWSDDCT
007110*****                                                          CD 48311287
007210         10  XDED-REDE-DEP-BDATE       PIC  X(08)  VALUE SPACE.   54551281
006300         10  XDED-REDE-DEP-SEX         PIC  X(01)  VALUE SPACE.   CPWSDDCT
006400         10  XDED-REDE-DEP-SSN         PIC  X(09)  VALUE SPACE.   CPWSDDCT
006500         10  XDED-REDE-DEP-REL         PIC  X(01)  VALUE SPACE.   CPWSDDCT
006600         10  XDED-REDE-DEP-DIS         PIC  X(01)  VALUE SPACE.   CPWSDDCT
006700         10  XDED-REDE-DEP-COVEFFDT.                              CPWSDDCT
007800             15  XDED-REDE-DEP-C-CC    PIC  X(02)  VALUE SPACE.   54551281
006710             15  XDED-REDE-DEP-C-YY    PIC  X(02)  VALUE SPACE.   CPWSDDCT
006720             15  XDED-REDE-DEP-C-MM    PIC  X(02)  VALUE SPACE.   CPWSDDCT
006730             15  XDED-REDE-DEP-C-DD    PIC  X(02)  VALUE SPACE.   CPWSDDCT
007200         10  XDED-REDE-DEP-RESERVED-1  PIC  X(26)  VALUE SPACE.   42371094
007300         10  XDED-REDE-DEP-RESERVED-2  PIC  X(01)  VALUE SPACE.   42371094
008310         10  XDED-REDE-DEP-COVENDDT.                              54551281
007800             15  XDED-REDE-DEP-END-CC  PIC  X(02)  VALUE SPACE.   54551281
007810             15  XDED-REDE-DEP-END-YY  PIC  X(02)  VALUE SPACE.   54551281
007900             15  XDED-REDE-DEP-END-MM  PIC  X(02)  VALUE SPACE.   54551281
008000             15  XDED-REDE-DEP-END-DD  PIC  X(02)  VALUE SPACE.   54551281
008315*****                                                          CD 48311287
008410         10  XDED-REDE-DEP-FILLER      PIC  X(147) VALUE SPACE.   54551281
008520*****                                                          CD 48311287
008800         10  XDED-REDE-DEP-RESERVED-3  PIC  X(09)  VALUE SPACE.   54551281
008900         10  XDED-REDE-LOCATION-CODE   PIC  X(02)  VALUE SPACE.   32841147
006900         10  XDED-REDE-SORT-KEY        PIC  X(02)  VALUE SPACE.   CPWSDDCT
009100     05  XDED-ADJ-DATA-REC.                                       48311287
009200         10  XDED-ADJ-REC-TYPE         PIC  X(02)  VALUE SPACE.   48311287
009300         10  XDED-ADJ-PLAN-CODE        PIC  X(02)  VALUE SPACE.   48311287
009400         10  XDED-ADJ-ID-NO            PIC  X(09)  VALUE SPACE.   48311287
009500         10  XDED-ADJ-SOC-SEC-NR       PIC  9(09)  VALUE ZERO.    48311287
009500         10  XDED-ADJ-COV-MONTH        PIC  X(08)  VALUE ZERO.    48311287
009600         10  XDED-ADJ-PREMIUM-AMOUNT   PIC S9(05)V9(02).          48311287
009900         10  FILLER                    PIC  X(232) VALUE SPACE.   48311287
010000         10  XDED-ADJ-LOCATION-CODE    PIC  X(02)  VALUE SPACE.   48311287
010100         10  XDED-ADJ-SORT-KEY         PIC  X(02)  VALUE SPACE.   48311287
010200                                                                  05971536
010210     05  XDED-FADDR-DATA-REC.                                     05971536
010300         10  XDED-FADDR-REC-TYPE       PIC  X(02)  VALUE SPACE.   05971536
010400         10  XDED-FADDR-PLAN-CODE      PIC  X(02)  VALUE SPACE.   05971536
010500         10  XDED-FADDR-ID-NO          PIC  X(09)  VALUE SPACE.   05971536
010510         10  XDED-FADDR-SOC-SEC-NR     PIC  9(09)  VALUE ZERO.    05971536
010600         10  XDED-FADDR-PROVINCE       PIC  X(20)  VALUE SPACE.   05971536
010700         10  XDED-FADDR-COUNTRY        PIC  X(30)  VALUE SPACE.   05971536
010800         10  XDED-FADDR-POSTAL-CODE    PIC  X(10)  VALUE SPACE.   05971536
010900         10  FILLER                    PIC  X(187) VALUE SPACE.   05971536
011000         10  XDED-FADDR-LOCATION-CODE  PIC  X(02)  VALUE SPACE.   05971536
011100         10  XDED-FADDR-SORT-KEY       PIC  X(02)  VALUE SPACE.   05971536
