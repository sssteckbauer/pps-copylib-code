000100**************************************************************/   36430911
000200*  COPYMEMBER: UCROUTWA                                      */   36430911
000300*  RELEASE: ___0911______ SERVICE REQUEST(S): _____3643____  */   36430911
000400*  NAME:_______MLG_______ MODIFICATION DATE:  __06/30/94____ */   36430911
000500*  DESCRIPTION:                                              */   36430911
000600*    REMOVE MAP NAME FIELDS SO THEY CAN BE LOCATED IN        */   36430911
000700*    UCCOMMON                                                */   36430911
000800**************************************************************/   36430911
000100**************************************************************/   36480863
000200*  COPYMEMBER: UCROUTWA                                      */   36480863
000300*  RELEASE: ___0863______ SERVICE REQUEST(S): _____3648____  */   36480863
000400*  NAME:_______PXP_______ MODIFICATION DATE:  __01/27/94____ */   36480863
000500*  DESCRIPTION:                                              */   36480863
000600*  ADD ITEM TO CARRY SYSTEM PARM 65 INFO, STATUS OF THE      */   36480863
000700*  PAN SUBSYSTEM.                                            */   36480863
000800**************************************************************/   36480863
000100**************************************************************/   36430795
000200*  COPYLIB: UCROUTWA                                         */   36430795
000300*  RELEASE # ___0795___   SERVICE REQUEST NO(S)____3643______*/   36430795
000400*  NAME __MLG__________   MODIFICATION DATE ____07/15/93_____*/   36430795
000500*  DESCRIPTION                                               */   36430795
000600*                                                            */   36430795
000700*       UCROUTER WORK AREA                                   */   36430795
000900*                                                            */   36430795
001000**************************************************************/   36430795
001100*01  UCROUTWA  EXTERNAL.                                          UCROUTWA
001200     05  UCROUTWA-EXT-AREA                 PIC X(2048).           UCROUTWA
001300     05  UCROUTWA-EXT-DATA REDEFINES UCROUTWA-EXT-AREA.           UCROUTWA
014600         10  UCROUTWA-TRANS-ID             PIC X(04).             UCROUTWA
014700         10  UCROUTWA-TOP-MAP-LENGTH       PIC S9(4) COMP.        UCROUTWA
014800         10  UCROUTWA-BOT-MAP-LENGTH       PIC S9(4) COMP.        UCROUTWA
014900         10  UCROUTWA-SUBSYSTEM-NAME       PIC X(21).             UCROUTWA
015000         10  UCROUTWA-DTL-SCREEN-PGM       PIC X(08).             UCROUTWA
003400*******  10  UCROUTWA-DTL-MAP-NAME         PIC X(08).             36430911
003500*******  10  UCROUTWA-TOP-MAP-NAME         PIC X(08).             36430911
003600*******  10  UCROUTWA-BOT-MAP-NAME         PIC X(08).             36430911
015400         10  UCROUTWA-FUNC-DESC            PIC X(30).             UCROUTWA
015500         10  UCROUTWA-FUNCTION-SELECTED    PIC X(04).             UCROUTWA
015600             88  UCROUTWA-FUNCTION-SEL-IS-MMNU VALUE 'MMNU'.      UCROUTWA
015610             88  UCROUTWA-FUNCTION-SEL-IS-PRNT VALUE 'PRNT'.      UCROUTWA
015700         10  UCROUTWA-PREVIOUS-MENU-FUNC   PIC X(04).             UCROUTWA
015800             88  UCROUTWA-PREVIOUS-MENU-IS-MMNU VALUE 'MMNU'.     UCROUTWA
015900         10  UCROUTWA-SAVE-CURSOR-POSITION PIC S9(04) COMP.       UCROUTWA
016000         10  UCROUTWA-LOGIC-AID-FUNC-ARRAY.                       UCROUTWA
016100             15  UCROUTWA-LOGIC-AID-FUNC-ENTRY                    UCROUTWA
016101                        OCCURS 12 INDEXED BY UCROUTWA-LAF-INDEX.  UCROUTWA
016102                 20  UCROUTWA-LOGIC-AID-FUNCTION   PIC X(12).     UCROUTWA
016110                 20  UCROUTWA-LOGIC-AID-SIM-STRING PIC X(08).     UCROUTWA
016300         10  UCROUTWA-RETURN-FUNCTION      PIC X(04).             UCROUTWA
016301         10  UCROUTWA-APPLICATION-ID       PIC X(03).             UCROUTWA
016302         10  UCROUTWA-RECORD-KEY           PIC X(15).             UCROUTWA
016303         10  UCROUTWA-AUDIT-INDICATOR      PIC 9(05).             UCROUTWA
016304             88  UCROUTWA-AUDIT-OFF            VALUE ZERO.        UCROUTWA
016305             88  UCROUTWA-AUDIT-ON             VALUE 1.           UCROUTWA
016306         10  UCROUTWA-HOLD-CURSOR          PIC S9(04) COMP.       UCROUTWA
016307         10  UCROUTWA-HOLD-FUNC            PIC X(04).             UCROUTWA
004900         10  UCROUTWA-PAN-INDICATOR        PIC 9(05).             36480863
005000             88  UCROUTWA-PAN-OFF              VALUE ZERO.        36480863
005100             88  UCROUTWA-PAN-PREP-UNCOND      VALUE 1.           36480863
005200             88  UCROUTWA-PAN-PREP-ON-CMD      VALUE 2.           36480863
005300             88  UCROUTWA-PAN-NO-PREP          VALUE 3.           36480863
005310             88  UCROUTWA-PAN-GEN              VALUE 1 2 3.       36480863
005320             88  UCROUTWA-PAN-KNOWN            VALUE 0 1 2 3.     36480863
005400         10  UCROUTWA-FILLER               PIC X(1669).           36480863
005500*****    10  UCROUTWA-FILLER               PIC X(1674).           36480863
016400***************    END OF SOURCE - UCROUTWA    *******************UCROUTWA
