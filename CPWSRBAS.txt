000000**************************************************************/   03861482
000001*  COPYMEMBER: CPWSRBAS                                      */   03861482
000002*  RELEASE: ___1482______ SERVICE REQUEST(S): ____80386____  */   03861482
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___02/13/03__  */   03861482
000004*  DESCRIPTION:                                              */   03861482
000005*  - ADDED START_PERCENT, START_PERCENT_C, START_BEGIN_DATE, */   03861482
000006*    START_BEGIN_DATE_C, START_END_DATE, START_END_DATE_C.   */   03861482
000007**************************************************************/   03861482
000000**************************************************************/   02561450
000001*  COPYMEMBER: CPWSRBAS                                      */   02561450
000002*  RELEASE: ___1450______ SERVICE REQUEST(S): ____80256____  */   02561450
000003*  NAME:____J KENNEDY____ MODIFICATION DATE:  ___11/07/02__  */   02561450
000004*  DESCRIPTION:                                              */   02561450
000005*  - ADD ELEMENT TO BAS TABLE - RECENTLY SEPARATED VETERAN   */   02561450
000006*  DATE -  RECENT_VET_SEPDT AND RECENT_VET_SEPDT_C           */   02561450
000007**************************************************************/   02561450
000000**************************************************************/   51231302
000001*  COPYMEMBER: CPWSRBAS                                      */   51231302
000002*  RELEASE: ___1302______ SERVICE REQUEST(S): _14838, 15123_ */   51231302
000003*  NAME:___ROY STAPLES___ MODIFICATION DATE:  ___07/28/00__  */   51231302
000004*  DESCRIPTION:                                              */   51231302
000005*    ADDED WCE-VET-STATUS AND WCE-VET-STATUS-C.  REMOVED     */   51231302
000006*    FIELDS CAMPUS-ADDRESS, CAMPUS-BLDG, AND CAMPUS-ROOM, AS */   51231302
000007*    WELL AS ASSOCIATED CHANGE INDICATORS.  REPLACED WITH    */   51231302
000008*    23 BYTE FILLER AS PLACEHOLDER.                          */   51231302
000009**************************************************************/   51231302
000000**************************************************************/   25981207
000001*  COPYMEMBER: CPWSRBAS                                      */   25981207
000002*  RELEASE: ___1207______ SERVICE REQUEST(S): ____12598____  */   25981207
000003*  NAME:___ROY STAPLES___ MODIFICATION DATE:  ___08/26/98__  */   25981207
000004*  DESCRIPTION:                                              */   25981207
000005*    ADDED COMMENTS BEFORE AND AFTER THE TRIP FIELDS TO      */   25981207
000006*    ALERT USERS TO THEIR OBSOLESCENCE IN THE EDB.           */   25981207
000007**************************************************************/   25981207
000001**************************************************************/   32171197
000002*  COPYMEMBER: CPWSRBAS                                      */   32171197
000003*  RELEASE: ___1197______ SERVICE REQUEST(S): ____13217____  */   32171197
000004*  NAME:_____R. STAPLES__ MODIFICATION DATE:  ___06/30/98__  */   32171197
000005*  DESCRIPTION:                                              */   32171197
000006*    ADDED US-DATE-OF-ENTRY (EDB # 1169)                     */   32171197
000007**************************************************************/   32171197
000000**************************************************************/   42601109
000001*  COPYMEMBER: CPWSRBAS                                      */   42601109
000002*  RELEASE: ___1109______ SERVICE REQUEST(S): ____14260____  */   42601109
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___01/30/97__  */   42601109
000004*  DESCRIPTION:                                              */   42601109
000005*  - ADD RETIREMENT/FICA DERIVATION INDICATOR                */   42601109
000007**************************************************************/   42601109
000000**************************************************************/   19530846
000001*  COPYMEMBER: CPWSRBAS                                      */   19530846
000002*  RELEASE: ___0846______ SERVICE REQUEST(S): ____11953____  */   19530846
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___12/16/93__  */   19530846
000004*  DESCRIPTION:                                              */   19530846
000005*    ADD THREE TRIP DATA ELEMENTS AND CHANGE FLAGS           */   19530846
000007**************************************************************/   19530846
000008**************************************************************/   36450793
000009*  COPYMEMBER: CPWSRBAS                                      */   36450793
000010*  RELEASE: ___0793______ SERVICE REQUEST(S): _____3645____  */   36450793
000011*  NAME:_______DDM_______ MODIFICATION DATE:  __07/09/93____ */   36450793
000012*  DESCRIPTION: ADD ALT_DEPT_CD FOR                          */   36450793
000013*               EDB ENTRY/UPDATE IMPLEMENTATION.             */   36450793
000014**************************************************************/   36450793
000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSRBAS                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    DCLGEN FOR THE PPPVBAS1 VIEW                            */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSRBAS
001000* COBOL DECLARATION FOR TABLE PPPVBAS1_BAS                       *CPWSRBAS
001100******************************************************************CPWSRBAS
001200*01  DCLPPPVBAS1-BAS.                                             CPWSRBAS
001300     10 EMPLID               PIC X(9).                            CPWSRBAS
001400     10 ITERATION-NUMBER     PIC X(3).                            CPWSRBAS
001500     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSRBAS
001600     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSRBAS
001700     10 DELETE-FLAG          PIC X(1).                            CPWSRBAS
001800     10 ERH-INCORRECT        PIC X(1).                            CPWSRBAS
001900     10 EMPLOYEE-NAME        PIC X(26).                           CPWSRBAS
002000     10 EMPLOYEE-NAME-C      PIC X(1).                            CPWSRBAS
002100     10 SOC-SEC-NUM          PIC X(9).                            CPWSRBAS
002200     10 SOC-SEC-NUM-C        PIC X(1).                            CPWSRBAS
002300     10 BIRTH-DATE           PIC X(10).                           CPWSRBAS
002400     10 BIRTH-DATE-C         PIC X(1).                            CPWSRBAS
002500     10 FILLER               PIC X(23).                           48381302
003100     10 CITIZEN-STATUS       PIC X(1).                            CPWSRBAS
003200     10 CITIZEN-STATUS-C     PIC X(1).                            CPWSRBAS
003300     10 COUNTRY-CODE         PIC X(3).                            CPWSRBAS
003400     10 COUNTRY-CODE-C       PIC X(1).                            CPWSRBAS
003500     10 CRIT-BACK-IND        PIC X(1).                            CPWSRBAS
003600     10 CRIT-BACK-IND-C      PIC X(1).                            CPWSRBAS
003700     10 CUR-SPECIALTY-1      PIC X(4).                            CPWSRBAS
003800     10 CUR-SPECIALTY-1-C    PIC X(1).                            CPWSRBAS
003900     10 CUR-SPECIALTY-2      PIC X(4).                            CPWSRBAS
004000     10 CUR-SPECIALTY-2-C    PIC X(1).                            CPWSRBAS
004100     10 CUR-SPECIALTY-3      PIC X(4).                            CPWSRBAS
004200     10 CUR-SPECIALTY-3-C    PIC X(1).                            CPWSRBAS
004300     10 DEGREE-DATE          PIC X(10).                           CPWSRBAS
004400     10 DEGREE-DATE-C        PIC X(1).                            CPWSRBAS
004500     10 DEGREE-FIELD         PIC X(4).                            CPWSRBAS
004600     10 DEGREE-FIELD-C       PIC X(1).                            CPWSRBAS
004700     10 DEGREE-INST          PIC X(3).                            CPWSRBAS
004800     10 DEGREE-INST-C        PIC X(1).                            CPWSRBAS
004900     10 EDU-LEVEL            PIC X(1).                            CPWSRBAS
005000     10 EDU-LEVEL-C          PIC X(1).                            CPWSRBAS
005100     10 EDU-LEVEL-YR         PIC X(2).                            CPWSRBAS
005200     10 EDU-LEVEL-YR-C       PIC X(1).                            CPWSRBAS
005300     10 EMPLOYMT-DATE        PIC X(10).                           CPWSRBAS
005400     10 EMPLOYMT-DATE-C      PIC X(1).                            CPWSRBAS
005500     10 EMP-STATUS           PIC X(1).                            CPWSRBAS
005600     10 EMP-STATUS-C         PIC X(1).                            CPWSRBAS
005700     10 ETHNIC-ID            PIC X(1).                            CPWSRBAS
005800     10 ETHNIC-ID-C          PIC X(1).                            CPWSRBAS
005900     10 FICA-MED-ELIG        PIC X(1).                            CPWSRBAS
006000     10 FICA-MED-ELIG-C      PIC X(1).                            CPWSRBAS
006100     10 FOREIGN-ADDR-IND     PIC X(1).                            CPWSRBAS
006200     10 FOREIGN-ADDR-IND-C   PIC X(1).                            CPWSRBAS
006300     10 FUTURE-INST          PIC X(3).                            CPWSRBAS
006400     10 FUTURE-INST-C        PIC X(1).                            CPWSRBAS
006500     10 HANDICAP-STAT        PIC X(1).                            CPWSRBAS
006600     10 HANDICAP-STAT-C      PIC X(1).                            CPWSRBAS
006700     10 HIGH-DEGREE          PIC X(4).                            CPWSRBAS
006800     10 HIGH-DEGREE-C        PIC X(1).                            CPWSRBAS
006900     10 HIRE-DATE            PIC X(10).                           CPWSRBAS
007000     10 HIRE-DATE-C          PIC X(1).                            CPWSRBAS
007100     10 HOME-DEPT            PIC X(6).                            CPWSRBAS
007200     10 HOME-DEPT-C          PIC X(1).                            CPWSRBAS
007300     10 NEXT-SALARY-REV      PIC X(1).                            CPWSRBAS
007400     10 NEXT-SALARY-REV-C    PIC X(1).                            CPWSRBAS
007500     10 NEXT-SALREV-DATE     PIC X(10).                           CPWSRBAS
007600     10 NEXT-SALREV-DATE-C   PIC X(1).                            CPWSRBAS
007700     10 OATH-SIGN-DATE       PIC X(10).                           CPWSRBAS
007800     10 OATH-SIGN-DATE-C     PIC X(1).                            CPWSRBAS
007900     10 PERM-ADDR-1          PIC X(30).                           CPWSRBAS
008000     10 PERM-ADDR-1-C        PIC X(1).                            CPWSRBAS
008100     10 PERM-ADDR-2          PIC X(30).                           CPWSRBAS
008200     10 PERM-ADDR-2-C        PIC X(1).                            CPWSRBAS
008300     10 PERM-ADDR-CITY       PIC X(21).                           CPWSRBAS
008400     10 PERM-ADDR-CITY-C     PIC X(1).                            CPWSRBAS
008500     10 PERM-ADDR-ST         PIC X(2).                            CPWSRBAS
008600     10 PERM-ADDR-ST-C       PIC X(1).                            CPWSRBAS
008700     10 PERM-ADDR-ZIP        PIC X(5).                            CPWSRBAS
008800     10 PERM-ADDR-ZIP-C      PIC X(1).                            CPWSRBAS
008900     10 PRIOR-SERVICE-CODE   PIC X(1).                            CPWSRBAS
009000     10 PRIOR-SERVICE-C      PIC X(1).                            CPWSRBAS
009100     10 PRIOR-SERV-INST      PIC X(3).                            CPWSRBAS
009200     10 PRIOR-SERV-INST-C    PIC X(1).                            CPWSRBAS
009300     10 PROB-END-DATE        PIC X(10).                           CPWSRBAS
009400     10 PROB-END-DATE-C      PIC X(1).                            CPWSRBAS
009500     10 PROVINCE-CODE        PIC X(15).                           CPWSRBAS
009600     10 PROVINCE-CODE-C      PIC X(1).                            CPWSRBAS
009700     10 RET-SYS-CODE         PIC X(1).                            CPWSRBAS
009800     10 RET-SYS-CODE-C       PIC X(1).                            CPWSRBAS
009900     10 SEPARATE-DATE        PIC X(10).                           CPWSRBAS
010000     10 SEPARATE-DATE-C      PIC X(1).                            CPWSRBAS
010100     10 SEPARATE-DESTIN      PIC X(1).                            CPWSRBAS
010200     10 SEPARATE-DESTIN-C    PIC X(1).                            CPWSRBAS
010300     10 SEPARATE-REASON      PIC X(2).                            CPWSRBAS
010400     10 SEPARATE-REASON-C    PIC X(1).                            CPWSRBAS
010500     10 SEXCODE              PIC X(1).                            CPWSRBAS
010600     10 SEXCODE-C            PIC X(1).                            CPWSRBAS
010700     10 STUDENT-STATUS       PIC X(1).                            CPWSRBAS
010800     10 STUDENT-STATUS-C     PIC X(1).                            CPWSRBAS
010900     10 VET-DISAB-STAT       PIC X(1).                            CPWSRBAS
011000     10 VET-DISAB-STAT-C     PIC X(1).                            CPWSRBAS
011100     10 VET-STATUS           PIC X(1).                            CPWSRBAS
011200     10 VET-STATUS-C         PIC X(1).                            CPWSRBAS
011300     10 VISA-END-DATE        PIC X(10).                           CPWSRBAS
011400     10 VISA-END-DATE-C      PIC X(1).                            CPWSRBAS
011500     10 VISA-TYPE            PIC X(2).                            CPWSRBAS
011600     10 VISA-TYPE-C          PIC X(1).                            CPWSRBAS
011700     10 LAST-DAY-ON-PAY      PIC X(10).                           CPWSRBAS
011800     10 LAST-DAY-ON-PAY-C    PIC X(1).                            CPWSRBAS
011900     10 PRIMARY-TITLE        PIC X(4).                            CPWSRBAS
012000     10 PRIMARY-TITLE-C      PIC X(1).                            CPWSRBAS
012100     10 JOB-GROUP-ID         PIC X(3).                            CPWSRBAS
012200     10 JOB-GROUP-ID-C       PIC X(1).                            CPWSRBAS
012210     10 ALT-DEPT-CD          PIC X(6).                            36450793
012220     10 ALT-DEPT-CD-C        PIC X(1).                            36450793
012221***** THE FIELDS ON THE EDB CORRESPONDING TO THE FOLLOWING TRIP   25981207
012222***** FIELDS ARE OBSOLETE.                                        25981207
012230     10 TRIP-PERCENT         PIC S9(4) USAGE COMP.                19530846
012240     10 TRIP-PERCENT-C       PIC X(1).                            19530846
012250     10 TRIP-DURATION        PIC S9(4) USAGE COMP.                19530846
012260     10 TRIP-DURATION-C      PIC X(1).                            19530846
012270     10 TRIP-BEGIN-DATE      PIC X(10).                           19530846
012280     10 TRIP-BEGIN-DATE-C    PIC X(1).                            19530846
012281***** END TRIP FIELDS.                                            25981207
012290     10 RET-FICA-DERIVE      PIC X(1).                            42601109
012291     10 RET-FICA-DERIVE-C    PIC X(1).                            42601109
012292     10 US-DATE-OF-ENTRY     PIC X(10).                           32171197
012293     10 US-DATE-OF-ENTRY-C   PIC X(1).                            32171197
012294     10 WCE-VET-STATUS       PIC X(1).                            51231302
012295     10 WCE-VET-STATUS-C     PIC X(1).                            51231302
012299     10 RECENT-VET-SEPDT     PIC X(10).                           02561450
012300     10 RECENT-VET-SEPDT-C   PIC X(1).                            02561450
012413     10 START-PERCENT        PIC S9(4) USAGE COMP.                03861482
012414     10 START-PERCENT-C      PIC X(1).                            03861482
012417     10 START-BEGIN-DATE     PIC X(10).                           03861482
012418     10 START-BEGIN-DATE-C   PIC X(1).                            03861482
012419     10 START-END-DATE       PIC X(10).                           03861482
012420     10 START-END-DATE-C     PIC X(1).                            03861482
012421*****                                                          CD 03861482
012422*****10 RBAS-FILLER          PIC X(565).                          03861482
012423     10 RBAS-FILLER          PIC X(540).                          03861482
012300******************************************************************CPWSRBAS
012430* THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS NOW 126 *03861482
012500******************************************************************CPWSRBAS
