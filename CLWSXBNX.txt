000100*==========================================================%      PXX00505
000200*=    COPY MEMBER: CLWSXBNX                               =%      PXX00505
000300*=    CHANGE # PXX00505     PROJ. REQUEST: CHANGE DB/PAN  =%      PXX00505
000400*=    NAME: JIM PIERCE      MODIFICATION DATE: 07/31/95   =%      PXX00505
000500*=                                                        =%      PXX00505
000600*=    DESCRIPTION:                                        =%      PXX00505
000700*=     ADD ALT-DEPT-CODE, ALT-DEPT-2, ALT-DEPT-3 TO XBNX  =%      PXX00505
000800*==========================================================%      PXX00505
000100*==========================================================%      CLWSXBNX
000200*=    COPY MEMBER: CLWSXBNX                               =%      CLWSXBNX
000300*=    CHANGE # DU059        PROJ. REQUEST: CHANGE DB/PAN  =%      CLWSXBNX
000400*=    NAME:JIM PIERCE       CREATION DATE: 11/06/94       =%      CLWSXBNX
000500*=                                                        =%      CLWSXBNX
000600*=    DESCRIPTION: CHANGE DATABASE BENEFITS EXTRACT FILE  =%      CLWSXBNX
000700*==========================================================%      CLWSXBNX
000800*    COPYID=CLWSXBNX                                              CLWSXBNX
001700*01  XBNX-BEN-EXTRACT-RECORD     SIZE 366.                        CLWSXBNX
001000   05  XBNX-EMPLOYEE-ID                    PIC  X(9).             CLWSXBNX
001100   05  XBNX-BEN-TABLE-DATA.                                       CLWSXBNX
001200     10  XBNX-DENTAL-PLAN                  PIC  X(2).             CLWSXBNX
001300     10  XBNX-DENTAL-COVERAGE              PIC  X(3).             CLWSXBNX
001400     10  XBNX-DENTAL-DEENROLL              PIC  X(1).             CLWSXBNX
001500     10  XBNX-DENTAL-OPTOUT                PIC  X(1).             CLWSXBNX
001600     10  XBNX-DENTAL-COVEFFDATE-X.                                CLWSXBNX
001700       15  XBNX-DENTAL-CENTURY             PIC  9(2).             CLWSXBNX
001800       15  XBNX-DENTAL-COVEFFDATE.                                CLWSXBNX
001900         20  XBNX-DENTAL-YEAR              PIC  9(2).             CLWSXBNX
002000         20  XBNX-DENTAL-MONTH             PIC  9(2).             CLWSXBNX
002100         20  XBNX-DENTAL-DAY               PIC  9(2).             CLWSXBNX
002200     10  XBNX-DENTAL-EMPLOYEE-DEDUCT       PIC S9(5)V9(2).        CLWSXBNX
002300     10  XBNX-DENTAL-EMPLOYER-CONTRIB      PIC S9(5)V9(2).        CLWSXBNX
002400     10  XBNX-LIFEINS-PLAN                 PIC  X(1).             CLWSXBNX
002500     10  XBNX-LIFE-SALARY-BASE             PIC S9(4).             CLWSXBNX
002600     10  XBNX-LIFE-UCPAIDAMT               PIC  X(3).             CLWSXBNX
002700     10  XBNX-LIFE-DEENROLL                PIC  X(1).             CLWSXBNX
002800     10  XBNX-LIFE-UCPD-EFFDATE-X.                                CLWSXBNX
002900       15  XBNX-LIFE-UCPD-CENTURY          PIC  9(2).             CLWSXBNX
003000       15  XBNX-LIFE-UCPD-EFFDATE.                                CLWSXBNX
003100         20  XBNX-LIFE-UCPD-YEAR           PIC  9(2).             CLWSXBNX
003200         20  XBNX-LIFE-UCPD-MONTH          PIC  9(2).             CLWSXBNX
003300         20  XBNX-LIFE-UCPD-DAY            PIC  9(2).             CLWSXBNX
003400     10  XBNX-LIFE-COVEFFDATE-X.                                  CLWSXBNX
003500       15  XBNX-LIFE-CENTURY               PIC  9(2).             CLWSXBNX
003600       15  XBNX-LIFE-COVEFFDATE.                                  CLWSXBNX
003700         20  XBNX-LIFE-YEAR                PIC  9(2).             CLWSXBNX
003800         20  XBNX-LIFE-MONTH               PIC  9(2).             CLWSXBNX
003900         20  XBNX-LIFE-DAY                 PIC  9(2).             CLWSXBNX
004000     10  XBNX-LIFE-EMPLOYEE-DEDUCT         PIC S9(3)V9(2).        CLWSXBNX
004100     10  XBNX-DEP-LIFE-IND                 PIC  X(1).             CLWSXBNX
004200     10  XBNX-DEP-LIFE-DEENROLL            PIC  X(1).             CLWSXBNX
004300     10  XBNX-DEP-LIFE-COVEFFDATE-X.                              CLWSXBNX
004400       15  XBNX-DEP-LIFE-CENTURY           PIC  9(2).             CLWSXBNX
004500       15  XBNX-DEP-LIFE-COVEFFDATE.                              CLWSXBNX
004600         20  XBNX-DEP-LIFE-YEAR            PIC  9(2).             CLWSXBNX
004700         20  XBNX-DEP-LIFE-MONTH           PIC  9(2).             CLWSXBNX
004800         20  XBNX-DEP-LIFE-DAY             PIC  9(2).             CLWSXBNX
004900     10  XBNX-DEP-LIFE-EMPLOYEE-DEDUCT     PIC S9(3)V9(2).        CLWSXBNX
005000     10  XBNX-ADD-PRINC-SUM                PIC  X(3).             CLWSXBNX
005100     10  XBNX-ADD-COVERAGE                 PIC  X(1).             CLWSXBNX
005200     10  XBNX-ADD-DEENROLL                 PIC  X(1).             CLWSXBNX
005300     10  XBNX-ADD-COVEFFDATE-X.                                   CLWSXBNX
005400       15  XBNX-ADD-CENTURY                PIC  9(2).             CLWSXBNX
005500       15  XBNX-ADD-COVEFFDATE.                                   CLWSXBNX
005600         20  XBNX-ADD-YEAR                 PIC  9(2).             CLWSXBNX
005700         20  XBNX-ADD-MONTH                PIC  9(2).             CLWSXBNX
005800         20  XBNX-ADD-DAY                  PIC  9(2).             CLWSXBNX
005900     10  XBNX-ADD-EMPLOYEE-DEDUCT          PIC S9(5)V9(2).        CLWSXBNX
006000     10  XBNX-ADD-EMPLOYER-CONTRIB         PIC S9(5)V9(2).        CLWSXBNX
006100     10  XBNX-HLTH-PLAN                    PIC  X(2).             CLWSXBNX
006200     10  XBNX-HLTH-COVERCODE               PIC  X(3).             CLWSXBNX
006300     10  XBNX-HLTH-DEENROLL                PIC  X(1).             CLWSXBNX
006400     10  XBNX-HLTH-OPTOUT                  PIC  X(1).             CLWSXBNX
006500     10  XBNX-HLTH-COVEFFDATE-X.                                  CLWSXBNX
006600       15  XBNX-HLTH-CENTURY               PIC  9(2).             CLWSXBNX
006700       15  XBNX-HLTH-COVEFFDATE.                                  CLWSXBNX
006800         20  XBNX-HLTH-YEAR                PIC  9(2).             CLWSXBNX
006900         20  XBNX-HLTH-MONTH               PIC  9(2).             CLWSXBNX
007000         20  XBNX-HLTH-DAY                 PIC  9(2).             CLWSXBNX
007100     10  XBNX-HLTH-EMPLOYEE-DEDUCT         PIC S9(5)V9(2).        CLWSXBNX
007200     10  XBNX-HLTH-EMPLOYER-CONTRIB        PIC S9(5)V9(2).        CLWSXBNX
007300     10  XBNX-VIS-PLAN                     PIC  X(2).             CLWSXBNX
007400     10  XBNX-VIS-COVERAGE                 PIC  X(3).             CLWSXBNX
007500     10  XBNX-VIS-DEENROLL                 PIC  X(1).             CLWSXBNX
007600     10  XBNX-VIS-OPTOUT                   PIC  X(1).             CLWSXBNX
007700     10  XBNX-VIS-COVEFFDATE-X.                                   CLWSXBNX
007800       15  XBNX-VIS-CENTURY                PIC  9(2).             CLWSXBNX
007900       15  XBNX-VIS-COVEFFDATE.                                   CLWSXBNX
008000         20  XBNX-VIS-YEAR                 PIC  9(2).             CLWSXBNX
008100         20  XBNX-VIS-MONTH                PIC  9(2).             CLWSXBNX
008200         20  XBNX-VIS-DAY                  PIC  9(2).             CLWSXBNX
008300     10  XBNX-VIS-EMPLOYEE-DEDUCT          PIC S9(5)V9(2).        CLWSXBNX
008400     10  XBNX-VIS-EMPLOYER-CONTRIB         PIC S9(5)V9(2).        CLWSXBNX
008500     10  XBNX-LEGAL-PLAN                   PIC  X(2).             CLWSXBNX
008600     10  XBNX-LEGAL-COVERAGE               PIC  X(3).             CLWSXBNX
008700     10  XBNX-LEGAL-DEENROLL               PIC  X(1).             CLWSXBNX
008800     10  XBNX-LEGAL-COVEFFDATE-X.                                 CLWSXBNX
008900       15  XBNX-LEGAL-CENTURY              PIC  9(2).             CLWSXBNX
009000       15  XBNX-LEGAL-COVEFFDATE.                                 CLWSXBNX
009100         20  XBNX-LEGAL-YEAR               PIC  9(2).             CLWSXBNX
009200         20  XBNX-LEGAL-MONTH              PIC  9(2).             CLWSXBNX
009300         20  XBNX-LEGAL-DAY                PIC  9(2).             CLWSXBNX
009400     10  XBNX-LEGAL-EMPLOYEE-DEDUCT        PIC S9(3)V9(2).        CLWSXBNX
009500     10  XBNX-LEGAL-EMPLOYER-CONTRIB       PIC S9(3)V9(2).        CLWSXBNX
009600     10  XBNX-INS-REDUCT-IND               PIC  X(1).             CLWSXBNX
009700     10  XBNX-NDI-CODE                     PIC  X(1).             CLWSXBNX
009800     10  XBNX-NDI-COVEFFDATE-X.                                   CLWSXBNX
009900       15  XBNX-NDI-CENTURY                PIC  9(2).             CLWSXBNX
010000       15  XBNX-NDI-COVEFFDATE.                                   CLWSXBNX
010100         20  XBNX-NDI-YEAR                 PIC  9(2).             CLWSXBNX
010200         20  XBNX-NDI-MONTH                PIC  9(2).             CLWSXBNX
010300         20  XBNX-NDI-DAY                  PIC  9(2).             CLWSXBNX
010400     10  XBNX-EPD-SALARY-BASE              PIC S9(5)V.            CLWSXBNX
010500     10  XBNX-EPD-WAIT-PERIOD              PIC  X(3).             CLWSXBNX
010600     10  XBNX-EPD-DEENROLL                 PIC  X(1).             CLWSXBNX
010700     10  XBNX-EPD-COVRGE-DATE-X.                                  CLWSXBNX
010800       15  XBNX-EPD-CENTURY                PIC  9(2).             CLWSXBNX
010900       15  XBNX-EPD-COVRGE-DATE.                                  CLWSXBNX
011000         20  XBNX-EPD-YEAR                 PIC  9(2).             CLWSXBNX
011100         20  XBNX-EPD-MONTH                PIC  9(2).             CLWSXBNX
011200         20  XBNX-EPD-DAY                  PIC  9(2).             CLWSXBNX
011300     10  XBNX-EPD-EMPLOYEE-DEDUCT          PIC S9(5)V9(2).        CLWSXBNX
011400     10  XBNX-DCP-PLAN-CODE                PIC  X(1).             CLWSXBNX
011500     10  XBNX-COV-COMP-LIMIT-CD            PIC  X(01).            CLWSXBNX
011600   05  XBNX-PAY-TABLE-DATA.                                       CLWSXBNX
011700     10  XBNX-COUNTRY-ORIGIN               PIC  X(2).             CLWSXBNX
011800     10  XBNX-FEDTAX-MARITAL               PIC  X(1).             CLWSXBNX
011900     10  XBNX-FEDTAX-EXEMPT                PIC S9(4).             CLWSXBNX
012000     10  XBNX-MAXFED-EXEMPT                PIC S9(4).             CLWSXBNX
012100     10  XBNX-STATE-TAX-MARITAL            PIC  X(1).             CLWSXBNX
012200     10  XBNX-STATE-TAX-PERDED             PIC S9(4).             CLWSXBNX
012300     10  XBNX-STATE-TAX-ITMDED             PIC S9(4).             CLWSXBNX
012400     10  XBNX-STATE-TAX-MAXEXMPT           PIC S9(4).             CLWSXBNX
012500     10  XBNX-TT-INCOME-CODE               PIC  X(2).             CLWSXBNX
012600     10  XBNX-ALT-TT-CODE                  PIC  X(2).             CLWSXBNX
012700     10  XBNX-TT-END-DATE-X.                                      CLWSXBNX
012800       15  XBNX-TT-END-CENTURY             PIC  9(2).             CLWSXBNX
012900       15  XBNX-TT-END-DATE.                                      CLWSXBNX
013000         20  XBNX-TT-END-YEAR              PIC  9(2).             CLWSXBNX
013100         20  XBNX-TT-END-MONTH             PIC  9(2).             CLWSXBNX
013200         20  XBNX-TT-END-DAY               PIC  9(2).             CLWSXBNX
013300     10  XBNX-TT-ARTICLE-NO                PIC  X(3).             CLWSXBNX
013400     10  XBNX-TT-DOLLAR-LIMIT              PIC S9(5)V9(2).        CLWSXBNX
013500     10  XBNX-I-9-DATE-X.                                         CLWSXBNX
013600       15  XBNX-I-9-CENTURY                PIC  9(2).             CLWSXBNX
013700       15  XBNX-I-9-DATE.                                         CLWSXBNX
013800         20  XBNX-I-9-YEAR                 PIC  9(2).             CLWSXBNX
013900         20  XBNX-I-9-MONTH                PIC  9(2).             CLWSXBNX
014000         20  XBNX-I-9-DAY                  PIC  9(2).             CLWSXBNX
014100   05  XBNX-PCM-TABLE-DATA.                                       CLWSXBNX
014200     10  XBNX-YTD-TOTAL-GROSS              PIC S9(7)V9(2).        CLWSXBNX
014300     10  XBNX-YTD-FWT-GROSS                PIC S9(7)V9(2).        CLWSXBNX
014400     10  XBNX-LAST-PAY-DATE-X.                                    CLWSXBNX
014500       15  XBNX-LAST-PAY-CENTURY           PIC  9(2).             CLWSXBNX
014600       15  XBNX-LAST-PAY-DATE.                                    CLWSXBNX
014700         20  XBNX-LAST-PAY-YEAR            PIC  9(2).             CLWSXBNX
014800         20  XBNX-LAST-PAY-MONTH           PIC  9(2).             CLWSXBNX
014900         20  XBNX-LAST-PAY-DAY             PIC  9(2).             CLWSXBNX
015000   05  XBNX-PER-TABLE-DATA.                                       CLWSXBNX
015100     10  XBNX-VISA-END-DATE-X.                                    CLWSXBNX
015200       15  XBNX-VISA-END-CENTURY           PIC  9(2).             CLWSXBNX
015300       15  XBNX-VISA-END-DATE.                                    CLWSXBNX
015400         20  XBNX-VISA-END-YEAR            PIC  9(2).             CLWSXBNX
015500         20  XBNX-VISA-END-MONTH           PIC  9(2).             CLWSXBNX
015600         20  XBNX-VISA-END-DAY             PIC  9(2).             CLWSXBNX
015700     10  XBNX-OATH-SIGN-DATE-X.                                   CLWSXBNX
015800       15  XBNX-OATH-SIGN-CENTURY          PIC  9(2).             CLWSXBNX
015900       15  XBNX-OATH-SIGN-DATE.                                   CLWSXBNX
016000         20  XBNX-OATH-SIGN-YEAR           PIC  9(2).             CLWSXBNX
016100         20  XBNX-OATH-SIGN-MONTH          PIC  9(2).             CLWSXBNX
016200         20  XBNX-OATH-SIGN-DAY            PIC  9(2).             CLWSXBNX
016300     10  XBNX-TIMEKEEP-DEPT                PIC  X(6).             CLWSXBNX
016400     10  XBNX-TIMEKEEP-SUB                 PIC  X(2).             CLWSXBNX
016500     10  XBNX-EMP-CHANGED-AT-DATE-X.                              CLWSXBNX
016600       15  XBNX-EMP-CHANGED-AT-CENTURY     PIC  9(2).             CLWSXBNX
016700       15  XBNX-EMP-CHANGED-AT-DATE.                              CLWSXBNX
016800         20  XBNX-EMP-CHANGED-AT-YEAR      PIC  9(2).             CLWSXBNX
016900         20  XBNX-EMP-CHANGED-AT-MONTH     PIC  9(2).             CLWSXBNX
017000         20  XBNX-EMP-CHANGED-AT-DAY       PIC  9(2).             CLWSXBNX
017100     10  XBNX-EMP-CHANGED-BY               PIC  X(08).            CLWSXBNX
018000     10  XBNX-ALT-DEPT-CD                  PIC  X(06).            PXX00505
018100     10  XBNX-ALT-DEPT-2-CD                PIC  X(06).            PXX00505
018200     10  XBNX-ALT-DEPT-3-CD                PIC  X(06).            PXX00505
