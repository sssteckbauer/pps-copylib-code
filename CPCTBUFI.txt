000100**************************************************************/   01171422
000200*  COPYMEMBER: CPCTBUFI                                      */   01171422
000300*  RELEASE: ___1422______ SERVICE REQUEST(S): ____80117____  */   01171422
000310*                         SERVICE REQUEST(S): ____14870____  */   01171422
000400*  NAME:_____J.WILCOX____ MODIFICATION DATE:  ___07/23/02__  */   01171422
000500*  DESCRIPTION:                                              */   01171422
000600*  - Added field BUFI-SSN-MASK-LNGTH to control masking of   */   01171422
000700*    Social Security Numbers for Agency Fee/Charity reporting*/   01171422
000710*  - Added field BUFI-PER-ITEM-CHRG to indicate whether a    */   01171422
000720*    per-item charge should be assessed for Dues, Agency Fee,*/   01171422
000730*    both, or neither.                                       */   01171422
000800**************************************************************/   01171422
001800**************************************************************/   30871405
001900*  COPYMEMBER: CPCTBUFI                                      */   30871405
002000*  RELEASE: ___1405______ SERVICE REQUEST(S): _____3087____  */   30871405
002100*  NAME:_____SRS_________ CREATION DATE:      ___04/16/02__  */   30871405
002200*  DESCRIPTION:                                              */   30871405
002300*  - NEW COPY MEMBER FOR BARGAINING UNIT BUF TABLE INPUT     */   30871405
002400**************************************************************/   30871405
002500*    COPYID=CPCTBUFI                                              CPCTBUFI
002600*01  BARG-UNIT-BUF-TABLE-INPUT.                                   CPCTBUFI
002700     05 BUFI-BUC                         PIC XX.                  CPCTBUFI
002800     05 BUFI-REP                         PIC X.                   CPCTBUFI
002900     05 BUFI-SHC                         PIC X.                   CPCTBUFI
003000     05 BUFI-DISTRIBUTION                PIC X.                   CPCTBUFI
003100     05 BUFI-UDUE-GTN                    PIC XXX.                 CPCTBUFI
003200     05 BUFI-AGENCY-FEE-GTN              PIC XXX.                 CPCTBUFI
003300     05 BUFI-AGENCY-FEE-VAL.                                      CPCTBUFI
003400        10 BUFI-AGENCY-FEE-VAL-N         PIC 9(7)V99.             CPCTBUFI
003500     05 BUFI-CHARITY-GTN1                PIC XXX.                 CPCTBUFI
003600     05 BUFI-CHARITY-GTN2                PIC XXX.                 CPCTBUFI
003700     05 BUFI-CHARITY-GTN3                PIC XXX.                 CPCTBUFI
003800     05 BUFI-UNIT-LINK                   PIC X.                   CPCTBUFI
003900     05 BUFI-UDUE-BASE                   PIC XX.                  CPCTBUFI
004000     05 BUFI-AF-BASE                     PIC XX.                  CPCTBUFI
004100     05 BUFI-BUC-ORGNZ-LINK              PIC X.                   CPCTBUFI
004200     05 BUFI-PER-ITEM-CHRG               PIC X.                   01171422
004300     05 BUFI-SSN-MASK-LNGTH.                                      01171422
004400        10 BUFI-SSN-MASK-LNGTH-N         PIC 9.                   01171422
