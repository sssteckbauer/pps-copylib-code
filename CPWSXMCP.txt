000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXMCP                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/22/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000010**************************************************************/   13250191
000020*  COPYID:   CPWSXMCP                                        */   13250191
000030*  RELEASE # __0191____   SERVICE REQUEST NO(S)___1325_______*/   13250191
000040*  NAME ___SLB_________   MODIFICATION DATE ____01/27/86_____*/   13250191
000050*  DESCRIPTION                                               */   13250191
000060*  RECORD LAYOUT FOR PPP711, PPP712 AND PPP713 (FCP INTER-   */   13250191
000070*  FACE PROGRAMS) STANDARD MESSAGE LINE.                     */   13250191
000080**************************************************************/   13250191
000090*                                                                 13250191
000100*    COPYID=CPWSXMCP                                              CPWSXMCP
000200*                                                                 CPWSXMCP
000300*01  XMCP-MESSAGE-LINE1 REDEFINES PRT-LINE.                       30930413
000400     05  FILLER                      PIC X(01).                   CPWSXMCP
000500     05  FILLER                      PIC X(02).                   CPWSXMCP
000600     05  XMCP-EMPLOYEE-ID            PIC X(09).                   CPWSXMCP
000700     05  FILLER                      PIC X(02).                   CPWSXMCP
000800     05  XMCP-MESSAGE-NO.                                         CPWSXMCP
000900         10  XMCP-MSG-PROG           PIC X(02).                   CPWSXMCP
001000         10  XMCP-MSG-DASH           PIC X(01).                   CPWSXMCP
001010         10  XMCP-MSG-NO             PIC X(03).                   CPWSXMCP
001020     05  FILLER                      PIC X(03).                   CPWSXMCP
001030     05  XMCP-MSG-SEVERITY           PIC X(13).                   CPWSXMCP
001040     05  FILLER                      PIC X(04).                   CPWSXMCP
001050     05  XMCP-MSG-TEXT               PIC X(65).                   CPWSXMCP
001060     05  FILLER                      PIC X(28).                   CPWSXMCP
001070*                                                                 CPWSXMCP
001080 01  XMCP-MESSAGE-LINE2 REDEFINES XMCP-MESSAGE-LINE1.             CPWSXMCP
001090     05  FILLER                      PIC X(01).                   CPWSXMCP
001091     05  FILLER                      PIC X(44).                   CPWSXMCP
001092     05  XMCP-REF-DATA               PIC X(88).                   CPWSXMCP
001100     SKIP1                                                        CPWSXMCP
