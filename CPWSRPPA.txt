000100**************************************************************/   25991140
000200*  COPYMEMBER: CPWSRPPA                                      */   25991140
000300*  RELEASE: ___1140______ SERVICE REQUEST(S): ____12599____  */   25991140
000400*  NAME:______M SANO_____ CREATION DATE:      ___05/19/97__  */   25991140
000500*  DESCRIPTION:                                              */   25991140
000600*    WORKING STORAGE FOR PPPPPA ROW OF THE EDB               */   25991140
000700**************************************************************/   25991140
000800*COPYID=CPWSPPA                                              */   CPWSRPPA
000900**************************************************************/   CPWSRPPA
001000*                                                                 CPWSRPPA
001100*01  PPA-ROW.                                                     CPWSRPPA
001200     10  EMPLOYEE-ID             PIC X(09).                       CPWSRPPA
001300     10  PPA-BENEFIT-TYPE        PIC X(01).                       CPWSRPPA
001400     10  PPA-PAY-CYCLE           PIC X(01).                       CPWSRPPA
001500     10  PPA-PAY-END-DATE        PIC X(10).                       CPWSRPPA
001600     10  PPA-PLAN-CODE           PIC X(02).                       CPWSRPPA
001700     10  PPA-COVERAGE            PIC X(03).                       CPWSRPPA
001800     10  PPA-FLAG                PIC X(01).                       CPWSRPPA
001900     10  PPA-ADC-CODE            PIC X(01).                       CPWSRPPA
