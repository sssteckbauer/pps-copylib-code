000100**************************************************************/   34221114
000200*  COPYMEMBER: CPFDXCHG                                      */   34221114
000300*  RELEASE: ___1114______ SERVICE REQUEST(S): _____3422____  */   34221114
000400*  NAME:______M SANO_____ MODIFICATION DATE:  ___02/21/97__  */   34221114
000500*  DESCRIPTION:                                              */   34221114
000600*     INCREASE FILE SIZE FOR 30 BYTE OCCURENCE KEY           */   34221114
000700**************************************************************/   34221114
000100**************************************************************/  *36090513
000200*  COPYMEMBER:   CPFDXCHG                                    */  *36090513
000300*  RELEASE # ___0513___   SERVICE REQUEST NO(S)____3609______*/  *36090513
000400*  NAME ______JAG______   CREATION DATE     ____11/05/90_____*/  *36090513
000500*  DESCRIPTION                                               */  *36090513
000600*     DB2 EDB CHANGE FILE.                                   */  *36090513
000700**************************************************************/  *36090513
000800*                                                                 CPFDXCHG
000900*    COPYID=CPFDXCHG                                              CPFDXCHG
001000     BLOCK CONTAINS 0 RECORDS                                     CPFDXCHG
001100     RECORDING MODE IS V                                          CPFDXCHG
001900**** RECORD CONTAINS 68  TO   158 CHARACTERS                      34221114
002000     RECORD CONTAINS 80  TO   170 CHARACTERS                      34221114
001300     LABEL RECORDS ARE STANDARD                                   CPFDXCHG
001400     DATA RECORD IS EDB-CHANGE-RECORD.                            CPFDXCHG
