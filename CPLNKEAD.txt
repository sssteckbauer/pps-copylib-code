000100******************************************************************48381302
000200*  COPYMEMBER: CPLNKEAD                                          *48381302
000300*  RELEASE: ____1302____  SERVICE REQUEST(S): ____14838___       *48381302
000400*  NAME:______RENNER____  CREATION DATE:      __07/11/00__       *48381302
000500*  DESCRIPTION:                                                  *48381302
000600*    LINKAGE FOR PPEADUTL SUBROUTINE CREATED FOR DB2 EDB         *48381302
000700*    CONVERSION.                                                 *48381302
000900******************************************************************CPLNKEAD
001000*    COPYID=CPLNKEAD                                              CPLNKEAD
001100*01  PPEADUTL-INTERFACE.                                          CPLNKEAD
001200*                                                                *CPLNKEAD
001300******************************************************************CPLNKEAD
001400*    P P E A D U T L   I N T E R F A C E                         *CPLNKEAD
001500******************************************************************CPLNKEAD
001600*                                                                *CPLNKEAD
001700     05  PPEADUTL-ERROR-SW       PICTURE X(1).                    CPLNKEAD
001800         88  PPEADUTL-ERROR VALUE 'Y'.                            CPLNKEAD
001900     05  PPEADUTL-EAD-FOUND-SW   PICTURE X(1).                    CPLNKEAD
002000         88  PPEADUTL-EAD-FOUND VALUE 'Y'.                        CPLNKEAD
002100     05  PPEADUTL-EMPLOYEE-ID   PICTURE X(9).                     CPLNKEAD
