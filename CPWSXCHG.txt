000100**************************************************************/   34221114
000200*  COPYMEMBER: CPWSXCHG                                      */   34221114
000300*  RELEASE: ___1114______ SERVICE REQUEST(S): _____3422____  */   34221114
000400*  NAME:______M SANO_____ MODIFICATION DATE:  ___01/16/97__  */   34221114
000500*  DESCRIPTION:                                              */   34221114
000600*  - ADDED 12 BYTES TO OCCURENCE KEY                         */   34221114
000700**************************************************************/   34221114
000100**************************************************************/   36060628
000200*  COPYMEMBER: CPWSXCHG                                      */   36060628
000300*  RELEASE # ____0628____ SERVICE REQUEST NO(S)___3606_______*/   36060628
000400*  NAME ___G. STEINITZ___ MODIFICATION DATE ____01/10/92_____*/   36060628
000500*  DESCRIPTION                                               */   36060628
000600*   - MADE SLACK BYTE EXPLICIT TO AVOID HIDDEN PITFALLS      */   36060628
000700*   - REMOVED UNNECCESSARY FILLER LEVEL                      */   36060628
000100**************************************************************/  *36090513
000200*  COPYMEMBER: CPWSXCHG                                      */  *36090513
000300*  RELEASE # ____0513____ SERVICE REQUEST NO(S)___3609_______*/  *36090513
000400*  NAME ______JAG______   MODIFICATION DATE ____11/05/90_____*/  *36090513
000500*  DESCRIPTION                                               */  *36090513
000600*   - NEW EMPLOYEE CHANGE RECORD FOR DB2 EDB UPDATE.         */  *36090513
000700**************************************************************/  *36090513
000800******************************************************************CPWSXCHG
000900*                                                                *CPWSXCHG
001000*          E M P L O Y E E   C H A N G E   R E C O R D           *CPWSXCHG
001100*                                                                *CPWSXCHG
001200******************************************************************CPWSXCHG
001300*                                                                *CPWSXCHG
001400* THE EMPLOYEE CHANGE RECORD IDENTIFIES INDIVIDUAL DATA ELEMENT  *CPWSXCHG
001500* VALUE CHANGES MADE TO THE EMPLOYEE RECORD. THE KEY TO THIS     *CPWSXCHG
001600* RECORD IS THE EMPLOYEE ID, DATA ELEMENT NUMBER, AND OCCURRENCE *CPWSXCHG
001700* KEY (FOR THOSE DATA ELEMENTS WITHIN A MULTIPLE OCCURRENCE      *CPWSXCHG
001800* STRUCTURE).                                                    *CPWSXCHG
001900*                                                                *CPWSXCHG
002000******************************************************************CPWSXCHG
002100*    COPYID=CPWSXCHG                                              CPWSXCHG
002900******************************************************************36060628
002200*01  EDB-CHANGE-RECORD.                                           CPWSXCHG
002300     05  XCHG-FIXED-AREA.                                         CPWSXCHG
002400         10  XCHG-EMPLOYEE-ID            PIC X(09).               CPWSXCHG
002500         10  XCHG-DATA-ELEM-NO           PIC X(04).               CPWSXCHG
004100****     10  XCHG-OCCURENCE-KEY          PIC X(18).               34221114
004200         10  XCHG-OCCURENCE-KEY          PIC X(30).               34221114
002700         10  XCHG-PROCESS-ID             PIC X(08).               CPWSXCHG
002800         10  XCHG-CHANGE-SOURCE          PIC X(08).               CPWSXCHG
002900         10  XCHG-TIMESTAMP              PIC X(14).               CPWSXCHG
003800         10  XCHG-SLACK-BYTE             PIC X(01).               36060628
003000         10  XCHG-ODO-LENGTHS.                                    CPWSXCHG
003100             15  XCHG-OLD-VAL-LNGTH      PIC S9(04) COMP SYNC.    CPWSXCHG
003200             15  XCHG-NEW-VAL1-LNGTH     PIC S9(04) COMP SYNC.    CPWSXCHG
003300             15  XCHG-NEW-VAL2-LNGTH     PIC S9(04) COMP SYNC.    CPWSXCHG
003400     05  XCHG-OLD-VALUE.                                          CPWSXCHG
003500         10  XCHG-OLD-VAL                                         CPWSXCHG
004500                                         PIC  X(01)               36060628
003600                 OCCURS 0 TO 30 TIMES                             CPWSXCHG
003700                 DEPENDING ON XCHG-OLD-VAL-LNGTH.                 CPWSXCHG
005600****                                                           CD 34221114
003900     05  XCHG-NEW-VALUE1.                                         CPWSXCHG
004000         10  XCHG-NEW-VAL1                                        CPWSXCHG
005100                                         PIC  X(01)               36060628
004100                 OCCURS 0 TO 30 TIMES                             CPWSXCHG
004200                 DEPENDING ON XCHG-NEW-VAL1-LNGTH.                CPWSXCHG
006200****                                                           CD 34221114
004400     05  XCHG-NEW-VALUE2.                                         CPWSXCHG
004500         10  XCHG-NEW-VAL2                                        CPWSXCHG
005700                                         PIC  X(01)               36060628
004600                 OCCURS 0 TO 30 TIMES                             CPWSXCHG
004700                 DEPENDING ON XCHG-NEW-VAL2-LNGTH.                CPWSXCHG
006800****                                                           CD 34221114
006100*                                                                 36060628
006200*****   END OF CPWSXCHG  *****************************************36060628
