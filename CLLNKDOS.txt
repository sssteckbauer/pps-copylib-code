
       01  DOS-DATA.
           05  DOS-ARRLENGTH             PIC S9(4) COMP.
           05  DOS-RTNCD.
               10  DOS-RETURN-FILERR     PIC X.
               10  DOS-RETURN-ARROVER    PIC X.
               10  DOS-RETURN-STDMISS    PIC X.
               10  DOS-RETURN-EXTMISS    PIC X.
               10  DOS-RETURN-EXTDUPL    PIC X.
               10  DOS-RETURN-EDITERR    PIC X.
           05  DOS-DATATBL-GRP.
               10  DOS-DATATBL  OCCURS 300 TIMES.
                   15  DOS-DOSKEY       PIC XXX.
                   15  DOS-STDFLAG      PIC X.
                   15  DOS-EXTFLAG      PIC X.
                   15  DOS-STDATA       PIC X(224).
                   15  DOS-EXTDATA      PIC X(80).
                   15  DOS-EDITFLAG     PIC 99.
