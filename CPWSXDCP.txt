000100**************************************************************/   35472040
000200*  COPYMEMBER: CPWSXDCP                                      */   35472040
000300*  RELEASE: ___2040______ SERVICE REQUEST(S): ____83547____  */   35472040
000400*  NAME:___N_SCHUMAKER___ MODIFICATION DATE:  ___06/15/12__  */   35472040
000500*  DESCRIPTION:                                              */   35472040
000600*  - ADDED CHECK DATE (10 BYTES)                             */   35472040
000700*  - ADDED EARNINGS TRANSACTION CODE (2 BYTES)               */   35472040
000800*  - ADDED FULL FAU (30 BYTES)                               */   35472040
000900*  - ADDED AN EXTRA 8 BYTES OF FILLER TO EXISTING 14 BYTES   */   35472040
001000**************************************************************/   35472040
001100**************************************************************/   17681790
001200*  COPYMEMBER: CPWSXDCP                                      */   17681790
001300*  RELEASE: ___1790______ SERVICE REQUEST(S): ____81768____  */   17681790
001400*  NAME:______PAYPLT_____ MODIFICATION DATE:  ___09/28/07__  */   17681790
001500*  DESCRIPTION:                                              */   17681790
001600*  - ADDED DERIVED PERCENT TIME (5 BYTES)                    */   17681790
001700**************************************************************/   17681790
000000**************************************************************/   08201570
000001*  COPYMEMBER: CPWSXDCP                                      */   08201570
000002*  RELEASE: ___1570______ SERVICE REQUEST(S): ____80820____  */   08201570
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___04/13/04__  */   08201570
000004*  DESCRIPTION:                                              */   08201570
000005*  - ADDED UCRS SUBJECT GROSS INDICATOR (1 BYTE),            */   08201570
000006*    DOS PAY CATEGORY (1 BYTE),                              */   08201570
000007*    DOS TYPE HOURS (1 BYTE), AND 19 BYTES OF FILLERS FOR    */   08201570
000008*    TOTAL RECORD LENGTH OF 100.                             */   08201570
000009**************************************************************/   08201570
000000**************************************************************/   28390997
000001*  COPYMEMBER: CPWSXDCP                                      */   28390997
000002*  RELEASE: ___0997______ SERVICE REQUEST(S): ____12839____  */   28390997
000003*  NAME:___H. TRUONG  ___ MODIFICATION DATE:  ___07/06/95__  */   28390997
000004*  DESCRIPTION:                                              */   28390997
000005*                                                            */   28390997
000005*  REDEFINED XDCP-C128-UC-LOC-3 DATA FIELD TO CARRY TWO      */   28390997
000005*  VALUES, "1 AND 2".                                        */   28390997
000005*                                                            */   28390997
000006*  ADDED XDCP-SAU-CD DATA FIELD TO CARRY SIX VALUES, "0, 4,  */   28390997
000006*  6, 7, 8, AND 9".                                          */   28390997
000006*                                                            */   28390997
000007**************************************************************/   28390997
000000**************************************************************/   17530909
000001*  COPYMEMBER: CPWSXDCP                                      */   17530909
000002*  RELEASE: ___0909______ SERVICE REQUEST(S): ____11753____  */   17530909
000003*  NAME:__H. TRUONG  ____ MODIFICATION DATE:  ___06/16/94__  */   17530909
000004*  DESCRIPTION:                                              */   17530909
000005*     ADDED FINANCIAL AID TYPE CODE.                         */   17530909
000007*                                                            */   17530909
000008**************************************************************/   17530909
000100**************************************************************/   30930413
000200*  COPY MODULE:  CPWSXDCP                                    */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____05/10/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   11910211
000200*  COPYID:   CPWSXDCP                                        */   11910211
000300*  RELEASE # __0211____   SERVICE REQUEST NO(S)___1191_______*/   11910211
000400*  NAME ___SLB_________   MODIFICATION DATE ____05/01/86_____*/   11910211
000500*  DESCRIPTION                                               */   11910211
000600*                                                            */   11910211
000700*  CHANGES REFLECT THE NEED TO SUPPLY DATA FOR THE CORPORATE */   11910211
000800*  BENEFITS COUNSELING SYSTEM (BCS).  THE FOLLOWING ELEMENTS */   11910211
000900*  WERE ADDED:                                               */   11910211
001000*                - XDCP-PAY-CYCLE                            */   11910211
001100*                - XDCP-SPEC-RET-ELIG                        */   11910211
001200*                - XDCP-PPS-DOS (3-BYTE PAR DOS)             */   11910211
001300*                                                            */   11910211
001400*  THE FOLLOWING DATA NAME HAS BEEN CHANGED (FOR CLARIFICA-  */   11910211
001500*  TION):                                                    */   11910211
001600*                - FROM XDCP-C078-DOS                        */   11910211
001700*                - TO XDCP-C078-FCP-DOS (2-BYTE FCP DOS)     */   11910211
001800*                                                            */   11910211
001900**************************************************************/   11910211
002000     SKIP1                                                        11910211
002100**************************************************************/   13250191
002200*  COPYID:   CPWSXDCP                                        */   13250191
002300*  RELEASE # __0191____   SERVICE REQUEST NO(S)___1325_______*/   13250191
002400*  NAME ___SLB_________   MODIFICATION DATE ____01/27/86_____*/   13250191
002500*  DESCRIPTION                                               */   13250191
002600*  RECORD LAYOUT FOR FCP DISTRIBUTION FILE IN PPP713.        */   13250191
002700**************************************************************/   13250191
002800*                                                                 13250191
002900*    COPYID=CPWSXDCP                                              CPWSXDCP
003000*                                                                 CPWSXDCP
003100*01  XDCP-DISTRIBUTION-REC.                                       30930413
003200     05  XDCP-C021-LOCATION                      PIC X(02).       CPWSXDCP
003300     05  XDCP-C041-EMP-ID                        PIC X(09).       CPWSXDCP
003400     05  XDCP-C038-DIST-NO                       PIC X(02).       CPWSXDCP
003500     05  XDCP-C127-UC-LOC-2                      PIC X(01).       CPWSXDCP
003600         88  XDCP-C127-LOC-2-LOCAL               VALUE '1'.       CPSWXDCP
003700         88  XDCP-C127-LOC-2-UWIDE               VALUE '2'.       CPSWXDCP
003800     05  XDCP-C128-UC-LOC-3                      PIC X(01).       CPWSXDCP
003900         88  XDCP-C128-LOC-3-GENLCMP             VALUE '1'.       CPSWXDCP
004000         88  XDCP-C128-LOC-3-HLTHSCI             VALUE '2'.       CPSWXDCP
004100*****                                                          CD 08201570
004800     05  XDCP-C001-ACCOUNT                       PIC X(06).       CPWSXDCP
004900     05  XDCP-C062-FUND                          PIC X(05).       CPWSXDCP
005000     05  XDCP-C105-SUB                           PIC X(02).       CPWSXDCP
005200     05  XDCP-C078-FCP-DOS                       PIC X(02).       11910211
005300     05  XDCP-C900-PAY-PER-DATE                  PIC X(06).       CPWSXDCP
005400     05  XDCP-C107-TITLE-CODE                    PIC X(04).       CPWSXDCP
005500     05  XDCP-C918-DIST-PAY-RATE                 PIC 9(06)V9(04). CPWSXDCP
005600     05  XDCP-C930-GROSS                         PIC S9(05)V99.   CPWSXDCP
005700     05  XDCP-C931-HRS-PAID                      PIC S9(03)V99.   CPWSXDCP
005800     05  XDCP-C974-TIME-DIST                     PIC S9V9(04).    CPWSXDCP
005900     05  XDCP-C916-RATE-ADJ-IND                  PIC X(01).       CPWSXDCP
006000         88  XDCP-C916-IGNORE-TIME-HRS           VALUE 'A'.       CPSWXDCP
006100         88  XDCP-C916-NOT-IGNORE-TIME-HRS       VALUE ' '.       CPSWXDCP
006200     05  XDCP-C891-DIST-RATE-CODE                PIC X(01).       CPWSXDCP
006300         88  XDCP-C916-HOURLY-RATE               VALUE 'H'.       CPSWXDCP
006400         88  XDCP-C916-PAY-PER-AMOUNT            VALUE 'A'.       CPSWXDCP
006500     05  XDCP-C886-HEERA-DIST-UNIT-CODE          PIC X(01).       CPWSXDCP
006700     05  XDCP-PAY-CYCLE                          PIC X(02).       11910211
006800     05  XDCP-SPEC-RET-ELIG                      PIC X(01).       11910211
006900         88  XDCP-SR-NOT-ELIG                    VALUE '0'.       11910211
007000         88  XDCP-SR-ELIG-ASSOC                  VALUE '1'.       11910211
007100         88  XDCP-SR-ELIG-SAFETY                 VALUE '2'.       11910211
007200         88  XDCP-SR-ELIG-REGENTS-3              VALUE '3'.       11910211
007300         88  XDCP-SR-ELIG-REGENTS-4              VALUE '4'.       11910211
007400     05  XDCP-PPS-DOS                            PIC X(03).       11910211
007500     05  XDCP-FINAID-TYPE                        PIC X(01).       17530909
007501     05  XDCP-SAU-CD                             PIC X(01).       28390997
007502         88  XDCP-SAU-OTHER-PROG                 VALUE '0'.       28390997
007503         88  XDCP-SAU-AGRISCI                    VALUE '4'.       28390997
007504         88  XDCP-SAU-OFCPRES                    VALUE '6'.       28390997
007505         88  XDCP-SAU-REGOFFC                    VALUE '7'.       28390997
007506         88  XDCP-SAU-UNIVPRG                    VALUE '8'.       28390997
007507         88  XDCP-SAU-UNIVPRV                    VALUE '9'.       28390997
007508     05  XDCP-UCRS-GROSS-IND                     PIC S9(01).      08201570
007509     05  XDCP-DOS-PAY-CATEGORY                   PIC X(01).       08201570
007510     05  XDCP-DOS-TYPE-HOURS                     PIC X(01).       08201570
013300     05  XDCP-DERIVED-PCT                        PIC S9V9(04).    17681790
013400     05  XDCP-CHECK-DATE                         PIC X(10).       35472040
013500     05  XDCP-EARNINGS-TRANS-CD                  PIC X(02).       35472040
013600     05  XDCP-FULL-FAU                           PIC X(30).       35472040
013700     05  FILLER                                  PIC X(22).       35472040
007514*****05  FILLER                                  PIC X(14).       35472040
007515*****                                                          CD 35472040
