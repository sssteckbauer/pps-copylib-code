000100******************************************************************36060628
000200*  COPYMEMBER: CPLNKFNW                                          *36060628
000300*  RELEASE: ____0628____  SERVICE REQUEST(S): ____3606____       *36060628
000400*  NAME:__G. STEINITZ___  CREATION DATE:      __01/10/92__       *36060628
000500*                                                                *36060628
000600*  DESCRIPTION:                                                  *36060628
000700*    LINKAGE FOR PPFNWUTL SUBROUTINE CREATED FOR DB2 EDB.        *36060628
000800*                                                                *36060628
000900******************************************************************36060628
001000*    COPYID=CPLNKFNW                                              CPLNKFNW
001100******************************************************************CPLNKFNW
001200*01  PPFNWUTL-INTERFACE.                                          CPLNKFNW
001300*                                                                *CPLNKFNW
001400******************************************************************CPLNKFNW
001500*    P P F N W U T L   I N T E R F A C E                         *CPLNKFNW
001600******************************************************************CPLNKFNW
001700*                                                                *CPLNKFNW
001800     05  PPFNWUTL-ERROR-SW       PICTURE  X(01).                  CPLNKFNW
001900         88  PPFNWUTL-ERROR                          VALUE 'Y'.   CPLNKFNW
002000     05  PPFNWUTL-EMPLOYEE-ID    PICTURE  X(09).                  CPLNKFNW
002100     05  PPFNWUTL-ROW-COUNT      PICTURE S9(04)      COMP.        CPLNKFNW
002200******************************************************************CPLNKFNW
