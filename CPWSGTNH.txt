000000**************************************************************/   48881487
000001*  COPYMEMBER: CPWSGTNH                                      */   48881487
000002*  RELEASE: ___1487______ SERVICE REQUEST(S): ____14888____  */   48881487
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___04/28/03__  */   48881487
000004*  DESCRIPTION:                                              */   48881487
000005*  - ADD GTN DIRECT DEPOSIT INDICATOR                        */   48881487
000007**************************************************************/   48881487
000000**************************************************************/   48071211
000001*  COPYMEMBER: CPWSGTNH                                      */   48071211
000002*  RELEASE: ___1211______ SERVICE REQUEST(S): ____14807____  */   48071211
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___10/13/98__  */   48071211
000004*  DESCRIPTION:                                              */   48071211
000006*  - ADD LINK GTN NUMBER                                     */   48071211
000005*        INITIAL LINK GTN FLAG                               */   48071211
000007**************************************************************/   48071211
000000**************************************************************/   32031184
000001*  COPYMEMBER: CPWSGTNH                                      */   32031184
000002*  RELEASE: ___1184______ SERVICE REQUEST(S): ____13203____  */   32031184
000003*  NAME:______SRS________ MODIFICATION DATE:  ___03/09/98__  */   32031184
000004*  DESCRIPTION:                                              */   32031184
000005*  - MODIFIED TO ADD COLUMN GTN-DEPT-PAR-IND                 */   32031184
000007**************************************************************/   32031184
000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSGTNH                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:______PAYJIG_____ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*  - MODIFIED FOR FULL ACCOUNTING UNIT                       */   32021138
000007**************************************************************/   32021138
000100**************************************************************/   36410724
000200*  COPYMEMBER: CPWSGTNH                                      */   36410724
000300*  REL#: __0724_________  SERVICE REQUEST NO(S)___3641_______*/   36410724
000400*  REF REL: ___0717_____                                     */   36410724
000500*  NAME ____PXP________   MODIFICATION DATE ____12/11/92_____*/   36410724
000600*  DESCRIPTION                                               */   36410724
000700*   - ADDED CON-EDIT ROUTINE NUMBER.                         */   36410724
000800*     PER ERROR REPORT 874                                   */   36410724
000900**************************************************************/   36410724
000100**************************************************************/   36330704
000200*  COPYMEMBER: CPWSGTNH                                      */   36330704
000300*  RELEASE: ____0704____  SERVICE REQUEST(S): ____3633____   */   36330704
000400*  NAME:    ___B.I.T.___  CREATION DATE:      __10/02/92__   */   36330704
000500*  DESCRIPTION:                                              */   36330704
000600*    DCLGEN FOR THE PPPVGTNH VIEW                            */   36330704
000700*                                                            */   36330704
000800**************************************************************/   36330704
000900******************************************************************CPWSGTNH
001900* COBOL DECLARATION FOR TABLE PPPVGTNH_GTNH                      *48881487
001100******************************************************************CPWSGTNH
001200*01  DCLPPPVGTNH-GTNH.                                            CPWSGTNH
001300     10 GTN-PRIORITY         PIC X(4).                            CPWSGTNH
001400     10 GTN-DEDUCTION        PIC X(3).                            CPWSGTNH
001500     10 SYSTEM-ENTRY-DATE    PIC X(10).                           CPWSGTNH
001600     10 SYSTEM-ENTRY-TIME    PIC X(8).                            CPWSGTNH
001700     10 GTN-MATCH-ELEMENT    PIC X(3).                            CPWSGTNH
001800     10 GTN-DESCRIPTION      PIC X(15).                           CPWSGTNH
001900     10 GTN-TYPE             PIC X(1).                            CPWSGTNH
002000     10 GTN-GROUP            PIC X(1).                            CPWSGTNH
002100     10 GTN-SM-SCHED-CODE    PIC X(1).                            CPWSGTNH
002200     10 GTN-BI-SCHED-CODE    PIC X(1).                            CPWSGTNH
002300     10 GTN-MN-SCHED-CODE    PIC X(1).                            CPWSGTNH
002400     10 GTN-YTD              PIC X(1).                            CPWSGTNH
002500     10 GTN-SUSPENSE         PIC X(1).                            CPWSGTNH
002600     10 GTN-QTD              PIC X(1).                            CPWSGTNH
002700     10 GTN-DECLINING-BAL    PIC X(1).                            CPWSGTNH
002800     10 GTN-ETD              PIC X(1).                            CPWSGTNH
002900     10 GTN-FYTD             PIC X(1).                            CPWSGTNH
003000     10 GTN-USER             PIC X(1).                            CPWSGTNH
003100     10 GTN-FULL-PART        PIC X(1).                            CPWSGTNH
003200     10 GTN-PRT-YTD-ON-CK    PIC X(1).                            CPWSGTNH
003300     10 GTN-USAGE            PIC X(1).                            CPWSGTNH
003400     10 GTN-BASE             PIC X(1).                            CPWSGTNH
003500     10 GTN-REDUCE-BASE      PIC X(1).                            CPWSGTNH
003600     10 GTN-SP-CALC-NO       PIC X(2).                            CPWSGTNH
003700     10 GTN-SP-CALC-IND      PIC X(1).                            CPWSGTNH
003800     10 GTN-SP-UPDATE-NO     PIC X(2).                            CPWSGTNH
003900     10 GTN-STATUS           PIC X(1).                            CPWSGTNH
004000     10 GTN-STOP-AT-TERM     PIC X(1).                            CPWSGTNH
004100     10 GTN-VALUE-RNG-EDIT   PIC X(1).                            CPWSGTNH
004200     10 GTN-VALUE1           PIC S99999V99 USAGE COMP-3.          CPWSGTNH
004300     10 GTN-VALUE2           PIC S99999V99 USAGE COMP-3.          CPWSGTNH
004500     10 GTN-DEDUCTION-CODE   PIC X(2).                            CPWSGTNH
005500     10 GTN-LIABILITY-FAU    PIC X(30).                           32021138
005500     10 GTN-LIABILITY-OBJ    PIC X(4).                            32021138
004900     10 GTN-BEN-CODE         PIC X(1).                            CPWSGTNH
005000     10 GTN-REDUCTION-FWT    PIC X(1).                            CPWSGTNH
005100     10 GTN-REDUCTION-SWT    PIC X(1).                            CPWSGTNH
005200     10 GTN-REDUCTION-FICA   PIC X(1).                            CPWSGTNH
005300     10 GTN-CB-ELIG-IND      PIC X(1).                            CPWSGTNH
005400     10 GTN-MAX-AMOUNT       PIC S99999V99 USAGE COMP-3.          CPWSGTNH
005500     10 GTN-CB-BEHAV-CODE    PIC X(1).                            CPWSGTNH
005600     10 GTN-BENEFIT-TYPE     PIC X(1).                            CPWSGTNH
005700     10 GTN-BENEFIT-PLAN     PIC X(2).                            CPWSGTNH
005800     10 GTN-SET-INDICATOR    PIC X(1).                            CPWSGTNH
005900     10 GTN-IGTN-INDICATOR   PIC X(1).                            CPWSGTNH
006000     10 GTN-IDED-INDICATOR   PIC X(1).                            CPWSGTNH
006100     10 GTN-IRTR-INDICATOR   PIC X(1).                            CPWSGTNH
006200     10 GTN-IRET-INDICATOR   PIC X(1).                            CPWSGTNH
006700     10 GTN-OVRD-DEDUCTION   PIC X(3).                            CPWSGTNH
006800     10 GTN-ICED-INDICATOR   PIC X(1).                            CPWSGTNH
008000     10 GTN-CON-EDIT-RTN     PIC X(3).                            32021138
008000     10 GTN-RECEIVABLE-FAU   PIC X(30).                           32021138
008000     10 GTN-PREPAYMENT-FAU   PIC X(30).                           32021138
006900     10 GTN-LAST-ACTION      PIC X(1).                            CPWSGTNH
007000     10 GTN-LAST-ACTION-DT   PIC X(10).                           CPWSGTNH
008000     10 GTN-DEPT-PAR-IND     PIC X(1).                            32031184
008008     10 GTN-LINK-GTN-NUM     PIC X(3).                            48071211
008007     10 GTN-INIT-LINK-FLAG   PIC X(1).                            48071211
135231     10 GTN-DIR-DEP-IND      PIC X(1).                            48881487
135232*****                                                          CD 48881487
