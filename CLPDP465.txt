000010*==========================================================%      DS795
000020*=    COPY MEMBER: CLPDP465                               =%      DS795
000030*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000040*=                                         CONVERSION     =%      DS795
000050*=    NAME: R. SWIDERSKI    MODIFICATION DATE: 11/30/93   =%      DS795
000060*=                                                        =%      DS795
000070*=    DESCRIPTION:                                        =%      DS795
000080*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000090*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
000091*==========================================================%      DS795
000100*                                                                 CLPDP465
000200*      CONTROL REPORT PPP465CR                                    CLPDP465
000300*                                                                 CLPDP465
000400*                                                                 CLPDP465
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP465
000600                                                                  CLPDP465
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP465
000800                                                                  CLPDP465
000900     MOVE 'PPP465CR'             TO CR-HL1-RPT.                   CLPDP465
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP465
001200                                                                  CLPDP465
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP465
001500                                                                  CLPDP465
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP465
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP465
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP465
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP465
002100                                                                  CLPDP465
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP465
002400                                                                  CLPDP465
002500     MOVE SPEC-CARD-RCD          TO CR-DL5-CTL-CARD.              CLPDP465
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP465
002800                                                                  CLPDP465
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP465
003100                                                                  CLPDP465
003110*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
003200     COPY CPPDTIME.                                               DS795
003210     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP465
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP465
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP465
003700                                                                  CLPDP465
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP465
004000*                                                                 CLPDP465
004100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP465
004200*                                                                 CLPDP465
004300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP465
004400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP465
004500*                                                                 CLPDP465
004600*                                                                 CLPDP465
004700*                                                                 CLPDP465
004800     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA OF CONTROLRECORD.CLPDP465
004900     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP465
005000                                                                  CLPDP465
005100     CLOSE CONTROLREPORT.                                         CLPDP465
