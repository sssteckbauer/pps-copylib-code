000800*==========================================================%      UCSD0102
000200*=    COPY MEMBER: CPPDETRA                               =%      UCSD0102
000300*=    CHANGE #UCSD0102      PROJ. REQUEST: RELEASE 1138   =%      UCSD0102
000400*=    NAME: D. CHILCOAT     MODIFICATION DATE: 12/01/97   =%      UCSD0102
000500*=                                                        =%      UCSD0102
000600*=    DESCRIPTION                                         =%      UCSD0102
000700*=    CHANGES MADE TO ACCOMMODATE THE LOCAL COA           =%      UCSD0102
000900*=                                                        =%      UCSD0102
008000*==========================================================%      UCSD0102
000100**************************************************************/   32021138
000200*  COPYMEMBER: CPPDETRA                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_PHIL THOMPSON___ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*  BASE FAU RELATED PROCEDURE CODE FOR ONLINE FUNCTION ETRA. */   32021138
000700*  THIS CODE ONLY WORKS WITH THE BASE PROGRAM CPPDETRA AND   */   32021138
000800*  BASE MAP PPETRA0.                                         */   32021138
000900**************************************************************/   32021138
072700 CPPD-INITIALIZE-FIELDS SECTION.                                  CPPDETRA
072800******************************************************************CPPDETRA
072900*  INITIALIZE COLOR, HIGHLIGHTING AND PROTECTION ATTRIBUTES      *CPPDETRA
073000******************************************************************CPPDETRA
073300     PERFORM VARYING I FROM                                       CPPDETRA
073400         1 BY 1 UNTIL I > WS-NUMBER-SCRN-TRANS                    CPPDETRA
073500         MOVE UCCOMMON-CLR-ENTRY TO                               CPPDETRA
074500*****                           DE0230-XC2T-LOCC (I)              UCSD0102
074600*****                           DE0230-XC2T-ACCTC (I)             UCSD0102
074700*****                           DE0230-XC2T-COST-CENTERC (I)      UCSD0102
074800*****                           DE0230-XC2T-FNDC (I)              UCSD0102
074900*****                           DE0230-XC2T-PROJECT-CODEC (I)     UCSD0102
074910                                DE0230-XC2T-IFIS-INDEXC (I)       UCSD0102
075000                                DE0230-XC2T-SUBC (I)              CPPDETRA
076100     END-PERFORM.                                                 CPPDETRA
077700     PERFORM VARYING I FROM                                       CPPDETRA
077800         1 BY 1 UNTIL I > WS-NUMBER-SCRN-TRANS                    CPPDETRA
079800*****    MOVE WS-ATR TO         DE0230-XC2T-LOCA (I)              UCSD0102
080000*****    MOVE WS-ATR TO         DE0230-XC2T-ACCTA (I)             UCSD0102
080200*****    MOVE WS-ATR TO         DE0230-XC2T-COST-CENTERA (I)      UCSD0102
080600*****    MOVE WS-ATR TO         DE0230-XC2T-FNDA (I)              UCSD0102
080600*****    MOVE WS-ATR TO         DE0230-XC2T-PROJECT-CODEA (I)     UCSD0102
079520         MOVE WS-ATR TO         DE0230-XC2T-IFIS-INDEXA (I)       UCSD0102
080800         MOVE WS-ATR TO         DE0230-XC2T-SUBA (I)              CPPDETRA
083800     END-PERFORM.                                                 CPPDETRA
083900                                                                  CPPDETRA
084000     PERFORM VARYING I FROM                                       CPPDETRA
084100         1 BY 1 UNTIL I > WS-NUMBER-SCRN-TRANS                    CPPDETRA
084200         MOVE UCCOMMON-HLT-ENTRY TO                               CPPDETRA
085200*****                           DE0230-XC2T-LOCH (I)              UCSD0102
085300*****                           DE0230-XC2T-ACCTH (I)             UCSD0102
085400*****                           DE0230-XC2T-COST-CENTERH (I)      UCSD0102
085500*****                           DE0230-XC2T-FNDH (I)              UCSD0102
085600*****                           DE0230-XC2T-PROJECT-CODEH (I)     UCSD0102
079520                                DE0230-XC2T-IFIS-INDEXH (I)       UCSD0102
085700                                DE0230-XC2T-SUBH (I)              CPPDETRA
086800     END-PERFORM.                                                 CPPDETRA
087500                                                                  CPPDETRA
102400 CPPD-FORMAT-DATA           SECTION.                              CPPDETRA
102500******************************************************************CPPDETRA
102600*  FORMAT CPWSEDTH WITH DATA FROM THE TRANSACTION FOR PPVRTHFO   *CPPDETRA
102700******************************************************************CPPDETRA
109400                                                                  CPPDETRA
109510     ADD +1  TO CPWSEDTH-CNT.                                     CPPDETRA
109600     MOVE I TO CPWSEDTH-ELEM-SCREEN-OCCURNCE (CPWSEDTH-CNT).      CPPDETRA
109610     MOVE XC2T-FAU OF CCWSTHFT-ITEM (I)                           CPPDETRA
109620       TO CPWSEDTH-TRN-DATA(CPWSEDTH-CNT).                        CPPDETRA
109900     MOVE 0230 TO CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT).             CPPDETRA
110000     SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUE.     CPPDETRA
121400                                                                  CPPDETRA
121500 CPPD-FORMAT-SCREEN         SECTION.                              CPPDETRA
121600******************************************************************CPPDETRA
121700*  MOVE DATA FORMATTED BY PPVRTHFO TO THE MAP                    *CPPDETRA
121800******************************************************************CPPDETRA
121900                                                                  CPPDETRA
130300     MOVE CPWSEDTH-ELEM-SCREEN-OCCURNCE (EDIT-SUB) TO I.          CPPDETRA
130400     MOVE CPWSEDTH-SCREEN-DATA(EDIT-SUB) TO FAUR-FAU.             CPPDETRA
130510*****MOVE FAUR-LOCATION TO DE0230-XC2T-LOCO (I).                  UCSD0102
130530*****MOVE FAUR-ACCOUNT TO DE0230-XC2T-ACCTO (I).                  UCSD0102
130550*****MOVE FAUR-COST-CENTER TO DE0230-XC2T-COST-CENTERO (I).       UCSD0102
130560*****MOVE FAUR-FUND TO DE0230-XC2T-FNDO (I).                      UCSD0102
130570*****MOVE FAUR-PROJECT-CODE TO DE0230-XC2T-PROJECT-CODEO (I).     UCSD0102
130571     MOVE FAUR-INDEX TO DE0230-XC2T-IFIS-INDEXO (I)               UCSD0102
130571     MOVE FAUR-FUND TO DE0230-XC2T-IFIS-FUNDO (I)                 UCSD0102
130580     MOVE FAUR-SUB-ACCOUNT TO DE0230-XC2T-SUBO (I).               CPPDETRA
130600     IF CCWSTHFT-CHANGED-AT (I) > SPACE                           CPPDETRA
130700            AND UCCOMMON-FUNCTION(1:2) NOT = 'ET'                 CPPDETRA
130800        MOVE UCCOMMON-ATR-ENTRY-PRO TO                            CPPDETRA
130900*****                           DE0230-XC2T-LOCA (I)              UCSD0102
130910*****                           DE0230-XC2T-ACCTA (I)             UCSD0102
130920*****                           DE0230-XC2T-COST-CENTERA (I)      UCSD0102
130930*****                           DE0230-XC2T-FNDA (I)              UCSD0102
130940*****                           DE0230-XC2T-PROJECT-CODEA (I)     UCSD0102
506000                                DE0230-XC2T-IFIS-INDEXA (I)       UCSD0102
506100                                DE0230-XC2T-IFIS-FUNDA (I)        UCSD0102
130950                                DE0230-XC2T-SUBA (I)              CPPDETRA
131000     END-IF.                                                      CPPDETRA
145300                                                                  CPPDETRA
145400 CPPD-EDIT-DATA   SECTION.                                        CPPDETRA
145500***************************************************************** CPPDETRA
145600*  ALL ALTERED MAP FIELDS WILL BE PASSED TO VREDIT FOR          * CPPDETRA
145700*  VALUE-RANGE EDITING.                                         * CPPDETRA
145900*  FOR MAP FIELDS NOT ALTERED, VALUES FROM THE TRN EXTERNALS    * CPPDETRA
146000*  WILL BE PASSED TO VREDIT FOR DATA FORMATTING.                * CPPDETRA
146200***************************************************************** CPPDETRA
175500                                                                  CPPDETRA
175501                                                                  CPPDETRA
175502     ADD +1 TO CPWSEDTH-CNT                                       CPPDETRA
175503     MOVE I TO  CPWSEDTH-ELEM-SCREEN-OCCURNCE (CPWSEDTH-CNT)      CPPDETRA
175504     MOVE 0230 TO CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT)              CPPDETRA
175507     MOVE XC2T-FAU OF CCWSTHFT-ITEM (I) TO FAUR-FAU               CPPDETRA
175508     MOVE XC2T-FAU OF CCWSTHFH-ITEM-HOLD (I) TO FAUR-FAU-HOLD     CPPDETRA
175509                                                                  CPPDETRA
175510*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
175511*****                 DE0230-XC2T-LOCC (I)                        UCSD0102
175512*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
175513*****                 DE0230-XC2T-LOCH (I)                        UCSD0102
175514*****IF DE0230-XC2T-LOCL (I) > ZERO    OR                         UCSD0102
175515*****   DE0230-XC2T-LOCF (I) = ERASE-EOF                          UCSD0102
175516*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
175517*****   IF DE0230-XC2T-LOCF (I) = ERASE-EOF                       UCSD0102
175518*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
175519*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
175520*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
175521*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
175522*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
175523*****      MOVE FAUR-LOCATION-HOLD TO FAUR-LOCATION               UCSD0102
175524*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
175525*****                  DE0230-XC2T-LOCA (I)                       UCSD0102
175526*****   ELSE                                                      UCSD0102
175527*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
175600*****                  DE0230-XC2T-LOCA (I)                       UCSD0102
175700*****      MOVE DE0230-XC2T-LOCI (I) TO FAUR-LOCATION             UCSD0102
175900*****   END-IF                                                    UCSD0102
176000*****ELSE                                                         UCSD0102
176100*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
176200*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
176300*****   END-IF                                                    UCSD0102
176400*****END-IF.                                                      UCSD0102
176500*****                                                             UCSD0102
176900*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
177000*****                 DE0230-XC2T-ACCTC (I)                       UCSD0102
177100*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
177200*****                 DE0230-XC2T-ACCTH (I)                       UCSD0102
177300*****IF DE0230-XC2T-ACCTL (I) > ZERO    OR                        UCSD0102
177400*****   DE0230-XC2T-ACCTF (I) = ERASE-EOF                         UCSD0102
177500*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
177600*****   IF DE0230-XC2T-ACCTF (I) = ERASE-EOF                      UCSD0102
177700*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
177800*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
177900*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
178000*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
178100*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
178200*****      MOVE FAUR-ACCOUNT-HOLD TO FAUR-ACCOUNT                 UCSD0102
178400*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
178500*****                  DE0230-XC2T-ACCTA (I)                      UCSD0102
178600*****   ELSE                                                      UCSD0102
178700*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
178800*****                  DE0230-XC2T-ACCTA (I)                      UCSD0102
178900*****      MOVE DE0230-XC2T-ACCTI (I) TO FAUR-ACCOUNT             UCSD0102
179100*****   END-IF                                                    UCSD0102
179200*****ELSE                                                         UCSD0102
179210*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
179400*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
179500*****   END-IF                                                    UCSD0102
179600*****END-IF.                                                      UCSD0102
179700*****                                                             UCSD0102
180100*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
180200*****                 DE0230-XC2T-COST-CENTERC (I)                UCSD0102
180300*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
180400*****                 DE0230-XC2T-COST-CENTERH (I)                UCSD0102
180500*****IF DE0230-XC2T-COST-CENTERL (I) > ZERO    OR                 UCSD0102
180600*****   DE0230-XC2T-COST-CENTERF (I) = ERASE-EOF                  UCSD0102
180700*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
180800*****   IF DE0230-XC2T-COST-CENTERF (I) = ERASE-EOF               UCSD0102
180900*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
181000*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
181100*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
181200*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
181300*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
181400*****      MOVE FAUR-COST-CENTER-HOLD TO FAUR-COST-CENTER         UCSD0102
181600*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
181700*****                  DE0230-XC2T-COST-CENTERA (I)               UCSD0102
181800*****   ELSE                                                      UCSD0102
181900*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
182000*****                  DE0230-XC2T-COST-CENTERA (I)               UCSD0102
182100*****      MOVE DE0230-XC2T-COST-CENTERI (I) TO FAUR-COST-CENTER  UCSD0102
182300*****   END-IF                                                    UCSD0102
182400*****ELSE                                                         UCSD0102
182410*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
182600*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
182700*****   END-IF                                                    UCSD0102
182800*****END-IF.                                                      UCSD0102
182900*****                                                             UCSD0102
183300*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
183400*****                 DE0230-XC2T-FNDC (I)                        UCSD0102
183500*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
183600*****                 DE0230-XC2T-FNDH (I)                        UCSD0102
183700*****IF DE0230-XC2T-FNDL (I) > ZERO    OR                         UCSD0102
183800*****   DE0230-XC2T-FNDF (I) = ERASE-EOF                          UCSD0102
183900*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
184000*****   IF DE0230-XC2T-FNDF (I) = ERASE-EOF                       UCSD0102
184100*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
184200*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
184300*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
184400*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
184500*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
184600*****      MOVE FAUR-FUND-HOLD TO FAUR-FUND                       UCSD0102
184800*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
184900*****                  DE0230-XC2T-FNDA (I)                       UCSD0102
185000*****   ELSE                                                      UCSD0102
185100*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
185200*****                  DE0230-XC2T-FNDA (I)                       UCSD0102
185300*****      MOVE DE0230-XC2T-FNDI (I) TO FAUR-FUND                 UCSD0102
185500*****   END-IF                                                    UCSD0102
185600*****ELSE                                                         UCSD0102
185610*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
185800*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
185900*****   END-IF                                                    UCSD0102
186000*****END-IF.                                                      UCSD0102
186100*****                                                             UCSD0102
186500*****MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
186600*****                 DE0230-XC2T-PROJECT-CODEC (I)               UCSD0102
186700*****MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
186800*****                 DE0230-XC2T-PROJECT-CODEH (I)               UCSD0102
186900*****IF DE0230-XC2T-PROJECT-CODEL (I) > ZERO    OR                UCSD0102
187000*****   DE0230-XC2T-PROJECT-CODEF (I) = ERASE-EOF                 UCSD0102
187100*****   SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
187200*****   IF DE0230-XC2T-PROJECT-CODEF (I) = ERASE-EOF              UCSD0102
187300*****      OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
187400*****          AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
187500*****      OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
187600*****      AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
187700*****      SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
187800*****      MOVE FAUR-PROJECT-CODE-HOLD TO FAUR-PROJECT-CODE       UCSD0102
188000*****      MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
188100*****                  DE0230-XC2T-PROJECT-CODEA (I)              UCSD0102
188200*****   ELSE                                                      UCSD0102
188300*****      MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
188400*****                  DE0230-XC2T-PROJECT-CODEA (I)              UCSD0102
188500*****      MOVE DE0230-XC2T-PROJECT-CODEI (I) TO FAUR-PROJECT-CODEUCSD0102
188700*****   END-IF                                                    UCSD0102
188800*****ELSE                                                         UCSD0102
188810*****   IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           UCSD0102
189000*****      SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUEUCSD0102
189100*****   END-IF                                                    UCSD0102
189200*****END-IF.                                                      UCSD0102
160210*****                                                             UCSD0102
160250     MOVE UCCOMMON-CLR-ENTRY TO                                   UCSD0102
160260                      DE0230-XC2T-IFIS-INDEXC (I)                 UCSD0102
160270     MOVE UCCOMMON-HLT-ENTRY TO                                   UCSD0102
160280                      DE0230-XC2T-IFIS-INDEXH (I)                 UCSD0102
160290     IF DE0230-XC2T-IFIS-INDEXL (I) > ZERO      OR                UCSD0102
160291        DE0230-XC2T-IFIS-INDEXF (I) = ERASE-EOF                   UCSD0102
160292        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      UCSD0102
160293        IF DE0230-XC2T-IFIS-INDEXF (I) = ERASE-EOF                UCSD0102
160294           OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          UCSD0102
160295               AND DE0040-LINE-COMMANDI (I) = 'R')                UCSD0102
160296           OR (DE0040-LINE-COMMANDI (I) = 'B'                     UCSD0102
160297           AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     UCSD0102
160298           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       UCSD0102
182599           MOVE FAUR-INDEX-HOLD TO FAUR-INDEX                     UCSD0102
182599           MOVE FAUR-ORGANIZATION-HOLD TO FAUR-ORGANIZATION       UCSD0102
182599           MOVE FAUR-PROGRAM-HOLD     TO FAUR-PROGRAM             UCSD0102
182599           MOVE FAUR-FUND-HOLD        TO FAUR-FUND                UCSD0102
182599           MOVE FAUR-LOCATION-HOLD    TO FAUR-LOCATION            UCSD0102
160301           MOVE UCCOMMON-ATR-ENTRY TO                             UCSD0102
160302                       DE0230-XC2T-IFIS-INDEXA (I)                UCSD0102
160303        ELSE                                                      UCSD0102
160304           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         UCSD0102
160305                       DE0230-XC2T-IFIS-INDEXA (I)                UCSD0102
160306           MOVE DE0230-XC2T-IFIS-INDEXI (I)                       UCSD0102
523900                         TO FAUR-INDEX                            UCSD0102
524000           MOVE DE0230-XC2T-SUBI (I) TO FAUR-SUB-ACCOUNT          UCSD0102
524100           MOVE FAUR-FAU TO F001-FAU                              UCSD0102
271142           CALL PGM-PPFAU001 USING PPFAU001-INTERFACE             UCSD0102
330334           IF F001-FAU-VALID                                      UCSD0102
330335              MOVE F001-FAU        TO CPWSFAUR                    UCSD0102
330337           ELSE                                                   UCSD0102
330339              MOVE SPACES          TO FAUR-FUND                   UCSD0102
330339                                      FAUR-ORGANIZATION           UCSD0102
330339                                      FAUR-PROGRAM                UCSD0102
330339                                      FAUR-LOCATION               UCSD0102
330340           END-IF                                                 UCSD0102
160308        END-IF                                                    UCSD0102
160309     ELSE                                                         UCSD0102
160310        SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUE   UCSD0102
160313     END-IF                                                       UCSD0102
160314                                                                  UCSD0102
189300                                                                  CPPDETRA
189700     MOVE UCCOMMON-CLR-ENTRY TO                                   CPPDETRA
189800                      DE0230-XC2T-SUBC (I)                        CPPDETRA
189900     MOVE UCCOMMON-HLT-ENTRY TO                                   CPPDETRA
190000                      DE0230-XC2T-SUBH (I)                        CPPDETRA
190100     IF DE0230-XC2T-SUBL (I) > ZERO    OR                         CPPDETRA
190200        DE0230-XC2T-SUBF (I) = ERASE-EOF                          CPPDETRA
190300        SET CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT) TO TRUE      CPPDETRA
190400        IF DE0230-XC2T-SUBF (I) = ERASE-EOF                       CPPDETRA
190500           OR (CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) = 0180          CPPDETRA
190600               AND DE0040-LINE-COMMANDI (I) = 'R')                CPPDETRA
190700           OR (DE0040-LINE-COMMANDI (I) = 'B'                     CPPDETRA
190800           AND CPWSEDTH-ELEM-NUMBER(CPWSEDTH-CNT) NOT = 0040)     CPPDETRA
190900           SET CPWSEDTH-ERASE-EOF-YES(CPWSEDTH-CNT) TO TRUE       CPPDETRA
191000           MOVE FAUR-SUB-ACCOUNT-HOLD TO FAUR-SUB-ACCOUNT         CPPDETRA
191200           MOVE UCCOMMON-ATR-ENTRY TO                             CPPDETRA
191300                       DE0230-XC2T-SUBA (I)                       CPPDETRA
191400        ELSE                                                      CPPDETRA
191500           MOVE UCCOMMON-ATR-ENTRY-MDT TO                         CPPDETRA
191600                       DE0230-XC2T-SUBA (I)                       CPPDETRA
191700           MOVE DE0230-XC2T-SUBI (I) TO FAUR-SUB-ACCOUNT          CPPDETRA
191900        END-IF                                                    CPPDETRA
192400     ELSE                                                         CPPDETRA
192401        IF NOT CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)           CPPDETRA
192404           SET CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT) TO TRUECPPDETRA
192405        END-IF                                                    CPPDETRA
192406     END-IF.                                                      CPPDETRA
192407                                                                  CPPDETRA
192408*****IF CPWSEDTH-ELEM-REQUEST-EDIT(CPWSEDTH-CNT)                  CPPDETRA
192409        MOVE FAUR-FAU TO CPWSEDTH-ENTERED-DATA(CPWSEDTH-CNT).     CPPDETRA
192410*****ELSE                                                         CPPDETRA
192411*****IF CPWSEDTH-ELEM-REQUEST-DISPLAY(CPWSEDTH-CNT)               CPPDETRA
192412        MOVE FAUR-FAU TO CPWSEDTH-TRN-DATA(CPWSEDTH-CNT).         CPPDETRA
192420*****END-IF                                                       CPPDETRA
192430*****END-IF.                                                      CPPDETRA
228900                                                                  CPPDETRA
251800 CPPD-SET-VR-ERROR SECTION.                                       CPPDETRA
251900******************************************************************CPPDETRA
252000*  SET ERROR CODE AND HIGHLIGHTING FOR FIELDS WITH VALUE RANGE   *CPPDETRA
252100*  ERRORS RETURNED FROM PPEDTMGR                                 *CPPDETRA
252200******************************************************************CPPDETRA
252300                                                                  CPPDETRA
268400       SET CPWSEDTH-ELEM-ERRORS-FOUND(EDIT-SUB) TO TRUE.          CPPDETRA
268500       MOVE UCCOMMON-HLT-ERROR                                    CPPDETRA
268600*****            TO DE0230-XC2T-LOCH (I).                         UCSD0102
268601                 TO DE0230-XC2T-IFIS-INDEXH (I)                   UCSD0102
268700       MOVE UCCOMMON-ATR-ERROR-MDT                                CPPDETRA
268800*****            TO DE0230-XC2T-LOCA (I).                         UCSD0102
268601                 TO DE0230-XC2T-IFIS-INDEXA (I)                   UCSD0102
268900       MOVE UCCOMMON-CLR-ERROR                                    CPPDETRA
269000*****            TO DE0230-XC2T-LOCC (I).                         UCSD0102
268601                 TO DE0230-XC2T-IFIS-INDEXC (I)                   UCSD0102
269100       MOVE CPWSEDTH-SCREEN-DATA(EDIT-SUB)                        CPPDETRA
269200*****            TO DE0230-XC2T-LOCO (I).                         UCSD0102
531000                 TO FAUR-FAU                                      UCSD0102
531100       MOVE FAUR-INDEX TO DE0230-XC2T-IFIS-INDEXO (I).            UCSD0102
531200       MOVE FAUR-SUB-ACCOUNT TO DE0230-XC2T-SUBO (I).             UCSD0102
269300*****  MOVE  -1 TO DE0230-XC2T-LOCL (I)                           UCSD0102
269300       MOVE  -1 TO DE0230-XC2T-IFIS-INDEXL (I)                    UCSD0102
269400       SET ERRORS-FLAGGED TO TRUE.                                CPPDETRA
269500       SET UCCOMMON-EDIT-ERRORS-FOUND TO TRUE.                    CPPDETRA
269600                                                                  CPPDETRA
269700       MOVE CPWSSEVX-SEV-FATAL TO                                 CPPDETRA
269800                 HIGHEST-SEV OF CCWSTHFT-ITEM (I).                CPPDETRA
296100                                                                  CPPDETRA
299300 CPPD-EDIT-ERROR-ROUTINE    SECTION.                              CPPDETRA
299400******************************************************************CPPDETRA
299500*  SET ATTRIBUTES AND ERROR CODE FOR FIELDS IN ERROR             *CPPDETRA
299600******************************************************************CPPDETRA
532500       IF CPWSEDTH-ELEM-ERRORS-FOUND(EDIT-SUB)                    CPPDETAP
532600          MOVE CPWSEDTH-ELEM-SCREEN-OCCURNCE (EDIT-SUB) TO I      CPPDETAP
532700*****     MOVE UCCOMMON-HLT-ERROR                                 UCSD0102
532800*****            TO DE0230-XC2T-LOCH (I).                         UCSD0102
532900*****     MOVE UCCOMMON-ATR-ERROR-MDT                             UCSD0102
533000*****            TO DE0230-XC2T-LOCA (I).                         UCSD0102
533100*****     MOVE UCCOMMON-CLR-ERROR                                 UCSD0102
533200*****            TO DE0230-XC2T-LOCC (I).                         UCSD0102
533300          IF UCCOMMON-MSG-NUMBER = 'AU002'                        UCSD0102
533400             MOVE UCCOMMON-HLT-ERROR                              UCSD0102
315600                 TO DE0230-XC2T-IFIS-INDEXH (I)                   UCSD0102
533600             MOVE UCCOMMON-ATR-ERROR-MDT                          UCSD0102
315800                 TO DE0230-XC2T-IFIS-INDEXA (I)                   UCSD0102
533800             MOVE UCCOMMON-CLR-ERROR                              UCSD0102
316000                 TO DE0230-XC2T-IFIS-INDEXC (I)                   UCSD0102
534000             MOVE -1  TO DE0230-XC2T-IFIS-INDEXL (I)              UCSD0102
534100          ELSE                                                    UCSD0102
534200          IF UCCOMMON-MSG-NUMBER = 'AU001'                        UCSD0102
534210             MOVE UCCOMMON-HLT-ERROR                              UCSD0102
534220                 TO DE0230-XC2T-SUBH (I)                          UCSD0102
534230             MOVE UCCOMMON-ATR-ERROR-MDT                          UCSD0102
534240                 TO DE0230-XC2T-SUBA (I)                          UCSD0102
534250             MOVE UCCOMMON-CLR-ERROR                              UCSD0102
534260                 TO DE0230-XC2T-SUBC (I)                          UCSD0102
534270             MOVE -1  TO DE0230-XC2T-SUBL (I)                     UCSD0102
534280          END-IF                                                  UCSD0102
534290          END-IF                                                  UCSD0102
534291          MOVE CPWSEDTH-SCREEN-DATA(EDIT-SUB)                     CPPDETAP
316200*****            TO DE0230-XC2T-LOCO (I)                          UCSD0102
534293                 TO FAUR-FAU                                      UCSD0102
534294          MOVE FAUR-INDEX   TO DE0230-XC2T-IFIS-INDEXO (I)        UCSD0102
534295          MOVE FAUR-SUB-ACCOUNT TO DE0230-XC2T-SUBO (I)           UCSD0102
316300*****     MOVE  -1 TO DE0230-XC2T-LOCL (I)                        UCSD0102
316400               SET ERRORS-FLAGGED TO TRUE                         CPPDETRA
316500          MOVE CPWSSEVX-SEV-FATAL TO                              CPPDETRA
316600                 HIGHEST-SEV OF CCWSTHFT-ITEM (I)                 CPPDETRA
316700       END-IF.                                                    CPPDETRA
342900                                                                  CPPDETRA
343000 CPPD-UPDATE-TRANS         SECTION.                               CPPDETRA
343100******************************************************************CPPDETRA
343200*  UPDATE TRANSACTION WITH DATA IN CPWSEDTH ARRAY                *CPPDETRA
343300******************************************************************CPPDETRA
349800                                                                  CPPDETRA
350000       IF CPWSEDTH-ELEM-REQUEST-EDIT(EDIT-SUB)                    CPPDETRA
350100          MOVE CPWSEDTH-ELEM-SCREEN-OCCURNCE (EDIT-SUB) TO I      CPPDETRA
350200          MOVE CPWSEDTH-TRN-DATA(EDIT-SUB)                        CPPDETRA
350300                 TO XC2T-FAU OF CCWSTHFT-ITEM (I)                 CPPDETRA
350400       END-IF.                                                    CPPDETRA
361200                                                                  CPPDETRA
385000 CPPD-COMPLETE-LINE-COMMANDS SECTION.                             CPPDETRA
385100******************************************************************CPPDETRA
385200*  MOVE COPIED OR RETRIEVED TRANSACTIONS FROM THE HOLD AREA      *CPPDETRA
385300*  TO THE CURRENT TRANSACTION AREA                               *CPPDETRA
385400******************************************************************CPPDETRA
390500       MOVE UCCOMMON-ATR-ENTRY-MDT TO                             CPPDETRA
390600*****                  DE0230-XC2T-LOCA (J).                      UCSD0102
390601                       DE0230-XC2T-IFIS-INDEXA (J).               UCSD0102
390700*****  MOVE UCCOMMON-ATR-ENTRY-MDT TO                             UCSD0102
390800*****                  DE0230-XC2T-ACCTA (J).                     UCSD0102
390900*****  MOVE UCCOMMON-ATR-ENTRY-MDT TO                             UCSD0102
391000*****                  DE0230-XC2T-COST-CENTERA (J).              UCSD0102
391100*****  MOVE UCCOMMON-ATR-ENTRY-MDT TO                             UCSD0102
391200*****                  DE0230-XC2T-FNDA (J).                      UCSD0102
391300*****  MOVE UCCOMMON-ATR-ENTRY-MDT TO                             UCSD0102
391400*****                  DE0230-XC2T-PROJECT-CODEA (J).             UCSD0102
391500       MOVE UCCOMMON-ATR-ENTRY-MDT TO                             CPPDETRA
391600                       DE0230-XC2T-SUBA (J).                      CPPDETRA
401400                                                                  CPPDETRA
428800 CPPD-SET-ATTRIBUTES SECTION.                                     CPPDETRA
428900******************************************************************CPPDETRA
429000*  SET ATTRIBUTES FOR THE DATA ASSOCIATED WITH A MESSAGE         *CPPDETRA
429100******************************************************************CPPDETRA
429200                                                                  CPPDETRA
438700*****  MOVE -1   TO DE0230-XC2T-LOCL (WS-TRANS).                  UCSD0102
438700       MOVE -1   TO DE0230-XC2T-IFIS-INDEXL (WS-TRANS).           UCSD0102
438800       MOVE UCCOMMON-ATR-ERROR-MDT                                CPPDETRA
438900*****            TO DE0230-XC2T-LOCA (WS-TRANS).                  UCSD0102
438900                 TO DE0230-XC2T-IFIS-INDEXA (WS-TRANS).           UCSD0102
439000       MOVE UCCOMMON-CLR-ERROR                                    CPPDETRA
439100*****            TO DE0230-XC2T-LOCC (WS-TRANS).                  UCSD0102
439100                 TO DE0230-XC2T-IFIS-INDEXC (WS-TRANS).           UCSD0102
439200       MOVE UCCOMMON-HLT-ERROR                                    CPPDETRA
439300*****            TO DE0230-XC2T-LOCH (WS-TRANS).                  UCSD0102
439300                 TO DE0230-XC2T-IFIS-INDEXH (WS-TRANS).           UCSD0102
