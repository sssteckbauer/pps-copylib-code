000100*                                                                 CLPDP785
000200*      CONTROL REPORT PPP785CR                                    CLPDP785
000300*                                                                 CLPDP785
000400*                                                                 CLPDP785
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP785
000600                                                                  CLPDP785
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP785
000800                                                                  CLPDP785
000900     MOVE 'PPP785CR'             TO CR-HL1-RPT.                   CLPDP785
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP785
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP785
001200                                                                  CLPDP785
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP785
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP785
001500                                                                  CLPDP785
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP785
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP785
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP785
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP785
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP785
002100                                                                  CLPDP785
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP785
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP785
002400                                                                  CLPDP785
002500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP785
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP785
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP785
002800                                                                  CLPDP785
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP785
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP785
003100                                                                  CLPDP785
003200     ACCEPT TIME-WORK-AREA FROM TIME.                             CLPDP785
003300*    MOVE TIME-OF-DAY            TO TIME-WORK-AREA.               CLPDP785
003400     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP785
003500     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP785
003600     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP785
003700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP785
003800                                                                  CLPDP785
003900     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP785
004000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP785
004100*                                                                 CLPDP785
004200*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP785
004300*                                                                 CLPDP785
004400*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP785
004500*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP785
004600*                                                                 CLPDP785
004700     MOVE 'CNA EMPLOYEES'        TO CR-DL9-FILE.                  CLPDP785
004800     MOVE 'DELETED'              TO CR-DL9-ACTION.                CLPDP785
004900     MOVE WS-TOT-CPR-COUNT       TO CR-DL9-VALUE.                 CLPDP785
005000     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP785
005100     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP785
005200*                                                                 CLPDP785
005300*                                                                 CLPDP785
005400*                                                                 CLPDP785
005500     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP785
005600     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP785
005700                                                                  CLPDP785
005800     CLOSE CONTROLREPORT.                                         CLPDP785
