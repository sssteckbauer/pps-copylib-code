000000**************************************************************/   48631400
000001*  COPYMEMBER: CPWSXFEA                                      */   48631400
000002*  RELEASE: ___1400______ SERVICE REQUEST(S): ____14863____  */   48631400
000003*  NAME:_______QUAN______ MODIFICATION DATE:  ___02/14/02__  */   48631400
000004*  DESCRIPTION:                                              */   48631400
000005*  - INCREASE LENGTH OF PERCENT OF EXCESS FROM V999 TO V99999*/   48631400
000006*  - CHANGED ENDING ENTRIES FOR SINGLE AND MARRIED RATE SETS,*/   48631400
000007*    ENTRIES 1  - 6                                          */   48631400
000008*  - CHANGED ENDING ENTRIES FOR MARRIED, BOTH SPOUSES FILING */   48631400
000009*    W-5.                                                    */   48631400
000010*    ENTRIES 7  - 12                                         */   48631400
000011*  - ADDED THIRD RATE SET FOR MARRIED, SPOUSE NOT FILING W-5,*/   48631400
000012*    ENTRIES 13 - 18                                         */   48631400
000020**************************************************************/   48631400
000100**************************************************************/   EFIX1371
000200*  COPYMEMBER: CPWSXFEA                                      */   EFIX1371
000300*  RELEASE: ___1371______ SERVICE REQUEST(S): _____EFIX____  */   EFIX1371
000400*  NAME:_______QUAN______ CREATION DATE:      ___08/20/01__  */   EFIX1371
000500*  DESCRIPTION:                                              */   EFIX1371
000600*  LINKAGE AREA FOR PPEICUTL.                                */   EFIX1371
000700**************************************************************/   EFIX1371
000800*    COPYID=CPWSXFEA                                              CPWSXFEA
000900*01  FEDERAL-EIC-AREA.                                            CPWSXFEA
001000     05  XFEA-HOLD               OCCURS 4 TIMES.                  CPWSXFEA
001100         10  XFEA-PERIOD         PIC X.                           CPWSXFEA
001200         10  XFEA-NO-SINGLES     PIC S9(3)       COMP-3.          CPWSXFEA
001300         10  XFEA-NO-MARRIED     PIC S9(3)       COMP-3.          CPWSXFEA
001310         10  XFEA-NO-MAR-SP-NOTFILW5 PIC S9(3)   COMP-3.          48631400
001400         10  XFEA-TAX-TABLE.                                      CPWSXFEA
001500*****        15  XFEA-TAXES      OCCURS 22 TIMES.                 48631400
001510             15  XFEA-TAXES      OCCURS 18 TIMES.                 48631400
001600                 20  XFEA-SALARY PIC S9(5)       COMP-3.          CPWSXFEA
001700                 20  XFEA-TAX    PIC S9(5)V99    COMP-3.          CPWSXFEA
001800*****            20  XFEA-PCT    PIC SV999       COMP-3.          48631400
001810                 20  XFEA-PCT    PIC SV99999     COMP-3.          48631400
001900*                                                                 CPWSXFEA
002000*    TABLE AREAS ARE LOADED AS THEY COME FROM CONTROL TABLE -AND  CPWSXFEA
002100*    MATCH CYCLES RUNNING THIS PAYROLL - ON TAX TABLE             48631400
002110*               1  - 06   IS SINGLE                               48631400
002200*               07 - 12  ARE MARRIED ENTRIES                      48631400
002210*               13 - 18  ARE MARRIED WITH SPOUSE NOT FILING W-5   48631400
002300*                                                                 CPWSXFEA
