000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNF109                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______JLT_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000600*    Initial release of linkage copymember between calling   */   32021138
000700*    program and module PPFAU109.                            */   32021138
000800**************************************************************/   32021138
000900*                                                             *   CPLNF109
001000*  This copymember is one of the Full Accounting Unit modules *   CPLNF109
001100*  which campuses may need to modify to accomodate their own  *   CPLNF109
001200*  Chart of Accounts structure.                               *   CPLNF109
001300*                                                             *   CPLNF109
001400***************************************************************   CPLNF109
001500*                                                                 CPLNF109
001600*01  PPFAU109-INTERFACE.                                          CPLNF109
001700     05  F109-INPUT.                                              CPLNF109
001800*                                                                 CPLNF109
001900         10  F109-IN-FAU                 PIC X(30).               CPLNF109
002000         10  F109-IN-CAMPUS              PIC X(02).               CPLNF109
002100*                                                                 CPLNF109
002200      05  F109-OUTPUT.                                            CPLNF109
002300*                                                                 CPLNF109
002400         10  F109-OUT-FAU                PIC X(30).               CPLNF109
002500         10  F109-RETURN-IND             PIC X(01).               CPLNF109
002600             88  F109-RETURN-OK                  VALUE ' '.       CPLNF109
002700*                                                                 CPLNF109
002800*  *  *  *  *                                                     CPLNF109
