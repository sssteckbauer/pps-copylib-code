000100**************************************************************/   SSSSRRRR
000200*  COPYMEMBER: CPWSCNMO                                      */   SSSSRRRR
000300*  RELEASE: ___1236______ SERVICE REQUEST(S): ____15045____  */   SSSSRRRR
000400*  NAME:_______QUAN______ CREATION DATE:      ___03/02/99__  */   SSSSRRRR
000500*  DESCRIPTION:                                              */   SSSSRRRR
000600*  - RECORD LAYOUT OF CNA MONTHLY CHANGE FILE.               */   SSSSRRRR
000700*    ANNUAL FILE USES THE SAME RECORD STRUCTURE.             */   SSSSRRRR
000800**************************************************************/   SSSSRRRR
001000*    COPYID=CPWSCNMO                                              CPWSCNMO
001100     SKIP1                                                        CPWSCNMO
001200*                                                                *CPWSCNMO
001300*01  XCNA-MONTH-ANNUAL-RECORD.                                    CPWSCNMO
001400******************************************************************CPWSCNMO
001500*                C N A    F I L E                                 CPWSCNMO
001600******************************************************************CPWSCNMO
031144*----> NOTE: TESTING OF THE PROPER PLACEMENT OF THE TABS SHOULD BECPWSCNMO
031144*---->       MADE BY EXPORTING THE OUTPUT FILE TO THE PC, THEN    CPWSCNMO
031144*---->       OPEN FILE IN A SPREADSHEET (I.E., EXCEL).            CPWSCNMO
031144*---->       LOADING ONTO A NG THE OUTPUT FILE TO THE PC FOR      CPWSCNMO
031144*----> NOTE: WHEN ADDING NEW FIELDS TO THIS COPYMEMBER, MAKE SURE CPWSCNMO
031145*----> THAT THE EXISTING FIELDS XCNA-:TYPE:-END-REC-FLAG (DETAIL) CPWSCNMO
011010*---->                          XCNA-:TYPE:-CNTRL-END-REC (HEADER)CPWSCNMO
011010*---->                          XCNA-:TYPE:-TRLR-END-REC (TRAILER)CPWSCNMO
011010*----> ARE ALWAYS THE VERY LAST FIELDS OF THE DETAIL, HEADER, AND CPWSCNMO
011010*----> TRAILER RECORDS TO PREVENT THE FTP PROCESS FROM TRUNCATING CPWSCNMO
011010*----> BLANKS TOWARD THE END OF THE RECORDS.                      CPWSCNMO
011146*----*                                                            CPWSCNMO
001700      03  XCNA-:TYPE:.                                            CPWSCNMO
001800          05  XCNA-:TYPE:-UPDATE-CODE       PIC X(01).            CPWSCNMO
001900              88 XCNA-:TYPE:-ADD                VALUE 'A'.        CPWSCNMO
002000              88 XCNA-:TYPE:-CHANGE             VALUE 'C'.        CPWSCNMO
002100              88 XCNA-:TYPE:-DELETE             VALUE 'D'.        CPWSCNMO
002210          05  XCNA-:TYPE:-FILL1             PIC X(01) VALUE X'05'.CPWSCNMO
002300          05  XCNA-:TYPE:-KEY.                                    CPWSCNMO
002400              07  XCNA-:TYPE:-LOC-CODE          PIC X(02).        CPWSCNMO
002510              07  XCNA-:TYPE:-FILL2         PIC X(01) VALUE X'05'.CPWSCNMO
002600              07  XCNA-:TYPE:-EMP-NAME          PIC X(26).        CPWSCNMO
002710              07  XCNA-:TYPE:-FILL3         PIC X(01) VALUE X'05'.CPWSCNMO
002800              07  XCNA-:TYPE:-NAMESUFFIX        PIC X(04).        CPWSCNMO
002910              07  XCNA-:TYPE:-FILL4         PIC X(01) VALUE X'05'.CPWSCNMO
003000              07  XCNA-:TYPE:-ID-NO             PIC X(09).        CPWSCNMO
003100          05  XCNA-:TYPE:-DATA.                                   CPWSCNMO
003200              07  XCNA-:TYPE:-FILL5         PIC X(01) VALUE X'05'.CPWSCNMO
003300              07  XCNA-:TYPE:-ACTION            PIC X(13).        CPWSCNMO
003410              07  XCNA-:TYPE:-FILL6         PIC X(01) VALUE X'05'.CPWSCNMO
003500              07  XCNA-:TYPE:-EMP-NAME-DATA     PIC X(26).        CPWSCNMO
003610              07  XCNA-:TYPE:-FILL7         PIC X(01) VALUE X'05'.CPWSCNMO
003700              07  XCNA-:TYPE:-NAMESUFFIX-DATA   PIC X(04).        CPWSCNMO
003810              07  XCNA-:TYPE:-FILL8         PIC X(01) VALUE X'05'.CPWSCNMO
003900              07  XCNA-:TYPE:-ADDRESS-LINE1         PIC X(30).    CPWSCNMO
004010              07  XCNA-:TYPE:-FILL9         PIC X(01) VALUE X'05'.CPWSCNMO
004100              07  XCNA-:TYPE:-ADDRESS-LINE2         PIC X(30).    CPWSCNMO
004210              07  XCNA-:TYPE:-FILL10        PIC X(01) VALUE X'05'.CPWSCNMO
004300              07  XCNA-:TYPE:-ADDRESS-CITY         PIC X(21).     CPWSCNMO
004410              07  XCNA-:TYPE:-FILL11        PIC X(01) VALUE X'05'.CPWSCNMO
004500              07  XCNA-:TYPE:-ADDRESS-STATE         PIC X(02).    CPWSCNMO
004610              07  XCNA-:TYPE:-FILL12        PIC X(01) VALUE X'05'.CPWSCNMO
004700              07  XCNA-:TYPE:-ADDRESS-ZIP           PIC X(09).    CPWSCNMO
004810              07  XCNA-:TYPE:-FILL13        PIC X(01) VALUE X'05'.CPWSCNMO
004900              07  XCNA-:TYPE:-HIRE-DATE             PIC X(10).    CPWSCNMO
005010              07  XCNA-:TYPE:-FILL14        PIC X(01) VALUE X'05'.CPWSCNMO
005100              07  XCNA-:TYPE:-DEPT-ADDRESS          PIC X(30).    CPWSCNMO
005210              07  XCNA-:TYPE:-FILL15        PIC X(01) VALUE X'05'.CPWSCNMO
005300              07  XCNA-:TYPE:-DEPT-NAME             PIC X(30).    CPWSCNMO
005410              07  XCNA-:TYPE:-FILL16        PIC X(01) VALUE X'05'.CPWSCNMO
005500              07  XCNA-:TYPE:-LOA-BEGIN-DATE        PIC X(10).    CPWSCNMO
005610              07  XCNA-:TYPE:-FILL17        PIC X(01) VALUE X'05'.CPWSCNMO
005700              07  XCNA-:TYPE:-LOA-RETURN-DATE       PIC X(10).    CPWSCNMO
005810              07  XCNA-:TYPE:-FILL18        PIC X(01) VALUE X'05'.CPWSCNMO
005900              07  XCNA-:TYPE:-LOA-TYPE-CODE         PIC X(02).    CPWSCNMO
006010              07  XCNA-:TYPE:-FILL19        PIC X(01) VALUE X'05'.CPWSCNMO
006100              07  XCNA-:TYPE:-LOA-TYPE-DESC         PIC X(30).    CPWSCNMO
006210              07  XCNA-:TYPE:-FILL20        PIC X(01) VALUE X'05'.CPWSCNMO
006300              07  XCNA-:TYPE:-SEPARATE-DATE         PIC X(10).    CPWSCNMO
006410              07  XCNA-:TYPE:-FILL21        PIC X(01) VALUE X'05'.CPWSCNMO
006500              07  XCNA-:TYPE:-SEPARATE-DESC         PIC X(30).    CPWSCNMO
006610              07  XCNA-:TYPE:-FILL22        PIC X(01) VALUE X'05'.CPWSCNMO
006700              07  XCNA-:TYPE:-DUES-DEDUCTION-AMT                  CPWSCNMO
006800                                                    PIC -9(07).99.CPWSCNMO
006900              07  XCNA-:TYPE:-DUES-DEDUCT-AMT-X REDEFINES         CPWSCNMO
007000                  XCNA-:TYPE:-DUES-DEDUCTION-AMT    PIC X(11).    CPWSCNMO
007110              07  XCNA-:TYPE:-FILL23        PIC X(01) VALUE X'05'.CPWSCNMO
007200*                                                                 CPWSCNMO
007300          05  XCNA-:TYPE:-NO-TITLES                 PIC 9(02).    CPWSCNMO
007400          05  XCNA-:TYPE:-NO-TITLES-X     REDEFINES               CPWSCNMO
007500              XCNA-:TYPE:-NO-TITLES                 PIC X(02).    CPWSCNMO
007600*                                                                 CPWSCNMO
007700          05  XCNA-:TYPE:-APPT-ARRAY.                             CPWSCNMO
007800              07  XCNA-:TYPE:-APPT-ENTRIES  OCCURS 9 TIMES.       CPWSCNMO
007900                  09  XCNA-:TYPE:-APPT-FILLER-1     PIC X(01).    CPWSCNMO
008000                  09  XCNA-:TYPE:-TITLE-CODE        PIC X(04).    CPWSCNMO
008100                  09  XCNA-:TYPE:-APPT-FILLER-2     PIC X(01).    CPWSCNMO
008200                  09  XCNA-:TYPE:-ABRV-TITLE-NAME   PIC X(30).    CPWSCNMO
008300                  09  XCNA-:TYPE:-APPT-FILLER-3     PIC X(01).    CPWSCNMO
008400                  09  XCNA-:TYPE:-PAY-RATE      PIC -9(06).99.    CPWSCNMO
008500                  09  XCNA-:TYPE:-PAY-RATE-X  REDEFINES           CPWSCNMO
008600                      XCNA-:TYPE:-PAY-RATE           PIC X(10).   CPWSCNMO
008700                  09  XCNA-:TYPE:-APPT-FILLER-4      PIC X(01).   CPWSCNMO
008800                  09  XCNA-:TYPE:-PRCNT-FULLTIME     PIC -9.99.   CPWSCNMO
008900                  09  XCNA-:TYPE:-PRCNT-FULLTIME-X REDEFINES      CPWSCNMO
009000                      XCNA-:TYPE:-PRCNT-FULLTIME     PIC X(05).   CPWSCNMO
009010                                                                  CPWSCNMO
009020          05  XCNA-:TYPE:-END-REC-FLAG      PIC X(01).            CPWSCNMO
009100                                                                  CPWSCNMO
009200      03  XCNA-:TYPE:-CNTRL-DATA     REDEFINES                    CPWSCNMO
009300          XCNA-:TYPE:.                                            CPWSCNMO
009400          05  FILLER                                PIC X(01).    CPWSCNMO
009500          05  XCNA-:TYPE:-CNTRL-FILLER-1            PIC X(01).    CPWSCNMO
009600          05  XCNA-:TYPE:-CNTRL-KEY.                              CPWSCNMO
009700              07  XCNA-:TYPE:-CNTRL-LOC-CODE    PIC X(02).        CPWSCNMO
009500              07  XCNA-:TYPE:-CNTRL-FILLER-2    PIC X(01).        CPWSCNMO
009800              07  XCNA-:TYPE:-CNTRL-EMP-NAME    PIC X(26).        CPWSCNMO
009800              07  XCNA-:TYPE:-CNTRL-FILLER-3    PIC X(01).        CPWSCNMO
009800              07  XCNA-:TYPE:-CNTRL-NAMESUFFIX  PIC X(04).        CPWSCNMO
009800              07  XCNA-:TYPE:-CNTRL-FILLER-4    PIC X(01).        CPWSCNMO
009800              07  XCNA-:TYPE:-CNTRL-EMP-ID      PIC X(09).        CPWSCNMO
010200          05  XCNA-:TYPE:-CNTRL-FILLER-5        PIC X(05).        CPWSCNMO
010300          05  XCNA-:TYPE:-CNTRL-FILLER-6        PIC X(01).        CPWSCNMO
010400          05  XCNA-:TYPE:-CNTRL-TRANS-DATE          PIC 9(08).    CPWSCNMO
010500          05  XCNA-:TYPE:-CNTRL-TRANS-DATE-X REDEFINES            CPWSCNMO
010600              XCNA-:TYPE:-CNTRL-TRANS-DATE.                       CPWSCNMO
010700              07  XCNA-:TYPE:-CNTRL-TRANS-MM         PIC 9(02).   CPWSCNMO
010800              07  XCNA-:TYPE:-CNTRL-TRANS-DD         PIC 9(02).   CPWSCNMO
010900              07  XCNA-:TYPE:-CNTRL-TRANS-CCYY       PIC 9(04).   CPWSCNMO
011000          05  FILLER                                 PIC X(792).  CPWSCNMO
011010          05  XCNA-:TYPE:-CNTRL-END-REC         PIC X(01).        CPWSCNMO
011100                                                                  CPWSCNMO
011200      03  XCNA-:TYPE:-TRAILER-DATA  REDEFINES                     CPWSCNMO
011300          XCNA-:TYPE:.                                            CPWSCNMO
011400          05  FILLER                          PIC X(01).          CPWSCNMO
011500          05  XCNA-:TYPE:-TRLR-FILLER-1       PIC X(01).          CPWSCNMO
011600          05  XCNA-:TYPE:-TRLR-KEY.                               CPWSCNMO
011700              07  XCNA-:TYPE:-TRLR-LOC-CODE       PIC X(02).      CPWSCNMO
011500              07  XCNA-:TYPE:-TRLR-FILLER-2       PIC X(01).      CPWSCNMO
011700              07  XCNA-:TYPE:-TRLR-EMP-NAME       PIC X(26).      CPWSCNMO
011800              07  XCNA-:TYPE:-TRLR-FILLER-3       PIC X(01).      CPWSCNMO
011700              07  XCNA-:TYPE:-TRLR-NAMESUFFIX     PIC X(04).      CPWSCNMO
011800              07  XCNA-:TYPE:-TRLR-FILLER-4       PIC X(01).      CPWSCNMO
011700              07  XCNA-:TYPE:-TRLR-EMP-ID         PIC X(09).      CPWSCNMO
012200          05  XCNA-:TYPE:-TRLR-FILLER-5       PIC X(01).          CPWSCNMO
012200          05  XCNA-:TYPE:-TRLR-FILLER-6       PIC X(04).          CPWSCNMO
012300          05  XCNA-:TYPE:-TRLR-FILLER-7       PIC X(01).          CPWSCNMO
012400          05  XCNA-:TYPE:-TRLR-REC-CT         PIC 9(09).          CPWSCNMO
012500          05  FILLER                          PIC X(791).         CPWSCNMO
012510          05  XCNA-:TYPE:-TRLR-END-REC        PIC X(01).          CPWSCNMO
012600*                                                                 CPWSCNMO
