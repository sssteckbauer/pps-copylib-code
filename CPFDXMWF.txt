000100*    COPYID=CPFDXMWF                                              CPFDXMWF
000200     RECORDING MODE F                                             CPFDXMWF
000300     LABEL RECORDS STANDARD                                       CPFDXMWF
000400     BLOCK CONTAINS 0 RECORDS.                                    CPFDXMWF
000500*                                                                 CPFDXMWF
000600 01  MESSAGE-RECORD.                                              CPFDXMWF
000700     05  FILLER                  PIC X(260).                      CPFDXMWF
000800     05  MESSAGE-SEQ             PIC 9(6).                        CPFDXMWF
