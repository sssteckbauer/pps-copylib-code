000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNF030                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______WJG_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000510*                                                            */   32021138
000600*  Initial release of linkage copymember between calling     */   32021138
000700*  program and module PPFAU030                               */   32021138
000710*                                                            */   32021138
000800**************************************************************/   32021138
000900*                                                            */   CPLNF030
001000*  This copymember is one of the Full Accounting Unit modules*/   CPLNF030
001100*  which each campus may need to modify to accommodate its   */   CPLNF030
001200*  Chart of Accounts structure.                              */   CPLNF030
001300*                                                            */   CPLNF030
002100**************************************************************/   CPLNF030
002300*01  PPFAU030-INTERFACE.                                          CPLNF030
002400    05  F030-INPUTS.                                              CPLNF030
002500        10  F030-FAU                     PIC X(30).               CPLNF030
002620    05  F030-OUTPUTS.                                             CPLNF030
002633        10  F030-BUDGET-LEVEL            PIC X(06).               CPLNF030
002634            88  F030-BUDGETED-NON-ACAD-SALARY VALUE '1     '.     CPLNF030
002635            88  F030-NON-BUDGETED-SALARY      VALUE '2     '.     CPLNF030
002640        10  F030-FAU-MINUS-BUDGET-LEVEL  PIC X(30).               CPLNF030
002730        10  F030-FAILURE-CODE            PIC S9(04) COMP SYNC.    CPLNF030
002740        10  F030-FAILURE-TEXT            PIC X(35).               CPLNF030
