000010*==========================================================%      DS795
000020*=    COPY MEMBER: CLPDP380                               =%      DS795
000030*=    CHANGE # DS795        PROJ. REQUEST: LE/370         =%      DS795
000040*=                                         CONVERSION     =%      DS795
000050*=    NAME: R. SWIDERSKI    MODIFICATION DATE: 09/01/93   =%      DS795
000060*=                                                        =%      DS795
000070*=    DESCRIPTION:                                        =%      DS795
000080*=     REPLACED TIME 'ACCEPT' STATEMENT WITH              =%      DS795
000090*=     LE/370 VERSION OF CPPDTIME                         =%      DS795
000091*==========================================================%      DS795
000100*                                                                 CLPDP380
000200*      CONTROL REPORT PPP380CR                                    CLPDP380
000300*                                                                 CLPDP380
000400*                                                                 CLPDP380
000500 PRINT-CONTROL-REPORT SECTION.                                    CLPDP380
000600                                                                  CLPDP380
000700     OPEN OUTPUT CONTROLREPORT.                                   CLPDP380
000800                                                                  CLPDP380
000900     MOVE 'PPP380CR'             TO CR-HL1-RPT.                   CLPDP380
001000     MOVE CR-HDG-LINE1           TO CONTROL-DATA.                 CLPDP380
001100     WRITE CONTROLRECORD AFTER ADVANCING PAGE.                    CLPDP380
001200                                                                  CLPDP380
001300     MOVE CR-HDG-LINE2           TO CONTROL-DATA.                 CLPDP380
001400     WRITE CONTROLRECORD AFTER ADVANCING 1 LINES.                 CLPDP380
001500                                                                  CLPDP380
001600     MOVE WHEN-COMPILED          TO COMPILE-TIME-AND-DATE.        CLPDP380
001700     MOVE COMPILE-TIME           TO CR-HL3-CMP-TIME.              CLPDP380
001800     MOVE COMPILE-DATE           TO CR-HL3-CMP-DATE.              CLPDP380
001900     MOVE CR-HDG-LINE3           TO CONTROL-DATA.                 CLPDP380
002000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP380
002100                                                                  CLPDP380
002200     MOVE CR-HDG-LINE4           TO CONTROL-DATA.                 CLPDP380
002300     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP380
002400                                                                  CLPDP380
002500     MOVE NO-CONTROL-CARD        TO CR-DL5-CTL-CARD.              CLPDP380
002600     MOVE CR-DET-LINE5           TO CONTROL-DATA.                 CLPDP380
002700     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP380
002800                                                                  CLPDP380
002900     MOVE CR-HDG-LINE6           TO CONTROL-DATA.                 CLPDP380
003000     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP380
003100                                                                  CLPDP380
003200*    ACCEPT TIME-WORK-AREA FROM TIME.                             DS795
003210     COPY CPPDTIME.                                               DS795
003220     MOVE PRE-EDIT-TIME          TO TIME-WORK-AREA.               DS795
003300     MOVE CORR TIME-WORK-AREA    TO TIME-WORK-AREA-SPLIT.         CLPDP380
003400     MOVE TIME-WORK-AREA-SPLIT   TO CR-DL7-CTL-TIME.              CLPDP380
003500     MOVE CR-DET-LINE7           TO CONTROL-DATA.                 CLPDP380
003600     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP380
003700                                                                  CLPDP380
003800     MOVE CR-HDG-LINE8           TO CONTROL-DATA.                 CLPDP380
003900     WRITE CONTROLRECORD AFTER ADVANCING 3 LINES.                 CLPDP380
004000*                                                                 CLPDP380
004100*  FOLLOWING CODE MUST BE DUPLICATED FOR EVERY FILE               CLPDP380
004200*                                                                 CLPDP380
004300*  FILE NAME, ACTION (SO-CREATED OR SO-READ), AND                 CLPDP380
004400*  THE ACTUAL RECORD COUNTER MUST BE FILLED IN                    CLPDP380
004500*                                                                 CLPDP380
004600     MOVE 'CPA'                  TO CR-DL9-FILE.                  CLPDP380
004700     MOVE SO-CREATED             TO CR-DL9-ACTION.                CLPDP380
004800     MOVE RCDS-WRITTEN           TO CR-DL9-VALUE.                 CLPDP380
004900     MOVE CR-DET-LINE9           TO CONTROL-DATA.                 CLPDP380
005000     WRITE CONTROLRECORD AFTER ADVANCING 2 LINES.                 CLPDP380
005100*                                                                 CLPDP380
005200*                                                                 CLPDP380
005300*                                                                 CLPDP380
005400     MOVE END-OF-REPORT-MSG      TO CONTROL-DATA.                 CLPDP380
005500     WRITE CONTROLRECORD AFTER ADVANCING 4 LINES.                 CLPDP380
005600                                                                  CLPDP380
005700     CLOSE CONTROLREPORT.                                         CLPDP380
