002800*    COPYID=CPWSXFUT                                              CPWSXFUT
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXFUT                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000200**************************************************************/   14420362
000300*  COPYMEMBER: CPWSXFUT                                      */   14420362
000400*  RELEASE # ____0362____ SERVICE REQUEST NO(S)____1442______*/   14420362
000500*  NAME ______SBI______   MODIFICATION DATE ____05/26/88_____*/   14420362
000600*                                                            */   14420362
000700*  DESCRIPTION                                               */   14420362
000800*   - ADDED OVERFLOW CONDITION TO RETURN INDICATOR.          */  *14420362
000900**************************************************************/   14420362
001000**************************************************************/   30890361
001100*    COPYMEMBER:  CPWSXFUT                                   */   30890361
001200*    REL#: ____0361____  SERVICE REQUEST NO(S):____3089____  */   30890361
001300*    NAME: ____WEJ____   MODIFICATION DATE: ___07/06/88____  */   30890361
001400*    DESCRIPTION:                                            */   30890361
001500*     - ADDITION OF 11 NEW CALLS TO THE FUNCTION-SEND        */   30890361
001600*       SECTION AS PART OF THE CONVERSION OF 'ENTRY'         */   30890361
001700*       POINTS TO PARAGRAPHS TO ALLOW DYNAMIC LINKAGE.       */   30890361
001800**************************************************************/   30890361
001900******************************************************************CPWSXFUT
002000*    THIS COPY IS A GENERAL UTILITY TO DEFINE FUNCTIONS TO BE    *CPWSXFUT
002100*    PERFORMED TO A CALLED MODULE - FUNCTION-OPERATION WILL BE   *CPWSXFUT
002200*    DEFINED AS A 8 BYTE FIELD IN WHICH CALLING PROGRAM CAN ENTER*CPWSXFUT
002300*    THE REQUIRED FUNCTION, ROUTINE OR WHATEVER IN THESE 8 BYTES,*CPWSXFUT
002400*    INCLUDE A COMMENT IN COPY MEMBER IF USING ROUTINE IN YOUR   *CPWSXFUT
002500*    PROGRAM - SOME SWITCHS WILL BE SET WITH SPECIAL MEANINGS &  *CPWSXFUT
002600*    CAN BE USED IN OTHER ROUTINES IF SO REQUIRED                *CPWSXFUT
002700******************************************************************CPWSXFUT
002800*                                                                 CPWSXFUT
002900*01  FUNCTION-UTILITY.                                            30930413
003000     05  FUNCTION-SEND           PIC X(8).                        CPWSXFUT
003100         88  STARTJOB-CALL                       VALUE 'STARTJOB'.CPWSXFUT
003200         88  STARTJOB-12-CALL                    VALUE 'START12 '.30890361
003300         88  PREAUDIT-PASS-CALL                  VALUE 'PREAUDIT'.CPWSXFUT
003400         88  PREAUDIT-12-CALL                    VALUE 'PRE12   '.30890361
003500         88  CON-EDIT-CALL                       VALUE 'CONEDIT '.30890361
003600         88  ERROR-WRITE-CALL                    VALUE 'ERRWRITE'.30890361
003700         88  DATA-LIST-INSERT-CALL               VALUE 'DLINSERT'.30890361
003800         88  DATA-LIST-SEARCH-CALL               VALUE 'DLSEARCH'.30890361
003900         88  POST-EDIT-CALL                      VALUE 'POSTCALL'.CPWSXFUT
004000         88  POST-DATA-CALL                      VALUE 'POSTDATA'.30890361
004100         88  POST-12-CALL                        VALUE 'POST12  '.30890361
004200         88  HISTORY-ACTIVITY-CALL               VALUE 'HISTACTV'.30890361
004300         88  BEFORE-IMAGE-CALL                   VALUE 'BEFORIMG'.30890361
004400         88  AUDIT-PROCESS-CALL                  VALUE 'AUDIT   '.30890361
004500     05  FUNCTION-SINGLE REDEFINES FUNCTION-SEND PIC X.           CPWSXFUT
004600         88  CALL-TO-UNLOAD                      VALUE 'U'.       CPWSXFUT
004700         88  CALL-TO-UNLOAD-NO-CLEAR             VALUE 'X'.       CPWSXFUT
004800         88  CLEAR-BEFORE-LOAD                   VALUE 'C'.       CPWSXFUT
004900         88  NORMAL-LOAD-CALL                    VALUE 'L'.       CPWSXFUT
005000     05  FUNCTION-RETURN REDEFINES FUNCTION-SEND PIC X.           CPWSXFUT
005100         88  ARRAY-UNLOAD-COMPLETE               VALUE HIGH-VALUE.CPWSXFUT
005200         88  ERROR-INVALID-ELEMENT               VALUE '1'.       CPWSXFUT
005300         88  ERROR-INVALID-BALANCE-IND           VALUE '2'.       CPWSXFUT
005400         88  ERROR-INVALID-SEGMENT               VALUE '3'.       CPWSXFUT
005500         88  CORRECT-RETURN                      VALUE SPACES.    CPWSXFUT
005600         88  OVERFLOW-CONDITION-EXISTS           VALUE '4'.       14420362
005700*                                                                 CPWSXFUT
005800*    NOTE;   PPDEDUTL (DEDUCTION BUILD ARRAY) USES THE SINGLE     CPWSXFUT
005900*            CHARACTER DEFINITION OF FUNCTION - RETURN CODE IS    CPWSXFUT
006000*            ITEM CREATED BY THAT PROGRAM.                        CPWSXFUT
