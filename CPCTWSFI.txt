000000**************************************************************/   30871465
000001*  COPYMEMBER: CPCTWSFI                                      */   30871465
000002*  RELEASE: ___1465______ SERVICE REQUEST(S): _____3087____  */   30871465
000003*  NAME:_____SRS_________ CREATION DATE:      ___02/11/03__  */   30871465
000004*  DESCRIPTION:                                              */   30871465
000005*  - NEW COPY MEMBER FOR WORK STUDY TABLE(WSF) INPUT         */   30871465
000007**************************************************************/   30871465
000008*    COPYID=CPCTWSFI                                              CPCTWSFI
010000*01  WORK-STUDY-WSF-TABLE-INPUT.                                  CPCTWSFI
010100     05 WSFI-PROGRAM-CODE                PIC X.                   CPCTWSFI
010400     05 WSFI-BEGIN-DATE                  PIC X(06).               CPCTWSFI
010500     05 WSFI-FUNDING-FAU                 PIC X(30).               CPCTWSFI
010500     05 WSFI-SPLIT-PERCENT.                                       CPCTWSFI
010500        10 WSFI-SPLIT-PERCENT-9          PIC 9V9999.              CPCTWSFI
