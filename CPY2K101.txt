000100**************************************************************/   CPY2K101
000200*  COPYMEMBER: CPY2K101                                      */   CPY2K101
000300*  RELEASE # ___NONE_____ SERVICE REQUEST NO(S)___NONE_______*/   CPY2K101
000400*  NAME NORM BUFFINGTON   MODIFICATION DATE     05/09/1997   */   CPY2K101
000500*  DESCRIPTION                                               */   CPY2K101
000600*  - NEW MEMBER CREATED FOR DETERMINING THE CENTURY          */   CPY2K101
000700*    USED NUMEROUS PLACES FOR DATE ARITHMETIC                */   CPY2K101
000800**************************************************************/   CPY2K101
000900                                                                  CPY2K101
001000     MOVE WS-YY-A6 TO WS-YY-A8                                    CPY2K101
001100     MOVE WS-MM-A6 TO WS-MM-A8                                    CPY2K101
001200     MOVE WS-DD-A6 TO WS-DD-A8                                    CPY2K101
001300     IF WS-DATE-A6 = ZERO  THEN                                   CPY2K101
001400       MOVE '00' TO WS-CN-A8                                      CPY2K101
001500     ELSE                                                         CPY2K101
001510       IF WS-DATE-A6 = '999999'  THEN                             CPY2K101
001520         MOVE '99' TO WS-CN-A8                                    CPY2K101
001530       ELSE                                                       CPY2K101
001600         IF WS-YY-A8 < '35'  THEN                                 CPY2K101
001700           MOVE '20' TO WS-CN-A8                                  CPY2K101
001800         ELSE                                                     CPY2K101
001900           MOVE '19' TO WS-CN-A8                                  CPY2K101
002000         END-IF                                                   CPY2K101
002100       END-IF                                                     CPY2K101
002110     END-IF                                                       CPY2K101
002200                                                                  CPY2K101
002300     MOVE WS-YY-B6 TO WS-YY-B8                                    CPY2K101
002400     MOVE WS-MM-B6 TO WS-MM-B8                                    CPY2K101
002500     MOVE WS-DD-B6 TO WS-DD-B8                                    CPY2K101
002600     IF WS-DATE-B6 = ZERO  THEN                                   CPY2K101
002700       MOVE '00' TO WS-CN-B8                                      CPY2K101
002800     ELSE                                                         CPY2K101
002810       IF WS-DATE-B6 = '999999'  THEN                             CPY2K101
002820         MOVE '99' TO WS-CN-B8                                    CPY2K101
002830       ELSE                                                       CPY2K101
002900         IF WS-YY-B8 < '35'  THEN                                 CPY2K101
003000           MOVE '20' TO WS-CN-B8                                  CPY2K101
003100         ELSE                                                     CPY2K101
003200           MOVE '19' TO WS-CN-B8                                  CPY2K101
003300         END-IF                                                   CPY2K101
003400       END-IF                                                     CPY2K101
003410     END-IF                                                       CPY2K101
003500                                                                  CPY2K101
003600     MOVE WS-YY-C6 TO WS-YY-C8                                    CPY2K101
003700     MOVE WS-MM-C6 TO WS-MM-C8                                    CPY2K101
003800     MOVE WS-DD-C6 TO WS-DD-C8                                    CPY2K101
003900     IF WS-DATE-C6 = ZERO  THEN                                   CPY2K101
004000       MOVE '00' TO WS-CN-C8                                      CPY2K101
004100     ELSE                                                         CPY2K101
004110       IF WS-DATE-C6 = '999999'  THEN                             CPY2K101
004120         MOVE '99' TO WS-CN-C8                                    CPY2K101
004130       ELSE                                                       CPY2K101
004200         IF WS-YY-C8 < '35'  THEN                                 CPY2K101
004300           MOVE '20' TO WS-CN-C8                                  CPY2K101
004400         ELSE                                                     CPY2K101
004500           MOVE '19' TO WS-CN-C8                                  CPY2K101
004600         END-IF                                                   CPY2K101
004700       END-IF                                                     CPY2K101
004800     END-IF.                                                      CPY2K101
