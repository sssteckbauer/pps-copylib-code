000100******************************************************************36070535
000200*  COPYMEMBER: CPLNKAC1                                          *36070535
000300*  RELEASE: ____0535____  SERVICE REQUEST(S): ____3607____       *36070535
000400*  NAME:  ______JAG_____  CREATION DATE:      __02/06/91__       *36070535
000500*  DESCRIPTION:                                                  *36070535
000600*    LINKAGE FOR PPACTION SUBROUTINE CREATED FOR DB2 EDB         *36070535
000700*    CONVERSION.                                                 *36070535
000800*                                                                *36070535
000900******************************************************************36070535
001000*    COPYID=CPLNKAC1                                              CPLNKAC1
001100******************************************************************CPLNKAC1
001200*                                                                *CPLNKAC1
001300*  THIS COPYMEMBER IDENTIFIES THE INTERFACE FIELDS USED BETWEEN  *CPLNKAC1
001400* A CALLING PROGRAM (E.G. USER08, USER12) AND PPACTION.          *CPLNKAC1
001500* INPUT PARAMETERS CONSIST OF                                    *CPLNKAC1
001600* MESSAGE LITERALS (AS DEFINED IN COMMENTS BELOW). THE INPUT     *CPLNKAC1
001700* PARAMETERS SHOULD BE ESTABLISHED DURING INITIALZATION OF THE   *CPLNKAC1
001800* CALLING PROGRAM, AND MAY THEN REMAIN UNCHANGED. THE OUTPUT     *CPLNKAC1
001900* 'RESULTS' CONSISTS OF ONE TABLE,                               *CPLNKAC1
002000*     THE 'ERROR' TABLE, IDENTIFYING ALL ERRORS ENCOUNTERED      *CPLNKAC1
002100* DURING CODE DERIVATION.                                        *CPLNKAC1
002200******************************************************************CPLNKAC1
002300                                                                  CPLNKAC1
002400*01  PPACTION-INTERFACE.                                          CPLNKAC1
002500                                                                  CPLNKAC1
002600                                                                  CPLNKAC1
002700******************************************************************CPLNKAC1
002800*                                                                *CPLNKAC1
002900* MESSAGE LITERALS:                                              *CPLNKAC1
003000*                                                                *CPLNKAC1
003100*  THE MODULE CALLING PPACTION MUST PROVIDE A LIST OF ERROR      *CPLNKAC1
003200* MESSAGE LITERALS FOR ASSIGNMENT BY PPACTION. MESSAGE LITERALS  *CPLNKAC1
003300* MUST CORRESPOND, PRECISELY, TO THE ERROR CONDITION REPRESENTED *CPLNKAC1
003400* BY THE POSITION WITHIN THE LIST, AS IDENTIFIED BELOW:          *CPLNKAC1
003500*                                                                *CPLNKAC1
003600*                                                                *CPLNKAC1
003700*  (NOTE: IF ANY ERROR MESSAGE LITERAL IS ZERO, THEN THE         *CPLNKAC1
003800*   NO MESSAGE WILL BE POSTED BY PPACTION).                      *CPLNKAC1
003900*                                                                *CPLNKAC1
004000*                                                                *CPLNKAC1
004100* POSITION    ERROR CONDITION                                    *CPLNKAC1
004200* --------    ------------------------------------------------   *CPLNKAC1
004300*                                                                *CPLNKAC1
004400*    1        PPACTION-INTERFACE ERROR-TABLE-OVERFLOW            *CPLNKAC1
004500******************************************************************CPLNKAC1
004600                                                                  CPLNKAC1
004700                                                                  CPLNKAC1
004800     05  PPACTION-MESSAGE-LITERALS.                               CPLNKAC1
004900         10  PPACTION-MESSAGE-LITERAL OCCURS 1 PIC 9(05).         CPLNKAC1
005000                                                                  CPLNKAC1
005100                                                                  CPLNKAC1
005200******************************************************************CPLNKAC1
005300*  THE ERROR TABLE COMMUNICATES, TO THE CALLING PROGRAM, A LIST  *CPLNKAC1
005400* OF ERRORS ENCOUNTERED BY PPACTION FOR AN EMPLOYEE RECORD. THIS *CPLNKAC1
005500* LIST MAY BE USED BY THE CALLING PROGRAM TO POST ERROR MESSAGES *CPLNKAC1
005600* TO AN ERROR REPORT OR FILE. THE NUMBER OF ENTRIES IN THE TABLE *CPLNKAC1
005700* IS IDENTIFIED BY AC1-ERROR-TABLE-SIZE,                         *CPLNKAC1
005800* WHICH IS SET BY PPACTION. AC1-ERROR-TABLE-PTR IS A UTILITY     *CPLNKAC1
005900* INDEX WHICH MAY BE USED BY THE CALLING PROGRAM.                *CPLNKAC1
006000******************************************************************CPLNKAC1
006100                                                                  CPLNKAC1
006200                                                                  CPLNKAC1
006300     05  AC1-ERROR-TABLE-PTR      PIC 9(03).                      CPLNKAC1
006400     05  AC1-ERROR-TABLE-SIZE     PIC 9(03).                      CPLNKAC1
006500     05  AC1-ERROR-TABLE-ENTRY OCCURS 100 TIMES.                  CPLNKAC1
006600         10  AC1-ERROR-TABLE-NO   PIC 9(05).                      CPLNKAC1
006700         10  AC1-ERROR-TABLE-FIELD PIC 9(04).                     CPLNKAC1
006710     05  AC1-CURRENT-DATE         PIC X(06).                      CPLNKAC1
006800                                                                  CPLNKAC1
006900**********************   END OF CPLNKAC1   ***********************CPLNKAC1
