000010*==========================================================%      UCSD9999
000020*= UCSD LOCAL CHANGES HAVE BEEN IMPLEMENTED.              =%      UCSD9999
000030*= COLUMN 73 THRU 80 DENOTES CHANGE NUMBER - UCSD9999     =%      UCSD9999
000040*=                                                        =%      UCSD9999
000050*==========================================================%      UCSD9999
000060*==========================================================%      DU0081
000070*=    COPYLIB: PARYNINE                                   =%      DU0081
000080*=    CHANGE #DU0081        PROJ. REQUEST: DU0081         =%      DU0081
000090*=    NAME: R WOLBERG       MODIFICATION DATE: 12/17/01   =%      DU0081
000091*=                                                        =%      DU0081
000092*=    DESCRIPTION: EXPAND NINE MONTH TABLE FROM 100       =%      DU0081
000093*=        OCCURRENCES TO 300.                             =%      DU0081
000096*==========================================================%      DU0081
000100******************************************************************PARNINE
000200*                                                                *PARNINE
000300*                P A R Y N I N E                                 *PARNINE
000400*                                                                *PARNINE
000500*    THIS MEMBER DEFINES THE INTERNAL PROGRAM TABLE WHERE THE    *PARNINE
000600*    TITLE CODES THAT ARE TO BE CONSIDERED NINE MONTH            *PARNINE
000700*    FACULTY ARE TABLED.  THIS MEMBER IS USED BY THE PAR         *PARNINE
000800*    EXTRACTION PROCESS(PAR0200A).                               *PARNINE
000900*                                                                *PARNINE
001000******************************************************************PARNINE
001100*01  NINE-MONTH-FACULTY-NM.                                       PARNINE
001200***  05  MAX-ENTRS-NM             PIC S9(4) COMP SYNC VALUE +100. DU0081
001210     05  MAX-ENTRS-NM             PIC S9(4) COMP SYNC VALUE +300. DU0081
001300     05  NUM-ENTRS-NM             PIC S9(4) COMP SYNC.            PARNINE
001400     05  MAX-CODE-NM              PIC 9(4).                       PARNINE
001500     05  MIN-CODE-NM              PIC 9(4).                       PARNINE
001600***  05  NINE-MONTH-ENTRS-NM      OCCURS 100 TIMES                DU0081
001610     05  NINE-MONTH-ENTRS-NM      OCCURS 300 TIMES                DU0081
001700                                  INDEXED BY NIN-IX1              PARNINE
001800                                             NIN-IX2              PARNINE
001900                                             NIN-IX3.             PARNINE
002000         10  NINE-MON-CD-NM       PIC 9(4).                       PARNINE
