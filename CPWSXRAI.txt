000000**************************************************************/   74611387
000001*  COPYMEMBER: CPWSXRAI                                      */   74611387
000002*  RELEASE: ___1387______ SERVICE REQUEST(S): ____17461____  */   74611387
000003*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___01/22/02__  */   74611387
000004*  DESCRIPTION:                                              */   74611387
000005*  - ADDED ERROR VALUE FOR PPIMULTI AND PPFAU002 FAILURE.    */   74611387
000007*    COMMENTS ONLY; RECORD ITSELF WAS NOT CHANGED.           */   74611387
000008**************************************************************/   74611387
000100**************************************************************/   74611376
000200*  COPYMEMBER: CPWSXRAI                                      */   74611376
000300*  RELEASE: ___1376______ SERVICE REQUEST(S): ____17461____  */   74611376
000400*  NAME:___STEINITZ______ MODIFICATION DATE:  ___10/31/01__  */   74611376
000500*  DESCRIPTION:                                              */   74611376
000600*  - ADDED LOGIC NEEDED FOR SUB-LOCATION PROCESSING.         */   74611376
000700**************************************************************/   74611376
000016**************************************************************/   32261164
000017*  COPYMEMBER: CPWSXRAI                                      */   32261164
000018*  RELEASE: ___1164______ SERVICE REQUEST(S): ____13226____  */   32261164
000019*  NAME:__PHIL THOMPSON__ MODIFICATION DATE:  ___01/12/98__  */   32261164
000020*  DESCRIPTION:                                              */   32261164
000021*  - ADDED TWO FIELDS FOR USE IN RUNNING MULTIPLE RETROS.    */   32261164
000022*    XRAI-PROCESS-ID IDENTIFIES THE RETRO PROCESS AND        */   32261164
000023*         MUST HAVE A MATCH ON THE PPPRPG TABLE              */   32261164
000024*    XRAI-PROCESS-SEQ IDENTIFIES THE SPECIFIC RETRO STEP     */   32261164
000025*         WITHIN THE PROCESS ID AND MUST HAVE A MATCH ON THE */   32261164
000026*         PPPRPG TABLE.                                      */   32261164
000027**************************************************************/   32261164
000000**************************************************************/   32021138
000001*  COPYMEMBER: CPWSXRAI                                      */   32021138
000002*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000003*  NAME:_______WJG_______ MODIFICATION DATE:  ___08/04/97__  */   32021138
000004*  DESCRIPTION:                                              */   32021138
000005*                                                            */   32021138
000006*  IMPLEMENTATION OF THE FLEXIBLE FULL ACCOUNTING UNIT (3202)*/   32021138
000007*                                                            */   32021138
000008**************************************************************/   32021138
000028**************************************************************/   37050946
000029*  COPYMEMBER: CPWSXRAI                                      */   37050946
000030*  RELEASE: ___0946______ SERVICE REQUEST(S): ____13705____  */   37050946
000031*  NAME:__H. TRUONG _____ MODIFICATION DATE:  ___12/05/94__  */   37050946
000032*  DESCRIPTION:                                              */   37050946
000033*    ADDED XRAI-MIN-PAYRATE FIELD.                           */   37050946
000034*                                                            */   37050946
000040**************************************************************/   37050946
000100**************************************************************/   05440708
000200*  COPYMEMBER: CPWSXRAI                                      */   05440708
000300*  RELEASE # ____0708____ SERVICE REQUEST NO(S)__10544_______*/   05440708
000400*  NAME _________JLT___   MODIFICATION DATE ____10/20/92_____*/   05440708
000600*  DESCRIPTION                                               */   05440708
000700*   - ADD EMPLOYEE ID TO ACCOMODATE NEW CALL TO PPIMERIT.    */   05440708
000900**************************************************************/   05440708
000100**************************************************************/   30930413
000200*  COPYMEMBER: CPWSXRAI                                      */   30930413
000300*  RELEASE # ____0413____ SERVICE REQUEST NO(S)___3093_______*/   30930413
000400*  NAME __JIM WILLIAMS_   MODIFICATION DATE ____06/30/89_____*/   30930413
000600*  DESCRIPTION                                               */   30930413
000700*   - CHANGED FOR CONVERSION FROM OS/VS COBOL TO VS COBOL II.*/   30930413
000900**************************************************************/   30930413
000100**************************************************************/   14490297
000200*  COPYMEMBER: CPWSXRAI                                      */   14490297
000300*  RLSE # ____297___  REF #_______ SVC REQ NO(S)__1449_______*/   14490297
000400*  NAME __S.ISAACS__ MODIFICATION DATE ____06/10/87__________*/   14490297
000500*  DESCRIPTION                                               */   14490297
000600*  FIELD XRAI-LOCATION ADDED TO ALLOW CALLING PROGRAMS       */   14490297
000700*  PPP910, PPP920 AND PPP930 TO PASS LOCATION CODE TO        */   14490297
000800*  CALLED PROGRAM PPIRANGE FOR USE IN DETERMINING THE        */   14490297
000900*  APPROPRIATE RDUC VALUE TO RETURN TO THE CALLING PROGRAM.  */   14490297
001000**************************************************************/   13630212
001100**************************************************************/   13760241
001200*  COPYMEMBER: CPWSXRAI                                      */   13760241
001300*  RLSE # ____241___  REF #_______ SVC REQ NO(S)__1376_______*/   13760241
001400*  NAME ___JAL______ MODIFICATION DATE ____10/10/86__________*/   13760241
001500*  DESCRIPTION                                               */   13760241
001600*  ADD A RETURN CODE TO COPYMEMBER COMMENTS:                 */   13760241
001700*     04 TITLE CODE & REP CODE FOUND BUT NOT RDUC/RATEDEF    */   13760241
001800**************************************************************/   13760241
001900*  COPYMEMBER: CPWSXRAI                                      */   13630212
002000*  RLSE # ___0212___  REF # ________ SERVICE REQ NO(S) 1363  */   13630212
002100*  NAME ___K.KELLER____   MODIFICATION DATE ____06/11/86_____*/   13630212
002200*  DESCRIPTION                                               */   13630212
002300*  A FOUR-BYTE FIELD, XRAI-CST-EFF-DATE, WAS ADDED TO        */   13630212
002400*  DESCRIBE THE COSTING EFFECTIVE DATE.                      */   13630212
002500**************************************************************/   13630212
002600**************************************************************/   13880212
002700*  COPYMEMBER: CPWSXRAI                                      */   13880212
002800*  RLSE # ___0212___  REF # ________ SERVICE REQ NO(S) 1388  */   13880212
002900*  NAME ___M.NISHI_____   MODIFICATION DATE ____05/15/86_____*/   13880212
003000*  DESCRIPTION                                               */   13880212
003100*  REMOVED THE ACADEMIC TITLE CODE RANGE 0800-3999 SINCE     */   13880212
003200*  PPIRANGE WILL NOW USE PPTTLUTL TO DETERMINE TITLE GROUP.  */   13880212
003300*                                                            */   13880212
003400*  ALSO ADDED TWO NEW ERROR CODES '31' AND '32' EXPLAINED    */   13880212
003500*  BELOW FOR ERRORS IN USING THE PPTTLUTL CALLED MODULE.     */   13880212
003600**************************************************************/   13880212
003700**************************************************************/   13340158
003800*  COPYMEMBER: CPWSXRAI                                      */   13340158
003900*  RELEASE # ___0158___   SERVICE REQUEST NO(S) 1334         */   13340158
004000*  NAME ___JAG_________   MODIFICATION DATE ____07/22/85_____*/   13340158
004100*  DESCRIPTION                                               */   13340158
004200*   ONE COMMENT LINE ADDED TO DESCRIBE XRAI-RATE-ADJ-        */   13340158
004300*  INDICATOR VALUE 99.                                       */   13340158
004400**************************************************************/   13340158
004500**************************************************************/   21980149
004600*  COPY MODULE:  CPWSXRAI                                    */   21980149
004700*  RELEASE # ___0149___   SERVICE REQUEST NO(S) ____2198____ */   21980149
004800*  NAME __B.SOLOMON____   MODIFICATION DATE ____06/01/85_____*/   21980149
004900*  DESCRIPTION                                               */   21980149
005000*       THIS IS THE INTERFACE AREA FOR THE CALLED MODULE     */   21980149
005100*       PPIRANGE.  IT IS USED FOR THE RANGE ADJUSTMENT       */   21980149
005200*       PROCESS TO DETERMINE THE RDUC CODE AND NEW PAY RATE  */   21980149
005300*       FROM THE PAY SCALE TABLE AND RDUC TABLE (21).        */   21980149
005400**************************************************************/   21980149
005500*    COPYID=CPWSXRAI                                              CPWSXRAI
005600*01  XRAI-PPIRANGE-INTERFACE.                                     30930413
005700*                                                                 CPWSXRAI
005800     05  XRAI-INPUT-DATA.                                         CPWSXRAI
005900*                                                                 CPWSXRAI
006500         10  XRAI-EMPL-ID              PIC  X(9).                 05440708
006000         10  XRAI-TITLE-CODE           PIC  X(4).                 CPWSXRAI
011200*****                                                          CD 74611387
006200             88  TITLE-CODE-DISPLAY    VALUE '1210' '3210'.       CPWSXRAI
011400         10  XRAI-SUB-LOCATION         PIC  X(2).                 74611376
006300         10  XRAI-RATE-DEF             PIC  X(1).                 CPWSXRAI
006400             88  XRAI-HOURLY-RATE      VALUE 'H'.                 CPWSXRAI
006500         10  XRAI-REPRESENTATION-CODE  PIC  X(1).                 CPWSXRAI
006600             88  XRAI-SUPV-REP-CODE    VALUE 'S'.                 CPWSXRAI
006700             88  XRAI-VALID-REP-CODE  VALUE 'C' 'U' 'A' 'S'.      CPWSXRAI
006800         10  XRAI-CURRENT-PAY          PIC  X(7).                 CPWSXRAI
006900         10  XRAI-CURRENT-PAY-RATE  REDEFINES  XRAI-CURRENT-PAY   CPWSXRAI
007000                                       PIC  9(3)V9(4).            CPWSXRAI
007100         10  XRAI-CURRENT-PAY-AMT   REDEFINES  XRAI-CURRENT-PAY   CPWSXRAI
007200                                       PIC  9(5)V9(2).            CPWSXRAI
007300*                                                                 CPWSXRAI
007400         10  XRAI-TITLE-UNIT-CODE      PIC  X(2).                 CPWSXRAI
012610*****                                                          CD 74611387
007710         10  XRAI-FAU                  PIC  X(30).                32021138
007800         10  XRAI-PROCESS-INDICATOR    PIC  X(1).                 CPWSXRAI
007900             88  XRAI-CLOSE-FILES-IND  VALUE 'C'.                 CPWSXRAI
008000             88  XRAI-EFF-DATE-IND     VALUE 'D'.                 CPWSXRAI
008100             88  XRAI-PAY-RATE-IND     VALUE 'P'.                 CPWSXRAI
008200*                                                                 CPWSXRAI
008300     05  XRAI-RETURNED-DATA.                                      CPWSXRAI
008400*                                                                 CPWSXRAI
008500         10  XRAI-RATE-ADJ-INDICATOR   PIC  9(2).                 CPWSXRAI
008600                                                                  13760241
008700*  IND=00;TITLE CODE/REP CODE/RDUC/RATE DEF/CURRENT PAY  FOUND    CPWSXRAI
008800*        RATE IS ADJUSTED TO PAY RATE OR AMT FROM TYPE 1 RECORD   CPWSXRAI
008900*  IND=01;REPORT ONLY RECORD FOUND                                CPWSXRAI
009000*  IND=02;TITLE CODE/REP CODE/RDUC/RATE DEF  FOUND, BUT           CPWSXRAI
009100*         CURRENT PAY NOT FOUND AND                               CPWSXRAI
009200*         CURRENT PAY > MIN     ON TYPE 2 RECORD AND              CPWSXRAI
009300*         CURRENT PAY < OLD MAX ON TYPE 2 RECORD                  CPWSXRAI
009400*  IND=03;TITLE CODE FOUND.  BUT                                  CPWSXRAI
009500*         NO RECORD FOUND FOR GIVEN REP CODE/RDUC/RATE DEF        CPWSXRAI
009600*  IND=04;TITLE CODE & REP CODE FOUND BUT NOT RDUC/RATEDEF        13760241
009700*  IND=05;CURRENT PAY < PAY MIN ON TYPE 2 RECORD                  CPWSXRAI
009800*  IND=06;CURRENT PAY > NEW MAX ON TYPE 2 RECORD                  CPWSXRAI
009900*  IND=07;CURRENT PAY > OLD MAX ON TYPE 2 RECORD &                CPWSXRAI
010000*                NOT > NEW MAX ON TYPE 2 RECORD &                 CPWSXRAI
010100*         TITLE CODE   NOT ACADEMIC                               CPWSXRAI
010200*             RATE IS ADJUSTED TO NEW MAX ON TYPE 2 RECORD        CPWSXRAI
010300*  IND=08;CURRENT PAY > OLD MAX ON TYPE 2 RECORD &                CPWSXRAI
010400*                NOT > NEW MAX ON TYPE 2 RECORD &                 CPWSXRAI
010500*         TITLE CODE   IS ACADEMIC                                CPWSXRAI
010600*  IND=09;TITLE CODE/REP CODE/RDUC/RATE DEF NOT FOUND             CPWSXRAI
010700*  IND=10;INVALID PROCESSING INDICATOR                            CPWSXRAI
010800*  IND=11;EFFECTIVE DATE RECORD NOT FOUND                         CPWSXRAI
016210*****                                                          CD 74611387
010910*  IND=12;SQL ERROR ACCESSING DUC/DUA TABLES                      32021138
016310*  IND=13;TITLE CODE FOUND BUT NOT NON-DEFAULT SUB-LOCATION       74611376
011000*  IND=20;CONTROL FILE OPEN ERROR                                 CPWSXRAI
011100*  IND=21;CONTROL FILE READ ERROR                                 CPWSXRAI
011200*  IND=22;RDUC KEY TABLE OVERFLOW                                 CPWSXRAI
011300*  IND=31;TITLE TYPE NOT FOUND ON TITLE CODE TABLE                13880212
011400*  IND=32;ERROR ACCESSING THE TITLE CODE TABLE                    13880212
016810*  IND=33;PPIMULTI SQL ERROR                                      74611387
016820*  IND=34;PPFAU002 CALL FAILURE                                   74611387
011500*  IND=99;VSAM ABEND IN PPIRANGE                                  13340158
011600         10  XRAI-ADJUSTED-PAY           PIC  X(7).               CPWSXRAI
011700         10  XRAI-ADJUSTED-PAY-RATE  REDEFINES  XRAI-ADJUSTED-PAY CPWSXRAI
011800                                         PIC  9(3)V9(4).          CPWSXRAI
011900         10  XRAI-ADJUSTED-PAY-AMT   REDEFINES  XRAI-ADJUSTED-PAY CPWSXRAI
012000                                         PIC  9(5)V9(2).          CPWSXRAI
012100         10  XRAI-MO-RATE-ADJ-EFF-DATE.                           CPWSXRAI
012200             15  XRAI-MO-RATE-ADJ-EFF-YR    PIC  9(2).            CPWSXRAI
012300             15  XRAI-MO-RATE-ADJ-EFF-MO    PIC  9(2).            CPWSXRAI
012400             15  XRAI-MO-RATE-ADJ-EFF-DA    PIC  9(2).            CPWSXRAI
012500*                                                                 CPWSXRAI
012600         10  XRAI-BI-RATE-ADJ-EFF-DATE.                           CPWSXRAI
012700             15  XRAI-BI-RATE-ADJ-EFF-YR    PIC  9(2).            CPWSXRAI
012800             15  XRAI-BI-RATE-ADJ-EFF-MO    PIC  9(2).            CPWSXRAI
012900             15  XRAI-BI-RATE-ADJ-EFF-DA    PIC  9(2).            CPWSXRAI
013000*                                                                 CPWSXRAI
013100         10  XRAI-RDUC                   PIC  X(1).               CPWSXRAI
013200         10  XRAI-CST-EFF-DATE.                                   13630212
013300             15  XRAI-CST-EFF-YR         PIC  9(2).               13630212
013400             15  XRAI-CST-EFF-MO         PIC  9(2).               13630212
013500         10  XRAI-MIN-PAYRATE            PIC  X(7).               37050946
013600         10  XRAI-MIN-PAYRATE-HR REDEFINES  XRAI-MIN-PAYRATE      37050946
013700                                         PIC  9(3)V9(4).          37050946
013800         10  XRAI-MIN-PAYRATE-M  REDEFINES  XRAI-MIN-PAYRATE      37050946
013900                                         PIC  9(5)V9(2).          37050946
014000         10  XRAI-MULTI-RETRO-DATA.                               32261164
014200             15  XRAI-PROCESS-ID         PIC  X(8).               32261164
014300             15  XRAI-PROCESS-SEQ        PIC  X(4).               32261164
