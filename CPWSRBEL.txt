000000**************************************************************/   02980667
000001*  COPYMEMBER: CPWSRBEL                                      */   02980667
000002*  RELEASE: ___0667______ SERVICE REQUEST(S): _____10298___  */   02980667
000003*  NAME:_____J. QUAN_____ MODIFICATION DATE:  ___04/22/92__  */   02980667
000004*  DESCRIPTION:                                              */   02980667
000005*    ADDED PRI-STAT-QUAL-CODE, PRI-STAT-QUAL-DATE,           */   02980667
000006*          SEC-STAT-QUAL-CODE, SEC-STAT-QUAL-DATE            */   02980667
000008**************************************************************/   02980667
000100**************************************************************/  *36090553
000200*  COPYMEMBER: CPWSRBEL                                      */  *36090553
000300*  RELEASE: ____0553____  SERVICE REQUEST(S): ____3609____   */  *36090553
000400*  NAME: ______PXP______  CREATION DATE: ____04/08/91_____   */  *36090553
000500*  DESCRIPTION:                                              */  *36090553
000600*  - WORKING-STORAGE LAYOUT OF BEL TABLE ROW.                */  *36090553
000700**************************************************************/  *36090553
000800*    COPYID=CPWSRBEL                                              CPWSRBEL
000900*01  BEL-ROW.                                                     CPWSRBEL
001000     10 EMPLOYEE-ID          PIC X(9).                            CPWSRBEL
001100     10 BELI-IND             PIC X(1).                            CPWSRBEL
001200     10 BELI-DATE            PIC X(10).                           CPWSRBEL
001300     10 CURR-BEN-COVERAGE    PIC X(1).                            CPWSRBEL
001400     10 BELI-DERIVED-IND     PIC X(1).                            CPWSRBEL
001500     10 BELI-CONFLICTDATE    PIC X(10).                           CPWSRBEL
001600     10 BELI-CHANGEDATE      PIC X(10).                           CPWSRBEL
001700     10 BELI-AVGHRSWEEK      PIC S9999999V99 USAGE COMP-3.        CPWSRBEL
001800     10 LOA-CURR-AVGEXCLUD   PIC X(1).                            CPWSRBEL
001900     10 LOA-JAN-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002000     10 LOA-FEB-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002100     10 LOA-MAR-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002200     10 LOA-APR-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002300     10 LOA-MAY-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002400     10 LOA-JUN-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002500     10 LOA-JUL-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002600     10 LOA-AUG-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002700     10 LOA-SEP-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002800     10 LOA-OCT-AVGEXCLUD    PIC X(1).                            CPWSRBEL
002900     10 LOA-NOV-AVGEXCLUD    PIC X(1).                            CPWSRBEL
003000     10 LOA-DEC-AVGEXCLUD    PIC X(1).                            CPWSRBEL
003100     10 JAN-BEN-COV-IND      PIC X(1).                            CPWSRBEL
003200     10 FEB-BEN-COV-IND      PIC X(1).                            CPWSRBEL
003300     10 MAR-BEN-COV-IND      PIC X(1).                            CPWSRBEL
003400     10 APR-BEN-COV-IND      PIC X(1).                            CPWSRBEL
003500     10 MAY-BEN-COV-IND      PIC X(1).                            CPWSRBEL
003600     10 JUN-BEN-COV-IND      PIC X(1).                            CPWSRBEL
003700     10 JUL-BEN-COV-IND      PIC X(1).                            CPWSRBEL
003800     10 AUG-BEN-COV-IND      PIC X(1).                            CPWSRBEL
003900     10 SEP-BEN-COV-IND      PIC X(1).                            CPWSRBEL
004000     10 OCT-BEN-COV-IND      PIC X(1).                            CPWSRBEL
004100     10 NOV-BEN-COV-IND      PIC X(1).                            CPWSRBEL
004200     10 DEC-BEN-COV-IND      PIC X(1).                            CPWSRBEL
004300     10 BELI-EFF-DATE        PIC X(10).                           CPWSRBEL
004310     10 PRI-STAT-QUAL-CODE   PIC X(02).                           02980667
004320     10 PRI-STAT-QUAL-DATE   PIC X(10).                           02980667
004330     10 SEC-STAT-QUAL-CODE   PIC X(02).                           02980667
004340     10 SEC-STAT-QUAL-DATE   PIC X(10).                           02980667
004400******************************************************************CPWSRBEL
