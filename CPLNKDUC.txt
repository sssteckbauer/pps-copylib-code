000100**************************************************************/   32021138
000200*  COPYMEMBER: CPLNKDUC                                      */   32021138
000300*  RELEASE: ___1138______ SERVICE REQUEST(S): ____13202____  */   32021138
000400*  NAME:_______WJG_______ CREATION DATE:      ___08/04/97__  */   32021138
000500*  DESCRIPTION:                                              */   32021138
000510*                                                            */   32021138
000600*    Initial release of linkage copymember between calling   */   32021138
000700*    program and module PPDUCUTL                             */   32021138
000710*                                                            */   32021138
000800**************************************************************/   32021138
000900*                                                            */   CPLNKDUC
001000*  This copymember is part of the flexible Full Accounting   */   CPLNKDUC
001100*  Unit (3202) release and is considered to be "below the    */   CPLNKDUC
001200*  line", i.e., campuses using locally defined charts of     */   CPLNKDUC
001210*  accounts may have to modify this member to accommodate    */   CPLNKDUC
001220*  local idiosyncrasies.                                     */   CPLNKDUC
001300*                                                            */   CPLNKDUC
002100***************************************************************   CPLNKDUC
002300*01  PPDUCUTL-INTERFACE.                                          CPLNKDUC
002400    05  KDUC-INPUTS.                                              CPLNKDUC
002500        10  KDUC-TUC                     PIC X(02).               CPLNKDUC
002501        10  KDUC-FAU                     PIC X(30).               CPLNKDUC
002502        10  KDUC-REP-CODE                PIC X(01).               CPLNKDUC
002510        10  KDUC-FUNCTION                PIC X(04).               CPLNKDUC
002511            88  KDUC-GET-RDUC            VALUE 'RDUC'.            CPLNKDUC
002600    05  KDUC-OUTPUTS.                                             CPLNKDUC
002610        10  KDUC-RDUC                    PIC X(01).               CPLNKDUC
002720        10  KDUC-COMPLETION-FLAG         PIC X(01).               CPLNKDUC
002721            88  KDUC-FOUND               VALUE 'F'.               CPLNKDUC
002722            88  KDUC-NOT-FOUND           VALUE 'N'.               CPLNKDUC
002723        10  KDUC-LEVEL-INDICATOR         PIC X(02).               CPLNKDUC
002724            88  KDUC-ACCOUNT-FUND        VALUE 'AF'.              CPLNKDUC
002725            88  KDUC-ACCOUNT-RANGE       VALUE 'AR'.              CPLNKDUC
002726            88  KDUC-FUND-RANGE          VALUE 'FR'.              CPLNKDUC
002727            88  KDUC-LOCATION            VALUE 'LO'.              CPLNKDUC
002730        10  KDUC-FAILURE-CODE            PIC S9(04) COMP.         CPLNKDUC
002740        10  KDUC-FAILURE-TEXT            PIC X(35).               CPLNKDUC
